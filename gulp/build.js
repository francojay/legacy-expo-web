'use strict';

var gulp = require('gulp');
var ignore = require('gulp-ignore');
const hash = require('gulp-hash-filename');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/{app,components}/**/*.html',
    paths.src + '/app/views/template_editor/**/*.html',
    paths.tmp + '/{app,components}/**/*.html'
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'expo'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(paths.tmp + '/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html');
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var assets;

  return gulp.src(paths.tmp + '/serve/*.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe(ignore.exclude(["**/*.map"]))
    .pipe($.uglify({ preserveComments: $.uglifySaveLicense }))
    .on('error', function (error) {
      console.log(error);
    })
    .pipe(jsFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/assets/images/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/images/'));
});

gulp.task('custom_fonts', function () {
  return gulp.src(paths.src + '/assets/fonts/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/fonts/'));
});

gulp.task('custom_scripts', function () {
  return gulp.src(paths.src + '/assets/scripts/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/scripts/'));
});

gulp.task('components', function () {
  return gulp.src(paths.src + '/assets/components/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/components/'));
});

gulp.task('demos', function () {
  return gulp.src(paths.src + '/assets/demo/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/demo/'));
});

gulp.task('sounds', function () {
  return gulp.src(paths.src + '/assets/sounds/**/*')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/assets/sounds/'));
});

gulp.task('robots', function () {
  return gulp.src(paths.src + '/**/robots.txt')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe(hash())
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('misc', function () {
  return gulp.src(paths.src + '/**/*.ico')
    .pipe(hash())
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('clean', function (done) {
  $.del([paths.dist + '/', paths.tmp + '/'], done);
});

gulp.task('build', ['html', 'images', 'custom_fonts', 'custom_scripts', 'robots', 'sounds', 'fonts', 'misc', 'demos',
  'components']);
