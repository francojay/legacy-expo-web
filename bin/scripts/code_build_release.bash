source venv/bin/activate

if [ "$CODEBUILD_BUILD_SUCCEEDING" = "1" ]; then
    VERSION="$(git rev-parse HEAD)"
    SLACK="https://hooks.slack.com/services/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"
    if [ "$CODEBUILD_WEBHOOK_HEAD_REF" = "refs/heads/master" ]; then
        aws s3 cp dist s3://legacy-expo-web --recursive --acl public-read
        aws s3 cp s3://legacy-expo-web/index.html s3://legacy-expo-web/index.html --metadata-directive REPLACE \
        --cache-control 'max-age=0, must-revalidate; no-cache' --content-type text/html --acl public-read
        curl -X POST -H 'Content-type: application/json' --data '{"text":"`🎉 Successfully Deployed 🎉` ```http://legacy-expo-web.s3-website-us-east-1.amazonaws.com```"}' https://hooks.slack.com/services/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
    elif [ "$CODEBUILD_WEBHOOK_HEAD_REF" = "refs/heads/develop" ]; then
        echo "deploying to staging_experimental"
        aws s3 cp dist s3://legacy-expo-web-dev --recursive --acl public-read
        aws s3 cp s3://legacy-expo-web/index.html s3://legacy-expo-web/index.html --metadata-directive REPLACE \
        --cache-control 'max-age=0, must-revalidate; no-cache' --content-type text/html --acl public-read
        curl -X POST -H 'Content-type: application/json' --data '{"text":"`🎉 Successfully Deployed 🎉` ```http://legacy-expo-web.s3-website-us-east-1.amazonaws.com```"}' https://hooks.slack.com/services/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
    else
        echo "could be worse"
    fi
fi