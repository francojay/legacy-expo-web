#!/usr/bin/env bash

#for debian flavors with apt package manager
#sudo apt-get install ruby
#if [ $(dpkg-query -W -f='${Status}' ruby 2>/dev/null | grep -c "ok installed") -eq 0 ];
#then
#  sudo apt-get install ruby;
#else
#  echo "ruby already installed";
#fi

#for red hat flavors with yum package manager
# if [ $(yum list installed 2>/dev/null | grep -c "ruby") -eq 0 ];
# then
#   sudo yum install ruby;
# else
#   echo "ruby already installed";
# fi


#all platforms
sudo gem install sass
sudo npm install bower -g
npm install