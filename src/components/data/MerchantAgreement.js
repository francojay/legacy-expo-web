(function(){
  'use strict';

  angular.module('app')
          .service('merchantService', [
          '$q', 'API', 'ConferenceService', 'ngDialog', 'toastr', 'FileService',
          merchantService
  ]);

  function merchantService($q, API, ConferenceService, ngDialog, toastr, FileService){
    var service = {};
    var vm = this;
    var $scope = this;

    vm.agreeMerchantTerms = false;
    vm.merchantAgreementStep = 1;
    // vm.merchantCompanyForm = true;
    // vm.merchantIndividualForm = false;
    vm.merchantExistence = false;

    var agreeMerchantTerms = false;

    try {
        Stripe.setPublishableKey(API.STRIPE_KEY);
    }
    catch(err) {
        console.log(err);
    }

    function merchantAgreementToggle() {
        vm.$scope.agreeMerchantTerms = !vm.$scope.agreeMerchantTerms;
    }

    function merchantAgreementBack() {
        if(vm.merchantExistence != true || vm.$scope.merchantAgreementStep == 3){
            vm.$scope.merchantAgreementStep --;
        }
    }

    function setCompanyType() {
        vm.$scope.merchantIndividualForm = false;
        vm.$scope.merchantCompanyForm = true;
    }

    function setIndividualType() {
        vm.$scope.merchantIndividualForm = true;
        vm.$scope.merchantCompanyForm = false;
    }

    function uploadMerchantDocument(files, type) {
        FileService.getUploadSignature(files[0])
            .then(function(response){
                var file_link = response.data.file_link;
                FileService.uploadFile(files[0], response)
                    .then(function(uploadResponse){
                        if(type == 'company'){
                            vm.$scope.merchantCompanyProfileModel.verification_document = file_link;
                        } else {
                            vm.$scope.merchantIndividualProfileModel.verification_document = file_link;
                        }
                    })
                    .catch(function(error){
                        console.log(error)
                    });
            })
            .catch(function(error){
                console.log(error);
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                    toastr.error('Error!', 'There has been a problem with your request.');
                }
            });
    };

    function stripeResponseHandlerBankAccount(status, response) {
        if (response.error) { // Problem!
            toastr.error(response.error.message);
        } else {
            vm.$scope.loadResponseHandler = true;
            if(vm.$scope.merchantIndividualForm){
                vm.$scope.merchantIndividualProfileModel.stripe_token = response.id;
                ConferenceService
                    .submitMerchantAgreement(vm.$scope.merchantIndividualProfileModel)
                    .then(function(response){
                        vm.$scope.loadResponseHandler = false;
                        try {
                            if (typeof  vm.$scope.merchantAgreementSuccessful === 'function') { 
                                vm.$scope.merchantAgreementSuccessful();
                            }
                        }
                        catch(err) {
                            console.log(err);
                        }

                        ngDialog.close();
                    })
                    .catch(function(error){
                        vm.$scope.loadResponseHandler = false;
                        console.log(error);
                        toastr.error(error);
                    });

            } else if(vm.$scope.merchantCompanyForm){
                vm.$scope.merchantCompanyProfileModel.stripe_token = response.id;
                ConferenceService
                    .submitMerchantAgreement(vm.$scope.merchantCompanyProfileModel)
                    .then(function(response){
                        vm.$scope.loadResponseHandler = false;
                        if (typeof  vm.$scope.merchantAgreementSuccessful === 'function') { 
                            vm.$scope.merchantAgreementSuccessful();
                        }
                        ngDialog.close();
                    })
                    .catch(function(error){
                        console.log(error);
                        vm.$scope.loadResponseHandler = false;
                        toastr.error(error);
                    });

            }
        }
    };

    function validateDate(profileModel){
        if (profileModel['dob_day'] != null && profileModel['dob_month'] != null && profileModel['dob_year'] != null){
            if(isValid(profileModel['dob_day'], profileModel['dob_month'], profileModel['dob_year']) == true){
                vm.$scope.dobIsValid = true;
            } else{
                vm.$scope.dobIsValid = false;
            }
        }
    }

    function daysInMonth(m, y) {
        switch (m) {
            case 2 :
                return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
            case 9 : case 4 : case 6 : case 11 :
                return 30;
            default :
                return 31;
        }
    }

    function isValid(d, m, y) {
        return m >= 1 && m <= 12 && d <= daysInMonth(m, y);
    }

    function validateDobDay(profileModel) {
        var daysMax = 31;
        if (profileModel['dob_month']) {
            daysMax = daysInMonth(profileModel['dob_month'], profileModel['dob_year']);
        }
        
        if (profileModel['dob_day'] < 1) profileModel['dob_day'] = 1;
        else if (profileModel['dob_day'] > daysMax) profileModel['dob_day'] = daysMax;
    }

    function validateDobMonth(profileModel) {
        if (profileModel['dob_month'] < 1) profileModel['dob_month'] = 1;
        else if (profileModel['dob_month'] > 12) profileModel['dob_month'] = 12;
        validateDobDay(profileModel);
    }

    function validateDobYear(profileModel) {
        if (profileModel['dob_year'] < 1900) profileModel['dob_year'] = 1900;
        else if (profileModel['dob_year'] > 2017) profileModel['dob_year'] = 2017;
        validateDobDay(profileModel);
    }

    function updateMerchantAccount() {
        if(vm.$scope.merchantIndividualForm){
            ConferenceService
                .submitMerchantAgreement(vm.$scope.merchantIndividualProfileModel)
                .then(function(response){
                    ngDialog.close();
                })
                .catch(function(error){
                    toastr.error(error.data.detail);
                });

        } else if(vm.$scope.merchantCompanyForm){
            ConferenceService
                .submitMerchantAgreement(vm.$scope.merchantCompanyProfileModel)
                .then(function(response){
                    ngDialog.close();
                })
                .catch(function(error){
                    toastr.error(error.data.detail);
                });
        }
    }

    function submitMerchantAgreement(){
        if (vm.$scope.merchantIdentityValid) {
            updateMerchantAccount();
        } else {
            var merchant_form = angular.element('#bank_information_form');
            Stripe.bankAccount.createToken(merchant_form, stripeResponseHandlerBankAccount);
        }
    };

    function merchantAgreementStepError(){
        toastr.error("Date of Birth is not a valid date");
    }

    function merchantAgreementNextStep(){
        if (vm.$scope.merchantAgreementStep == 1) {
            if(!vm.$scope.agreeMerchantTerms){
                toastr.warning('You must agree with the merchant agreement by checking the box after you have read the agreement.');
                return;
            } else {
                vm.$scope.merchantAgreementStep = 2;
            }
        } else if(vm.$scope.merchantAgreementStep == 2){
            var ok = true;
            if(vm.$scope.merchantIndividualForm){
                if (!vm.$scope.merchantIndividualProfileModel.verification_document && !vm.$scope.merchantCompanyProfileModel.has_verification_document) {
                    toastr.error('The verification document is required!');
                    ok = false;
                    return false;
                }
                if (!vm.$scope.merchantIndividualProfileModel.personal_id_number && !vm.$scope.merchantIndividualProfileModel.personal_id_number_provided) {
                    toastr.error('The social security number is required!');
                    ok = false;
                    return false;
                }
                vm.$scope.merchantIndividualProfileModel['legal_entity_type'] = 'individual';
                _.each(vm.$scope.merchantIndividualFields, function(detail){
                    if(detail.code != 'personal_id_number' && detail.code != 'verification_document' && detail.code != 'business_name'
                            && detail.code != 'business_tax_id'){
                        if(_.isArray(detail.code)){
                            _.each(detail.code, function(prop){
                                if(vm.$scope.merchantIndividualProfileModel[prop] == '' || vm.$scope.merchantIndividualProfileModel[prop] == null){
                                    toastr.error(detail.label + ' field is required!');
                                    ok = false;
                                    return false;
                                }
                            });
                            return false;
                        } else if(vm.$scope.merchantIndividualProfileModel[detail.code] == '' || vm.$scope.merchantIndividualProfileModel[detail.code] == null){
                            toastr.error(detail.label + ' field is required!');
                            ok = false;
                            return false;
                        }
                    }
                });
            } else if(vm.$scope.merchantCompanyForm){

                vm.$scope.merchantCompanyProfileModel['legal_entity_type'] = 'company';
                if (!vm.$scope.merchantCompanyProfileModel.verification_document && !vm.$scope.merchantCompanyProfileModel.has_verification_document) {
                    toastr.error('The verification document is required!');
                    ok = false;
                    return false;
                }

                if (!vm.$scope.merchantCompanyProfileModel.business_tax_id && !vm.$scope.merchantCompanyProfileModel.has_business_tax_id) {
                    toastr.error('The business tax id is required!');
                    ok = false;
                    return false;
                }

                if (!vm.$scope.merchantCompanyProfileModel.ssn_last_4 && !vm.$scope.merchantCompanyProfileModel.ssn_last_4_provided) {
                    toastr.error('The SSN Last 4 Digits field is required!');
                    ok = false;
                    return false;
                } else if (vm.$scope.merchantCompanyProfileModel.ssn_last_4 && vm.$scope.merchantCompanyProfileModel.ssn_last_4.length < 4){
                    toastr.error('The SSN Last 4 Digits field must have at least 4 digits!');
                    ok = false;
                    return false;
                }

                _.each(vm.$scope.merchantCompanyFields, function(detail){
                    if (detail.code == 'verification_document') {
                        if (!vm.$scope.merchantCompanyProfileModel.verification) {
                            toastr.error(detail.label + ' field is required!');
                            ok = false;
                            return false;
                        }
                    }

                    if(detail.code != 'business_tax_id' && detail.code != 'verification_document'){
                        if(_.isArray(detail.code)){
                            _.each(detail.code, function(prop){
                                if(vm.$scope.merchantCompanyProfileModel[prop] == '' || vm.$scope.merchantCompanyProfileModel[prop] == null){
                                    toastr.error(detail.label + ' field is required!');
                                    ok = false;
                                    return false;
                                }
                            });
                            return false;
                        } else if(vm.$scope.merchantCompanyProfileModel[detail.code] == '' || vm.$scope.merchantCompanyProfileModel[detail.code] == null){
                            toastr.error(detail.label + ' field is required!');
                            ok = false;
                            return false;
                        }
                    }
                });
            }
            if(ok){
                vm.$scope.merchantAgreementStep = 3;
            } else return;
        }
    }

    function validateSSNlast4() {
        vm.$scope.merchantCompanyProfileModel['ssn_last_4'] = vm.$scope.merchantCompanyProfileModel['ssn_last_4'].replace(/\D/g,'');
        vm.$scope.merchantCompanyProfileModel['ssn_last_4'] = vm.$scope.merchantCompanyProfileModel['ssn_last_4'].substring(0, 4);
    }
    
    service.startMerchantAgreement = function($scope) {
        vm.merchantExistence = false;
        $scope.merchantAgreementNextStep = $scope.merchantAgreementNextStep;
        $scope.validateDate = validateDate;
        $scope.dobIsValid = $scope.dobIsValid;
        $scope.united_states = API.UNITED_STATES;
        $scope.merchantAgreementStepError = merchantAgreementStepError;
        $scope.merchantAgreementStep = $scope.merchantAgreementStep;
        $scope.merchantCompanyFields = presets.merchantCompanyFields();
        $scope.merchantAgreementNextStep = merchantAgreementNextStep;
        $scope.agreeMerchantTerms = agreeMerchantTerms;
        $scope.merchantAgreementToggle = merchantAgreementToggle;
        $scope.merchantAgreementBack = merchantAgreementBack;
        $scope.submitMerchantAgreement = submitMerchantAgreement;
        $scope.uploadMerchantDocument = uploadMerchantDocument;
        $scope.validateSSNlast4 = validateSSNlast4;
        $scope.setCompanyType = setCompanyType;
        $scope.setIndividualType = setIndividualType;
        $scope.validateDobDay = validateDobDay;
        $scope.validateDobMonth = validateDobMonth;
        $scope.validateDobYear = validateDobYear;
        $scope.agreeMerchantTerms = false;
        $scope.merchantAgreementStep = 1;
        $scope.merchantCompanyForm = true;
        $scope.merchantIndividualForm = false;
        $scope.monthStringList = presets.monthStringList();
        $scope.editingExistingForm = false;
        // $scope.merchantAgreementSuccessful();
        vm.$scope = $scope;
        $scope.loadingData = true;
        ConferenceService
            .getMerchantAgreementResponse()
            .then(function(response){
                $scope.state_count = -1;
                vm.merchantExistence = true;
                vm.agreeMerchantTerms = true;
                $scope.dobIsValid = true;
                $scope.dobIsValid = $scope.dobIsValid;
                $scope.loadingData = false;
                $scope.merchantIndividualForm = false;
                $scope.merchantCompanyForm = true;
                $scope.editingExistingForm = true;
                $scope.agreeMerchantTerms = true;
                $scope.merchantAgreementStep = 2;
                vm.$scope.merchantAgreementStep = $scope.merchantAgreementStep;
                $scope.merchantCompanyFields = presets.merchantCompanyFields();
                $scope.merchantIndividualFields = presets.merchantIndividualFields();
                $scope.merchantIndividualProfileModel = {};
                $scope.merchantCompanyProfileModel = {};
                $scope.bankAccountData = {};

                $scope.bankAccountData.bank_account_number = '*****' + response.account.account_number_last4;
                $scope.bankAccountData.routing_number = response.account.routing_number;
                $scope.bankAccountData.bank_name = response.account.bank_name;

                // $scope.account.name = response.business_name;

                // $scope.account.routing_number = response.account.routing_number;

                if(response.legal_entity_type == 'company'){
                    $scope.type_locked = true;
                    $scope.merchantIndividualForm = false;
                    $scope.merchantCompanyForm = true;
                    $scope.merchantIdentityValid = true;
                    $scope.merchantCompanyProfileModel.last_name = response.last_name;
                    $scope.merchantCompanyProfileModel.first_name = response.first_name;
                    $scope.merchantCompanyProfileModel.line1 = response.line1;
                    $scope.merchantCompanyProfileModel.business_name = response.business_name;
                    $scope.merchantCompanyProfileModel.name = response.business_name;
                    $scope.merchantCompanyProfileModel.business_tax_id_provided = response.business_tax_id_provided;
                    $scope.merchantCompanyProfileModel.business_tax_id = '*********';
                    $scope.merchantCompanyProfileModel.city = response.city;
                    $scope.merchantCompanyProfileModel.state = response.state;
                    _.each(API.UNITED_STATES, function(state){
                        $scope.state_count++;
                        if(state.abbreviation == $scope.merchantCompanyProfileModel.state){
                            return false;
                        }
                    });
                    $scope.merchantCompanyProfileModel.country = response.country;
                    $scope.merchantCompanyProfileModel.postal_code = response.postal_code;
                    $scope.merchantCompanyProfileModel.dob_day = response.dob_day;
                    $scope.merchantCompanyProfileModel.dob_month = response.dob_month;
                    $scope.merchantCompanyProfileModel.dob_year = response.dob_year;
                    $scope.merchantCompanyProfileModel.personal_id_number_provided = response.personal_id_number_provided;
                    $scope.merchantCompanyProfileModel.ssn_last_4_provided = response.ssn_last_4_provided;
                    $scope.merchantCompanyProfileModel.ssn_last_4 = '****';
                    if (response.verification_document) {
                        $scope.merchantCompanyProfileModel.has_verification_document = true;
                    }

                    if (response.personal_id_number_provided) {
                        $scope.merchantCompanyProfileModel.personal_id_number_provided = true;
                    }
                } else if(response.legal_entity_type == 'individual'){
                    $scope.merchantIndividualProfileModel = response;
                    $scope.type_locked = true;
                    $scope.merchantIndividualForm = true;
                    $scope.merchantCompanyForm = false;
                    $scope.merchantIdentityValid = true;
                    $scope.merchantIndividualProfileModel.last_name = response.last_name;
                    $scope.merchantIndividualProfileModel.first_name = response.first_name;
                    $scope.merchantIndividualProfileModel.address = response.address;
                    $scope.merchantIndividualProfileModel.city = response.city;
                    $scope.merchantIndividualProfileModel.state = response.state;
                    _.each(API.UNITED_STATES, function(state){
                        $scope.state_count++;
                        if(state.abbreviation == $scope.merchantIndividualForm.state){
                            return false;
                        }
                    });
                    $scope.merchantIndividualProfileModel.country = response.country;
                    $scope.merchantIndividualProfileModel.zip_code = response.zip_code;
                    $scope.merchantIndividualProfileModel.dob_day = response.dob_day;
                    $scope.merchantIndividualProfileModel.business_name = "None";
                    $scope.merchantIndividualProfileModel.dob_month = response.dob_month;
                    $scope.merchantIndividualProfileModel.dob_year = response.dob_year;
                    $scope.merchantIndividualProfileModel.personal_id_number_provided = response.personal_id_number_provided;
                    $scope.merchantIndividualProfileModel.ssn_last_4_provided = response.ssn_last_4_provided;
                    $scope.merchantIndividualProfileModel.ssn_last_4 = '****';
                    if (response.verification_document) {
                        $scope.merchantIndividualProfileModel.has_verification_document = true;
                    }

                    if (response.personal_id_number_provided) {
                        $scope.merchantIndividualProfileModel.personal_id_number_provided = true;
                    }
                }
            })
            .catch(function(error){
                $scope.loadingData = false;
                vm.$scope.merchantAgreementStep = $scope.merchantAgreementStep;
                $scope.merchantCompanyFields = presets.merchantCompanyFields();
                $scope.merchantIndividualFields = presets.merchantIndividualFields();
                $scope.merchantCompanyProfileModel = {};
                $scope.merchantIndividualProfileModel = {};
                _.each($scope.merchantCompanyFields, function(field){
                    if(_.isArray(field.code)){
                        _.each(field.code, function(prop){
                            $scope.merchantCompanyProfileModel[prop] = '';
                        });
                    } else{
                        $scope.merchantCompanyProfileModel[field.code] = '';
                    }
                });
                _.each($scope.merchantIndividualFields, function(field){
                    if(_.isArray(field.code)){
                        _.each(field.code, function(prop){
                            $scope.merchantIndividualProfileModel[prop] = '';
                        });
                    } else{
                        $scope.merchantIndividualProfileModel[field.code] = '';
                    }
                });

                $scope.merchantCompanyProfileModel.country = "US";
                $scope.merchantIndividualProfileModel.country = "US";
            });

        ngDialog.open({template: 'app/views/partials/conference/merchant-agreement-popup/merchant_agreement_model.html',
            scope : $scope,
            className: 'app/stylesheets/_merchantAgreementModal.scss'
        });
    }

    return service;
  }

})();
