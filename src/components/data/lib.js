(function (exports)
{
    function stringify(json)
    {
        return $.trim(
            JSON.stringify(
                json,
                undefined,
                4
            )
        );
    }

    //functions for manage cookies

    function getCookie(cname) {
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            console.log(c);
            if(c.indexOf(cname) >= 0){
                console.log('good cookie');
                return true;
            } else{
                console.log('bad cookie');
            }
        }
        return false;
    }

    function setCookie(cname, exdate) {
        var d = new Date(exdate);
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=true;" + expires + ";path=/";
    }



    // Configure and Draw the chart according to the data sent via the values parameter
    // TODO update style to not cut the chart at the last day
    // TODO update style to not have a tick where the y axis value is zero (if possible)
    var vm = this;

    function drawChart(values, type) {
        nv.addGraph(function() {
            vm.chart = nv.models.lineChart()
            .showYAxis(true)
            .interpolate("monotone");

            vm.chart.tooltip.contentGenerator(function (d) {
                var pop_up = '<div'+
                ' style="width: 61px; height:43px; border-radius: 5px; background-color: #fff; opacity:1; color:#26a7de;'+
                'font-family:AvenirNextLTPro-Demi; line-height: 40px;'+
                'font-size:18px; text-align: center;">'+
                d.series[0].value+'</div>';
                return pop_up;
            });

            vm.chart.xAxis
                .tickFormat(function(d){
                    return d3.time.format("%m/%d")(d);
                }).tickPadding(20).tickSize(0,10);

            vm.chart.yAxis.axisLabelDistance(55).tickPadding(15);
            vm.chart.xScale(d3.time.scale());

            vm.chart.forceY([0, 10]);
            var data = dataChart(values);
            d3.select("#registration-chart").selectAll("svg").remove();
            d3.select('#registration-chart').append('svg')
                .datum(data)
                .transition().duration(500)
                .call(vm.chart);

            d3.select('svg .nv-groups > .nv-group')
                .style('fill-opacity', 0.2);
            if(type == 'no_chart'){
                d3.select('svg .nv-linesWrap')
                .style('opacity', 0);
            }

            nv.utils.windowResize(vm.chart.update);

            return vm.chart;
        });
    }

    function updateChart(values){
        var data = dataChart(values);
        d3.select('#registration-chart').datum(data).transition().duration(500).call(vm.chart);
        d3.select('svg .nv-groups > .nv-group').style('fill-opacity', 0.2);
        nv.utils.windowResize(vm.chart.update);
    }

    function timezoneToOffset(stringRepresentation) {
        // +0500
        var parsedOffset = parseInt(stringRepresentation);
        var nrHours = parseInt(parsedOffset / 100);
        var nrMinutes = parsedOffset % 100;
        var totalMinutesOffset = nrHours * 60 + nrMinutes;
        return totalMinutesOffset;
    }

    function offsetToTimezone(offsetMinutes) {
        // -120
        offsetMinutes = -offsetMinutes;

        var nrHours = parseInt(offsetMinutes / 60);
        var nrMinutes = offsetMinutes % 60;
        var offset = nrHours * 100 + nrMinutes;
        if (offset > -1000 && offset < 0) {
            return "-0" +  Math.abs(offset);
        } else if (offset < 1000 && offset > 0) {
            return "+0" + offset;
        }
        return String(offset);
    }

    function checkCreditCard(document) {
        var number = document.getElementById('CCN').value;
        number = number.toString();
        if(_.isEmpty(number) === false){
            if((number.indexOf(4) === 0) && (number.length === 13 || number.length === 16)){
                CCNType = 'visa';
            } else if((number.indexOf(5) === 0) && (number.charAt(1) >= 1 || number.charAt(1) <= 5) && (number.length === 16)){
                CCNType = 'master';
            } else if((number.length === 16) && ((_.inRange(number.substring(0, 8), 60110000,  60119999)) ||
                (_.inRange(number.substring(0, 8), 65000000,  65999999)) ||
                (_.inRange(number.substring(0, 8), 62212600,  62292599)) ) ){
                CCNType = 'discover';
            } else if((number.indexOf(3) === 0) && ((number.indexOf(4) === 1 || number.indexOf(7) === 1)) && (number.length === 15)){
                CCNType = 'american_exp'
            } else if(((number.substring(0,4) == 3088) || (number.substring(0,4) == 3096) || (number.substring(0,4) == 3112) ||
                (number.substring(0,4) == 3158) || (number.substring(0,4) == 3337) || (_.inRange(number.substring(0,8),35280000, 35899999))) &&
                (number.length === 16)){
                CCNType = 'jcb';
            } else if((number.indexOf(3) === 0) && (number.indexOf(0) === 1 || number.indexOf(6) === 1 || number.indexOf(8) === 1) && (number.length === 14)){
                CCNType = 'diners';
            } else {
                CCNType = '';
            }
        } else{
            CCNType = '';
        }

        return CCNType;
    }

    function CSVToArray(strData, strDelimiter) {
        strDelimiter = (strDelimiter || ",");
        var objPattern = new RegExp((
        "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
        "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
        "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
        var arrData = [[]];
        var arrMatches = null;
        while (arrMatches = objPattern.exec(strData)) {
            var strMatchedDelimiter = arrMatches[1];
            if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                arrData.push([]);
            }
            if (arrMatches[2]) {
                var strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"), "\"");
            } else {
                var strMatchedValue = arrMatches[3];
            }
            arrData[arrData.length - 1].push(strMatchedValue);
        }
        for (var i = 0; i < arrData[0].length; i++) {
          arrData[0][i] = arrData[0][i].trim().replace(/[ ]{2,}/i, " ");
        }
        return (arrData);
    }

    // Used in the registration wizard views
    //
    function functionDropSubPanel(type, e, index, element){
        e.stopPropagation()
        console.log(e);
        if(type == 'reg_form_level'){
            if (!document.getElementById('level' + index)) {
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child'){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            }
            else if(document.getElementById('level' + index).style.display == "flex"){
                document.getElementById('level' + index).style.display = "none";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child'){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            } else{
                document.getElementById('level' + index).style.display = "flex";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-down';
                } else if(element == 'child'){
                    e.target.parentElement.children[0].className = 'fa fa-caret-down';
                }
            }
        } else if(type == 'reg_form_fields'){
            if(document.getElementById('fields_review').style.display == "flex"){
                document.getElementById('fields_review').style.display = "none";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            } else{
                document.getElementById('fields_review').style.display = "flex";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-down';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-down';
                }
            }
        } else if(type == 'reg_form_legal'){
            if(document.getElementById('legal_review').style.display == "flex"){
                document.getElementById('legal_review').style.display = "none";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            } else{
                document.getElementById('legal_review').style.display = "flex";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-down';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-down';
                }
            }
        } else if(type == 'level_detail'){
            if(document.getElementById('level_detail' + index).style.display == "flex" ||
                document.getElementById('level_detail' + index).style.display == ""){
                document.getElementById('level_detail' + index).style.display = "none";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            } else{
                document.getElementById('level_detail' + index).style.display = "flex";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-down';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-down';
                }
            }
        }  else if(type == 'level_sessions'){
            if(document.getElementById('level_sessions' + index).style.display == "flex" ||
                document.getElementById('level_sessions' + index).style.display == ""){
                document.getElementById('level_sessions' + index).style.display = "none";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-right';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-right';
                }
            } else{
                document.getElementById('level_sessions' + index).style.display = "flex";
                if(element != 'child' && e.target.children[0]){
                    e.target.children[0].className = 'fa fa-caret-down';
                } else if(element == 'child' && e.target.parentElement.children[0]){
                    e.target.parentElement.children[0].className = 'fa fa-caret-down';
                }
            }
        }
        else if(type == 'level_add_details'){
           if(document.getElementById('level_add_details' + index).style.display == "flex" ||
               document.getElementById('level_add_details' + index).style.display == ""){
               document.getElementById('level_add_details' + index).style.display = "none";
               if(element != 'child' && e.target.children[0]){
                   e.target.children[0].className = 'fa fa-caret-right';
               } else if(element == 'child' && e.target.parentElement.children[0]){
                e.target.parentElement.children[0].className = 'fa fa-caret-right';
               }
           } else{
               document.getElementById('level_add_details' + index).style.display = "flex";
               if(element != 'child' && e.target.children[0]){
                   e.target.children[0].className = 'fa fa-caret-down';
               } else if(element == 'child' && e.target.parentElement.children[0]){
                e.target.parentElement.children[0].className = 'fa fa-caret-down';
               }
           }
       }
       else if(type == 'level_members'){
          if(document.getElementById('level_members' + index).style.display == "flex" ||
              document.getElementById('level_members' + index).style.display == ""){
              document.getElementById('level_members' + index).style.display = "none";
              if(element != 'child' && e.target.children[0]){
                  e.target.children[0].className = 'fa fa-caret-right';
              } else if(element == 'child' && e.target.parentElement.children[0]){
                e.target.parentElement.children[0].className = 'fa fa-caret-right';
              }
          } else{
              document.getElementById('level_members' + index).style.display = "flex";
              if(element != 'child' && e.target.children[0]){
                  e.target.children[0].className = 'fa fa-caret-down';
              } else if(element == 'child' && e.target.parentElement.children[0]){
                e.target.parentElement.children[0].className = 'fa fa-caret-down';
              }
          }
      }
    };

    function daysBetween(date1, date2) {
        var day = 1000 * 60 * 60 * 24
        var date1_ms = date1.getTime()
        var date2_ms = date2.getTime()
        var difference_ms = Math.abs(date1_ms - date2_ms)

        return Math.round(difference_ms/day);
    }

    function dataChart(values) {
        return [
            {
                area: true,
                values:values,
                color: "#26a7de",
                strokeWidth: 1
            }
        ];
    }

    function capitalizeFirstLetter(string){
      return string[0].toUpperCase() + string.slice(1);
    }

    function titleCase(str){
        var i, j, lowers, uppers;

        str = str.replace(
            /\b[\w-\']+/g,
            function (txt)
            {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );

        // Certain minor words should be left lowercase unless
        // they are the first or last words in the string
        lowers = [
            'A',
            'An',
            'The',
            'And',
            'But',
            'Or',
            'For',
            'Nor',
            'As',
            'At',
            'By',
            'For',
            'From',
            'In',
            'Into',
            'Near',
            'Of',
            'On',
            'Onto',
            'To',
            'With',
            'Is'
        ];

        for (i = 0, j = lowers.length; i < j; i++)
        {
            str = str.replace(
                new RegExp(
                        '\\s' + lowers[i] + '\\s',
                        'g'
                ),
                function (txt)
                {
                    return txt.toLowerCase();
                }
            );
        }

        // Certain words such as initialisms or acronyms should be left uppercase
        uppers = [
            'Id',
            'Tv',
            'Ibm',
            'Dvr',
            'Pwc',
            'Usa'
        ];

        for (i = 0, j = uppers.length; i < j; i++)
        {
            str = str.replace(
                new RegExp(
                        '\\b' + uppers[i] + '\\b',
                        'g'
                ),
                uppers[i].toUpperCase()
            );
        }

        return str;
    }

    function truncate(str, len, onWord, appendStr)
    {
        if (str)
        {
            if (!appendStr)
            {
                appendStr = String.fromCharCode(8230);
            }

            appendStr = stripOuterQuotes(appendStr);

            if (str.length > len)
            {
                var trunc = str.substring(
                    0,
                    len - 1
                );

                if (onWord)
                {
                    trunc = trunc.substring(
                        0,
                        trunc.lastIndexOf(' ')
                    );
                }

                return trunc + appendStr;
            }
            else
            {
                return str;
            }

        }
        else
        {
            return '';
        }
    }

    function stripOuterQuotes(str)
    {
        var stripped = str.trim();

        var start = false;
        var end = false;

        if (stripped.charAt(0) == '\"' || stripped.charAt(0) == '\'')
        {
            start = true;
        }

        if (stripped.charAt(stripped.length - 1) == '\"' || stripped.charAt(stripped.length - 1) == '\'')
        {
            end = true;
        }

        if (start && end)
        {
            stripped = stripped.slice(
                1,
                stripped.length - 1
            );
            return stripped;
        }

        return str;
    }

    function strip(str)
    {
        // Stripping the legal HTML tags
        str = str.replace(
            /<[^>]*>/g,
            ''
        );
        // strip the HTML tags
        return _.unescape(str);
    }

    function url(str)
    {
        // Stripping the legal URL
        // crazy pattern: http://jmrware.com/articles/2010/linkifyurl/linkify.html
        var url_pattern = /(\()((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&'()*+,;=:\/?#[\]@%]+)(\))|(\[)((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&'()*+,;=:\/?#[\]@%]+)(\])|(\{)((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&'()*+,;=:\/?#[\]@%]+)(\})|(<|&(?:lt|#60|#x3c);)((?:ht|f)tps?:\/\/[a-z0-9\-._~!$&'()*+,;=:\/?#[\]@%]+)(>|&(?:gt|#62|#x3e);)|((?:^|[^=\s'"\]])\s*['"]?|[^=\s]\s+)(\b(?:ht|f)tps?:\/\/[a-z0-9\-._~!$'()*+,;=:\/?#[\]@%]+(?:(?!&(?:gt|#0*62|#x0*3e);|&(?:amp|apos|quot|#0*3[49]|#x0*2[27]);[.!&',:?;]?(?:[^a-z0-9\-._~!$&'()*+,;=:\/?#[\]@%]|$))&[a-z0-9\-._~!$'()*+,;=:\/?#[\]@%]*)*[a-z0-9\-_~$()*+=\/#[\]@%])/img;

        str = str.replace(
            url_pattern,
            ''
        );
        return str;
    }

    function unixdate(str)
    {
        try
        {
            var date = new Date(parseInt(str) * 1000);
            str = date.toISOString();
        }
        catch (ex)
        {
            console.log(
                'bad date format: %s',
                str
            );
        }

        return str;
    }

    function isodate(str)
    {
        try
        {
            var date = new Date(str);
            str = date.toISOString();
        }
        catch (ex)
        {
            console.log(
                'bad date format: %s',
                str
            );
        }

        return str;
    }

    function split(str, char, start, end)
    {
        var i;
        var newStr = '';

        try
        {
            var pieces = str.split(char);

            if (pieces && pieces.length > 1)
            {
                if (start - 1 < pieces.length && end - 1 < pieces.length && start <= end)
                {
                    for (i = start - 1; i <= end - 1; i++)
                    {
                        if (newStr.length > 0)
                        {
                            newStr += char;
                        }

                        newStr += pieces[i];
                    }

                    str = newStr;
                }
            }
        }
        catch (ex)
        {
            console.log(
                'bad split format: %s',
                str
            );
        }

        return str;
    }

    function image(str)
    {
        var m, urls = [];

        var rex = /<img.*?src="([^">]*\/([^">]*?))".*?>/g;

        while ( m = rex.exec( str ) ) {
            urls.push( m[1] );
        }

        return urls[0];
    }

    function sentence(str)
    {
        var sentencePattern = /(\S.+?[.!?])(?=\s+|$)/;
        str = str.match(sentencePattern);

        if (str && str.length > 0)
        {
            return str[0];
        }

        return '';
    }

    function CSV2JSON(csv, delim) {
        var array = CSVToArray(csv);
        var objArray = [];
        for (var i = 1; i < array.length; i++) {
            objArray[i - 1] = {};
            for (var k = 0; k < array[0].length && k < array[i].length; k++) {
                var key = array[0][k];
                if (array[i][k] === undefined) {
                  objArray[i - 1][key.trim()] = "";
                } else {
                  objArray[i - 1][key.trim()] = array[i][k];
                }
            }
        }
        return objArray;
    }

    function validateCsv(val){
        val = val.split(',');
        for(var i=0;i<val.length;i++)
        {
            if(val[i].length != 1){
                return false;
            }
        }
        return true;
    }

    function sessionsJSON2CSV(sessionMappingFields, sessionMappingFieldsSample) {
        var sessions_values = [];
        _.each(sessionMappingFields, function(field) {
            if(field.code && typeof(field.code) != "number" ) {
                sessions_values.push(field.code);
            } else {
              sessions_values.push(field.name);
            }
        });
        var string = '';
        var csv = _.map(sessions_values, function(title) {
          console.log(title)
          if (title === "title") {
            title = "session_title"
          }
          if (title === 'session_date') {
            title = 'date';
          }
          if (title === 'starts_at') {
            title = 'start_time';
          }
          if (title === 'ends_at') {
            title = "end_time"
          }
          if (title === 'max_capacity') {
            title = "maximum_capacity"
          }
          if (title === 'description') {
            title = "session_description"
          }
          if (title === 'continuing_education_data') {
            title = "continuing_education";
          }
          if (title === 'speaker_facebook') {
            title = "speaker_facebook_url";
          }
          if (title === 'speaker_youtube') {
            title = "speaker_youtube_url";
          }
          if (title === 'speaker_linkedin') {
            title = "speaker_linkedin_url";
          }

          if (title === 'speaker_twitter') {
            title = "speaker_twitter_url";
          }

          if(title.search('_') > -1){
              var words = title.split('_');
              _.each(words, function(word){
                  word = word.replace(/^.*?([a-z])/, word[0].toUpperCase());
                  string = string.concat(word, ' ');
              });
              title = string;
              string = '';
          } else {
              title = title.replace(/^.*?([a-z])/, title[0].toUpperCase());
          }
          return title.trim();
        });

        _.each(sessionMappingFields, function(prop) {
            if(!prop.hasOwnProperty('code')){
                csv.push(prop.name);
            }
        });
        csv += '\r\n';
        _.each(sessionMappingFieldsSample, function(propLine) {
            _.each(propLine, function(prop) {
              csv = csv + '"' + prop + '"';
              csv += ", ";
            });
            csv += '\r\n';
        });
        console.log(csv)
        return csv;
    }

    function titleCaseForKey(str) {
        return str.split(' ').map(function(val){
            return val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
        }).join(' ').split('_').join(' ');
    }

    function getFinalObject(item) {
        var firstField = Object.keys(item)[0];
        var firstFieldFirstError = item[firstField][0];
        if (typeof firstFieldFirstError === 'object') {
            return getFinalObject(firstFieldFirstError);
        } else {
            return item;
        }
    }

    function processValidationError(error, toastr) {
        if(typeof error.data.detail === 'object') {
            var finalObject = getFinalObject(error.data.detail);
            var firstField = Object.keys(finalObject)[0];
            var firstFieldFirstError = finalObject[firstField][0];
            var firstFieldText = titleCaseForKey(firstField);
            var errorMessage = firstFieldText + ": " + firstFieldFirstError;
            toastr.error(errorMessage);
        } else {
            if (error.data.detail.indexOf("is not a u'email'") > -1) {
                toastr.error('Email: Enter a valid email address.');
            } else {
                toastr.error(error.data.detail);
            }

        }
    }

    function validateCreateSession(session, toastr) {
        var validData = true;
        if (!session.title) {
            toastr.error('Session title field is required!');
            validData = false;
        }
        if (session.title.length > 144) {
            toastr.error('Session title field can have maximum 144 characters!');
            validData = false;
        }
        if (!session.starts_at_editable) {
            toastr.error('Session starts at field is required!');
            validData = false;
        }

        if (!session.ends_at_editable) {
            toastr.error('Session ends at field is required!');
            validData = false;
        }

        if (!session.location) {
            toastr.error('Session location is required!');
            validData = false;
        }
        if(session.starts_at_editable >= session.ends_at_editable){
            toastr.error('Set hours - starts at not before ends at!');
            validData = false;
        }
        session.sessionquestion_set.forEach(function(question){
          //console.log(question);
          if(question.text == ""){
            toastr.error('Session feedback question can\'t be empty!');
            validData = false;
          }
        })

        return validData;
    }

    function validateSpeakers(speakers, toastr) {
        var validData = true;
        _.forEach(speakers, function(speaker){
            if (speaker && (speaker.first_name || speaker.last_name)) {
                if (!speaker.first_name) {
                    toastr.error('Speaker first name is required!');
                    validData = false;
                }

                if (!speaker.last_name) {
                    toastr.error('Speaker last name is required!');
                    validData = false;
                }
            }
        });

        return validData;
    }

    function postSaveSessionSessionDayProcessing(data, sessionDays) {
        var day = new Date(data.dateDay).getDate();
        var month = new Date(data.dateDay).getMonth();
        var year = new Date(data.dateDay).getFullYear();
        if (!sessionDays[day + '/' + month + '/' + year]) {
            sessionDays[day + '/' + month + '/' + year] = [];
        }
        sessionDays[day + '/' + month + '/' + year].push(data);
    }

    // function processSpeakers(speakers) {
    //     var validSpeakersData = [];

    //     _.forEach(speakers, function(speaker){
    //         updatedSpeaker = _.omitBy(speaker, _.isNil);
    //         validSpeakersData.push(updatedSpeaker);
    //         updatedSpeaker = {};
    //     });

    //     return validSpeakersData;
    // }

    function JSON2CSV(attendees, selectedAttendees, csvUrl, conference) {
        var attendeesCsv = '';
        _.forEach(attendees, function(attendee) {
            if(attendee.checked === true){
                var mainProfileAttendee = _.omit(attendee,
                ['custom_fields' , 'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                _.forEach(attendee.custom_fields, function(customField){
                    mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                });
                selectedAttendees.push(mainProfileAttendee);
            }
        });
        var json = selectedAttendees;
        var fields = Object.keys(json[0]);
        var csv = json.map(function(row){
          return fields.map(function(fieldName){
            return JSON.stringify(row[fieldName] || '');
          });
        });
        csv.unshift(fields); // add header column

        var csv = csv.join('\r\n');
        selectedAttendees = [];

        var blob = new Blob([csv], {type: "text/csv"});
        var url = URL.createObjectURL(blob);
        var date = new Date();
        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
        csvUrl.href = url;
        csvUrl.download = conference.name + actualDate + ".csv";
    };

    function ConvertToCSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ',';
                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }

    function ConvertToCSV2(objArray) {
        var json = objArray;
        var fields = Object.keys(json[0]);
        var csv = json.map(function(row){
          return fields.map(function(fieldName){
            return JSON.stringify(row[fieldName] || '');
          });
        });
        var string = '';
        fields = _.map(json[0], function(key, title){
            if(title.search('_') > -1){
                var words = title.split('_');
                _.each(words, function(word){
                    word = word.replace(/^.*?([a-z])/, word[0].toUpperCase());
                    string = string.concat(word, ' ');
                });
                title = string;
                string = '';
            } else{
                if (title == '{BCT#QRCode}') {

                } else if (title == 'Conference Name') {

                } else {
                    // title = title.replace(/^.*?([a-zA-Z])/, title[0].toUpperCase());
                    title = '"' + title + '"';
                }
            }
            return title;
        });
        csv.unshift(fields); // add header column
        csv = csv.join('\r\n');
        return csv;
    }

    function ConvertToCSVImproved(objArray, fields) {
        var json = objArray;

        var csv = json.map(function(row){
          return fields.map(function(fieldName){
            return JSON.stringify(row[fieldName] || '');
          });
        });

        var goodFields = [];
        _.each(fields, function(field){
            goodFields.push('"' + titleCase(field) + '"');
        });

        csv.unshift(goodFields); // add header column
        csv = csv.join('\r\n');
        return csv;
    }

    function escapeCSVValue(textItem) {
        return '"' + textItem + '"';
    }

    function ConvertToCSVRaw(objArray) {
        var json = objArray;
        var fields = Object.keys(json[0]);
        var csv = json.map(function(row){
          return fields.map(function(fieldName){
            return JSON.stringify(row[fieldName] || '');
          });
        });
        var string = '';
        fields = _.map(json[0], function(key, title){
            return title;
        });
        csv.unshift(fields); // add header column
        csv = csv.join('\r\n');
        return csv;
    }

    function uploadExcelFile(file, excelJson){
        var extension = file[0]['name'].split('.').pop();
        if(extension === 'xlsx' || extension === 'xls'){
            var reader = new FileReader();
            var name = file[0].name;
            reader.onload = function(e){
                var data = e.target.result;
                var workbook = XLSX.read(data, {type: 'binary'});
                var sheet1 = Object.keys(workbook.Sheets)[0];
                var colNumber = workbook.Sheets[sheet1]['!range'].e.c;
                var rowNumber = workbook.Sheets[sheet1]['!range'].e.r;
                var colrange = workbook.Sheets[sheet1]['!ref'].split(':');
                var firstLetter = colrange[0][0];
                var i, j, k;
                var workbookJson = [];
                var workbookArr = [];
                var workbookObj = [];
                var infoObj = {};
                var iterator = 0;
                for(i = 0; i < colNumber; i++){
                    if(colrange[0].charCodeAt(0) <= colrange[1].charCodeAt(0) && workbook.Sheets[sheet1][colrange[0][0] + 1] !== undefined){
                       var prop = workbook.Sheets[sheet1][colrange[0][0] + 1].v;
                       var ascii = colrange[0].charCodeAt(0) + 1;
                       colrange[0] = String.fromCharCode(ascii) + 1;
                       workbookArr[iterator] = prop;
                       iterator++;
                    }
                }
                var nr = 0;
                var numberOfRows = 1;
                iterator = 2;
                colrange[0] = firstLetter + iterator;
                    while(numberOfRows <= rowNumber){
                        for(j = 1; j <= colNumber; j++){
                            if(colrange[0].charCodeAt(0) <= colrange[1].charCodeAt(0) && workbook.Sheets[sheet1][colrange[0][0] + 1] !== undefined
                                && iterator <= rowNumber){
                                var info = workbook.Sheets[sheet1][colrange[0]].v;
                                var ascii = colrange[0].charCodeAt(0) + 1;
                                colrange[0] = String.fromCharCode(ascii) + iterator;
                                workbookObj[nr] = info;
                                nr++;
                            }
                        }
                        for(i = 0; i < workbookArr.length; i++){
                            infoObj[workbookArr[i]] = workbookObj[i];
                        }
                        workbookJson.push(infoObj);
                        workbookObj = [];
                        infoObj = {};
                        iterator++;
                        nr = 0;
                        colrange[0] = firstLetter + iterator;
                        numberOfRows++;
                    }
                workbookJson.pop();
                excelJson = workbookJson;
            }
            reader.readAsBinaryString(file[0]);
        } else if(extension === 'csv'){
            vm.uploadFile(file);
        }
    }

    function initSessionDay(data, sessionDays) {
        var day = new Date(data.starts_at).getDate();
        var month = new Date(data.starts_at).getMonth();
        var year = new Date(data.starts_at).getFullYear();
        if (!sessionDays[day + '/' + month + '/' + year]) {
            sessionDays[day + '/' + month + '/' + year] = [];
        }
        return sessionDays;
    }

    function initSessionDayAndReturn(data, sessionDays, data) {
        var day = new Date(data.starts_at).getDate();
        var month = new Date(data.starts_at).getMonth();
        var year = new Date(data.starts_at).getFullYear();
        if (!sessionDays[day + '/' + month + '/' + year]) {
            sessionDays[day + '/' + month + '/' + year] = [];
        }

        var existingSession2 = _.find(sessionDays[day + '/' + month + '/' + year], {'id':data.id});
        return existingSession2;
    }

    function format_date_for_validation(date) {
        if (!date) return format_date_for_validation(new Date());
        var dd = date.getDate();
        var mm = date.getMonth()+1; //January is 0!
        var yyyy = date.getFullYear();

        if(dd<10) {
            dd = '0'+dd;
        }

        if(mm<10) {
            mm = '0'+mm;
        }
        var today = yyyy+'-'+mm+'-'+dd;
        return today;
    }

    function printTransactionsAsCsv(transactions, promo_codes) {
        var checkedAttendeeList = [];
        _.each(transactions, function(transaction) {
            if (transaction.checked == true) {
                var mainProfileAttendee = _.omit(transaction,
                ['created_at', 'deleted_at', 'updated_at', 'checked', 'payment', '$$hashKey']);
                checkedAttendeeList.push(mainProfileAttendee);
            }
        });

        if (transactions === promo_codes) {
            _.each(checkedAttendeeList, function(object){
                if (object.exhibitor) object.exhibitor = object.exhibitor.name;
                else object.exhibitor = 'No Name';
            });
        }

        var thData = "<tr>";
        var keys = Object.keys(checkedAttendeeList[0]);
        _.each(keys, function(key){
            thData += "<th>" + key + "</th>";
        });

        thData += "</tr>";
        _.each(checkedAttendeeList, function(exhibitor){
            var tr = "<tr>";
            _.each(keys, function(key){
                tr += "<td>" + exhibitor[key] + "</td>";
            });
            tr += "</tr>";
            thData += tr;
        });

        var popupWin = window.open('', '_blank', 'width=1000px,height=500px');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">'+
        '<div style="width: 0;">' +
        '<table>'
        + thData +'</table>' +
        '</div></body></html>');
        popupWin.document.close();
    }

    function downloadTransactionsAsCsv(transactions, conference) {
        var checkedAttendeeList = [];
        _.each(transactions, function(transaction) {
            if (transaction.checked == true) {
                var mainProfileAttendee = _.omit(transaction,
                ['created_at', 'deleted_at', 'updated_at', '$$hashKey', 'checked']);
                checkedAttendeeList.push(mainProfileAttendee);
            }
        });

        var csvData =  lib.ConvertToCSV2(checkedAttendeeList);
        var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document

        var date = new Date();
        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
        var filename = conference.name + actualDate + ".csv";

        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
            target: '_blank',
            download: filename
        })[0].click();

        anchor.remove();
    }

    function printCsvFile(csvData, filename) {
        var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document

        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
            target: '_blank',
            download: filename
        })[0].click();

        anchor.remove();
    }

    function downloadAdvancedTransactionsAsCsv(transactions, conference) {
        var checkedAttendeeList = [];
        _.each(transactions, function(transaction) {
            var mainProfileAttendee = _.omit(transaction,
            ['created_at', 'deleted_at', 'updated_at', '$$hashKey']);
            checkedAttendeeList.push(mainProfileAttendee);
        });

        var csvData =  ConvertToCSVRaw(checkedAttendeeList);
        var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document

        var date = new Date();
        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
        var filename = conference.name + actualDate + ".csv";

        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
            target: '_blank',
            download: filename
        })[0].click();

        anchor.remove();
    }

    function formatTimeZoneApi(defaultTimezones){
        _.each(defaultTimezones, function(tmz){
                var new_tmz = '', decimals = '';
                if(tmz.offset < 0){
                    if(tmz.offset <= -10){
                        if(tmz.offset.toString().indexOf(".") < 0){
                            new_tmz = tmz.offset.toString() + '00';
                        } else{
                            decimals = tmz.offset.toString().split('.')[1].length == 2 ? tmz.offset.toString().split('.')[1] : tmz.offset.toString().split('.')[1] + '0';
                            new_tmz = tmz.offset.toString() + decimals;
                        }
                    } else{
                        var minus = tmz.offset.toString()[0];
                        if(tmz.offset.toString().indexOf(".") < 0){
                            new_tmz = minus + "0" + tmz.offset.toString()[1] + '00';
                        } else{
                            decimals = tmz.offset.toString().split('.')[1].length == 2 ? tmz.offset.toString().split('.')[1] : tmz.offset.toString().split('.')[1] + '0';
                            new_tmz = minus + "0" + tmz.offset.toString()[1] + decimals;
                        }
                    }
                } else if(tmz.offset > 0){
                    if(tmz.offset >= 10){
                        if(tmz.offset.toString().indexOf(".") < 0){
                            new_tmz = tmz.offset.toString() + '00';
                        } else{
                            decimals = tmz.offset.toString().split('.')[1].length == 2 ? tmz.offset.toString().split('.')[1] : tmz.offset.toString().split('.')[1] + '0'
                            new_tmz = tmz.offset.toString() + decimals;
                        }
                    } else{
                        if(tmz.offset.toString().indexOf(".") < 0){
                            new_tmz = "0" + tmz.offset.toString()[0] + '00';
                        } else{
                            decimals = tmz.offset.toString().split('.')[1].length == 2 ? tmz.offset.toString().split('.')[1] : tmz.offset.toString().split('.')[1] + '0';
                            new_tmz = "0" + tmz.offset.toString()[0] + decimals;
                        }
                    }
                } else{
                    new_tmz = '0000';
                }
                tmz.offset = new_tmz;
            });
    }

    function squareifyImage($q, files) {
        var deferred = $q.defer();

        var canvas = document.createElement('canvas');
        canvas.id = "ConferenceNewLogo";
        canvas.width = 1000;
        canvas.height = 1000;

        var context = canvas.getContext("2d");

        var img = new Image;
        img.onload = function() {
            var width = img.width;
            var height = img.height;

            var finalWidth = 1000;
            var finalHeight = 1000;
            var xOffset = 0;
            var yOffset = 0;
            if (width > height) {
                finalHeight = 1000 * (height / width);
                yOffset = (1000 - finalHeight) / 2;
            } else if (height > width) {
                finalWidth = 1000 * (width / height);
                xOffset = (1000 - finalWidth) / 2;
            }

            context.drawImage(img, xOffset, yOffset, finalWidth, finalHeight);
            var pngUrl = canvas.toDataURL();
            var myBlob = lib.dataURItoBlob(pngUrl);
            var file = new File([myBlob], files[0].name, {type: 'image/png'});

            deferred.resolve({
                "img": pngUrl,
                "file": file
            });
        }
        img.src = files[0].$ngfBlobUrl;
        return deferred.promise;
    }

    function overallConferenceRevenueGraph(data, type){
        nv.addGraph(function() {
            var chart = nv.models.pieChart()
                .x(function(d) { return d.label })
                .y(function(d) { return parseInt(d.value) })
                .showLabels(true)
                .width(410)
                .height(410)
                .margin({left: 120, top: -75, bottom: 100})
                .labelThreshold(.01)
                .showLegend(false)
                .showLabels(true)
                .labelsOutside(true);

            var svg = d3.select('#super-admin-overall-revenue-graph').append('svg');

            svg.datum(data).call(chart);

            nv.utils.windowResize(chart.update);

            return chart;
        });
    }

    exports.overallConferenceRevenueGraph = function(data, type){
        return overallConferenceRevenueGraph(data, type);
    };

    exports.titleCaseForKey = function(item) {
        return titleCaseForKey(item);
    }

    exports.processValidationError = function(error, toastr){
        return processValidationError(error, toastr);
    };

    exports.capitalizeFirstLetter = function(string){
      return capitalizeFirstLetter(string);
    };

    function generateGoogleMapsDetailed(key, country_name, city, street) {
        return 'https://maps.googleapis.com/maps/api/staticmap?key='+key+
        '&size=700x256&sensor=false&zoom=15&markers=color%3Ared%7Clabel%3A.%7C'+country_name
        +'%20'+city+'%20'+street;
    }

    function generateGoogleMaps(country, city, street) {
        return 'https://www.google.ro/maps/place/' + country + ',' + city  + ',' + street;
    }

    function dataURItoBlob(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: 'image/png'});
    }

    exports.getCookie = function(cname){
      return getCookie(cname);
    };

    exports.setCookie = function(cname, exdate){
        return setCookie(cname, exdate);
    };

    exports.squareifyImage = function(files, logo, file){
        return squareifyImage(files, logo, file);
    }

    exports.dataURItoBlob = function(dataURI){
        return dataURItoBlob(dataURI);
    }

    exports.formatTimeZoneApi = function(defaultTimezones){
        return formatTimeZoneApi(defaultTimezones);
    }

    exports.ConvertToCSVRaw = function(transfers){
        return ConvertToCSVRaw(transfers);
    }

    exports.escapeCSVValue = function(textItem){
        return escapeCSVValue(textItem);
    }

    exports.generateGoogleMaps = function( country_name, city, street)
    {
        return generateGoogleMaps(country_name, city, street);
    };

    exports.generateGoogleMapsDetailed = function(key, country_name, city, street)
    {
        return generateGoogleMapsDetailed(key, country_name, city, street);
    };

    exports.JSON2CSV = function(attendees, selectedAttendees, csvUrl, conference)
    {
        return JSON2CSV(attendees, selectedAttendees, csvUrl, conference);
    };

    exports.drawChart = function (values, type)
    {
        return drawChart(values, type);
    };

    exports.format_date_for_validation = function (data)
    {
        return format_date_for_validation(data);
    };

    exports.printTransactionsAsCsv = function (transactions, promo_codes)
    {
        return printTransactionsAsCsv(transactions, promo_codes);
    };

    exports.downloadTransactionsAsCsv = function (transactions, conference)
    {
        return downloadTransactionsAsCsv(transactions, conference);
    };

    exports.downloadAdvancedTransactionsAsCsv = function (transactions, conference)
    {
        return downloadAdvancedTransactionsAsCsv(transactions, conference);
    };

    exports.initSessionDay = function (data, sessionDays)
    {
        return initSessionDay(data, sessionDays);
    };

    exports.initSessionDayAndReturn = function (data, sessionDays, data)
    {
        return initSessionDayAndReturn(data, sessionDays, data);
    };

    exports.validateCreateSession = function (session, toastr)
    {
        return validateCreateSession(session, toastr);
    };

    exports.daysBetween = function(date1, date2)
    {
        return daysBetween(date1, date2);
    };

    exports.updateChart = function(values)
    {
        return updateChart(values);
    };

    exports.postSaveSessionSessionDayProcessing = function (data, sessionDays)
    {
        return postSaveSessionSessionDayProcessing(data, sessionDays);
    };

    exports.validateSpeakers = function (speakers, toastr)
    {
        return validateSpeakers(speakers, toastr);
    };

    // exports.processSpeakers = function (speakers)
    // {
    //     return processSpeakers(speakers);
    // };

    exports.ConvertToCSV = function (objArray)
    {
        return ConvertToCSV(objArray);
    };

    exports.ConvertToCSVImproved = function (objArray, fieldArray)
    {
        return ConvertToCSVImproved(objArray, fieldArray);
    };

    exports.printCsvFile = function (csvData, fileName)
    {
        return printCsvFile(csvData, fileName);
    };

    exports.ConvertToCSV2 = function (objArray)
    {
        return ConvertToCSV2(objArray);
    };

    exports.sessionsJSON2CSV = function (sessionMappingFields, sessionMappingFieldsSample)
    {
        return sessionsJSON2CSV(sessionMappingFields, sessionMappingFieldsSample);
    };

    exports.CSVToArray = function (strData, strDelimiter)
    {
        return CSVToArray(strData, strDelimiter);
    };

    exports.checkCreditCard = function (document)
    {
        return checkCreditCard(document);
    };

    exports.validateCsv = function (val)
    {
        return validateCsv(val);
    };

    exports.CSV2JSON = function (csv, delim)
    {
        return CSV2JSON(csv, delim);
    };

    exports.functionDropSubPanel = function (type, e, index, element)
    {
        return functionDropSubPanel(type, e, index, element);
    };

    exports.stringify = function (json)
    {
        return stringify(json);
    };

    exports.title = function (str)
    {
        return titleCase(str);
    };

    exports.timezoneToOffset = function (str)
    {
        return timezoneToOffset(str);
    };

    exports.offsetToTimezone = function (minutes)
    {
        return offsetToTimezone(minutes);
    };

    exports.clip = function (str, len, appendStr)
    {
        return truncate(
            str,
            len,
            appendStr
        );
    };

    exports.truncate = function (str, len, appendStr)
    {
        return truncate(
            str,
            len,
            true,
            appendStr
        );
    };

    exports.strip = function (str)
    {
        return strip(str);
    };

    exports.url = function (str)
    {
        return url(str);
    };

    exports.image = function (str)
    {
        return image(str);
    };
})(typeof exports === "undefined" ? (window.lib = {}) : exports);
