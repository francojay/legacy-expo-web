(function (exports)
{
    function setStandardAttendeeFields() {
        return [
            {label:'First Name', type:"required", name: 'first_name', type_code: 'Required'},
            {label:'Last Name', type:"required", name: 'last_name', type_code: 'Required'},
            {label:'Email', type:"required", name: 'email_address', type_code: 'Required'},
            {label:"Company", type:"optional", name: 'company_name' ,type_code:"Optional"},
            {label:"Job Title", type:"optional", name: 'job_title' ,type_code:"Optional"},
            {label:"Phone", type:"optional", name: 'phone_number' ,type_code:"Optional"},
            {label:"Street", type:"optional", name: 'street_address' ,type_code:"Optional"},
            {label:"City", type:"optional", name: 'city' ,type_code:"Optional"},
            {label:"Zip", type:"optional", name: 'zip_code' ,type_code:"Optional"},
            {label:"State", type:"optional", name: 'state' ,type_code:"Optional"},
            {label:"Country", type:"optional", name: 'country' ,type_code:"Optional"}
        ];
    }

    function sessionSpeakerFields(){
        return [
            {label:'First Name', code: 'first_name'},
            {label:'Last Name', code: 'last_name'},
            {label:'Company', code: 'company'},
            {label:'Job Title', code: 'job_title'},
            {label:"Email Address", code : 'email'},
            {label:"Phone Number", code : 'phone_number'},
            {label:"LinkedIn url", code : 'linkedin_url'},
            {label:"Facebook url", code : 'facebook_url'},
            {label:"Twitter url", code : 'twitter_url'},
            {label:"Youtube url", code : 'youtube_url'},
            {label:"Biography", code : 'biography'}
        ];
    };

    function dayMonths() {
        return {
          "1": 31,
          "2": 28,
          "3": 31,
          "4": 30,
          "5": 31,
          "6": 30,
          "7": 31,
          "8": 31,
          "9": 30,
          "10": 31,
          "11": 30,
          "12": 31
        }
    }

    function merchantCompanyFields(){
        return [
            {label:'Company Name', code: 'business_name'},
            {label:'Address', code: 'line1'},
            {label:'City', code: 'city'},
            {label:"State", code : 'state'},
            {label:"Country", code : 'country'},
            {label:"Zip Code", code : 'postal_code'},
            {label:"Business Tax ID", code : 'business_tax_id'},
            {label:"Legal Entity Type", code : 'legal_entity_type'},
            {label:"Your Last Name", code : 'last_name'},
            {label:"Your First Name", code : 'first_name'},
            {label:"Your Date of Birth", code : ['dob_day', 'dob_month', 'dob_year']},
            {label:"SSN Last 4 Digits", code : 'ssn_last_4'}
        ];
    }

    function merchantIndividualFields(){
        return [
            {label:"Last Name", code : 'last_name'},
            {label:"First Name", code : 'first_name'},
            {label:"Legal Entity Type", code : 'legal_entity_type'},
            {label:'Address', code: 'line1'},
            {label:'City', code: 'city'},
            {label:"State", code : 'state'},
            {label:"Country", code : 'country'},
            {label:"Zip Code", code : 'postal_code'},
            {label:"Date of Birth", code : ['dob_day', 'dob_month', 'dob_year']},
            // {label:"SSN Last 4 Digits", code : 'ssn_last_4'},
            {label:"Social Security Number", code : 'personal_id_number'}
        ];
    }

    function newTravelType() {
        return {
            'name': '',
            'city': '',
            'state': '',
            'street_address': '',
            'website': '',
            'special_instructions': '',
            'logo': '',
            'image': '',
            'country': '',
            'group_code': '',
            'phone_number': '',
            'price_range_start': '',
            'price_range_end': '',
            'zip_code': ''
        };
    }

    function defaultHotels(){
        return[
            'Aloft Hotels',
            'Clarion Inn',
            'Comfort INN',
            'DoubleTree By Hilton',
            'Element Hotels',
            'Four Seasons Hotels',
            'Hilton Hotels',
            'Holiday Inn Hotels',
            'Hyatt Hotels',
            'Hyatt Place',
            'Marriott Hotels',
            'Quality Inn',
            'Radisson Hotels',
            'Sheraton Hotels',
            'Sleep Inn',
            'Westin Hotels & Resorts',
            'W Hotels'
        ];
    };

    function defaultCarRentals(){
        return[
            'Budget',
            'Thrifty',
            'Hertz',
            'National',
            'AVIS',
            'Payless',
            'Enterprise',
            'Alamo'
        ];
    };

    function defaultAirlines(){
        return[
            'United Airlines',
            'American Airlines',
            'Southwest Airlines',
            'Virgin America',
            'Virgin Altantic',
            'jetBlue',
            'Delta',
            'Frontier',
            'Alaska'
        ];
    };

    function defaultTransportations(){
        return[
            'Uber',
            'Lyft'
        ];
    };

    function initAttendeeFields() {
        return [
             {
                id: -1,
                code: 'full_name',
                name: 'Attendee'
            },
            {
                id: -2,
                code: 'email_address',
                name: 'Email'
            },
            {
                id: -3,
                code: 'phone_number',
                name: 'Phone'
            },
            {
                id: -4,
                code: 'company_name',
                name: 'Company'
            },
            {
                id: -5,
                code: 'qr_code',
                name: 'QR Code'
            }
        ];
    }

    function registrantFields() {
        return [
            {
                code: 'full_name',
                title: 'Registrant'
            },
            {
                code: 'email',
                title: 'Email'
            },
            {
                code: 'phone',
                title: 'Phone'
            },
            {
                code: 'company',
                title: 'Company'
            },
            {
                code: 'qr_code',
                title: 'QR Code'
            }
        ];
    }

    function removableAttendeeFields() {
        return [
            {
                id: -10,
                code: 'attendee_id',
                name: 'Attendee Id'
            },
            {
                id: -2,
                code: 'email_address',
                name: 'Email'
            },
            {
                id: -3,
                code: 'phone_number',
                name: 'Phone'
            },
            {
                id: -4,
                code: 'company_name',
                name: 'Company'
            },
            {
                id: -5,
                code: 'qr_code',
                name: 'QR Code'
            },
            {
                id: -21,
                code: 'street_address',
                name: 'Street Address'
            },
            {
                id: -22,
                code: 'city',
                name: 'City'
            },
            {
                id: -23,
                code: 'state',
                name: 'State'
            },
            {
                id: -24,
                code: 'country',
                name: 'Country'
            },
            {
                id: -25,
                code: 'zip_code',
                name: 'Zip Code'
            },
            {
                id: -26,
                code: 'guest_of',
                name: 'Guest of'
            }
        ]
    }

    function editAttendeeFields(){
        return [
            {
                id: -6,
                code: 'first_name',
                name: 'First Name'
            },
            {
                id: -7,
                code: 'last_name',
                name: 'Last Name'
            },
            {
                id: -8,
                code: 'job_title',
                name: 'Job Title'
            }
        ];
    }

    function smallFormForTravel() {
        return [
            {
                required: true,
                code: 'name',
                label: 'Name'
            },
            {
                required: false,
                code: 'logo',
                label: 'Logo'
            },
            {
                required: false,
                code: 'discount',
                label: 'Discount '
            },
            {
                required: false,
                code: 'group_code',
                label: 'Group Code'
            },
            {
                required: false,
                code: 'website',
                label: 'Website'
            }
        ];
    }

    function largeFormForTravel() {
         return [
            {
                required: true,
                code: 'name',
                label: 'Name'
            },
            {
                required: false,
                code: 'logo',
                label: 'Logo'
            },
            {
                required: false,
                code: 'group_code',
                label: 'Group Code'
            },
            {
                required: true,
                code: 'phone_number',
                label: 'Phone Number'
            },
            {
                required: true,
                code: 'street_address',
                label: 'Street Address'
            },
            {
                required: true,
                code: 'city',
                label: 'City'
            },

            {
                required: true,
                code: 'zip_code',
                label: 'Zip Code'
            },
            {
                required: true,
                code: 'state',
                label: 'State'
            },
            {
                required: true,
                code: 'country',
                label: 'Country'
            },
            {
                required: false,
                code: 'website',
                label: 'Website'
            },
            {
                required: true,
                label: 'Price Range Per Day'
            }
        ];
    }

    function memberMappingFields() {
        return [
            {
                id: -8,
                code: 'email',
                name: 'Email'
            },
            {
                id: -6,
                code: 'first_name',
                name: 'First Name'
            },
            {
                id: -7,
                code: 'last_name',
                name: 'Last Name'
            }
        ];
    }

    function exhibitorMappingFields(){
        return [
            {
                id: -1,
                code: 'name',
                name: 'Company Name'
            },
            {
                id: -2,
                code: 'booth',
                name: 'Booth Number'
            },
            {
                id: -3,
                code: 'booth_first_name',
                name: 'Booth Contact First Name'
            },
            {
                id: -4,
                code: 'booth_last_name',
                name: 'Booth Contact Last Name'
            },
            {
                id: -5,
                code: 'booth_contact_email',
                name: 'Booth Contact Email'
            }
        ];
    };

    function attendeeMappingFields() {
        return [
            {
                id: -8,
                code: 'job_title',
                name: 'Job Title'
            },
            {
                id: -6,
                code: 'first_name',
                name: 'First Name'
            },
            {
                id: -7,
                code: 'last_name',
                name: 'Last Name'
            }
        ];
    }

    function attendeeMappingFields2() {
        return [
            {
                id: -6,
                code: 'first_name',
                name: 'First Name'
            },
            {
                id: -7,
                code: 'last_name',
                name: 'Last Name'
            },
            {
                id: -8,
                code: 'job_title',
                name: 'Job Title'
            }
        ];
    }

    function tempAttendeeFields() {
        return [
            {
                id: -1,
                code: 'full_name',
                name: 'Attendee'
            },
            {
                id: -2,
                code: 'email_address',
                name: 'Email'
            },
            {
                id: -3,
                code: 'phone_number',
                name: 'Phone'
            },
            {
                id: -4,
                code: 'company_name',
                name: 'Company'
            },
            {
                id: -5,
                code: 'qr_code',
                name: 'QR Code'
            }
        ];
    }

    function sessionInputFields() {
        return [
            {
                id: -1,
                code: 'title',
                name: 'Session Title'
            },
            {
                id: -2,
                code: 'session_date',
                name: 'Date'
            },
            {
                id: -3,
                code: 'starts_at',
                name: 'Start Time'
            },
            {
                id: -4,
                code: 'ends_at',
                name: 'End Time'
            },
            {
                id: -5,
                code: 'location',
                name: 'Location'
            },
            {
                id: -6,
                code: 'session_type',
                name: 'Session Type'
            },
            {
                id: -7,
                code: 'maximum_capacity',
                name: 'Maximum Capacity'
            },
            {
                id: -8,
                code: 'description',
                name: 'Session Description'
            },
            {
                id: -9,
                code: 'speaker_first_name',
                name: "Speaker's First"
            },
            {
                id: -10,
                code: 'speaker_last_name',
                name: "Speaker's Last"
            },
            {
                id: -11,
                code: 'speaker_company',
                name: "Speaker's Company"
            },
            {
                id: -12,
                code: 'speaker_email',
                name: "Speaker's Email"
            },
            {
                id: -13,
                code: 'speaker_phone_number',
                name: "Speaker's Phone Number"
            },
            {
                id: -14,
                code: 'speaker_linkedin',
                name: "Speaker's Linkedin URL"
            },
            {
                id: -15,
                code: 'speaker_twitter',
                name: "Speaker's Twitter URL"
            },
            {
                id: -16,
                code: 'speaker_facebook',
                name: "Speaker's Facebook URL"
            },
            {
                id: -17,
                code: 'speaker_youtube',
                name: "Speaker's Youtube URL"
            },
            {
                id: -18,
                code: 'speaker_biography',
                name: "Speaker's Biography"
            },
            {
                id: -19,
                code: 'continuing_education_data',
                name: "Continuing Education"
            },
            {
                id: -20,
                code: 'ce_hours',
                name: "CE Hours"
            }
        ];
    }

    function sessionInputFieldsSampleData(conference_start_date) {
        return [
                  [
                    "Breakfast",
                    "5/7/2019",
                    "8:00 AM",
                    "9:00 AM",
                    "Cafe - Room",
                    "Breakfast",
                    100,
                    "1. Mandatory fields must be populated (Title, Sessions Date, Starts At, Ends At, Location).  All other fields are optional. 2. You can enter up to 2000 characters, approximately 300 words including spaces and special characters for sessions descriptions and Speakers biography.  3. You can add multiple speakers to one session by inserting addtional lines.  3. Acceptable DATE/TIME formats: M-DD-YYYY, MM/DD/YY, YYYY-MM-DD, YYYY/MM/DD and H:MM AM/PM.",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 1",
                    "5/7/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    10,
                    "1st Speaker",
                    "Lisa",
                    "Jones",
                    "Swimmers Inc",
                    "LisaTest@yahoo.com",
                    "(312) 777-7777",
                    "linkedin.com/lisasample",
                    "www.twitter.com/lisa",
                    "www.facebook.com/lisa",
                    "www.youtube.com/lisafake",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "Course 289",
                    1
                  ],
                  [
                    "Analytics Part 1",
                    "5/7/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    10,
                    "2nd Speaker",
                    "Mark-Lee",
                    "S.",
                    "Runners Inc",
                    "Marktest@yahoo.com",
                    "800.789-1234",
                    "www.linkedin/mark",
                    "www.twitter.com/mark",
                    "",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 1",
                    "5/7/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    10,
                    "3rd Speaker",
                    "Andrew",
                    "Holland",
                    "Ford Inc.",
                    "Andrew.Hollandtest@yahoo.com",
                    "800.789-1234",
                    "linked.com/andrewtest",
                    "www.twitter.com/andrew",
                    "",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 1",
                    "5/7/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    10,
                    "4th Speaker",
                    "Lisa A",
                    "Jones",
                    "Nissan",
                    "lisaa@yahoo.com",
                    "800.789-1234",
                    "linkedin.com/mike",
                    "www.twitter.com/mike",
                    "",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 1",
                    "5/7/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    10,
                    "5th Speaker",
                    "Jeeremy",
                    "Smith",
                    "Biking, Inc",
                    "Jeremy_Smithtest@yahoo.com",
                    "800.123-7890",
                    "linkedin.com/jeremySmith",
                    "www.twitter.com/jeremy",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "Lunch",
                    "5/7/2019",
                    "12:30 PM",
                    "1:30 PM",
                    "Cafe - Room",
                    "Lunch",
                    80,
                    "Lunch provided in the cafeteria.",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "Business Development",
                    "5/7/2019",
                    "1:45 PM",
                    "3:00 PM",
                    "Room B",
                    "Individual Presentation",
                    80,
                    "Business development discussions.",
                    "Lisa",
                    "Jones",
                    "Speakers / Networking. Inc.",
                    "LisaTest@yahoo.com",
                    "312-777-0000",
                    "linkedin.com/lisa",
                    "www.twitter.com/lisa",
                    "www.facebook.com/lisa",
                    "www.youtube.com/lisafake",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Sales 101",
                    "5/7/2019",
                    "3:45 PM",
                    "4:00 PM",
                    "Room C",
                    "Individual Presentation",
                    80,
                    "Sales presentation.",
                    "Jeff",
                    "B",
                    "Speakers Networking Inc",
                    "",
                    "(312) 777-0000",
                    "linkedin.com/jeffsample",
                    "twitter.com/jeff",
                    "www.facebook.com/jeff",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "Course-BUS 245",
                    0.2
                  ],
                  [
                    "Breakfast",
                    "5/8/2019",
                    "8:00 AM",
                    "9:00 AM",
                    "Cafe - Room",
                    "Breakfast",
                    45,
                    "Lunch provided in the cafeteria.",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 2",
                    "5/8/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    45,
                    "1st Speaker",
                    "Lisa",
                    "Jones",
                    "Swimmers Inc",
                    "LisaTest@yahoo.com",
                    "(312) 777-7777",
                    "linkedin.com/lisasample",
                    "www.twitter.com/lisafake",
                    "www.facebook.com/lisafake",
                    "www.youtube.com/lisafake",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 2",
                    "5/8/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    45,
                    "2nd Speaker",
                    "Mark Lee",
                    "Smith",
                    "Runners Inc",
                    "Marktest@yahoo.com",
                    "(800) 789-1234",
                    "www.linkedin/marksample",
                    "www.twitter.com/mark",
                    "",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Analytics Part 2",
                    "5/8/2019",
                    "10:15 AM",
                    "11:00 AM",
                    "Room A",
                    "Workshop",
                    45,
                    "3rd Speaker",
                    "Andrew",
                    "Payne",
                    "Expo Inc",
                    "Andrew.payne@yahoo.com",
                    "(800) 789-1234",
                    "linked.com/andrewtest",
                    "www.twitter.com/andrew",
                    "",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Lunch",
                    "5/8/2019",
                    "12:30 PM",
                    "1:30 PM",
                    "Cafe - Room",
                    "Lunch",
                    45,
                    "Lunch provided in the cafeteria.",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                  ],
                  [
                    "Data Collection",
                    "5/8/2019",
                    "1:45 PM",
                    "3:00 PM",
                    "Room B",
                    "Individual Presentation",
                    45,
                    "Open discussions.",
                    "Lisa",
                    "Jones",
                    "Speakers Networking Inc",
                    "Lisatest@yahoo.com",
                    "(312) 777-0000",
                    "linkedin.com/lisasample",
                    "www.twitter.com/lisa",
                    "www.facebook.com/lisa",
                    "www.youtube.com/lisafake",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Project Management",
                    "5/8/2019",
                    "3:45 PM",
                    "4:00 PM",
                    "Room C",
                    "Workshop",
                    45,
                    "Develop and manage your projects.",
                    "Lisa",
                    "Jones",
                    "Speakers Networking Inc",
                    "Lisatest@yahoo.com",
                    "(312) 777-0000",
                    "linkedin.com/lisasample",
                    "www.twitter.com/lisa",
                    "www.facebook.com/lisa",
                    "www.youtube.com/lisafake",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Dinner/Gala",
                    "5/8/2019",
                    "6:00 PM",
                    "11:55 PM",
                    "Lounge",
                    "Gala",
                    45,
                    "Gala continued",
                    "Jeff",
                    "B",
                    "Speakers Networking Inc",
                    "Jefftest@gmail.com",
                    "312.444.5656",
                    "linkedin.com/jeffsample",
                    "twitter.com/jeff",
                    "www.facebook.com/jeff",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ],
                  [
                    "Dinner/Gala",
                    "5/9/2019",
                    "12:00 AM",
                    "2:00 AM",
                    "Lounge",
                    "Gala",
                    45,
                    "Gala continued",
                    "Jeff",
                    "B",
                    "Speakers Networking Inc",
                    "Jefftest@gmail.com",
                    "312.444.5656",
                    "linkedin.com/jeffsample",
                    "twitter.com/jeff",
                    "www.facebook.com/jeff",
                    "",
                    "This field is optional but you can enter up to 2000 characters, approximately 300 words including spaces and special characters.",
                    "",
                    ""
                  ]
                ]
    };

    function sessionTypeSimple() {
        return [
            "Individual Presentation",
            "Interactive Roundtable Discussion",
            "Panel Discussion",
            "Workshop",
            "Pre-Conference Workshop",
            "Breakout",
            "Networking",
            "Reception",
            "Breakfast",
            "Lunch",
            "Dinner",
            "Gala",
            "Keynote"
        ];
    }

    function monthStringList() {
        return [
            {
                "label": 1,
                "id": 1
            }
        ]
    }

    function sessionQuestionOptionType(){
        return [
            {'name' : 'Answer type: Freeform text', 'id' : 1},
            {'name' : 'Answer type: Single-select', 'id': 2},
            {'name' : 'Answer type: Multi-select', 'id' : 3}
        ];
    };

    function sessionTypeOptions(){
        return [
            {
                id: 'individual_presentation',
                name: 'Presentation'
            },
            {
                id: 'interactive_roundtable_discussion',
                name: 'Interactive Roundtable Discussion'
            },
            {
                id: 'panel_discussion',
                name: 'Panel Discussion'
            },
            {
                id: 'workshop',
                name: 'Workshop'
            },
            {
                id: 'pre_conference_workshop',
                name: 'Pre-Conference Workshop'
            },
            {
                id: 'breakout',
                name: 'Breakout'
            },
            {
                id: 'networking',
                name: 'Networking'
            },
            {
                id: 'reception',
                name: 'Reception'
            },
            {
                id: 'breakfast',
                name: 'Breakfast'
            },
            {
                id: 'lunch',
                name: 'Lunch'
            },
            {
                id: 'dinner',
                name: 'Dinner'
            },
            {
                id: 'gala',
                name: 'Gala'
            },
            {
                id: 'keynote',
                name: 'Keynote'
            }
        ];
    }

    function  exhibitorProfileFields() {
        return [
            {
             code: 'exhibitor_admin',
             label: 'Exhibitor Admin'
            },
            {
             code: 'email',
             label: 'Company Email'
            },
            {
             code: 'phone_number',
             label: 'Phone Number'
            },
            {
             code: 'street_address',
             label: 'Street Address'
            },
            {
             code: 'city',
             label: 'City'
            },
            {
             code: 'zip_code',
             label: 'Zip Code'
            },
            {
             code: 'state',
             label: 'State'
            },
            {
             code: 'country',
             label: 'Country'
            },
            {
             code: 'users',
             label: 'Users'
            },
            {
             code: 'promo_code',
             label: 'Promo Code'
            }
        ];
    }

    function editModeExhibitorProfileFields(){
        return [
            {
             code: 'company_name',
             label: 'Company Name'
            },
            {
             code: 'admin',
             label: 'Admin Name'
            },
            {
             code: 'admin_email',
             label: 'Company Email'
            },
            {
             code: 'phone_number',
             label: 'Phone Number'
            },
            {
             code: 'street_address',
             label: 'Street Address'
            },
            {
             code: 'city',
             label: 'City'
            },

            {
             code: 'zip_code',
             label: 'Zip Code'
            },
            {
             code: 'state',
             label: 'State'
            },
            {
             code: 'country',
             label: 'Country'
            },
            {
             code: 'booth_contact_first_name',
             label: 'Booth Contact First Name'
            },
            {
             code: 'booth_contact_last_name',
             label: 'Booth Contact Last Name'
            },
            {
             code: 'booth_contact_email',
             label: 'Booth Contact Email'
            },
            {
             code: 'booth',
             label: 'Booth Number'
            }
        ];
    }

    function  exhibitorFields() {
        return [
            {
                code: 'name',
                title: 'Company'
            },
            {
                code: 'exhib_admin',
                title: 'Exhibitor Admin'
            },
            {
                code: 'admin_email',
                title: 'Email'
            },
            {
                code: 'booth',
                title: 'Booth'
            },
            {
                code: 'users',
                title: 'Users'
            },
            {
                code: 'scanned_attendees',
                title: 'Attendees Scanned'
            }
        ];
    }

    exports.sessionInputFieldsSampleData = function(conference_start_date){
        return sessionInputFieldsSampleData(conference_start_date);
    };

    exports.defaultHotels = function(){
        return defaultHotels();
    };

    exports.monthStringList = function(){
        return monthStringList();
    };

    exports.defaultCarRentals = function(){
        return defaultCarRentals();
    };

    exports.defaultAirlines = function(){
        return defaultAirlines();
    };

    exports.defaultTransportations = function(){
        return defaultTransportations();
    };

    exports.sessionQuestionOptionType = function(){
        return sessionQuestionOptionType();
    };

    exports.sessionSpeakerFields = function(){
        return sessionSpeakerFields();
    };

    exports.editModeExhibitorProfileFields = function(){
        return editModeExhibitorProfileFields();
    };

    exports.exhibitorProfileFields = function ()
    {
        return sessionTypeOptions();
    };

    exports.exhibitorFields = function ()
    {
        return exhibitorFields();
    };

    exports.sessionTypeOptions = function ()
    {
        return sessionTypeOptions();
    };

    exports.sessionTypeSimple = function ()
    {
        return sessionTypeSimple();
    };

    exports.initAttendeeFields = function ()
    {
        return initAttendeeFields();
    };

    exports.registrantFields = function ()
    {
        return registrantFields();
    };

    exports.sessionInputFields = function ()
    {
        return sessionInputFields();
    };

    exports.tempAttendeeFields = function ()
    {
        return tempAttendeeFields();
    };

    exports.attendeeMappingFields = function ()
    {
        return attendeeMappingFields();
    };

    exports.attendeeMappingFields2 = function ()
    {
        return attendeeMappingFields2();
    };

    exports.exhibitorMappingFields = function()
    {
        return exhibitorMappingFields();
    };

    exports.memberMappingFields = function ()
    {
        return memberMappingFields();
    };

    exports.smallFormForTravel = function ()
    {
        return smallFormForTravel();
    };

    exports.largeFormForTravel = function ()
    {
        return largeFormForTravel();
    };

    exports.dayMonths = function ()
    {
        return dayMonths();
    };

    exports.editAttendeeFields = function ()
    {
        return editAttendeeFields();
    };

    exports.removableAttendeeFields = function ()
    {
        return removableAttendeeFields();
    };

    exports.setStandardAttendeeFields = function ()
    {
        return setStandardAttendeeFields();
    };

    exports.newTravelType = function ()
    {
        return newTravelType();
    };

    exports.merchantCompanyFields = function()
    {
        return merchantCompanyFields();
    };

    exports.merchantIndividualFields = function()
    {
        return merchantIndividualFields();
    };
})(typeof exports === "undefined" ? (window.presets = {}) : exports);
