(function (exports)
{

    function trackSignUp(env) {
        if (env != 'PROD') return;

        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-77226205-1', 'auto');
        ga('send', {
          hitType: 'event',
          eventCategory: 'Account',
          eventAction: 'SignUp'
        });
    }

    function trackFirstConference(env) {
        if (env != 'PROD') return;

        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'UA-77226205-1', 'auto');
        ga('send', {
          hitType: 'event',
          eventCategory: 'Account',
          eventAction: 'ConferenceCreated'
        });
    }

    function trackFirstConferenceFB(env) {
        if (env != 'PROD') return;

        fbq('track', 'ConferenceComplete', {
        value: 1,
        version: '1.0'
        });
    }

    function trackSignUpFB(env) {
        if (env != 'PROD') return;

        fbq('track', 'SignUpComplete', {
        value: 1,
        version: '1.0'
        });
    }

    function trackSignUpViewFB(env) {
        if (env != 'PROD') return;

        fbq('track', 'SignUpView', {
        value: 1,
        version: '1.0'
        });
    }

    function googleRemarketingTag(env) {
        if (env != 'PROD') return;

        var google_conversion_id = 859842105;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* <![CDATA[ */
        window.google_trackConversion({
          google_conversion_id: 859842105,
          google_custom_params: window.google_tag_params,
          google_remarketing_only: true
        });
        //]]>
    }

    function googleAdwordsTracking(env) {
        if (env != 'PROD') return;

        /* <![CDATA[ */
        var google_conversion_id = 859842105;
        var google_conversion_language = "en"
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "gR2pCNrB6W4QucyAmgM";
        var google_remarketing_only = false;
        var google_custom_params = window.google_tag_params;
        window.google_trackConversion({
          google_conversion_id: 859842105,
          google_custom_params: google_custom_params,
          google_remarketing_only: false,
          google_conversion_label: "gR2pCNrB6W4QucyAmgM",
          google_conversion_color: "ffffff",
          google_conversion_format: "3",
          google_conversion_language: "en"
        });
        /* ]]> */
    }

    function linkedinTracking(env) {
        if (env != 'PROD') return;
        (function(){
          var s = document.getElementsByTagName("script")[0];
          var b = document.createElement("script");
          b.type = "text/javascript";
          b.async = true;
          b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
          s.parentNode.insertBefore(b, s);})();
          }

    function facebookPixel(env) {
        if (env != 'PROD') return;
        if(typeof fbq === 'undefined') {
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '424803151191975', {
         em: 'insert_email_variable,'
         });

        }
      }

    exports.trackSignUp = function (env)
    {
        return trackSignUp(env);
    };

    exports.trackFirstConference = function (env)
    {
        return trackFirstConference(env);
    };

    exports.trackFirstConferenceFB = function (env)
    {
        return trackFirstConferenceFB(env);
    };

    exports.trackSignUpFB = function (env)
    {
        return trackSignUpFB(env);
    };

    exports.trackSignUpViewFB = function (env)
    {
        return trackSignUpViewFB(env);
    };

    exports.googleRemarketingTag = function (env)
    {
        return googleRemarketingTag(env);
    };

    exports.googleAdwordsTracking = function (env)
    {
        return googleAdwordsTracking(env);
    };

    exports.linkedinTracking = function (env)
    {
        return linkedinTracking(env);
    };

    exports.facebookPixel = function (env)
    {
        return facebookPixel(env);
    };
})(typeof exports === "undefined" ? (window.marketing = {}) : exports);
