(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){
    var service = {};
    var menuItems = [
      {
        name: 'Dashboard',
        icon: 'explore',
        sref: '.dashboard'
      }      
    ];

    
    service.loadAllItems = function() {
      return $q.when(menuItems);
    }

    return service;
  }

})();
