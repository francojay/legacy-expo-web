//errSrc.js
(function(){
    'use strict';

    angular
        .module('app')
        .directive('preserveDate', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel){
              ngModel.$parsers.unshift(extendDate);
              ngModel.$formatters.unshift(extendDate);

              function extendDate(newDate) {
                  var oldDate = ngModel.$modelValue;
                  console.log(oldDate);
                  // newDate = new Date(newDate);
                  newDate= oldDate;

                  if (newDate && oldDate && newDate !== oldDate) {
                   oldDate.setFullYear(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
                   oldDate.setHours(newDate.getHours(), newDate.getMinutes(), newDate.getSeconds(), newDate.getMilliseconds());
                   return oldDate;
                  }

                  return newDate;
              }
            }
          };
    });
})();