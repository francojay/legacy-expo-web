//errSrc.js
(function(){
	'use strict';

	angular
		.module('app')
		.directive('scrolly', function() {
		return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var raw = element[0];
                element.bind('scroll', function () {
                    if (raw.scrollTop + raw.offsetHeight > (raw.scrollHeight - 500)) {
                        scope.$apply(attrs.scrolly);
                    }
                });
            }
        };
	});
})();