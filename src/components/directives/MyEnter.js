//errSrc.js
(function(){
    'use strict';

    angular
        .module('app')
        .directive('myEnter', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var raw = element[0];
                element.bind('keydown keypress', function () {
                    if(event.which === 13) {
                        scope.$apply(function (){
                            scope.$eval(attrs.myEnter);
                        });

                        event.preventDefault();
                    }
                });
            }
        };
    });
})();