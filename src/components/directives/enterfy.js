(function(){
	'use strict';

	angular
		.module('app')
		.directive('enterfy', function() {
		return {
            restrict: 'A',
            link: function (scope, element, attrs) {
               console.log('enterfy loader');
               element.bind("keydown keypress", function (event) {
                   if(event.which === 13) {
                       scope.$apply(function (){
                           scope.$eval(attrs.ngEnter);
                       });

                       event.preventDefault();
                   }
               });
            }
        };
	});
})();