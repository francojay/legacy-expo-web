(function(){
	'use strict';
		angular
			.module('app')
			.controller('ConferenceAttendeesController', ['$scope', '$state', '$stateParams', 'ngDialog', 'API',
			'ConferenceAttendeesService', 'ConferenceService', 'FileService', '$q', '$window', 'toastr', conferenceAttendeesController]);

			function conferenceAttendeesController($scope, $state, $stateParams, ngDialog, API, ConferenceAttendeesService,
			ConferenceService, FileService, $q, $window, toastr){

				var vm = this;
				vm.conference_id = ConferenceService.hash_conference_id.decode($stateParams.conference_id)[0];
				vm.editModeAttendee = false;
				vm.chka = vm.chkr = false;
                vm.editPermission = $scope.editPermission;
                vm.checkAllAttendees = $scope.$parent.checkAllAttendees;
                vm.attendees_total_count = $scope.$parent.attendees_total_count;
				vm.attendee = _.cloneDeep($scope.$parent.infoAttendee);
				vm.conference = _.cloneDeep($scope.$parent.conference);
                vm.refundAmount = 0;
                vm.attendeePayments = {};
                vm.sendNotificationForRefund = false;
                vm.all_countries = API.COUNTRIES;
                vm.united_states = API.UNITED_STATES;

                vm.forceMaxRefundAmount = function() {
                    var maxAmount = vm.attendeePayments.amount;
                    _.each(vm.attendeePayments.refunds, function(refund){
                        maxAmount -= refund.amount;
                    })

                    if (vm.refundAmount > maxAmount) {
                        vm.refundAmount = maxAmount;
                    }
                }

				if (vm.attendee) {
                    if(_.isEmpty(vm.attendeePayments)){
                        ConferenceAttendeesService
                            .getAttendeePayments(vm.attendee.id)
                            .then(function(response){
                                console.log(response);
                                vm.attendeePayments = response;
                                var maxAmount = vm.attendeePayments.amount;
                                _.each(vm.attendeePayments.refunds, function(refund){
                                    maxAmount -= refund.amount;
                                })
                                vm.refundAmount = maxAmount;
                                vm.attendee_session_info = false;
                            })
                            .catch(function(error){
                                vm.attendeePayments = null;
                                vm.attendee_session_info = true;
                                console.log(error);
                            });
                    }

                    ConferenceAttendeesService.listAttendeeSessions(vm.attendee.id)
                        .then(function(attendee_sessions){
                            console.log(attendee_sessions); console.log(" sessions for this attendee");
                            vm.attendee_sessions = attendee_sessions.sessions_attendees;
                            _.each(vm.attendee_sessions, function(session){
                                session.session_id = session.session.id;
                                session.checkRegister = true;
                            });

                            ConferenceAttendeesService.listScannedSessions(vm.attendee.id)
                                .then(function(scanned_sessions){
                                    console.log(scanned_sessions); console.log(" sessions for this scanned");
                                    vm.attendee_scanned = scanned_sessions.scanned_attendees;
                                    _.each(vm.attendee_scanned, function(session){
                                        var existing_scanned_session = _.find(vm.attendee_sessions, {session_id: session.session.id});
                                        if (existing_scanned_session) {
                                            existing_scanned_session.scanned_at = session.scanned_at;
                                            console.log(existing_scanned_session.scanned_at);
                                            existing_scanned_session.checkScanned = true;
                                        }
                                        else {
                                            session.checkScanned = true;
                                            session.checkRegister = false;
                                            vm.attendee_sessions.push(session);
                                        }
                                    });
                                });
                        });
				}

                if($scope.$parent.conference){
                    $scope.$parent.conference.hasOwnProperty('registration_form') &&
                    $scope.$parent.conference.registration_form.completed_at &&
                    (vm.attendeePayments != null) ?
                    vm.attendee_session_info = false : vm.attendee_session_info = true;
                }

                vm.openRefundPaymentPopup = function(){
                    $scope.$parent.openAttendeeRefundPaymentPopup();
                };

                vm.lockRefundButton = false;
                vm.refundPayment = function(){
                    if(vm.refundAmount > 0){
                        if (!vm.lockRefundButton) {
                            vm.lockRefundButton = true;
                            ConferenceAttendeesService
                                .refundAttendeePayment(vm.attendee.id, vm.refundAmount)
                                .then(function(response){
                                    $scope.$parent.postRefundProcessing(response, vm.attendee);
                                    vm.lockRefundButton = false;
                                    $scope.closeThisDialog();

                                })
                                .catch(function(error){
                                    console.log(error);
                                    vm.lockRefundButton = false;
                                    $scope.closeThisDialog();
                                });
                        }

                    } else{
                        toastr.error('You can not refund $' + vm.refundAmount + ' !');
                        return;
                    }
                };

				vm.newAttendeeInfo = {
					first_name: null,
					last_name: null,
                    company_name: null
				};

                vm.updateAveryFileOption = function(selection) {
                    vm.averyTemplateFile = selection;
                }

                vm.goToAvery = function() {
                    $window.open(vm.averyUrl, "_blank");
                }

                vm.testIfHasTemplates = function(templates) {
                    if (templates.length <=0) {
                        vm.uploadAttendeesAsCsv();
                    }
                }

                vm.checkRegisterAttendee = function(session) {
                    if (!session.checkRegister) {
                        session.checkRegister = true;
                    } else {
                        session.checkRegister = false;
                    }
                }

                vm.checkScannedAttendee = function(session) {
                    if (!session.checkScanned) {
                        session.checkScanned = true;
                    } else {
                        session.checkScanned = false;
                    }
                }

                vm.uploadAttendeesAsCsv = function() {
                    vm.attendees = $scope.$parent.attendees;
                    var allChecked = false;
                    var checkedAttendeeList = [];
                    var customAttendeeFields = [];
                    if (!vm.checkAllAttendees) {
                        _.each(vm.attendees, function(attendee) {
                            if (attendee.checked == true) {
                                var mainProfileAttendee = _.omit(attendee,
                                ['custom_fields' , 'id', 'attendee_id', 'guest_of', 'attendee_photo',
                                    'attendee_registration_level_name','created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                                _.forEach(attendee.custom_fields, function(customField){

                                    if (customField.custom_attendee_field.type == 3) {
                                        try {
                                            mainProfileAttendee[customField.custom_attendee_field.name] = customField.value.join();
                                        }
                                        catch(err) {
                                            mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                        }
                                    } else {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                    }
                                    var index = customAttendeeFields.indexOf(customField.custom_attendee_field.name);
                                    if (index == -1) {
                                        customAttendeeFields.push(customField.custom_attendee_field.name);
                                    }
                                });

                                mainProfileAttendee['Conference Name'] = $scope.$parent.conferenceName;
                                mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                                checkedAttendeeList.push(mainProfileAttendee);
                            }
                        });

                        var finalList = [];
                        _.each(checkedAttendeeList, function(item) {
                            _.each(customAttendeeFields, function(field) {
                                if (!item[field]) {
                                    item[field] = "";
                                }
                            });
                            finalList.push(item);
                        });
                    } else {
                        allChecked = true;
                        var nr_pages = parseInt(vm.attendees_total_count / 30) + 2;
                        var promises = [];
                        for (var idx = 1; idx <= nr_pages + 1; idx ++) {

                            promises.push(ConferenceAttendeesService.getAllAttendees(vm.conference_id, idx));
                        }

                        $q.all(promises).then(function(responses){
                            var allAttendees = [];

                            _.each(responses, function(response){
                                allAttendees = allAttendees.concat(response.attendees);
                            });

                            _.each(allAttendees, function(attendee) {
                                var mainProfileAttendee = _.omit(attendee,
                                ['custom_fields' , 'id', 'attendee_id', 'guest_of', 'attendee_photo',
                                    'attendee_registration_level_name','created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
																
                                _.forEach(attendee.custom_fields, function(customField){
                                    if (customField.custom_attendee_field.type == 3) {
                                        try {
                                            mainProfileAttendee[customField.custom_attendee_field.name] = customField.value.join();
                                        }
                                        catch(err) {
                                            mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                        }
                                    } else {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                    }
                                    var index = customAttendeeFields.indexOf(customField.custom_attendee_field.name);
                                    if (index == -1) {

                                        customAttendeeFields.push(customField.custom_attendee_field.name);
                                    }
                                });
                                mainProfileAttendee['Conference Name'] = $scope.$parent.conferenceName;
                                mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                                checkedAttendeeList.push(mainProfileAttendee);
                            });

                            var finalList = [];
                            _.each(checkedAttendeeList, function(item) {
                                _.each(customAttendeeFields, function(field) {
                                    if (!item[field]) {
                                        item[field] = "";
                                    }
                                });
                                finalList.push(item);
                            });

                            vm.uploadAttendeesFileAndGoToAvery(finalList);

                        })
                        .catch(function(error){
                            console.log(error);
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                lib.processValidationError(error, toastr);
                            }
                            return;
                        });
                        return;
                    }

                    vm.uploadAttendeesFileAndGoToAvery(checkedAttendeeList);
                }

                vm.uploadAttendeesFileAndGoToAvery = function(checkedAttendeeList) {
                    var date = new Date();
                    var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                    var filename = $scope.$parent.conferenceName + actualDate + ".csv";

                    var csvData =  $scope.$parent.ConvertToCSV2(checkedAttendeeList);

                    var file = new File([csvData], filename, {type: "text/csv", lastModified: date});

                    FileService.getUploadSignature(file)
                        .then(function(response){
                            var file_link = response.data.file_link;
                            FileService.uploadFile(file, response)
                                .then(function(uploadResponse){
                                    var sku = 5392;
                                    if ($scope.$parent.templateTypeName=='badge') sku = 5392;
                                    else sku = 22805;

                                    if (vm.averyTemplateFile) {
                                        vm.averyUrl = "http://services.print.avery.com/dpp/public/v1/open/US_en/"
                                            + "?averyFileName=" + vm.averyTemplateFile.file_url
                                            + "&mergeDataURL=" + file_link + "&provider=expopass";
                                    } else {
                                        vm.averyUrl = "http://services.print.avery.com/dpp/public/v1/open/US_en/?sku=" + sku +
                                            "&mergeDataURL=" + file_link + "&provider=expopass";
                                    }

                                    console.log(vm.averyUrl);

                                    console.log(vm.averyTemplateFile);
                                })
                                .catch(function(error){
                                    console.log(error);
                                });
                        })
                        .catch(function(error){
                            console.log(error);
                        });
                }

                vm.getUserQrCode = function(user) {
                    return user.attendee_id + ';' + user.first_name + ';' + user.last_name;
                }

				vm.uploadAttendeePhoto = function(files) {
                    lib.squareifyImage($q, files)
                        .then(function(response){
                            var imgGile = response.file;

                            FileService.getUploadSignature(imgGile)
                                .then(function(response){
                                    var file_link = response.data.file_link;
                                    FileService.uploadFile(imgGile, response)
                                        .then(function(uploadResponse){
                                            vm.newAttendeeInfo.attendee_photo = file_link;
                                        })
                                        .catch(function(error){
                                            console.log(error)
                                        });
                                })
                                .catch(function(error){
                                    console.log(error);
                                    if (error.status == 403) {
                                        toastr.error('Permission Denied!');
                                    } else {
                                        toastr.error('Error!', 'There has been a problem uploading your image.');
                                    }
                                });
                        });
				}

				vm.uploadExistingAttendeePhoto = function(files, attendee) {
                    lib.squareifyImage($q, files)
                        .then(function(response){
                            var imgGile = response.file;

                            FileService.getUploadSignature(imgGile)
                                .then(function(response){
                                    var file_link = response.data.file_link;
                                    var promises = [];

                                    if(attendee.id) {
                                        promises.push(FileService.uploadFile(imgGile, response));
                                    } else {
                                        promises.push(FileService.uploadFile(imgGile, response));
                                    }

                                    $q.all(promises).then(function(response){
                                        attendee.attendee_photo = file_link;
                                    }).catch(function(error){
                                        console.log(error);
                                    });

                                })
                                .catch(function(error){
                                    console.log(error);
                                    if (error.status == 403) {
                                        toastr.error('Permission Denied!');
                                    } else {
                                        toastr.error('Error!', 'There has been a problem with your request.');
                                    }
                                });
                        });
                }

				vm.createAttendee = function(next, closeThisDialog){
                    var ok = true;
                    _.forOwn(vm.newAttendeeInfo, function(value, key) {
                        if (!value || String(value).trim() == '') {
                            vm.newAttendeeInfo[key] = null;
                        }

                        if (value && value.length >= 255) {
                            ok = false;
                            toastr.error(lib.titleCaseForKey(key) + ': ensure this field is no longer that 255 characters!')
                        }
                    });

                    if (!ok) {
                        return;
                    } else {
                        console.log(vm.newAttendeeInfo);
                        ConferenceAttendeesService
                            .validateNewAttendee(vm.newAttendeeInfo, vm.conference)
                            .then(function(attendee){
                                attendee.conference_id = vm.conference_id;
                                ConferenceAttendeesService
                                    .addAttendee(attendee)
                                    .then(function(postedData){
                                        vm.newAttendeeInfo = {};
                                        $scope.updatedStuff(postedData, true, next);
                                        closeThisDialog();
                                    })
                                    .catch(function(error){
                                        toastr.error(error.data.detail);
                                        return;
                                    });
                            })
                            .catch(function(error){
                                console.log(error);
                                if (error.status == 403) {
                                    toastr.error('Permission Denied!'); return;
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                    return;
                                }
                            });
                    }
				};

                vm.saveAndAdd = function(next, closeThisDialog){
                    ConferenceAttendeesService
                        .validateNewAttendee(vm.newAttendeeInfo, vm.conference)
                        .then(function(attendee){
                            attendee.conference_id = vm.conference_id;
                            ConferenceAttendeesService
                                .addAttendee(attendee)
                                .then(function(postedData){
                                    vm.newAttendeeInfo = {};
                                    $scope.updatedStuff(postedData, true, next);
                                    closeThisDialog();
                                    setTimeout($scope.openForm, 1000);
                                })
                                .catch(function(error){
                                    console.log(error);
                                    toastr.error(error.data.detail);
                                    return;
                                });
                        })
                        .catch(function(error){
                            console.log(error);
                            if (error.status == 403) {
                                toastr.error('Permission Denied!'); return;
                            } else {
                                toastr.error('Error!', 'There has been a problem with your request.'); return;
                            }
                        });
                }

                $scope.$watch();

				vm.updateAttendee = function(updatedAttendee){
				    if (vm.attendee_sessions) {
				        updatedAttendee.attendee_sessions = [];
				        _.each(vm.attendee_sessions, function(attendee_session){
				            var session_id = attendee_session.id;
                            if(attendee_session){
    				            updatedAttendee.attendee_sessions.push({
    				                session_id: attendee_session.session_id,
    				                check_register: attendee_session.checkRegister || false,
    				                check_scanned: attendee_session.checkScanned || false
    				            });
                            }
				        });

				    } else {
				        updatedAttendee.attendee_sessions = [];
				    }
                    ConferenceAttendeesService
                        .validateNewAttendee(updatedAttendee, vm.conference)
                        .then(function(attendee){
                            attendee.conference_id = vm.conference_id;
                            ConferenceAttendeesService
                                .updateAttendee(_.pickBy(attendee))
                                .then(function(postedData){
                                    vm.newAttendeeInfo = {};
                                    $scope.updatedStuff(attendee);
                                    $scope.infoAttendee = attendee;
                                    $scope.closeThisDialog();
                                })
                                .catch(function(error){
                                    console.log(error);
                                    if (error.status == 403) {
                                        toastr.error('Permission Denied!');
                                        return;
                                    } else {
                                        // toastr.error(error.data.detail);
                                        return;
                                    }
                                });
                        });
                };
        	}
})();
