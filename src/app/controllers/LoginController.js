
//LoginController
(function(){
	'use strict';

	angular
		.module('app')
		.controller('LoginController', [
			'$rootScope', '$scope', 'SharedProperties', 'ngDialog', 'LoginService', '$http', 'toastr', 'localStorageService',
			'$location',
			loginController
		]);

	function loginController($rootScope, $scope, SharedProperties, ngDialog, LoginService, $http, toastr, localStorageService,
	$location){
		var sharedData = SharedProperties.sharedObject;
		sharedData.defaultPhoto = "/assets/images/login_bg.png";
		sharedData.currentPageTitle = "Login";
		var vm = this;
		vm.user = {
			'email': '',
			'pass': ''
		};
		vm.login = true;
		vm.forgotPass = false;
		vm.sentScreen = false;
		vm.newPass = false;
		vm.invalid_email = false;
		vm.input_email = '';
		vm.new_password = '';
		vm.confirmed_password = '';
		vm.invalidEmailData = false;
		vm.invalidPasswordData = false;

		vm.login = function(){
			if(vm.invalidEmailData || vm.invalidPasswordData){
				return
			} else{
	            LoginService.authenticate(vm.user.email, vm.user.pass)
	            .then(function(data) {
	            	if(data.hasOwnProperty('message')){
	            		vm.token_code = vm.user.pass;
	            		vm.resetPasswordScreen();
	            	} else{
	            		if (data.is_not_verified) {
	            			localStorageService.set('verifyEmail', vm.user.email);
	            			$location.path("/confirm");
	            		}
	            		else {
	            			var isChrome = !!window.chrome;
	            			if(!isChrome){
	            				ngDialog.open({template: 'app/views/browser_detect.html',
				                    className: 'app/stylesheets/_browserDetect.scss'
				                });
	            			} else{
	            				console.log('go to conferences');
	            				$location.path("/conferences");
	            			}
	            		}
		            }
	            })
	            .catch(function(error) {
	                if (error.status == 404) {
	                    toastr.error('There is no user with this email on Expo Pass!');
	                } else {
	                    toastr.error('The password you entered for the user was incorrect!');
	                }
	            });
	        }
		};
		vm.loginWithTempPass = function(){
			location.reload(false);
			vm.sentScreen = false;
			vm.login = true;
		};

		vm.validateData = function(account){
			var numberExp = /[0-9]/g;
			if(account.email && (account.email.lastIndexOf(".com") === -1)){
				account.email = '';
				vm.invalidEmailData = true;
			} else {
				vm.invalidEmailData = false;
			}
			if(account.pass && ((account.pass.length < 8 || account.pass.length > 16) || (!numberExp.test(account.pass)) || vm.invalidEmailData)){
				account.pass = '';
				vm.invalidPasswordData = true;
			} else {
				vm.invalidPasswordData = false;
			}
		};
		vm.validateNewPassword = function(){
			var numberExp = /[0-9]/g;
			if(vm.new_password && vm.confirmed_password && ((vm.new_password.length < 8 || vm.new_password.length > 16) || (!numberExp.test(vm.new_password)) ||
				(vm.new_password !== vm.confirmed_password))){
				vm.new_password = '';
				vm.confirmed_password = '';
				return false;
			} else {
				return true;
			}
		};

		vm.resetPasswordScreen = function(){
			vm.newPass = true;
			vm.login = false;
		};

		vm.backToLogin = function(){
			vm.forgotPass = false;
			vm.login = true;
			location.reload(false);
		};

		vm.forgotPassword = function(){
				LoginService
				.forgotPassword(vm.input_email)
				.then(function(data){
					vm.sentScreen = true;
					vm.forgotPass = false;
					vm.invalid_email = false;
				})
				.catch(function(error){
				    toastr.error('This email address was not found in our community!');
					vm.invalid_email = true;
				});
		};

		vm.resetAccountPassword = function(){
			if(vm.validateNewPassword() === true){
				var newData = {
					'email_address' : vm.user.email,
					'token_code': vm.token_code,
					'password': vm.confirmed_password
				};
			} else {
				var newData = {};
			}
				LoginService
					.resetPassword(newData)
					.then(function(data){
						toastr.success('Your password has been reseted!');
						location.reload(false);
						vm.login = true;
						vm.newPass = false;
					})
					.catch(function(error){
						toastr.error('The password must contain at least 1 digit', 'Must be longer than 8 characters and shorter than 16 characters');
					});
		};
	}

})();
