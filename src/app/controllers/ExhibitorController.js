(function(){
    'use strict';
        angular
            .module('app')
            .controller('ExhibitorController', [
                '$scope', '$rootScope', '$state', 'API', 'ngDialog', 'SharedProperties', 'ConferenceExhibitorsService',
                'ConferenceService', '$stateParams', 'ConferenceAttendeesService', 'ConferenceSessionsService', 'ConferenceUsersService', 'localStorageService',
                'toastr', '$location', 'selectedPane', 'selectedType', 'conference', 'FileService', '$q', '$window', '$timeout',
                exhibitorController
                ]);

        function exhibitorController($scope, $rootScope, $state, API, ngDialog, SharedProperties, ConferenceExhibitorsService,
        ConferenceService, $stateParams, ConferenceAttendeesService, ConferenceSessionsService, ConferenceUsersService, localStorageService,
          toastr, $location, selectedPane, selectedType, conference, FileService, $q, $window, $timeout){

            var vm = this;
            var sharedData = SharedProperties.sharedObject;
            sharedData.displayConference = false;
            vm.user = SharedProperties.user;

            if (selectedPane) {
                vm.selectedPane = selectedPane;
                SharedProperties.sharedObject.selectedPane = vm.selectedPane;
            }

            try {
                Stripe.setPublishableKey(API.STRIPE_KEY);
            }
            catch(err) {
                console.log(err);
            }

            function format_date_for_validation(date) {
                if (!date) return format_date_for_validation(new Date());
                var dd = date.getDate();
                var mm = date.getMonth()+1; //January is 0!
                var yyyy = date.getFullYear();

                if(dd<10) {
                    dd='0'+dd
                }

                if(mm<10) {
                    mm='0'+mm
                }
                var today = yyyy+'-'+mm+'-'+dd;
                return today;
            }

            vm.united_states = API.UNITED_STATES;
            vm.all_countries = API.COUNTRIES;
            vm.conferenceDays = [];

            vm.selectedType = null;
            if (selectedType) {
                vm.selectedType = selectedType;
            }

            console.log(vm.selectedType);
            vm.checkAllUsers = false;
            vm.userChecked = false;

            vm.verifyCurrentUser = function(){
                var isFound, usersChecked = [], ok = true;
               //  isFound = _.find(vm.exhibitorUsers, function(conference_user){
               //      return (conference_user.conference_user_rights.user_id === vm.user.id);
               //  }) || undefined;
               //  _.each(vm.conferenceUsers, function(current_user){
               //      if(current_user.checked){
               //          usersChecked.push(current_user);
               //      }
               //  });
               //  _.each(usersChecked, function(userChecked){
               //      if((isFound !== undefined) && isFound.conference_user_rights.user_id === userChecked.conference_user_rights.user_id){
               //          ok = false;
               //          return;
               //      }
               //  });
               return ok;
            };

            vm.testIfUserChecked = function() {
                var checkedUsersList = [];

                _.each(vm.exhibitorUsers, function(user) {
                    if (user.checked == true) {
                        checkedUsersList.push(user);
                    }
                });
                if (checkedUsersList.length > 0) {
                    vm.userChecked = true;
                }
                else {
                    vm.userChecked = false;
                }
            };

            vm.checkAll = function(array) {
                if(array === vm.exhibitorUsers){
                    vm.checkAllUsers = !vm.checkAllUsers;
                    _.each(array, function(element) {
                        element.checked = vm.checkAllUsers;
                    });
                    vm.testIfUserChecked();
                }
            };

            vm.checkOne = function(element, type) {
                if (element.checked != true) {
                    element.checked = true;
                } else {
                    element.checked = false;
                }
                vm.testIfUserChecked();
            };

            vm.verifyExhibitorContactEmail = function(){
                if(vm.exhibitorContactModel.contact_email_address.indexOf('@') > 0 &&
                 vm.exhibitorContactModel.contact_email_address.indexOf('@') < vm.exhibitorContactModel.contact_email_address.indexOf('.') && 
                 vm.exhibitorContactModel.contact_email_address.indexOf('.') - vm.exhibitorContactModel.contact_email_address.indexOf('@') > 1 && 
                 vm.exhibitorContactModel.contact_email_address.indexOf('.') < vm.exhibitorContactModel.contact_email_address.length - 1){
                    return true;
                } else{
                    return false;
                }
            };

            vm.exhibitorContactsOrder = 'asc';

            vm.sortExhibitorContacts = function(){
                if(vm.exhibitorContactsOrder == 'asc'){
                    vm.exhibitorContactsOrder = 'desc';
                    vm.exhibitorContacts = _.orderBy(vm.exhibitorContacts, 'first_name', vm.exhibitorContactsOrder);
                } else if(vm.exhibitorContactsOrder == 'desc'){
                    vm.exhibitorContactsOrder = 'asc';
                    vm.exhibitorContacts = _.orderBy(vm.exhibitorContacts, 'first_name', vm.exhibitorContactsOrder);
                }
            };

            vm.verifyExhibitorContactsSelected = function(){
                var ok = false;
                _.each(vm.exhibitorContacts, function(contact){
                    if(contact.checked && contact.checked == true){
                        ok = true;

                    }
                });
                if(ok){
                    return true;
                } else{
                    return false;
                }
            };

            vm.checkExhibitorContact = function(contact){
                if(!contact.hasOwnProperty('checked')){
                    contact.checked = true;
                } else{
                    contact.checked = !contact.checked;
                }
            };

            vm.checkAllExhibitorContacts = function(){
                if(vm.contactsCheckedAll == false){
                    _.each(vm.exhibitorContacts, function(contact){
                        contact.checked = true;
                    });
                } else{
                     _.each(vm.exhibitorContacts, function(contact){
                        contact.checked = false;
                    });
                }
            };

            vm.contactsCheckedAll = false;

            vm.removeExhibitorContacts = function(){
                var arr_ids = [];
                _.each(vm.exhibitorContacts, function(contact){
                    if(contact.checked && contact.checked == true){
                       arr_ids.push(contact.id);
                    }
                });
                ConferenceExhibitorsService
                    .deleteExhibitorUser(vm.exhibitor.id, arr_ids) 
                    .then(function(response){
                        console.log(response);
                        _.each(vm.exhibitorContacts, function(contact){
                            _.each(arr_ids, function(contact_id){
                                if(contact.id == contact_id){
                                    vm.exhibitorContacts = _.reject(vm.exhibitorContacts, ['id', contact_id]);
                                }
                            });
                        });
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            };

            vm.addExhibitorContact = function(){
                if(vm.verifyExhibitorContactEmail()){
                    ConferenceExhibitorsService
                        .createExhibitorContacts(vm.exhibitorContactModel)
                        .then(function(contact){
                            console.log(contact);
                            _.each(contact, function(value, key){
                                if(contact.user != null && (key == 'first_name' || key == 'last_name') && value == null){
                                    contact[key] = contact.user[key];
                                } else if (contact.user != null && (key != 'first_name' && key != 'last_name') && value == null) {
                                    contact[key] = contact.user.userprofile[key];
                                    contact[key] = contact.user.userprofile[key];
                                    if(key == 'photo'){
                                        contact[key] = contact.user.userprofile.profile_url;
                                    }
                                }
                            });
                            vm.exhibitorContacts.push(contact);
                            vm.exhibitorContactModel.contact_email_address = '';
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                } else{
                    toastr.error('Contact Email Address is incorrect! Please type a valid email address!');

                }
            };

            vm.createNewSupportContact = function(contact, closeThisDialog, eraseInputs){
                ConferenceExhibitorsService
                    .createExhibitorContacts(vm.newSupportContact)
                    .then(function(contact){
                        console.log(contact);
                        vm.newSupportContact = {
                            exhibitor: vm.exhibitor.id
                        };
                        _.each(contact, function(value, key){
                            if(contact.user != null && (key == 'first_name' || key == 'last_name') && value == null){
                                contact[key] = contact.user[key];
                            } else if (contact.user != null && (key != 'first_name' && key != 'last_name') && value == null) {
                                contact[key] = contact.user.userprofile[key];
                                contact[key] = contact.user.userprofile[key];
                                if(key == 'photo'){
                                    contact[key] = contact.user.userprofile.profile_url;
                                }
                            }
                        });
                        vm.exhibitorContacts.push(contact);
                        vm.exhibitorContactModel = {};
                        if (eraseInputs) {
                            eraseInputs()
                        }
                        closeThisDialog();
                    })
                    .catch(function(error){
                        lib.processValidationError(error, toastr);
                    });
            };

            vm.canSaveSupportContacts = function(supportContact) {
                if (!supportContact.first_name || !supportContact.last_name || !supportContact.contact_email_address || !supportContact.area_of_support) {
                    return false;
                }
                return true;
            };

            vm.addExhibitorSupportContact = function(){
                vm.newSupportContact = vm.exhibitorContactModel;
                $scope.contactLabels = vm.contactLabels;
                $scope.canSaveSupportContacts = vm.canSaveSupportContacts;
                $scope.createNewSupportContact = vm.createNewSupportContact;
                $scope.updateSupportContact = vm.updateSupportContact;
                $scope.uploadSupportPhoto = vm.uploadSupportPhoto;
                ngDialog.open({template: 'app/views/partials/exhibitor/exhibitor_support_contact_modal.html',
                   className: 'app/stylesheets/_exhibitorSuportContactsModal.scss',
                   scope: $scope
                });
            };

            vm.editExhibitorContact = function(contact){
                $scope.currentContact = _.cloneDeep(contact);
                $scope.contactLabels = vm.contactLabels;
                $scope.updateSupportContact = vm.updateSupportContact;
                $scope.uploadSupportPhoto = vm.uploadSupportPhoto;
                ngDialog.open({template: 'app/views/partials/exhibitor/edit_company_contact.html',
                   className: 'app/stylesheets/_exhibitorSuportContactsModal.scss',
                   scope: $scope
                });
            };

            vm.updateSupportContact = function(contact, closeThisDialog){
                ConferenceExhibitorsService
                    .updateExhibitorContact(contact)
                    .then(function(data){
                        var exhibitorContacts = [];
                        _.each(vm.exhibitorContacts, function(exhibitor_contact){
                            if (exhibitor_contact.id == data.id) exhibitor_contact = data;
                            exhibitorContacts.push(exhibitor_contact);
                        });
                        vm.exhibitorContacts = exhibitorContacts;
                        closeThisDialog();
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr);
                        }
                    });
            };

            vm.uploadSupportPhoto = function(files, contact) {
                lib.squareifyImage($q, files)
                    .then(function(response){
                        var imgGile = response.file;

                        FileService.getUploadSignature(imgGile)
                            .then(function(response){
                                var file_link = response.data.file_link;
                                var promises = [];

                                promises.push(FileService.uploadFile(imgGile, response));

                                $q.all(promises).then(function(response){
                                    contact.photo = file_link;
                                }).catch(function(error){
                                    console.log(error);
                                });
                            })
                            .catch(function(error){
                                console.log(error);
                                if (error.status == 403) {
                                    toastr.error('Permission Denied!');
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                }
                            });
                    });
            };

            vm.getExhibitorContacts = function(){
                ConferenceExhibitorsService
                    .getExhibitorContacts(vm.exhibitor.id)
                    .then(function(response){
                        console.log(response);
                        _.each(response, function(contact){
                            _.each(contact, function(value, key){
                                if(contact.user != null && (key == 'first_name' || key == 'last_name') && value == null){
                                    contact[key] = contact.user[key];
                                } else if (contact.user != null && (key != 'first_name' && key != 'last_name') && value == null) {
                                    contact[key] = contact.user.userprofile[key];
                                    contact[key] = contact.user.userprofile[key];
                                    if(key == 'photo'){
                                        contact[key] = contact.user.userprofile.profile_url;
                                    }
                                }
                            });
                        });
                        vm.exhibitorContacts = response;
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            };

            vm.getExhibitorFiles = function(){
                ConferenceExhibitorsService
                    .getExhibitorFiles(vm.exhibitor.id)
                    .then(function(response){
                        console.log(response);
                        vm.exhibitorFiles = response;
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            };

            vm.removeExhibitorFile = function(exhibitor_file_id){
                ConferenceExhibitorsService
                    .deleteExhibitorFile(exhibitor_file_id)
                    .then(function(response){
                        vm.exhibitorFiles = _.reject(vm.exhibitorFiles, ['id', exhibitor_file_id]);
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            };

            vm.uploadExhibitorFiles = function(files){
                console.log(files);
                var file_name = files[0].name, file_type = files[0].type;
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        console.log(response);
                        FileService.uploadFile(files[0], response)
                            .then(function(uploadResponse){
                                ConferenceExhibitorsService
                                    .createExhibitorFiles({'exhibitor' : vm.exhibitor.id, 'file_url' : file_link, 'name' : file_name, 'type' : file_type})
                                    .then(function(response){
                                        console.log(response);
                                        vm.exhibitorFiles.push(response);
                                    })
                                    .catch(function(error){
                                        lib.processValidationError(error, toastr);
                                    });
                            })
                            .catch(function(error){
                                lib.processValidationError(error, toastr);
                            });
                    })
                    .catch(function(error){
                        console.log(error);
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr);
                        }
                    });
            };

            vm.reloadConference = function(conference_id) {
                ConferenceService
                    .reloadExhibitorCompleteData(vm.conference.conference_id)
                    .then(function(conference) {
                        vm.setupConferenceData(conference, true);
                    });
            };

            vm.setupConferenceData = function(conference, dontReloadMqtt) {
                dontReloadMqtt = dontReloadMqtt || false;
                vm.conference = conference.conference_info;
                $rootScope.$emit('Conference', vm.conference);
                vm.exhibitor = conference.exhibitor_info;
                vm.conferenceOwner = conference.conference_info.user;
                vm.exhibitorOwner = conference.exhibitor_info.admin;
                vm.conference_id = $stateParams.conference_id;
                vm.leads = conference.leads.leads_list;
                vm.totalLeads = conference.leads.total_leads;
                vm.supportContacts = conference.conference_support_contacts;
                vm.conferenceexhibitorSupportContacts = conference.conference_exhibitor_support_contacts;
                vm.exhibitorUsers = conference.exhibitor_users;
                vm.exhibitor_users_total_count = conference.exhibitor_users.total_count;
                vm.conference.min_date = format_date_for_validation(new Date());
                vm.conferencePermissions = conference.permissions;
                vm.hasInitialPayment = conference.made_initial_payment;
                vm.exhibitorContacts = [];
                vm.exhibitorFiles = [];
                vm.conferenceDays = [];

                SharedProperties.conference = vm.conference;


                if (!dontReloadMqtt && SharedProperties.mqttClient && SharedProperties.mqttClient.isConnected()) {
                    if (API.ENV == 'DEV') {
                        SharedProperties.mqttClient.subscribe("conference_" + vm.conference.conference_id);
                    } else {
                        SharedProperties.mqttClient.subscribe("conference_prod_" + vm.conference.conference_id);
                    }
                    if (SharedProperties.conference) {
                        SharedProperties.mqttClient.unsubscribe("conference_" + SharedProperties.conference.conference_id);
                        SharedProperties.mqttClient.unsubscribe("conference_prod_" + SharedProperties.conference.conference_id);
                    }
                }

                SharedProperties.ConferenceVM = vm;

                vm.exhibitorContactModel = {
                    'exhibitor' : vm.exhibitor.id,
                    'contact_email_address': ''
                };
                vm.getExhibitorFiles();
                vm.getExhibitorContacts();

                if (vm.user.id == vm.exhibitor.admin.id) {
                    vm.currentUserPermissions = {
                        'edit_company_profile': true,
                        'manage_leads': true,
                        'manage_users': true,
                        'download_leads': true
                    }
                } else {
                    vm.currentUserPermissions = {
                        'edit_company_profile': false,
                        'manage_leads': false,
                        'manage_users': false,
                        'download_leads': false
                    }
                }

                var hoursObject = _.groupBy(vm.conference.hours, function(hour){
                    if(hour.type == 1){
                        return new Date(hour.starts_at).getHours();
                    }
                });
                vm.conferenceExhibitingDays = [];
                _.each(hoursObject, function(val, key){
                    vm.conferenceExhibitingDays.push(val);
                });

                console.log(vm.conferenceExhibitingDays);

                var country_name = conference.country;
                if (vm.conference.country.name) {
                    country_name = vm.conference.country.name;
                }

                vm.key = 'AIzaSyA0dA47GflyWNZ4nbB91WDhFyygDLYo9qU';
                vm.googleMap = 'https://www.google.com/maps/place/' + vm.conference.country + ',' + vm.conference.street + ',' + vm.conference.city;
                vm.googleStaticMap = 'https://maps.googleapis.com/maps/api/staticmap?key='+vm.key+
                 '&size=582x256&sensor=false&zoom=15&markers=color%3Ared%7Clabel%3A.%7C' + vm.conference.country
                  +'%20'+vm.conference.city+'%20'+vm.conference.street;

                console.log('mapo');
                console.log(vm.googleMap);

                if (vm.conference.payment_type == 1) {
                    vm.paymentType = 'standard';
                } else if (vm.conference.payment_type == 3) {
                    vm.paymentType = 'conference';
                }
                SharedProperties.exhibitor_info = conference;
                vm.conference.date_from = new Date(vm.conference.date_from);
                vm.conference.date_to = new Date(vm.conference.date_to);
                for (var d = new Date(vm.conference.date_from); d <= new Date(conference.date_to); d.setDate(d.getDate() + 1)) {
                    vm.conferenceDays.push(new Date(d));
                }
                
                vm.exhibitor_materials = conference.exhibitor_materials;

                if (vm.conference.country) {
                    var found = _.find(vm.all_countries, {name: conference.country});
                    if (found) {
                        vm.conference.country = found;
                    }
                }

                if (vm.conference.country == vm.all_countries[0]) {
                    var found = _.find(vm.united_states, {abbreviation: vm.conference.state});
                    if (found) {
                        vm.conference.state = found;
                    }
                }
            };

            if (!_.isEmpty(conference) && conference) {
                vm.setupConferenceData(conference);
            } else {
                //SharedProperties.conference = null;
            }

            vm.afterUpgradePayment = function() {
                if (vm.leadRetrievalSelected) {
                    vm.hasInitialPayment = true;
                }
                if (vm.additionalUsersSelected) {
                    vm.exhibitor.user_slots += vm.additionalUsers;
                }
                vm.leadRetrievalSelected = false;
                vm.additionalUsersSelected = false;
                vm.additionalUsers = null;

            };

            vm.editCompanyProfile = function(){
                $scope.uploadCompanyLogo = vm.uploadCompanyLogo;
                $scope.exhibitor = _.cloneDeep(vm.exhibitor);
                vm.editedExhibitor = $scope.exhibitor;

                _.each(vm.all_countries, function(country){
                    if(country.name == vm.editedExhibitor.country){
                        vm.editedExhibitor.country = country;
                    }
                });
                if(vm.exhibitor.country == 'United States'){
                    _.each(vm.united_states, function(state){
                        if(state.abbreviation == vm.editedExhibitor.state){
                            vm.editedExhibitor.state = state;
                        }
                    });
                }
                $scope.mainExhibitorProfileFields = vm.mainExhibitorProfileFields;
                $scope.updateExhibitorProfile = vm.updateExhibitorProfile;
                ngDialog.open({template: 'app/views/partials/exhibitor/edit_company_profile.html',
                   className: 'app/stylesheets/_editCompanyProfile.scss',
                   scope: $scope
                });
            };

            vm.uploadCompanyLogo = function(files){
                lib.squareifyImage($q, files)
                    .then(function(response){
                        var imgGile = response.file;

                        FileService.getUploadSignature(imgGile)
                            .then(function(response){
                                var file_link = response.data.file_link;
                                FileService.uploadFile(imgGile, response)
                                    .then(function(uploadResponse){
                                        vm.editedExhibitor.logo = file_link;
                                    })
                                    .catch(function(error){
                                        console.log(error)
                                    });
                            })
                            .catch(function(error){
                                console.log(error);
                                if (error.status == 403) {
                                    toastr.error('Permission Denied!');
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                }
                            });
                    });                
            };

            $scope.$watch(function(){
                if(vm.editedExhibitor) return vm.editedExhibitor.country;
            },function(previous_state){
                if(previous_state != undefined){
                    if(previous_state.name == 'United States'){
                        _.each(vm.united_states, function(state){
                            if(state.abbreviation == vm.editedExhibitor.state){
                                vm.editedExhibitor.state = state;
                            }
                        });
                    } else{
                        vm.editedExhibitor.state = '';
                    }
                }
            });

            vm.updateExhibitorProfile = function(closeThisDialog){
                var editedExhibitor = _.cloneDeep(vm.editedExhibitor);

                if (!editedExhibitor.company_name) {
                    toastr.error('The company name is a required field!');
                    return;
                }

                if (!editedExhibitor.admin_email) {
                    toastr.error('The company email is a required field!');
                    return;
                }

                if(_.isObject(editedExhibitor.country)){
                    var country = editedExhibitor.country.name;
                    if(country == 'United States'){
                        var state = editedExhibitor.state.abbreviation;
                        editedExhibitor.state = state;
                        if (!editedExhibitor.state || editedExhibitor.state == '--') {
                            toastr.error('The company state is a required field!');
                            return;
                        }
                    }
                    editedExhibitor.country = country;
                }
                var exhibitor = editedExhibitor;
                exhibitor = _.pickBy(exhibitor);
                exhibitor.name = exhibitor.company_name;
                ConferenceExhibitorsService
                    .updateExhibitorCompanyProfile(exhibitor)
                    .then(function(response){
                        vm.exhibitor = response;
                        vm.exhibitor.company_name = response.name;
                        if(vm.exhibitor.country == 'United States'){
                            _.each(vm.united_states, function(state){
                                if(state.abbreviation == vm.exhibitor.state){
                                    vm.exhibitor.state = state.abbreviation;
                                }
                            });
                        }
                        toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                        closeThisDialog();
                    })
                    .catch(function(error){
                        lib.processValidationError(error, toastr);
                    });
            };

            vm.permissionsForUsers = function() {
                return true;
            };

            vm.deleteSelectedUsers = function() {
                if (vm.checkAllUsers) {
                    $scope.type = '';
                    $scope.resource = 'users';
                    $scope.removeExhibitorUsers = vm.removeExhibitorUsers;
                    ngDialog.open({template: 'app/views/partials/exhibitor/warning_popup_for_delete_exhibitor_users.html',
                          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                          scope: $scope
                    });
                }
                else {
                    var checkedUserList = [];
                    var idList = [];
                    _.each(vm.exhibitorUsers, function(user) {
                        if (user.checked == true && (!user.user || user.user.id !== vm.user.id)) {
                            checkedUserList.push(user);
                            idList.push(user.id);
                        }
                    });

                    if (idList.length > 0) {
                        $scope.nrUsers = idList.length;
                        $scope.removeExhibitorUsers = vm.removeExhibitorUsers;
                        $scope.checkedAttendeeList = checkedUserList;
                        $scope.ids = idList;
                        if (idList.length == 1) $scope.resource = 'user';
                        else $scope.resource = 'users';
                        $scope.type = "partial";
                        ngDialog.open({template: 'app/views/partials/exhibitor/warning_popup_for_delete_exhibitor_users.html',
                              className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                              scope: $scope
                        });
                    }
                }
            };

            vm.selectLeadRetrieval = function() {
                vm.leadRetrievalSelected = true;
            };

            vm.selectAdditionalUsers = function() {
                vm.additionalUsersSelected = true;
                if (!vm.additionalUsers) {
                    vm.additionalUsers = 1;
                }
            };

            vm.unSelectLeadRetrieval = function() {
                vm.leadRetrievalSelected = false;
            };

            vm.unSelectAdditionalUsers = function() {
                vm.additionalUsersSelected = false;
                if (!vm.additionalUsers) {
                    vm.additionalUsers = null;
                }
            };

            vm.updateUsersSlotsCount = function(nrUsers) {
                vm.exhibitor.user_slots += nrUsers;
            };

            vm.promoCodeValue = 0;
            vm.promoCodeError = false;
            vm.addPromoCode = function() {
                if(_.isEmpty(vm.addedPromoCode)){
                    return;
                }
                vm.loadPromoCodeResponse = true;
                ConferenceExhibitorsService
                    .assignPromoCode(vm.exhibitor, vm.addedPromoCode)
                    .then(function(response){
                        toastr.info('Promo code added!');
                        vm.promoCodeError = false;
                        vm.promoCodeValue = response.value; 
                        console.log(vm.promoCodeValue);
                        vm.loadPromoCodeResponse = false;
                    })
                    .catch(function(error){
                        vm.loadPromoCodeResponse = false;
                        vm.promoCodeError = true;
                        vm.addedPromoCode = null;
                    });
            };

            vm.openExhibitorUsersPurchaseForm = function(){
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.conference = vm.conference;
                $scope.user = vm.user;
                $scope.changeCreditCard = vm.changeCreditCard;
                $scope.updatePromoCodes = vm.updatePromoCodes;
                $scope.changeConferencePayment = false;
                $scope.updateUser = vm.updateUser;
                $scope.updateUsersSlotsCount = vm.updateUsersSlotsCount;
                $scope.$parent.exhibitor = vm.exhibitor;

                vm.popup = ngDialog.open({template: 'app/views/partials/exhibitor/exhibitor_user_payment_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            };

            vm.showPaymentTermsFiveFreeScans = function() {
                $scope.leadRetrievalFee = vm.conference.initial_fee + vm.conference.markup_fee;
                ngDialog.open({template: 'app/views/partials/exhibitor/payment_terms.html',
                   className: 'app/stylesheets/_fireFreeScansModal.scss',
                   scope: $scope
                });
            }

            vm.showExibitorPromo = function() {
                ngDialog.open({template: 'app/views/partials/exhibitor/exhibitor_scanning_promo.html',
                   className: 'app/stylesheets/_fireFreeScansModal.scss',
                   scope: $scope
                });
            }

            vm.onSuccessfulLeadRetrievalPayment = function() {
                vm.hasInitialPayment = true;
                vm.exhibitor.payment_status = 'payment_success';
            }

            vm.openExhibitorUserPaymentForm = function(){
                if (vm.popup) {
                    vm.popup.close();
                }
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.conference = vm.conference;
                $scope.user = vm.user;
                $scope.changeCreditCard = vm.changeCreditCard;
                $scope.updatePromoCodes = vm.updatePromoCodes;
                $scope.changeConferencePayment = false;
                $scope.updateUser = vm.updateUser;
                $scope.leadRetrievalSelected = vm.leadRetrievalSelected;
                $scope.additionalUsersSelected = vm.additionalUsersSelected;
                $scope.onSuccessfulLeadRetrievalPayment = vm.onSuccessfulLeadRetrievalPayment;
                
                $scope.conference = vm.conference;
                $scope.exhibitor = vm.exhibitor;
                $scope.totalCost = 0;
                $scope.afterUpgradePayment = vm.afterUpgradePayment;
                if (vm.leadRetrievalSelected) {
                    $scope.totalCost += vm.conference.initial_fee + vm.conference.markup_fee;
                }
                if (vm.additionalUsersSelected && vm.additionalUsers) {
                    $scope.totalCost += vm.conference.exhibitor_user_fee * vm.additionalUsers;
                    $scope.additionalUsers = vm.additionalUsers;
                }

                console.log(vm.leadRetrievalSelected);
                console.log($scope.totalCost);

                vm.popup = ngDialog.open({template: 'app/views/partials/exhibitor/exhibitor_payment_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            };

            vm.usersPermissionsPane = false;
            vm.typeInvitedUserEmailAddress = function() {
                vm.searchEmail = true;
                if (vm.permissionsForActualUser) {
                    vm.setupInitialUserPermissions();
                    vm.permissionsForActualUser = false;
                }
            };

            vm.verifySingleUserChecked = function(){
                var nr = 0;
                 _.each(vm.exhibitorUsers, function(user) {
                    if (user.checked == true) {
                        nr++;
                    }  
                });
                 if(nr == 1){
                    return true;
                 } else 
                    return false;
            };

            vm.setupInitialUserPermissions = function() {
                vm.userPermissions = {
                    download_leads: true,
                    edit_company_profile: true,
                    manage_leads: true,
                    manage_users: true,
                    scan_leads: true
                }
            };

            vm.permissionsForActualUser = true;

            vm.usersPermissions = function() {
                vm.usersPermissionsPane = !vm.usersPermissionsPane;
                if(vm.usersPermissionsPane){
                    _.each(vm.exhibitorUsers, function(user) {
                        if (user.checked == true) {
                            vm.permissionsForActualUser = true;
                            vm.userPermissions = user;
                        }
                    });
                }
            };

            vm.permissionsForUsers = function(){
                if (vm.currentUserPermissions['manage_users']) return true;
                return false;
            };

            vm.permissionsForEditCompanyProfile = function(){
                if (vm.currentUserPermissions['edit_company_profile']) return true;
                return false;
            };

            vm.changePermission = function(option){
                if (vm.userPermissions[option]) vm.userPermissions[option] = false;
                else vm.userPermissions[option] = true;

                if (!vm.permissionsForActualUser) return;
                ConferenceExhibitorsService.updateExhibitorUser(vm.userPermissions)
                    .then(function(data){
                        console.log(data);
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            };

            vm.CCNType = '';
            vm.checkTypeOfCard = function(){
                vm.CCNType = lib.checkCreditCard(document);
                console.log(vm.CCNType);
            }

            vm.applyPromoCode = function() {

            }

            vm.removePromoCode = function() {
                vm.addedPromoCode = null;
                vm.promoCodeValue = 0;
            }

            function stripeResponseHandlerFiveFreeScans(status, response) {
                  if (response.error) { // Problem!
                        toastr.error(response.error.message);
                        vm.processingPayment = false;
                  } else {
                        vm.last4 = response.card.last4;
                        var token = response.id;
                        var addedPromoCode = vm.addedPromoCode;
                        console.log('vm.promoCodeValue');
                        console.log(vm.promoCodeValue);
                        if (!vm.promoCodeValue || vm.promoCodeValue <= 0) {
                            addedPromoCode = null;
                        }
                        ConferenceExhibitorsService
                            .exhibitorInitialPayNew(vm.exhibitor, token, addedPromoCode)
                            .then(function(response){
                                vm.processingPayment = false;
                                vm.exhibitor.payment_status = 'card_stored';
                                toastr.success("Congratulations, You have signed up for 'Five Free Scans'!");
                                vm.closeThisDialog();
                                $timeout(function() {
                                    vm.showExibitorPromo();
                                }, 2000);
                            })
                            .catch(function(error){
                                console.log(error);
                                vm.processingPayment = false;
                                toastr.error(error.data.detail);
                            });
                  }
            };


            vm.processingPayment = false;
            vm.purchaseFiveFreeScans = function(closeThisDialog) {
                vm.closeThisDialog = closeThisDialog;
                vm.processingPayment = true;
                if ($scope.leadRetrievalFee - vm.promoCodeValue > 0) {
                    var form = angular.element('#payment-form');
                    Stripe.card.createToken(form, stripeResponseHandlerFiveFreeScans);
                } else {
                    ConferenceExhibitorsService
                        .exhibitorInitialPayNew(vm.exhibitor, null, vm.addedPromoCode)
                        .then(function(response){
                            vm.processingPayment = false;
                            vm.exhibitor.payment_status = 'payment_success';
                            toastr.success("You are signed up for lead retrieval!");
                            vm.closeThisDialog();
                             $timeout(function() {
                                vm.showExibitorPromo();
                            }, 2000);
                        })
                        .catch(function(error){
                            console.log(error);
                            vm.processingPayment = false;
                            toastr.error(error.data.detail);
                        });
                }
                
            }


            vm.autoFormatExp = function() {
                if (!vm.expirationDate) {
                    return
                }
                if (vm.expirationDate.length >= 2) {
                    var indexOfSlash = vm.expirationDate.indexOf('/');
                    if (indexOfSlash == -1) {
                        vm.expirationDate += '/';
                    }
                }

                vm.expirationDate = vm.expirationDate.replace('//', '/')
            }

            vm.payFiveFreeScans = function(type) {
                $scope.showFiveFreeScansPayment = true;
            }

            vm.go_to_details = function() {
                $location.path('/exhibitor/' + vm.conference_id + '/company-profile');
            }

            vm.fiveFreeScansScreen = 1;
            vm.setUpFiveFreeScans = function(type) {
                vm.promoCodeValue = 0;
                vm.promoCodeError = false;
                vm.addedPromoCode = null;
                if (vm.user.ab_tests.ab_test_code) {
                    var index = vm.user.ab_tests.ab_test_code;
                    if (index > 0 && index < .33) {
                        vm.fiveFreeScansScreen = 1;
                    } else  if (index >= .33 && index < 0.66) {
                        vm.fiveFreeScansScreen = 2;
                    } else {
                        vm.fiveFreeScansScreen = 3;
                    }
                }
                vm.newSupportContact = vm.exhibitorContactModel;
                $scope.payFiveFreeScans = vm.payFiveFreeScans;
                if (type == 'go_to_home') {
                    $scope.action_for_cancel = vm.go_to_details;
                }
                $scope.leadRetrievalFee = vm.conference.initial_fee + vm.conference.markup_fee;
                $scope.showFiveFreeScansPayment = false;
                var dialog = ngDialog.open({template: 'app/views/partials/exhibitor/five_free_scans.html',
                   className: 'app/stylesheets/_fireFreeScansModal.scss',
                   scope: $scope
                });

                if (type == 'go_to_home') {
                    dialog.closePromise.then(function(data) {
                        vm.go_to_details();

                        return true;
                    });


                }
            }

            vm.addUser = function() {
                var ok = true;
                var invalidUser = false;
                _.forEach(vm.exhibitorUsers, function(userFromTable){
                    if(vm.invitedUser.invited_user_email === userFromTable.invited_user_email){
                        ok = false;
                    } else if(vm.invitedUser.invited_user_email === vm.user.email || vm.exhibitorOwner.email === vm.invitedUser.invited_user_email){
                        invalidUser = true;
                    }
                });
                if(ok && !invalidUser){
                    vm.invitedUser.rights = [];
                    vm.invitedUser = _.merge(vm.invitedUser, vm.userPermissions);

                    ConferenceExhibitorsService.inviteUser(vm.invitedUser, vm.exhibitor.id)
                        .then(function(data){
                            if(data){
                                    toastr.success('The user has been invited to your account.', 'Invitation Sent!');
                                    vm.exhibitorUsers.push(data);
                                    vm.invitedUser = {
                                        rights: [],
                                        invited_user_email: ''
                                    };
                                    vm.searchEmail = false;
                            }
                        })
                        .catch(function(error){
                            console.log(error);
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error(error.data.detail);
                            }
                        });
                } else if(!ok){
                    toastr.warning('This user has been already invited to your conference.');
                } else if(invalidUser){
                    toastr.warning("You can't perform this action!")
                }
            };

            vm.removeExhibitorUsers = function(ids, boolAll){
                ConferenceExhibitorsService
                    .removeExhibitorUsers(vm.exhibitor.id, ids, boolAll)
                    .then(function(response){
                        if (boolAll) {
                            vm.exhibitorUsers = [];
                            vm.exhibitor_users_total_count = 0;
                            SharedProperties.exhibitor_info.exhibitor_users = [];
                            vm.userChecked = false;
                        } else {
                            _.each(ids, function(id){
                                _.remove(vm.exhibitorUsers, {
                                    id: id
                                });
                            });
                            SharedProperties.exhibitor_info.exhibitor_users = vm.exhibitorUsers;
                            vm.userChecked = false;
                        }
                        console.log(response);
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            };

            vm.mainExhibitorProfileFields = [
                {
                    label: 'Company Name',
                    code: 'company_name'
                },
                {
                    label: 'Company Email',
                    code: 'admin_email'
                }
            ];

            vm.exhibitorProfileFields = [
                {
                    label: 'Phone Number',
                    code: 'phone_number'
                },
                {
                    label: 'Street Address',
                    code: 'street_address'
                },
                {
                    label: 'City',
                    code: 'city'
                },
                {
                    label: 'State',
                    code: 'state'
                },
                {
                    label: 'Zip Code',
                    code: 'zip_code'
                },
                {
                    label: 'Country',
                    code: 'country'
                },
                {
                    label: 'Website',
                    code: 'website'
                },
                {
                    label: 'Promo Code',
                    code: 'promo_code'
                },
                {
                    label: 'Description',
                    code: 'company_description'
                },
                {
                    label: 'Company Logo',
                    code: 'logo'
                }
            ];

            vm.contactLabels = [
                {label:'First Name', code: 'first_name'},
                {label:'Last Name', code: 'last_name'},
                {label:'Area of Support', code: 'area_of_support'},
                {label:'Email', code: 'contact_email_address'},
                {label:'Phone Number', code: 'phone_number'}
            ];

            vm.exhibitorOptionalProfileFields = _.take(vm.exhibitorProfileFields, vm.exhibitorProfileFields.length - 2);
            vm.mainExhibitorProfileFields = vm.mainExhibitorProfileFields.concat(vm.exhibitorProfileFields);
        }
})();