//MainController.js
(function(){
	'use strict';

	angular
		.module('app')
		.controller('MainController', [
			'$rootScope', '$scope', 'SharedProperties', 'navService', '$mdSidenav', '$log', '$q', '$state', '$location',
			'UserService', 'ConferenceService', 'toastr', 'localStorageService', 'API',
			mainController
		]);

	function mainController($rootScope, $scope, SharedProperties, navService, $mdSidenav, $log, $q, $state, $location,
			UserService, ConferenceService, toastr, localStorageService, API){
		var sharedData = SharedProperties.sharedObject;
		var vm = this;

        $rootScope.$on('Conference', function(Conference, conference){
            vm.conference = conference;
        });

        $rootScope.$on('EditConference', function(EditConference, editMode){
            vm.editMode = editMode;
        });

		function onMessageArrived(message) {
          var messageJson = JSON.parse(message.payloadString);
          var sessionUpdates = ['session'];
          if (messageJson.conference == SharedProperties.conference.conference_id) {
              // toastr.info("Conference Updated");
              if (messageJson.deleted && messageJson.deleted.length > 0) {
                  console.log('conference deleted');
              } else {
                  if (sessionUpdates.indexOf(messageJson.update_model) > -1) {
                        SharedProperties.ConferenceVM.reloadSessions(SharedProperties.conference.conference_id);
                  } else {
                        SharedProperties.ConferenceVM.reloadConference(SharedProperties.conference.conference_id);
                  }
              }


          }
        }

        var unique_identifier = String(Math.random() * 1000);

        vm.connectToMqtt = function() {
            ConferenceService.getSignedUrl()
                .then(function(response){
                    var requestUrl = response;
                    SharedProperties.mqttClient = new Paho.MQTT.Client(requestUrl, unique_identifier);
                    SharedProperties.mqttConnected = false;
                    SharedProperties.mqttClient.onMessageArrived = onMessageArrived;
                    var connectOptions = {
                        onSuccess: function(){
                            SharedProperties.mqttConnected = true;
                            if (API.ENV == 'DEV') {
                                SharedProperties.mqttClient.subscribe("user_" + SharedProperties.user.id);
                            } else {
                                SharedProperties.mqttClient.subscribe("user_prod_" + SharedProperties.user.id);
                            }
                        },
                        useSSL: true,
                        timeout: 3,
                        mqttVersion: 4,
                        onFailure: function() {
                            // connect failed
                        }
                    };
                    SharedProperties.mqttClient.connect(connectOptions);
                }).catch(function(error){
                    console.log(error);
                });
        };

        vm.connectToMqtt();
        SharedProperties.connectToMqtt = vm.connectToMqtt;

		vm.renderStyle = function(){
			if($location.path() == '/conferences' && localStorageService.get('is_superuser')){
				vm.displayScrollBar = false;
				vm.displayConference = true;
			} else if($location.path() == '/conferences' && !localStorageService.get('is_superuser')){
				vm.displayScrollBar = true;
				vm.displayConference = true;
			}else if($location.path() == '//dashboard'){
				vm.displayScrollBar = false;
				vm.displayConference = true;
			} else {
				vm.displayScrollBar = false;
				vm.displayConference = false;
			}
			sharedData.displayConference = vm.displayConference;
			sharedData.displayConference = vm.displayScrollBar;
		};

		UserService.getProfile();
		$scope.$watch(function(){return sharedData.displayConference}, function(nv){vm.displayConference = nv;});
		vm.title = sharedData.currentPageTitle;
		vm.menuItems = [];

		$scope.$watch(function(){ return sharedData.currentPageTitle; }, function(){
			vm.title=sharedData.currentPageTitle;
		});

		$rootScope.$on('ngDialog.opened', function (e, $dialog) {
			console.log('ngDialog opened: ' + $dialog.attr('id'));
			vm.blurred = 'main-div-container';
		});

		$rootScope.$on('ngDialog.closed', function (e, $dialog) {
			console.log('ngDialog closed: ' + $dialog.attr('id'));
			vm.blurred = 'main-div-container1';
			$scope.$apply();
			console.log(vm.blurred);
		});

		navService
			.loadAllItems()
			.then(function(menuItems){
				vm.menuItems = menuItems;
			});

    $scope.env = API.ENV;
    $scope.currentUrl = $state.current.url;
	}

}());
