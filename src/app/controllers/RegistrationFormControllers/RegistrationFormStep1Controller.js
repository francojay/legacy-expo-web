(function() {
    'use strict';

    var controllerId = 'RegistrationFromStep1Controller';
    angular
        .module('app')
        .controller(controllerId, [
            '$rootScope', '$scope', '$state', '$location', '$filter', 'API', 'SharedProperties', 'ConferenceRegistrationService',
            registrationFromStep1Controller
        ]);

    function registrationFromStep1Controller($rootScope, $scope, $state, $location, $filter, API, SharedProperties, ConferenceRegistrationService) {
        $scope.loadingRegistration = true;
        $scope.all_countries = API.COUNTRIES;
        $scope.united_states = API.UNITED_STATES;
        $scope.stepCounter = 1;
        $scope.registrationLoader = false;
        $scope.register_custom_fields_response = [];
        $scope.editMode = $state.params.edit === true ? true : false;
        //$scope.agreeChecked = false; this will be enabled when the GDPR will be released
        $scope.agreeChecked = true;
        $(window).load(function(){
          parent.postMessage({'task': 'form_loaded'}, '*');
        });

        $rootScope.$on('REGISTRATION_FORM_LOADED_FIRST', function() {
            $scope.registration_form = SharedProperties.registration_form;
            $scope.loadingRegistration = false;
            $scope.registeredAttendee = {};
            if (SharedProperties.callback_url) {
                $scope.registeredAttendee.callback_url = SharedProperties.callback_url;
            }
            $scope.standard_fields = $scope.registration_form.registrationformstandardfield_set;
            $scope.custom_fields = $scope.registration_form.registrationformcustomfield_set;
            $scope.validationErrors = [];
        });

        $rootScope.$on('REGISTRATION_FORM_LOADED_SECOND', function() {
            $scope.registration_form = SharedProperties.registration_form;
            $scope.registration_levels = $scope.registration_form.registrationlevel_set;
            if ($location.search().callback) {
                $scope.registeredAttendee.callback_url = $location.search().callback;
                SharedProperties.callback_url = $scope.registeredAttendee.callback_url;
            } else if (SharedProperties.callback_url) {
                $scope.registeredAttendee.callback_url = SharedProperties.callback_url;
            }
            if ($scope.registration_form.facebook_tracking_code) {
                var pass_data = {
                    'func': 'facebook',
                    'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                    'page': 'Expo_ProfileLoad',
                    'event_object': { 'event': $scope.registration_form.conference.code}
                };
                parent.postMessage(JSON.stringify(pass_data), "*");
            }
            var ticket = $location.search().ticket;
            if (ticket !== undefined) {
                verifyRegistrationStatus(ticket);
            } else {
                $scope.loadingRegistration = false;
            }
        });

        $rootScope.$on('SELECTED_OPTION_FOR_THIS_CUSTOM_FIELD', function(SELECTED_OPTION_FOR_THIS_CUSTOM_FIELD, customField) {
            if (!$scope.registeredAttendee.customValues) {
                $scope.registeredAttendee.customValues = [];
            }
            $scope.registeredAttendee.customValues[customField.custom_field.id] = customField.customFieldValueTxt;
        });

        if ($state.params.previous === true) {
            $scope.loadingRegistration = false;
            $scope.registration_form = SharedProperties.registration_form;
            $scope.registration_levels = $scope.registration_form.registrationlevel_set;
            $scope.registeredAttendee = SharedProperties.registeredAttendee;
            if (SharedProperties.callback_url) {
                $scope.registeredAttendee.callback_url = SharedProperties.callback_url;
            }
            $scope.standard_fields = $scope.registration_form.registrationformstandardfield_set;
            $scope.custom_fields = $scope.registration_form.registrationformcustomfield_set;
            if (!$scope.registeredAttendee.customValues) {
                $scope.registeredAttendee.customValues = [];
            }
            $scope.validationErrors = [];
        } else if ($state.params.previous === false) {
            $scope.loadingRegistration = false;
            $scope.registeredAttendee = {};

            if (SharedProperties.callback_url) {
                $scope.registeredAttendee.callback_url = SharedProperties.callback_url;
            }

            if (SharedProperties.registrant_duplicated_sections && SharedProperties.registrant_duplicated_sections.length > 0) {
                _.each(SharedProperties.registrant_duplicated_sections, function(duplicated_section) {
                    $scope.registeredAttendee[duplicated_section.code] = duplicated_section.data;
                });
            }

            if (!$scope.registeredAttendee.customValues) {
                $scope.registeredAttendee.customValues = [];
            }

            $scope.registration_form = SharedProperties.registration_form;
            $scope.registration_levels = $scope.registration_form.registrationlevel_set;
            $scope.standard_fields = $scope.registration_form.registrationformstandardfield_set;
            $scope.custom_fields = _.cloneDeep(SharedProperties.initialCustomFieldsList);
            $scope.validationErrors = [];
        }

        function verifyRegistrationStatus(ticket) {
            ConferenceRegistrationService.continueRegistration(ticket)
                .then(function(response) {
                    SharedProperties.registeredAttendee = response.attendee;
                    SharedProperties.promoCode = response.attendee_registration.promo_code;
                    SharedProperties.registeredAttendee.id = response.attendee_registration.id;
                    SharedProperties.registeredAttendee.continuing_educations = [];
                    SharedProperties.registeredAttendee.pre_register_session = [];
                    SharedProperties.registeredAttendee.receipt_url = response.attendee_registration.receipt_url;
                    SharedProperties.registeredAttendee.access_token = response.temporary_access_token;
                    _.each($scope.registration_form.registrationlevel_set, function(level) {
                        if (level.id === response.attendee_registration.registration_level) {
                            SharedProperties.selectedLevel = level;
                            SharedProperties.levelPrice = response.attendee_registration.level_price;
                            calculateTotalPriceForAttendeeThatWasAlreadyRegistered(response.attendee_session_list);
                            mapPreRegisterSessionsThatWereAdded(response.attendee_registration.attendeeregistrationsession_set, response.attendee_session_list);
                            mapContinuingEducationsThatWereAdded(response.attendee_registration.attendeecontinuingeducation_set);
                            $scope.loadingRegistration = false;
                            // TODO make sure it has pre-registration sessions available
                            if (level.include_pre_registration) {
                                if ($state.$current.url.prefix.search('preview') >= 0) {
                                    $state.go('preview.pre_register_step1');
                                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                    $state.go('registration.pre_register_step1');
                                }
                                return false;
                            } else if (level.include_continuing_education && $scope.registration_form.continuing_educations.length > 0) {
                                if ($state.$current.url.prefix.search('preview') >= 0) {
                                    $state.go('preview.pre_register_step2', { stepNumber: 1 });
                                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                    $state.go('registration.pre_register_step2', { stepNumber: 1 });
                                }
                                return false;
                            } else {
                                if ($state.$current.url.prefix.search('preview') >= 0) {
                                    $state.go('preview.registration_last_step');
                                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                    $state.go('registration.registration_last_step');
                                }
                            }
                        }
                    });
                }, function(error) {
                    $scope.loadingRegistration = false;
                });
        }

        function calculateTotalPriceForAttendeeThatWasAlreadyRegistered(attendee_session_list) {
            SharedProperties.totalPrice = SharedProperties.levelPrice;
            SharedProperties.attendee_adds_on_session_list_with_ticket = [];
            _.each(attendee_session_list, function(attendee_add_on) {
                if (attendee_add_on.session_price > 0) {
                    SharedProperties.attendee_adds_on_session_list_with_ticket.push(attendee_add_on);
                    SharedProperties.totalPrice += attendee_add_on.session_price;
                }
            })
        }

        function mapPreRegisterSessionsThatWereAdded(attendeeregistrationsessions_set, all_registered_sessions) {
            _.each(attendeeregistrationsessions_set, function(attendeeregistrationsession_set) {
                var add_on_already_paid = _.find(all_registered_sessions, function(session_registered) {
                    return session_registered.session_price > 0 && session_registered.session_id === attendeeregistrationsession_set.session.id;
                });
                if (add_on_already_paid === undefined) {
                    SharedProperties.registeredAttendee.pre_register_session.push(attendeeregistrationsession_set.session.id);
                }
            });
        }

        function mapContinuingEducationsThatWereAdded(attendeecontinuingeducations_set) {
            _.each(attendeecontinuingeducations_set, function(attendeecontinuingeducation_set) {
                SharedProperties.registeredAttendee.continuing_educations.push(attendeecontinuingeducation_set.continuing_education.id);
            });
        }

        $scope.resetIframe = function() {
            $scope.registeredAttendee = {};
            $scope.custom_fields = _.cloneDeep(SharedProperties.initialCustomFieldsList);
            $scope.validationErrors = [];
        };

        function mapCustomFieldsForRegisteredAttendee() {
            var customAttendeeFieldValueList = [];
            _.forOwn($scope.registeredAttendee.customValues, function(item, key) {
                if (item) {
                    var custom_attendee_field = {
                        id: key
                    };

                    if (item.value) {
                        item = item.value;
                    }
                    customAttendeeFieldValueList.push({
                        custom_attendee_field: custom_attendee_field,
                        value: item,
                        name: _.find($scope.custom_fields, function(custom_field_item) {
                            return custom_field_item.custom_field.id === parseInt(key);
                        }).custom_field.name
                    })
                }
            });

            $scope.registeredAttendee.customattendeefieldvalue_set = customAttendeeFieldValueList;
        }

        $scope.createRegistrationOrder = function() {
            var data = {
                "attendee_registrations": [{
                    "first_name": $scope.registeredAttendee.first_name,
                    "last_name": $scope.registeredAttendee.last_name,
                    "email_address": $scope.registeredAttendee.email_address,
                    "customValues": $scope.registeredAttendee.customValues,
                    "customattendeefieldvalue_set": $scope.registeredAttendee.customattendeefieldvalue_set
                }]
            };
            return ConferenceRegistrationService.createRegistrationOrder($scope.registration_form.conference.id, data);
        };

        $scope.goToStep2 = function() {
            if (validateRegistrationForm()) {
                $scope.registrationLoader = true;
                ConferenceRegistrationService.getRegistrationLevelsMemberTestUnauthorized(
                        $scope.registration_form.id, $scope.registeredAttendee['email_address'])
                    .then(function(response) {
                        if (_.isEmpty(response)) {
                            $scope.validationErrors = [];
                            $scope.validationErrors.push("There is no available levels for this email address");
                            $scope.registrationLoader = false;
                            return;
                        }
                        $scope.availableLevels = response;
                        var array = _.cloneDeep($scope.registration_levels);
                        var remove_list_of_ids = [];
                        _.forIn(array, function(registration_level) {
                            var ok = false;
                            _.each($scope.availableLevels, function(ok_level_id) {
                                if (registration_level.id === ok_level_id) {
                                    ok = true;
                                }
                            });
                            if (!ok) {
                                remove_list_of_ids.push(registration_level.id);
                            }
                        });
                        _.each(remove_list_of_ids, function(removed_level_id) {
                            array = _.reject(array, function(level) {
                                return removed_level_id == level.id;
                            });
                        });

                        SharedProperties.registration_levels = array;
                        $scope.registrationLoader = false;
                        getRegisterCustomFieldsValues();

                        SharedProperties.registeredAttendee = $scope.registeredAttendee;

                        if ($state.params.edit === true && verifIfAlreadySelectedLevelIsAvailable(response) !== undefined) {
                            if ($state.$current.url.prefix.search('preview') >= 0) {
                                $state.go('preview.step5', { stepNumber: $state.params.previousStep });
                            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                $state.go('registration.step5', { stepNumber: $state.params.previousStep });
                            }
                        } else {
                            $scope.createRegistrationOrder()
                                .then(function(order) {
                                    $scope.registration_form.order = order;
                                    if ($state.$current.url.prefix.search('preview') >= 0) {
                                        $state.go('preview.step2', { stepNumber: 2, edit: $state.params.edit });
                                    } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                        $state.go('registration.step2', { stepNumber: 2, edit: $state.params.edit });
                                    }
                                })
                                .catch(function(error) {
                                    $scope.registrationLoader = false;
                                    getRegisterCustomFieldsValues();
                                });
                        }
                    }).catch(function(error) {
                        $scope.registrationLoader = false;
                        getRegisterCustomFieldsValues();
                    });
            }
        };

        function verifIfAlreadySelectedLevelIsAvailable(levels_available) {
            if (!$scope.registeredAttendee.hasOwnProperty('registration_level')) {
                return undefined;
            } else {
                var ok = _.find(levels_available, function(level) {
                    return level === $scope.registeredAttendee.registration_level;
                });
                return ok;
            }
        }

        function getRegisterCustomFieldsValues() {
            _.each($scope.registeredAttendee.customValues, function(value, key) {
                _.each($scope.custom_fields, function(cf) {
                    if (cf.custom_field.id === key) {
                        if (_.isString(value)) {
                            $scope.register_custom_fields_response.push({ 'custom_field_name': cf.custom_field.name, 'custom_field_response': value });
                        } else if (_.isPlainObject(value)) {
                            $scope.register_custom_fields_response.push({ 'custom_field_name': cf.custom_field.name, 'custom_field_response': value.value });
                        } else if (_.isArray(value)) {
                            var obj = { 'custom_field_name': cf.custom_field.name, 'custom_field_response': '' };
                            _.each(value, function(val) {
                                obj.custom_field_response += val.value + ', ';
                            });
                            obj.custom_field_response = obj.custom_field_response.slice(0, obj.custom_field_response.length - 2);
                            $scope.register_custom_fields_response.push(obj);
                        }
                    }
                });
            });
            mapCustomFieldsForRegisteredAttendee();
        }

        // Validate registration form
        function validateRegistrationForm() {
            var validated = true;
            $scope.validationErrors = [];
            _.each($scope.standard_fields, function(standard_field) {
              if (standard_field.type === 'required' && !$scope.registeredAttendee[standard_field.name]) {
                if (standard_field.name === 'state' && $scope.registeredAttendee.country != 'United States') {
                  return
                } else {
                  $scope.validationErrors.push("Field is required: " + $filter('formatLabel')(standard_field.name));
                  validated = false;
                }
              }
            });
            _.each($scope.custom_fields, function(standard_field) {
                if (standard_field.type === 'required' &&
                    (!$scope.registeredAttendee.customValues || !$scope.registeredAttendee.customValues[standard_field.custom_field.id])) {
                    $scope.validationErrors.push("Field is required: " + standard_field.custom_field.name);
                    validated = false;
                }
            });
            if ($scope.registeredAttendee['email_address'] && ($scope.registeredAttendee['email_address'].indexOf('@') <= -1 ||
                $scope.registeredAttendee['email_address'].indexOf('@') >= $scope.registeredAttendee['email_address'].length - 1 ||
                $scope.registeredAttendee['email_address'].indexOf('@') === 0)) {
                $scope.validationErrors.push("Email Address is incorrect!");
                validated = false;
            }
            if(!$scope.agreeChecked){
              $scope.validationErrors.push("You must agree to our Terms & Conditions and Privacy policy to continue.");
              validated = false;
            }
            return validated;
        }
        $scope.agreeTos = function() {
          $scope.agreeChecked = !$scope.agreeChecked ? true : false;
        }
    }
})();
