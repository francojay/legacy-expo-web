(function() {
    'use strict';

    var controllerId = 'RegistrationFromOrderCompleteController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$rootScope', '$stateParams', '$state', 'ngDialog', '$timeout', '$window', 'SharedProperties',
            'ConferenceRegistrationService',
            registrationOrderCompleteController
        ]);

    function registrationOrderCompleteController($scope, $rootScope, $stateParams, $state, ngDialog, $timeout, $window, SharedProperties, ConferenceRegistrationService) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        if ($stateParams.redir === 'y') {
            $window.parent.location.href = $window.location.href.replace(/\/y\//g, '/n/');
            //$window.location.href = $window.location.href.replace(/\/y\//g, '/n/');
        } else {
            $scope.loadingRegistration = true;
            ngDialog.closeAll();
            $scope.registration_form = SharedProperties.registration_form;
            $scope.selectedLevel = SharedProperties.selectedLevel;
            ConferenceRegistrationService.getRegistrationOrder($scope.registration_form.conference.id, $stateParams.order_code)
                .then(function(response) {
                    SharedProperties.registration_order = response;
                    $scope.loadingRegistration = false;
                    if (response.hasOwnProperty('registration_data')) {
                        if ($scope.registration_form.tracking_code) {
                            var pass_data = {
                                'func': 'analytics',
                                'tracking_code': $scope.registration_form.tracking_code,
                                'price': response.value,
                                'code': $scope.registration_form.conference.code
                            };
                            parent.postMessage(JSON.stringify(pass_data), "*");
                        }

                        goToIntermediaryStep();
                    } else {
                        SharedProperties.retRegisteredAttendee = response.order_data[0];
                        SharedProperties.retRegisteredAttendee.attendee_guest_set = response.order_data[0].attendee_guest_set;
                        SharedProperties.registration_order = response;
                        var errMsg = "";
                        if(response.order_data[0].hasOwnProperty('cc_return_msg')){
                          errMsg = response.order_data[0].cc_return_msg;
                        }
                        SharedProperties.fis_errors = errMsg;

                        if (SharedProperties.order_type === 'preview') {
                            $state.go('preview.step5', { conference_id: $stateParams.conference_id });
                        } else if (SharedProperties.order_type === 'registration') {
                            $state.go('registration.step5', { conference_id: $stateParams.conference_id });
                        }
                    }

                })
                .catch(function(err) {
                    $scope.loadingRegistration = false;
                    SharedProperties.promoCode = null;
                    //$scope.validationErrors.push(error.data.detail);
                });
        }

        function goToIntermediaryStep() {
            var continuing_education_valability = $scope.selectedLevel.include_continuing_education;
            var pre_registration_valability = $scope.selectedLevel.include_pre_registration;

            if (pre_registration_valability) {
                if (SharedProperties.order_type === 'preview') {
                    $state.go('preview.intermediary_step', { conference_id: $stateParams.conference_id });
                } else if (SharedProperties.order_type === 'registration') {
                    $state.go('registration.intermediary_step', { conference_id: $stateParams.conference_id });
                }
            } else if (continuing_education_valability && $scope.registration_form.continuing_educations.length > 0) {
                if (SharedProperties.order_type === 'preview') {
                    $state.go('preview.intermediary_step');
                } else if (SharedProperties.order_type === 'registration') {
                    $state.go('registration.intermediary_step');
                }
            } else {
                if (SharedProperties.order_type === 'preview') {
                    $state.go('preview.registration_last_step', { conference_id: $stateParams.conference_id });
                } else if (SharedProperties.order_type === 'registration') {
                    $state.go('registration.registration_last_step', { conference_id: $stateParams.conference_id });
                }
            }
        }
    }


})();
