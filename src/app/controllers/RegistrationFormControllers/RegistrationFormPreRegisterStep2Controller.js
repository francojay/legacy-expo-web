(function() {
    'use strict';

    var controllerId = 'RegistrationFormPreRegisterStep2Controller';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', 'redirectToFirstStep',
            registrationFormPreRegisterStep2Controller
        ]);

    function registrationFormPreRegisterStep2Controller($scope, $state, SharedProperties, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');
      
        if(!redirectToFirstStep){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false});
            }
        }

        $scope.stepCounter = $state.params.stepNumber;
        $scope.registration_form = SharedProperties.registration_form;
        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.continuingEducations = $scope.registration_form.continuing_educations;

        function checkContinuingEducationsThatWereAdded(){
            _.each(SharedProperties.registeredAttendee.continuing_educations, function(continuingEducationThatWasAdded){
                _.each($scope.continuingEducations, function(continuingEducation){
                        if(continuingEducationThatWasAdded === continuingEducation.id){
                            continuingEducation.checked = true;
                        }
                });
            });
        }

        if(!_.isEmpty(SharedProperties.registeredAttendee.continuing_educations)) {
            checkContinuingEducationsThatWereAdded();
        }

        $scope.stepBack = function(){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.pre_register_step1');
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.pre_register_step1');
            }
        };

        $scope.resetIframe = function(){
            uncheckAllContinuingEducations();
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;

            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false, edit: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false, edit: false});
            }
        };

        $scope.selectRegisterContinuingEducation = function(ce){
            if(ce.hasOwnProperty('checked')){
                ce.checked = !ce.checked;
            }
            else{
                ce.checked = true;
            }

            if (ce.checked) {
                $scope.registeredAttendee.continuing_educations.push(ce.id);
            } else {
                var index = $scope.registeredAttendee.continuing_educations.indexOf(ce.id);
                if (index > -1) {
                    $scope.registeredAttendee.continuing_educations.splice(index, 1);
                }
            }
        };

        function uncheckAllContinuingEducations(){
          _.each($scope.continuingEducations, function(ce){
              ce.checked = false;
          });
        }

        $scope.goToLastStepRegister = function(){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.registration_last_step');
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.registration_last_step');
            }
        };
    }

})();
