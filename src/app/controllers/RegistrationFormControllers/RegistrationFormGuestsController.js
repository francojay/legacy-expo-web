(function() {
    'use strict';

    var controllerId = 'RegistrationFormGuestsController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', 'redirectToFirstStep',
            registrationFormGuestsController
        ]);

    function registrationFormGuestsController($scope, $state, SharedProperties, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false });
            }
        }
        $scope.stepCounter = $state.params.stepNumber;
        $scope.selectedLevel = SharedProperties.selectedLevel;
        $scope.registration_form = SharedProperties.registration_form;
        $scope.attendeeGuests = SharedProperties.registeredAttendee.attendee_guest_set || [];
        $scope.editMode = $state.params.edit === true ? true : false;
        $scope.individualEdit = $state.params.individualEdit === true ? true : false;
        createAttendeeGuestsObjects();

        $scope.resetIframe = function() {
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;

            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        };

        $scope.stepBack = function() {
            if (verifyAttendeeGuests()) {
                storeGuestsData();
            }
            if (!SharedProperties.hasOwnProperty('registration_adds_on_sessions_avilable')) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step2', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step2', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
                }
            } else if (SharedProperties.hasOwnProperty('registration_adds_on_sessions_avilable')) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step3', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step3', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
                }
            }
        };

        function storeGuestsData() {
            SharedProperties.registeredAttendee.attendee_guest_set = _.filter(SharedProperties.registeredAttendee.attendee_guest_set, function(guest) {
                return !_.isEmpty(guest.first_name) && !_.isEmpty(guest.last_name) && !_.isEmpty(guest.email_address);
            });
            _.uniqWith(SharedProperties.registeredAttendee.attendee_guest_set, _.isEqual);
        }

        $scope.goToPaymentStep = function() {
            if (verifyAttendeeGuests()) {
                storeGuestsData();
                if ($state.params.edit === true) {
                    redirectEditedStep();
                    return;
                }
                SharedProperties.totalPrice += SharedProperties.registeredAttendee.attendee_guest_set.length * $scope.selectedLevel.guest_fee;

                if (SharedProperties.totalPrice > 0 &&
                    !SharedProperties.hasOwnProperty('stripe_token') &&
                    $scope.registration_form.conference.payment_provider === 'stripe'
                ) {
                    if ($state.$current.url.prefix.search('preview') >= 0) {
                        $state.go('preview.step4', { stepNumber: $scope.stepCounter + 1 });
                    } else if ($state.$current.url.prefix.search('registration') >= 0) {
                        $state.go('registration.step4', { stepNumber: $scope.stepCounter + 1 });
                    }
                } else {
                    if ($state.$current.url.prefix.search('preview') >= 0) {
                        $state.go('preview.step5', { stepNumber: ($scope.stepCounter + 1) });
                    } else if ($state.$current.url.prefix.search('registration') >= 0) {
                        $state.go('registration.step5', { stepNumber: ($scope.stepCounter + 1) });
                    }
                }
            }
        };

        function redirectEditedStep() {
            if (SharedProperties.totalPrice > 0 &&
                !SharedProperties.hasOwnProperty('stripe_token') &&
                $scope.registration_form.conference.payment_provider === 'stripe'
            ) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step4', { stepNumber: $scope.stepCounter + 1 });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step4', { stepNumber: $scope.stepCounter + 1 });
                }
            } else {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step5', { stepNumber: ($scope.stepCounter + 2) });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step5', { stepNumber: ($scope.stepCounter + 2) });
                }
            }

        }

        function verifyAttendeeGuests() {
            var ok = true;
            var attendeeGuestObjects = _.cloneDeep($scope.attendeeGuests);
            $scope.validationErrors = [];
            SharedProperties.registeredAttendee.attendee_guest_set = [];
            _.each(attendeeGuestObjects, function(guest) {
                if ((!_.isEmpty(guest.first_name) && !_.isEmpty(guest.last_name) && !_.isEmpty(guest.email_address)) ||
                    (_.isEmpty(guest.first_name) && _.isEmpty(guest.last_name) && _.isEmpty(guest.email_address))) {
                    SharedProperties.registeredAttendee.attendee_guest_set.push(guest);
                } else {
                    ok = false;
                    if (_.isEmpty(guest.first_name)) {
                        $scope.validationErrors.push('First Name is required for Guest ' + (attendeeGuestObjects.indexOf(guest) + 1));
                    }
                    if (_.isEmpty(guest.last_name)) {
                        $scope.validationErrors.push('Last Name is required for Guest ' + (attendeeGuestObjects.indexOf(guest) + 1));
                    }
                    if (_.isEmpty(guest.email_address)) {
                        $scope.validationErrors.push('A valid Email is required for Guest ' + (attendeeGuestObjects.indexOf(guest) + 1));
                    }
                }
            });
            return ok;
        }

        function createAttendeeGuestsObjects() {
            var i = 0;
            var number_of_completed_guests = $scope.attendeeGuests.length;
            while (i !== $scope.selectedLevel.nr_attendee_guests - number_of_completed_guests) {
                $scope.attendeeGuests.push({
                    'first_name': '',
                    'last_name': '',
                    'email_address': ''
                });
                i++;
            }
        }

    }
})();
