(function() {
    'use strict';

    var controllerId = 'RegistrationFormStep5Controller';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$state', '$q', 'ngDialog', 'SharedProperties',
            'ConferenceRegistrationService', 'redirectToFirstStep', '$window',
            registrationFormStep5Controller
        ]);

    function registrationFormStep5Controller($rootScope, $scope, $state, $q, ngDialog, SharedProperties, ConferenceRegistrationService,
        redirectToFirstStep, $window) {
          parent.postMessage({'task': 'scroll_top'}, '*');

        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false });
            }
        }

        $scope.stepCounter                       = $state.params.stepNumber;
        $scope.registration_form                 = SharedProperties.registration_form;
        $scope.registeredAttendee                = SharedProperties.registeredAttendee;
        $scope.attendeeGuestList                 = $scope.registeredAttendee.attendee_guest_set;

        if (SharedProperties.hasOwnProperty('retRegisteredAttendee')) {
            $scope.registeredAttendee = SharedProperties.retRegisteredAttendee;
            SharedProperties.registeredAttendee = SharedProperties.retRegisteredAttendee;
            delete SharedProperties.retRegisteredAttendee;
        }
        if (SharedProperties.hasOwnProperty('registration_order') && !$scope.registration_form) {
            SharedProperties.registration_form.order = SharedProperties.registration_order;
            $scope.registration_form = SharedProperties.registration_form;
            //delete SharedProperties.registration_order;
        }
        if (SharedProperties.registration_form.hasOwnProperty('conference')) {
            $scope.conference = SharedProperties.registration_form.conference;
        }
        $scope.register_add_on_sessions          = SharedProperties.registration_adds_on_selected;
        $scope.selectedLevel                     = SharedProperties.selectedLevel;
        $scope.levelPrice                        = SharedProperties.levelPrice;
        $scope.totalPrice                        = calculateTotalPrice()
        SharedProperties.totalPriceAfterDiscount = SharedProperties.totalPrice;
        $scope.temporaryTotalPrice               = $scope.totalPrice;
        $scope.custom_fields                     = $scope.registeredAttendee.customattendeefieldvalue_set;
        $scope.variables                         = {};
        $scope.variables.conditions_agreed       = false;
        $scope.discountApplied                   = false;
        $scope.promoCode                         = {};
        $scope.promoCodeModel                    = "";
        $scope.subtotalPriceForThisRegister      = $scope.totalPrice;
        $scope.registration_refund_policy        = "";
        $scope.registration_terms_and_conditions = "";
        $scope.validationErrors                  = [];
        if(SharedProperties.fis_errors){
          $scope.validationErrors.push(SharedProperties.fis_errors);
          delete SharedProperties.fis_errors;
        }


        $rootScope.$on('ngDialog.opened', function(e, $dialog) {
            $scope.blurred = 'main-div-container';
        });

        $rootScope.$on('ngDialog.closed', function(e, $dialog) {
            $scope.blurred = false;
            $scope.$apply();
        });
        if ($scope.registration_form.facebook_tracking_code) {
            var pass_data = {
                'func': 'facebook',
                'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                'page': 'Expo_ReviewPayment',
                'event_object': { 'event':$scope.registration_form.conference.code, 'value': $scope.totalPrice }
            };
            parent.postMessage(JSON.stringify(pass_data), "*");
        }
        $scope.resetIframe = function() {
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.registration_adds_on_selected;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;

            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        };

        $scope.stepBack = function() {
            if (SharedProperties.totalPrice > 0 && $scope.registration_form.conference.payment_provider === 'stripe') {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step4', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step4', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            } else if (SharedProperties.selectedLevel.nr_attendee_guests > 0) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step_guests', { stepNumber: SharedProperties.totalPrice > 0 ? ($scope.stepCounter - 2) : ($scope.stepCounter - 1), edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step_guests', { stepNumber: SharedProperties.totalPrice > 0 ? ($scope.stepCounter - 2) : ($scope.stepCounter - 1), edit: false });
                }
            } else if (!_.isEmpty(SharedProperties.registration_adds_on_sessions_avilable)) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step3', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step3', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            } else {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step2', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step2', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            }

        };

        $scope.editThisStep = function(step) {
            var step_number;
            if (step === 'step1') {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step1', { previous: true, edit: true, previousStep: $scope.stepCounter });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step1', { previous: true, edit: true, previousStep: $scope.stepCounter });
                }
            } else {
                if (step === 'step2') {
                    step_number = 2;
                } else if (step === 'step3') {
                    step_number = $scope.stepCounter - 3 + (SharedProperties.selectedLevel.nr_attendee_guests > 0 ? 1 : 0);
                } else if (step === 'step_guests') {
                    step_number = 4;
                }
                var obj = {
                    stepNumber: step_number,
                    edit: true,
                    previousStep: $scope.stepCounter
                };

                if (step_number != 2) {
                    obj.individualEdit = true;
                }
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.' + step, obj);
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.' + step, obj);
                }
            }
        };

        $scope.getHPPage = function() {
            var order = $scope.registration_form.order;
            var conference_id = $scope.registration_form.conference.id;
            order.order_data[0] = $scope.registeredAttendee;

            order.return_url = "https://" + window.location.hostname + "/#!/order-complete/" + $state.params.conference_id + "/y";
            order.payment_mode = 'P';
            return ConferenceRegistrationService.get_FIS_HPPage(conference_id, order);
        };

        $scope.openHPPageModal = function(hpp) {
          if ($scope.registration_form.facebook_tracking_code) {
              var pass_data = {
                  'func': 'facebook',
                  'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                  'page': 'Expo_CCard',
                  'event_object': { 'event': $scope.registration_form.conference.code, 'value': $scope.totalPrice}
              };
              parent.postMessage(JSON.stringify(pass_data), "*");
          }
            ngDialog.open({
                plain: true,
                template: '<hpp-page-modal></hpp-page-modal>',
                className: 'app/directives/hppPageModal/hppPageModalStyle.scss',
                closeByDocument: false,
                showClose: false,
                data: hpp
            });
        };

        $scope.registerAttendee = function() {
            if ($scope.variables.conditions_agreed) {
                if ($scope.registration_form.conference.payment_provider === 'fisglobal' &&
                    SharedProperties.totalPrice > 0 && SharedProperties.totalPriceAfterDiscount > 0
                ) {
                    $scope.validationErrors = [];
                    $scope.totalPrice = $scope.temporaryTotalPrice;
                    $scope.registrationLoader = true;
                    $scope.registeredAttendee.continuing_educations = [];
                    $scope.registeredAttendee.pre_register_session = [];

                    $scope.getHPPage()
                        .then(function(resp) {
                            $scope.registrationLoader = false;
                            $scope.openHPPageModal(resp);
                            SharedProperties.order_type = 'registration';
                            if ($state.$current.url.prefix.search('preview') > 0) SharedProperties.order_type = 'preview';
                        })
                        .catch(function(error) {
                            $scope.registrationLoader = false;
                            $scope.validationErrors.push(error.data.detail);
                        });
                } else {

                    $scope.validationErrors = [];
                    $scope.totalPrice = $scope.temporaryTotalPrice;
                    $scope.registrationLoader = true;
                    $scope.registeredAttendee.continuing_educations = [];
                    $scope.registeredAttendee.pre_register_session = [];

                    ConferenceRegistrationService.registerAttendee($scope.registeredAttendee, $scope.registration_form.conference.id)
                        .then(function(response) {
                            $scope.registrationLoader = false;
                            $scope.registeredAttendee.id = response.attendee_registration.id;
                            $scope.registeredAttendee.access_token = response.temporary_access_token;
                            $scope.registeredAttendee.receipt_url = response.attendee_registration.receipt_url;
                            SharedProperties.promoCode = $scope.promoCode;
                            if ($scope.registration_form.tracking_code) {
                                var pass_data = {
                                    'func': 'analytics',
                                    'tracking_code': $scope.registration_form.tracking_code,
                                    'price': $scope.totalPrice,
                                    'code': $scope.registration_form.conference.code
                                };
                                parent.postMessage(JSON.stringify(pass_data), "*");
                            }
                            if ($scope.registration_form.facebook_tracking_code) {
                                var pass_facebook_data = {
                                    'func': 'facebook',
                                    'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                                    'page': 'Expo_RegistrationComplete',
                                    'event_object': { 'event':$scope.registration_form.conference.code, 'value': $scope.totalPrice, promo_code: $scope.promoCode.code, promo_value: $scope.promoCode.value }
                                };
                                parent.postMessage(JSON.stringify(pass_facebook_data), "*");
                            }
                            goToIntermediaryStep();
                        }).catch(function(error) {
                            $scope.registrationLoader = false;
                            SharedProperties.promoCode = null;
                            if(error.data != undefined){
                              $scope.validationErrors.push(error.data.detail);
                            }
                            else{
                              console.log(error);
                            }
                        });
                }
            } else {
                $scope.validationErrors.push("You must agree to the Terms and Conditions and Refund Policy!");
            }
        };

        $scope.openRefundPolicyPopup = function() {
            var getRegisterFormRefundPolicyFunction = getRegisterFormRefundPolicy();
            getRegisterFormRefundPolicyFunction.then(function(result) {
                ngDialog.open({
                    template: 'app/views/partials/conference/registration-steps/registration_form_refund_policy.html',
                    className: 'app/stylesheets/_registrationFrom6.scss',
                    data: {
                        legal: {
                            refund_policy: result
                        }
                    }
                })
            })
        };

        $scope.openTermsAndConditionsPopup = function() {
            var getRegisterFormTermsAndConditionsFunction = getRegisterFormTermsAndConditions();
            getRegisterFormTermsAndConditionsFunction.then(function(result) {
                ngDialog.open({
                    template: 'app/views/partials/conference/registration-steps/registration_form_terms_and_contidions.html',
                    className: 'app/stylesheets/_registrationFrom6.scss',
                    data: {
                        legal: {
                            terms_of_service: result
                        }
                    }
                });
            });
        };

        $scope.showAddsOnList = true;

        $scope.toggleCarret = function(carretState) {
            $scope.showAddsOnList = !carretState;
        };

        $scope.processPromoCode = function(code) {
            if ($scope.promoCode && $scope.promoCode.code === code) {
                return;
            } else {
                $scope.temporaryTotalPrice = $scope.totalPrice;
            }
            if (!_.isEmpty(code)) {
                $scope.promoCodeLoading = true;
                $scope.promoCodeResult = "";
                $scope.promoCode = null;
                $scope.discountApplied = false;
                ConferenceRegistrationService.processPromoCode(code, $scope.selectedLevel.id)
                    .then(function(response) {
                        $scope.promoCodeLoading = false;
                        $scope.promoCodeResult = response.value;
                        $scope.discountType = response.discount_type;
                        $scope.promoCode = response;
                        $scope.registeredAttendee.promo_code = code;
                        $scope.registeredAttendee.code = code;
                        if ($scope.discountApplied) {
                            $scope.applyDiscount();
                        }
                    }).catch(function(error) {
                        $scope.promoCodeLoading = false;
                        $scope.promoCodeResult = error.data.detail;
                        $scope.promoCode = null;
                        $scope.discountApplied = false;
                        $scope.registeredAttendee.promo_code = "";
                    });
            } else {
                $scope.temporaryTotalPrice = $scope.totalPrice;
                $scope.registeredAttendee.promo_code = "";
                delete $scope.registeredAttendee.code;
                $scope.promoCodeResult = "";
                $scope.promoCode = null;
                $scope.discountApplied = false;
            }
        };

        $scope.applyDiscount = function() {
            if ($scope.promoCode !== null) {
                $scope.temporaryTotalPrice = $scope.totalPrice;
                if ($scope.discountType === 'usd') {
                    $scope.temporaryTotalPrice -= $scope.promoCodeResult;
                } else if ($scope.discountType === 'pct') {
                    $scope.temporaryTotalPrice -= ($scope.temporaryTotalPrice * ($scope.promoCodeResult / 100));
                }
                if ($scope.temporaryTotalPrice < 0) {
                    $scope.temporaryTotalPrice = 0;
                }
                $scope.discountApplied = true;
                $scope.registeredAttendee.promo_code = $scope.promoCode;
                SharedProperties.totalPriceAfterDiscount = $scope.temporaryTotalPrice;
            } else {
                if ($scope.promoCodeLoading) {
                    $scope.discountApplied = true;
                }
            }
        };

        function goToIntermediaryStep() {
            var continuing_education_valability = $scope.selectedLevel.include_continuing_education;
            var pre_registration_valability = $scope.selectedLevel.include_pre_registration;

            if (pre_registration_valability) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.intermediary_step');
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.intermediary_step');
                }
            } else if (continuing_education_valability && $scope.registration_form.continuing_educations.length > 0) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.intermediary_step');
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.intermediary_step');
                }
            } else {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.registration_last_step');
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.registration_last_step');
                }
            }
        }

        function getRegisterFormTermsAndConditions() {
            var deferred = $q.defer();
            if (!$scope.registration_form.terms_of_service) {
                deferred.reject('No registration terms and conditions');
                $scope.registration_terms_and_conditions = null;
            } else if ($scope.registration_terms_and_conditions !== null || !_.isEmpty($scope.registration_terms_and_conditions)) {
                ConferenceRegistrationService
                    .getFileContent($scope.registration_form.terms_of_service)
                    .then(function(response) {
                        $scope.registration_terms_and_conditions = response.data;
                        deferred.resolve(response.data);
                    })
                    .catch(function(error) {
                        deferred.reject(error);
                    });
            } else {
                deferred.resolve($scope.registration_terms_and_conditions);
            }
            return deferred.promise;
        }

        function getRegisterFormRefundPolicy() {
            var deferred = $q.defer();
            if (!$scope.registration_form.refund_policy) {
                deferred.reject('No registration refund policy');
                $scope.registration_refund_policy = null;
            } else if ($scope.registration_refund_policy !== null || !_.isEmpty($scope.registration_refund_policy)) {
                ConferenceRegistrationService
                    .getFileContent($scope.registration_form.refund_policy)
                    .then(function(response) {
                        $scope.registration_refund_policy = response.data;
                        deferred.resolve(response.data);
                    })
                    .catch(function(error) {
                        deferred.reject(error);
                    });
            } else {
                deferred.resolve($scope.registration_refund_policy);
            }
            return deferred.promise;
        }

        function calculateTotalPrice() {
          var price = 0;
          price += $scope.levelPrice;

          if ($scope.register_add_on_sessions && $scope.register_add_on_sessions.length != 0) {
            for (var i = 0; i < $scope.register_add_on_sessions.length; i++) {
              price += $scope.register_add_on_sessions[i].registrationleveladdonsessionpricing_set[0].price;
            };
          }

          if ($scope.attendeeGuestList && $scope.attendeeGuestList.length != 0) {
            price += $scope.selectedLevel.guest_fee * $scope.attendeeGuestList.length;
          }

          return price;
        }
    }
})();
