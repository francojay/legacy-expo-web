(function() {
    'use strict';

    var controllerId = 'RegistrationFormRegisterAnotherController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', 'redirectToFirstStep',
            registrationFormRegisterAnotherController
        ]);

    function registrationFormRegisterAnotherController($scope, $state, SharedProperties, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');
      
        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', {previous: false, edit: false});
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', {previous: false, edit: false});
            }
        }

        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.itemsToDuplicate = [
            {
                label: 'Company Name',
                code: 'company_name'
            },
            {
                label: 'Phone',
                code: 'phone_number'
            },
            {
                label: 'Street Address',
                code: 'street_address'
            },
            {
                label: 'City',
                code: 'city'
            },
            {
                label: 'State',
                code: 'state'
            },
            {
                label: 'Country',
                code: 'country'
            },
            {
                label: 'Zip Code',
                code: 'zip_code'
            }
        ];

        setItemsForDuplicate();

        $scope.resetIframe = function(){
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;

            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false, edit: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false, edit: false});
            }
        };

        $scope.saveDuplicatedSections = function(){
            SharedProperties.registrant_duplicated_sections = _.filter($scope.itemsToDuplicate, function(item){
                return item.checked === true;
            });
            _.each(SharedProperties.registrant_duplicated_sections, function(duplicated_section){
                duplicated_section.data = $scope.registeredAttendee[duplicated_section.code];
            });
            $scope.resetIframe();
        };

        function setItemsForDuplicate(){
            var array = _.cloneDeep($scope.itemsToDuplicate);
            $scope.itemsToDuplicate = _.filter(array, function(duplicated_field){
                return $scope.registeredAttendee[duplicated_field.code];
            });
        }

    }
})();
