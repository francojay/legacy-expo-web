(function() {
    'use strict';

    var controllerId = 'RegistrationFormCompleteController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', 'ConferenceRegistrationService', 'redirectToFirstStep',
            registrationFormCompleteController
        ]);

    function registrationFormCompleteController($scope, $state, SharedProperties, ConferenceRegistrationService, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        }
        $scope.registration_form = SharedProperties.registration_form;
        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.custom_fields = $scope.registeredAttendee.customattendeefieldvalue_set;
        $scope.conference = SharedProperties.registration_form.conference;
        $scope.selectedLevel = SharedProperties.selectedLevel;
        $scope.adds_on_selected = SharedProperties.registration_adds_on_selected;
        $scope.levelPrice = SharedProperties.levelPrice;
        $scope.totalPrice = $scope.totalPriceWithDiscount = SharedProperties.totalPrice;
        $scope.promoCode = SharedProperties.promoCode;
        if ($scope.registration_form.conference.payment_provider === 'fisglobal') {
          $scope.continuing_educations                    = $scope.registeredAttendee.continuing_educations;
          $scope.pre_register_session                     = $scope.registeredAttendee.pre_register_session;
          if(SharedProperties.hasOwnProperty('registration_order')){
            $scope.registeredAttendee                       = SharedProperties.registration_order.registration_data.attendee_registration.attendee;
            $scope.registeredAttendee.attendee_guest_set    = SharedProperties.registration_order.order_data[0].attendee_guest_set;
            $scope.registeredAttendee.id                    = SharedProperties.registration_order.registration_data.attendee_registration.id;
            $scope.registeredAttendee.receipt_url           = SharedProperties.registration_order.registration_data.attendee_registration.receipt_url;
            $scope.registeredAttendee.access_token          = SharedProperties.registration_order.registration_data.temporary_access_token;
            $scope.promoCode                                = SharedProperties.registration_order.registration_data.attendee_registration.promo_code;
            $scope.custom_fields                            = SharedProperties.registration_order.registration_data.attendee_registration.customattendeefieldvalue_set;
          }
          $scope.registeredAttendee.continuing_educations = $scope.continuing_educations;
          $scope.registeredAttendee.pre_register_session  = $scope.pre_register_session;
        }

        $scope.itemsToDuplicate = [{
                label: 'Company Name',
                code: 'company_name'
            },
            {
                label: 'Phone',
                code: 'phone_number'
            },
            {
                label: 'Street Address',
                code: 'street_address'
            },
            {
                label: 'City',
                code: 'city'
            },
            {
                label: 'State',
                code: 'state'
            },
            {
                label: 'Country',
                code: 'country'
            },
            {
                label: 'Zip Code',
                code: 'zip_code'
            }
        ];

        if ($scope.promoCode) {
            if ($scope.promoCode.discount_type === 'usd') {
                $scope.totalPriceWithDiscount -= $scope.promoCode.value;
            } else {
                $scope.totalPriceWithDiscount -= ($scope.totalPriceWithDiscount * ($scope.promoCode.value / 100));
            }

            if ($scope.totalPriceWithDiscount < 0) {
                $scope.totalPriceWithDiscount = 0;
            }
        }

        if (SharedProperties.hasOwnProperty('attendee_adds_on_session_list_with_ticket') && !_.isEmpty(SharedProperties.attendee_adds_on_session_list_with_ticket)) {
            extractSessionObjects(SharedProperties.attendee_adds_on_session_list_with_ticket);
        }
        var hash = new Hashids('', 6);
        $scope.attendee_registered_id = hash.encode($scope.registeredAttendee.id);

        $scope.registrationFormComplete = function(register_another) {
            $scope.registrationLoader = true;
            ConferenceRegistrationService.updateRegisteredAttendee($scope.registeredAttendee)
                .then(
                    function(response) {
                        $scope.registrationLoader = false;
                        if (!register_another) {
                            resetIframe();
                            $scope.registeredAttendee = SharedProperties.registeredAttendee;
                            if ($state.$current.url.prefix.search('preview') >= 0) {
                                $state.go('preview.step1', { previous: false, edit: false });
                            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                $state.go('registration.step1', { previous: false, edit: false });
                            }
                        } else {
                            if ($state.$current.url.prefix.search('preview') >= 0) {
                                $state.go('preview.register_another');
                            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                                $state.go('registration.register_another');
                            }
                        }
                    },
                    function(error) {
                        $scope.validationErrors = [
                            error.data.detail
                        ]
                        $scope.registrationLoader = false;
                    });
        };

        $scope.getUserQrCode = function() {
            return $scope.attendee_registered_id + ';' + $scope.registeredAttendee.first_name + ';' + $scope.registeredAttendee.last_name;
        };

        $scope.printReceipt = function() {
            var new_window = window.open($scope.registeredAttendee.receipt_url);
            new_window.onload = function() {
                new_window.window.print();
            };
        };

        $scope.downloadReceipt = function() {
            var filename = "receipt.pdf";
            var anchor = angular.element('<a/>');
            anchor.css({ display: 'none' }); // Make sure it's not visible
            angular.element(document.body).append(anchor); // Attach to document

            anchor.attr({
                href: $scope.registeredAttendee.receipt_url,
                target: '_blank',
                download: filename
            })[0].click();

            anchor.remove();
        };

        $scope.verifyIfExistsItemsForDuplication = function() {
            var array = _.filter($scope.itemsToDuplicate, function(item) {
                return $scope.registeredAttendee[item.code];
            });
            return !_.isEmpty(array);
        };

        function resetIframe() {
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;
        }

        function extractSessionObjects(attendee_adds_on_session_list_with_ticket) {
            $scope.adds_on_selected = [];
            _.each(attendee_adds_on_session_list_with_ticket, function(add_on_session_added_by_register) {
                var add_on_session = _.find($scope.registration_form.sessions, ['id', add_on_session_added_by_register.session_id]);
                $scope.adds_on_selected.push({
                    'session': add_on_session,
                    'registrationleveladdonsessionpricing_set': [{ 'price': add_on_session_added_by_register.session_price }]
                });
            });
        }
    }
})();
