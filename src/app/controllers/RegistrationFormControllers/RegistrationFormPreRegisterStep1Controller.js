(function() {
    'use strict';

    var controllerId = 'RegistrationFormPreRegisterStep1Controller';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$state', '$window', 'ngDialog', '$timeout', 'SharedProperties', 'redirectToFirstStep',
            registrationFormPreRegisterStep1Controller
        ]);

    function registrationFormPreRegisterStep1Controller($rootScope, $scope, $state, $window, ngDialog, $timeout, SharedProperties, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        $timeout(function() {
            window.getSliderItemsNumber();
        }, 0);

        if(!redirectToFirstStep){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false});
            }
        }

        if($rootScope.eventHandler){
            $rootScope.eventHandler();
        }

        $rootScope.eventHandler = $rootScope.$on('SELECT_PRE_REGISTER_SESSION', function (SELECT_PRE_REGISTER_SESSION, selectedPreRegister) {
            $scope.selectPreRegisterSession(selectedPreRegister.pre_register, selectedPreRegister.hour);
        });

        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            $scope.blurred = 'main-div-container';
        });

        $rootScope.$on('ngDialog.closed', function (e, $dialog) {
            $scope.blurred = false;
            $scope.$apply();
        });

        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.selectedLevel = SharedProperties.selectedLevel;
        $scope.registration_form = SharedProperties.registration_form;
        $scope.conference_time_zone = $scope.registration_form.conference.time_zone;
        $scope.preRegisterCollection = {};
        $scope.preRegisterSelectedDay = {};
        $scope.daysAreSelected = [];
        $scope.indexForPreRegisterSelectedDay = 0;
        $scope.preRegisterSessionsWithoutPrice = _.differenceBy($scope.registration_form.sessions, _.map($scope.selectedLevel.registrationlevelsession_set, 'session'), 'id');
        groupPreRegistersByDates();
        uncheckAllPreRegisterSessions()

        $scope.resetIframe = function(){
            uncheckAllPreRegisterSessions();
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.promoCode;

            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false, edit: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false, edit: false});
            }
        };

        angular.element($window).bind('orientationchange', function() {
            window.getSliderItemsNumber();
            var scrolledDiv = document.getElementById('pre_register_sessions_slider').scrollLeft;
            var element = document.getElementById('pre_register_sessions_slider');
            var maxScrollLeft = element.scrollWidth - element.clientWidth;
            $scope.left_arrow = scrolledDiv < maxScrollLeft;
            $scope.right_arrow = scrolledDiv > 0;
            $scope.$apply();
        });

        $scope.$watch(function(){
            return document.getElementById('pre_register_sessions_slider').scrollLeft;
        }, function(scrolledDiv){
            var element = document.getElementById('pre_register_sessions_slider');
            var maxScrollLeft = element.scrollWidth - element.clientWidth;
            $scope.left_arrow = scrolledDiv < maxScrollLeft;
            $scope.right_arrow = scrolledDiv > 0;
        });

        $scope.goToPreRegisterStep2 = function(){
            SharedProperties.registeredAttendee = $scope.registeredAttendee;
            angular.element($window).unbind('orientationchange');
            if($scope.selectedLevel.include_continuing_education && $scope.registration_form.continuing_educations.length > 0) {
                if($state.$current.url.prefix.search('preview') >= 0){
                    $state.go('preview.pre_register_step2', {stepNumber: 2});
                } else if($state.$current.url.prefix.search('registration') >= 0){
                    $state.go('registration.pre_register_step2', {stepNumber: 2});
                }
            } else{
                if($state.$current.url.prefix.search('preview') >= 0){
                    $state.go('preview.registration_last_step');
                } else if($state.$current.url.prefix.search('registration') >= 0){
                    $state.go('registration.registration_last_step');
                }
            }
        };

        $scope.selectPreRegisterSession = function(pre_register, pre_register_hour){
            if(pre_register.hasOwnProperty('checked') && pre_register.checked === false || !pre_register.hasOwnProperty('checked')) {
                pre_register.checked = true;
                $scope.registeredAttendee.pre_register_session.push(pre_register.id);
                verifySelectedDay(true);
                uncheckOtherPreRegistersFromHour(pre_register_hour, pre_register);

            } else if(pre_register.hasOwnProperty('checked') && pre_register.checked === true){
                pre_register.checked = false;
                var index = $scope.registeredAttendee.pre_register_session.indexOf(pre_register.id);
                if (index > -1) {
                    $scope.registeredAttendee.pre_register_session.splice(index, 1);
                }
                verifySelectedDay(false, pre_register_hour);
            }
        };

        $scope.sanitizeSpeakerUrl = function(url) {
            if (url && url.substring(0, 7) !== "http://" && url.substring(0, 8) !== "https://") {
                return "http://" + url;
            } else if(url && url.search('https://') === 0){
                return url.replace('https://', 'http://');
            }
            return url;
        };

        $scope.openPreRegisterSessionProfile = function(pre_register, hour){
            ngDialog.open({
                template: 'app/views/registration_iframe_steps/pre_register_session_profile.html',
                className: 'app/stylesheets/_registrationFromSessionProfile.scss',
                data:{
                    preRegisterSessionProfile: {
                        session: pre_register,
                        hour: hour,
                        sanitizeSpeakerUrl: $scope.sanitizeSpeakerUrl,
                        conference_time_zone: $scope.conference_time_zone
                    }
                }
            });
        };

        $scope.selectPreRegisterDay = function(selectedDay, index){
            $scope.preRegisterSelectedDay = selectedDay;
            $scope.indexForPreRegisterSelectedDay = index;
        };

        $scope.displayPreRegisterSessionDate = function(day){
            var month, d;
            _.each(day, function(hour){

                month = new Date(hour[0].starts_at);
                var starts_at = new Date(hour[0].starts_at);
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                starts_at.setTime( starts_at.getTime() + (starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                d = starts_at.getDate();
                return false;
            });

            return (month.getMonth() + 1) + '/' + d;
        };

        $scope.displayPreRegisterSessionDay = function(day){
            var d;
            var weekday = new Array(7);
            weekday[0]=  "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            _.each(day, function(hour){

                var starts_at = new Date(hour[0].starts_at);
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                starts_at.setTime( starts_at.getTime() + (starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                d = starts_at.getDay();

                return false;
            });
            return weekday[d];
        };

        $scope.scrollPreRegistersRight = function(){
                $scope.slider_index--;
                var position = document.getElementById('pre_register_sessions_slider').scrollLeft;
                document.getElementById('pre_register_sessions_slider').scrollLeft -= 130;
        };

        $scope.scrollPreRegistersLeft = function(){
                $scope.slider_index++;
                var position = document.getElementById('pre_register_sessions_slider').scrollLeft;
                document.getElementById('pre_register_sessions_slider').scrollLeft += 130;
        };

        function uncheckAllPreRegisterSessions(){
            _.each($scope.preRegisterSessionsWithoutPrice, function(pre_register_session){
                pre_register_session.checked = false;
            });
        }

        function checkPreRegisterSessionsThatWasAdded(pre_registered_sessions_checked_list){
            _.each(pre_registered_sessions_checked_list, function(pre_register_session){
                _.each($scope.preRegisterSessionsWithoutPrice, function(pre_register_that_was_added){
                    if(pre_register_that_was_added.id === pre_register_session){
                        pre_register_that_was_added.checked = true;
                    }
                });
            });
        }

        function groupPreRegistersByDates(){
            if(!_.isEmpty($scope.registeredAttendee.pre_register_session)){
                checkPreRegisterSessionsThatWasAdded($scope.registeredAttendee.pre_register_session);
            }
            var preRegistersGroupByDays = _.groupBy($scope.preRegisterSessionsWithoutPrice, function(pre_register){

                var starts_at = new Date(moment(pre_register.starts_at).format());
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                starts_at.setTime( starts_at.getTime() + (starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                return starts_at.getMonth()+'/'+starts_at.getDate();
            });

            setNumberOfPreRegistersSelectedDays(Object.keys(preRegistersGroupByDays).length); // number of days

            preRegistersGroupByDays = _.sortBy(preRegistersGroupByDays,function(o) {return o[0].starts_at;});

            _.each(preRegistersGroupByDays, function(pre_registers_per_day, day){
                var preRegistersGroupByHours = _.groupBy(pre_registers_per_day, function(pre_register_per_day){
                    var pre_register_date = new Date(moment(pre_register_per_day.starts_at).format());
                    pre_register_date = new Date(pre_register_date.getTime() + pre_register_date.getTimezoneOffset() * 60 * 1000);
                    if(parseInt($scope.conference_time_zone)/100 < 0){
                        return new Date(moment(pre_register_date).add(parseInt($scope.conference_time_zone)/100, 'H').format()).getHours();
                    } else{
                        return new Date(moment(pre_register_date).subtract(parseInt($scope.conference_time_zone)/100, 'H').format()).getHours();
                    }
                });

                preRegistersGroupByHours = _.sortBy(preRegistersGroupByHours,function(o) {return o[0].starts_at;});
                if(_.isEmpty($scope.preRegisterSelectedDay)){
                    $scope.preRegisterSelectedDay = preRegistersGroupByHours;
                }
                
                $scope.preRegisterCollection[day] = preRegistersGroupByHours;
            });
        }

        function setNumberOfPreRegistersSelectedDays(length){
            var index_day = 0;
            while(index_day !== length){
                $scope.daysAreSelected[index_day] = false;
                index_day++;
            }
        }

        function uncheckOtherPreRegistersFromHour(pre_register_hour, selected_pre_register) {
          var selectedPreRegisterStartTime = moment(selected_pre_register.starts_at);
          var selectedPreRegisterEndTime   = moment(selected_pre_register.ends_at);
            _.each(pre_register_hour, function(pre_register) {
                if(pre_register.id !== selected_pre_register.id && pre_register.checked === true) { //if this is not the session that was clicked and if it was previously checked
                  var checkedPreRegisterStartTime = moment(pre_register.starts_at);
                  var checkedPreRegisterEndTime   = moment(pre_register.ends_at);

                  var startOfSelectedPreRegisterIsBetweenTimesOfCheckedPreRegister = selectedPreRegisterStartTime.isBetween(checkedPreRegisterStartTime, checkedPreRegisterEndTime);
                  var endOfSelectedPreRegisterIsBetweenTimesOfCheckedPreRegister   = selectedPreRegisterEndTime.isBetween(checkedPreRegisterStartTime, checkedPreRegisterEndTime);
                  if (startOfSelectedPreRegisterIsBetweenTimesOfCheckedPreRegister || endOfSelectedPreRegisterIsBetweenTimesOfCheckedPreRegister) {
                    pre_register.checked = false;
                    var index = $scope.registeredAttendee.pre_register_session.indexOf(pre_register.id);
                    if (index > -1) {
                        $scope.registeredAttendee.pre_register_session.splice(index, 1);
                    }
                  }
                }
            });
        }

        function verifySelectedDay(bool){
            if(bool){
                $scope.daysAreSelected[$scope.indexForPreRegisterSelectedDay] = true;
            } else{
                var verifyIfSelected;
                var ok = false;
                _.each($scope.preRegisterSelectedDay, function(hour){
                    verifyIfSelected = _.find(hour, ['checked', true]);
                    if(verifyIfSelected !== undefined){
                        ok = true;
                        $scope.daysAreSelected[$scope.indexForPreRegisterSelectedDay] = true;
                        return false;
                    }
                });
                if(!ok){
                    $scope.daysAreSelected[$scope.indexForPreRegisterSelectedDay] = false;
                }
            }
        }

        window.getSliderItemsNumber = function(){
            var container_width = document.getElementById('pre_register_sessions_slider_container').clientWidth;
            $scope.nr_slider_items = Math.floor(container_width/130);
            $scope.leftGrayedOut = (Math.ceil((document.getElementById('pre_register_sessions_slider').scrollLeft) / 130)) - 2;
            $scope.rightGrayedOut = (Math.floor((document.getElementById('pre_register_sessions_slider').scrollLeft + container_width) / 130)) - 1;
            $scope.$apply();
        };

        window.scrollAddsOn = function() {
            var scrollIndex = document.getElementById('pre_register_sessions_slider').scrollLeft;
            var container_width = document.getElementById('pre_register_sessions_slider_container').clientWidth;
            $scope.nr_slider_items = Math.floor(container_width/130);
            $scope.leftGrayedOut = Math.ceil(scrollIndex / 130) - 2;
            $scope.rightGrayedOut = Math.floor((scrollIndex + container_width) / 130) - 1;
            $scope.$apply();
        };
    }
})();
