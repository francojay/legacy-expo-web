(function() {
    'use strict';

    var controllerId = 'RegistrationFormStep3Controller';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$rootScope', '$window', 'ngDialog', '$state', 'SharedProperties', '$timeout', 'redirectToFirstStep',
            registrationFormStep3Controller
        ]);

    function registrationFormStep3Controller($scope, $rootScope, $window, ngDialog, $state, SharedProperties, $timeout, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        $timeout(function() {
            window.getSliderItemsNumber();
        }, 0);

        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        }


        $rootScope.$on('ngDialog.opened', function(e, $dialog) {
            $scope.blurred = 'main-div-container';
        });

        $rootScope.$on('ngDialog.closed', function(e, $dialog) {
            $scope.blurred = false;
            $scope.$apply();
        });

        angular.element($window).bind('orientationchange', function() {
            window.getSliderItemsNumber();
            var scrolledDiv = document.getElementById('adds_on_slider').scrollLeft;
            var element = document.getElementById('adds_on_slider');
            var maxScrollLeft = element.scrollWidth - element.clientWidth;
            $scope.left_arrow = scrolledDiv < maxScrollLeft;
            $scope.right_arrow = scrolledDiv > 0;
            $scope.$apply();
        });

        $scope.$watch(function() {
            return document.getElementById('adds_on_slider').scrollLeft;
        }, function(scrolledDiv) {
            var element = document.getElementById('adds_on_slider');
            var maxScrollLeft = element.scrollWidth - element.clientWidth;
            $scope.left_arrow = scrolledDiv < maxScrollLeft;
            $scope.right_arrow = scrolledDiv > 0;
        });

        if ($rootScope.eventHandler) {
            $rootScope.eventHandler();
        }

        $rootScope.eventHandler = $rootScope.$on('SELECT_ADD_ON_SESSION', function(SELECT_ADD_ON_SESSION, selectedAddOn) {
            $scope.selectRegisterAddOnSession(selectedAddOn.add_on, selectedAddOn.hour);
        });

        $scope.registration_form = SharedProperties.registration_form;
        $scope.stepCounter = $state.params.stepNumber;
        $scope.conference_time_zone = SharedProperties.registration_form.conference.time_zone;
        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.registration_adds_on_sessions_avilable = angular.copy(SharedProperties.registration_adds_on_sessions_avilable);
        $scope.levelPrice = SharedProperties.levelPrice;
        $scope.addsOnTotalPrice = SharedProperties.addsOnTotalPrice || 0;
        $scope.totalPrice = $scope.levelPrice + $scope.addsOnTotalPrice;
        $scope.registeredAttendee.pre_register_add_on_session = [];
        $scope.selectedLevel = SharedProperties.selectedLevel;
        $scope.addsOnCollection = {};
        $scope.addOnSelectedDay = {};
        $scope.daysAreSelected = [];
        $scope.indexForAddOnSelectedDay = 0;
        $scope.editMode = $state.params.edit === true ? true : false;
        $scope.individualEdit = $state.params.individualEdit === true ? true : false;

        groupAddsOnByDates();

        if ($scope.registration_form.facebook_tracking_code) {
            var pass_data = {
                'func': 'facebook',
                'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                'page': 'Expo_AddOnSession',
                'event_object': { 'event': $scope.registration_form.conference.code}
            };
            parent.postMessage(JSON.stringify(pass_data), "*");
        }
        $scope.goToStep4 = function() {
            SharedProperties.totalPrice = $scope.totalPrice;
            SharedProperties.addsOnTotalPrice = $scope.addsOnTotalPrice;
            angular.element($window).unbind('orientationchange');
            SharedProperties.registration_adds_on_selected = _.filter($scope.registration_adds_on_sessions_avilable, ['checked', true]);
            if ($state.params.edit === true) {
                redirectEditedStep();
                return;
            }
            if ($scope.selectedLevel.nr_attendee_guests > 0) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step_guests', { stepNumber: 4, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step_guests', { stepNumber: 4, edit: false });
                }
            } else if ($scope.totalPrice != 0 && $scope.registration_form.conference.payment_provider === 'stripe') {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step4', { stepNumber: 4 });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step4', { stepNumber: 4 });
                }
            } else {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step5', { stepNumber: 4 });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step5', { stepNumber: 4 });
                }
            }
        };

        $scope.sanitizeSpeakerUrl = function(url) {
            if (url && url.substring(0, 7) !== "http://" && url.substring(0, 8) !== "https://") {
                return "http://" + url;
            } else if (url && url.search('https://') === 0) {
                return url.replace('https://', 'http://');
            }
            return url;
        };

        $scope.resetIframe = function() {
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.registration_adds_on_selected;
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false });
            }
        };

        $scope.stepBack = function() {
            SharedProperties.addsOnTotalPrice = 0;
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step2', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step2', { stepNumber: $scope.stepCounter - 1, edit: $state.params.edit });
            }
        };

        $scope.selectRegisterAddOnSession = function(add_on, add_on_hour) {
            if (add_on.hasOwnProperty('checked') && add_on.checked === false || !add_on.hasOwnProperty('checked')) {
                add_on.checked = true;
                $scope.registeredAttendee.pre_register_add_on_session.push(add_on.session.id);
                if ($scope.registration_form.facebook_tracking_code) {
                    var pass_data = {
                        'func': 'facebook',
                        'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                        'page': 'Expo_AddOnSelect',
                        'event_object': { 'event': $scope.registration_form.conference.code, 'session': add_on.session.title, 'value': add_on.registrationleveladdonsessionpricing_set[0].price}
                    };
                    parent.postMessage(JSON.stringify(pass_data), "*");
                }
                verifySelectedDay(true);
                uncheckOtherAddsOnFromHour(add_on_hour, add_on);
                setTotalPrice(true, add_on);

            } else if (add_on.hasOwnProperty('checked') && add_on.checked === true) {
                add_on.checked = false;
                var index = $scope.registeredAttendee.pre_register_add_on_session.indexOf(add_on.session.id);
                if (index > -1) {
                    $scope.registeredAttendee.pre_register_add_on_session.splice(index, 1);
                }
                verifySelectedDay(false);
                setTotalPrice(false, add_on);
            }
        };

        $scope.openAddOnSessionProfile = function(add_on, hour) {
            var price = add_on.registrationleveladdonsessionpricing_set[0].price;
            ngDialog.open({
                template: 'app/views/registration_iframe_steps/add_on_profile.html',
                className: 'app/stylesheets/_registrationFromSessionProfile.scss',
                data: {
                    addOnSessionProfile: {
                        add_on: add_on,
                        session_price: price,
                        session: add_on.session,
                        hour: hour,
                        sanitizeSpeakerUrl: $scope.sanitizeSpeakerUrl,
                        conference_time_zone: $scope.conference_time_zone
                    }
                }
            });
        };

        $scope.selectAddOnDay = function(selectedDay, index) {
            $scope.addOnSelectedDay = selectedDay;
            $scope.indexForAddOnSelectedDay = index;
        };

        $scope.displayAddOnSessionDate = function(day) {
            var month, d;
            _.each(day, function(hour) {
                month = new Date(hour[0].session.starts_at).getMonth() + 1;
                var starts_at = new Date(hour[0].session.starts_at);
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                starts_at.setTime(starts_at.getTime() + (starts_at.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);
                d = starts_at.getDate();
                return false;
            });

            return month + '/' + d;
        };

        $scope.displayAddOnSessionDay = function(day) {
            var d;
            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            _.each(day, function(hour) {
                var starts_at = new Date(hour[0].session.starts_at);
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                starts_at.setTime(starts_at.getTime() + (starts_at.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);

                d = starts_at.getDay();
                return false;
            });
            return weekday[d];
        };

        $scope.scrollAddsOnRight = function() {
            document.getElementById('adds_on_slider').scrollLeft -= 130;
        };

        $scope.scrollAddsOnLeft = function() {
            document.getElementById('adds_on_slider').scrollLeft += 130;
        };

        function redirectEditedStep() {
            if ($scope.selectedLevel.nr_attendee_guests > 0 && !$state.params.individualEdit) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step_guests', { stepNumber: 4, edit: true });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step_guests', { stepNumber: 4, edit: true });
                }
            } else {
                if (SharedProperties.totalPrice > 0 &&
                    !SharedProperties.hasOwnProperty('stripe_token') &&
                    $scope.registration_form.conference.payment_provider === 'stripe'
                ) {
                    if ($state.$current.url.prefix.search('preview') >= 0) {
                        $state.go('preview.step4', { stepNumber: $scope.stepCounter + 1 });
                    } else if ($state.$current.url.prefix.search('registration') >= 0) {
                        $state.go('registration.step4', { stepNumber: $scope.stepCounter + 1 });
                    }
                } else {
                    if ($state.$current.url.prefix.search('preview') >= 0) {
                        $state.go('preview.step5', { stepNumber: ($scope.stepCounter + 2) });
                    } else if ($state.$current.url.prefix.search('registration') >= 0) {
                        $state.go('registration.step5', { stepNumber: ($scope.stepCounter + 2) });
                    }
                }

            }
        }

        function setTotalPrice(bool, add_on) {
            if (bool) {
                $scope.totalPrice += add_on.registrationleveladdonsessionpricing_set[0].price;
                $scope.addsOnTotalPrice += add_on.registrationleveladdonsessionpricing_set[0].price;
            } else {
                $scope.totalPrice -= add_on.registrationleveladdonsessionpricing_set[0].price;
                $scope.addsOnTotalPrice -= add_on.registrationleveladdonsessionpricing_set[0].price;
            }
        }

        function groupAddsOnByDates() {
            var adds_on = $scope.registration_adds_on_sessions_avilable;
            var addsOnGroupByDays = _.groupBy(adds_on, function(add_on) {
                var offsetInMinutes = lib.timezoneToOffset($scope.conference_time_zone);
                var addOnDate = new Date(add_on.session.starts_at)
                addOnDate.setTime(addOnDate.getTime() + (addOnDate.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);
                return addOnDate.getDay() - 1;
            });
            addsOnGroupByDays = _.sortBy(addsOnGroupByDays, function(o) { return o[0].session.starts_at; });
            var add_on_already_selected = false;
            var index_of_day_already_selected;

            setNumberOfAddsOnSelectedDays(Object.keys(addsOnGroupByDays).length); // number of days

            _.each(addsOnGroupByDays, function(adds_on_per_day, day) {
                var addsOnGroupByHours = _.groupBy(adds_on_per_day, function(add_on_per_day) {
                    if (add_on_per_day.checked === true) {
                        add_on_already_selected = true;
                        index_of_day_already_selected = _.indexOf(Object.keys(addsOnGroupByDays), day);
                    }
                    var add_on_date = new Date(moment(add_on_per_day.session.starts_at).format());
                    add_on_date = new Date(add_on_date.getTime() + add_on_date.getTimezoneOffset() * 60 * 1000);
                    if (parseInt($scope.conference_time_zone) / 100 < 0) {
                        return new Date(moment(add_on_date).add(parseInt($scope.conference_time_zone) / 100, 'H').format()).getHours();
                    } else {
                        return new Date(moment(add_on_date).subtract(parseInt($scope.conference_time_zone) / 100, 'H').format()).getHours();
                    }
                });

                addsOnGroupByHours = _.sortBy(addsOnGroupByHours, function(o) { return o[0].session.starts_at; });
                if (_.isEmpty($scope.addOnSelectedDay)) {
                    $scope.addOnSelectedDay = addsOnGroupByHours;
                }
                $scope.addsOnCollection[day] = addsOnGroupByHours;

                if (add_on_already_selected) {
                    verifySelectedDay(true, index_of_day_already_selected);
                }
            });
        }

        function setNumberOfAddsOnSelectedDays(length) {
            var index_day = 0;
            while (index_day !== length) {
                $scope.daysAreSelected[index_day] = false;
                index_day++;
            }
        }

        function uncheckOtherAddsOnFromHour(add_on_hour, selected_add_on) {
          var selectedAddonStartTime = moment(selected_add_on.session.starts_at);
          var selectedAddonEndTime   = moment(selected_add_on.session.ends_at);
            _.each(add_on_hour, function(add_on) {
                if (add_on.id !== selected_add_on.id && add_on.checked === true) { //if this is not the session that was clicked and if it was previously checked
                  var checkedAddonStartTime = moment(add_on.session.starts_at);
                  var checkedAddonEndTime   = moment(add_on.session.ends_at);

                  var startOfSelectedAddonIsBetweenTimesOfCheckedAddon = selectedAddonStartTime.isBetween(checkedAddonStartTime, checkedAddonEndTime);
                  var endOfSelectedAddonIsBetweenTimesOfCheckedAddon   = selectedAddonEndTime.isBetween(checkedAddonStartTime, checkedAddonEndTime);

                  if (startOfSelectedAddonIsBetweenTimesOfCheckedAddon || endOfSelectedAddonIsBetweenTimesOfCheckedAddon) {
                    add_on.checked = false;
                    var index = $scope.registeredAttendee.pre_register_add_on_session.indexOf(add_on.session.id);
                    if (index > -1) {
                        $scope.registeredAttendee.pre_register_add_on_session.splice(index, 1);
                    }
                    setTotalPrice(false, add_on);
                  }
                }
            });
        }

        function verifySelectedDay(bool, index) {
            if (bool) {
                if (index) {
                    $scope.daysAreSelected[index] = true;
                } else {
                    $scope.daysAreSelected[$scope.indexForAddOnSelectedDay] = true;
                }
            } else {
                var verifyIfSelected;
                var ok = false;
                _.each($scope.addOnSelectedDay, function(hour) {
                    verifyIfSelected = _.find(hour, ['checked', true]);
                    if (verifyIfSelected !== undefined) {
                        ok = true;
                        return false;
                    }
                });
                if (!ok) {
                    $scope.daysAreSelected[$scope.indexForAddOnSelectedDay] = false;
                }
            }
        }

        window.getSliderItemsNumber = function() {
            var container_width = document.getElementById('adds_on_slider_container').clientWidth;
            $scope.nr_slider_items = Math.floor(container_width / 130);
            $scope.leftGrayedOut = (Math.ceil((document.getElementById('adds_on_slider').scrollLeft) / 130)) - 2;
            $scope.rightGrayedOut = (Math.floor((document.getElementById('adds_on_slider').scrollLeft + container_width) / 130)) - 1;
            $scope.$apply();
        };

        window.scrollAddsOn = function() {
            var scrollIndex = document.getElementById('adds_on_slider').scrollLeft;
            var container_width = document.getElementById('adds_on_slider_container').clientWidth;
            $scope.nr_slider_items = Math.floor(container_width / 130);
            $scope.leftGrayedOut = Math.ceil(scrollIndex / 130) - 2;
            $scope.rightGrayedOut = Math.floor((scrollIndex + container_width) / 130) - 1;
            $scope.$apply();
        };
    }
})();
