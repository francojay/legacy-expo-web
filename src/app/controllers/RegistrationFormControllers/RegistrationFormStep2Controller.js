(function() {
    'use strict';

    var controllerId = 'RegistrationFormStep2Controller';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', '$timeout', '$window', 'redirectToFirstStep',
            'ConferenceRegistrationService',
            registrationFromStep2Controller
        ]);

    function registrationFromStep2Controller($scope, $state, SharedProperties, $timeout, $window, redirectToFirstStep, ConferenceRegistrationService) {
      parent.postMessage({'task': 'scroll_top'}, '*');

      if (!redirectToFirstStep) {
          if ($state.$current.url.prefix.search('preview') >= 0) {
              $state.go('preview.step1', { previous: false });
          } else if ($state.$current.url.prefix.search('registration') >= 0) {
              $state.go('registration.step1', { previous: false });
          }
      }

        $timeout(function() {
            getSliderItemsNumber();
        }, 0);

        angular.element($window).bind('orientationchange', function() {
            getSliderItemsNumber();
        });

        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.registration_form = SharedProperties.registration_form;
        $scope.registration_levels = SharedProperties.registration_levels;
        $scope.selectedLevel = $scope.registration_levels[0];
        $scope.currentLevel = 0;
        $scope.stepCounter = $state.params.stepNumber;
        $scope.editMode = $state.params.edit === true ? true : false;
        $scope.selectThisLevel = function(index) {
            $scope.currentLevel = index;
            $scope.selectedLevel = $scope.registration_levels[$scope.currentLevel];
            if ($scope.registration_form.facebook_tracking_code) {
                var pass_data = {
                    'func': 'facebook',
                    'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                    'page': 'Expo_LevelSelect',
                    'event_object': { 'event': $scope.registration_form.conference.code, 'level': $scope.selectedLevel.name, 'value': $scope.standardPrices($scope.selectedLevel)}
                };
                parent.postMessage(JSON.stringify(pass_data), "*");
            }
        };

        if ($scope.registration_form.facebook_tracking_code) {
            var pass_data = {
                'func': 'facebook',
                'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                'page': 'Expo_LevelLoad',
                'event_object': { 'event': $scope.registration_form.conference.code}
            };
            parent.postMessage(JSON.stringify(pass_data), "*");
        }

        $scope.resetIframe = function() {
            uncheckAllAddsOn();
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.registration_adds_on_selected;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        };

        $scope.stepBack = function() {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: true, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: true, edit: false });
            }
        };

        $scope.updateRegistrationOrder = function() {
            var order = $scope.registration_form.order;
            var registration_level = $scope.selectedLevel.id;
            var conference_id = $scope.registration_form.conference.id;
            var attendee_registrations = order.order_data;
            _.each(attendee_registrations, function(x) {
                x.registration_level = $scope.selectedLevel.id;
            });
            var data = {
                "attendee_registrations": attendee_registrations
            };

            return ConferenceRegistrationService.updateRegistrationOrder(conference_id, order, data);
        };

        $scope.goToStep3 = function() {
            $scope.saveLevelChoice();
            if (SharedProperties.selectedLevel !== undefined && SharedProperties.selectedLevel.id !== $scope.selectedLevel.id) {
                uncheckAllAddsOn();
                delete SharedProperties.totalPrice;
                delete SharedProperties.levelPrice;
                delete SharedProperties.addsOnTotalPrice;
                delete SharedProperties.registration_adds_on_sessions_avilable;
                delete SharedProperties.registration_adds_on_selected;
            } else if (SharedProperties.hasOwnProperty('selectedLevel') && $state.params.edit === true) {
                redirectEditedStep();
                return;
            }
            SharedProperties.selectedLevel = $scope.selectedLevel;
            SharedProperties.registeredAttendee = $scope.registeredAttendee;
            $scope.updateRegistrationOrder()
                .then(function(order) {
                    $scope.registration_form.order = order;

                    if (getAvailableAddsOn()) {
                        SharedProperties.levelPrice = $scope.standardPrices($scope.selectedLevel);
                        SharedProperties.registration_adds_on_sessions_avilable = $scope.registration_adds_on_sessions_avilable;
                        if ($state.$current.url.prefix.search('preview') >= 0) {
                            $state.go('preview.step3', { stepNumber: 3, edit: $state.params.edit });
                        } else if ($state.$current.url.prefix.search('registration') >= 0) {
                            $state.go('registration.step3', { stepNumber: 3, edit: $state.params.edit });
                        }
                    } else if ($scope.selectedLevel.nr_attendee_guests > 0) {
                        SharedProperties.levelPrice = $scope.standardPrices($scope.selectedLevel);
                        SharedProperties.totalPrice = SharedProperties.levelPrice;
                        SharedProperties.registeredAttendee.pre_register_add_on_session = [];

                        if ($state.$current.url.prefix.search('preview') >= 0) {
                            $state.go('preview.step_guests', { stepNumber: 3, edit: $state.params.edit });
                        } else if ($state.$current.url.prefix.search('registration') >= 0) {
                            $state.go('registration.step_guests', { stepNumber: 3, edit: $state.params.edit });
                        }
                    } else if ($scope.standardPrices($scope.selectedLevel) !== 0 &&
                        !SharedProperties.registeredAttendee.hasOwnProperty('stripe_token') &&
                        $scope.registration_form.conference.payment_provider === 'stripe'
                    ) {
                        SharedProperties.registeredAttendee.pre_register_add_on_session = [];
                        SharedProperties.levelPrice = $scope.standardPrices($scope.selectedLevel);
                        SharedProperties.totalPrice = SharedProperties.levelPrice;
                        if ($state.$current.url.prefix.search('preview') >= 0) {
                            $state.go('preview.step4', { stepNumber: 3 });
                        } else if ($state.$current.url.prefix.search('registration') >= 0) {
                            $state.go('registration.step4', { stepNumber: 3 });
                        }
                    } else if ($scope.standardPrices($scope.selectedLevel) !== 0 &&
                        $scope.registration_form.conference.payment_provider === 'fisglobal'
                    ) {
                        SharedProperties.registeredAttendee.pre_register_add_on_session = [];
                        SharedProperties.levelPrice = $scope.standardPrices($scope.selectedLevel);
                        SharedProperties.totalPrice = SharedProperties.levelPrice;
                        if ($state.$current.url.prefix.search('preview') >= 0) {
                            $state.go('preview.step5', { stepNumber: 3 });
                        } else if ($state.$current.url.prefix.search('registration') >= 0) {
                            $state.go('registration.step5', { stepNumber: 3 });
                        }
                    } else if ($scope.standardPrices($scope.selectedLevel) === 0 || $state.params.edit === true) {
                        SharedProperties.registeredAttendee.pre_register_add_on_session = [];
                        SharedProperties.levelPrice = $scope.standardPrices($scope.selectedLevel);
                        SharedProperties.totalPrice = SharedProperties.levelPrice;
                        if ($state.$current.url.prefix.search('preview') >= 0) {
                            $state.go('preview.step5', { stepNumber: 3 });
                        } else if ($state.$current.url.prefix.search('registration') >= 0) {
                            $state.go('registration.step5', { stepNumber: 3 });
                        }
                    }
                })
                .catch(function(error) {
                });
        };

        $scope.saveLevelChoice = function() {
            $scope.registration_adds_on_sessions_avilable = [];
            $scope.registeredAttendee.registration_level = $scope.selectedLevel.id;
        };

        function redirectEditedStep() {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step5', { stepNumber: $state.params.previousStep });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step5', { stepNumber: $state.params.previousStep });
            }
        }

        function getAvailableAddsOn() {
            var adds_on = $scope.selectedLevel.registrationleveladdonsession_set;
            var ok = false;

            if (adds_on.length > 0) {
                _.each(adds_on, function(add_on) {
                    _.each(add_on.registrationleveladdonsessionpricing_set, function(add_on_price) {
                        if (getPriceAvailability(add_on_price)) {
                            ok = true;
                            setOnlyOnePricePerAddOn(add_on, add_on_price);
                        }
                    });
                });
            }

            return ok;
        }

        function setOnlyOnePricePerAddOn(add_on, add_on_price) {
            add_on.registrationleveladdonsessionpricing_set = [add_on_price];
            $scope.registration_adds_on_sessions_avilable.push(add_on);
        }

        function getPriceAvailability(add_on_price) {
            var price_start = new Date(add_on_price.starts_at);
            var price_end = new Date(add_on_price.ends_at);
            var current_date = new Date();

            return price_start <= current_date && price_end >= current_date;
        }

        $scope.scrollLevelsRight = function() {
            $scope.slider_index--;
            document.getElementById('level_slider').scrollLeft -= 130;
        };

        $scope.scrollLevelsLeft = function() {
            $scope.slider_index++;
            document.getElementById('level_slider').scrollLeft += 130;
        };

        $scope.$watch(function() {
            if (document.getElementById('level_slider')) {
                return document.getElementById('level_slider').scrollLeft;
            }
            return 0;
        }, function(scrolledDiv) {
            var element = document.getElementById('level_slider');
            if (element) {
                var maxScrollLeft = element.scrollWidth - element.clientWidth;
                $scope.left_arrow = scrolledDiv < maxScrollLeft;
                $scope.right_arrow = scrolledDiv > 0;
            }
        });

        $scope.standardPrices = function(level) {
            if (level.registrationlevelpricing_set.length !== 0) {
                var currentDate = new Date();
                var availablePrice;
                _.each(level.registrationlevelpricing_set, function(level_price_item) {
                    if (!_.isNull(level_price_item.starts_at) && !_.isNull(level_price_item.ends_at)) {
                        if (currentDate >= new Date(level_price_item.starts_at) && currentDate <= new Date(level_price_item.ends_at)) {
                            availablePrice = level_price_item.price;
                            return false;
                        }
                    }
                });
                if (availablePrice !== undefined) {
                    return availablePrice;
                } else {
                    var standardPrice = _.find(level.registrationlevelpricing_set, function(level_price_item) {
                        return _.isNull(level_price_item.starts_at) && _.isNull(level_price_item.ends_at);
                    });
                    if (standardPrice !== undefined) {
                        return standardPrice.price;
                    } else {
                        return 0;
                    }
                }
            } else {
                return 0;
            }
        };

        function uncheckAllAddsOn() {
            _.each($scope.registration_levels, function(level) {
                _.each(level.registrationleveladdonsession_set, function(add_on) {
                    add_on.checked = false;
                })
            });
        }

        function getSliderItemsNumber() {
            var container_width = document.getElementById('level_slider').clientWidth;
            $scope.nr_slider_items = Math.ceil(container_width / 130);
            $scope.leftGrayedOut = (Math.ceil((document.getElementById('level_slider').scrollLeft) / 130)) - 2;
            $scope.rightGrayedOut = (Math.floor((document.getElementById('level_slider').scrollLeft + container_width) / 130)) - 1;
            if($scope.leftGrayedOut){
              $scope.left_arrow = true;
            }
            $scope.$apply();
        }

        window.scrollLevels = function() {
            var scrollIndex = document.getElementById('level_slider').scrollLeft;
            var container_width = document.getElementById('level_slider').clientWidth;
            $scope.nr_slider_items = Math.floor(container_width / 130);
            $scope.leftGrayedOut = Math.ceil(scrollIndex / 130) - 2;
            $scope.rightGrayedOut = Math.floor((scrollIndex + container_width) / 130) - 1;
            $scope.$apply();
        }
    }
})();
