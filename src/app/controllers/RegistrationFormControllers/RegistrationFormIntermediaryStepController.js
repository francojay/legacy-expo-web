(function() {
    'use strict';

    var controllerId = 'RegistrationFormIntermediaryStepController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'SharedProperties', 'redirectToFirstStep',
            registrationFormIntermediaryStepController
        ]);

    function registrationFormIntermediaryStepController($scope, $state, SharedProperties, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');
      
        if(!redirectToFirstStep){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.step1', {previous: false, edit: false});
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.step1', {previous: false, edit: false});
            }
        }
        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.registeredAttendeeEmail = $scope.registeredAttendee.email_address;
        $scope.selectedLevel = SharedProperties.selectedLevel;
        $scope.registration_form = SharedProperties.registration_form;
        $scope.itemsToDuplicate = [
            {
                label: 'Company Name',
                code: 'company_name'
            },
            {
                label: 'Phone',
                code: 'phone_number'
            },
            {
                label: 'Street Address',
                code: 'street_address'
            },
            {
                label: 'City',
                code: 'city'
            },
            {
                label: 'State',
                code: 'state'
            },
            {
                label: 'Country',
                code: 'country'
            },
            {
                label: 'Zip Code',
                code: 'zip_code'
            }
        ];

        $scope.registerAnother = function(){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.register_another');
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.register_another');
            }
        };

        $scope.goToPreRegisterStep1 = function(){
            var continuing_education_valability = $scope.selectedLevel.include_continuing_education;
            var pre_registration_valability = $scope.selectedLevel.include_pre_registration;

            if(pre_registration_valability){
                if($state.$current.url.prefix.search('preview') >= 0){
                    $state.go('preview.pre_register_step1');
                } else if($state.$current.url.prefix.search('registration') >= 0){
                    $state.go('registration.pre_register_step1');
                }
            } else if(continuing_education_valability && $scope.registration_form.continuing_educations.length > 0){
                if($state.$current.url.prefix.search('preview') >= 0){
                    $state.go('preview.pre_register_step2', {stepNumber: 1});
                } else if($state.$current.url.prefix.search('registration') >= 0){
                    $state.go('registration.pre_register_step2', {stepNumber: 1});
                }
            } else{
                $state.go('preview.registration_last_step');
                if($state.$current.url.prefix.search('preview') >= 0){
                    $state.go('preview.registration_last_step');
                } else if($state.$current.url.prefix.search('registration') >= 0){
                    $state.go('registration.registration_last_step');
                }
            }
        };

        $scope.skipPreRegister = function(){
            if($state.$current.url.prefix.search('preview') >= 0){
                $state.go('preview.registration_last_step');
            } else if($state.$current.url.prefix.search('registration') >= 0){
                $state.go('registration.registration_last_step');
            }
        };

        $scope.verifyIfExistsItemsForDuplication = function(){
            var array = _.filter($scope.itemsToDuplicate, function(item){
                return $scope.registeredAttendee[item.code];
            });
            return !_.isEmpty(array);

        }
    }
})();
