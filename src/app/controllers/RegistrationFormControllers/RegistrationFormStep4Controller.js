(function() {
    'use strict';

    var controllerId = 'RegistrationFormStep4Controller';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$rootScope', '$state', 'SharedProperties', 'API', 'redirectToFirstStep',
            registrationFormStep4Controller
        ]);

    function registrationFormStep4Controller($scope, $rootScope, $state, SharedProperties, API, redirectToFirstStep) {
      parent.postMessage({'task': 'scroll_top'}, '*');

        if (!redirectToFirstStep) {
            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false });
            }
        }

        $scope.registration_form = SharedProperties.registration_form;

        $scope.stepCounter = $state.params.stepNumber;
        $scope.registrationLoader = false;
        $scope.expirationDate = '';
        $scope.registeredAttendee = SharedProperties.registeredAttendee;
        $scope.validationErrors = [];
        if ($scope.registration_form.facebook_tracking_code) {
            var pass_data = {
                'func': 'facebook',
                'facebook_tracking_code': $scope.registration_form.facebook_tracking_code,
                'page': 'Expo_CCard',
                'event_object': { 'event': $scope.registration_form.conference.code, 'value': SharedProperties.totalPrice}
            };
            parent.postMessage(JSON.stringify(pass_data), "*");
        }
        $scope.resetIframe = function() {
            SharedProperties.registeredAttendee = {};
            delete SharedProperties.levelPrice;
            delete SharedProperties.registration_adds_on_sessions_avilable;
            delete SharedProperties.registration_adds_on_selected;
            delete SharedProperties.selectedLevel;
            delete SharedProperties.totalPrice;
            delete SharedProperties.addsOnTotalPrice;

            if ($state.$current.url.prefix.search('preview') >= 0) {
                $state.go('preview.step1', { previous: false, edit: false });
            } else if ($state.$current.url.prefix.search('registration') >= 0) {
                $state.go('registration.step1', { previous: false, edit: false });
            }
        };

        $scope.stepBack = function() {
            if (SharedProperties.selectedLevel.nr_attendee_guests > 0) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step_guests', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step_guests', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            } else if (!SharedProperties.hasOwnProperty('registration_adds_on_sessions_avilable')) {
                // delete SharedProperties.levelPrice;
                // delete SharedProperties.registration_adds_on_sessions_avilable;
                // delete SharedProperties.selectedLevel;
                // delete SharedProperties.totalPrice;
                // delete SharedProperties.addsOnTotalPrice;
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step2', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step2', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            } else if (SharedProperties.hasOwnProperty('registration_adds_on_sessions_avilable')) {
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step3', { stepNumber: $scope.stepCounter - 1, edit: false });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step3', { stepNumber: $scope.stepCounter - 1, edit: false });
                }
            }
        };

        $scope.checkTypeOfCard = function() {
            var number = document.getElementById('CCN_iframe').value;
            number = number.toString();
            if (_.isEmpty(number) === false) {
                if ((number.indexOf(4) === 0) && (number.length === 13 || number.length === 16)) {
                    $scope.CCNType = 'visa';
                } else if ((number.indexOf(5) === 0) && (number.charAt(1) >= 1 || number.charAt(1) <= 5) && (number.length === 16)) {
                    $scope.CCNType = 'master';
                } else if ((number.length === 16) && ((_.inRange(number.substring(0, 8), 60110000, 60119999)) ||
                        (_.inRange(number.substring(0, 8), 65000000, 65999999)) ||
                        (_.inRange(number.substring(0, 8), 62212600, 62292599)))) {
                    $scope.CCNType = 'discover';
                } else if ((number.indexOf(3) === 0) && ((number.indexOf(4) === 1 || number.indexOf(7) === 1)) && (number.length === 15)) {
                    $scope.CCNType = 'american_exp'
                } else if (((number.substring(0, 4) === 3088) || (number.substring(0, 4) === 3096) || (number.substring(0, 4) === 3112) ||
                        (number.substring(0, 4) === 3158) || (number.substring(0, 4) === 3337) || (_.inRange(number.substring(0, 8), 35280000, 35899999))) &&
                    (number.length === 16)) {
                    $scope.CCNType = 'jcb';
                } else if ((number.indexOf(3) === 0) && (number.indexOf(0) === 1 || number.indexOf(6) === 1 || number.indexOf(8) === 1) && (number.length === 14)) {
                    $scope.CCNType = 'diners';
                } else {
                    $scope.CCNType = '';
                }
            } else {
                $scope.CCNType = '';
            }
        };

        $scope.insert_slash = function(event) {
            var expiration_date = document.getElementById('expiration_date');
            if (expiration_date.value.length === 2 && event.keyCode !== 8 && event.keyCode !== 46 && event.key !== '/') {
                expiration_date.value = expiration_date.value.concat('/');
            }
        };

        $scope.goToSubmitStep = function() {
            if (verifyEmptyInputs()) {
                registrationPayment();
            }
        };

        function verifyEmptyInputs() {
            var ok = true;
            $scope.validationErrors = [];
            var fields_counter = 0;
            _.each(document.getElementsByClassName('required-payment-field'), function(required_field) {
                if (!_.isEmpty(required_field.value)) {
                    fields_counter++;
                }
            });
            if (fields_counter < 5) {
                $scope.validationErrors.push("Please complete the required fields.");
                ok = false;
            }
            return ok;
        }

        function registrationPayment() {
            try {
                Stripe.setPublishableKey(API.STRIPE_KEY);
            } catch (err) {
            }
            var form = angular.element('#register_payment_for_iframe');
            $scope.registrationLoader = true;
            Stripe.card.createToken(form, stripeResponseHandler);
        }

        function stripeResponseHandler(status, response) {
            $scope.registrationLoader = false;
            if (response.hasOwnProperty('error')) {
                $scope.validationErrors = [];
                $scope.validationErrors.push($scope.errorMessages[!response.error.code ? response.error.type : response.error.code]);
                $scope.$apply();
            } else {
                $scope.$apply();
                $scope.validationErrors = [];
                $scope.registeredAttendee.stripe_token = response.id;
                SharedProperties.registeredAttendee = $scope.registeredAttendee;
                if ($state.$current.url.prefix.search('preview') >= 0) {
                    $state.go('preview.step5', { stepNumber: $scope.stepCounter + 1 });
                } else if ($state.$current.url.prefix.search('registration') >= 0) {
                    $state.go('registration.step5', { stepNumber: $scope.stepCounter + 1 });
                }
            }
        }

        $scope.errorMessages = {
            incorrect_number: "Invalid Credit Card Number",
            invalid_number: "Invalid Credit Card Number",
            invalid_expiry_month: "Invalid Exp. Date",
            invalid_expiry_year: "Invalid Exp. Date",
            invalid_cvc: "Invalid CVV",
            expired_card: "The card has expired.",
            incorrect_cvc: "Invalid CVV",
            incorrect_zip: "Invalid Zip Code",
            card_declined: "Invalid card. Please double check your card and submit again.",
            missing: "There is no card on a customer that is being charged.",
            invalid_request_error: "An error was detected." + "\n" + "Please complete the required fields.",
            processing_error: "An error occurred while processing the card.",
            rate_limit: "An error occurred due to requests hitting the API too quickly. Please let us know if you're consistently running into this error."
        };
    }
})();
