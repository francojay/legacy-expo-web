(function(){
	'use strict';
		angular
			.module('app')
			.controller('GetConfController', [
				'$scope', '$rootScope', '$state', 'API', 'ngDialog', '$filter', 'SharedProperties', 'ConferenceExhibitorsService', 'ConferenceCoverScreensService',
				'ConferenceService', 'ConferenceRegistrationService', '$stateParams', 'ConferenceAttendeesService', 'ConferenceSessionsService', 'ConferenceUsersService', 'localStorageService',
				'toastr', '$location', 'selectedPane', 'selectedType', 'conference', 'users', 'attendees', 'sessions', 'session', 'exhibitors', 'fields', 'promoCodes',
                'exhibitor_materials','FileService', '$q', '$window', 'merchantService', '$interval', '$timeout', '$anchorScroll', 'default_cover_screens',
                'manageNotificationsService', 'vkEmojiTransforms', 'EmojiHex', 'EmojiGroups', '$compile',
                getConfController
				]);

        function getConfController($scope, $rootScope, $state, API, ngDialog, $filter, SharedProperties, ConferenceExhibitorsService, ConferenceCoverScreensService,
            ConferenceService, ConferenceRegistrationService, $stateParams, ConferenceAttendeesService, ConferenceSessionsService,
            ConferenceUsersService, localStorageService,
            toastr, $location, selectedPane, selectedType, conference, users, attendees, sessions, session, exhibitors, fields, promoCodes,
            exhibitor_materials, FileService, $q, $window, merchantService, $interval, $timeout, $anchorScroll, default_cover_screens,
            manageNotificationsService, vkEmojiTransforms, EmojiHex, EmojiGroups, $compile){

		    var vm = this;
			var sharedData = SharedProperties.sharedObject;

            sharedData.displayConference = false;

            //super-admin

            if(localStorageService.get('is_superuser')){
                vm.superUserOn = true;
                vm.exhibitorProfilePanelActive = 'details';
            } else{
                vm.superUserOn = false;
                vm.exhibitorProfilePanelActive = null;
            }
            ////////

            (function(){
                var currentDate = new Date();
                vm.currentDate = currentDate.getMonth() + 1 + '/' + currentDate.getDate() + '/' + currentDate.getFullYear();
                vm.currentDateMoment = moment(currentDate).startOf('day');
            })();


            vm.cancelDrop = true;
            vm.searchEmail = false;
            vm.sessionRegistrantsView = false;
            vm.sessionAttendeesView = false;
            vm.travelSelected = 'hotel';
            vm.travel_type_table_order = 'desc';
            vm.travel_loader = true;
            vm.united_states = API.UNITED_STATES;
            vm.all_countries = API.COUNTRIES;
            vm.editSpeakerProfileMode = false;
            vm.conferenceSpeakerModel = {};
            vm.iframeCurrentUrl = window.location.href.split('/#')[0];

            vm.sessionSpeakerFields = presets.sessionSpeakerFields();

            if (session) {
                vm.currentSessionItem = session;
            }

            vm.openAddSpeakerModal = function(){
                ngDialog.open({template: 'app/views/partials/conference/add_speaker_modal.html',
                    scope : $scope,
                    className: 'app/stylesheets/_addSpeakerModal.scss'
                });
            };

            vm.setModeratorSpeaker = function(session_speaker_id){
                if(vm.newSessionItem.is_moderator == session_speaker_id){
                    vm.newSessionItem.is_moderator = null;
                } else{
                    vm.newSessionItem.is_moderator = session_speaker_id;
                }
            };

            vm.editSpeakerProfile = function(speaker){
                vm.conferenceSpeakerModel = _.cloneDeep(speaker);
                vm.editSpeakerProfileMode = true;
            };

            vm.uploadSpeakerProfilePhoto = function(files){
                lib.squareifyImage($q, files)
                    .then(function(response){
                        var imgGile = response.file;

                        FileService.getUploadSignature(imgGile)
                            .then(function(response){
                                var file_link = response.data.file_link;
                                var promises = [];

                                promises.push(FileService.uploadFile(imgGile, response));

                                $q.all(promises).then(function(response){
                                    vm.conferenceSpeakerModel.headshot_url = file_link;
                                }).catch(function(error){

                                });

                            })
                            .catch(function(error){

                                if (error.status == 403) {
                                    toastr.error('Permission Denied!');
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                }
                            });
                    });
            };

						vm.saveConferenceSpeakerButtonStatus = true;
            vm.saveConferenceSpeakerData = function(call_from){
							vm.saveConferenceSpeakerButtonStatus = false;
                if(!_.isEmpty(vm.conferenceSpeakerModel.first_name) && !_.isEmpty(vm.conferenceSpeakerModel.last_name)){
                    ConferenceService
                        .saveConferenceSpeakerData(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.conferenceSpeakerModel)
                        .then(function(response){
														vm.saveConferenceSpeakerButtonStatus = true;
                            if(!vm.conferenceSpeakerModel.hasOwnProperty('id')){
                                vm.conferenceSpeakersList.speakers.push(response);
                                if(vm.newSession == true){
                                    var arr = _.pullAt(vm.conferenceSpeakersList.speakers, [vm.conferenceSpeakersList.speakers.length - 1]);
                                    vm.conferenceSpeakersList.speakers = _.concat(arr, vm.conferenceSpeakersList.speakers);
                                    vm.currentSessionSpeakersList = _.cloneDeep(vm.conferenceSpeakersList.speakers);
                                }
                            } else{
                                vm.conferenceSpeakersList.speakers = _.reject(vm.conferenceSpeakersList.speakers, ['id', vm.conferenceSpeakerModel.id]);
                                vm.conferenceSpeakersList.speakers.push(response);
                            }
                            toastr.success('The speaker has been created successfully.', 'Speaker Created!');
                            vm.conferenceSpeakerModel = {};
                            vm.editSpeakerProfileMode = false;
                            if(call_from == 'dialog'){
                                ngDialog.close();
                            }
                        })
                        .catch(function(error){
														vm.saveConferenceSpeakerButtonStatus = true;
                            lib.processValidationError(error, toastr);
                        });
                } else{
										vm.saveConferenceSpeakerButtonStatus = true;
                    toastr.error('Please fill the inputs with proper data!');
                    return;
                }
            };

            vm.removeConferenceSpeaker = function(speaker){
                ConferenceService
                    .deleteConferenceSpeaker(speaker.id)
                    .then(function(response){

                        vm.conferenceSpeakersList.speakers = _.reject(vm.conferenceSpeakersList.speakers, ['id', speaker.id]);
                    })
                    .catch(function(error){

                    });
            };

            vm.updateConferenceSocialTags = function(){
                var social_obj = {
                    'facebook_tags' : vm.social_tags['social_tag'],
                    'twitter_tags' : vm.social_tags['social_tag'],
                    'linkedin_tags' : vm.social_tags['social_tag'],
                    'instagram_tags' : vm.social_tags['social_tag'],
                    'social_tags': vm.social_tags['social_tag']
                };

                ConferenceService
                    .updateSocialTags(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], social_obj)
                    .then(function(response){
                        vm.social_tags['social_tag'] = response.facebook_tags;
                        SharedProperties.social_tags = vm.social_tags;
						toastr.success("Your social tag has been updated!");
                    })
                    .catch(function(error){

                    });
            };

            vm.getConferenceSocialTags = function(){
                if (SharedProperties.social_tags
                        && SharedProperties.social_tags_conference == ConferenceService.hash_conference_id.decode(vm.conference_id)[0]) {
                    vm.social_tags['social_tag'] = SharedProperties.social_tags['social_tag'];
                } else {
                    ConferenceService
                        .getSocialTags(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            vm.social_tags['social_tag'] = response.facebook_tags;
                            SharedProperties.social_tags = {};
                            SharedProperties.social_tags['social_tag'] = response.facebook_tags;
                            SharedProperties.social_tags_conference = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                        })
                        .catch(function(error){

                        });
                }
            };

            vm.session_field_drop_down = false;
            vm.sessionCustomFieldModel = {
                'name' : '',
                'type' : 1,
                'customsessionfieldoption_set': [{'value' : ''}]
            };

            vm.moreOptionsForSessionCustomField = function(){
                vm.sessionCustomFieldModel.customsessionfieldoption_set.push({'value' : ''});
            };

            vm.removeOptionFromSessionCustomField = function(index){
                var fields = vm.sessionCustomFieldModel.customsessionfieldoption_set;
                if (index > -1) {
                    fields.splice(index, 1);
                }
                vm.sessionCustomFieldModel.customsessionfieldoption_set = fields;
            }

            vm.selectSessionCustomFieldOption = function(session_field_type){
                vm.sessionCustomFieldModel.type = session_field_type;
            };

            vm.getSessionCustomFields = function(){
                // initiateCustomFieldsData(vm);
                if (SharedProperties.sessionCustomFields &&
                        SharedProperties.sessionCustomFieldsConference == ConferenceService.hash_conference_id.decode(vm.conference_id)[0]) {
                    vm.sessionCustomFieldsPerConference = SharedProperties.sessionCustomFields;
                    vm.sessionMappingFields = vm.sessionMappingFields.concat(vm.sessionCustomFieldsPerConference);
                } else {
                    ConferenceSessionsService
                        .getSessionCustomFields(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            vm.sessionCustomFieldsPerConference = response;
                            SharedProperties.sessionCustomFields = response;
                            SharedProperties.sessionCustomFieldsConference = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                            vm.sessionMappingFields = vm.sessionMappingFields.concat(vm.sessionCustomFieldsPerConference);
                        })
                        .catch(function(error){

                        });
                }

            };

            vm.saveSessionCustomField = function(){
                vm.sessionCustomFieldModel.customsessionfieldoption_set = _.reject(vm.sessionCustomFieldModel.customsessionfieldoption_set, ['value', '']);
                if(vm.sessionCustomFieldModel.hasOwnProperty('id') == false){
                    ConferenceSessionsService
                        .createSessionCustomFields(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.sessionCustomFieldModel)
                        .then(function(response){
                            vm.sessionCustomFieldsPerConference.push(response);
                            vm.sessionCustomFieldModel = {
                                'name' : '',
                                'type' : 1,
                                'customsessionfieldoption_set': [{'value' : ''}]
                            };
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                } else{
                    ConferenceSessionsService
                        .updateSessionCustomField(vm.sessionCustomFieldModel)
                        .then(function(response){
                            vm.sessionCustomFieldModel = {
                                'name' : '',
                                'type' : 1,
                                'customsessionfieldoption_set': [{'value' : ''}]
                            };
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            }

            vm.editLevelNameInput = function(level) {
                if (level == vm.currentLevelItem) {
                    level.editmode = true;
                }
            }

            vm.getExhibitorList = function(){
                if (SharedProperties.exhibitorList &&
                        SharedProperties.exhibitorListConference == ConferenceService.hash_conference_id.decode(vm.conference_id)[0]) {
                    vm.exhibitorList = SharedProperties.exhibitorList;
                    vm.initialPayments = SharedProperties.initialPayments;
                } else {
                    ConferenceExhibitorsService
                        .getExhibitorsForWeb(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            vm.exhibitorList = response.exhibitors;
                            vm.initialPayments = response.initial_payments;

                            SharedProperties.exhibitorList = vm.exhibitorList;
                            SharedProperties.initialPayments = vm.initialPayments;
                            SharedProperties.exhibitorListConference = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                        })
                        .catch(function(error){

                        });
                }
            };

            vm.editSessionCustomField = function(session_custom_field){
                vm.sessionCustomFieldModel = session_custom_field;
            };

            vm.cancelEditSessionCustomField = function(){
                vm.sessionCustomFieldModel = {
                    'name' : '',
                    'type' : 1,
                    'customsessionfieldoption_set': [{'value' : ''}]
                };
            };

            vm.deleteSessionCustomField = function(session_custom_field_id){
                ConferenceSessionsService
                    .deleteSessionCustomField(session_custom_field_id)
                    .then(function(response){
                        vm.sessionCustomFieldsPerConference = _.reject(vm.sessionCustomFieldsPerConference, ['id', session_custom_field_id]);
                        if(vm.sessionCustomFieldModel.hasOwnProperty('id') && vm.sessionCustomFieldModel.id == session_custom_field_id){
                            vm.cancelEditSessionCustomField();
                        }
                    })
                    .catch(function(error){

                    });
            };

            vm.displayOptionsForGraphic = false;
            vm.selectOptionForGraphic = function(time){
                _.each(vm.registrationGraphicDays, function(option){
                    option.checked = false;
                });
                time.checked = true;
                var current_day = new Date();
                var option_time, diff;
                vm.displayOptionsForGraphic = false;
                if(time.value == 'Last 7 Days'){
                    diff = 7;
                    vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count_last_seven;
                } else if(time.value == 'Current Month'){
                    option_time = new Date(current_day.getFullYear(), current_day.getMonth() , 1);
                    diff = lib.daysBetween(current_day, option_time);
                    vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count_current_month;
                } else if(time.value == 'Last Month'){
                    if(current_day.getMonth() > 0){
                        option_time = new Date(current_day.getFullYear(), current_day.getMonth() - 1, 1);
                    } else{
                        option_time = new Date(current_day.getFullYear() - 1, 11, 1);
                    }
                    diff = lib.daysBetween(current_day, option_time);
                    vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count_last_month;
                } else{
                    var date2 = new Date(vm.registrationStats.registration_form_created_at);
                    var date1 = new Date();
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    diff = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count;
                }
                vm.setupStats(diff, 'update');
            };

            vm.registrationGraphicDays = [
                {'value' : 'Last 7 Days', 'checked' : true},
                {'value' : 'Current Month', 'checked' : false},
                {'value' : 'Last Month', 'checked' : false},
                {'value' : 'All Time', 'checked' : false}
            ];

            vm.finishedTOSSetup = function() {
                if (!vm.conference.registration_form.terms_of_service_file_name) {
                    toastr.warning("The Terms and Conditions File is required");
                    return
                } if (!vm.conference.registration_form.refund_policy_file_name) {
                    toastr.warning("The Refund Policy File is required");
                    return
                }   else {
                    vm.registrationNextStep('review_panel');
                }

            }

            vm.openRefundPolicyRequirements = function(){
                ngDialog.open({
                    template: 'app/views/partials/conference/registration-steps/refund_policy_requirements.html'
                });
            };
            vm.registrationNextStep = function(panel, marker){
                $("#create-registration-form-content").scrollTop(0);
                var ok = true;
                if(vm.registrationSteps['registration_levels_setup_panel'] && panel == 'registration_level_options_panel' && vm.registrationLevels.length <= 0){
                    toastr.warning("You need to create a level in order to continue.");
                    return;
                } else if(vm.registrationSteps['registration_levels_setup_panel'] && panel == 'registration_level_options_panel'){
                    _.each(vm.registrationLevels, function(level, index) {
                        if (level.id == vm.currentLevelItem.id) {
                            level = vm.currentLevelItem;
                            vm.registrationLevels[index] = vm.currentLevelItem;
                        }
                        if (level.name == '' && ok) {
                            toastr.error('Please set names for all the registration levels!');
                            ok = false;
                            vm.currentLevelItem = level;
                            vm.currentLevel = level.id;
                            return false;
                        }

                        if (level.registrationlevelpricing_set.length <= 0 && ok) {
                            if (!(level.id == vm.currentLevelItem.id && vm.standardPricing &&
                                    vm.standardPricing.price && vm.standardPricing.name && vm.standardPricing.name != "")) {
                                toastr.error('Please set at least one price for all the registration levels!');
                                ok = false;
                                vm.currentLevelItem = level;
                                vm.currentLevel = level.id;
                                return false;
                            }
                        }

                        _.each(level.registrationlevelpricing_set, function(pricing){
                            if (pricing.price == null || pricing.price == undefined) {
                                toastr.error('The price field is required for each of the registration prices!');
                                vm.currentLevelItem = level;
                                vm.currentLevel = level.id;
                                ok = false;
                                return false;
                            }
                        });
                    });
                } else if (vm.registrationSteps['review_panel'] && !vm.conference.registration_form.terms_of_service_file_name) {

                }
                if (!ok) {
                    return;
                }
                if(vm.registration_panel_edit && marker == undefined){
                    vm.registration_panel_edit = false;
                    vm.registrationNextStep('review_panel');
                } else{
                    _.each(vm.registrationSteps, function(value, step){
                        if(panel !== step){
                            vm.registrationSteps[step] = false;
                        } else{
                            vm.registrationSteps[step] = true;
                        }
                    });
                }
            }

            vm.registrationNextStepForm = function(panel){
                if(panel == 'attendee_fields_entry_panel'){
                    vm.registrationSteps[panel] = false;
                    vm.registrationSteps['registration_levels_setup_panel'] = true;
                } else if(panel == 'registration_levels_setup_panel'){
                    vm.registrationSteps[panel] = false;
                    vm.registrationSteps['registration_level_options_panel'] = true;
                }  else if(panel == 'registration_level_options_panel'){
                    vm.registrationSteps[panel] = false;
                    vm.registrationSteps['legal_panel'] = true;
                }  else if(panel == 'legal_panel'){
                    vm.registrationSteps[panel] = false;
                    vm.registrationSteps['review_panel'] = true;
                }
            }

            vm.editThisRegistrationLevel = function(level, type){
                vm.registration_panel_edit = true;
                vm.registrationNextStep(type, true);
                vm.levelSelected(level);
            };

            vm.securePricing = function(pricing) {
                pricing.price = pricing.price.replace(/[^0-9$.,]/g, '');

                var nrDots = pricing.price.split(".").length;
                if (nrDots > 2) {
                    var i = pricing.price.lastIndexOf('.');
                    if (i != -1) {
                        pricing.price = pricing.price.substr(0, i);
                    }
                }
            }

            vm.functionDropSubPanel = function(type, e, index, element){
                lib.functionDropSubPanel(type, e, index, element);
            };

            // Toggles the promo code 'single_use' flag
            // Updates the backend in the background
            // If the promo code does not exist yet (it's a new promo code) it will only adjust the payload to be sent to the backend
            // when the promo code create button is clicked
            vm.promoCodeToggle = function(promo_code) {
                if (promo_code.single_use == undefined) promo_code.single_use = false;
                promo_code.single_use = !promo_code.single_use;

                if (!promo_code.id || promo_code.id == undefined) return;

                ConferenceRegistrationService
                    .updateRegistrationPromoCode(promo_code.id, promo_code)
                    .then(function(response){
                        //
                    })
                    .catch(function(error){

                    });
            }

            vm.standardFields = presets.setStandardAttendeeFields();

            vm.customFieldsForRegistration = [];
            vm.typeOfFieldChoose = function(field, type, e){
                field.type = type;

                if(type == 'required')
                    field.type_code = 'Required';
                else if(type == 'optional')
                    field.type_code = 'Optional';
                else if(type == 'hidden')
                    field.type_code = 'Hidden';

                e.target.parentElement.style.visibility = 'hidden';
            };

            vm.hideDropDown = function(index){
                if(document.getElementsByClassName('drop-down-type-for-field')[index])
                document.getElementsByClassName('drop-down-type-for-field')[index].style.visibility = 'hidden';
            };

            vm.typeOfCustomFieldChoose = function(custom_field, type, e){
                    custom_field.regsitration_form_type = type;
                    if(type == 'required')
                        custom_field.regsitration_form_type_code = 'Required';
                    else if(type == 'optional')
                        custom_field.regsitration_form_type_code = 'Optional';
                    else if(type == 'hidden')
                        custom_field.regsitration_form_type_code = 'Hidden';

                e.target.parentElement.style.visibility = 'hidden';
            }

            vm.addNewBenefit = function() {
                vm.currentLevelItem.registrationlevelbenefit_set.push({});
            }

            vm.agreeMerchantTerms = false;
            vm.merchantAgreementStep = 1;
            vm.merchantCompanyForm = true;
            vm.merchantIndividualForm = false;
            vm.merchantAgreementNextStep = function() {
                if (vm.merchantAgreementStep == 1) {
                    if(!vm.agreeMerchantTerms){
                        toastr.warning('You must agree with the merchant agreement by checking the box after you have read the agreement.');
                        return;
                    } else {
                        vm.merchantAgreementStep = 2;
                    }
                } else if(vm.merchantAgreementStep == 2){
                    var ok = true;
                    if(vm.merchantIndividualForm){
                        vm.merchantIndividualProfileModel['legal_entity_type'] = 'individual';
                        _.each(vm.merchantIndividualFields, function(detail){
                            if(detail.code != 'personal_id_number' && detail.code != 'verification_document'){
                                if(_.isArray(detail.code)){
                                    _.each(detail.code, function(prop){
                                        if(vm.merchantIndividualProfileModel[prop] == '' || vm.merchantIndividualProfileModel[prop] == null){
                                            toastr.error(detail.label + ' field is required!');
                                            ok = false;
                                            return false;
                                        }
                                    });
                                    return false;
                                } else if(vm.merchantIndividualProfileModel[detail.code] == '' || vm.merchantIndividualProfileModel[detail.code] == null){
                                    toastr.error(detail.label + ' field is required!');
                                    ok = false;
                                    return false;
                                }
                            }
                        });
                    } else if(vm.merchantCompanyForm){
                        vm.merchantCompanyProfileModel['legal_entity_type'] = 'company';
                        _.each(vm.merchantCompanyFields, function(detail){
                            if(detail.code != 'verification_document'){
                                if(_.isArray(detail.code)){
                                    _.each(detail.code, function(prop){
                                        if(vm.merchantCompanyProfileModel[prop] == '' || vm.merchantCompanyProfileModel[prop] == null){
                                            toastr.error(detail.label + ' field is required!');
                                            ok = false;
                                            return false;
                                        }
                                    });
                                    return false;
                                } else if(vm.merchantCompanyProfileModel[detail.code] == '' || vm.merchantCompanyProfileModel[detail.code] == null){
                                    toastr.error(detail.label + ' field is required!');
                                    ok = false;
                                    return false;
                                }
                            }
                        });
                    }
                    if(ok){
                        vm.merchantAgreementStep = 3;
                    } else return;
                }
            }

            function merchantAgreementSuccessful() {
                vm.conference.business_data.merchant_id_verified = true;
            }

            vm.startMerchantAgreement = function() {
                $scope.merchantAgreementSuccessful = merchantAgreementSuccessful;
                merchantService.startMerchantAgreement($scope);
            };

            vm.uploadMerchantDocument = function(files) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        FileService.uploadFile(files[0], response)
                            .then(function(uploadResponse){
                                if(vm.merchantCompanyForm){
                                    vm.merchantCompanyProfileModel.verification_document = file_link;
                                } else if(vm.merchantIndividualForm){
                                    vm.merchantIndividualProfileModel.verification_document = file_link;
                                }
                            })
                            .catch(function(error){

                            });
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            };

            function stripeResponseHandlerBankAccount(status, response) {
                if (response.error) {
                    toastr.error(response.error.message);
                } else {
                    if(vm.merchantIndividualForm){
                        vm.merchantIndividualProfileModel.stripe_token = response.id;
                        ConferenceService
                            .submitMerchantAgreement(vm.merchantIndividualProfileModel)
                            .then(function(response){
                            })
                            .catch(function(error){

                            });

                    } else if(vm.merchantCompanyForm){
                        vm.merchantCompanyProfileModel.stripe_token = response.id;
                        ConferenceService
                            .submitMerchantAgreement(vm.merchantCompanyProfileModel)
                            .then(function(response){
                                ngDialog.close();
                            })
                            .catch(function(error){

                            });

                    }
                }
            };

            vm.submitMerchantAgreement = function(){
                var merchant_form = angular.element('#bank_information_form');
                Stripe.bankAccount.createToken(merchant_form, stripeResponseHandlerBankAccount);
            };

            vm.startMerchantAgreementOld = function(){
                ConferenceRegistrationService
                    .startRegistrationForm(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        console.log('start merchant');
                        vm.setRegistrationFormData(response, 'first_setup');
                        _.each(vm.savedCustomFields, function(custom_field){
                            custom_field.regsitration_form_type = 'optional';
                            custom_field.regsitration_form_type_code = "Optional";
                        });
                    })
                    .catch(function(error){
                        vm.conference.registration_form = null;
                    });
            };

            vm.saveFirstStepRegistration = function(){
                var customFieldsForForm = [];
                vm.nextButtonBlocked = true;
                _.each(vm.savedCustomFields, function(savedCustomField){
                    customFieldsForForm.push(
                        {
                            custom_field_id: savedCustomField.id,
                            type: savedCustomField.regsitration_form_type || 'optional'
                        }
                    );
                });
                if (!vm.registration_form_id) {
                    ConferenceRegistrationService.startRegistrationForm(vm.conference.conference_id)
                        .then(function(response) {
                            vm.conference.registration_form = response[0];
                            vm.registration_form_id = response.id;
                        }).catch(function(error) {
                            toastr.error('There was an error when initiating your registration form!');
                        });
                }

                if (!vm.registration_form_id) {
                    return;
                }

                ConferenceRegistrationService
                    .setRegistrationFormFields(vm.registration_form_id, vm.standardFields, customFieldsForForm)
                    .then(function(response){
                        var key =
                        _.findKey(vm.registrationSteps, function(key){
                            return key == true;
                        });

                       if (!vm.registrationLevels) {
                            vm.registrationLevels = [];
                       }
                        if(vm.registrationLevels.length === 0) {
                            vm.addRegistrationLevel();
                        }
                        vm.registrationNextStepForm(key);
                        vm.nextButtonBlocked = false;
                    }).catch(function(err){
                        vm.nextButtonBlocked = false;
                    });
                if(vm.registration_panel_edit){
                    vm.registrationNextStep('review_panel');
                    vm.registration_panel_edit = false;
                }
            };

			vm.stepTwoRegistrationLoadLevel = function(){
                if (!vm.currentLevelItem) {
                    if(!_.isEmpty(vm.registrationLevels)){
                        vm.levelSelected(vm.registrationLevels[0], true);
                        vm.noLevels = false;
                    } else{
                        vm.noLevels = true;
                    }
                }

                if (!vm.allSessionsRegistration) {
                    if (SharedProperties.registrationSessionsConferenceId == ConferenceService.hash_conference_id.decode(vm.conference_id)[0] && SharedProperties.registrationSessions) {
                        vm.allSessionsRegistration = SharedProperties.registrationSessions;
                        _.each(vm.allSessionsRegistration, function(session){
                            session.marked = false;
                        });
                    } else{
                        vm.loadingRegistrationSessions = true;
                        ConferenceSessionsService
                            .getAllSessions(vm.conference_id)
                            .then(function(res){
                                SharedProperties.registrationSessions = res.sessions;
                                SharedProperties.registrationSessionsConferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                                vm.allSessionsRegistration = res.sessions;
                                _.each(vm.allSessionsRegistration, function(session){
                                    session.marked = false;
                                });
                                vm.loadingRegistrationSessions = false;
                            })
                            .catch(function(err){

                                vm.loadingRegistrationSessions = false;
                            });
                        }
                }
			};

			vm.selectableRemove = false;
			vm.selectableAdd = false;

			vm.registrationSessionSelected = function(session, type){
				if(session.marked){
					session.marked = !session.marked;
				} else{
					session.marked = true;
				}
				if(session.marked){
					if(type == 'delete'){
						vm.selectableRemove = true;
						vm.selectableAdd = false;
					} else if(type == 'add'){
						vm.selectableRemove = false;
						vm.selectableAdd = true;
					}
				}
			};

			vm.removeSessionsFromLevel = function(){
				_.each(vm.allSessionsRegistration, function(session){
					if(session.marked && session.marked == true){
						vm.currentLevelItem.registrationlevelsession_set.push({'session': session});
					}
				});

				var session_ids = [];
				_.each(vm.currentLevelItem.registrationlevelsession_set, function(session){
                    if (!session.registrationleveladdonsessionpricing_set ||
                            session.registrationleveladdonsessionpricing_set.length === 0) {
                        session.session.addOnSession = {};
                        session.session.addOnSession.add_on_price_model = {
                            session: session.session.id,
                            registration_level: session.registration_level,
                            price: null
                        };
                    }

					if(session.session.marked && session.session.marked == true){
						session_ids.push(session.session.id);
					}
					session.session.marked = false;
				});
				ConferenceRegistrationService
					.addSessionToRegistrationLevel(vm.currentLevel, {"sessions": session_ids})
					.then(function(res){
                        _.each(session_ids, function(session_id){
                            vm.allSessionsRegistration = _.reject(vm.allSessionsRegistration, function(conference_session){
                                return conference_session.id == session_id;
                            });
                        });

                        ConferenceRegistrationService.getRegistrationFormV1(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                                .then(function(response){
                                    SharedProperties.registrationForm = response;
                                    if(response.length != 0){
                                        vm.setRegistrationFormData(response, 'update');
                                        vm.levelSelected(vm.currentLevelItem);
                                    }
                                })
                                .catch(function(error){
                                    if (error.status == 404) {
                                        vm.setupStats(7, 'no_form');
                                        vm.conference.registration_form = null;
                                        vm.registrationStats = null;
                                    }
                                });
					})
					.catch(function(err){

					});
			};

			vm.addSessionsToLevel = function(){
				var session_ids = [];
                var session_to_add = [];
				vm.currentLevelItem.registrationlevelsession_set = _.reject(vm.currentLevelItem.registrationlevelsession_set, function(session){
					if(session.session.marked && session.session.marked == true){
                        session_to_add.push(session.session);
						session_ids.push(session.session.id);
					}
					return session.session.marked == true;
				});
				_.each(vm.currentLevelItem.registrationlevelsession_set, function(session){
					session.session.marked = false;
				});
				_.each(vm.allSessionsRegistration, function(session){
					session.marked = false;
				});
				ConferenceRegistrationService
					.removeSessionToRegistrationLevel(vm.currentLevel, session_ids)
					.then(function(res){
                        _.each(session_to_add, function(session){
                            session.marked = false;
                            var foundSession = _.find(vm.allSessionsRegistration, function(sessionItem){
                                return sessionItem.id === session.id;
                            });
                            if (!foundSession) {
                                vm.allSessionsRegistration.push(session);
                            }

                        });
					})
					.catch(function(err){

					});
			};

            vm.addRegistrationLevel = function(){
                vm.addLevelLoader = true;
                if (!vm.level.name) {
                    vm.level.name = " ";
                }

                ConferenceRegistrationService.createRegsitrationFormLevels(vm.registration_form_id, vm.level)
                    .then(function (response) {
                        vm.registrationLevels.push(response);
                        SharedProperties.registrationForm[0].registrationlevel_set = vm.registrationLevels;

                        vm.levelSelected(response);
                        vm.editLevelNameInput(response);
                        vm.addLevelLoader = false;
                        vm.level.name = '';
                        if (vm.noLevels) {
                            vm.noLevels = false;
                        }
                    })
                    .catch(function (error) {
                        console.log('error');
                        console.log(error);
                        vm.addLevelLoader = false;
                    });
            };

            function verifyExistingLevel(newLevel){
                return _.find(vm.registrationLevels, function(level) {
                        return (level.id !== newLevel.id && level.name === newLevel.name);
                    }) === undefined;
            }

            vm.deleteRegistrationLevelConfirmed = function(level) {
                if(vm.registrationLevels.length === 1){
                    toastr.warning("Your registration form needs to have at least one level available!")
                    return;
                }
                vm.addLevelLoader = true;
                ConferenceRegistrationService
                 .deleteRegistrationLevels(level.id)
                 .then(function(response){
                        vm.addLevelLoader = false;
                     var index = _.indexOf(vm.registrationLevels, level, 0);
                     vm.registrationLevels  = _.reject(vm.registrationLevels, ['id', level.id]);
                     SharedProperties.registrationForm[0].registrationlevel_set = vm.registrationLevels;
                     if(!_.isEmpty(vm.registrationLevels)){
                         vm.levelSelected(vm.registrationLevels[index - 1]);
                     } else{
                         vm.noLevels = true;
                         vm.currentLevelItem = null;
                         vm.currentLevel = null;
                         vm.levelName = '';
                         vm.standardStatus = vm.verifEmpty(null);
                         vm.standardPricing = {
                             'name' : '',
                             'price' : ''
                         };
                     }
                 })
                 .catch(function(err){
                        vm.addLevelLoader = false;
                 })
            }

			vm.removeRegistrationLevel = function(level){
                $scope.deleteRegistrationLevelConfirmed = vm.deleteRegistrationLevelConfirmed;
                $scope.level = level;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_delete_registration_level.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });

			};

            vm.level = {
                'name' : ''
            };

			vm.benefit = {
				'name' : ''
			};

            vm.updateLevelTitle = function(level) {
                if (level.name === '') return;
                level.editmode = false;
                if(verifyExistingLevel(level)) {
                    ConferenceRegistrationService
                        .updateRegistrationLevel(level)
                        .then(function (response) {
                            level = response;
                            vm.levelName = level.name;

                            var arr = _.takeWhile(level.registrationlevelpricing_set, function (price) {
                                if (price.starts_at == null) {
                                    return price;
                                }
                            });
                            level.registrationlevelpricing_set = _.takeWhile(level.registrationlevelpricing_set, function (price) {
                                if (price.starts_at != null) {
                                    return price;
                                }
                            });
                            level.registrationlevelpricing_set = _.union(arr, level.registrationlevelpricing_set);
                            _.each(level.registrationlevelpricing_set, function (price) {
                                if (price.starts_at) {
                                    price.starts_at = moment(price.starts_at);
                                    price.ends_at = moment(price.ends_at);
                                }
                            });
                            vm.registrationSessionsPerLevel = level.registrationlevelsession_set;
                        })
                        .catch(function (err) {

                        });
                } else{
                    toastr.warning("You can't have two levels with the same name.");
                    level.name = '';
                    level.editmode = true;
                }
            };

            vm.attendeeGuestItems = [
                {
                    id: 0,
                    label: "0"
                },
                {
                    id: 4,
                    label: "4"
                }
            ];

			vm.updateLevel = function() {
                ConferenceRegistrationService
                    .updateRegistrationLevel(vm.currentLevelItem)
                        .then(function(response){
                                // vm.currentLevelItem = response;
                                // var arr = _.takeWhile(vm.currentLevelItem.registrationlevelpricing_set, function(price){
                                //     if(price.starts_at == null){
                                //         return price;
                                //     }
                                // });
                                // vm.currentLevelItem.registrationlevelpricing_set = _.takeWhile(vm.currentLevelItem.registrationlevelpricing_set, function(price){
                                //     if(price.starts_at != null){
                                //         return price;
                                //     }
                                // });
                                // vm.currentLevelItem.registrationlevelpricing_set = _.union(arr, vm.currentLevelItem.registrationlevelpricing_set);
                                // _.each(vm.currentLevelItem.registrationlevelpricing_set, function(price){
                                //     if(price.starts_at){
                                //         price.starts_at = moment(price.starts_at);
                                //         price.ends_at = moment(price.ends_at);
                                //     }
                                // });
                                // vm.registrationSessionsPerLevel = vm.currentLevelItem.registrationlevelsession_set;
                        })
                        .catch(function(err){
                             if (err.data.detail.description) {
                                toastr.error('The session description cannot have more than 2000 characters');
                             }
                        })
            }

			vm.addBenefit = function(benefit) {
                benefit.loading = true;
				ConferenceRegistrationService
					.addBenefit(vm.currentLevel, benefit)
						.then(function(response){
                                benefit.loading = false;
                                benefit.id = response.id;
								vm.benefit = null;
                                vm.currentLevelItem.hasNewBenefit = false;
								vm.currentLevelItem.registrationlevelbenefit_set.push(response);
						})
						.catch(function(err){
                            benefit.loading = false;

						})
			}

			vm.updateBenefit = function(benefit) {
                benefit.loading = true;
                if (benefit.id) {
                    ConferenceRegistrationService
                        .updateBenefit(benefit)
                            .then(function(response){
                                benefit.loading = false;
                                benefit = response;
                            })
                            .catch(function(err){

                                benefit.loading = false;
                            })
                }
                else {
                    ConferenceRegistrationService
                        .addBenefit(vm.currentLevel, benefit)
                            .then(function(response){
                                    benefit.loading = false;
                                    benefit.id = response.id;
                            })
                            .catch(function(err){
                                benefit.loading = false;

                            })
                }

			}

			vm.setStandardPrice = function(pricing) {
                vm.standardPricing = pricing;
				if (pricing.name && pricing.name != "" && pricing.price) {
                    if (pricing.price < 20 && pricing.price != 0) {
                        pricing.price = 20;
                    }
					ConferenceRegistrationService
    					.addPricing(vm.currentLevel, pricing)
    						.then(function(response){
                                var existing_price = _.find(vm.currentLevelItem.registrationlevelpricing_set, {id:response.id});
                                if (existing_price) {
                                    existing_price = response;
                                } else {
                                    vm.currentLevelItem.registrationlevelpricing_set.push(response);
                                    pricing = response;
                                    vm.standardStatus = false;
                                    vm.standardPricing = response;
                                }
    						})
    						.catch(function(error){
                                vm.standardPricing.price = 0;
                                pricing.price = 0;
                                lib.processValidationError(error, toastr);
    						})
				} else if(pricing.name){
                    vm.standardPricing = pricing;
                }
			};

            vm.removeCustomPrice = function() {
                vm.currentLevelItem.hasCustomPrice = false;
                vm.pricing = null;
            }

            vm.isLastCustomPrice = function(pricing) {
                var last_item;
                _.each(vm.currentLevelItem.registrationlevelpricing_set, function(pricing){
                    if (pricing.starts_at) {
                        last_item = pricing;
                    }
                });

                return last_item && pricing.id === last_item.id;

            };

			vm.setCustomPrice = function() {
                if (vm.pricing && vm.pricing.name && vm.pricing.name != "" && vm.pricing.price && vm.pricing.starts_at_editable && vm.pricing.ends_at_editable){
                    var starts_at = new Date(vm.pricing.starts_at_editable);
                    var ends_at = new Date(vm.pricing.ends_at_editable);
                    vm.pricing.starts_at_raw = {
                        "year": starts_at.getFullYear(),
                        "month": starts_at.getMonth() + 1,
                        "day": starts_at.getDate()
                    }

                    vm.pricing.ends_at_raw = {
                        "year": ends_at.getFullYear(),
                        "month": ends_at.getMonth() + 1,
                        "day": ends_at.getDate()
                    }
                    ConferenceRegistrationService
                        .addPricing(vm.currentLevel, vm.pricing)
                            .then(function(response){
                                vm.pricing = null;
                                var price = response;
                                price.starts_at = moment(price.starts_at);
                                price.ends_at = moment(price.ends_at);
                                price.starts_at_editable = vm.createMomentFromRawTime(price.starts_at_raw);
                                price.ends_at_editable = vm.createMomentFromRawTime(price.ends_at_raw);
                                vm.currentLevelItem.registrationlevelpricing_set.push(price);
                                vm.sortDatesForLevelPrices(vm.currentLevelItem.registrationlevelpricing_set);
                                vm.currentLevelItem.hasCustomPrice = false;
                            })
                            .catch(function(error){
                                vm.pricing.price = 0;
                                lib.processValidationError(error, toastr)
                            })
                }
            };

            vm.createMomentFromRawTime = function(rawDatetime) {
                if (!rawDatetime || !rawDatetime.year) {
                    return null;
                }
                return moment(new Date(
                    rawDatetime.year,
                    rawDatetime.month - 1,
                    rawDatetime.day,
                    rawDatetime.hour,
                    rawDatetime.minute
                ));
            }

			vm.removeBenefit = function(benefit) {
                benefit.loading = true;
                var index = vm.currentLevelItem.registrationlevelbenefit_set.indexOf(benefit);
                if(_.isEmpty(benefit.name)){
                    vm.currentLevelItem.registrationlevelbenefit_set.splice(index, 1);
                    benefit.loading = false;
                    return;
                }
				ConferenceRegistrationService
					.removeBenefit(benefit)
						.then(function(response){
                            benefit.loading = false;
							_.remove(vm.currentLevelItem.registrationlevelbenefit_set, function(benefit_item){
								return benefit_item.id == benefit.id;
							});
						})
						.catch(function(err){
                            benefit.loading = false;

						})
			}

			vm.removePricing = function(pricing_id){
                ConferenceRegistrationService
                    .deletePricing(pricing_id)
                    .then(function(response){
                        vm.currentLevelItem.registrationlevelpricing_set = _.reject(vm.currentLevelItem.registrationlevelpricing_set, ['id', pricing_id]);
                        vm.sortDatesForLevelPrices(vm.currentLevelItem.registrationlevelpricing_set);
                        if(vm.currentLevelItem.registrationlevelpricing_set.length <= 1){
                            vm.currentLevelItem.hasCustomPirce = false;
                        }
                    })
                    .catch(function(error){

                    })
            }

			vm.updatePricing = function(level_pricing){
                if (level_pricing.price < 20) {
                    level_pricing.price = 20;
                }

                if (level_pricing.starts_at_editable && level_pricing.ends_at_editable) {
                    var starts_at = new Date(level_pricing.starts_at_editable);
                    var ends_at = new Date(level_pricing.ends_at_editable);

                    level_pricing.starts_at_raw = {
                        "year": starts_at.getFullYear(),
                        "month": starts_at.getMonth() + 1,
                        "day": starts_at.getDate()
                    }

                    level_pricing.ends_at_raw = {
                        "year": ends_at.getFullYear(),
                        "month": ends_at.getMonth() + 1,
                        "day": ends_at.getDate()
                    }

                    ConferenceRegistrationService
                        .updatePricing(level_pricing)
                        .then(function (res) {
                            res.starts_at = moment(res.starts_at);
                            res.ends_at = moment(res.ends_at);
                            res.starts_at_editable = vm.createMomentFromRawTime(res.starts_at_raw);
                            res.ends_at_editable = vm.createMomentFromRawTime(res.ends_at_raw);
                            var price_index = vm.currentLevelItem.registrationlevelpricing_set.indexOf(level_pricing);
                            vm.currentLevelItem.registrationlevelpricing_set = _.reject(vm.currentLevelItem.registrationlevelpricing_set, ['id', res.id]);
                            vm.currentLevelItem.registrationlevelpricing_set.splice(price_index, 0, res);
                            vm.sortDatesForLevelPrices(vm.currentLevelItem.registrationlevelpricing_set);
                        })
                        .catch(function (err) {
                            level_pricing.price = 0;
                            lib.processValidationError(error, toastr);
                        });
                } else {
                    ConferenceRegistrationService
                        .updatePricing(level_pricing)
                        .then(function(res){
                        })
                        .catch(function(error){
                            level_pricing.price = 0;
                            lib.processValidationError(error, toastr);
                            level_pricing.ends_at = null;
                        });
                }


			}

            vm.addCustomPrice = function() {
                vm.currentLevelItem.hasCustomPrice = true;
            }

            vm.registrationAddOnLevelHasPricing = function(levelSession) {
                return levelSession && levelSession.session.addOnSession && levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length > 0;
            }

            vm.registrationAddOnLevelHasNoPricing = function(levelSession) {
                return !levelSession.session.addOnSession || levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length <= 0;
            }

            vm.levelSelected = function(level, type_call){
                vm.currentLevel = level.id;
                vm.levelName = level.name;
                vm.currentLevelItem = level;
                _.each(level.registrationlevelpromocode_set, function(prom_code){
                    prom_code.starts_at = moment(prom_code.starts_at);
                    prom_code.starts_at_editable = vm.createMomentFromRawTime(prom_code.starts_at_raw);
                    prom_code.ends_at = moment(prom_code.ends_at);
                    prom_code.ends_at_editable = vm.createMomentFromRawTime(prom_code.ends_at_raw);
                });
                _.each(vm.currentLevelItem.registrationlevelsession_set, function(levelSession){
                    var existing_add_on = null;
                    _.each(vm.currentLevelItem.registrationleveladdonsession_set, function(add_on_session){
                        if (add_on_session.session.id === levelSession.session.id) {
                            if (add_on_session.deleted_at === null) {
                                levelSession.session.add_on_selected = true;
                            }

                            _.each(add_on_session.registrationleveladdonsessionpricing_set, function(pricing){
                                pricing.starts_at = moment(pricing.starts_at);
                                pricing.starts_at_editable = vm.createMomentFromRawTime(pricing.starts_at_raw);
                                pricing.ends_at = moment(pricing.ends_at);
                                pricing.ends_at_editable = vm.createMomentFromRawTime(pricing.ends_at_raw);
                            });

                            levelSession.session.addOnSession = add_on_session;
                            if(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length <= 0){
                                vm.addExtraPricingForAddOnSession(levelSession);
                            } else{
                                levelSession.session.addOnSession.add_on_price_model = null;
                                levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
                                    vm.sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
                            }
                        }
                    });
                });

                vm.standardStatus = vm.verifEmpty(level);
                vm.standardPricing = _.find(vm.currentLevelItem.registrationlevelpricing_set, ['starts_at', null]);
                vm.sortDatesForLevelPrices(vm.currentLevelItem.registrationlevelpricing_set);

                if (SharedProperties.registrationSessionsConferenceId == ConferenceService.hash_conference_id.decode(vm.conference_id)[0]
                        && SharedProperties.registrationSessions) {
                    vm.allSessionsRegistration = SharedProperties.registrationSessions;
                    _.each(vm.allSessionsRegistration, function(session){
                        session.marked = false;
                    });
                } else{
                    vm.loadingRegistrationSessions = true;
                    ConferenceSessionsService
                        .getAllSessions(vm.conference_id)
                        .then(function(res){
                            SharedProperties.registrationSessions = res.sessions;
                            SharedProperties.registrationSessionsConferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                            vm.allSessionsRegistration = res.sessions;
                            _.each(vm.allSessionsRegistration, function(session){
                                session.marked = false;
                            });
                            vm.loadingRegistrationSessions = false;
                        })
                        .catch(function(err){

                            vm.loadingRegistrationSessions = false;
                        });
                    }
            };

			vm.verifEmpty = function(lv){
				if(lv !== null){
					var pr = _.find(lv.registrationlevelpricing_set, ['starts_at', null]);
					if(pr == undefined) {
						return true;
					}
					else return false;
				} else{
					return false;
				}
			}

			vm.addRegistrationBenefit = function(){
				vm.benefit.registration_level = vm.currentLevel;
				ConferenceRegistrationService
					.createRegistrationFormBenefits(vm.currentLevel, vm.benefit)
					.then(function(response){
						vm.registrationLevels.benefitsSet.push(response);
						vm.benefit = {
							'name' : ''
						};
					})
					.catch(function(error){
						vm.registrationBenefits = [];

					});
			}

            vm.getRevenueForSuperAdmin = function(conference_id){
                ConferenceService
                    .getSuperAdminRevenue(conference_id)
                    .then(function(response){
                        vm.exhibitor_revenue = response.exhibitor_data;
                                                vm.exhibitor_revenue.payment_method = lib.capitalizeFirstLetter(vm.exhibitor_revenue.payment_method);
                        vm.conference_revenue = response.conference_data;
                        vm.registration_revenue = response.registration_data;
                        vm.totalNumberOfRegistrants = getTotalNumberOfRegistrants(vm.registration_revenue.levels);
                        vm.overall_revenue = response.overall_data;
                        setOverallConferenceRevenueGraph(vm.overall_revenue);
                    }, function(error){
                        console.log(error)
                    });
            };

            function getTotalNumberOfRegistrants(levels) {
                var total_number = 0;
                _.each(levels, function (level) {
                    total_number += level.nr_registrants;
                });
                return total_number;
            }

            function setOverallConferenceRevenueGraph(overall_revenue_data){
                var exhibitor_rev = overall_revenue_data.total_exhibitor_revenue;
                var organizer_rev = overall_revenue_data.total_conference_organizer_revenue;
                var registration_rev = overall_revenue_data.total_registration_expo_revenue;

                var arr =
                    [
                        {
                            'label': 'Exhibitor $' + (Math.ceil(exhibitor_rev) >= 1000 ? Math.ceil(exhibitor_rev)/1000 : Math.ceil(exhibitor_rev) ) + (Math.ceil(exhibitor_rev) >= 1000 ? 'k' : ''),
                            'value': exhibitor_rev,
                            'color': '#8bd0ef'
                        },
                        {
                            'label': 'Organizer $' + (Math.ceil(organizer_rev) >= 1000 ? Math.ceil(organizer_rev)/1000 : Math.ceil(organizer_rev) ) + (Math.ceil(organizer_rev) >= 1000 ? 'k' : ''),
                            'value': organizer_rev,
                            'color': '#dbf4ff'
                        },
                        {
                            'label': 'Registration $' + (Math.ceil(registration_rev) >= 1000 ? Math.ceil(registration_rev)/1000 : Math.ceil(registration_rev) ) + (Math.ceil(registration_rev) >= 1000 ? 'k' : ''),
                            'value': registration_rev,
                            'color': '#26a7de'
                        }
                    ];
                lib.overallConferenceRevenueGraph(arr);
            }

			vm.initializeAddSessionList = function(){
				vm.addOnSessionModel = [];
				_.each(vm.currentLevelItem.registrationlevelsession_set, function(levelSession){
					if(!_.isArray(vm.addOnSessionModel[levelSession.session.id]) || _.isEmpty(vm.addOnSessionModel[levelSession.session.id])){
						vm.addOnSessionModel[levelSession.session.id] = [];
						vm.addOnSessionModel[levelSession.session.id].push({
							'price' : '',
							'starts_at' : '',
							'ends_at' : '',
							'session' : levelSession.session.id,
							'registration_level' : levelSession.registration_level
						});
					}
				});
			}

			vm.toggleAddOnSelection = function(levelSession){
                if(levelSession.session.hasOwnProperty('add_on_selected')){
                    levelSession.session.add_on_selected = !levelSession.session.add_on_selected;
                } else{
                    levelSession.session.add_on_selected = true;
                    if (!levelSession.session.addOnSession.registrationleveladdonsessionpricing_set) {
                        levelSession.session.addOnSession.registrationleveladdonsessionpricing_set = [];
                    }
                }
                if (!levelSession.registration_level) {
                    levelSession.registration_level = vm.currentLevel;
                }
                if(levelSession.session.add_on_selected){
                    ConferenceRegistrationService
                        .createRegistrationAddOnSession(levelSession.registration_level, levelSession.session.id)
                        .then(function(response){
                            _.each(response.registrationleveladdonsessionpricing_set, function(pricing){
                                pricing.starts_at = moment(pricing.starts_at);
                                pricing.ends_at = moment(pricing.ends_at);
                            });
                            levelSession.session.addOnSession = response;
                            if(!levelSession.session.addOnSession.hasOwnProperty('add_on_price_model')) {
                                vm.addExtraPricingForAddOnSession(levelSession);
                            } else {
                                levelSession.session.addOnSession.add_on_price_model.id = response.id;
                            }
                            levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
                                vm.sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
                            vm.currentLevelItem.registrationleveladdonsession_set.push(response);
                        })
                        .catch(function(error){

                        });
                } else{
                    var add_on_session_id = _.find(vm.currentLevelItem.registrationleveladdonsession_set, function(add_on_session){
                        if(add_on_session.session.id == levelSession.session.id){
                            return add_on_session.id;
                        }
                    });
                    ConferenceRegistrationService
                        .removeRegistrationAddOnSession(add_on_session_id.id)
                        .then(function(response){
                            vm.currentLevelItem.registrationleveladdonsession_set = _.reject(vm.currentLevelItem.registrationleveladdonsession_set, ['id', add_on_session_id.id]);
                        })
                        .catch(function(error){

                        });
                }
            };
            vm.updateChangesOnPricing = function(newValue, oldValue, pricing){
                vm.updatePricing(pricing);
            };

            vm.updateChangesOnAddOnPricing = function(newValue, oldValue, levelSession, add_on_price, type){
                vm.setAddOnSessionPrice(levelSession, add_on_price, type);
            };

            vm.findIndexOfThisDate = function(selectedDate, date_type, list_of_sorted_dates){
                if (!selectedDate) {
                    return vm.currentDate;
                }
                var date_index;
                if(date_type === 'start') {
                    date_index = _.findIndex(list_of_sorted_dates, function (price_date) {
                        if (!price_date) {
                            return false;
                        }
                        return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                } else if(date_type === 'end'){
                    date_index = _.findLastIndex(list_of_sorted_dates, function (price_date) {
                        if (!price_date) {
                            return false;
                        }
                        return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                }

                if(date_type === 'start' && date_index > 0){
                    date_index -= 1;
                } else if(date_type === 'end' && date_index < (list_of_sorted_dates.length - 1)) {
                    date_index += 1;
                } else if(date_index === (list_of_sorted_dates.length - 1) || date_index === 0 ) {
                    date_index = -1;
                }

                if(date_index != -1){
                    var date_to_return = list_of_sorted_dates[date_index];
                    if (!date_to_return) {
                        return false;
                    }
                    if(date_type === 'end'){
                        return date_to_return.clone().subtract(1, 'days').format('MM/DD/YYYY');
                    } else if(date_type === 'start'){
                        return date_to_return.clone().add(1, 'days').format('MM/DD/YYYY');
                    }
                } else if(date_type === 'end'){
                    return vm.conference_date_to_moment;
                } else if(date_type === 'start'){
                    return null;
                }

            };


        // calculate dates interval for add on sessions prices

        vm.sortDatesForAddsOnPrices = function(list_of_prices){
            var dates_array = [];
            _.each(list_of_prices, function (price_model) {
                if (price_model.starts_at_editable !== null && price_model.ends_at_editable !== null) {
                    dates_array.push(price_model.starts_at_editable);
                    dates_array.push(price_model.ends_at_editable);
                }
            });
            dates_array = _.sortBy(dates_array);
            return dates_array;
        };

        vm.verifyIfDateIsAvailableForAddOnSessionPrices = function(date, type, price_model, add_on, picker_type){
                var list_of_prices = add_on.registrationleveladdonsessionpricing_set;
                var sorted_dates_for_add_on = add_on.registration_adds_on_session_dates_sorted;

                if (date > vm.conference.date_to_editable) {
                    return false;
                }

                if (price_model && price_model.hasOwnProperty('starts_at_editable')) {
                    price_model.starts_at = price_model.starts_at_editable;
                }
                if (price_model && price_model.hasOwnProperty('ends_at_editable')) {
                    price_model.ends_at = price_model.ends_at_editable;
                }

                if(type !== 'day' || list_of_prices === undefined || list_of_prices.length === 0){
                    return true;
                }

                var ok = true;
                if(price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) {
                    var model_start_index = _.findIndex(sorted_dates_for_add_on, function(price_date){
                        return price_model.starts_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                    var model_end_index = _.findIndex(sorted_dates_for_add_on, function(price_date){
                        return price_model.ends_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                }

                if(picker_type === 'end'){
                    var price_date = _.find(sorted_dates_for_add_on, function(price_date){
                        if (!price_date || !price_model.starts_at) {
                            return null;
                        }
                        return price_date.format('MM/DD/YYYY') > price_model.starts_at.format('MM/DD/YYYY');
                    });
                    if(price_date === undefined){
                        ok = true;
                    } else if(price_date != undefined && (date.format('MM/DD/YYYY') < price_date.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY'))){
                        ok = true;
                    } else{
                        ok = false;
                    }
                    return ok;
                }

                if(!price_model.hasOwnProperty('starts_at') || !price_model.hasOwnProperty('ends_at')){
                    for(var i = 0; i < sorted_dates_for_add_on.length; i+=2){
                        var j = i + 1;
                        if(sorted_dates_for_add_on[i].format('MM/DD/YYYY') <= date.format('MM/DD/YYYY') &&
                            date.format('MM/DD/YYYY') <= sorted_dates_for_add_on[j].format('MM/DD/YYYY')){
                            ok = false;
                            break;
                        }
                    }
                } else if((price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) &&
                    (date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') <= price_model.ends_at.format('MM/DD/YYYY')) ||
                    (date.format('MM/DD/YYYY') < price_model.starts_at.format('MM/DD/YYYY') &&
                        (model_start_index > 0 && date.format('MM/DD/YYYY') > sorted_dates_for_add_on[model_start_index - 1].format('MM/DD/YYYY') ||
                        (model_start_index === 0 && date.format('MM/DD/YYYY') >= vm.currentDate))
                    ) ||
                    (date.format('MM/DD/YYYY') > price_model.ends_at.format('MM/DD/YYYY') &&
                        (
                            (model_end_index < (sorted_dates_for_add_on.length - 1) && date.format('MM/DD/YYYY') < sorted_dates_for_add_on[model_end_index + 1].format('MM/DD/YYYY'))
                            || (model_end_index === (sorted_dates_for_add_on.length - 1))
                        )
                    )
                ){
                    ok = true;
                } else {
                    ok = false;
                }

                return ok;
            };

        vm.sortedDatesForLevelPrices = [];

        vm.sortDatesForLevelPrices = function(list_of_prices){
            vm.sortedDatesForLevelPrices = [];
            _.each(list_of_prices, function (price_model) {
                if (price_model.starts_at_editable !== null && price_model.ends_at_editable !== null) {
                    vm.sortedDatesForLevelPrices.push(price_model.starts_at_editable);
                    vm.sortedDatesForLevelPrices.push(price_model.ends_at_editable);
                }
            });
            vm.sortedDatesForLevelPrices = _.sortBy(vm.sortedDatesForLevelPrices);
        };

        vm.verifyIfDateIsAvailable = function(date, type, price_model, list_of_prices, picker_type){

                if(type !== 'day'){
                    return true;
                }

                var ok = true;
                if (price_model && price_model.hasOwnProperty('starts_at_editable')) {
                    price_model.starts_at = price_model.starts_at_editable;
                }
                if (price_model && price_model.hasOwnProperty('ends_at_editable')) {
                    price_model.ends_at = price_model.ends_at_editable;
                }

                if(price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) {
                    var model_start_index = _.findIndex(vm.sortedDatesForLevelPrices, function(price_date){
                        return price_model.starts_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                    var model_end_index = _.findIndex(vm.sortedDatesForLevelPrices, function(price_date){
                        return price_model.ends_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
                    });
                }

                if(picker_type === 'end' && price_model != null && price_model !== undefined && price_model.hasOwnProperty('starts_at')){
                    var price_date = _.find(vm.sortedDatesForLevelPrices, function(price_date){
                        return price_date.format('MM/DD/YYYY') > price_model.starts_at.format('MM/DD/YYYY');
                    });

                    if(price_date === undefined){
                        ok = true;
                    } else if(price_date != undefined && (date.format('MM/DD/YYYY') < price_date.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY'))){
                        ok = true;
                    } else{
                        ok = false;
                    }
                    return ok;
                }

                if(price_model == null || price_model == undefined || !price_model.hasOwnProperty('starts_at') || !price_model.hasOwnProperty('ends_at')){
                    for(var i = 0; i < vm.sortedDatesForLevelPrices.length; i+=2){
                        var j = i + 1;
                        if(vm.sortedDatesForLevelPrices[i].format('MM/DD/YYYY') <= date.format('MM/DD/YYYY') &&
                            date.format('MM/DD/YYYY') <= vm.sortedDatesForLevelPrices[j].format('MM/DD/YYYY')){
                            ok = false;
                            break;
                        }
                    }
                } else if((price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) &&
                    (date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') <= price_model.ends_at.format('MM/DD/YYYY')) ||
                    (date.format('MM/DD/YYYY') < price_model.starts_at.format('MM/DD/YYYY') &&
                        (model_start_index > 0 && date.format('MM/DD/YYYY') > vm.sortedDatesForLevelPrices[model_start_index - 1].format('MM/DD/YYYY') ||
                        (model_start_index === 0 && date.format('MM/DD/YYYY') >= vm.currentDate))
                    ) ||
                    (date.format('MM/DD/YYYY') > price_model.ends_at.format('MM/DD/YYYY') &&
                        (
                            (model_end_index < (vm.sortedDatesForLevelPrices.length - 1) && date.format('MM/DD/YYYY') < vm.sortedDatesForLevelPrices[model_end_index + 1].format('MM/DD/YYYY'))
                            || (model_end_index === (vm.sortedDatesForLevelPrices.length - 1))
                        )
                    )
                ){
                    ok = true;
                } else {
                    ok = false;
                }

                return ok;
            };

            vm.setAddOnSessionPrice = function(levelSession, model, start){
                if(model.starts_at_editable && model.ends_at_editable && model.price && parseInt(model.price) > 0){
                    if(model.hasOwnProperty('id')){

                        if (model.starts_at_editable) {
                            var starts_at = new Date(model.starts_at_editable);
                            model.starts_at_raw = {
                                "year": starts_at.getFullYear(),
                                "month": starts_at.getMonth() + 1,
                                "day": starts_at.getDate()
                            }
                        }

                        if (model.ends_at_editable) {
                            var ends_at = new Date(model.ends_at_editable);
                            model.ends_at_raw = {
                                "year": ends_at.getFullYear(),
                                "month": ends_at.getMonth() + 1,
                                "day": ends_at.getDate()
                            }
                        }

                        ConferenceRegistrationService
                            .updateRegistrationAddOnSessionPricing(model.id, model)
                            .then(function(response){
                                response.starts_at = moment(response.starts_at);
                                response.ends_at = moment(response.ends_at);
                                response.starts_at_editable = vm.createMomentFromRawTime(response.starts_at_raw);
                                response.ends_at_editable = vm.createMomentFromRawTime(response.ends_at_raw);
                                var model_index = levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.indexOf(model);
                                levelSession.session.addOnSession.registrationleveladdonsessionpricing_set = _.reject(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set, ['id', model.id]);
                                levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.splice(model_index, 0, response);
                                levelSession.session.addOnSession.registration_adds_on_session_dates_sorted = vm.sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
                            }, function(error){
                                model.price = 0;
                                toastr.error(error.data.detail);
                            });
                    } else{
                        if (model.starts_at_editable) {
                            var starts_at = new Date(model.starts_at_editable);
                            model.starts_at_raw = {
                                "year": starts_at.getFullYear(),
                                "month": starts_at.getMonth() + 1,
                                "day": starts_at.getDate()
                            }
                        }

                        if (model.ends_at_editable) {
                            var ends_at = new Date(model.ends_at_editable);
                            model.ends_at_raw = {
                                "year": ends_at.getFullYear(),
                                "month": ends_at.getMonth() + 1,
                                "day": ends_at.getDate()
                            }
                        }
                        ConferenceRegistrationService
                            .createRegistrationAddOnSessionPricing(levelSession.session.addOnSession.id, model)
                            .then(function(response){
                                response.starts_at = moment(response.starts_at);
                                response.ends_at = moment(response.ends_at);
                                response.starts_at_editable = vm.createMomentFromRawTime(response.starts_at_raw);
                                response.ends_at_editable = vm.createMomentFromRawTime(response.ends_at_raw);
                                response.can_be_remove = true;
                                levelSession.session.addOnSession.add_on_price_model = null;
                                levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.push(response);
                                levelSession.session.addOnSession.registration_adds_on_session_dates_sorted = vm.sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);

                            }, function(error){
                                model.price = 0;
                                toastr.error(error.data.detail);
                            });
                    }
                }
            };

        function validateDateInterval(start_date, end_date){
		    var ok = true;
		    if(start_date > end_date){
		        ok = false;
            }
            return ok;
        }

            function validatePriceInterval(list_of_prices, start_date, end_date, price_model){
                var ok = false;
                if(list_of_prices.length > 0) {
                    _.each(list_of_prices, function (price_detail) {
                        if ((price_detail.hasOwnProperty('id') && price_model.id !== price_detail.id) || (!price_model.hasOwnProperty('id') && price_model !== price_detail)) {
                            var start_date_price_from_list = new Date(price_detail.starts_at),
                                end_date_price_from_list = new Date(price_detail.ends_at);

                            if ((start_date > start_date_price_from_list && end_date < end_date_price_from_list) || (end_date < start_date_price_from_list || start_date > end_date_price_from_list)
                                || (start_date_price_from_list > start_date && end_date_price_from_list < end_date) || (end_date_price_from_list < start_date || start_date_price_from_list > end_date)) {
                                ok = true;
                            } else {
                                ok = false;
                                return false;
                            }
                        } else {
                            ok = true;
                        }
                    });
                } else{
                    ok = true;
                }
                return ok;
            }

			vm.removePricingFromAddOnSession = function(levelSession, model){
                if(!model.hasOwnProperty('id')){
                    levelSession.session.addOnSession.add_on_price_model = null;
                    return;
                }
                ConferenceRegistrationService
                    .removeRegistrationAddOnSessionPricing(model.id)
                    .then(function(response){
                        levelSession.session.addOnSession.registrationleveladdonsessionpricing_set =
                            _.reject(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set, ['id', model.id]);

                        if(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length <= 0){
                            vm.addExtraPricingForAddOnSession(levelSession);
                        } else{
                            levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
                                vm.sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
                        }
                    }, function(error){
                        toastr.error(error.data.detail);
                    });
            };

			vm.addExtraPricingForAddOnSession = function(levelSession){
                levelSession.session.addOnSession.add_on_price_model = {
                    session: levelSession.session.id,
                    registration_level: levelSession.registration_level,
                    price: null
                };
            };

			vm.includeContinuingEducationInLevel = function(answer){
				if(answer){
					vm.currentLevelItem.include_continuing_education = true;
				} else{
					vm.currentLevelItem.include_continuing_education = false;
				}
				ConferenceRegistrationService
					.updateRegistrationLevel(vm.currentLevelItem)
					.then(function(response){
					})
					.catch(function(error){

					});
			};

			vm.includePreRegistrationInLevel = function(answer){
				if(answer){
					vm.currentLevelItem.include_pre_registration = true;
				} else{
					vm.currentLevelItem.include_pre_registration = false;
				}
				ConferenceRegistrationService
					.updateRegistrationLevel(vm.currentLevelItem)
					.then(function(response){
					})
					.catch(function(error){

					});
			};

			vm.registrationPromoCodeModel = {
                single_use: true
            };

			vm.generatePromoCode = function(){
				vm.registrationPromoCodeModel.code = '';
				var length = 0;
				while(length <= 7){
					var chr_number = Math.floor(Math.random() * (91 - 65) + 65);
					var character = String.fromCharCode(chr_number);
					if( Math.floor(Math.random() * (3 - 1) + 1) % 2 == 0){
						vm.registrationPromoCodeModel.code += character;
					} else{
						vm.registrationPromoCodeModel.code += String.fromCharCode(Math.floor(Math.random() * (58 - 48) + 48));
					}
					length++;
				}
			};

			vm.createRegistrationPromoCode = function(){
				var ok = _.find(vm.registrationPromoCodeModel, function(promo_code_prop){
					return promo_code_prop == '';
				});

                if(!vm.registrationPromoCodeModel.hasOwnProperty('starts_at_editable') ||
                        !vm.registrationPromoCodeModel.hasOwnProperty('ends_at_editable') ||
                        !vm.registrationPromoCodeModel.hasOwnProperty('value')) {
                    ok = true;
                }
                if (!vm.registrationPromoCodeModel.value ||
                        vm.registrationPromoCodeModel.value <= 0) {
                    toastr.error('Please chose a promo code value higher than 0');
                    ok = true;
                    return;
                }
				var discount = document.getElementById('promo-code-dicount-type').value;
				if(discount == "USD"){
					vm.registrationPromoCodeModel.discount_type = 'usd';
				} else{
					vm.registrationPromoCodeModel.discount_type = 'pct';
				}
				if(ok == undefined || ok == false){
                    if (vm.registrationPromoCodeModel.starts_at_editable) {
                        var starts_at = new Date(vm.registrationPromoCodeModel.starts_at_editable);
                        vm.registrationPromoCodeModel.starts_at_raw = {
                            "year": starts_at.getFullYear(),
                            "month": starts_at.getMonth() + 1,
                            "day": starts_at.getDate()
                        }
                    }

                    if (vm.registrationPromoCodeModel.ends_at_editable) {
                        var ends_at = new Date(vm.registrationPromoCodeModel.ends_at_editable);
                        vm.registrationPromoCodeModel.ends_at_raw = {
                            "year": ends_at.getFullYear(),
                            "month": ends_at.getMonth() + 1,
                            "day": ends_at.getDate()
                        }
                    }

					ConferenceRegistrationService
						.addRegistrationPromoCodes(vm.currentLevel, vm.registrationPromoCodeModel)
						.then(function(response){
							response.starts_at = moment(response.starts_at);
							response.ends_at = moment(response.ends_at);
                            response.starts_at_editable = vm.createMomentFromRawTime(response.starts_at_raw);
                            response.ends_at_editable = vm.createMomentFromRawTime(response.ends_at_raw);

							vm.currentLevelItem.registrationlevelpromocode_set.push(response);
							vm.registrationPromoCodeModel = {};
						})
						.catch(function(error){
                            if(typeof error.data.detail === 'object') {
                                var firstField = Object.keys(error.data.detail)[0];
                                var firstFieldFirstError = error.data.detail[firstField][0];
                                var errorMessage = firstField + ": " + firstFieldFirstError;
                                toastr.error(errorMessage);
                            } else {
                                toastr.error(error.data.detail);
                            }

						});
				} else{
                    toastr.error('Wrong promo code!');
				}
			};

			vm.deleteRegistrationPromoCode = function(promo_code_id){
				ConferenceRegistrationService
					.removeRegistrationPromoCode(promo_code_id)
					.then(function(response){
						vm.currentLevelItem.registrationlevelpromocode_set = _.reject(vm.currentLevelItem.registrationlevelpromocode_set, ['id', promo_code_id]);
					})
					.catch(function(error){

					});
			}

			vm.updateRegistrationPromoCode = function(promo_code_item){
				var ok = _.find(promo_code_item, function(promo_code_prop){
					return promo_code_prop !== 'single_use' && promo_code_prop === '';
				});
                if (!promo_code_item.value || promo_code_item.value <= 0) {
                    toastr.error('Please chose a promo code value higher than 0');
                    ok = true;
                    return;
                }
				if(ok == undefined){
                    if (promo_code_item.starts_at_editable) {
                        var starts_at = new Date(promo_code_item.starts_at_editable);
                        promo_code_item.starts_at_raw = {
                            "year": starts_at.getFullYear(),
                            "month": starts_at.getMonth() + 1,
                            "day": starts_at.getDate()
                        }
                    }

                    if (promo_code_item.ends_at_editable) {
                        var ends_at = new Date(promo_code_item.ends_at_editable);
                        promo_code_item.ends_at_raw = {
                            "year": ends_at.getFullYear(),
                            "month": ends_at.getMonth() + 1,
                            "day": ends_at.getDate()
                        }
                    }

					ConferenceRegistrationService
					.updateRegistrationPromoCode(promo_code_item.id, promo_code_item)
					.then(function(response){
					    toastr.success("Promo code updated!");
					})
					.catch(function(error){
						toastr.error(error.data.detail);
                        promo_code_item.value = null;
					});
				}
			}

			vm.uploadLevelMembers = function(files){
				var reader = new FileReader();
                reader.onload = function(e) {
                    var data = lib.CSV2JSON(reader.result);
                    vm.membersCsvFields = data;
                   	var keys = Object.keys(data[0]);
                   	vm.membersCsvKeys = [];
                   	_.each(keys, function(csvField) {
                       vm.membersCsvKeys.push({
                        'code': csvField
                       });
                   	});
                    vm.memberFileName = files[0].name;

                    vm.memberFieldMapping = {};
                    vm.memberFieldMappingObj = {};
                    vm.memberFieldMappingCode = {};

                    _.each(vm.membersCsvKeys, function(csvField) {
                        csvField.mapping = null;
                    });

                    _.each(vm.memberMappingFields, function(field) {
                        field.mapping = null;
                        field.is_hovered = null;
                    });

                    vm.openMapMemberInformation();

                   $scope.$apply();
                }

                reader.readAsText(files[0], 'ISO-8859-1');
			};

            vm.registrant_member_model = [{
                'first_name' : '',
                'last_name' : '',
                'email': ''
            }];

            // function validateMemberEmail(email){
            //     if(email.indexOf('@') < 1 || email.indexOf('.') <= email.indexOf('@') ){
            //         toastr.error("Member email is incorrect!");
            //         return false;
            //     } else{
            //         return true;
            //     }
            // };

            vm.addRegistrationMember = function(){
                var empty = _.find(vm.registrant_member_model[0], _.isEmpty());
                if(empty === undefined){
                    ConferenceRegistrationService
                        .bulkCreateMembers(vm.registrant_member_model, vm.currentLevel)
                        .then(function(response){
                            ConferenceRegistrationService
                                .getRegistrationMembers(vm.currentLevel)
                                .then(function(membersList){
                                    vm.currentLevelItem.registrationlevelmember_set = membersList;
                                });
                            vm.registrant_member_model = [{
                                'first_name' : '',
                                'last_name' : '',
                                'email': ''
                            }];
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            };

            vm.memebersSelectedBool = false;
            vm.selectRegistrationMember = function(member){
                if(member){
                    if(member.selected){
                        member.selected = !member.selected;
                    } else{
                        member.selected = true;
                    }
                }
                var ok = false;
                if(vm.currentLevelItem.registrationlevelmember_set){
                    _.each(vm.currentLevelItem.registrationlevelmember_set, function(member){
                        if(member.selected && member.selected == true){
                            ok = true;
                        }
                    });
                } if(ok){
                    vm.memebersSelectedBool = true;
                } else{
                    vm.memebersSelectedBool = false;
                }
            }

            vm.removeRegistrationMembers = function(){
                var memebersToRemove = '';
                _.each(vm.currentLevelItem.registrationlevelmember_set, function(member){
                    if(member.selected && member.selected == true){
                        memebersToRemove += member.id + ',';
                    }
                });
                memebersToRemove = memebersToRemove.substring(0, (memebersToRemove.length - 1));
                ConferenceRegistrationService
                    .removeRegistrationMembers(vm.currentLevel, memebersToRemove)
                    .then(function(response){
                        vm.currentLevelItem.registrationlevelmember_set = _.reject(vm.currentLevelItem.registrationlevelmember_set, function(member){
                            if(member.selected && member.selected == true){
                                return member;
                            }
                        });
                        vm.memebersSelectedBool = false;
                    })
                    .catch(function(error){

                    });
            };

            vm.openViewMembersPopup = function(level){
                $scope.members = level.registrationlevelmember_set;
                ngDialog.open({template: 'app/views/partials/conference/registration-steps/registration_form_level_members.html',
                   className: 'app/stylesheets/_registrationReview.scss',
                   scope: $scope
                });
            };

            vm.usd_sign = true;
			vm.changeDiscountType = function(){
				if(document.getElementById('promo-code-dicount-type').value == 'USD'){
                    vm.usd_sign = false;
					document.getElementById('promo-code-dicount-type').value = 'PCT';
					document.getElementById('promo-code-dicount-value').innerHTML = '%';
				} else{
                    vm.usd_sign = true;
					document.getElementById('promo-code-dicount-type').value = 'USD';
					document.getElementById('promo-code-dicount-value').innerHTML = '$';
				}
			};

			vm.uploadRegistrationTrmAndCond = function(files){
                if (!files[0]) {
                    return;
                } else if(files[0].size === 0){
                    toastr.error("You can't upload an empty file");
                    return;
                }
				if(files[0].type == 'text/plain') {
                    FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var file_name = files[0].name;
                        var file_type = files[0].type;
                        FileService.uploadFile(files[0], response)
                            .then(function(uploadResponse){
                                ConferenceRegistrationService
                                    .uploadRegistrationFormLegalAgreements(vm.registration_form_id, file_link, 'terms_and_conditions', file_name, file_type)
                                    .then(function(response){
                                        vm.conference.registration_form.terms_of_service_file_name = response.terms_of_service_file_name;
                                        ConferenceRegistrationService
                                            .getFileContent(file_link)
                                            .then(function(response){
                                                vm.termsAndConditionsText = response.data;
                                            })
                                            .catch(function(error){

                                            });
                                    })
                                    .catch(function(error){
                                    });
                            })
                            .catch(function(error){
                            });
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
                } else {
                    toastr.error("The file must be a .txt file.");
                }

			};

            vm.openTermsAndContitionsPopup = function(){
                ngDialog.open({
                    template: 'app/views/partials/conference/registration-steps/registration_form_terms_and_contidions.html',
                    className: 'app/stylesheets/_registrationFrom6.scss',
                    data: {
                        legal: {
                            terms_of_service: vm.termsAndConditionsText
                        }
                    }
                });
            };

            vm.completeRegistrationForm = function() {
                ConferenceRegistrationService
                    .registratioFormComplete(vm.registration_form_id)
                    .then(function(response){
                        SharedProperties.registrationForm = null;
                        $state.go('home.registration', {conference_id: vm.conference_id}, {reload: true});
                    })
                    .catch(function(error){

                    });
            }

			vm.uploadRegistrationRefundPolicy = function(files){
                if (!files[0]) {
                    return;
                } else if(files[0].size === 0){
                    toastr.error("You can't upload an empty file");
                    return;
                }
				if(files[0].type == 'text/plain') {
                    FileService.getUploadSignature(files[0])
                        .then(function(response){
                            var file_link = response.data.file_link;
                            var file_name = files[0].name;
                            var file_type = files[0].type;
                            FileService.uploadFile(files[0], response)
                                .then(function(uploadResponse){
                                    ConferenceRegistrationService
                                        .uploadRegistrationFormLegalAgreements(vm.registration_form_id, file_link, 'refund_policy', file_name, file_type)
                                        .then(function(response){
                                            vm.conference.registration_form.refund_policy_file_name = response.refund_policy_file_name;
                                            ConferenceRegistrationService
                                                .getFileContent(file_link)
                                                .then(function(response){
                                                    vm.refundPolicyText = response.data;
                                                })
                                                .catch(function(error){

                                                });
                                        })
                                        .catch(function(error){

                                        });
                                })
                                .catch(function(error){

                                });
                        })
                        .catch(function(error){

                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error('Error!', 'There has been a problem with your request.');
                            }
                        });
                } else {
                    toastr.error("The file must be a .txt file.");
                }

			};

            vm.openRefundPolicyPopup = function(){
                ngDialog.open({
                    template: 'app/views/partials/conference/registration-steps/registration_form_refund_policy.html',
                    className: 'app/stylesheets/_registrationFrom6.scss',
                    data: {
                        legal: {
                            refund_policy: vm.refundPolicyText
                        }
                    }
                });
            };

			vm.uploadAdditionalLegalAgreements = function(files){
                if(files[0].size === 0){
                    toastr.warning("You can't upload an empty file");
                    return;
                }
				FileService.getUploadSignature(files[0])
				.then(function(response){
					var file_link = response.data.file_link;
                    var file_name = files[0].name;
                    var file_type = files[0].type;
					FileService.uploadFile(files[0], response)
						.then(function(uploadResponse){
							ConferenceRegistrationService
								.uploadRegistrationFormAdditionalLegalAgreements(vm.registration_form_id, file_link, file_name, file_type)
								.then(function(response){
									vm.conference.registration_form.registrationformdocument_set.push(response);
								})
								.catch(function(error){

								});
						})
						.catch(function(error){

						});
				})
				.catch(function(error){

					if (error.status == 403) {
						toastr.error('Permission Denied!');
					} else {
						toastr.error('Error!', 'There has been a problem with your request.');
					}
				});
			};

            vm.dropDownTypeOfField = function(e){
                e.stopPropagation();
                if(e.target.nextElementSibling.style.visibility == 'hidden' || _.isEmpty(e.target.nextElementSibling.style.visibility)){
                    e.target.nextElementSibling.style.visibility = "visible";
                } else{
                    e.target.nextElementSibling.style.visibility = "hidden";
                }
            };

			if (selectedPane) {
			    vm.selectedPane = selectedPane;
                SharedProperties.sharedObject.selectedPane = vm.selectedPane;
			}

            $scope.$watch(function(){
                return SharedProperties.sharedObject.selectedPane;
            }, function(view){
                if(view !== vm.selectedPane){
                    _.each(vm.conferenceUsers, function(user){
                        if(user.checked)
                        user.checked = false;
                    });
                    _.each(vm.exhibitors, function(exhibitor){
                        if(exhibitor.checked)
                        exhibitor.checked = false;
                    });
                    _.each(vm.attendees, function(attendee){
                        if(attendee.checked)
                        attendee.checked = false;
                    });
                }
            });

            vm.selectedType = null;
			if (selectedType) {
			    vm.selectedType = selectedType;
			}

			vm.user = SharedProperties.user;

			vm.conferenceDays = [];


			initiateCustomFieldsData(vm);
            vm.conferenceStreetAddress = '';
            vm.hasAttendeeId = false;
            vm.edit_travel_item = false;
            vm.plus_travel = false;
            vm.defaultHotels = presets.defaultHotels();
            vm.defaultCarRentals = presets.defaultCarRentals();
            vm.defaultAirlines = presets.defaultAirlines();
            vm.defaultTransportations = presets.defaultTransportations();

            vm.getDefaultTravelLogo = function(option){
                var s3_link = 'https://s3.amazonaws.com/expo-static/resources/travel/', index;
                if(vm.travelSelected == 'hotel'){
                    s3_link = s3_link + 'hotel_logos/';
                    index = vm.defaultHotels.indexOf(option) + 1;
                    vm.newTravelType.logo = s3_link + 'logo_hotel' + index + '.png';
                } else if(vm.travelSelected == 'car_rental'){
                    s3_link = s3_link + 'car_rental_logos/';
                    index = vm.defaultCarRentals.indexOf(option) + 1;
                    vm.newTravelType.logo = s3_link + 'logo_car' + index + '.png';
                } else if(vm.travelSelected == 'airline'){
                    s3_link = s3_link + 'airline_logos/';
                    index = vm.defaultAirlines.indexOf(option) + 1;
                    vm.newTravelType.logo = s3_link + 'logo_airline' + index + '.png';
                } else if(vm.travelSelected == 'transportation'){
                    s3_link = s3_link + 'transportation_logos/';
                    index = vm.defaultTransportations.indexOf(option) + 1;
                    vm.newTravelType.logo = s3_link + 'logo_transportation' + index + '.png';
                }
            };

            vm.sortTravelType = function(){
                vm.travel[vm.travelSelected] = _.orderBy(vm.travel[vm.travelSelected], 'name', vm.travel_type_table_order);
                if(vm.travel_type_table_order == 'asc'){
                    vm.travel_type_table_order = 'desc';
                } else{
                    vm.travel_type_table_order = 'asc';
                }
            }

            vm.addTravelType = function(){
                vm.newTravelType = presets.newTravelType();
                vm.newTravelType.country = vm.all_countries[0].name;
                vm.newTravelType.state = vm.united_states[0].abbreviation;
                vm.count_country = 0;
                vm.count_state = 0;
                vm.newTravelType.label = vm.travelSelected;
                vm.plus_travel = true;
                if(vm.travelSelected == 'hotel'){
                    vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                } else if(vm.travelSelected == 'car_rental'){
                    vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                } else if(vm.travelSelected == 'airline'){
                    vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                } else {
                    vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                }
            };

            $scope.$watch(
                function(){
                    return vm.travelSelected;
                },
                function(travel_type){
                    if(travel_type == 'hotel' || travel_type == 'car_rental'){
                        vm.newTravelType = {
                            'name': '',
                            'city': '',
                            'state': vm.united_states[0].abbreviation,
                            'street_address': '',
                            'website': '',
                            'special_instructions': '',
                            'logo': '',
                            'image': '',
                            'country': vm.all_countries[0].name,
                            'group_code': '',
                            'phone_number': '',
                            'price_range_start': '',
                            'price_range_end': '',
                            'zip_code': ''
                        };
                    } else{
                        vm.newTravelType = {
                            'name': '',
                            'logo': '',
                            'group_code': '',
                            'discount': '',
                            'website': '',
                            'image': ''
                        };
                    }
                    if(travel_type == 'hotel'){
                        vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                    } else if(travel_type == 'car_rental'){
                        vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                    } else if(travel_type == 'airline'){
                        vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                    } else {
                        vm.newTravelType.image = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                    }
                }
            );

            vm.displayTextForUser = function(){
                if(vm.travelSelected.indexOf('_') < 0){
                    return vm.travelSelected[0].toUpperCase() + vm.travelSelected.slice(1, vm.travelSelected.length);
                } else{
                    var words = vm.travelSelected.split('_');
                    return words[0][0].toUpperCase() + words[0].slice(1, words[0].length) + ' ' + words[1][0].toUpperCase() + words[1].slice(1, words[1].length);
                }
            };

            // Upload a travel type logo
            // Get the calculated signature version 4 url from the backend
            // Upload the image to the s3 bucket using that presigned url
            // store the image url in the payload that will be sent to the backend for creating the travel type
            vm.uploadTravelTypeLogo = function(files) {
                lib.squareifyImage($q, files)
                    .then(function(response){
                        var imgGile = response.file;

                        FileService.getUploadSignature(imgGile)
                            .then(function(response){
                                var file_link = response.data.file_link;
                                FileService.uploadFile(imgGile, response)
                                    .then(function(uploadResponse){
                                        vm.newTravelType.logo = file_link;
                                    })
                                    .catch(function(error){

                                    });
                            })
                            .catch(function(error){

                                if (error.status == 403) {
                                    toastr.error('Permission Denied!');
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                }
                            });
                    });

            };

            // Upload a travel type image
            // Get the calculated signature version 4 url from the backend
            // Upload the image to the s3 bucket using that presigned url
            // store the image url in the payload that will be sent to the backend for creating the travel type
            vm.uploadTravelTypeImage = function(files) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        FileService.uploadFile(files[0], response)
                            .then(function(uploadResponse){
                                vm.newTravelType.image = file_link;
                            })
                            .catch(function(error){

                            });
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            };

            vm.editThisTravelItem = function(travel_item){
                vm.count_country = -1;
                _.each(vm.all_countries, function(country){
                    vm.count_country++;
                    if(_.isString(travel_item.country) && country.name.trim() == travel_item.country.trim()){
                        return false;
                    }
                });
                vm.count_state = 0;
                if(travel_item.hasOwnProperty('country') && travel_item.country.trim() == 'United States'){
                    _.each(vm.united_states, function(state){
                        if(state.abbreviation != travel_item.state){
                            vm.count_state++;
                        } else{
                            return false;
                        }
                    });
                }
                vm.newTravelType = _.cloneDeep(travel_item);
                vm.plus_travel = true;
                vm.edit_travel_item = true;
            };

            vm.travelCountryUpdate = function(modelItem) {
                if (modelItem.country.hasOwnProperty('name') ? modelItem.country.name.trim() == 'United States' : modelItem.country.trim() == 'United States') {
                    vm.count_state = 0;
                    modelItem.state = vm.united_states[0].abbreviation;
                } else{
                }
            }

            // Removes a travel & lodging option
            // After the backend response it removes the option from the frontend app view
            // TODO -- test functionality of removing from the frond app view before the backend response for smoother experience
            vm.deleteTravelItem = function(){
                if(vm.travelSelected == 'hotel'){
                    ConferenceService
                        .deleteHotel(vm.newTravelType.id)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.newTravelType = {};
                            vm.plus_travel = false;
                            vm.edit_travel_item = false;
                        })
                        .catch(function(error){

                        });
                } else if(vm.travelSelected == 'car_rental'){
                    ConferenceService
                        .deleteCarRental(vm.newTravelType.id)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.newTravelType = {};
                            vm.plus_travel = false;
                            vm.edit_travel_item = false;
                        })
                        .catch(function(error){

                        });
                } else if(vm.travelSelected == 'airline'){
                    ConferenceService
                        .deleteAirline(vm.newTravelType.id)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.newTravelType = {};
                            vm.plus_travel = false;
                            vm.edit_travel_item = false;
                        })
                        .catch(function(error){

                        });
                } else if(vm.travelSelected == 'transportation'){
                    ConferenceService
                        .deleteTransportation(vm.newTravelType.id)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.newTravelType = {};
                            vm.plus_travel = false;
                            vm.edit_travel_item = false;
                        })
                        .catch(function(error){

                        });
                }
            };

            vm.changeLevelGuestsNr = function(level) {
                ConferenceRegistrationService.updateRegistrationLevel(level)
                    .then(function(response) {
                    }).catch(function(error){
                        toastr.error(error.data.detail);
                    });
            }

            vm.selectTravelTypeToAdd = function(flag_for_another){
                if(vm.travelSelected == 'hotel'){
                    vm.addHotel(flag_for_another);
                } else if(vm.travelSelected == 'car_rental'){
                    vm.addCarRental(flag_for_another);
                } else if(vm.travelSelected == 'airline'){
                    vm.addAirline(flag_for_another);
                } else if(vm.travelSelected == 'transportation'){
                    vm.addTransportation(flag_for_another);
                }
                return;
            };




            // Add hotel option
            // Validates if the payload contains the required fields
            // TODO better error handling
            vm.lockTravelButtons = false;
            vm.addHotel = function(flag_for_another){
                var ok = true;
                _.each(vm.newTravelType, function(val, key){
                    if((key == 'price_range_start' || key == 'price_range_end') && val == ''){
                        vm.newTravelType[key] = 0;
                    } else if((key == 'price_range_start' ||  key == 'price_range_end') && val != ''){
                        vm.newTravelType[key] = Number(vm.newTravelType[key]);
                    } else if(key == 'image' && val == ''){
                        vm.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                    }
                });

                if(_.isEmpty(vm.newTravelType.name)){
                    toastr.error('The name is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.phone_number)){
                    toastr.error('The phone number is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.street_address)){
                    toastr.error('The street address is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.city)){
                    toastr.error('The city is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.zip_code)){
                    toastr.error('The zip code is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.country)){
                    toastr.error('The country is a required field!');
                    ok = false;
                    return;
                }

                if(vm.newTravelType.country.trim() != 'United States'){
                    vm.newTravelType.state = '';
                } else if(vm.newTravelType.state == '' || vm.newTravelType.state == '--'){
                    toastr.error('The state is a required field!');
                    ok = false;
                    return;
                }

                if(Number(vm.newTravelType.price_range_start) >= Number(vm.newTravelType.price_range_end)){
                    toastr.error('"Start Price" must be lower then "End Price"!');
                    ok = false;
                    return;
                }

								if(Number(vm.newTravelType.price_range_start) < 0 || Number(vm.newTravelType.price_range_end) <0){
                    toastr.error('Price range can\'t be negative!');
                    ok = false;
                    return;
                }

                if(ok && vm.newTravelType.id == undefined && !vm.lockTravelButtons){
                    vm.lockTravelButtons = true;
                    ConferenceService
                        .createHotel(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'city': '',
                                'state': vm.united_states[0].abbreviation,
                                'street_address': '',
                                'website': '',
                                'special_instructions': '',
                                'logo': '',
                                'image': 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png',
                                'country': vm.all_countries[0].name,
                                'group_code': '',
                                'phone_number': '',
                                'price_range_start': '',
                                'price_range_end': '',
                                'zip_code': ''
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                            vm.lockTravelButtons = false;
                        })
                        .catch(function(error){
                            vm.lockTravelButtons = false;
                            lib.processValidationError(error, toastr);
                        });
                }  else if(ok && vm.newTravelType.id != undefined && !vm.lockTravelButtons){
                    ConferenceService
                        .updateHotel(vm.newTravelType.id, vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'city': '',
                                'state': vm.united_states[0].abbreviation,
                                'street_address': '',
                                'website': '',
                                'special_instructions': '',
                                'logo': '',
                                'image': 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png',
                                'country': vm.all_countries[0].name,
                                'group_code': '',
                                'phone_number': '',
                                'price_range_start': '',
                                'price_range_end': '',
                                'zip_code': ''
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            };

            // Add car rental option
            // Validates if the payload contains the required fields
            // TODO better error handling
            vm.addCarRental = function(flag_for_another){
                var ok = true;
                _.each(vm.newTravelType, function(val, key){
                    if((key == 'price_range_start' ||  key == 'price_range_end') && val == ''){
                        vm.newTravelType[key] = 0;
                    } else if((key == 'price_range_start' ||  key == 'price_range_end') && val != ''){
                        vm.newTravelType[key] = Number(vm.newTravelType[key]);
                    } else if(key == 'image' && val == ''){
                        vm.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                    }
                });

                if(_.isEmpty(vm.newTravelType.name)){
                    toastr.error('The name is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.phone_number)){
                    toastr.error('The phone number is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.street_address)){
                    toastr.error('The street address is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.city)){
                    toastr.error('The city is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.zip_code)){
                    toastr.error('The zip code is a required field!');
                    ok = false;
                    return;
                }

                if(_.isEmpty(vm.newTravelType.country)){
                    toastr.error('The country is a required field!');
                    ok = false;
                    return;
                }

                if(Number(vm.newTravelType.price_range_start) >= Number(vm.newTravelType.price_range_end)){
                    toastr.error('"Start Price" must be lower then "End Price"!');
                    ok = false;
                }

                if(vm.newTravelType.country.trim() != 'United States'){
                    vm.newTravelType.state = '';
                } else if(vm.newTravelType.state == '' || vm.newTravelType.state == '--'){
                    toastr.error('The state is a required field!');
                    ok = false;
                }

                if(ok && vm.newTravelType.id == undefined && !vm.lockTravelButtons){
                    vm.lockTravelButtons = true;
                    ConferenceService
                        .createCarRental(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'city': '',
                                'state': vm.united_states[0].abbreviation,
                                'street_address': '',
                                'website': '',
                                'special_instructions': '',
                                'logo': '',
                                'image': 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png',
                                'country': vm.all_countries[0].name,
                                'group_code': '',
                                'phone_number': '',
                                'price_range_start': '',
                                'price_range_end': '',
                                'zip_code': ''
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                            vm.lockTravelButtons = false;
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                            vm.lockTravelButtons = false;
                        });
                } else if(ok && vm.newTravelType.id != undefined && !vm.lockTravelButtons){
                    ConferenceService
                        .updateCarRental(vm.newTravelType.id, vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'city': '',
                                'state': vm.united_states[0].abbreviation,
                                'street_address': '',
                                'website': '',
                                'special_instructions': '',
                                'logo': '',
                                'image': 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png',
                                'country': vm.all_countries[0].name,
                                'group_code': '',
                                'phone_number': '',
                                'price_range_start': '',
                                'price_range_end': '',
                                'zip_code': ''
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            };

            // Add airline option
            // Validates if the payload contains the name (required field)
            // TODO better error validation
            vm.addAirline = function(flag_for_another){
                var ok = true;
                _.each(vm.newTravelType, function(val, key){
                    if(key == 'image' && val == ''){
                        vm.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                    }
                });

                if(_.isEmpty(vm.newTravelType.name)){
                    toastr.error('The name is a required field!');
                    ok = false;
                    return;
                }

                if(ok && vm.newTravelType.id == undefined && !vm.lockTravelButtons){
                    vm.lockTravelButtons = true;
                    ConferenceService
                        .createAirline(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'logo': '',
                                'group_code': '',
                                'discount': '',
                                'website': '',
                                'image': 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png'
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                            vm.lockTravelButtons = false;
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                            vm.lockTravelButtons = false;
                        });
                }  else if(ok && vm.newTravelType.id != undefined && !vm.lockTravelButtons){
                    ConferenceService
                        .updateAirline(vm.newTravelType.id, vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'logo': '',
                                'group_code': '',
                                'discount': '',
                                'website': '',
                                'image': 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png'
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            };

            // Add transportation option
            // Validates if the payload contains the name (required field)
            // TODO better error validation
            vm.addTransportation = function(flag_for_another){
                var ok = true;
                _.each(vm.newTravelType, function(val, key){
                    if(key == 'image' && val == ''){
                        vm.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                    }
                });

                if(_.isEmpty(vm.newTravelType.name)){
                    toastr.error('The name is a required field!');
                    ok = false;
                    return;
                }

                if(ok && vm.newTravelType.id == undefined && !vm.lockTravelButtons){
                    vm.lockTravelButtons = true;
                    ConferenceService
                        .createTransportation(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'logo': '',
                                'group_code': '',
                                'discount': '',
                                'website': '',
                                'image': 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png'
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                            vm.lockTravelButtons = false;
                        })
                        .catch(function(error){
                            vm.lockTravelButtons = false;
                            lib.processValidationError(error, toastr);
                        });
                }  else if(ok && vm.newTravelType.id != undefined && !vm.lockTravelButtons){
                    ConferenceService
                        .updateTransportation(vm.newTravelType.id, vm.newTravelType)
                        .then(function(response){
                            vm.travel[vm.travelSelected] = _.reject(vm.travel[vm.travelSelected], ['id', vm.newTravelType.id]);
                            vm.travel[vm.travelSelected].push(response);
                            if(flag_for_another == true){
                                vm.plus_travel = true;
                            } else{
                                vm.plus_travel = false;
                            }
                            vm.newTravelType = {
                                'name': '',
                                'logo': '',
                                'group_code': '',
                                'discount': '',
                                'website': '',
                                'image': 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png'
                            };
                            vm.edit_travel_item = false;
                            $('#travelPage').scrollTop(0);
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                }
            };

            // Get travel and lodging data from the backend
            // Stores the data in the travel parameter to be used in the travel and lodging screens
            vm.requestOnRolling = false;
            vm.getTravelItems = function(){
                if(vm.newTravelToLoad == true){

                    SharedProperties.travelItems = {};
                    vm.requestOnRolling = true;
                    vm.newTravelToLoad = false;

                    $q.all([
                        ConferenceService
                            .getHotels(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                            .then(function(response){
                                vm.travel['hotel'] = _.orderBy(response, 'name', 'asc');
                                SharedProperties.travelItems['hotel'] = vm.travel['hotel'];
                            })
                            .catch(function(error){

                            }),
                        ConferenceService
                            .getCarRentals(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                            .then(function(response){
                                vm.travel['car_rental'] = _.orderBy(response, 'name', 'asc');
                                SharedProperties.travelItems['car_rental'] = vm.travel['car_rental'];
                            })
                            .catch(function(error){

                            }),
                        ConferenceService
                            .getAirlines(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                            .then(function(response){
                                vm.travel['airline'] = _.orderBy(response, 'name', 'asc');
                                SharedProperties.travelItems['airline'] = vm.travel['airline'];
                            })
                            .catch(function(error){

                            }),
                        ConferenceService
                            .getTransportations(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                            .then(function(response){
                                vm.travel['transportation'] = _.orderBy(response, 'name', 'asc');
                                SharedProperties.travelItems['transportation'] = vm.travel['transportation'];
                            })
                            .catch(function(error){

                            })
                    ])
                    .then(function(){
                        vm.requestOnRolling = false;
                    });

                } else{
                    vm.travel = SharedProperties.travelItems;
                }
            };

            $scope.$watch(function(){
                return vm.newTravelType.name;
            }, function(changed_name){
                if(changed_name == ''){
                    vm.newTravelType.logo = '';
                }
            });

            // Processes the date for the registration chart
            vm.setupStats = function(time_option, type) {
                var last_days = time_option;
                var today = new Date();
                today.setHours(0,0,0,0);
                var lastDate = new Date(today.getTime() - last_days*24*60*60*1000);
                var values = [];
                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                if(type != 'no_form'){
                    for (var d = lastDate; d <= today; d.setDate(d.getDate() + 1)) {
                        var date = _.cloneDeep(d);
                        var countForDay = 0;
                        _.each(vm.registrationStats.registration, function(item){
                            var item_day_data = item.day.split('-');
                            var item_day = new Date(item_day_data[0], item_day_data[1]-1, item_day_data[2]);
                            if (d.toDateString() == item_day.toDateString()) {
                                countForDay = item.count;
                                return;
                            }
                        });
                        values.push({
                            x: date,
                            y: countForDay
                        });
                    }
                } else{
                    var i;
                    for(i = lastDate; i <= today; i.setDate(i.getDate() + 1)){
                        var date = _.cloneDeep(i);
                        values.push({
                            x: date,
                            y: 0
                        });
                    }
                }

                if(type == 'update'){
                    lib.updateChart(values);
                } else if(type == 'no_form'){
                    lib.drawChart(values, 'no_chart');
                } else{
                    lib.drawChart(values);
                }
            }

            vm.previewRegistrationForm = function() {
                var location = window.location.href.split('#')[0];
                var form_id = ConferenceSessionsService.hash_session_id.encode(vm.registration_form_id);
                $window.open(vm.iframeCurrentUrl + '/#!/preview/' + form_id + '/step-1');
            }

            vm.getRegistrationCodeFormId = function() {
                var form_id = ConferenceSessionsService.hash_session_id.encode(vm.registration_form_id);
                return form_id;
            }

            vm.loadingTransactions = false;

            vm.loadConferenceExhibitorPayments = function() {
                if (!vm.conferenceExhibitorPayments) {
                    vm.loadingTransactions = true;
                }
                ConferenceExhibitorsService.getConferenceExhibitorPaymentList(vm.conference.conference_id)
                    .then(function(response){
                        vm.loadingTransactions = false;
                        vm.conferenceExhibitorPayments = response;
                        _.each(vm.conferenceExhibitorPayments, function(payment) {
                            if (!payment.transfer_date) {

                            } else {
                                payment.transfer_date = new Date(payment.transfer_date);
                            }

                        });

                        vm.conferenceExhibitorPayments = _.sortBy(vm.conferenceExhibitorPayments, 'transfer_date');
                        var today = new Date()
                        var nextPayout = null;
                        var totalPayment = 0;
                        var nrProcessed = 0;
                        var nrPending = 0;
                        _.each(vm.conferenceExhibitorPayments, function(payment) {
                            if (today < payment.transfer_date && !nextPayout) {
                                nextPayout = payment;
                                nrPending ++;
                            } else {
                                totalPayment += payment.payment.amount - vm.conference.initial_fee * 100;
                                nrProcessed ++;
                            }
                        });

                        vm.nextPayout = nextPayout.payment.amount - vm.conference.initial_fee * 100;
                        vm.nextPayoutDate = nextPayout.transfer_date;
                        vm.today = today;
                        vm.totalPayment = totalPayment;
                        vm.nrProcessed = nrProcessed;
                        vm.nrPending = nrPending;
                    }).catch(function(error){
                        vm.loadingTransactions = false;

                    });
            }

            vm.reloadConference = function(conference_id) {
                var travel = vm.travel;
                ConferenceService
                    .reloadConferenceById(vm.conference.conference_id)
                    .then(function(conference) {
                        vm.setupConferenceData(conference, true);
                        vm.travel = travel;
                    });
            }

            // vm.reloadSessions = function(conference_id) {
            //     ConferenceSessionsService.reloadAllSessions(vm.conference.conference_id)
            //         .then(function(sessions){
            //             vm.sessionDays = {};
            //             vm.sessionsForDay = [];
            //             vm.setupSessions(sessions);
            //             _.each(vm.conferenceDays, function(day) {
            //                 if (vm.sessionSelectedDay == day.getTime()) {
            //                     vm.sessionSelectDay(day);
            //                 }
            //             })
            //
            //             if (vm.sessionDetailsView) {
            //                 var session = _.find(sessions.sessions, {id: vm.sessionDetailsView.id});
            //                 if (session) {
            //                     vm.sessionDetailsView = session;
            //                 }
            //             }
            //
            //         });
            // }

            vm.getRegistrationJs = function() {
                if (API.ENV == 'DEV') {
                    return 'http://expo-registration.s3-website-us-west-2.amazonaws.com/assets/scripts/expo-dev-v2-js.js'
                } else {
                    return 'https://login.expopass.com/assets/scripts/expo-js.js'
                }
            }

            vm.setupMqtt = function(dontReloadMqtt) {
                if (dontReloadMqtt) return;

                if (SharedProperties.mqttClient && SharedProperties.mqttClient.isConnected()) {
                    if (API.ENV == 'DEV') {
                        SharedProperties.mqttClient.subscribe("conference_" + vm.conference.conference_id);
                    } else {
                        SharedProperties.mqttClient.subscribe("conference_prod_" + vm.conference.conference_id);
                    }

                    if (SharedProperties.conference && SharedProperties.conference.conference_id != vm.conference.conference_id) {
                        SharedProperties.mqttClient.unsubscribe("conference_" + SharedProperties.conference.conference_id);
                        SharedProperties.mqttClient.unsubscribe("conference_prod_" + SharedProperties.conference.conference_id);
                    }
                } else {
                    SharedProperties.connectToMqtt();




                    if (SharedProperties.mqttClient) {

                    }
                    $timeout(function() {
                      vm.setupMqtt(dontReloadMqtt)
                    }, 5000);
                }
            }

            // Process the conference data coming from the backend to be used in the app screens
            // Store the data on the $stateParams to be kept in memory so we don't have to hit the backend after the app loads
            // parameter: conference - the conference data that comes from the router required section
            vm.setupConferenceData = function(conference, dontReloadMqtt) {
                dontReloadMqtt = dontReloadMqtt || false;
                vm.conference = conference;
                // cover screen updates

                vm.coverScreen = vm.conference.cover_screen;
                // end cover screen updates
                vm.testConference = _.cloneDeep(conference);
                $rootScope.$emit('Conference', vm.conference);
                vm.merchant_id_verified = vm.conference.business_data.merchant_id_verified;
                vm.conferenceOwner = conference.user;
                vm.conference_id = $stateParams.conference_id;
								vm.conference.hashed_id = vm.conference_id;
                vm.supportContacts = conference.conference_support_contacts;
                vm.conferenceexhibitorSupportContacts = conference.conference_exhibitor_support_contacts;
                vm.conferenceUsers = conference.conference_users;
                vm.users_total_count = conference.conference_users.total_count;
                vm.conference.min_date = lib.format_date_for_validation(new Date());
                vm.conferencePermissions = conference.permissions;
                vm.newTravelToLoad = true;
                vm.conferenceDays = [];
                vm.sessionMappingFieldsSample = presets.sessionInputFieldsSampleData(vm.conference.date_from_aware);
                vm.setupMqtt(dontReloadMqtt);

                if(vm.superUserOn){
                    vm.getRevenueForSuperAdmin(ConferenceService.hash_conference_id.decode(vm.conference_id)[0]);
                }

                SharedProperties.ConferenceVM = vm;

                vm.getConferenceSocialTags();

                var country_name = conference.country;
                if (conference.country.name) {
                    country_name = conference.country.name;
                }

                vm.googleMap = 'https://www.google.com/maps/place/' + country_name + ',' + vm.conference.street + ',' + vm.conference.city;
                vm.googleStaticMap = 'https://maps.googleapis.com/maps/api/staticmap?key=' + API.GOOGLE_API_KEY +
                 '&size=582x256&sensor=false&zoom=15&markers=color%3Ared%7Clabel%3A.%7C' + country_name
                  + '%20' + vm.conference.city + '%20' + vm.conference.street;

                if (vm.conference.payment_type == 1) {
                    vm.paymentType = 'standard';
                } else if (vm.conference.payment_type == 3) {
                    vm.paymentType = 'conference';
                } else if (vm.conference.payment_type == 2) {
                    vm.paymentType = 'advanced';
                    vm.markupFee = vm.conference.markup_fee;
                    vm.loadConferenceExhibitorPayments();
                }

                SharedProperties.conference = vm.conference;
                SharedProperties.conference.user = vm.conferenceOwner;
                vm.travel = {
                    'hotel' : [],
                    'car_rental': [],
                    'airline': [],
                    'transportation': []
                };
                SharedProperties.travelItems = vm.travel;

                vm.conference.date_from = new Date(vm.conference.date_from);
                vm.conference.date_to = new Date(vm.conference.date_to);
                vm.conference_date_from_moment = moment(vm.conference.date_from);
                vm.conference_date_to_moment = moment(vm.conference.date_to);
                vm.conference.date_from_editable = vm.createMomentFromRawTime(vm.conference.date_from_raw);
                vm.conference.date_to_editable = vm.createMomentFromRawTime(vm.conference.date_to_raw);
                var test_starts_at  = _.clone(vm.conference.date_from);
                if (!vm.conference.time_zone) {
                    vm.conference.time_zone = "-0500"; // New york time
                } else if((vm.conference.time_zone[0] != '-') && (vm.conference.time_zone[0] != '+')){
                    vm.conference.time_zone = '+' + vm.conference.time_zone;
                }

                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);

                var test_ends_at  = _.clone(vm.conference.date_to);
                test_starts_at.setTime( test_starts_at.getTime() + (test_starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                test_ends_at.setTime( test_ends_at.getTime() + (test_ends_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );

                var today = new Date();
                today.setTime( today.getTime() + (today.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                today.setHours(0,0,0,0);

                vm.currentExhibitingDayDate = null;
                vm.currentExhibitingDayNr = 0;

                if (today <= test_ends_at && today >= test_starts_at) {
                    for (var iteratedDay = test_starts_at; iteratedDay <= test_ends_at; iteratedDay.setDate(iteratedDay.getDate() + 1)) {
                        iteratedDay.setHours(0, 0, 0, 0);

                        vm.conferenceDays.push(new Date(iteratedDay));

                        var currentIdx = vm.conferenceDays.length;
                        var newD = _.clone(iteratedDay);
                        var x = _.clone(newD);
                        var y = _.clone(new Date(newD.setDate(newD.getDate() + 1)));
                        var z = _.clone(x);
                        var t = _.clone(y);
                        z.setHours(0,0,0,0);
                        t.setHours(0,0,0,0);
                        if (today > z && today <= t) {
                            vm.currentExhibitingDayDate = y;
                            vm.currentExhibitingDayNr = currentIdx;
                        }
                    }
                } else {
                    for (var iteratedDay = test_starts_at; iteratedDay <= test_ends_at; iteratedDay.setDate(iteratedDay.getDate() + 1)) {
                        iteratedDay.setHours(0, 0, 0, 0);
                        vm.conferenceDays.push(new Date(iteratedDay));
                    }
                }

                if (vm.currentExhibitingDayDate) {
                    vm.sessionSelectedDay = vm.currentExhibitingDayDate.getTime();
                } else {
                    vm.sessionSelectedDay = vm.conferenceDays[0].getTime();
                }
                vm.attendees = _.sortBy(conference.attendees.attendees, ['first_name', 'desc']);
                if (conference.rule_type == 'custom_id') {
                    vm.force_custom_attendee_id = true;
                } else if (conference.rule_type == 'no_custom_id') {
                    vm.force_custom_attendee_id = false;
                }
                if (vm.force_custom_attendee_id) {
                    vm.hasAttendeeId = true;
                }
                vm.attendees_total_count = conference.attendees.total_count;

                if (vm.exhibitors) {
                    var exhibitorsProcessed = [];
                    _.each(conference.exhibitors.exhibitors, function(exhibitorItem){
                        var foundExistingExhibitor = _.find(vm.exhibitors, {"id": exhibitorItem.id});
                        if (foundExistingExhibitor && foundExistingExhibitor.checked) {
                            exhibitorItem.checked = True;
                        }

                        exhibitorsProcessed.push(exhibitorItem);
                    });

                    vm.exhibitors = exhibitorsProcessed;

                } else {
                    vm.exhibitors = conference.exhibitors.exhibitors;
                }

                vm.exhibitor_total_count = conference.exhibitors.total_count;


                vm.lockPaymentType = false;
                _.each(vm.exhibitors, function(exhibitor){
                    if (exhibitor.initial_payment) vm.lockPaymentType = true;
                });
                vm.exhibitor_total_count = conference.exhibitors.total_count;
                vm.savedCustomFields = conference.custom_fields;
                vm.customFields = vm.removableAttendeeFields.concat(vm.savedCustomFields);

                vm.viewMoreFields = _.clone(vm.customFields);
                vm.viewMoreFields.push({
                    id: -8,
                    code: 'job_title',
                    name: 'Job Title'
                });
                vm.attendeeMappingFields = vm.attendeeMappingFields.concat(vm.savedCustomFields);
                vm.promo_codes = conference.promo_codes;
                var nr_redeemed = 0;
                _.each(vm.promo_codes, function(promo_code) {
                    if (promo_code.exhibitor) nr_redeemed ++;
                });

                vm.promo_codes_redeemed = nr_redeemed;
                vm.exhibitor_materials = conference.exhibitor_materials;

                if (conference.country) {
                    var found = _.find(vm.all_countries, {name: conference.country});
                    if (found) {
                        vm.conference.country = found;
                    }
                }

                if (conference.country == vm.all_countries[0]) {
                    var found = _.find(vm.united_states, {abbreviation: conference.state});
                    if (found) {
                        vm.conference.state = found;
                    }
                }

                vm.today = new Date();
                if(SharedProperties.registrationForm && SharedProperties.registrationForm[0] && SharedProperties.registrationStats &&
                        SharedProperties.registrationForm[0].conference == ConferenceService.hash_conference_id.decode(vm.conference_id)[0]){
                    vm.setRegistrationFormData(SharedProperties.registrationForm);
                    vm.registrationStats = SharedProperties.registrationStats;
                    vm.setupStats(7);
                    if (!vm.registrationStats) {
                        vm.setupStats(7, 'no_form');
                    } else {
                        vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count_last_seven;
                    }
                } else{
                ConferenceRegistrationService.registrationStats(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        vm.registrationStats = response;
                        vm.setupStats(7);
                        vm.numberOfRegistrantsPerDate = vm.registrationStats.total_count_last_seven;
                        SharedProperties.registrationStats = vm.registrationStats;

                        ConferenceRegistrationService.getRegistrationTransfers(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], 1)
                            .then(function(response){
                                var futurePayments = 0;
                                var pastPayments = 0;
                                _.each(response.attendee_registration_data, function(attendee_registration){
                                    if (attendee_registration.transfer_date) {
                                        attendee_registration.transfer_date = new Date(attendee_registration.transfer_date);
                                    }

                                    if (attendee_registration.transfer_date > new Date()) {
                                        futurePayments++;
                                    } else {
                                        pastPayments++;
                                    }
                                });
                                vm.registrationStats.attendee_registration_data = response.attendee_registration_data;
                                vm.registrationStats.total_attendee_registrations = response.total_count;
                                vm.registrationStats.futurePayments = futurePayments;
                                vm.registrationStats.pastPayments = pastPayments;
                                vm.registrationStats.attendee_transfers_page = 1;
                            }).catch(function(error){

                            });
                    }).catch(function(error){
                        vm.setupStats(7, 'no_form');
                    });


                vm.nextButtonBlocked = true;
                ConferenceRegistrationService.getRegistrationFormV1(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        SharedProperties.registrationForm = response;
                        if(response.length != 0){
                            vm.setRegistrationFormData(response);
                        } else {
                            vm.nextButtonBlocked = false;
                        }

                    })
                    .catch(function(error){
                        if (error.status == 404) {
                            vm.setupStats(7, 'no_form');
                            vm.conference.registration_form = null;
                            vm.registrationStats = null;
                        }
                    });
                }

                if (vm.conference.features.badge) {
                    vm.viewMoreFields.push({
                        code: 'badge_title',
                        id: -1100,
                        name: 'Badge Title'
                    });
                }
            }

			vm.canIncludeSession = function(session) {
				var found = false;
				if(vm.currentLevelItem){
					_.each(vm.currentLevelItem.registrationlevelsession_set, function(level_session){
						if (session.id == level_session.session.id) {
							found = true;
						}
					});
				} else{
					return !found;
				}
				return !found;
			}

            vm.canIncludeSessionPerLevel = function(session, level) {
                var found = false;
                if(level){
                    _.each(level.registrationlevelsession_set, function(level_session){
                        if (session.id == level_session.session.id) {
                            found = true;
                        }
                    });
                } else{
                    return !found;
                }
                return !found;
            }

            if (!vm.registrationSteps) {
                vm.registrationSteps = {
                    attendee_fields_entry_panel: true,
                    registration_levels_setup_panel: false,
                    registration_level_options_panel: false,
                    legal_panel: false,
                    review_panel: false
                };
            }


            // After the registration form data is received from the backend
            // it's processed to be viewed in the registration form wizard
            vm.setRegistrationFormData = function(response, type){
                if(type != 'first_setup'){
				    response = response[0];
                }
                vm.conference.registration_form = response;
                vm.registration_panel_edit = false;
                if (type != 'update') {
                    if (vm.conference.registration_form.completed_at == null) {
                        vm.registrationFormComplete = false;
                        vm.setupStats(7, 'no_form');
                        vm.registrationSteps = {
                            attendee_fields_entry_panel: true,
                            registration_levels_setup_panel: false,
                            registration_level_options_panel: false,
                            legal_panel: false,
                            review_panel: false
                        };
                    } else{
                        vm.registrationFormComplete = true;
                        vm.registrationSteps = {
                            attendee_fields_entry_panel: false,
                            registration_levels_setup_panel: false,
                            registration_level_options_panel: false,
                            legal_panel: false,
                            review_panel: true
                        };
                    }
                }


                vm.registration_form_id = response.id;

				vm.registrationLevels = response.registrationlevel_set;
                if (!vm.registrationLevels) {
                    vm.registrationLevels = [];
                }

                if (vm.registrationLevels.length > 0) {
                    var alreadyProcessed = _.find(vm.viewMoreFields, function(o) { return o.id < -1000; });
                    if (!alreadyProcessed) {
                        vm.viewMoreFields.push({
                            code: 'attendee_registration_level_name',
                            id: -1000,
                            name: 'Registration Level'
                        });
                    }

                }

                vm.iframeCodeModel = SharedProperties.registrationFormIframe || '';
                _.each(vm.registrationLevels, function(level){
                    _.each(level.registrationlevelpricing_set, function(price){
                        if(price.starts_at != null && price.ends_at != null){
                            price.starts_at =  moment(price.starts_at);
                            price.ends_at = moment(price.ends_at);
                        }
                    });
                    _.each(level.registrationlevelsession_set, function(level_session){
                        if(!level_session.session.hasOwnProperty('addOnSession')){
                            level_session.session.addOnSession = {};
                            vm.addExtraPricingForAddOnSession(level_session);
                        }
                    });
                })

                var registrationLevelList = [];
                _.each(vm.registrationLevels, function(level){
                    var pricingSet = [];
                    _.each(level.registrationlevelpricing_set, function(price){
                        if(price.starts_at != null && price.ends_at != null){
                            price.starts_at = moment(price.starts_at);
                            price.starts_at_editable = vm.createMomentFromRawTime(price.starts_at_raw);
                            price.ends_at = moment(price.ends_at);
                            price.ends_at_editable = vm.createMomentFromRawTime(price.ends_at_raw);
                        } else if (price.starts_at) {
                            price.starts_at = moment(price.starts_at);
                            price.starts_at_editable = vm.createMomentFromRawTime(price.starts_at_raw);
                        } else if (price.ends_at) {
                            price.ends_at = moment(price.ends_at);
                            price.ends_at_editable = vm.createMomentFromRawTime(price.ends_at_raw);
                        }
                        pricingSet.push(price);
                    });
                    level.registrationlevelpricing_set = pricingSet;
                    registrationLevelList.push(level);
                })
                vm.registrationLevels = registrationLevelList;

                _.each(response.registrationformstandardfield_set, function(standard_field){
                    var item = _.find(vm.standardFields, {'name': standard_field.name});
                    if (item) {
                        if (standard_field.type == 'optional') item.type_code = 'Optional';
                        else if (standard_field.type == 'required') item.type_code = 'Required';
                        else if (standard_field.type == 'hidden') item.type_code = 'Hidden';
                        item.type = standard_field.type;
                    }
                });
                _.each(response.registrationformcustomfield_set, function(custom_field){
                    var custom_item = _.find(vm.savedCustomFields, {'id': custom_field.custom_field.id});
                    if(custom_item){
                        if (custom_field.type == 'optional') custom_item.regsitration_form_type_code = 'Optional';
                        else if (custom_field.type == 'required') custom_item.regsitration_form_type_code = 'Required';
                        else if (custom_field.type == 'hidden') custom_item.regsitration_form_type_code = 'Hidden';
                        custom_item.regsitration_form_type = custom_field.type;
                    }
                });

                if (vm.conference.registration_form.terms_of_service) {
                    ConferenceRegistrationService
                        .getFileContent(vm.conference.registration_form.terms_of_service)
                        .then(function(response){
                            vm.termsAndConditionsText = response.data;
                        })
                        .catch(function(error){

                        });
                }

                if (vm.conference.registration_form.refund_policy && !_.isEmpty(vm.conference.registration_form.refund_policy)) {
                    ConferenceRegistrationService
                        .getFileContent(vm.conference.registration_form.refund_policy)
                        .then(function(response){
                            vm.refundPolicyText = response.data;
                        })
                        .catch(function(error){

                        });
                }

                vm.nextButtonBlocked = false;
            }

			if (conference) {
	           vm.setupConferenceData(conference);
			}

            vm.continuing_education_model = {'name' : ''};
            // Add the continuing education to the conference
            vm.addContinuingEducation = function(){
                if(vm.continuing_education_model.name != '')
                    if(!vm.continuing_education_model.id){
                        ConferenceService
                        .createConferenceContinuingEducation(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.continuing_education_model)
                        .then(function(response){
                            vm.conference.continuing_educations.push(response);
                            if(vm.newSession){
                                vm.all_continuing_educations.push(response);
                            }
                            vm.continuing_education_model.name = '';
                        })
                        .catch(function(error){

                        });
                    } else{
                        ConferenceService
                        .updateConferenceContinuingEducation(vm.continuing_education_model)
                        .then(function(response){
                            vm.continuing_education_model = {
                                'name' : ''
                            };
                        })
                        .catch(function(error){

                        });
                    }
            }

            // Delete the continuing education from the conference
            vm.deleteContinuingEducation = function(continuing_education_id){
                ConferenceService
                    .removeConferenceContinuingEducation(continuing_education_id)
                    .then(function(response){
                        vm.conference.continuing_educations = _.reject(vm.conference.continuing_educations, function(ce){
                            return ce.id == continuing_education_id
                        });
                        vm.continuing_education_model = {
                            'name' : ''
                        };
                    })
                    .catch(function(error){

                    });
            };

            vm.updateContinuingEducation = function(continuing_education_to_update){
                vm.continuing_education_model = continuing_education_to_update;
            };

            vm.checkContinuingEducation = function(continuing_education){
                if(continuing_education.hasOwnProperty('checked')){
                    continuing_education.checked = !continuing_education.checked;
                } else{
                    continuing_education.checked = true;
                }
            }

            // Remove continuing education to the session
            vm.addContinuingEducationToSession = function(){
                _.each(vm.all_continuing_educations, function(ce){
                    if(ce.hasOwnProperty('checked') && ce.checked == true){
                        ce.checked = false;
                        vm.newSessionItem.continuing_educations.push(ce);
                        vm.all_continuing_educations = _.reject(vm.all_continuing_educations, ['id', ce.id]);
                    }
                });
            }

            vm.addConferenceSpeakerToSession = function(){
                _.each(vm.currentSessionSpeakersList, function(conferenceSpeaker){
                    if(conferenceSpeaker.hasOwnProperty('checked') && conferenceSpeaker.checked == true){
                        conferenceSpeaker.checked = false;
                        vm.newSessionItem.sessionspeaker_set.push(conferenceSpeaker);
                        vm.currentSessionSpeakersList = _.reject(vm.currentSessionSpeakersList, ['id', conferenceSpeaker.id]);
                    }
                });
            }

            // Remove continuing education from the session
            vm.removeContinuingEducationFromSession = function(){
                _.each(vm.newSessionItem.continuing_educations, function(ce){
                    if(ce.hasOwnProperty('checked') && ce.checked == true){
                        ce.checked = false;
                        vm.all_continuing_educations.push(ce);
                        vm.newSessionItem.continuing_educations = _.reject(vm.newSessionItem.continuing_educations, function(session_ce){
                            return session_ce.id == ce.id;
                        });
                    }
                });
            }

            vm.removeConferenceSpeakerFronSession = function(){
                _.each(vm.newSessionItem.sessionspeaker_set, function(sessionSpeaker){
                    if(sessionSpeaker.hasOwnProperty('checked') && sessionSpeaker.checked == true){
                        sessionSpeaker.checked = false;
                        vm.currentSessionSpeakersList.push(sessionSpeaker);
                        vm.newSessionItem.sessionspeaker_set = _.reject(vm.newSessionItem.sessionspeaker_set, function(session_spk){
                            return session_spk.id == sessionSpeaker.id;
                        });
                    }
                });
            }

            // Delete exhibitor material
			vm.removeExhibitorMaterial = function(exhibitor_material) {
                ConferenceExhibitorsService.removeExhibitorMaterial(exhibitor_material.id)
                    .then(function(response){
                        var index = vm.exhibitor_materials.indexOf(exhibitor_material);
                        if (index > -1) {
                            vm.exhibitor_materials.splice(index, 1);
                        }
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
			}

	    try {
	        Stripe.setPublishableKey(API.STRIPE_KEY);


	    }
	    catch(err) {

	    }

			if (attendees && attendees.length > 0) {
                vm.attendees = attendees.attendees;
                vm.attendees_total_count = attendees.total_count;
                SharedProperties.attendees = vm.attendees;
      }
      else {
          SharedProperties.attendees = null;
      }

      $scope.$watch(function(){
          return (SharedProperties.attendees) ? SharedProperties.attendees.length : 1;
      }, function(total_number){
      });

            vm.getTravelForm = function(type){
                if(type == 'hotel' || type == 'car_rental'){
                    vm.form_for_travel = vm.form_for_travel_standart
                }
            };

            // Upload the attendee list as a csv file to S3
            vm.uploadAttendeesAsCsv = function() {
                var allChecked = false;
                var checkedAttendeeList = [];
                if (!vm.checkAllAttendees) {
                    _.each(vm.attendees, function(attendee) {
                        if (attendee.checked == true) {
                            var mainProfileAttendee = _.omit(attendee,
                            ['custom_fields' , 'attendee_photo', 'id', 'attendee_id', 'guest_of',
                             'attendee_registration_level_name','created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.custom_fields, function(customField){
                                if (customField.custom_attendee_field.type == 3) {
                                    try {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value.join();
                                    }
                                    catch(err) {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                    }
                                } else {
                                    mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                }
                            });
                            mainProfileAttendee['Conference Name'] = vm.conference.name;
                            mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                            checkedAttendeeList.push(mainProfileAttendee);
                        }
                    });
                } else {
                    allChecked = true;
                    var nr_pages = parseInt(vm.attendees_total_count / 30) + 2;
                    var promises = [];
                    for (var idx = 1; idx <= nr_pages + 1; idx ++) {

                        promises.push(
                            ConferenceAttendeesService.getAllAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], idx)
                        );
                    }

                    $q.all(promises).then(function(responses){
                        var allAttendees = [];
                        _.each(responses, function(response){
                            allAttendees = allAttendees.concat(response.attendees);
                        });

                        _.each(allAttendees, function(attendee) {
                            var mainProfileAttendee = _.omit(attendee,
                            ['custom_fields' ,  'attendee_photo', 'id', 'attendee_id', 'guest_of',
                                    'attendee_registration_level_name','created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.custom_fields, function(customField){
                                if (customField.custom_attendee_field.type == 3) {
                                    try {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value.join();
                                    }
                                    catch(err) {
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                    }
                                } else {
                                    mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                }
                            });
                            mainProfileAttendee['Conference Name'] = vm.conference.name;
                            mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                            checkedAttendeeList.push(mainProfileAttendee);
                        });

                        vm.uploadAttendeesFileAndGoToAvery(checkedAttendeeList);

                    }).catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                        return
                    });
                    return;
                }

                vm.uploadAttendeesFileAndGoToAvery(checkedAttendeeList);
            }

            // For the avery badge generation process
            // First it uploads a csv file for the selected attendees
            // after that it accesses the correct avery url with the link to that file and the link to the selected avery template
            vm.uploadAttendeesFileAndGoToAvery = function(checkedAttendeeList) {
                    var date = new Date();
                    var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                    var filename = $scope.$parent.conferenceName + actualDate + ".csv";

                    var csvData =  lib.ConvertToCSV2(checkedAttendeeList);

                    var file = new File([csvData], filename, {type: "text/csv", lastModified: date});

                    FileService.getUploadSignature(file)
                        .then(function(response){
                            var file_link = response.data.file_link;
                            FileService.uploadFile(file, response)
                                .then(function(uploadResponse){
                                    var sku = 5392;
                                    if (vm.templateTypeName=='badge') {
                                        sku = 5392;
                                    }
                                    else {
                                        sku = 22805;
                                    }

                                    vm.averyUrl = "http://services.print.avery.com/dpp/public/v1/open/US_en/?sku="
                                        + sku +
                                        "&mergeDataURL=" + file_link + "&provider=expopass";

                                    $scope.goToAvery = vm.goToAvery;

                                    ngDialog.open({template: 'app/views/partials/conference/go_to_avery_modal.html',
                                       className: 'app/stylesheets/_createLabelModal.scss',
                                       scope: $scope
                                    });
                                })
                                .catch(function(error){

                                });
                        })
                        .catch(function(error){

                        });
                }

            vm.goToAvery = function() {
                $window.open(vm.averyUrl, "_blank");
            }

            vm.requestedMoreAttendees = false;
            vm.showMoreAttendees = function(elem) {
                if (!vm.requestedMoreAttendees && (vm.attendees_total_count > vm.attendees.length)) {
                    var page = parseInt(vm.currentPage) + 1;
                    vm.requestedMoreAttendees = true;

                    ConferenceAttendeesService
                        .getAllAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], page, true, vm.attendeeFilters)
                        .then(function(attendees) {
                            vm.currentPage = page;
                            vm.requestedMoreAttendees = false;
                            if(vm.checkAllAttendees === true){
                                _.forEach(attendees.attendees, function(attendee){
                                    attendee.checked = true;
                                });
                            }
                            vm.attendees = vm.attendees.concat(attendees.attendees);
                        })
                        .catch(function(error){
                            vm.requestedMoreAttendees = false;
                        });
                }
            }

            vm.forceMinPrice = function(pricing) {
                if (pricing.price < 20) {
                    pricing.price = 20;
                }
            }

            vm.requestedMoreExhibitors = false;
            vm.currentPageExhibitors = 1;
            vm.showMoreExhibitors = function(elem) {
                if (!vm.requestedMoreExhibitors) {
                    var page = parseInt(vm.currentPageExhibitors) + 1;
                    vm.requestedMoreExhibitors = true;

                    ConferenceExhibitorsService
                        .getAllExhibitors(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], page, true, vm.attendeeFilters)
                        .then(function(exhibitors) {
                            vm.currentPageExhibitors = page;
                            vm.requestedMoreExhibitors = false;
                            if(vm.checkAllAttendees === true){
                                _.forEach(exhibitors.exhibitors, function(exhibitors){
                                    exhibitors.checked = true;
                                });
                            }
                            vm.exhibitors = vm.exhibitors.concat(exhibitors.exhibitors);
                        })
                        .catch(function(error){
                            vm.requestedMoreExhibitors = false;
                        });
                }
            };

            vm.requestedMoreSessionAttendees = false;
            vm.currentSessionAttendeesPage = 1;
            vm.showMoreSessionAttendees = function(elem) {
                 vm.attendeeFilters = {
                    queryString: '',
                    sortBy: 'first_name',
                    sortOrder: "asc"
                };
                if (!vm.requestedMoreSessionAttendees) {
                    var page = parseInt(vm.currentSessionAttendeesPage) + 1;
                    vm.requestedMoreSessionAttendees = true;
                    if(vm.sessionRegistrantsView){
                        ConferenceSessionsService
                            .listSessionAttendees(vm.session_id, page, vm.attendeeFilters)
                            .then(function(attendeesFromSession) {
                                vm.currentSessionAttendeesPage = page;
                                if(vm.checkAllSessionRegistrants){
                                    _.forEach(attendeesFromSession.sessions_attendees, function(attendeeFromSession){
                                        attendeeFromSession.attendee.checked = true;
                                    });
                                }
                                vm.sessionAttendees.sessions_attendees = _.uniq(vm.sessionAttendees.sessions_attendees.concat(attendeesFromSession.sessions_attendees));
                            })
                            .catch(function(error){
                                vm.requestedMoreSessionAttendees = false;
                            });
                    } else if(vm.sessionAttendeesView){
                        ConferenceSessionsService
                            .listSessionScanned(vm.session_id, page, vm.attendeeFilters)
                            .then(function(scannedFromSession) {
                                vm.currentSessionAttendeesPage = page;
                                if(vm.checkAllSessionScanned){
                                    _.forEach(scannedFromSession.scanned_attendees, function(attendeeFromSession){
                                        attendeeFromSession.attendee.checked = true;
                                    });
                                }
                                vm.sessionAttendees.scanned_attendees = _.uniq(vm.sessionAttendees.scanned_attendees.concat(scannedFromSession.scanned_attendees));
                            })
                            .catch(function(error){
                                vm.requestedMoreSessionAttendees = false;
                            });
                    }
                }
            }

            vm.sessionDays = {};
            vm.sessionsForDay = [];
            vm.orderAsc = true;

            vm.setupSessions = function(sessions) {
                vm.sessions = sessions.sessions;
                vm.orderAsc = false;
                vm.sessions = _.orderBy(vm.sessions, ['starts_at', 'title'], ['asc', 'asc']);
                SharedProperties.sessions = sessions;
                SharedProperties.sessions_hashed_id = vm.conference_id;
                SharedProperties.sessionsConferences = vm.conference.conference_id;
                vm.totalSessions = sessions.total_count;
                _.each(vm.sessions, function(session) {
                    var startsAt = _.clone(new Date(session.starts_at));
                    var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                    startsAt.setTime( startsAt.getTime() + (startsAt.getTimezoneOffset() + offsetInMinutes)*60*1000 );

                    var day = startsAt.getDate();
                    var month = startsAt.getMonth();
                    var year = startsAt.getFullYear();
                    if (!vm.sessionDays[day + '/' + month + '/' + year]) {
                        vm.sessionDays[day + '/' + month + '/' + year] = [];
                    }
                    vm.sessionDays[day + '/' + month + '/' + year].push(session);
                });
                vm.getSessionCustomFields();

                if (SharedProperties.conferenceSpeakers && SharedProperties.conferenceSpeakersConfId == vm.conference_id) {
                    vm.conferenceSpeakersList = SharedProperties.conferenceSpeakers;
                } else
                ConferenceService
                    .getConferenceSpeakers(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        vm.conferenceSpeakersList = response;
                        SharedProperties.conferenceSpeakers = response;
                        SharedProperties.conferenceSpeakersConfId = vm.conference_id;
                    })
                    .catch(function(error){

                    });
            }

            if (sessions) {
                vm.setupSessions(sessions);
            }

            if (vm.session_id) {
                vm.session_id = ConferenceSessionsService.hash_session_id.decode(vm.session_id)[0];
                vm.sessionDetailsView = _.find(vm.sessions, {id: vm.session_id});
            }

            vm.averyUrl = null;

            // Orders sessions in the session list
            vm.orderSessions = function(orderAsc){
                if(orderAsc){
                    vm.sessionsForDay = _.orderBy(vm.sessionsForDay, ['starts_at', 'title'], ['asc', 'asc']);
                    vm.orderAsc = false;
                } else{
                    vm.sessionsForDay = _.orderBy(vm.sessionsForDay, ['starts_at', 'title'], ['desc', 'asc']);
                    vm.orderAsc = true;
                }

                _.forOwn(vm.sessionDays, function(sessionsForDay, key) {
                        if (!vm.orderAsc) {
                            sessionsForDay = _.orderBy(sessionsForDay, ['starts_at', 'title'],
                            ['asc', 'asc']);
                        } else {
                            sessionsForDay = _.orderBy(sessionsForDay, ['starts_at', 'title'],
                            ['desc', 'desc']);
                        }
                        vm.sessionDays[key] = sessionsForDay;
                    }
                );
            }

            vm.orderSessionBy = 'starts_at';
            vm.orderSessionsByField = function(orderAsc, field){
                if (field != vm.orderSessionBy) {
                    orderAsc = false;
                }
                vm.orderSessionBy = field;

                var orderBy = vm.orderSessionBy;
                var orderDir = 'asc';
                if(orderAsc){
                    vm.orderAsc = false;
                } else{
                    vm.orderAsc = true;
                }

                if (vm.orderAsc) {
                    orderDir = 'desc';
                } else {
                    orderDir = 'asc';
                }

                if (orderDir == 'asc') {
                    vm.sessionsForDay = _.sortBy(vm.sessionsForDay, function(item) {
                        if(_.isString(item[orderBy]))
                           return item[orderBy].toLowerCase();
                        else
                            return item[orderBy];
                    });
                } else {
                    vm.sessionsForDay = _.sortBy(vm.sessionsForDay, function(item) {
                        if(_.isString(item[orderBy]))
                           return item[orderBy].toLowerCase();
                        else
                            return item[orderBy];
                    }).reverse();
                }

                _.forOwn(vm.sessionDays, function(sessionsForDay, key) {
                        if (orderDir == 'asc') {
                            sessionsForDay = _.sortBy(sessionsForDay, function(item) {
                                if(_.isString(item[orderBy]))
                                   return item[orderBy].toLowerCase();
                                else
                                    return item[orderBy];
                            });
                        } else {
                            sessionsForDay = _.sortBy(sessionsForDay, function(item) {
                                if(_.isString(item[orderBy]))
                                   return item[orderBy].toLowerCase();
                                else
                                    return item[orderBy];
                            }).reverse();
                        }
                        vm.sessionDays[key] = sessionsForDay;
                    }
                );
            }

            vm.orderExhibitorBy = 'company_name';

            vm.orderExhibitorsByField = function(orderAsc, field){
                if (field != vm.orderExhibitorBy) {
                    orderAsc = false;
                }
                vm.orderExhibitorBy = field;

                var orderBy = [vm.orderExhibitorBy];
                var orderDir = ['asc'];
                if(orderAsc){
                    vm.orderAsc = false;
                } else{
                    vm.orderAsc = true;
                }

                if (vm.orderAsc) {
                    orderDir = ['desc'];
                } else {
                    orderDir = ['asc'];
                }

                if(vm.exhibitor_total_count > 30){
                    vm.exhibitorFilters = {
                        queryString: null,
                        sortBy: vm.orderExhibitorBy,
                        sortOrder: orderDir[0]
                    };

                    if (vm.orderExhibitorBy == 'company_name') {
                        vm.exhibitorFilters['sortBy'] = 'name';
                    }
                    if (vm.orderExhibitorBy == 'admin') {
                        vm.exhibitorFilters['sortBy'] = 'admin_first_name';
                    }
                    ConferenceExhibitorsService.getAllExhibitors(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.currentPage, false, vm.exhibitorFilters)
                        .then(function(exhibitors){

                            vm.exhibitors = exhibitors.exhibitors;
                            vm.exhibitor_total_count = exhibitors.total_count;
                            vm.sortOrderAscending = !vm.sortOrderAscending;
                    })
                    .catch(function(error){

                    });
                } else{
                    if (orderBy == 'admin') {
                        vm.exhibitors = _.orderBy(vm.exhibitors, [
                            function(o) {
                                if (!o.admin){
                                    return "";
                                }
                                return o.admin.first_name.toLowerCase();
                            },
                            function(o) {
                                if (!o.admin){
                                    return "";
                                }
                                return o.admin.last_name.toLowerCase();
                            },
                        ], [orderDir, orderDir]);

                    }
                    else if (orderBy == 'admin_email') {
                        vm.exhibitors = _.orderBy(vm.exhibitors, [
                            function(o) {
                                if (!o.admin_email){
                                    return "";
                                }
                                return o.admin_email.toLowerCase();
                            },
                        ], [orderDir]);
                    }
                    else {
                        vm.exhibitors = _.orderBy(vm.exhibitors, [
                            function(o) {
                                if (!o[orderBy]){
                                    return "";
                                }
                                if (_.isString(o[orderBy])) {
                                    return o[orderBy].toLowerCase();
                                } else {
                                    return o[orderBy];
                                }
                            },
                        ], [orderDir]);
                    }
                }
            }

            // Opens a qr code modal when clicking on the qr code in the attendee listing
            vm.openQRcodeModal = function(attendee) {
                $scope.attendeeData = vm.getUserQrCode(attendee);
                ngDialog.open({template: 'app/views/partials/conference/qr_code_modal.html',
                   className: 'ngdialog-theme-plain custom-width',
                   controller: "ConferenceAttendeesController as vm",
                   scope: $scope
                });
            }

            // Opens a modal to create an avery label template
            vm.openCreateLabelModal = function() {
                var labels = [];
                _.each(vm.conference.templates, function(template) {
                    if (template.type == 2) {
                        labels.push(template);
                    }
                });

                if (labels.length <= 0) {
                    vm.templateTypeName='label';
                    vm.uploadAttendeesAsCsv();
                    return;
                }

                $scope.templates = labels;
                $scope.templateType = 2;
                $scope.templateTypeName = "label";
                $scope.averyTemplateFile = vm.averyTemplateFile;
                $scope.updateAveryFileOption = vm.updateAveryFileOption;
                $scope.prepareFunction = vm.uploadAttendeesAsCsv;
                $scope.attendees = vm.attendees;
                $scope.conferenceName = vm.conference.name;
                $scope.ConvertToCSV2 = lib.ConvertToCSV2;
                $scope.averyUrl = vm.averyUrl;
                $scope.checkAllAttendees = vm.checkAllAttendees;
                $scope.attendees_total_count = vm.attendees_total_count;
                ngDialog.open({template: 'app/views/partials/conference/create_label_modal.html',
                               className: 'app/stylesheets/_createLabelModal.scss',
                               controller: "ConferenceAttendeesController as vm",
                               scope: $scope
                            });
            };

            // Opens a modal to create an avery badge template
            vm.openCreateBadgeModal = function(){
                var labels = []
                _.each(vm.conference.templates, function(template) {
                    if (template.type == 1) {
                        labels.push(template);
                    }
                });

                if (labels.length <= 0) {
                    vm.templateTypeName='badge';
                    vm.uploadAttendeesAsCsv();
                    return;
                }

                $scope.JSON2CSV = vm.JSON2CSV;
                $scope.csvUrl = vm.csvUrl;

                $scope.templates = labels;
                $scope.templateType = 1;
                $scope.averyTemplateFile = vm.averyTemplateFile;
                $scope.updateAveryFileOption = vm.updateAveryFileOption;
                $scope.prepareFunction = vm.uploadAttendeesAsCsv;
                $scope.attendees = vm.attendees;
                $scope.conferenceName = vm.conference.name;
                $scope.ConvertToCSV2 = lib.ConvertToCSV2;
                $scope.averyUrl = vm.averyUrl;
                $scope.checkAllAttendees = vm.checkAllAttendees;
                $scope.attendees_total_count = vm.attendees_total_count;
                $scope.templateTypeName = "badge";
                ngDialog.open({template: 'app/views/partials/conference/create_label_modal.html',
                               className: 'app/stylesheets/_createLabelModal.scss',
                               controller: "ConferenceAttendeesController as vm",
                               scope: $scope
                            });
            };

            // Opens the session detail view
            vm.viewSessionDetails = function(session) {
                if (session == null) {
                    vm.sessionDetailsView = session;
                    $state.go('home.sessions',{conference_id: vm.conference_id},{
                        notify:false,
                        reload:false,
                        location:'replace',
                        inherit:false
                    });
                }
                else {
                    vm.sessionDetailsView = session;
                    vm.newSessionItem = null;
                    vm.sessionDetailsView.session_type = vm.getSessionTypeName(vm.sessionDetailsView.session_type);
                    vm.session_id = session.id;

                    $state.go('home.session',{conference_id: vm.conference_id, session_id: ConferenceSessionsService.hash_session_id.encode(vm.session_id)},{
                        notify:false,
                        reload:false,
                        location:'replace',
                        inherit:false
                    });
                }
            }

            // Load the attendees registered to a session
            vm.loadSessionRegistrants = function() {
                vm.attendeeFilters = {
                    queryString: '',
                    sortBy: 'first_name',
                    sortOrder: "asc"
                };
                ConferenceSessionsService
                   .listSessionAttendees(vm.session_id, 1, false, vm.attendeeFilters)
                   .then(function(sessionAttendees) {
                        vm.sessionAttendees = sessionAttendees; // bug
                        vm.totalNumberOfRegistrants = vm.sessionAttendees.total_count;
                        vm.sessionRegistrantsView = true;
                        vm.sessionAttendeesView = false;
                        vm.checkAllSessionScanned = false;
                        vm.checkAllSessionRegistrants = false;
                        vm.sessionScannedChecked = false;
                        vm.sessionRegistrantsChecked = false;
                    })
                    .catch(function(error){

                    });
            }

            // Load the attendees scanned to a session
            vm.loadSessionScanned = function() {
                vm.attendeeFilters = {
                    queryString: '',
                    sortBy: 'first_name',
                    sortOrder: "asc"
                };
                ConferenceSessionsService
                   .listSessionScanned(vm.session_id, 1, false, vm.attendeeFilters)
                   .then(function(sessionAttendees) {
                        vm.sessionAttendees = sessionAttendees; // bug
                        vm.totalNumberOfRegistrants = vm.sessionAttendees.total_count;
                        vm.sessionRegistrantsView = false;
                        vm.sessionAttendeesView = true;
                        vm.checkAllSessionScanned = false;
                        vm.checkAllSessionRegistrants = false;
                        vm.sessionScannedChecked = false;
                        vm.sessionRegistrantsChecked = false;
                    })
                    .catch(function(error){

                    });
            }

            $scope.$watch('session', function(){
                if (session) {
                    vm.sessionDetailsView = session;
                    vm.session_id = session.id;
                }
            });

            // Sort the attendees registered in a session
            vm.sortSessionRegistrants = function(){
                if(!_.isEmpty(vm.attendeeFilters.sortOrder)){
                    vm.attendeeFilters = {
                        queryString: '',
                        sortBy: 'first_name',
                        sortOrder: vm.attendeeFilters.sortOrder === 'asc' ? 'desc' : 'asc'
                    };
                }
                ConferenceSessionsService
                   .listSessionAttendees(vm.session_id, 1, false, vm.attendeeFilters)
                   .then(function(registrantsList){
                        if(vm.checkAllSessionRegistrants){
                            _.each(registrantsList.sessions_attendees ,function(attendee){
                                attendee.attendee.checked = true;
                            });
                        }
                        vm.sessionAttendees = registrantsList;
                   })
                   .catch(function(error){

                   });
            };

            // Sort the attendees scanned in a session
            vm.sortSessionScanned = function(){
                if(!_.isEmpty(vm.attendeeFilters.sortOrder)){
                    vm.attendeeFilters = {
                        queryString: '',
                        sortBy: 'first_name',
                        sortOrder: vm.attendeeFilters.sortOrder === 'asc' ? 'desc' : 'asc'
                    };
                }
                ConferenceSessionsService
                   .listSessionScanned(vm.session_id, 1, false, vm.attendeeFilters)
                   .then(function(scannedList){
                        if(vm.checkAllSessionScanned){
                            _.each(scannedList.scanned_attendees ,function(attendee){
                                attendee.attendee.checked = true;
                            });
                        }
                        vm.sessionAttendees = scannedList;
                   })
                   .catch(function(error){

                   });
            };

            // On confirm, delete the scanned attendees from the session
            // Code Review: does this need it's own function?
            vm.confirmDeleteAttendeesFromSession = function(){
                vm.attendeesStillInSession = [];
                vm.numberOfScannesRemoved = 0;
                $scope.attendeesRemovedFromSessionOnScope = false;
                var i, j, ok = false, arrIds = [];
                vm.remove_type = false;
                if(vm.checkAllSessionScanned){
                    vm.remove_type = true;
                } else{
                    _.forEach(vm.sessionAttendees.scanned_attendees, function(sessionAttendee){
                        if(sessionAttendee != undefined && sessionAttendee.attendee.checked){
                            arrIds.push(sessionAttendee.attendee.id);
                        }
                    });
                    vm.remove_type = false;
                }
                ConferenceSessionsService
                    .removeSessionAttendeesScannedFromList(vm.session_id, arrIds, vm.remove_type)
                    .then(function(response){
                        for(i = 0; i < vm.sessionAttendees.scanned_attendees.length; i++){
                            for(j = 0; j < arrIds.length; j++){
                                if(vm.sessionAttendees.scanned_attendees[i].attendee.id !== arrIds[j]){
                                    ok = true;
                                } else{
                                    vm.checkOne(vm.sessionAttendees.scanned_attendees[i].attendee, 'session_scanned');
                                    ok = false;
                                    vm.numberOfRegistrantsRemoved++;
                                    break;
                                }
                            }
                            if(ok === true){
                                vm.attendeesStillInSession.push(vm.sessionAttendees.scanned_attendees[i]);
                            }
                            if(i === (vm.sessionAttendees.scanned_attendees.length - 1)){
                                $scope.attendeesRemovedFromSessionOnScope = true;
                            }
                        }
                    }).catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            $scope.$watch('attendeesRemovedFromSessionOnScope', function(){
                if($scope.attendeesRemovedFromSessionOnScope === true && vm.sessionAttendees.scanned_attendees !== vm.attendeesStillInSession){
                    vm.totalNumberOfRegistrants = !vm.remove_type ? (vm.totalNumberOfRegistrants - $scope.nrOfRemoves) : 0;
                    vm.sessionAttendees.scanned_attendees = vm.attendeesStillInSession;
                    vm.sessionAttendees.total_count = vm.totalNumberOfRegistrants;
                    vm.sessionScannedChecked = false;
                    _.each(vm.sessions, function(session){
                        if(session.id === vm.session_id){
                            session.nr_scanned = vm.totalNumberOfRegistrants;
                            return
                        }
                    });
                    if(vm.checkAllSessionScanned){
                        vm.checkAllSessionScanned = false;
                    }
                }
            });

            // On confirm, delete the registrants from the session
            vm.confirmDeleteRegistrantsFromSession = function(){
                vm.attendeesStillInSession = [];
                vm.numberOfRegistrantsRemoved = 0;
                $scope.registrantsRemovedFromSessionOnScope = false;
                var i, j, ok = false, arrIds = [];
                vm.remove_type = false;
                if(vm.checkAllSessionRegistrants){
                    vm.remove_type = true;
                } else{
                    _.forEach(vm.sessionAttendees.sessions_attendees, function(sessionAttendee){
                        if(sessionAttendee != undefined && sessionAttendee.attendee.checked){
                            arrIds.push(sessionAttendee.attendee.id);
                        }
                    });
                    vm.remove_type = false;
                }
                ConferenceSessionsService
                    .removeSessionAttendeesFromList(vm.session_id, arrIds, vm.remove_type)
                    .then(function(response){
                        for(i = 0; i < vm.sessionAttendees.sessions_attendees.length; i++){
                            for(j = 0; j < arrIds.length; j++){
                                if(vm.sessionAttendees.sessions_attendees[i].attendee.id !== arrIds[j]){
                                    ok = true;
                                } else{
                                    vm.checkOne(vm.sessionAttendees.sessions_attendees[i].attendee, 'session_registrants');
                                    ok = false;
                                    vm.numberOfRegistrantsRemoved++;
                                    break;
                                }
                            }
                            if(ok === true){
                                vm.attendeesStillInSession.push(vm.sessionAttendees.sessions_attendees[i]);
                            }
                            if(i === (vm.sessionAttendees.sessions_attendees.length - 1)){
                                $scope.registrantsRemovedFromSessionOnScope = true;
                            }
                        }
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Update the scanned/registered attendees number on delete
            $scope.$watch('registrantsRemovedFromSessionOnScope', function(){
                if($scope.registrantsRemovedFromSessionOnScope === true && vm.sessionAttendees.sessions_attendees !== vm.attendeesStillInSession){
                    vm.totalNumberOfRegistrants = !vm.remove_type ? (vm.totalNumberOfRegistrants - $scope.nrOfRemoves) : 0;
                    vm.sessionAttendees.sessions_attendees = vm.attendeesStillInSession;
                    vm.sessionAttendees.total_count = vm.totalNumberOfRegistrants;
                    vm.sessionRegistrantsChecked = false;
                    _.each(vm.sessions, function(session){
                        if(session.id === vm.session_id){
                            session.nr_registered = vm.totalNumberOfRegistrants;
                            return
                        }
                    });
                    if(vm.checkAllSessionRegistrants){
                        vm.checkAllSessionRegistrants = false;
                    }
                }
            });

            // Delete registrants/scanned attendees from a session
            vm.deleteFromSession = function(){
                $scope.confirmDeleteFromSession = vm.sessionRegistrantsView ?
                vm.confirmDeleteRegistrantsFromSession : (vm.sessionAttendeesView ? vm.confirmDeleteAttendeesFromSession : '');
                $scope.type = vm.sessionRegistrantsView ? 'registrants' : (vm.sessionAttendeesView ? 'attendees' : '');
                var nrDeletes = 0;
                var list = vm.sessionRegistrantsView ? vm.sessionAttendees.sessions_attendees : (vm.sessionAttendeesView ? vm.sessionAttendees.scanned_attendees : []);
                _.each(list, function(people){
                    if(people.attendee.checked === true){
                        nrDeletes++;
                    }
                });
                if((vm.sessionRegistrantsView && vm.checkAllSessionRegistrants) || (vm.sessionAttendeesView && vm.checkAllSessionScanned)){
                    $scope.all = true;
                } else{
                    $scope.all = false;
                }
                $scope.nrOfRemoves = nrDeletes;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_session_registrants.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }

            vm.scrollToDay = function() {
                var scrollLeft = $("#days-list-scroll").scrollLeft();
                $("#days-list-scroll").animate({
                  scrollLeft: scrollLeft + 171 * vm.currentExhibitingDayNr
                }, 100);
            }

            vm.sessionsForDay = [];

            vm.setupSessionsForDay = function() {
                if (vm.currentExhibitingDayDate) {
                    vm.sessionSelectedDay = vm.currentExhibitingDayDate.getTime();
                    var day = vm.currentExhibitingDayDate.getDate();
                    var month = vm.currentExhibitingDayDate.getMonth();
                    var year = vm.currentExhibitingDayDate.getFullYear();
                    vm.sessionsForDay = vm.sessionDays[day + '/' + month + '/' + year] || [];
                } else {
                    vm.sessionSelectedDay = vm.conferenceDays[0].getTime();
                    vm.sessionsForDay = vm.sessionDays[day + '/' + month + '/' + year] || [];
                }
            }

            vm.setupSessionsForDay();

            vm.addExhibitingHoursButton = true;
            vm.addExhibitingMoveInOutButton = true;


            vm.sessionSelectDay = function(date) {
                if (vm.exhibitingInfo && vm.exhibitingInfo.location && vm.exhibitingInfo.location != '') {
                    vm.addExhibitingInfo();
                }
                vm.newSession = false;
                vm.sessionSelectedDay = date.getTime();
                var day = date.getDate();
                var month = date.getMonth();
                var year = date.getFullYear();
                vm.sessionsForDay = vm.sessionDays[day + '/' + month + '/' + year] || [];
            }

            if (vm.conferenceDays[0] && vm.sessionSelectedDay == vm.conferenceDays[0].getTime()) {
                vm.sessionSelectDay(vm.conferenceDays[0]);
            }

            var attendeeFieldKey = 'attendeeFields_' + ConferenceService.hash_conference_id.decode(vm.conference_id)[0][0];
            var savedFields = localStorageService.get(attendeeFieldKey);
            var savedFields = null;
            if (savedFields) {
                var customFieldIds = []
                _.each(vm.savedCustomFields, function(existingField){
                    customFieldIds.push(existingField.id);
                });

                var expiredFields = [];
                _.each(savedFields, function(savedField) {
                    if (savedField.id > 0) {
                        if (customFieldIds.indexOf(savedField.id) <= -1) {
                            expiredFields.push(savedField)
                        }

                    }
                });

                _.each(expiredFields, function(expiredField){
                    var idx = savedFields.indexOf(expiredField);
                    if (idx > -1) {
                        savedFields.splice(idx, 1);
                    }
                });

                vm.attendeeFields = savedFields;
            } else {
                vm.attendeeFields = vm.initAttendeeFields;
            }

			sharedData.currentPageTitle = 'manage_conf';

			vm.currentPage = 1;


			vm.editMode = false;
			vm.usersPermissionsPane = false;
			vm.attendeesViewOptionsPane = false;

			vm.attendeesCsvFields = null;
            vm.attendeesCsvKeys = null;
            vm.sessionsCsvKeys = null;
            vm.exhibitorsCsvKeys = null;
			vm.membersCsvKeys = null;
			vm.attendeeFileName = "uploaded file";

			vm.select = function(option){
			    vm.selectedType = null;
				vm.selectedPane = option;
			};

            // Undo csv field mapping
			vm.undoMapping = function(field, type) {
                if(type == 'attendee'){
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].style.left="0px";
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].style.top="0px";
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].style.right="0px";
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].style.bottom="0px";
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].style.bottom="0px";
    			    vm.attendeeFieldMappingObj[field.id].draggable[0].classList.remove("field_is_mapped");

    			    vm.attendeeFieldMappingCode[field.mapping] = null;
    			    vm.attendeeFieldMappingObj[field.id].draggable.mapping=null;
    			    field.mapping = null;
    			    field.is_hovered = null;

                    _.each(vm.attendeeMappingFields, function(csvField) {
                        if (csvField.id == field.id) {
                            csvField.mapping = null;
                            csvField.is_hovered==false;
                        }
                    });
                } else if(type == 'session'){
                    vm.sessionFieldMappingObj[field.id].draggable[0].style.left="0px";
                    vm.sessionFieldMappingObj[field.id].draggable[0].style.top="0px";
                    vm.sessionFieldMappingObj[field.id].draggable[0].style.right="0px";
                    vm.sessionFieldMappingObj[field.id].draggable[0].style.bottom="0px";
                    vm.sessionFieldMappingObj[field.id].draggable[0].style.bottom="0px";
                    vm.sessionFieldMappingObj[field.id].draggable[0].classList.remove("field_is_mapped");

                    vm.sessionFieldMappingCode[field.mapping] = null;
                    vm.sessionFieldMappingObj[field.id].draggable.mapping=null;
                    field.mapping = null;
                    field.is_hovered = null;

                    _.each(vm.sessionMappingFields, function(csvField) {
                        if (csvField.id == field.id) {
                            csvField.mapping = null;
                            csvField.is_hovered==false;
                        }
                    });
                } else if(type == 'exhibitor'){
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].style.left="0px";
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].style.top="0px";
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].style.right="0px";
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].style.bottom="0px";
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].style.bottom="0px";
                    vm.exhibitorFieldMappingObj[field.id].draggable[0].classList.remove("field_is_mapped");

                    vm.exhibitorFieldMappingCode[field.mapping] = null;
                    vm.exhibitorFieldMappingObj[field.id].draggable.mapping=null;
                    field.mapping = null;
                    field.is_hovered = null;

                    _.each(vm.exhibitorMappingFields, function(csvField) {
                        if (csvField.id == field.id) {
                            csvField.mapping = null;
                            csvField.is_hovered==false;
                        }
                    });
                }
			}

            // After the csv field mapping is complete it processes/validates the mapped data and pushes it to the backend
            // For member lists for session levels
            vm.saveAndUploadAttendeeMembers = function() {
                if (vm.startedBulkUpload || !vm.membersCsvFields) return;
                var attendeeBulkUploadPayload = [];
                for (var item in vm.membersCsvFields) {
                    var attendeeObject = {};
                    _.each(vm.membersCsvFields[item], function(k, v) {
                        if (vm.attendeeFieldMappingCode[v] != undefined) {
                          attendeeObject[vm.attendeeFieldMappingCode[v]] = k;
                        }
                    });

                    if (attendeeObject.first_name && attendeeObject.last_name) {
                        attendeeBulkUploadPayload.push(attendeeObject);
                    }
                }

                var firstItem = attendeeBulkUploadPayload[0];

                if (!firstItem) {
                    toastr.error('The First Name field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.first_name) {
                    toastr.error('The First Name field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.last_name) {
                    toastr.error('The Last Name field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.email) {
                    toastr.error('The Email field is mandatory so it must be mapped!');
                    return;
                }

                var bad_email = false;
                _.each(attendeeBulkUploadPayload, function(item){
                    if (item.email && !validateEmail(item.email)) {
                        toastr.error(item.email + " is not a valid email address!");
                        bad_email = true;
                        return false;
                    }
                    _.each(item, function(k, v) {
                        if (k && k.length > 255) {
                            k = k.substring(0,255);
                            item[v] = k;
                        }
                    });

                    if (item.email != undefined && item.email.trim() == '') {
                        item.email = null;
                    }
                });

                if (bad_email) return;

                vm.startedBulkUpload = true;

                ConferenceRegistrationService.bulkCreateMembers(attendeeBulkUploadPayload, vm.currentLevelItem.id)
                    .then(function(response){
                        SharedProperties.conference = null;
                        vm.startedBulkUpload = false;
                        vm.membersCsvFields = null;

                        ConferenceRegistrationService
                            .getRegistrationMembers(vm.currentLevel)
                            .then(function(response){
                                vm.currentLevelItem.registrationlevelmember_set = response;
                                ngDialog.close();
                            })
                            .catch(function(error){

                            });
                    })
                    .catch(function(error){
                        vm.startedBulkUpload = false;
                        var data = error.data;
                        data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"','');
                        data = data.replace('"}','');
                        toastr.error(data);

                    });
            }

            // After the csv field mapping is complete it processes/validates the mapped data and pushes it to the backend
			vm.attendeeBulkUpload = function(session_id) {
			    if (vm.startedBulkUpload || !vm.attendeesCsvFields) return;
                session_id = session_id || false;

			    var attendeeBulkUploadPayload = [];
			    for (var item in vm.attendeesCsvFields) {
			        var attendeeObject = {};
			        _.each(vm.attendeesCsvFields[item], function(k, v) {
                        if (vm.attendeeFieldMappingCode[v] != undefined) {
                          attendeeObject[vm.attendeeFieldMappingCode[v]] = k;
                        }
                    });

                    if (attendeeObject.first_name && attendeeObject.last_name) {
                        attendeeBulkUploadPayload.push(attendeeObject);
                    }
			    }

			    var firstItem = attendeeBulkUploadPayload[0];

                if (!firstItem) {
                    toastr.error('The First Name field is mandatory so it must be mapped!');
                    return;
                }

			    if (!firstItem.first_name) {
			        toastr.error('The First Name field is mandatory so it must be mapped!');
			        return;
			    }

			    if (!firstItem.last_name) {
			        toastr.error('The Last Name field is mandatory so it must be mapped!');
			        return;
			    }

			    if (!firstItem.attendee_id && vm.force_custom_attendee_id) {
			        toastr.error('The Attendee Id field is mandatory for this conference so it must be mapped!');
			        return;
			    }

			    var bad_email = false;

			    _.each(attendeeBulkUploadPayload, function(item){
                    if (item.email_address && item.email_address != '') {
                        item.email_address = item.email_address.trim()
                    }

			        if (item.email_address && !validateEmail(item.email_address)) {
			            toastr.error(item.email_address + " is not a valid email address!");
			            bad_email = true;
			            return false;
			        }
			        _.each(item, function(k, v) {
                        if (k && k.length > 255) {
                            k = k.substring(0,255);
                            item[v] = k;
                        }
                    });

                    if (item.email_address != undefined && item.email_address.trim() == '') {
                        item.email_address = null;
                    }
			    });

			    if (bad_email) return;

			    vm.startedBulkUpload = true;

                if (session_id) {
                    ConferenceAttendeesService.bulkCreateAttendeesInSession(
                        attendeeBulkUploadPayload, ConferenceService.hash_conference_id.decode(vm.conference_id)[0], session_id)
                    .then(function(response){
                        SharedProperties.conference = null;
                        vm.startedBulkUpload = false;
                        vm.attendeesCsvFields = null;
                        // $location.path('/conference/' + vm.conference_id + '/session/' + vm.session_id);
                    })
                    .catch(function(error){
                        vm.startedBulkUpload = false;
                        toastr.error(error.data.detail);

                    });
                } else {
                    ConferenceAttendeesService.bulkCreateAttendees(
                        attendeeBulkUploadPayload, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        SharedProperties.conference = null;
                        vm.startedBulkUpload = false;
                        vm.attendeesCsvFields = null;
                        $location.path('/conference/' + vm.conference_id + '/attendees');
                    })
                    .catch(function(error){
                        vm.startedBulkUpload = false;
                        toastr.error(error.data.detail);
                    });
                }

			}

            // Bulk upload sesions from csv file
            // Work in progress
            vm.sessionBulkUpload = function() {
                if (vm.startedBulkUpload || !vm.sessionsCsvFields) return;

                var sessionBulkUploadPayload = [];
                for (var item in vm.sessionsCsvFields) {
                    var sessionObject = {};
                    _.each(vm.sessionsCsvFields[item], function(k, v) {
                        if (vm.sessionFieldMappingCode[v] != undefined) {
                          sessionObject[vm.sessionFieldMappingCode[v]] = k;
                        }
                    });
                    sessionBulkUploadPayload.push(sessionObject);
                }

                sessionBulkUploadPayload.splice(-1,1)

                var firstItem = sessionBulkUploadPayload[0];

                if (!firstItem) {
                    toastr.error('The Session Title field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.title) {
                    toastr.error('The Session Title field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.session_date) {
                    toastr.error('The Date field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.location) {
                    toastr.error('The Location field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.starts_at) {
                    toastr.error('The Start Time field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.ends_at) {
                    toastr.error('The End Time field is mandatory so it must be mapped!');
                    return;
                }

                if (!firstItem.session_type) {
                    toastr.error('The Session Type field is mandatory so it must be mapped!');
                    return;
                }

                if(!firstItem.speaker_first_name && firstItem.speaker_last_name){
                    toastr.error("The Speaker's First field is mandatory so it must be mapped!");
                    return;
                }

                if(firstItem.speaker_first_name && !firstItem.speaker_last_name){
                    toastr.error("The Speaker's Last field is mandatory so it must be mapped!");
                    return;
                }

                var badDate = false;
                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                _.each(sessionBulkUploadPayload, function(item){
                    var start_full_date = item.session_date + ' ' + item.starts_at;
                    try {
                        var starts_at = new Date(start_full_date);
                        starts_at.setTime( starts_at.getTime() - (starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                    }
                    catch(err) {
                        toastr.error(start_full_date + " is not a valid date!");
                        badDate = true;
                        return false;
                    }

                    var end_full_date = item.session_date + ' ' + item.ends_at;
                    try {
                        var ends_at = new Date(end_full_date);
                        ends_at.setTime( ends_at.getTime() - (ends_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                    }
                    catch(err) {
                        toastr.error(start_full_date + " is not a valid date!");
                        badDate = true;
                        return false;
                    }

                    var session_type = _.find(vm.sessionTypeOptions, {name: item.session_type});
                    if (!session_type) {
                        toastr.error(item.session_type + " is not a valid session type!");
                        badDate = true;
                        return false;
                    }

                    item.session_type = session_type.id;



                    if (!(new Date(item.session_date) >= new Date(vm.conferenceDays[0]))) {
                        toastr.error(item.session_date + " is before the conference start date!");
                        badDate = true;
                        return false;
                    }

                    if (!(new Date(item.session_date) <= new Date(vm.conferenceDays[vm.conferenceDays.length - 1]))) {
                        toastr.error(item.session_date + " is after the conference end date!");
                        badDate = true;
                        return false;
                    }

                    if(item.starts_at.search(/(pm|am)/i) < 2){
                        toastr.error(item.starts_at + ": the start time of the " + item.title + " does not have a valid format!");
                        badDate = true;
                        return false;
                    } else if(item.ends_at.search(/(pm|am)/i) < 2){
                        toastr.error(item.ends_at + ": the end time of the " + item.title + " does not have a valid format!");
                        badDate = true;
                        return false;
                    } else{
                        item.starts_at = starts_at.toISOString();
                        item.ends_at = ends_at.toISOString();
                    }

                    item.sessionquestion_set = [];

                    if (item.speaker_first_name && item.speaker_last_name) {
                        item.sessionspeaker_set = [{
                            speaker: {
                                first_name: item.speaker_first_name,
                                last_name: item.speaker_last_name,
                                email: item.speaker_email,
                                company: item.speaker_company,
                                phone_number: item.speaker_phone_number,
                                facebook_url: item.speaker_facebook,
                                twitter_url: item.speaker_twitter,
                                linkedin_url: item.speaker_linkedin,
                                youtube_url: item.speaker_youtube,
                                biography: item.speaker_biography
                            }
                        }];
                    } else {
                        item.sessionspeaker_set = [];
                    }
                    item.sessioncontinuingeducation_set = [];
                    if (item.continuing_education_data) {
                        var ce_items = item.continuing_education_data.split(';');
                        _.each(ce_items, function(ce_item){
                            ce_item = ce_item.trim()
                            if (ce_item != "") {
                                item.sessioncontinuingeducation_set.push(
                                    {
                                        "continuing_education": {
                                            "name": ce_item
                                        }
                                    }
                                );
                            }

                        });

                    }

                    item.max_capacity = item.maximum_capacity;
                    item.customsessionfieldvalue_set = [];
                    if (!item.ce_hours) item.ce_hours = 0;
                    item.sessionfile_set = [];

                    console.log('vm.sessionCustomFieldsPerConference');
                    console.log(vm.sessionCustomFieldsPerConference);

                    _.each(item, function(k, v) {
                        var custom_field = _.find(vm.sessionCustomFieldsPerConference, {
                            name:v}
                        );
                        if (custom_field) {
                            item.customsessionfieldvalue_set.push({
                                value: k,
                                custom_session_field: custom_field.id
                            });
                        }
                    });

                    if (item.session_date != undefined && item.session_date.trim() == '') {
                        item.session_date = null;
                    }
                });

                if(badDate) {
                    return;
                }

                vm.startedBulkUpload = true;

                ConferenceSessionsService.bulkCreateSession(sessionBulkUploadPayload, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        SharedProperties.conference = null;
                        vm.startedBulkUpload = false;
                        vm.sessionsCsvFields = null;
                        SharedProperties.sessions = null;
                        SharedProperties.registrationSessions = null;
                        $state.go('home.sessions', {conference_id: vm.conference_id});
                    })
                    .catch(function(error){
                        vm.startedBulkUpload = false;
                        var data = error.data;
                        data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"','');
                        data = data.replace('"}','');
                        toastr.error(data);

                    });
            }

            vm.exhibitorBulkUpload = function() {
                if (vm.startedBulkUpload || !vm.exhibitorsCsvFields) return;

                var exhibitorBulkUploadPayload = [];
                for (var item in vm.exhibitorsCsvFields) {
                    var exhibitorObject = {};
                    _.each(vm.exhibitorsCsvFields[item], function(k, v) {
                        if (vm.exhibitorFieldMappingCode[v] != undefined && k != undefined) {
                          exhibitorObject[vm.exhibitorFieldMappingCode[v]] = k;
                        }
                    });
                    if (!exhibitorObject.name && !exhibitorObject.booth) continue;
                    if(exhibitorObject.hasOwnProperty('booth_contact_email') && !_.isEmpty(exhibitorObject.booth_contact_email) &&
                        ( exhibitorObject.booth_contact_email.indexOf('@') < 2 ||
                        exhibitorObject.booth_contact_email.indexOf('@') > exhibitorObject.booth_contact_email.lastIndexOf('.') )){
                        toastr.error(exhibitorObject.booth_contact_email + " is not a valid email address!");
                        return;
                    }
                    // TODO add further validation and toastr cards like in the attendees endpoint
                    // if (!firstItem) {
                    //     toastr.error('The Session Title field is mandatory so it must be mapped!');
                    //     return;
                    // }
                    //
                    // if (!firstItem.title) {
                    //     toastr.error('The Session Title field is mandatory so it must be mapped!');
                    //     return;
                    // }
                    //
                    // if (!firstItem.session_date) {
                    //     toastr.error('The Date field is mandatory so it must be mapped!');
                    //     return;
                    // }
                    //
                    // if (!firstItem.location) {
                    //     toastr.error('The Location field is mandatory so it must be mapped!');
                    //     return;
                    // }
                    exhibitorBulkUploadPayload.push(exhibitorObject);
                }


                vm.startedBulkUpload = true;

                ConferenceExhibitorsService.uploadExhibitors(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], exhibitorBulkUploadPayload)
                    .then(function(response){
                        SharedProperties.conference = null;
                        vm.startedBulkUpload = false;
                        vm.exhibitorsCsvFields = null;
                        $state.go('home.exhibitors', {conference_id: vm.conference_id});
                    })
                    .catch(function(error){
                        vm.startedBulkUpload = false;
                        var data = error.data;
                        data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"','');
                        data = data.replace('"}','');
                        toastr.error(data);

                    });
            }

            function validateSessionDate(session_date){
                if(vm.sessionDays[session_date] != undefined){
                    return true;
                } else{
                    return false;
                }
            }

			function validateEmail(email) {
                var re = /^[^@]+@[^@]+.[a-zA-Z]{2,6}$/;
                return re.test(email);
            }

            // Create an attendee custom field and it's options if applicable
			vm.createCustomField = function() {
			    var customField = _.clone(vm.newCustomField), ok = true, nameOk = true, optionsOk = true;
                if (!customField || !customField.name) {
                    toastr.error('Field name is required');
                    nameOk = false;
                }
                else if(_.isEmpty(customField.name.trim())){
                    toastr.error('Field name is required');
                    nameOk = false;
                } else if(!vm.updateCustomFieldBoolean){
                     _.each(vm.customFields, function(field){
                        if(field.name === customField.name.trim()){
                            toastr.error('You already have a field with this name');
                            ok = false;
                            return
                        }
                    });
                }

                if(ok && nameOk && optionsOk){
    			    if (vm.newOption && vm.newOption != '') {
    			        customField.options.push(
    			            {
    			                value: vm.newOption
    			            }
    			        );
    			    }

                    vm.newOption = null;
                    customField.conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];

                    ConferenceAttendeesService.createCustomField(customField)
                        .then(function(result) {
                            if (!customField.id) {
                                result.regsitration_form_type_code = 'Optional';
                                result.regsitration_form_type = 'optional';
                                vm.savedCustomFields.push(result);
                                vm.attendeeMappingFields.push(result);
                            }
                            vm.customFields = vm.removableAttendeeFields.concat(vm.savedCustomFields);
                            vm.attendeeMappingFields = vm.attendeeMappingFields.concat(vm.savedCustomFields);

                            vm.cancelEditField();
                        })
                        .catch(function(error){
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                lib.processValidationError(error, toastr);
                            }
                        });
                }
			}

            // Delete a custom field
			vm.deleteCustomField = function(customField) {
			    ConferenceAttendeesService.deleteCustomField(customField.id)
                    .then(function(result) {
                        _.pull(vm.savedCustomFields, customField);
                        vm.customFields = vm.removableAttendeeFields.concat(vm.savedCustomFields);
                        vm.attendeeMappingFields = vm.attendeeMappingFields.concat(vm.savedCustomFields);
                        if(vm.newCustomField.hasOwnProperty('id') && customField.id == vm.newCustomField.id){
                            vm.cancelEditField();
                        }
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
			};

            // Get edit custom field data
			vm.getEditCustomField = function(customField) {
			    vm.newCustomField = customField;
			    vm.customFieldSelected = vm.newCustomField.type == 1 ?
                'Text' : (vm.newCustomField.type == 3 ? 'Multiple Choice' : (vm.newCustomField.type == 2 ? 'Single Choice' : '')) ;
                vm.customFieldsTypePane = false;
                vm.updateCustomFieldBoolean = true;
			};

            // Undo edit mode for custom field
			vm.cancelEditField = function() {
			    vm.newCustomField = {
                    options: [],
                    type: 1
                }
                vm.customFieldSelected = "Text";
                vm.customFieldsTypePane = false;
                vm.updateCustomFieldBoolean = false;
			};

            // TODO move these functions in a library file
			vm.hasEditButton = function() {
			    if (vm.selectedPane!='details') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
			    if ((vm.conferencePermissions.indexOf('edit_conference') <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
			};

            vm.permissionsForUsers = function() {
                var canEditConferenceUsers = vm.conferencePermissions.indexOf("edit_conference_users") >= 0;
                var isAdmin = vm.conferencePermissions.indexOf("admin") >= 0;


                if(vm.selectedType!='users') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if(canEditConferenceUsers || isAdmin) {
                    return true;
                }
                else {
                    return false;
                }
            };

            vm.permissionsForEditAttendeeFields = function(){
                if(vm.selectedPane!='attendees') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("edit_attendee_fields") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForManageAttendees = function(){
                if(vm.selectedPane!='attendees' && vm.selectedPane!='sessions') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("manage_attendees") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForDownloadAttendees = function(){
                if(vm.selectedPane!='attendees' && vm.selectedPane!='sessions') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("download_attendees") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForManageSessions = function(){
                if(vm.selectedPane!='sessions' && vm.selectedPane!='attendees') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("manage_sessions") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForManageRegistration = function(){
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("edit_registration") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForRefund = function(){
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("can_refund") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            vm.permissionsForManageExhibitors = function(){
                if(vm.selectedPane!='exhibitors' && vm.selectedPane!='details') return false;
                if(_.isEmpty(vm.conferencePermissions)) return false;
                if((vm.conferencePermissions.indexOf("manage_exhibitors") <= -1) && (vm.conferencePermissions.indexOf('admin') <= -1)) return false;
                else{
                    return true;
                }
            };

            // Upload an attendee csv file, process and redirect to the csv mapping view
            // TODO -- rename function for more accurate representation
            vm.uploadFile = function(files) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = lib.CSV2JSON(reader.result);
                    vm.attendeesCsvFields = data;
                   var keys = Object.keys(data[0]);
                   vm.attendeesCsvKeys = [];
                   _.each(keys, function(csvField) {
                       vm.attendeesCsvKeys.push({
                        'code': csvField
                       });
                   });

                   vm.attendeeFileName = files[0].name;
                   $scope.$apply();
                }

                vm.attendeeFieldMapping = {};
                vm.attendeeFieldMappingObj = {};
                vm.attendeeFieldMappingCode = {};

                _.each(vm.attendeesCsvKeys, function(csvField) {
                    csvField.mapping = null;
                });

                _.each(vm.attendeeMappingFields, function(field) {
                    field.mapping = null;
                    field.is_hovered = null;
                });

                reader.readAsText(files[0], 'ISO-8859-1');
            };

            // Generate the session csv file and download to pc
            vm.downloadSessionTemplate = function(){
                var csvData =  lib.sessionsJSON2CSV(vm.sessionMappingFields, vm.sessionMappingFieldsSample);
                var anchor = angular.element('<a/>');
                anchor.css({display: 'none'}); // Make sure it's not visible
                angular.element(document.body).append(anchor); // Attach to document

                var date = new Date();
                var filename = "ExpoSessionTemplate.csv";

                anchor.attr({
                    href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
                    target: '_blank',
                    download: filename
                })[0].click();

                anchor.remove();
            };

            // Uploads a session file
            // Work in progress
            vm.uploadSessionsFile = function(files) {
                var reader = new FileReader();
                reader.onload = function(e) {
                   var data = lib.CSV2JSON(reader.result);
                   vm.sessionsCsvFields = data;
                   var keys = Object.keys(data[0]);
                   vm.sessionsCsvKeys = [];

                   _.each(keys, function(csvField) {
                       vm.sessionsCsvKeys.push({
                        'code': csvField
                       });
                   });

                   vm.sessionFileName = files[0].name;
                   $scope.$apply();
                }
                vm.sessionFieldMapping = {};
                vm.sessionFieldMappingObj = {};
                vm.sessionFieldMappingCode = {};

                _.each(vm.sessionsCsvKeys, function(csvField) {
                    csvField.mapping = null;
                });

                _.each(vm.sessionMappingFields, function(field) {
                    field.mapping = null;
                    field.is_hovered = null;
                });

                reader.readAsText(files[0]);
            };

            vm.uploadCSS = function(files) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        promises.push(FileService.uploadFile(files[0], response));
                        promises.push(ConferenceRegistrationService.setRegistrationCSS(
                            vm.conference.registration_form, file_link, files[0].name, new Date()));

                        $q.all(promises).then(function(response){
                            vm.conference.registration_form.custom_css = file_link;
                            vm.conference.registration_form.custom_css_file_name = files[0].name;
                            vm.conference.registration_form.custom_css_file_date = new Date();
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            };

            vm.updateTrackingCode = function(){
                ConferenceRegistrationService
                    .setTrackingCode(vm.conference.registration_form)
                    .then(function(response){
                    }).catch(function(error){

                    })
            }

            vm.removeCSS = function() {
                ConferenceRegistrationService
                    .setRegistrationCSS(vm.conference.registration_form, null, null, null)
                    .then(function(response){
                        vm.conference.registration_form.custom_css = null;
                        vm.conference.registration_form.custom_css_file_name = null;
                        vm.conference.registration_form.custom_css_file_date = null;
                    }).catch(function(error){

                    })
            }

            vm.openEditExhibitorProfilePopup = function(exhibitor){
                ngDialog.close();
                setTimeout(function(){
                    $scope.editModeExhibitorProfileFields = vm.editModeExhibitorProfileFields;
                    $scope.exhibitorToEdit = _.clone(exhibitor, true);
                    $scope.updateExhibitor = vm.updateExhibitor;
                    ngDialog.open({template: 'app/views/partials/conference/edit_conference_exhibitor.html',
                      className: 'app/stylesheets/_editConferenceExhibitor.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                    });
                }, 700);
            };

            vm.updateExhibitor = function(exhibitor_to_update){
                var exhibitor = _.find(vm.exhibitors, {id:exhibitor_to_update.id});
                if (!exhibitor) return;
                exhibitor.booth = exhibitor_to_update.booth;
                exhibitor.name = exhibitor_to_update.name;
                exhibitor.company_name = exhibitor_to_update.name;
                exhibitor.booth_contact_first_name = exhibitor_to_update.booth_contact_first_name;
                exhibitor.booth_contact_last_name = exhibitor_to_update.booth_contact_last_name;
            };

            vm.addConferenceExhibitor = function(exhibitor_to_add, type){
                var last_exhibitor = _.first(exhibitor_to_add.exhibitor_set);
                last_exhibitor.company_name = last_exhibitor.name;
                vm.exhibitors.push(last_exhibitor);
                if(type == 'another'){
                    setTimeout(function(){
                        vm.openAddExhibitorPopup();
                    }, 1000);
                }
            }

            vm.downloadFeedbackAction = function(session) {
                ConferenceSessionsService.listSessionFeedbacks(session.id)
                    .then(function(response){
                        var feedbackList = [];
                        var sessionTitles = []
                        _.each(response.attendee_feedbacks, function(attendee_feedback) {
                            var feedbackItem = {};
                            feedbackItem['attendee'] = attendee_feedback.attendee.first_name + ' ' + attendee_feedback.attendee.last_name;
                            feedbackItem['attendee_id'] = attendee_feedback.attendee.attendee_id;
                            sessionTitles.push('attendee');
                            sessionTitles.push('attendee_id');

                            _.each(attendee_feedback.feedback_set, function(feedback_question){
                                if (feedback_question.session_question.type == 1) {
                                    feedbackItem[feedback_question.session_question.text] = feedback_question.answer.answer;
                                } else if (feedback_question.session_question.type == 3) {
                                    var options = feedback_question.answer.options;
                                    var optionsTxt = "";
                                    _.each(options, function(option){
                                        var item = _.find(feedback_question.session_question.sessionquestionoption_set, {'id': option.integer_value});
                                        if (item) {
                                            optionsTxt += item.value + "; ";
                                        }
                                    });

                                    feedbackItem[feedback_question.session_question.text] =optionsTxt;

                                } else if (feedback_question.session_question.type == 2){
                                    var item = _.find(feedback_question.session_question.sessionquestionoption_set, {'id': feedback_question.answer.option});
                                    feedbackItem[feedback_question.session_question.text] =  item.value;
                                }
                                sessionTitles.push(feedback_question.session_question.text);
                            });
                            feedbackList.push(feedbackItem);
                        });

                        sessionTitles = _.uniq(sessionTitles);
                        var csvData =  lib.ConvertToCSVImproved(feedbackList, sessionTitles);

                        var date = new Date();
                        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                        var filename = vm.conference.name + actualDate + "_feedback.csv";
                        lib.printCsvFile(csvData, filename);


                    }).catch(function(error){

                    });
            }

            vm.uploadExhibitorsListFile = function(files){
                var reader = new FileReader();
                reader.onload = function(e) {
                   var data = lib.CSV2JSON(reader.result);
                   vm.exhibitorsCsvFields = data;
                   var keys = Object.keys(data[0]);
                   vm.exhibitorsCsvKeys = [];

                   _.each(keys, function(csvField) {
                       vm.exhibitorsCsvKeys.push({
                        'code': csvField
                       });
                   });

                   vm.exhibitorsListFileName = files[0].name;
                   $scope.$apply();
                }
                vm.exhibitorFieldMapping = {};
                vm.exhibitorFieldMappingObj = {};
                vm.exhibitorFieldMappingCode = {};

                _.each(vm.exhibitorsCsvKeys, function(csvField) {
                    csvField.mapping = null;
                });

                $scope.$apply();

                _.each(vm.exhibitorMappingFields, function(field) {
                    field.mapping = null;
                    field.is_hovered = null;
                });

                reader.readAsText(files[0]);
            };

            vm.averyTemplateFileName = null;
            vm.averyTemplateFile = null;
            vm.averyTemplateType = 1; // Badge is default

            // Store the uploaded file on the web app to be uploaded by a future action
            vm.uploadTemplate = function(files) {
								if(!_.isEmpty(files)){
			          vm.averyTemplateFileName = files[0].name;
                vm.averyTemplateFile = files[0];
							}
            };

            // Upload a template file and add it on the backend and update the view
            vm.uploadTemplateAction = function() {
                if (!vm.averyTemplateFile) return;
                FileService.getUploadSignature(vm.averyTemplateFile, true)
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        var data = {
                            conference_id: ConferenceService.hash_conference_id.decode(vm.conference_id)[0],
                            file_url: file_link,
                            name: vm.averyTemplateFile.name,
                            type: vm.averyTemplateType
                        }

                        promises.push(FileService.uploadFile(vm.averyTemplateFile, response));
                        promises.push(ConferenceService.addTemplate(data));

                        $q.all(promises).then(function(response){
                            vm.conference.templates.push(response[1]);

                            vm.averyTemplateFileName = null;
                            vm.averyTemplateFile = null;
                            vm.averyTemplateType = 1; // Badge is default
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                    });
            }

            // Removes an avery template and updates the view
            vm.removeTemplateAction = function(template) {
                ConferenceService.removeTemplate(template.id)
                    .then(function(response){
                        var index = vm.conference.templates.indexOf(template);
                        if (index > -1) {
                            vm.conference.templates.splice(index, 1);
                        }
                    })
                    .catch(function(error){

                    });
            }

            // Undo all edits made to the conference that were not saved
            // disable edit mode
            vm.cancelEditConference = function() {
                vm.editMode = false;
                if (cloneConference) {
                    vm.conference = cloneConference;
                    $rootScope.$emit('Conference', vm.conference);
                }
            }

            vm.addNewSpeaker = function() {
                vm.newSessionItem.speakers.push({});
            }

            // Uploads the session speaker's headshot
            vm.uploadHeadshot = function(files, speaker) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        if(speaker.id) {
                            promises.push(FileService.uploadFile(files[0], response));
                        } else {
                            promises.push(FileService.uploadFile(files[0], response));
                        }

                        $q.all(promises).then(function(response){
                            speaker.headshot_url = file_link;
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Uploads the support contact's photo
            vm.uploadSupportPhoto = function(files, support_contact) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        promises.push(FileService.uploadFile(files[0], response));

                        $q.all(promises).then(function(response){
                            support_contact.photo = file_link;
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // On confirmation it will upload the exhibitor material
            vm.uploadExhibitorMaterialConfirm = function() {
                if (!vm.exhibitorMaterialFile) return;

                FileService.getUploadSignature(vm.exhibitorMaterialFile)
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        var data = {
                            conference_id: ConferenceService.hash_conference_id.decode(vm.conference_id)[0],
                            file_url: file_link,
                            name: vm.exhibitorMaterialFile.name,
                            type: vm.exhibitorMaterialFile.type
                        }

                        promises.push(FileService.uploadFile(vm.exhibitorMaterialFile, response));
                        promises.push(ConferenceExhibitorsService.addExhibitorMaterial(data));

                        $q.all(promises).then(function(response){
                            vm.exhibitor_materials.push(response[1]);
                            vm.exhibitorMaterialFile = null;
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });

            }

            // Saves a conference material file to be uploaded on a future action
            vm.uploadExhibitorMaterial = function(files) {
                vm.exhibitorMaterialFile = files[0];
            }

            // Uploads a session material file
            vm.uploadMaterial = function(files) {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        if (!vm.newSessionItem) {
                            vm.newSessionItem = vm.sessionDetailsView;
                        }

                        if (vm.newSessionItem.id) {
                            var data = {
                                session_id: vm.session_id,
                                file_url: file_link,
                                name: files[0].name,
                                type: files[0].type
                            }

                            promises.push(FileService.uploadFile(files[0], response));

                            promises.push(ConferenceSessionsService.addSessionFile(data));

                            $q.all(promises).
                            then(function(response){
                                if (!vm.sessionDetailsView.session_files) {
                                    vm.sessionDetailsView.session_files = [];
                                }
                                vm.sessionDetailsView.session_files.push(response[1]);
                                if (!vm.newSessionItem.session_files) {
                                    vm.newSessionItem.session_files = [];
                                }
                                vm.newSessionItem.session_files.push(response[1]);
                            }).catch(function(error){
                                console.log(error);
                            });
                        } else {
                            var data = {
                                file_url: file_link,
                                name: files[0].name,
                                type: files[0].type
                            }

                            if (!vm.newSessionItem.materials) {
                                vm.newSessionItem.materials = [];
                            }

                            vm.newSessionItem.materials.push(data);

                            promises.push(FileService.uploadFile(files[0], response));
                            $q.all(promises).then(function(response){
                            }).catch(function(error){

                            });
                        }
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            vm.removeSessionFile = function(session_file_id){

                ConferenceSessionsService
                    .removeSessionFile(session_file_id)
                    .then(function(response){
                        vm.sessionDetailsView.session_files = _.reject(vm.sessionDetailsView.session_files, ['id', session_file_id]);
                        vm.newSessionItem.session_files = _.reject(vm.newSessionItem.session_files, ['id', session_file_id]);
                    })
                    .catch(function(error){

                    });
            };

            vm.openSessionFile = function(file) {
                $window.open(file.file_url, '_blank');
            }

            vm.presetSessionDate = function(){
                if(vm.newSessionItem.ends_at_editable === undefined ||
                        vm.newSessionItem.ends_at_editable === true) {
                    vm.newSessionItem.ends_at_editable = _.cloneDeep(vm.newSessionItem.starts_at_editable);
                    document.getElementById('ends_at').focus();
                }
            };

            // On confirmation it deletes the conference and updates the view
            vm.confirmDeleteConference = function() {
                ConferenceService.deleteConference(vm.conference, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(){
                        toastr.success('The conference has been deleted successfully.', 'Conference Deleted!');
                        vm.editMode = false;
                        $location.path('/conferences/');
                    })
                    .catch(function(error){

                        toastr.error('Error!', 'There has been a problem with your request.');
                    })
            }

            // On confirmation it deletes the session and updates the view
            vm.confirmDeleteSession = function() {
                ConferenceSessionsService.deleteSession(vm.sessionDetailsView.id)
                    .then(function(){
                        toastr.success('The session has been deleted successfully!');
                        _.remove(vm.sessions, {'id':vm.sessionDetailsView.id});
												_.remove(vm.sessionsForDay, {'id':vm.sessionDetailsView.id});
                        SharedProperties.sessions = {
                            'sessions': vm.sessions
                        }
                        var startsAt = new Date(_.clone(vm.sessionDetailsView.starts_at));
                        var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                        startsAt.setTime( startsAt.getTime() + (startsAt.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                        var day = startsAt.getDate();
                        var month = startsAt.getMonth();
                        var year = startsAt.getFullYear();
                        if (!vm.sessionDays[day + '/' + month + '/' + year]) {
                            vm.sessionDays[day + '/' + month + '/' + year] = [];
                        }
                        _.remove(vm.sessionDays[day + '/' + month + '/' + year], {'id':vm.sessionDetailsView.id});
                        vm.viewSessionDetails(null);
                        vm.newSession = null;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Opens confirmation modal for deleting the conference
            vm.deleteConference = function() {
                $scope.deleteConferenceConfirm = vm.confirmDeleteConference;
                $scope.resource = 'attendees';
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_conference.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }

            // Opens confirmation modal for deleting the session
            vm.deleteSession = function() {
                $scope.deleteSessionConfirm = vm.confirmDeleteSession;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_session.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }

            // Updates the conference data
            // If a new logo was selected it will first upload it
            vm.updateConference = function() {
                if ($scope.conferenceLogo || (vm.conference.logo && ((vm.conference.logo.indexOf('data:image/png;base64') > -1) || (vm.conference.logo.indexOf('blob:') > -1)))) {
                    if (!$scope.conferenceLogo) {
                        $scope.conferenceLogo = vm.conference.conferenceLogoFile;
                    }
                    FileService.getUploadSignature($scope.conferenceLogo)
                        .then(function(response){
                            var file_link = response.data.file_link;
                            var promises = [];

                            promises.push(FileService.uploadFile($scope.conferenceLogo, response));
                            var cloneConference = _.clone(vm.conference, true);
                            cloneConference.logo = file_link;
                            promises.push(ConferenceService.updateConference(cloneConference));

                            $q.all(promises).then(function(response){
                                toastr.success('The conference has been updated successfully.', 'Conference Updated!');
                                vm.editMode = false;
                            }).catch(function(error){
                                lib.processValidationError(error, toastr);
                            });

                        })
                        .catch(function(error){

                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                lib.processValidationError(error, toastr);
                            }
                        });
                }
                else {
                    var cloneConference = _.cloneDeep(vm.conference, true);
                    cloneConference.date_from = new Date(cloneConference.date_from_editable);
                    cloneConference.date_to = new Date(cloneConference.date_to_editable);

                    if (cloneConference.state.abbreviation) {
                        cloneConference.state = cloneConference.state.abbreviation;
                    } else {
                        cloneConference.state = cloneConference.state;
                    }

                    if (cloneConference.country.name) {
                        cloneConference.country = cloneConference.country.name;
                    } else {
                        cloneConference.country = cloneConference.country;
                    }

                    ConferenceService.updateConference(cloneConference)
                        .then(function(response){
                            toastr.success('The conference has been updated successfully.', 'Conference Updated!');
                            vm.editMode = false;
                            vm.googleMap = lib.generateGoogleMaps(cloneConference.country, cloneConference.city, cloneConference.street);
                            vm.conference.date_from = new Date(response.date_from);
                            vm.conference.date_to = new Date(response.date_to);
                            var country_name = cloneConference.country;
                            if (cloneConference.country.name) {
                                country_name = cloneConference.country.name;
                            }

                            vm.googleStaticMap = lib.generateGoogleMapsDetailed(API.GOOGLE_API_KEY, country_name, cloneConference.city, cloneConference.street);
                        })
                        .catch(function(error){

                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                lib.processValidationError(error, toastr);
                            }
                        });
                }
            }

            vm.attendeesCancelUpload = function() {
                vm.attendeesCsvFields = null;
                vm.attendeeFileName = "uploaded file";
            };
            vm.exhibitorsCancelUpload = function() {
                vm.exhibitorsCsvFields = null;
                vm.exhibitorsListFileName = "uploaded file";
            };
            vm.sessionsCancelUpload = function() {
                vm.sessionsCsvFields = null;
                vm.sessionFileName = "uploaded file";
            };

			vm.selectType = function($event, option, option2){
                if ((vm.selectedType == null && option2 == null) || (vm.selectedPane != option)) {
                } else {
                    vm.selectedPane = option;
                    vm.selectedType = option2;
                    $event.stopPropagation();
                }
				if(option == 'registration' && option2 == null && !vm.conference.registration_form && !vm.nextButtonBlocked){
					vm.registrationSteps = {
						attendee_fields_entry_panel: true,
						registration_levels_setup_panel: false,
						registration_level_options_panel: false,
						legal_panel: false,
						review_panel: false
					};
				}
            };

            vm.testHasRegistration = function() {
                return (vm.conference.has_registration_form);
            }

            vm.getConferenceForm = function() {
                ConferenceRegistrationService.getRegistrationFormV1(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        SharedProperties.registrationForm = response;
                        if(response.length != 0){
                            vm.setRegistrationFormData(response);
                        } else {
                            vm.nextButtonBlocked = false;
                        }

                    })
                    .catch(function(error){
                        if (error.status == 404) {
                            vm.setupStats(7, 'no_form');
                            vm.conference.registration_form = null;
                            vm.registrationStats = null;
                        }
                    });
            }

            vm.createRegistrationForm = function() {
                ConferenceRegistrationService
                    .startRegistrationForm(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        vm.getConferenceForm();
                        vm.conference.has_registration_form = true;
                    })
                    .catch(function(error){
                        vm.conference.registration_form = null;
                    });
            }

            if (selectedPane == 'registration' && selectedType == 'create_registration_form' && vm.conference.registration_form == null) {
                vm.createRegistrationForm();
            }

            $scope.openMerchantAgreement = function() {
                ngDialog.close();
                setTimeout(function () {
                    vm.startMerchantAgreement();
                }, 700);
            };

			vm.openStartRegistrationFormPopup = function(){
                ngDialog.close();
                setTimeout(function(){
                    $scope.createRegistrationForm = vm.createRegistrationForm;
                    $scope.openMerchantAgreement = $scope.openMerchantAgreement;
                    $scope.hasMerchantAccount = vm.conference.business_data.merchant_id_verified;
                    ngDialog.open({
                        template: 'app/views/partials/conference/registration-steps/start_registration_form_popup.html',
                        className: 'app/stylesheets/_startRegistrationFormModal.scss',
                        scope: $scope
                    });
                }, 700);

			 };

            vm.printCsvListFile = function(checkedAttendeeList, fields) {
                    var csvData =  lib.ConvertToCSVImproved(checkedAttendeeList, fields);

                    var anchor = angular.element('<a/>');
                    anchor.css({display: 'none'}); // Make sure it's not visible
                    angular.element(document.body).append(anchor); // Attach to document

                    var date = new Date();

                    var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                    var filename = vm.conference.name + actualDate + ".csv";

                    var url = URL.createObjectURL( new Blob( [csvData], {type:'application/csv'} ) );
                    anchor.attr({
                            href: url,
                            target: '_blank',
                            download: filename
                    })[0].click();

                    anchor.remove();
            }

            vm.downloadSessionAttendeesAsCsv = function() {
                var attendee_ids = [];
                var checkedAttendeeList = [];
                var list = [];
                var type = 'Scan 1';
                var type_by = 'Scan 1 by';
                var type_url = 'scanned_in_session';
                var total_number = vm.totalNumberOfRegistrants;
                var scanned_again_at = 'Scanned Again At';
                if(vm.sessionRegistrantsView){
                        type = 'Registered At';
                        type_by = 'Registered By';
                        type_url = 'registered_to_session';
                        list = vm.sessionAttendees.sessions_attendees;
                        scanned_again_at = null;
                } else if(vm.sessionAttendeesView){
                        list = vm.sessionAttendees.scanned_attendees;
                }

                if ((!vm.checkAllSessionRegistrants && vm.sessionRegistrantsView) || (!vm.checkAllSessionScanned && vm.sessionAttendeesView)) {
                    _.each(list, function(attendee) {
                            if (attendee.attendee.checked == true) {
                                    attendee_ids.push(attendee.attendee.id);
                            }
                    });
                }

                var extra = type_url + "=" + vm.session_id;

                var promises = [];
                if (attendee_ids.length == 0) {
                        var nr_pages = parseInt(total_number / 30) + 2;
                        for (var idx = 1; idx <= nr_pages + 1; idx ++) {
                                promises.push(ConferenceAttendeesService.downloadAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], attendee_ids, extra, idx));
                        }
                } else {
                        promises.push(ConferenceAttendeesService.downloadAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], attendee_ids, extra));
                }

                $q.all(promises).then(function(responses){
                        var allAttendees = [];
                        _.each(responses, function(attendees){
                                allAttendees = allAttendees.concat(attendees);
                        });

                        var checkedAttendeeList = [];
                        var customFields = [];
                        var sessionTitles = [];
                        _.each(allAttendees, function(attendee) {
                                var mainProfileAttendee = _.omit(attendee,
                                ['custom_fields' , 'sessionattendee_set', 'scannedattendee_set', 'customattendeefieldvalue_set',
                                'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                                _.forEach(attendee.customattendeefieldvalue_set, function(customField){
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                        customFields.push(customField.custom_attendee_field.name);
                                });
                                _.forEach(attendee.scannedattendee_set, function(scannedAttendee){
                                    if (scannedAttendee.session.id == vm.session_id) {
                                        var itemDate = new Date(scannedAttendee.session.starts_at);
                                        var scannedAt = new Date(scannedAttendee.scanned_at);
                                        var actualDate = $filter('date')(itemDate, "MM/dd/yyyy - hh:mm a", vm.conference.time_zone);
                                        var scannedDate = $filter('date')(scannedAt, "MM/dd/yyyy hh:mm a", vm.conference.time_zone);

                                        mainProfileAttendee[type] = scannedDate;
                                        mainProfileAttendee[type_by] = scannedAttendee.added_by.first_name + ' ' + scannedAttendee.added_by.last_name;
                                        sessionTitles.push(type);
                                        sessionTitles.push(type_by);

                                        if (scanned_again_at != null) {

                                            var nr = 2;

                                            if (scannedAttendee.scannedattendeeadditional_set.length == 0) {
                                                _.each(scannedAttendee.scanned_again_at_array, function(scannedAtAgain){
                                                    var scannedAtAgainTXT = $filter('date')(scannedAtAgain, "MM/dd/yyyy - hh:mm a", vm.conference.time_zone);
                                                    var scannedAtAgainTXTBy =  ' ';
                                                    var fieldName = "Scan " + nr;
                                                    mainProfileAttendee[fieldName] = scannedAtAgainTXT;
                                                    sessionTitles.push(fieldName);
                                                    var fieldName2 = "Scan " + nr + " by";
                                                    mainProfileAttendee[fieldName2] = scannedAtAgainTXTBy;
                                                    sessionTitles.push(fieldName2);
                                                    nr ++;
                                                });
                                            } else {
                                                _.each(scannedAttendee.scannedattendeeadditional_set, function(scannedAtAgain){
                                                    var scannedAtAgainTXT = $filter('date')(scannedAtAgain.scanned_at, "MM/dd/yyyy - hh:mm a", vm.conference.time_zone);
                                                    var scannedAtAgainTXTBy =  scannedAtAgain.added_by.first_name + ' ' +
                                                        scannedAtAgain.added_by.last_name;
                                                    var fieldName = "Scan " + nr;
                                                    mainProfileAttendee[fieldName] = scannedAtAgainTXT;
                                                    sessionTitles.push(fieldName);
                                                    var fieldName2 = "Scan " + nr + " by";
                                                    mainProfileAttendee[fieldName2] = scannedAtAgainTXTBy;
                                                    sessionTitles.push(fieldName2);
                                                    nr ++;
                                                });
                                            }
                                        }
                                    }
                                });
                                delete mainProfileAttendee["id"];
                                mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                                checkedAttendeeList.push(mainProfileAttendee);

                        });
                        var fields = ['company_name', 'first_name', 'last_name', 'job_title', 'email_address', 'phone_number',
                        'street_address', 'city', 'zip_code', 'state', 'country', 'attendee_id', '{BCT#QRCode}'];
                        customFields = _.uniq(customFields);
                        sessionTitles = _.uniq(sessionTitles);
                        fields = fields.concat(customFields);
                        fields = fields.concat(sessionTitles);
                        vm.printCsvListFile(checkedAttendeeList, fields);

                }).catch(function(error){

                    toastr.error('There has been a problem with your request');
                });
            }

            vm.downloadAsCsvImproved = function() {
                var attendee_ids = [];
                if (!vm.checkAllAttendees) {
                        _.each(vm.attendees, function(attendee) {
                                if (attendee.checked == true) {
                                        attendee_ids.push(attendee.id);
                                }
                        });
                }

                var promises = [];
                if (attendee_ids.length == 0) {
                        var nr_pages = parseInt(vm.attendees_total_count / 30) + 2;
                        for (var idx = 1; idx <= nr_pages + 1; idx ++) {
                                promises.push(ConferenceAttendeesService.downloadAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], attendee_ids, null, idx));
                        }
                } else {
                        promises.push(ConferenceAttendeesService.downloadAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], attendee_ids));
                }


                $q.all(promises).then(function(responses){
                        var allAttendees = [];
                        _.each(responses, function(attendees){
                                allAttendees = allAttendees.concat(attendees);
                        });

                        var checkedAttendeeList = [];
                        var customFields = [];
                        var sessionTitles = [];
                        _.each(allAttendees, function(attendee) {
                                var mainProfileAttendee = _.omit(attendee,
                                ['custom_fields' , 'sessionattendee_set', 'scannedattendee_set', 'customattendeefieldvalue_set',
                                'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                                _.forEach(attendee.customattendeefieldvalue_set, function(customField){
                                        mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                        customFields.push(customField.custom_attendee_field.name);
                                });
                                _.forEach(attendee.scannedattendee_set, function(scannedAttendee){
                                        var itemDate = new Date(scannedAttendee.session.starts_at);
                                        var scannedAt = new Date(scannedAttendee.scanned_at);
                                        var actualDate = $filter('date')(itemDate, "MM/dd/yyyy - hh:mm a", vm.conference.time_zone);
                                        var scannedDate = $filter('date')(scannedAt, "MM/dd/yyyy hh:mm a", vm.conference.time_zone);

                                        var session_txt = scannedDate + ' - ' + scannedAttendee.added_by.first_name + ' ' + scannedAttendee.added_by.last_name;

                                        mainProfileAttendee[scannedAttendee.session.title + ': ' + actualDate] = session_txt;
                                        sessionTitles.push(scannedAttendee.session.title + ': ' + actualDate);

                                        mainProfileAttendee[scannedAttendee.session.title + ': ' + 'CE'] = String(scannedAttendee.session.ce_hours);
                                        sessionTitles.push(scannedAttendee.session.title + ': ' + 'CE');
                                });
                                delete mainProfileAttendee["id"];
                                mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                                checkedAttendeeList.push(mainProfileAttendee);

                        });
                        var fields = ['company_name', 'first_name', 'last_name', 'job_title', 'email_address', 'phone_number',
                        'street_address', 'city', 'state', 'zip_code', 'country', 'attendee_id', '{BCT#QRCode}'];
                        customFields = _.uniq(customFields);
                        sessionTitles = _.uniq(sessionTitles);
                        fields = fields.concat(customFields);
                        fields = fields.concat(sessionTitles);
                        vm.printCsvListFile(checkedAttendeeList, fields);

                }).catch(function(error){

                    toastr.error('There has been a problem with your request');
                });
            }

            // Download attendees as csv
            // The attendees are loaded in chuncks from the backend and after all the data is on our web application
            // it will generate the csv file
            vm.downloadAsCsv = function() {
                var checkedAttendeeList = [];
                if (!vm.checkAllAttendees) {
                    _.each(vm.attendees, function(attendee) {
                        if (attendee.checked == true) {
                            var mainProfileAttendee = _.omit(attendee,
                            ['custom_fields' , 'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.custom_fields, function(customField){
                                mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                            });
                            delete mainProfileAttendee["id"];
                            mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                            checkedAttendeeList.push(mainProfileAttendee);
                        }
                    });
                }
                else {
                    var nr_pages = parseInt(vm.attendees_total_count / 30) + 2;
                    var promises = [];
                    for (var idx = 1; idx <= nr_pages + 1; idx ++) {
                        promises.push(ConferenceAttendeesService.getAllAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], idx));
                    }

                    $q.all(promises).then(function(responses){
                        var allAttendees = [];
                        _.each(responses, function(response){
                            allAttendees = allAttendees.concat(response.attendees);

                        });

                        _.each(allAttendees, function(attendee) {
                            var mainProfileAttendee = _.omit(attendee,
                            ['custom_fields' , 'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.custom_fields, function(customField){
                                mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                            });
                            delete mainProfileAttendee["id"];
                            mainProfileAttendee['{BCT#QRCode}'] = vm.getUserQrCode(attendee);
                            checkedAttendeeList.push(mainProfileAttendee);
                        });

                        var csvData =  lib.ConvertToCSV2(checkedAttendeeList);

                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document

                        var date = new Date();

                        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                        var filename = vm.conference.name + actualDate + ".csv";

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
                            target: '_blank',
                            download: filename
                        })[0].click();

                        anchor.remove();
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                        return;
                    });
                    return;
                }
                var csvData =  lib.ConvertToCSV2(checkedAttendeeList);

                var anchor = angular.element('<a/>');
                anchor.css({display: 'none'}); // Make sure it's not visible
                angular.element(document.body).append(anchor); // Attach to document

                var date = new Date();
                var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                var filename = vm.conference.name + actualDate + ".csv";

                anchor.attr({
                    href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
                    target: '_blank',
                    download: filename
                })[0].click();

                anchor.remove();
            }

            vm.moreOptionsForSessionTabel = false;

            // Download session attendees as csv
            vm.downloadSessionPeopleAsCsv = function(){
                var checkedAttendeeList = [];
                var list = [];
                if(vm.sessionRegistrantsView){
                    list = vm.sessionAttendees.sessions_attendees;
                } else if(vm.sessionAttendeesView){
                    list = vm.sessionAttendees.scanned_attendees;
                }
                if ((!vm.checkAllSessionRegistrants && vm.sessionRegistrantsView) || (!vm.checkAllSessionScanned && vm.sessionAttendeesView)) {
                    _.each(list, function(attendee) {
                        if (attendee.attendee.checked == true) {
                            var mainProfileAttendee = _.omit(attendee.attendee,
                            ['custom_fields' , 'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.attendee.custom_fields, function(customField){
                                mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                            });
                            checkedAttendeeList.push(mainProfileAttendee);
                        }
                    });
                } else {
                    var nr_pages = parseInt(vm.sessionAttendees.total_count / 30) + 2;
                    var promises = [];
                    for (var idx = 1; idx <= nr_pages + 1; idx ++) {
                        if(vm.sessionAttendeesView){
                            promises.push(ConferenceSessionsService.listSessionScanned(vm.session_id, idx, false, vm.attendeeFilters));
                        } else if(vm.sessionRegistrantsView){
                            promises.push( ConferenceSessionsService.listSessionAttendees(vm.session_id, idx, false, vm.attendeeFilters));
                        }
                    }

                    $q.all(promises).then(function(responses){
                        var allPeople = [];
                        _.each(responses, function(response){
                            if(vm.sessionAttendeesView){
                                _.each(response.scanned_attendees, function(attendee){
                                    allPeople = allPeople.concat(attendee.attendee);
                                });
                            } else if(vm.sessionRegistrantsView){
                                _.each(response.sessions_attendees, function(attendee){
                                    allPeople = allPeople.concat(attendee.attendee);
                                });
                            }
                        });

                        _.each(allPeople, function(attendee) {
                            var mainProfileAttendee = _.omit(attendee,
                            ['custom_fields' , 'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                            _.forEach(attendee.custom_fields, function(customField){
                                mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                            });
                            checkedAttendeeList.push(mainProfileAttendee);
                        });

                        var csvData =  lib.ConvertToCSV2(checkedAttendeeList);

                        var anchor = angular.element('<a/>');
                        anchor.css({display: 'none'}); // Make sure it's not visible
                        angular.element(document.body).append(anchor); // Attach to document

                        var date = new Date();

                        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                        var filename = vm.sessionDetailsView.title +
                        (vm.sessionAttendeesView ? ' Attendees' : (vm.sessionRegistrantsView ? ' Registrants' : '')) + actualDate + ".csv";

                        anchor.attr({
                            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
                            target: '_blank',
                            download: filename
                        })[0].click();

                        anchor.remove();
                        vm.moreOptionsForSessionTabel = false;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                        return;
                    });
                    return;
                }

                var csvData =  lib.ConvertToCSV2(checkedAttendeeList);
                var anchor = angular.element('<a/>');
                anchor.css({display: 'none'}); // Make sure it's not visible
                angular.element(document.body).append(anchor); // Attach to document

                var date = new Date();
                var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                var filename = vm.sessionDetailsView.title +
                (vm.sessionAttendeesView ? ' Attendees' : (vm.sessionRegistrantsView ? ' Registrants' : '')) + actualDate + ".csv";

                anchor.attr({
                    href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csvData),
                    target: '_blank',
                    download: filename
                })[0].click();

                anchor.remove();
                vm.moreOptionsForSessionTabel = false;
            }

			vm.userPermissions = [
			    'edit_conference', 'edit_conference_users', 'edit_attendee_fields', 'download_attendees',
			    'manage_attendees', 'manage_exhibitors', 'manage_sessions', 'scan_attendees', 'can_refund', 'edit_registration'
			];

			vm.attendeesMoreOptionsCheck = false;
            vm.userOrderAsc = true;
            vm.sortBy = function(data, value, order){
                value = value + '';
                if(order == true){
                    var sortedArr =  _.orderBy(data, value , 'asc');
                } else{
                     var sortedArr =  _.orderBy(data, value , 'desc');
                }
                if(data === vm.exhibitors){
                    vm.exhibitors  = sortedArr;
                } else if(data === vm.conferenceUsers){
                    vm.conferenceUsers = sortedArr;
                }
            };

            vm.lockSortAttendees = false;
            vm.sortOrderAscending = false;

            vm.attendeeFilters = {
                queryString: null,
                sortBy: 'first_name',
                sortOrder: "desc"
            }

            // Sort attendees by field
            vm.sortAttendeesBy = function(sortBy) {
                if (sortBy == 'attendee') {
                    vm.attendeeFilters.sortBy = 'first_name';
                    if (vm.sortOrderAscending) vm.attendeeFilters.sortOrder = 'asc';
                    else vm.attendeeFilters.sortOrder = 'desc';
                } else if(sortBy != 'qr_code'){
                    vm.attendeeFilters.sortBy = sortBy;
                    if (vm.sortOrderAscending) vm.attendeeFilters.sortOrder = 'asc';
                    else vm.attendeeFilters.sortOrder = 'desc';
                }
                if(sortBy != 'qr_code' && vm.attendees_total_count > 30){
                    if (vm.attendeeFilters.sortBy.id) {
                        vm.attendeeFilters.sortBy = "custom_field_" + vm.attendeeFilters.sortBy.id;
                    }
                    ConferenceAttendeesService
                        .getAllAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], 1, false, vm.attendeeFilters)
                        .then(function(attendees) {
                            vm.currentPage = 1;
                            vm.lockSortAttendees = false;
                            if(vm.checkAllAttendees){
                                _.forEach(attendees.attendees, function(attendee){
                                    attendee.checked = true;
                                });
                            }
                                vm.attendees = attendees.attendees;
                                vm.sortOrderAscending = !vm.sortOrderAscending;
                        })
                        .catch(function(error){
                            vm.lockSortAttendees = false;
                        });
                } else{
                    if (!vm.attendeeFilters.sortBy.id) {
                         vm.attendees = _.orderBy(vm.attendees,  [function(item) {
                            if (item[vm.attendeeFilters.sortBy]) {
                                if (_.isString(item[vm.attendeeFilters.sortBy])) {
                                  return item[vm.attendeeFilters.sortBy].toLowerCase();
                                } else {
                                  return item[vm.attendeeFilters.sortBy].toLowerCase();
                                }
                            }
                            return null;
                        }], [vm.attendeeFilters.sortOrder]);
                        vm.sortOrderAscending = !vm.sortOrderAscending;
                    } else {
                        vm.attendees = _.orderBy(vm.attendees,  [function(item) {
                            if (item.custom_fields[vm.attendeeFilters.sortBy.id] && item.custom_fields[vm.attendeeFilters.sortBy.id].value) {
                                if (item.custom_fields[vm.attendeeFilters.sortBy.id].value instanceof Array) {
                                  return item.custom_fields[vm.attendeeFilters.sortBy.id].value.join(',').toLowerCase();
                                } else {
                                  return item.custom_fields[vm.attendeeFilters.sortBy.id].value.toLowerCase();
                                }
                            }
                            return null;
                        }], [vm.attendeeFilters.sortOrder]);
                        vm.sortOrderAscending = !vm.sortOrderAscending;
                    }

                }
            };

            // Search the attendees
            vm.searchAttendees = function() {
                if (vm.attendeeQuery && vm.attendeeQuery.length > 0) {
                    vm.attendeeFilters.queryString = vm.attendeeQuery;
                } else {
                    vm.attendeeFilters.queryString = null;
                }

                ConferenceAttendeesService
                    .getAllAttendees(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], 1, false, vm.attendeeFilters)
                    .then(function(attendees) {
                        vm.currentPage = 1;
                        vm.lockSortAttendees = false;
                        if(vm.checkAllAttendees){
                            _.forEach(attendees.attendees, function(attendee){
                                attendee.checked = true;
                            });
                        }
                            vm.attendees = attendees.attendees;
                            vm.sortOrderAscending = !vm.sortOrderAscending;
                    })
                    .catch(function(error){
                        vm.lockSortAttendees = false;
                    });
            };

            vm.exportTransactions = function() {
                var promises = [];
                var nr_pages = parseInt(vm.registrationStats.total_attendee_registrations / 20);
                for (var idx = 1; idx <= nr_pages + 1; idx ++) {
                    promises.push(ConferenceRegistrationService.getRegistrationTransfers(
                        ConferenceService.hash_conference_id.decode(vm.conference_id)[0], idx));
                }

                vm.loadExportTransfers = true;

                $q.all(promises).then(function(responses){
                    vm.loadExportTransfers = false;
                    var allTransfers = [];
                    _.each(responses, function(transfers){
                        allTransfers = allTransfers.concat(transfers.attendee_registration_data);
                    });

                    var exportTransfers = [];

                    _.each(allTransfers, function(transfer) {
                        var transferData = {};
                        transferData['Transaction'] = $filter('date')(transfer.created_at, "MMM/dd/yyyy", vm.conference.time_zone);
                        transferData['Time'] = $filter('date')(transfer.created_at, "hh:mm a", vm.conference.time_zone).toLowerCase();
                        transferData['Attendee'] = transfer.attendee.first_name + ' ' + transfer.attendee.last_name;
                        transferData['Email'] = transfer.attendee.email_address;
                        transferData['Level'] = transfer.registration_level.name;
                        transferData['Amount'] = transfer.payment.amount / 100 - ((transfer.payment.amount / 10000) * vm.conference.processing_fee);
                        if (!transfer.transfer_date) transfer.transfer_date = new Date('01/01/1970');
                        transferData['Transfer'] = $filter('date')(transfer.transfer_date, "MMM/dd/yyyy", vm.conference.time_zone);
                        transferData['Receipt'] = transfer.receipt_url;
                        exportTransfers.push(transferData);
                    });

                    var date = new Date();
                    var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
                    var filename = vm.conference.name + actualDate + "_export.csv";

                    var csvData =  lib.ConvertToCSVRaw(exportTransfers);
                    lib.printCsvFile(csvData, filename);
                });
            }

            vm.loadMoreTransactions = function() {
                if (vm.loadingMoreTransactions) return;

                var page = vm.registrationStats.attendee_transfers_page + 1;
                vm.loadingMoreTransactions = true;

                var q = vm.transferQuery;
                if (!q || q.length < 2) q = null;

                ConferenceRegistrationService.getRegistrationTransfers(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], page, q)
                    .then(function(response){
                        vm.loadingMoreTransactions = false;
                        _.each(response.attendee_registration_data, function(attendee_registration){
                            if (attendee_registration.transfer_date) {
                                attendee_registration.transfer_date = new Date(attendee_registration.transfer_date);
                            }
                            vm.registrationStats.attendee_registration_data.push(attendee_registration);
                        });
                        vm.registrationStats.attendee_transfers_page = page;
                    }).catch(function(error){
                        vm.loadingMoreTransactions = false;

                    });
            }

            vm.searchTransactions = function() {
                var page = 1;
                var q = vm.transferQuery;
                if (!q || q.length < 2) q = null;
                vm.loadingSearchTransactions = true;

                ConferenceRegistrationService.getRegistrationTransfers(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], page, q)
                    .then(function(response){
                        vm.loadingSearchTransactions = false;
                        _.each(response.attendee_registration_data, function(attendee_registration){
                            if (attendee_registration.transfer_date) {
                                attendee_registration.transfer_date = new Date(attendee_registration.transfer_date);
                            }
                        });

                        vm.registrationStats.attendee_registration_data = response.attendee_registration_data;
                        vm.registrationStats.attendee_transfers_page = page;
                        vm.registrationStats.total_attendee_registrations = response.total_count;
                        vm.registrationStats.searchQuery = q;

                    }).catch(function(error){
                        vm.loadingSearchTransactions = false;

                    });
            }

            vm.queryModel = '';
            // Search the scanned attendees or registered attendees
            vm.searchSession = function(){
                if (vm.queryModel.length > 0) {
                    vm.attendeeFilters.queryString = vm.queryModel;
                } else{
                    vm.attendeeFilters.queryString = '';
                }
                vm.attendeeFilters.sortBy = 'first_name';
                vm.attendeeFilters.sortOrder = 'asc';
                if(vm.sessionRegistrantsView){
                    ConferenceSessionsService
                        .listSessionAttendees(vm.session_id, 1, false, vm.attendeeFilters)
                        .then(function(attendeesFromSession) {
                            if(vm.checkAllSessionRegistrants){
                                _.forEach(attendeesFromSession.sessions_attendees, function(attendeeFromSession){
                                    attendeeFromSession.attendee.checked = true;
                                });
                            }
                            vm.sessionAttendees.sessions_attendees = attendeesFromSession.sessions_attendees;
                            vm.currentSessionAttendeesPage = 1;
                        });
                } else if(vm.sessionAttendeesView){
                    ConferenceSessionsService
                        .listSessionScanned(vm.session_id, 1, false, vm.attendeeFilters)
                        .then(function(scannedFromSession) {
                            if(vm.checkAllSessionScanned){
                                _.forEach(scannedFromSession.scanned_attendees, function(attendeeFromSession){
                                    attendeeFromSession.attendee.checked = true;
                                });
                            }
                            vm.sessionAttendees.scanned_attendees = scannedFromSession.scanned_attendees;
                            vm.currentSessionAttendeesPage = 1;
                        });
                }
            };

            vm.exhibitorFilters = {
                queryString: null,
                sortBy: 'company',
                sortOrder: "asc"
            };

            // Search exhibitor on the backend
            // TODO search on the frontend since all the exhibitors were loaded
            vm.searchExhibitors = function() {
                if (vm.exhibitorQuery && vm.exhibitorQuery.length > 0) {
                    vm.exhibitorFilters.queryString = vm.exhibitorQuery;
                } else {
                    vm.exhibitorFilters.queryString = null;
                }

                ConferenceExhibitorsService.getAllExhibitors(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.currentPage, false, vm.exhibitorFilters)
                    .then(function(exhibitors){
                        vm.currentPage = 1;
                        if(vm.checkAllExhibitors){
                            _.forEach(exhibitors.exhibitors, function(exhibitor){
                                exhibitor.checked = true;
                            });
                        }
                        vm.exhibitors = exhibitors.exhibitors;
                        vm.exhibitor_total_count = exhibitors.total_count;
                        vm.sortOrderAscending = !vm.sortOrderAscending;
                    })
                    .catch(function(error){

                    });
            }

            // Exhibitors Controller
            vm.exhibitorFields = presets.exhibitorFields();
            vm.exhibitorProfileFields = presets.exhibitorProfileFields();
            vm.editModeExhibitorProfileFields = presets.editModeExhibitorProfileFields();

            // Open the exhibitor modal profile
            vm.openExhibitorProfile = function(exhibitor){
                var i, noneInfo = '';
                $scope.exhibitorProfileFields = vm.exhibitorProfileFields;

                for(i = 0; i < vm.exhibitorProfileFields.length; i++){
                    if(!exhibitor.hasOwnProperty(vm.exhibitorProfileFields[i].code)){
                       noneInfo = vm.exhibitorProfileFields[i].code;
                       exhibitor[noneInfo] = 'No info provided';
                       noneInfo = '';
                    }
                }
                exhibitor.promo_code = "No promo code issued";

                $scope.exhibitor = _.clone(exhibitor, true);
                $scope.updateUser = vm.updateUser;
                $scope.updateExhibitorBooth = vm.updateExhibitorBooth;
                ngDialog.open({template: 'app/views/partials/conference/exhibitor_profile_modal.html',
                              className: 'app/stylesheets/_exhibitorProfileModal.scss',
                              scope: $scope
                });
            };

            vm.toggleForPopUpRegistrant = false;
            vm.toggleForPopUpScanned = false;

            // Open a modal to search for attendees to add to the registered list
            vm.openAttendeeSearchBox = function(){
                $scope.session_id = vm.session_id;
                $scope.force_custom_attendee_id = vm.force_custom_attendee_id;
                $scope.sessionAttendees = vm.sessionAttendees;
                $scope.updateAttendeeFromSessionList = vm.updateAttendeeFromSessionList;
                $scope.totalNumberOfRegistrants = vm.totalNumberOfRegistrants;
                $scope.toggleForPopUpRegistrant = vm.toggleForPopUpRegistrant;
                $scope.updateToggleForPopUp = vm.updateToggleForPopUp;
                $scope.openForm = vm.openForm;
                $scope.sessionRegistrantsView = vm.sessionRegistrantsView;
                $scope.sessionAttendeesView = false;
                ngDialog.open({template: 'app/views/partials/conference/attendee_search_box.html',
                               className: 'app/stylesheets/_attendeeSearchBox.scss',
                               controller: "ConferenceSessionsController as vm",
                               scope: $scope
                });
            };

            // Open a modal to search for attendees to add to the scanned list
            vm.openAttendeeSearchBoxScanned = function(){
                $scope.session_id = vm.session_id;
                $scope.sessionAttendees = vm.sessionAttendees;
                $scope.updateScannedFromSessionList = vm.updateScannedFromSessionList;
                $scope.totalNumberOfRegistrants = vm.totalNumberOfRegistrants;
                $scope.openForm = vm.openForm;
                $scope.toggleForPopUpScanned = vm.toggleForPopUpScanned;
                $scope.updateToggleForPopUp = vm.updateToggleForPopUp;
                $scope.sessionAttendeesView = vm.sessionAttendeesView;
                $scope.sessionRegistrantsView = false;
                ngDialog.open({template: 'app/views/partials/conference/attendee_search_box.html',
                               className: 'app/stylesheets/_attendeeSearchBox.scss',
                               controller: "ConferenceSessionsController as vm",
                               scope: $scope
                });
            };
            vm.updateAttendeeFromSessionList = function(attendeesForTabel, nrOfRegistrantsJustAdded){
                _.forEach(attendeesForTabel, function(attendeeAdded){
                    vm.sessionAttendees.sessions_attendees.push(attendeeAdded);
                });
                vm.totalNumberOfRegistrants = nrOfRegistrantsJustAdded;
                vm.sessionAttendees.total_count = vm.totalNumberOfRegistrants;
                _.each(vm.sessions, function(session){
                    if(session.id === vm.session_id){
                        session.nr_registered = vm.totalNumberOfRegistrants;
                        return
                    }
                });
                vm.sessionAttendees.sessions_attendees = _.sortBy(vm.sessionAttendees.sessions_attendees, function(sessionAttendee){
                    return sessionAttendee.attendee.first_name;
                });
            };

            vm.updateScannedFromSessionList = function(scannedForTabel, nrOfScannedJustAdded){
                 _.forEach(scannedForTabel, function(scannedAdded){
                    vm.sessionAttendees.scanned_attendees.push(scannedAdded);
                });
                vm.totalNumberOfRegistrants = nrOfScannedJustAdded;
                vm.sessionAttendees.total_count = vm.totalNumberOfRegistrants;
                _.each(vm.sessions, function(session){
                    if(session.id === vm.session_id){
                        session.nr_scanned = vm.totalNumberOfRegistrants;
                        return
                    }
                });
                vm.sessionAttendees.scanned_attendees = _.sortBy(vm.sessionAttendees.scanned_attendees, function(sessionAttendee){
                    return sessionAttendee.attendee.first_name;
                });
            }

            // Update the attendee from the info modal
            vm.updateAttendee = function(newAttendee, isNew, next) {
                isNew = isNew || false;
                var idx = -1;
                _(vm.attendees).forEach(function(attendee) {
                    idx ++;
                    if (attendee.id == newAttendee.id) {
                        return false;
                    }
                });

                if (isNew) {
                    vm.attendees.push(newAttendee);
                } else {
                    vm.attendees[idx] = newAttendee;
                }
                if(vm.toggleForPopUpScanned === true && next){
                    vm.toggleForPopUpScanned = false;
                    vm.toggleForPopUpRegistrant = false;
                    vm.openAttendeeSearchBoxScanned();
                } else if(vm.toggleForPopUpRegistrant === true && next){
                    vm.toggleForPopUpScanned = false;
                    vm.toggleForPopUpRegistrant = false;
                    vm.openAttendeeSearchBox();
                }
            }

            // TODO test if used
            vm.updatedExhibitor = function(newExhibitor, isNew) {
                isNew = isNew || false;
                var idx = -1;
                _(vm.exhibitors).forEach(function(exhibitor) {
                      idx ++;
                      if (exhibitor.id == newExhibitor.id) {
                        return false;
                      }
                });

                if (isNew) {
                    vm.exhibitors.push(newExhibitor);
                } else {
                    vm.exhibitors[idx] = newExhibitor;
                }
            }

            // Opens the attendee info modal
            // @param attendee - the attendee object
            vm.openAttendeeInfo = function(attendee){
                if(vm.sessionRegistrantsView == true || vm.sessionAttendeesView == true){
                    $scope.notEditable = true;
                } else{
                    $scope.notEditable = false;
                }
                $scope.getAteendeeThatAddedThisOne = vm.getAttendeeById;
                $scope.actualAttendee = attendee;
                $scope.updatedStuff = vm.updateAttendee;
                $scope.infoAttendee = attendee;
                $scope.editPermission = vm.permissionsForManageAttendees();
                $scope.tempFormLabels = vm.customFields;
                $scope.permissionsForRefund = vm.permissionsForRefund;
                $scope.force_custom_attendee_id = vm.force_custom_attendee_id;
                $scope.formLabels = _.uniq(vm.editAttendeeFields.concat(vm.customFields));
                $scope.conference = vm.conference;
                $scope.permissionsForManageSessions = vm.permissionsForManageSessions;
                $scope.openAttendeeRefundPaymentPopup = vm.openAttendeeRefundPaymentPopup;
                ngDialog.open({template: 'app/views/partials/conference/info_session_attendee.html',
                   className: 'app/stylesheets/_infoSessionAttendee.scss',
                   controller: "ConferenceAttendeesController as vm",
                   scope: $scope
                });
            };

            vm.getAttendeeById = function(attendee_id_to_get){
                ConferenceAttendeesService.getAttendeeById(attendee_id_to_get).then(function(response){
                    ngDialog.close();
                    setTimeout(function(){
                        vm.openAttendeeInfo(response);
                    }, 700);
                }, function(error){
                    toastr.error(error.data.detail);
                });
            };

            vm.postRefundProcessing = function(financial_data, attendee) {
                vm.attendeePayments = financial_data;
                ngDialog.close();
                vm.openAttendeeInfo(attendee);
            }

            vm.openAttendeeRefundPaymentPopup = function(){
                $scope.postRefundProcessing = vm.postRefundProcessing;
                setTimeout(function(){
                    ngDialog.open({template: 'app/views/partials/conference/attendee_refund_payment.html',
                        className: 'app/stylesheets/_infoSessionAttendee.scss',
                        controller: "ConferenceAttendeesController as vm",
                        scope : $scope
                    })
                }, 500);
            };

            vm.canSaveSupportContacts = function(supportContact) {
                if (!supportContact.first_name || !supportContact.last_name || !supportContact.company_name || !supportContact.area_of_support) {
                    return false;
                }
                return true;
            }

            vm.openSupportContactForm = function(){
                $scope.supportContacts = vm.supportContacts;
                $scope.createNewSupportContact = vm.createNewSupportContact;
                $scope.canSaveSupportContacts = vm.canSaveSupportContacts;
                $scope.uploadSupportContactPhoto = vm.uploadSupportContactPhoto;
                ngDialog.open({template: 'app/views/partials/conference/exhibitor_support_contacts_modal.html',
                  className: 'app/stylesheets/_exhibitorSuportContactsModal.scss',
                  controller: 'ConferenceExhibitorsController as vm',
                  scope: $scope
                });
            }

            vm.openCESupportContactForm = function(){
                $scope.supportContacts = vm.conferenceexhibitorSupportContacts;
                $scope.createNewSupportContact = vm.createNewCESupportContact;
                $scope.canSaveSupportContacts = vm.canSaveSupportContacts;
                $scope.uploadSupportContactPhoto = vm.uploadSupportContactPhoto;
                ngDialog.open({template: 'app/views/partials/conference/exhibitor_support_contacts_modal.html',
                  className: 'app/stylesheets/_exhibitorSuportContactsModal.scss',
                  controller: 'ConferenceExhibitorsController as vm',
                  scope: $scope
                });
            }

            vm.openSupportContactProfile = function(){
                $scope.supportContacts = vm.supportContacts;
                $scope.currentContact = vm.currentContact;
                $scope.updateSupportContact = vm.updateSupportContact;
                $scope.uploadSupportContactPhoto = vm.uploadSupportContactPhoto;
                $scope.canSaveSupportContacts = vm.canSaveSupportContacts;
                ngDialog.open({template: 'app/views/partials/conference/exhibitor_support_contacts_modal_profile.html',
                              className: 'app/stylesheets/_exhibitorSuportContactsModalProfile.scss',
                              controller: 'ConferenceExhibitorsController as vm',
                              scope: $scope
                });
            }

            vm.openCESupportContactProfile = function(){
                $scope.supportContacts = vm.conferenceexhibitorSupportContacts;
                $scope.currentContact = vm.currentContact;
                $scope.updateSupportContact = vm.updateCESupportContact;
                $scope.uploadSupportContactPhoto = vm.uploadSupportContactPhoto;
                $scope.canSaveSupportContacts = vm.canSaveSupportContacts;
                ngDialog.open({template: 'app/views/partials/conference/exhibitor_support_contacts_modal_profile.html',
                              className: 'app/stylesheets/_exhibitorSuportContactsModalProfile.scss',
                              controller: 'ConferenceExhibitorsController as vm',
                              scope: $scope
                });
            }

            $rootScope.$on('CurrentContact', function(CurrentContact, contact){
                vm.currentContact = _.omit(contact, ['id']);
                vm.currentContact.conference_support_contact_id = contact.id;
            });

            vm.openMapMemberInformation = function(){
                ngDialog.open({template: 'app/views/partials/conference/map_members_information.html',
                    className: 'app/stylesheets/_mapMembersInformation.scss',
                    scope: $scope
                });
            }

            vm.updateToggleForPopUp = function(boolean_t, boolean_f){
                if(vm.sessionAttendeesView){
                    vm.toggleForPopUpScanned = boolean_t;
                    vm.toggleForPopUpRegistrant = boolean_f;
                } else{
                    vm.toggleForPopUpScanned = boolean_f;
                    vm.toggleForPopUpRegistrant = boolean_t;
                }
            };

            vm.openForm = function(){
                $scope.formLabels = vm.formLabels.concat(vm.savedCustomFields);
                $scope.updatedStuff = vm.updateAttendee;
                $scope.conference = vm.conference;
                $scope.openForm = vm.openForm;
                ngDialog.open({template: 'app/views/partials/add_attendee.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceAttendeesController as vm",
                      scope: $scope
                });
            };

            vm.openThisDialog = function(){
                vm.openForm();
            };

            vm.overBottomScroller = function() {
                vm.scrollBottom();
                vm.bottomScroll = true;
            }

            // Used to force scroll functionality when the drag-drop functionality is active in the attendee csv mapping view

            vm.overBottomScrollerOver = function() {
                vm.bottomScroll = false;
            }


            vm.overTopScroller = function() {
                vm.scrollTop();
                vm.topScroll = true;
            }

            vm.overTopScrollerOver = function() {
                vm.topScroll = false;
            }

            vm.scrollBottom = function() {
                var scrollBottom = $("#right_scrollable").scrollTop();
                $("#right_scrollable").animate({
                  scrollTop: scrollBottom + 171
                }, {
                    duration: 200,
                    complete: function() {
                        if (vm.bottomScroll) vm.scrollBottom();
                    }
                });
            }

            vm.scrollTop = function() {
                var scrollBottom = $("#right_scrollable").scrollTop();
                $("#right_scrollable").animate({
                  scrollTop: scrollBottom - 171
                }, {
                   duration: 100,
                   complete: function() {
                       if (vm.topScroll) vm.scrollTop();
                   }
                });
            }

            vm.openAddExhibitorPopup = function(){
                $scope.addConferenceExhibitor = vm.addConferenceExhibitor;
                ngDialog.open({template: 'app/views/partials/conference/add_conference_exhibitor.html',
                  className: 'app/stylesheets/_plusExhibitor.scss',
                  controller: "ConferenceExhibitorsController as vm",
                  scope: $scope
                });
            };

            vm.openPaymentForm = function(){
                if (vm.popup) {
                    vm.popup.close();
                }
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.conference = vm.conference;
                $scope.user = vm.user;
                $scope.changeCreditCard = vm.changeCreditCard;
                $scope.updatePromoCodes = vm.updatePromoCodes;
                $scope.changeConferencePayment = false;
                $scope.updateUser = vm.updateUser;

                vm.popup = ngDialog.open({template: 'app/views/partials/conference/exhibitor_payment_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            };

            // A callback for the openConferenceUsersPurchaseForm modal to update the view
            vm.updateUsersSlotsCount = function(nrUsers) {
                vm.conference.nr_user_slots += nrUsers;
            }

            // Opens the purchase conference users modal
            vm.openConferenceUsersPurchaseForm = function(){
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.conference = vm.conference;
                $scope.user = vm.user;
                $scope.changeCreditCard = vm.changeCreditCard;
                $scope.updatePromoCodes = vm.updatePromoCodes;
                $scope.changeConferencePayment = false;
                $scope.updateUser = vm.updateUser;
                $scope.updateUsersSlotsCount = vm.updateUsersSlotsCount;

                vm.popup = ngDialog.open({template: 'app/views/partials/conference/conference_user_payment_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            };

            vm.updatePromoCodes = function(promo_codes) {
                vm.promo_codes  = promo_codes;
                vm.paymentCodes = null;
            }

            // Opens the create promo codes modal
            vm.openPromoCodePopup = function() {
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.conference = vm.conference;
                $scope.promo_codes = vm.promo_codes;
                $scope.checkOne = vm.checkOne;
                $scope.checkAll = vm.checkAll;
                $scope.downloadTransactionsAsCsv = vm.downloadTransactionsAsCsv;
                $scope.printTransactionsAsCsv = lib.printTransactionsAsCsv;
                $scope.updateUser = vm.updateUser;

                ngDialog.open({template: 'app/views/partials/conference/promo_codes_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            }

            vm.openConferencePaymentForm = function(changeConferencePayment){
                changeConferencePayment = changeConferencePayment || false;
                $scope.formLabels = vm.formLabels;
                $scope.updatedExhibitor = vm.updatedExhibitor;
                $scope.paymentCodes = vm.paymentCodes;
                $scope.updateUser = vm.updateUser;
                $scope.user = vm.user;
                $scope.updatePaymentMethodAfterRegisteringStripe = vm.updatePaymentMethodAfterRegisteringStripe;
                $scope.changeConferencePayment = changeConferencePayment;
                $scope.updateUser = vm.updateUser;

                ngDialog.open({template: 'app/views/partials/conference/conference_payment_form.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            };

            vm.tempInfo = [];
            vm.editAttendeeInfo = [];
            vm.sessionAttendees = [];

            vm.sessionRegistrantsView = false;
            vm.sessionAttendeesView = false;
            vm.conferenceAttendees = [];
            vm.switchArrowA = true;
            vm.switchArrowR = true;

            vm.sessionTypeOptionsSimple = presets.sessionTypeSimple();
            vm.sessionTypeOptions = presets.sessionTypeOptions();

            vm.getSessionTypeName = function(session_type) {
                if (!session_type) return null;
                var item = _.find(vm.sessionTypeOptions, {'id': session_type});
                if (!item) {
                    item = _.find(vm.sessionTypeOptions, {'id': session_type.id});
                }
                return item;
            }

            vm.getSessionTypeNameById = function(session_type) {
                if (!session_type) return null;
                var item = _.find(vm.sessionTypeOptions, {'id': session_type});
                if (!item) {
                    item = _.find(vm.sessionTypeOptions, {'id': session_type.id});
                }
                return item;
            }

            if (vm.session_id) {
                vm.sessionDetailsView = _.find(vm.sessions, {id: vm.session_id});
                vm.sessionDetailsView.session_type = vm.getSessionTypeName(vm.sessionDetailsView.session_type);
            }

            vm.registrantFields = presets.registrantFields();

            vm.getAttendeeFieldsCode = function() {
                var ids = [];
                _.each(vm.attendeeFields, function(attendeeField) {
                        ids.push(attendeeField.id);
                });
                return ids;
            }

            ConferenceAttendeesService.getCreateAttendeeFields()
                .then(function(attendeePostFields) {
                    vm.formLabels = attendeePostFields;
                });

            vm.changeConferenceLogo = function(files) {
                $scope.conferenceLogo = files[0];
                vm.conference.logo = files[0].$ngfBlobUrl;
            }

            vm.uploadedAveryTemplates = [];
            vm.attendeeFieldMapping = {};
            vm.attendeeFieldMappingCode = {};
            vm.attendeeFieldMappingObj = {};
            // On hover function for the attendee fields csv mapping screen
            vm.attendeeFieldMappingOver = function(field, field2, field3) {
                vm.topScroll = false;
                vm.bottomScroll = false;

                vm.attendeeFieldMapping[field3.id] = field2.draggable[0].innerText;

                vm.attendeeFieldMappingCode[field2.draggable[0].innerText] = field3.code;
                if (vm.attendeeFieldMappingCode[field2.draggable[0].innerText] == undefined) {
                    vm.attendeeFieldMappingCode[field2.draggable[0].innerText] = field3.name;
                }

                vm.attendeeFieldMappingObj[field3.id] = field2;
                field3.mapping = field2.draggable[0].innerText;

                _.each(vm.membersCsvKeys, function(csvField) {
                    if (csvField.code == field2.draggable[0].innerText) {
                        csvField.mapping = field3.id;
                    }
                });

                _.each(vm.attendeesCsvKeys, function(csvField) {
                    if (csvField.code == field2.draggable[0].innerText) {
                        csvField.mapping = field3.id;
                    }
                });
            };
            vm.speakerNotAlreadyInSession = function(conference_speaker) {
                var existingItem = _.find(vm.newSessionItem.sessionspeaker_set, {"id": conference_speaker.id})
                if (existingItem) return false;
                return true;
            }

            vm.speakerDropIn = function(field, field2) {
                if (!vm.newSessionItem.sessionspeaker_set) {
                    vm.newSessionItem.sessionspeaker_set = [];
                }
                var newItem = JSON.parse(field2.draggable.attr('data-content'));
                var existingItem = _.find(vm.newSessionItem.sessionspeaker_set, {"id": newItem.id})
                if (!existingItem) {
                    vm.newSessionItem.sessionspeaker_set.push(newItem);
                }

                vm.currentSessionSpeakersList = _.differenceBy(vm.currentSessionSpeakersList, vm.newSessionItem.sessionspeaker_set, 'id');
                vm.scrollBottomSpeakers();
            };

            vm.speakerDropOut = function(field, field2) {
                var fieldJSON = JSON.parse(field2.draggable.attr('data-content'));
                if (!vm.newSessionItem.sessionspeaker_set) {
                    vm.newSessionItem.sessionspeaker_set = [];
                }
                vm.newSessionItem.sessionspeaker_set = _.reject(vm.newSessionItem.sessionspeaker_set, ['id', fieldJSON.id ]);

                vm.newSessionItem.sessionspeaker_set = _.differenceBy(vm.newSessionItem.sessionspeaker_set, vm.currentSessionSpeakersList, 'id');

                var existingItem = _.find(vm.currentSessionSpeakersList, {"id": fieldJSON.id})
                if (!existingItem) {
                    if(vm.newSessionItem.is_moderator == fieldJSON.id){
                        vm.newSessionItem.is_moderator = null;
                    }
                    vm.currentSessionSpeakersList.push(fieldJSON);
                }
            };

            vm.sessionFieldMappingOver = function(field, field2, field3) {
                vm.topScroll = false;
                vm.bottomScroll = false;
                vm.sessionFieldMapping[field3.id] = field2.draggable[0].innerText;

                vm.sessionFieldMappingCode[field2.draggable[0].innerText] = field3.code;
                if (vm.sessionFieldMappingCode[field2.draggable[0].innerText] == undefined) {
                    vm.sessionFieldMappingCode[field2.draggable[0].innerText] = field3.name;
                }
                vm.sessionFieldMappingObj[field3.id] = field2;
                field3.mapping = field2.draggable[0].innerText;

                _.each(vm.sessionsCsvKeys, function(csvField) {
                    if (csvField.code == field2.draggable[0].innerText) {
                        csvField.mapping = field3.id;
                    }
                });
            };

            vm.exhibitorFieldMappingOver = function(field, field2, field3) {
                vm.topScroll = false;
                vm.bottomScroll = false;
                vm.exhibitorFieldMapping[field3.id] = field2.draggable[0].innerText;

                vm.exhibitorFieldMappingCode[field2.draggable[0].innerText] = field3.code;
                if (vm.exhibitorFieldMappingCode[field2.draggable[0].innerText] == undefined) {
                    vm.exhibitorFieldMappingCode[field2.draggable[0].innerText] = field3.name;
                }

                vm.exhibitorFieldMappingObj[field3.id] = field2;
                field3.mapping = field2.draggable[0].innerText;

                _.each(vm.exhibitorsCsvKeys, function(csvField) {
                    if (csvField.code == field2.draggable[0].innerText) {
                        csvField.mapping = field3.id;
                    }
                });
            }

            vm.attendeeFieldMappingHoverIn = function(field, field2, field3) {
                $scope.$apply();
            }

            vm.attendeeFieldMappingHoverOut = function(field, field2, field3) {
                $scope.$apply();
            }

            vm.startCallback = function(field, field2, field3, div) {
                field3.mapping = null;
                _.each(vm.customFields, function(customField) {
                    if (field3.code == customField.mapping) {
                        customField.mapping = null;
                        $scope.$apply();
                    }
                });
            }

            vm.stopCallback = function(field) {
                field.target.style.cursor = '-webkit-grab';
            }

            vm.changeDragStyle = function(element){
                // element.target.style.cursor = 'move';
                // element.target.style.display = 'flex';
                // element.target.style.alignItems = 'center';
                // if (element.target.children[1]) {
                //     element.target.children[1].style.display = 'flex';
                //     element.target.children[1].style.alignItems = 'center';
                //     element.target.children[1].style.flexDirection = 'column';
                // }
                // element.target.style.opacity = 0;
            }

            vm.changeDragStyleStop = function(element) {
                //
                // setTimeout(function () {
                //   element.target.style.opacity = 1;
                // }, 1);
            }

            vm.changeDragStyle2 = function(element){
                // element.target.style.cursor = 'move';
                // element.target.style.display = 'flex';
                // element.target.style.alignItems = 'center';
                // element.target.children[1].style.display = 'flex';
                // element.target.children[1].style.alignItems = 'center';
                // element.target.children[1].style.flexDirection = 'column';
                // element.target.style.display = 'none';
            }

            vm.changeDragStyle2Stop = function(element) {
                setTimeout(function () {
                  element.target.style.display = 'flex';
                }, 100);
            }

            vm.copyCodeToClipboard = function() {
                var doc = document
                    , text = doc.getElementById('registration_code')
                    , range, selection
                ;

                if (doc.body.createTextRange) {
                    range = document.body.createTextRange();
                    range.moveToElementText(text);
                    range.select();
                } else if (window.getSelection) {
                    selection = window.getSelection();
                    range = document.createRange();
                    range.selectNodeContents(text);
                    selection.removeAllRanges();
                    selection.addRange(range);
                }
                document.execCommand('copy');
                if ( document.selection ) {
                    document.selection.empty();
                } else if ( window.getSelection ) {
                    window.getSelection().removeAllRanges();
                }

                toastr.success("Code copied to clipboard!");
            }

            vm.changeDragStyleSessionSpeakerList = function(element){
                element.target.style.cursor = 'move';
                element.target.style.display = 'flex';
            }

            vm.customFieldOptions = [];

            vm.newOption = null;

            vm.newCustomField = {
                options: [],
                type: 1
            }

            vm.customFieldAddOption = function() {
                if (vm.newOption == null || vm.newOption == '') {
                    return
                }
                var fields = vm.newCustomField.options;
                fields.push(
                    {
                        value: vm.newOption
                    }
                );
                fields = Array.from(new Set(fields));

                vm.customFieldOptions = fields;
                vm.newOption = null;
            }

            vm.customFieldRmvOption = function(option) {

                var fields = vm.newCustomField.options;
                var index = fields.indexOf(option);

                if (index > -1) {
                    fields.splice(index, 1);
                }

                vm.customFieldOptions = fields;
                vm.newOption = null;
            }

            vm.customFieldSelected = 'Text';
            vm.customFieldChose = function(fieldType) {
                if (fieldType == 'Text') {
                    vm.newCustomField.type = 1;
                } else if (fieldType == 'Multiple Choice') {
                    vm.newCustomField.type = 3;
                }
                else if (fieldType == 'Single Choice') {
                    vm.newCustomField.type = 2;
                }
                vm.customFieldSelected = fieldType;
                vm.customFieldsTypePane = false;
            }

            vm.exhibitingDetails = [];
            vm.exhibitingDays = {};
            vm.exhibitingInfo = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                room: null,
                description: null
            };
            vm.moveDetails = false;
            vm.moveInfo = [];
            vm.moveDays = {};

            vm.moveInDetails = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                room: null,
                description: null
            };
            vm.showMoveIn = false;
            vm.showMoveOut = false;
            vm.moveOutDetails = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                room: null,
                description: null
            };
            vm.currentExhibitingDay = vm.conferenceDays[0].getDate() + '/' + vm.conferenceDays[0].getMonth() + '/' + vm.conferenceDays[0].getFullYear();
            vm.exhibitingDays[vm.currentExhibitingDay] = [];
            vm.exhibitingMoveDays = {};

            // Updates an existing hour's time
            vm.updateHourTime = function(room) {
                var foundItem = null;

                var starts_at_editable_date = new Date(room.starts_at_editable);
                room.starts_at_editable_end = moment(room.starts_at_editable).endOf('day');

                $timeout(function() {
                    if (room.ends_at_editable <= room.starts_at_editable) {
                        room.ends_at_editable = moment(room.starts_at_editable).add(1, 'hour');
                    }
                }, 100);

                if (room.id) {
                    var index = _.findIndex(vm.conferenceHours, _.pick(room, 'id'));
                    if( index !== -1) {
                        vm.conferenceHours.splice(index, 1, room);
                    }
                } else {
                    var index = vm.conferenceHours.indexOf(room);
                    if (index > -1) {
                        vm.conferenceHours[index] = room;
                    }
                }
                var index = vm.conferenceHours.indexOf(room);

                if (index == -1) {
                    vm.conferenceHours.push(room);
                }
            }

            // Adds a move in hour to the dataset
            vm.addExhibitingMoveDetails = function(){
                if (false && (!vm.moveInDetails || !vm.moveInDetails.starts_at_date || !vm.moveInDetails.location)) {

                } else {
                    vm.moveInDetails.type = 2;
                    vm.moveInDetails.title = "move in hour";

                    vm.conferenceHours.push(vm.moveInDetails);
                    vm.moveInHours.push(vm.moveInDetails);

                    vm.moveInDetails = {
                        starts_at: vm.defaultHourStart,
                        ends_at: vm.defaultHourEnd,
                        starts_at_editable: moment(vm.defaultHourStart),
                        ends_at_editable: moment(vm.defaultHourEnd),
                        room: null,
                        description: null
                    };

                    vm.showMoveIn = false;
                }
            }

            // Adds a move out hour to the dataset
            vm.addExhibitingMoveOutDetails = function() {
                if (false && (!vm.moveOutDetails || !vm.moveOutDetails.starts_at_date || !vm.moveOutDetails.location)) {

                } else {
                    vm.moveOutDetails.type = 3;
                    vm.moveOutDetails.title = "move in hour";

                    vm.conferenceHours.push(vm.moveOutDetails);
                    vm.moveOutHours.push(vm.moveOutDetails);

                    vm.moveOutDetails = {
                        starts_at: vm.defaultHourStart,
                        ends_at: vm.defaultHourEnd,
                        starts_at_editable: moment(vm.defaultHourStart),
                        ends_at_editable: moment(vm.defaultHourEnd),
                        room: null,
                        description: null
                    };

                    vm.showMoveOut = false;
                }
            }

            vm.addExhibitingInfoPrev = function() {
                if (!vm.exhibitingInfo || !vm.exhibitingInfo.starts_at || !vm.exhibitingInfo.ends_at
                    || !vm.exhibitingInfo.location) {
                    vm.exhibitingInfo.complete = false;
                } else {
                    vm.exhibitingInfo.complete = true;
                }
            }

            vm.uploadAttendeesToSession = function() {
                $location.path("/conference/" + vm.conference_id + '/session-upload/' + ConferenceSessionsService.hash_session_id.encode(vm.session_id));
            }

            // Adds an exhibiting hour to the dataset
            vm.addExhibitingInfo = function(){
                var ok = true;

                if (false && (!vm.exhibitingInfo || !vm.exhibitingInfo.starts_at || !vm.exhibitingInfo.ends_at
                    || !vm.exhibitingInfo.location)) {

                } else {
                    vm.exhibitingDetails.push(vm.exhibitingInfo);
                    vm.exhibitingDays[vm.currentExhibitingDay] = _.concat(vm.exhibitingDays[vm.currentExhibitingDay], vm.exhibitingDetails);

                    var currentDay = vm.currentExhibitingDay.split("/")[0];
                    var currentMonth = vm.currentExhibitingDay.split("/")[1];
                    var currentYear = vm.currentExhibitingDay.split("/")[2];

                    var hour_start = vm.exhibitingInfo.starts_at.getHours();
                    var minute_start = vm.exhibitingInfo.starts_at.getMinutes();

                    var hour_end = vm.exhibitingInfo.ends_at.getHours();
                    var minute_end = vm.exhibitingInfo.ends_at.getMinutes();

                    var diff = Math.abs(vm.exhibitingInfo.ends_at - vm.exhibitingInfo.starts_at)/1000;

                    vm.exhibitingInfo.starts_at = new Date(currentYear, currentMonth, currentDay, hour_start, minute_start, 0);
                    vm.exhibitingInfo.ends_at = _.cloneDeep(vm.exhibitingInfo.starts_at);
                    vm.exhibitingInfo.ends_at.setSeconds(vm.exhibitingInfo.starts_at.getSeconds() + diff);
                    vm.exhibitingInfo.starts_at_editable_end = vm.conference.date_to_editable;
                    var date = new Date(vm.sessionSelectedDay);

                    vm.exhibitingInfo.exhibiting_day = moment(date).set({ hour: 0, minute: 0 });
                    vm.conferenceHours.push(vm.exhibitingInfo);

                    vm.exhibitingInfo = {
                        starts_at: vm.defaultHourStart,
                        exhibiting_day: moment(date).set({ hour: 0, minute: 0 }),
                        ends_at: vm.defaultHourEnd,
                        starts_at_editable_end: vm.conference.date_to_editable,
                        room: null,
                        description: null
                    };

                    vm.exhibitingDetails = [];
                    vm.activeMinus = true;
                    $scope.exhibitingHourSaved = true;
                }
            }

            vm.addExhibitingMoveOut = function() {

            }

            $rootScope.$on('CurrentExhibitingDay', function(CurrentExhibitingDay, day){
                var currentDate = day;
                var day = currentDate.getDate();
                var month = currentDate.getMonth();
                var year = currentDate.getFullYear();
                vm.currentExhibitingDay = day + '/' + month + '/' + year;
                vm.exhibitingDetails = [];
                if( vm.exhibitingDays.hasOwnProperty(vm.currentExhibitingDay) === false ){
                    vm.exhibitingDays[vm.currentExhibitingDay] = [];
                }
            });

            $scope.exhibitingHourSaved = false;

            vm.addExhibitingMoveInButton = true;
            vm.addExhibitingMoveOutButton = true;

            vm.lockSetExhibitingDay = false;

            // When clicking on the save button on the exhibiting info view - it will update the exhibiting hours on the backend
            vm.setExhibitingDay = function(){
                if (vm.exhibitingInfo.location && vm.exhibitingInfo.location != '') {
                    vm.addExhibitingInfo();
                    vm.addExhibitingHoursButton = false;
                }
                if (vm.moveInDetails && vm.moveInDetails.location && vm.moveInDetails.starts_at_date) {
                    vm.addExhibitingMoveDetails();
                    vm.addExhibitingMoveInButton = false;
                }
                if (vm.moveOutDetails && vm.moveOutDetails.location && vm.moveOutDetails.starts_at_date) {
                    vm.addExhibitingMoveOutDetails();
                    vm.addExhibitingMoveOutButton = false;
                }
                vm.lockSetExhibitingDay = true;
                var conferenceHours = []
                _.each(vm.conferenceHours, function(conferenceHour) {
                    if (!conferenceHour || !conferenceHour.starts_at_editable || !conferenceHour.ends_at_editable || !conferenceHour.location) {
                    } else {
                        var starts_at = new Date(conferenceHour.starts_at_editable);
                        var ends_at = new Date(conferenceHour.ends_at_editable);

                        conferenceHour.starts_at_raw = {
                            "year": starts_at.getFullYear(),
                            "month": starts_at.getMonth() + 1,
                            "day": starts_at.getDate(),
                            "hour": starts_at.getHours(),
                            "minute": starts_at.getMinutes(),
                        }
                        conferenceHour.ends_at_raw = {
                            "year": ends_at.getFullYear(),
                            "month": ends_at.getMonth() + 1,
                            "day": ends_at.getDate(),
                            "hour": ends_at.getHours(),
                            "minute": ends_at.getMinutes(),
                        }
                        conferenceHours.push(conferenceHour);
                    }
                });

                ConferenceExhibitorsService.setConferenceHours(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], conferenceHours)
                    .then(function(response){
                        vm.addExhibitingMoveInButton = true;
                        vm.addExhibitingMoveOutButton = true;
                        SharedProperties.conference = null;
                        $state.reload();

                    })
                    .catch(function(error){
                        vm.lockSetExhibitingDay = false;
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            $scope.$watch('exhibitingHourSaved', function(){
                if($scope.exhibitingHourSaved === true){
                    $scope.exhibitingHourSaved = false;
                }
            });

            // When deleting exhibiting hours this will remove them from the payload to be sent to the backend
            vm.removeExhibitingInfo = function(info){
                if (info.type == 1 || !info.type) {
                    var details = vm.exhibitingDays[vm.currentExhibitingDay];
                    var index = details.indexOf(info);
                    if(index > -1){
                        details.splice(index, 1);
                    }
                    vm.exhibitingDays[vm.currentExhibitingDay] = details;
                    if (vm.exhibitingDays[vm.currentExhibitingDay].length == 0) {
                        vm.showExhibitingDay[vm.currentExhibitingDay] = true;
                    }

                    if (vm.exhibitingDays[vm.currentExhibitingDay].length <= 0) {
                        vm.exhibitingDays[vm.currentExhibitingDay].push({
                            starts_at: vm.defaultHourStart,
                            ends_at: vm.defaultHourEnd,
                            room: null,
                            description: null
                        });
                    }
                } else if (info.type == 2) {
                    var index = vm.moveInHours.indexOf(info);
                    if(index > -1){
                        vm.moveInHours.splice(index, 1);
                    }

                    if (vm.moveInHours.length == 0) {
                        vm.showMoveIn = true;
                    }

                    if (vm.moveInHours.length <= 0) {
                        vm.moveInHours.push({
                            starts_at: vm.defaultHourStart,
                            ends_at: vm.defaultHourEnd,
                            room: null,
                            description: null
                        });
                    }
                } else if (info.type == 3) {
                    var index = vm.moveOutHours.indexOf(info);
                    if(index > -1){
                        vm.moveOutHours.splice(index, 1);
                    }

                    if (vm.moveOutHours.length == 0) {
                        vm.showMoveOut = true;
                    }

                    if (vm.moveOutHours.length <= 0) {
                        vm.moveOutHours.push({
                            starts_at: vm.defaultHourStart,
                            ends_at: vm.defaultHourEnd,
                            room: null,
                            description: null
                        });
                    }
                }

                _.remove(vm.conferenceHours, function (conferenceHour) {
                  return conferenceHour.id === info.id;
                });

                var index = vm.conferenceHours.indexOf(info);
                if(index > -1){
                    vm.conferenceHours.splice(index, 1);
                }
            }

            // On cancel the updates that were not saved are reset
            vm.cancelSaveExhibitingDays = function() {
                vm.conferenceHours = _.cloneDeep(vm.conference.hours);
                vm.exhibitingDays = {};
                vm.moveInHours = [];
                vm.moveOutHours = [];

                _.each(vm.conferenceHours, function(conferenceHour) {
                    var startsAt = _.clone(new Date(conferenceHour.starts_at));
                    var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                    startsAt.setTime( startsAt.getTime() + (startsAt.getTimezoneOffset() + offsetInMinutes)*60*1000 );

                    var day = startsAt.getDate();
                    var month = startsAt.getMonth();
                    var year = startsAt.getFullYear();
                    if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
                        vm.exhibitingDays[day + '/' + month + '/' + year] = [];
                    }
                    conferenceHour.starts_at = new Date(conferenceHour.starts_at);
                    conferenceHour.ends_at = new Date(conferenceHour.ends_at);
                    conferenceHour.starts_at_editable = moment(conferenceHour.starts_at);
                    conferenceHour.ends_at_editable = moment(conferenceHour.ends_at);

                    if (conferenceHour.type == 1) {
                        vm.exhibitingDays[day + '/' + month + '/' + year].push(conferenceHour);
                    }
                    else if (conferenceHour.type == 2) {
                        vm.moveInHours.push(conferenceHour);
                    }
                    else if (conferenceHour.type == 3) {
                        vm.moveOutHours.push(conferenceHour);
                    }
                });
            }

            // -----------------------

			vm.usersPermissions = function() {
			    vm.usersPermissionsPane = !vm.usersPermissionsPane;
                if(vm.usersPermissionsPane){
                    _.each(vm.conferenceUsers, function(user) {
                        if (user.checked == true) {
                            vm.userPermissions = user.rights;
                        }
                    });
                }
			};

			vm.attendeesViewOptions = function() {
			    vm.attendeesViewOptionsPane = !vm.attendeesViewOptionsPane;
			};

			vm.invitedUser = {
			    rights: vm.userPermissions,
			    invited_user_email: null
			}

            // Updates a user permission set
			vm.changePermission = function(option){
                var currentPermissions = vm.userPermissions;
                var index = currentPermissions.indexOf(option);
                if (index > -1) {
                    currentPermissions.splice(index, 1);
                    vm.updateRightsPerUser(currentPermissions);
                }
                else {
                    currentPermissions.push(option);
                    vm.updateRightsPerUser(currentPermissions);
                }

                currentPermissions = Array.from(new Set(currentPermissions));
                if(vm.verifySingleUserChecked()){
                    _.each(vm.conferenceUsers, function(user) {
                        if (user.checked == true) {
                            currentPermissions = user.rights;
                        }
                    });
                    vm.userPermissions = currentPermissions;
                }
                vm.invitedUser.rights = vm.userPermissions;
            };

            vm.attendeeViews = [
                1, 2, 3
            ]

            vm.verifySingleUserChecked = function(){
                var nr = 0;
                 _.each(vm.conferenceUsers, function(user) {
                    if (user.checked == true) {
                        nr++;
                    }
                });
                 if(nr == 1){
                    return true;
                 } else
                    return false;
            };

            vm.checkView = function(option){
                var foundField = null;
                _.each(vm.attendeeFields, function(attendeeField) {
                    if (attendeeField.id == option.id) {
                        foundField = attendeeField;
                    }
                });

                var firstItem = vm.attendeeFields[0];
                var lastItem = vm.attendeeFields[vm.attendeeFields.length - 1];

                vm.attendeeFields.splice(0, 1);
                vm.attendeeFields.splice(vm.attendeeFields.length - 1, 1);

                if (foundField) {
                    var index = vm.attendeeFields.indexOf(foundField);
                    if (index > -1) {
                        vm.attendeeFields.splice(index, 1);
                    }
                }
                else {
                    vm.attendeeFields.push(option);
                }
                vm.attendeeFields = _.chain(vm.attendeeFields).omit([firstItem, lastItem]).sortBy('name').value();

                vm.attendeeFields.unshift(firstItem);
                vm.attendeeFields.push(lastItem);
                var currentFields = vm.attendeeFields;
                localStorageService.set(attendeeFieldKey, vm.attendeeFields);
            };

            vm.testIfExhibitorChecked = function() {
                var checkedUsersList = [];

                _.each(vm.exhibitors, function(exhibitor) {
                    if (exhibitor.checked == true) {
                        checkedUsersList.push(exhibitor);
                    }
                });


                if (checkedUsersList.length > 0) {
                    vm.exhibitorChecked = true;
                }
                else {
                    vm.exhibitorChecked = false;
                }
            }

            vm.testIfUserChecked = function() {
                var checkedUsersList = [];

                _.each(vm.conferenceUsers, function(user) {
                    if (user.checked == true) {
                        checkedUsersList.push(user);
                    }
                });
                if (checkedUsersList.length > 0) {
                    vm.userChecked = true;
                }
                else {
                    vm.userChecked = false;
                }
            }

            vm.testIfAttendeeChecked = function() {
                var checkedAttendeeList = [];

                _.each(vm.attendees, function(attendee) {
                    if (attendee.checked == true) {
                        checkedAttendeeList.push(attendee);
                    }
                });

                if (checkedAttendeeList.length > 0) {
                    vm.attendeeChecked = true;
                }
                else {
                    vm.attendeeChecked = false;
                }
            }

            vm.testIfSessionRegistrantsChecked = function(){
                var checkedSessionRegistrantsList = [];

                _.each(vm.sessionAttendees.sessions_attendees, function(attendeeFromSession){
                    if(attendeeFromSession.attendee.checked == true){
                        checkedSessionRegistrantsList.push(attendeeFromSession.attendee);
                    }
                });
                if (checkedSessionRegistrantsList.length > 0) {
                    vm.sessionRegistrantsChecked = true;
                }
                else {
                    vm.sessionRegistrantsChecked = false;
                }
            }

            vm.testIfSessionScannedChecked = function(){
                var checkedSessionScannedList = [];

                _.each(vm.sessionAttendees.scanned_attendees, function(scannedFromSession){
                    if(scannedFromSession.attendee.checked == true){
                        checkedSessionScannedList.push(scannedFromSession.attendee);
                    }
                });
                if (checkedSessionScannedList.length > 0) {
                    vm.sessionScannedChecked = true;
                }
                else {
                    vm.sessionScannedChecked = false;
                }
            }

            vm.deleteAll = false;

            // Deletes all attendees on confirm
            vm.deleteAllConfirmed = function() {
                ConferenceAttendeesService.removeAllAttendee(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(response){
                        vm.attendeeChecked = false;
                        vm.checkAllAttendees = false;
                        vm.attendees = [];
                        vm.attendees_total_count = 0;
                        vm.attendeesListForThisSession = [];
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Tests if the vertical scroll reached the end of the screen
            vm.verifyIfScrollIsRequired = function(for_hours) {
                var width = $window.innerWidth - 460;
                if (for_hours) {
                    var width = $window.innerWidth - 450;
                }

                if (vm.conferenceDays && (vm.conferenceDays.length * 172 > width)) return true;
                return false;
            }

            vm.checkSessionSpeaker = function(conference_speaker) {
                if (!conference_speaker.checked) {
                    conference_speaker.checked = true;
                } else {
                    conference_speaker.checked = false;
                }
            }

            // On Confirm it will delete the selected attendees
            vm.deleteAttendeesConfirmed = function(idList, checkedAttendeeList) {
                ConferenceAttendeesService.removeAttendee(idList, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                    .then(function(result){
                        toastr.success("Attendee removed from conference!", "Attendee Removed");
                        vm.attendeeChecked = false;
                        _.each(checkedAttendeeList, function(attendee) {
                            attendee.checked = false;
                            vm.attendeeChecked = false;
                            vm.checkAllAttendees = false;
                            var index = vm.attendees.indexOf(attendee);
                            if (index > -1) {
                                vm.attendees.splice(index, 1);
                            }
                        });
                        vm.attendeesListForThisSession = _.cloneDeep(vm.attendees);
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error(error.data.detail);
                        }
                    });
            }

            // On confirm it will delete the selected exhibitors
            vm.revokeExhibitorsConfirmed = function(checkedExhibitorList) {
                ConferenceExhibitorsService.revokeExhibitors(vm.conference.conference_id, checkedExhibitorList)
                    .then(function(response){
                        _.each(vm.exhibitors, function(exhibitor){
                            if (exhibitor.checked == true){
                                exhibitor.admin = null;
                                exhibitor.admin_email = null;
                                exhibitor.users_count = 0;
                                exhibitor.checked = false;
                            }
                        });
                        vm.exhibitorChecked = false;
                        vm.checkAllExhibitors = false;
                        SharedProperties.conference = null;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            vm.deleteExhibitorsConfirmed = function(checkedExhibitorList) {
                ConferenceExhibitorsService.deleteConferenceExhibitors(vm.conference.conference_id, checkedExhibitorList)
                    .then(function(response){
                        vm.exhibitors = _.reject(vm.exhibitors, ['checked', true]);
                        vm.exhibitorChecked = false;
                        vm.checkAllExhibitors = false;
                        SharedProperties.conference = null;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // On confirmation it will delete (revoke) all the exhibitors
            vm.revokeAllExhibitorsConfirmed = function() {
                ConferenceExhibitorsService.revokeAllExhibitors(vm.conference.conference_id)
                    .then(function(response){
                        _.each(vm.exhibitors, function(exhibitor){
                                exhibitor.admin = null;
                                exhibitor.admin_email = null;
                                exhibitor.users_count = 0;
                                exhibitor.checked = false;
                        });
                        vm.exhibitorChecked = false;
                        vm.checkAllExhibitors = false;
                        SharedProperties.conference = null;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            vm.deleteAllExhibitorsConfirmed = function() {
                var exhibitorIds = [];
                _.each(vm.exhibitors, function(exhibitor) {
                    exhibitorIds.push(exhibitor.id);
                });
                ConferenceExhibitorsService.deleteAllConferenceExhibitors(
                        vm.conference.conference_id,
                        exhibitorIds)
                    .then(function(response){
                        vm.exhibitorChecked = false;
                        vm.checkAllExhibitors = false;
                        console.log('what the f!!@!222');
                        vm.exhibitors = [];
                        SharedProperties.conference = null;
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            vm.viewMappingInstructions = true;
            vm.toggleViewMappingInstructions = function() {
                vm.viewMappingInstructions = !vm.viewMappingInstructions;
            }

            // Opens a confirmation for revoking the access to exhibitors
            vm.revokeAccessSelectedExhibitors = function() {
                if(vm.checkAllExhibitors){
                    $scope.deleteAllConfirmed = vm.revokeAllExhibitorsConfirmed;
                    $scope.resource = 'exhibitors';
                    ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_revoke_exhibitor_access.html',
                          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                          scope: $scope
                    });
                }
                else {
                    var checkedExhibitorList = [];
                    var idList = [];
                    _.each(vm.exhibitors, function(exhibitor) {
                        if (exhibitor.checked == true) {
                            checkedExhibitorList.push(exhibitor);
                            idList.push(exhibitor.id);
                        }
                    });

                    if (idList.length > 0) {
                        $scope.nrAttendees = idList.length;
                        $scope.deleteAttendeesConfirmed = vm.revokeExhibitorsConfirmed;
                        $scope.checkedAttendeeList = checkedExhibitorList;
                        $scope.ids = idList;
                        if (idList.length == 1) $scope.resource = 'exhibitor';
                        else $scope.resource = 'exhibitors';
                        $scope.type = "partial";
                        ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_revoke_exhibitor_access.html',
                              className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                              scope: $scope
                        });
                    }

                }
            }

            vm.deleteSelectedExhibitors = function() {
                if(vm.checkAllExhibitors){
                    $scope.deleteAllConfirmed = vm.deleteAllExhibitorsConfirmed;
                    $scope.resource = 'exhibitors';
                    $scope.type = "full";
                    ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_remove_exhibitors.html',
                          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                          scope: $scope
                    });
                }
                else {
                    var checkedExhibitorList = [];
                    var idList = [];
                    _.each(vm.exhibitors, function(exhibitor) {
                        if (exhibitor.checked == true) {
                            checkedExhibitorList.push(exhibitor);
                            idList.push(exhibitor.id);
                        }
                    });

                    if (idList.length > 0) {
                        $scope.nrAttendees = idList.length;
                        $scope.deleteAttendeesConfirmed = vm.deleteExhibitorsConfirmed;
                        $scope.checkedAttendeeList = checkedExhibitorList;
                        $scope.ids = idList;
                        if (idList.length == 1) $scope.resource = 'exhibitor';
                        else $scope.resource = 'exhibitors';
                        $scope.type = "partial";
                        ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_remove_exhibitors.html',
                              className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                              scope: $scope
                        });
                    }

                }
            }

            // Opens a confirmation popup for deleting attendees
            vm.deleteSelectedAttendees = function() {
                if(vm.checkAllAttendees){
                    $scope.deleteAllConfirmed = vm.deleteAllConfirmed;
                    $scope.resource = 'attendees';
                    $scope.type = "full";
                    ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_all_attendees.html',
                          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                          scope: $scope
                    });
                } else if(!(vm.checkAllAttendees && vm.deleteAll)){
                    var checkedAttendeeList = [];
                    var idList = [];
                    _.each(vm.attendees, function(attendee) {
                        if (attendee.checked == true) {
                            checkedAttendeeList.push(attendee);
                            idList.push(attendee.id);
                        }
                    });

                    if (idList.length > 0) {
                        $scope.nrAttendees = idList.length;
                        $scope.deleteAttendeesConfirmed = vm.deleteAttendeesConfirmed;
                        $scope.checkedAttendeeList = checkedAttendeeList;
                        $scope.ids = idList;
                        if (idList.length == 1) $scope.resource = 'attendee';
                        else $scope.resource = 'attendees';
                        $scope.type = "partial";
                        ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_all_attendees.html',
                              className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                              scope: $scope
                        });
                    }
                }
            }

            // Deletes all the conference users and updates the view
            vm.deleteAllUsersConfirmed = function() {
                ConferenceUsersService.removeAllUsers(vm.conference.conference_id)
                    .then(function(response){
                        vm.conferenceUsers = [];
                        vm.users_total_count = 0;
                        vm.userChecked = false;
                        vm.checkAllUsers = false;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Deletes the selected users and updates the view
            vm.deleteUsersConfirmed = function(idList, checkedUserList) {
                ConferenceUsersService.removeMultipleUsers(vm.conference.conference_id, idList)
                    .then(function(result){
                        toastr.success("User removed from conference!", "User Removed");
                        _.each(checkedUserList, function(user) {
                            var index = vm.conferenceUsers.indexOf(user);
                            if (index > -1) {
                                vm.conferenceUsers.splice(index, 1);
                            }
                        });
                        vm.userChecked = false;
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Opens a confirmation popup for deleting the conference users
            vm.deleteSelectedUsers = function() {
                if (vm.checkAllUsers) {
                    $scope.deleteAllConfirmed = vm.deleteAllUsersConfirmed;
                    $scope.resource = 'users';
                    ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_all_attendees.html',
                          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                          scope: $scope
                    });
                }
                else {
                    var checkedUserList = [];
                    var idList = [];
                    _.each(vm.conferenceUsers, function(user) {
                        if (user.checked == true && user.conference_user_rights.user_id !== vm.user.id) {
                            checkedUserList.push(user);
                            idList.push(user.conference_user_id);
                        }
                    });

                    if (idList.length > 0) {
                        $scope.nrAttendees = idList.length;
                        $scope.deleteAttendeesConfirmed = vm.deleteUsersConfirmed;
                        $scope.checkedAttendeeList = checkedUserList;
                        $scope.ids = idList;
                        if (idList.length == 1) $scope.resource = 'user';
                        else $scope.resource = 'users';
                        $scope.type = "partial";
                        ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_all_attendees.html',
                              className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                              scope: $scope
                        });
                    }
                }
            }

            // Updates the user stripe profile
            // on success it also updates the conference payment method
            vm.updateUser = function(user) {
                vm.user = user;
                $scope.user = vm.user;

                ConferenceService
                    .getConferenceById(vm.conference_id)
                    .then(function(conference) {
                      vm.conference=conference;
                      vm.setupConferenceData(conference);
                })
                .catch(function(error){

                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            };

            // Accessed when checking/unchecking items on the user rights panel
            // Updates the rights on the backend
            vm.updateRightsPerUser = function(rights_array){
                var user_id = -1;
                _.each(vm.conferenceUsers, function(user) {
                    if (user.checked == true) {
                        user_id = user.conference_user_id;
                    }
                });
                if(user_id != -1){
                ConferenceUsersService
                    .updateUserRights(user_id, rights_array)
                    .then(function(response){
                        toastr.success('User permissions updated successfully!');
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
                }
            };

            vm.changeCreditCard = function() {
                vm.openConferencePaymentForm();
            }

            // Accessed then clicking on the conference transcations link in the exhibitor payments module
            // if the transactions are loaded it just opens the modal
            // if not it loads the transcations
            vm.viewTransactions = function() {
                if (vm.conferenceTransactions) {
                    vm.openViewTransactionsModal(vm.conferenceTransactions);
                }
                else {
                    ConferenceExhibitorsService.listTransactions(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            vm.conferenceTransactions = response.conference_transactions;
                            vm.openViewTransactionsModal(vm.conferenceTransactions);
                        })
                        .catch(function(error){

                        });
                }
            }

            vm.checkTransactions = function(array, isChecked, previous) {
                _.each(array, function(element) {
                    if (previous) {
                        if (element.transfer_date < new Date()) {
                            element.checked = isChecked;
                        }
                    } else {
                        if (element.transfer_date >= new Date()) {
                            element.checked = isChecked;
                        }
                    }
                });
            }

            vm.openExhibitorAdvancedPayment = function() {
                $scope.conferenceTransactions = vm.conferenceExhibitorPayments;
                $scope.user = vm.user;
                $scope.conference = vm.conference;
                $scope.checkOne = vm.checkOne;
                $scope.checkTransactions = vm.checkTransactions;
                $scope.downloadTransactionsAsCsv = vm.downloadAdvancedTransactionsAsCsv;
                $scope.printTransactionsAsCsv = lib.printTransactionsAsCsv;

                $scope.nextPayout = vm.nextPayout;
                $scope.today = vm.today;
                $scope.totalPayment = vm.totalPayment;
                $scope.nrProcessed = vm.nrProcessed;
                $scope.nrPending = vm.nrPending;

                $scope.today = new Date();
                ngDialog.open({template: 'app/views/partials/conference/conference_paid_by_exhibitor_advanced_transactions.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            }

            // Opens the modal to view the transactions made to the conference owner's credit card for a conference that is
            // paid by the conference owner
            // @param transactions - the transactions that are loaded from the backend
            vm.openViewTransactionsModal = function(transactions) {
                $scope.conferenceTransactions = transactions;
                $scope.user = vm.user;
                $scope.conference = vm.conference;
                $scope.changeCreditCard = vm.changeCreditCard;
                $scope.checkOne = vm.checkOne;
                $scope.checkAll = vm.checkAll;
                $scope.downloadTransactionsAsCsv = vm.downloadTransactionsAsCsv;
                $scope.printTransactionsAsCsv = lib.printTransactionsAsCsv;
                ngDialog.open({template: 'app/views/partials/conference/conference_sign_up_transactions_modal.html',
                      className: 'app/stylesheets/_plusAttendee.scss',
                      controller: "ConferenceExhibitorsController as vm",
                      scope: $scope
                });
            }

            vm.attendeesMoreOptions = function() {
                vm.attendeesMoreOptionsCheck = !vm.attendeesMoreOptionsCheck;
            }


            vm.toggleHasAttendeeId = function() {
                vm.hasAttendeeId = !vm.hasAttendeeId;
            }

            // Process check one functionality from the list views
            vm.checkOne = function(element, name){
                if (element.checked != true) {
                    element.checked = true;
                } else {
                    element.checked = false;
                }
                if(name === 'user'){
                    vm.userPermissions = element.rights;
                    vm.testIfUserChecked();
                } else if(name === 'attendee') {
                    vm.testIfAttendeeChecked();
                } else if(name === 'exhibitor') {
                    vm.testIfExhibitorChecked();
                } else if(name === 'session_registrants') {

                    vm.testIfSessionRegistrantsChecked();
                } else if(name === 'session_scanned') {
                    vm.testIfSessionScannedChecked();
                }
            };
            vm.checkAllUsers = false;
            vm.checkAllAttendees = false;
            vm.checkAllExhibitors = false;
            vm.checkAllTransactions = false;
            vm.checkAllPromoCodes = false;
            vm.checkAllSessionRegistrants = false;
            vm.checkAllSessionScanned = false;

            // Process check all functionality from the list views
            vm.checkAll = function(array) {
                if(array === vm.conferenceUsers){
                    vm.checkAllUsers = !vm.checkAllUsers;
                    _.each(array, function(element) {
                        if(element.conference_user_rights.user_id !== vm.user.id){
                            element.checked = vm.checkAllUsers;
                        } else if(!vm.checkAllUsers){
                            element.checked = vm.checkAllUsers;
                        }
                    });
                    vm.testIfUserChecked();
                } else if(array === vm.attendees){
                    vm.checkAllAttendees = !vm.checkAllAttendees;
                     _.each(array, function(element) {
                        element.checked = vm.checkAllAttendees;
                    });
                    vm.testIfAttendeeChecked();
                } else if(array === vm.exhibitors){
                    vm.checkAllExhibitors = !vm.checkAllExhibitors;
                    _.each(array, function(element) {
                        element.checked = vm.checkAllExhibitors;
                    });
                    vm.testIfExhibitorChecked();
                }  else if(array === vm.conferenceTransactions){
                    vm.checkAllTransactions = !vm.checkAllTransactions;
                    _.each(array, function(element) {
                        element.checked = vm.checkAllTransactions;
                    });
                }   else if(array === vm.promo_codes){
                    vm.checkAllPromoCodes = !vm.checkAllPromoCodes;
                    _.each(array, function(element) {
                        element.checked = vm.checkAllPromoCodes;
                    });
                } else if(array === vm.sessionAttendees.sessions_attendees){
                    vm.checkAllSessionRegistrants = !vm.checkAllSessionRegistrants;
                    _.each(array, function(element) {
                        element.attendee.checked = vm.checkAllSessionRegistrants;
                    });
                    vm.testIfSessionRegistrantsChecked();
                } else if(array === vm.sessionAttendees.scanned_attendees){
                    vm.checkAllSessionScanned = !vm.checkAllSessionScanned;
                    _.each(array, function(element) {
                        element.attendee.checked = vm.checkAllSessionScanned;
                    });
                    vm.testIfSessionScannedChecked();
                } else if(array === vm.conferencePushNotificationPayments){
                    vm.checkAllPushNotificationPayments = !vm.checkAllPushNotificationPayments;
                    _.each(array, function(element) {
                        element.checked = vm.checkAllPushNotificationPayments;
                    });
                }
            };

            vm.downloadTransactionsAsCsv = function(transactions) {
                lib.downloadTransactionsAsCsv(transactions, vm.conference);
            }

            vm.modifyMarkupFlag = false;
            vm.modifyMarkup = function() {
                vm.modifyMarkupFlag = true;
            }

            vm.downloadAdvancedTransactionsAsCsv = function(transactions) {
                var printTransactions = [];
                _.each(transactions, function(transcation){
                    if (transcation.checked) {
                        printTransactions.push({
                            "Date": new Date(transcation.transfer_date).toString('MMMM, dddd ,yyyy'),
                            "Exhibitor": transcation.exhibitor.name,
                            "Processing": "$" + (transcation.payment.amount / 100).toFixed(2),
                            "Expo Fee": "$" + vm.conference.initial_fee.toFixed(2),
                            "Payout": "$" + ((transcation.payment.amount / 100) - vm.conference.initial_fee).toFixed(2)
                        })
                    }
                });
                lib.downloadAdvancedTransactionsAsCsv(printTransactions, vm.conference);
            }

            vm.sanitizeUrl = function(url) {
                if (url && url.substring(0, 7) != "http://" && url.substring(0, 8) != "https://") {
                    return "http://" + url;
                } else if(url && url.search('https://') == 0){
                    return url.replace('https://', 'http://');
                }

                return url;
            }

            vm.unSanitizeUrl = function(url) {
                if (url) {
                    if (url.substring(0, 7) == "http://") {
                        return url.replace("http://", "");
                    } else if (url.substring(0, 8) == "https://") {
                        return url.replace("https://", "");
                    }
                }

                return url;
            }

            var cloneConference = null;

            $scope.$watch(function(){
                return vm.editMode;
            }, function(editMode){
                $rootScope.$emit('EditConference', editMode);
            });

			vm.action = function(option){
                switch(option) {
                    case 'details':
                        vm.editMode = !vm.editMode;
                        if (vm.editMode) {
                            vm.conference.date_from_edited = moment(vm.conference.date_from);
                            vm.conference.date_to_edited = moment(vm.conference.date_to);

                            vm.conference.date_from_editable = vm.createMomentFromRawTime(vm.conference.date_from_raw);
                            vm.conference.date_to_editable = vm.createMomentFromRawTime(vm.conference.date_to_raw);
                            cloneConference = _.clone(vm.conference);
                        }
                        else {
                            if (cloneConference) {
                                vm.conference = _.cloneDeep(cloneConference);
                                $rootScope.$emit('Conference', vm.conference);
                            }
                        }
                        break;
                    default:

                }
            };

            vm.setNewQuestionForCurrentSession = function(){
                vm.newSessionItem.sessionquestion_set.push({
                    'type': 1,
                    'type_code': vm.sessionQuestionOptionType[0],
                    'text': '',
                    'sessionquestionoption_set': []
                });
            };

            vm.removeQuestionFromCurrentSession = function(index){
                vm.newSessionItem.sessionquestion_set = _.reject(vm.newSessionItem.sessionquestion_set, function(question){
                    return question == vm.newSessionItem.sessionquestion_set[index];
                });
            }

            vm.addOptionToQuestion = function(item){
                item.sessionquestionoption_set.push({'value': '', 'order': 1});
            };

            vm.removeOptionFromQuestion = function(item, index){
                item.sessionquestionoption_set = _.reject(item.sessionquestionoption_set, function(option){
                    return option == item.sessionquestionoption_set[index];
                });
            };

            vm.setQuestionType = function(item){
                if(item.type == 1){
                    item.sessionquestionoption_set = [];
                } else if(item.type == 2){
                    if(item.sessionquestionoption_set.length == 0) {
                        item.sessionquestionoption_set.push({'value': '', 'order': 1});
                    }
                } else if(item.type == 3){
                    if(item.sessionquestionoption_set.length == 0) {
                        item.sessionquestionoption_set.push({'value': '', 'order': 1});
                    }
                }
            };

            vm.newSession = false;
            vm.addNewSession = function(cloneSeletedDay) {
                vm.sessionQuestionOptionType = presets.sessionQuestionOptionType();

                if (!vm.newSessionItem || !vm.newSessionItem.session_day) {
                    vm.newSessionItem = {
                        speakers: [],
                        continuing_educations: [],
                        sessionquestion_set: [],
                        sessionspeaker_set: [],
                        validate_registrants: false
                    };
                }

                vm.currentSessionSpeakersList = _.cloneDeep(vm.conferenceSpeakersList.speakers);
                vm.newSessionItem.session_type = vm.sessionTypeOptions[0];
                if (cloneSeletedDay) {
                    vm.sessionSelectedDay = cloneSeletedDay;
                }
                var date = new Date(vm.sessionSelectedDay);

                vm.newSessionItem.starts_at = moment(date).set({ hour: 12, minute: 0 });
                vm.newSessionItem.ends_at = moment(date).set({ hour: 14, minute: 0 });

                vm.newSessionItem.session_day = moment(date).set({ hour: 0, minute: 0 });

                // if (cloneSeletedDay) {
                //     vm.newSessionItem.starts_at_editable = moment(date).set({ hour: 12, minute: 0 });
                //     vm.newSessionItem.ends_at_editable = moment(date).set({ hour: 13, minute: 0 });
                // }

                vm.newSession = true;
                vm.all_continuing_educations = [];
                _.each(vm.conference.continuing_educations, function(conference_ce){
                    vm.all_continuing_educations.push(conference_ce);
                });
            };

            var today = new Date();
            var starts_at_date = vm.moveInDetails.starts_at_date;
            if (!starts_at_date) {
                starts_at_date = new Date(vm.conference.date_from);
            }
            var currentDay = starts_at_date.getDate();
            var currentMonth = starts_at_date.getMonth();
            var currentYear = starts_at_date.getFullYear();
            vm.defaultHourStart = new Date(currentYear, currentMonth, currentDay, 12, 0, 0);
            vm.defaultHourEnd = new Date(currentYear, currentMonth, currentDay, 13, 0, 0);

            vm.moveInHours = [];
            vm.moveOutHours = [];
            vm.conferenceHours = [];
            _.each(vm.conference.hours, function(conferenceHour) {

                conferenceHour.starts_at_editable = vm.createMomentFromRawTime(
                    conferenceHour.starts_at_raw);
                conferenceHour.starts_at_editable_end = moment(
                    conferenceHour.starts_at_editable).endOf('day');
                conferenceHour.ends_at_editable = vm.createMomentFromRawTime(
                    conferenceHour.ends_at_raw);

                vm.conferenceHours.push(conferenceHour);
            });
            _.each(vm.conferenceHours, function(conferenceHour) {
                var dateData = _.cloneDeep(new Date(conferenceHour.starts_at));
                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                dateData.setTime( dateData.getTime() + (dateData.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                var day = dateData.getDate();
                var month = dateData.getMonth();
                var year = dateData.getFullYear();
                if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
                    vm.exhibitingDays[day + '/' + month + '/' + year] = [];
                }
                conferenceHour.starts_at = new Date(conferenceHour.starts_at);
                conferenceHour.ends_at = new Date(conferenceHour.ends_at);
                conferenceHour.exhibiting_day = moment(dateData).set({ hour: 0, minute: 0 });

                if (conferenceHour.type == 1) {
                    vm.exhibitingDays[day + '/' + month + '/' + year].push(conferenceHour);
                }
                else if (conferenceHour.type == 2) {
                    vm.moveInHours.push(conferenceHour);
                }
                else if (conferenceHour.type == 3) {
                    vm.moveOutHours.push(conferenceHour);
                }
            });

            vm.showExhibitingDay = {};

            _.each(vm.conferenceDays, function(day) {
                var dateData = _.cloneDeep(new Date(day));
                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                var day = dateData.getDate();
                var month = dateData.getMonth();
                var year = dateData.getFullYear();
                var defaultHourStart = new Date(year, month, day, 12, 0, 0);
                var defaultHourEnd = new Date(year, month, day, 13, 0, 0);
                if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
                    vm.showExhibitingDay[day + '/' + month + '/' + year] = true;
                }
                if (!vm.exhibitingDays[day + '/' + month + '/' + year] || vm.exhibitingDays[day + '/' + month + '/' + year].length < 1) {

                    vm.exhibitingDays[day + '/' + month + '/' + year] = [
                        {
                            starts_at: vm.defaultHourStart,
                            ends_at: vm.defaultHourEnd,
                            exhibiting_day: moment(dateData).set({ hour: 0, minute: 0 }),
                            ends_at_min: vm.conference.date_from_editable,
                            ends_at_max: vm.conference.date_to_editable,
                            room: null,
                            description: null
                        }
                    ];
                }
            });

            _.forOwn(vm.exhibitingDays, function(exhibitingDay, key){
                if (exhibitingDay.length == 0) {
                    vm.showExhibitingDay[key] = true;
                } else {
                    vm.showExhibitingDay[key] = false;
                }

                if (exhibitingDay.length == 0) {
                    exhibitingDay.push({});
                }
            });

            if (vm.moveInHours.length == 0) {
                vm.showMoveIn = true;
                vm.moveInHours.push(
                    {
                        starts_at: vm.defaultHourStart,
                        ends_at: vm.defaultHourEnd,
                        ends_at_min: vm.conference.date_from_editable,
                        ends_at_max: vm.conference.date_to_editable,
                        type: 2,
                        room: null,
                        description: null
                    }
                );
            }

            if (vm.moveOutHours.length == 0) {
                vm.showMoveOut = true;
                vm.moveOutHours.push(
                    {
                        starts_at: vm.defaultHourStart,
                        ends_at: vm.defaultHourEnd,
                        ends_at_min: vm.conference.date_from_editable,
                        ends_at_max: vm.conference.date_to_editable,
                        type: 3,
                        room: null,
                        description: null
                    }
                );
            }

            vm.moveInOutHours = {
                min: lib.format_date_for_validation(vm.conference.date_from),
                max: lib.format_date_for_validation(vm.conference.date_to)
            }

            vm.conferenceHours = _.cloneDeep(vm.conference.hours);

            vm.newExhibitingHour
            vm.exhibitingInfo = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                ends_at_min: vm.conference.date_from_editable,
                ends_at_max: vm.conference.date_to_editable,
                room: null,
                description: null
            };

            vm.moveInDetails = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                ends_at_min: vm.conference.date_from_editable,
                ends_at_max: vm.conference.date_to_editable,
                room: null,
                description: null
            };

            vm.moveOutDetails = {
                starts_at: vm.defaultHourStart,
                ends_at: vm.defaultHourEnd,
                ends_at_min: vm.conference.date_from_editable,
                ends_at_max: vm.conference.date_to_editable,
                room: null,
                description: null
            };

            vm.recalculateSessionEndsAtMax = function(newValue, oldValue) {
                var starts_at_editable_date = new Date(newValue);
                vm.newSessionItem.starts_at_editable_end = moment(newValue).endOf('day');

                $timeout(function() {
                    if (vm.newSessionItem.ends_at_editable <= vm.newSessionItem.starts_at_editable) {
                        vm.newSessionItem.ends_at_editable = moment(newValue).add(1, 'hour');
                    }
                }, 100);

                vm.presetSessionDate();
            }

            // Process the action of clicking on the edit conference button
            vm.editSession = function() {
                vm.sessionQuestionOptionType = presets.sessionQuestionOptionType();
                vm.currentSessionSpeakersList = [];
                vm.newSessionItem = _.cloneDeep(vm.sessionDetailsView);

                if (vm.newSessionItem.speakers.length == 0) {
                    vm.newSessionItem.sessionspeaker_set = [];
                } else{
                    vm.newSessionItem.sessionspeaker_set = _.cloneDeep(vm.newSessionItem.speakers);
                }

                vm.currentSessionSpeakersList = _.differenceBy(vm.conferenceSpeakersList.speakers, vm.newSessionItem.sessionspeaker_set, 'id');

                vm.all_continuing_educations = [];
                vm.newSessionItem.continuing_educations = [];
                vm.newSessionItem = _.omit(vm.newSessionItem, ['session_continuing_educations']);
                _.each(vm.sessionDetailsView.session_continuing_educations, function(session_ce){
                    vm.newSessionItem.continuing_educations.push(session_ce.continuing_education);
                });

                vm.newSessionItem.starts_at = moment(vm.newSessionItem.starts_at);
                vm.newSessionItem.ends_at = moment(vm.newSessionItem.ends_at);

                if (vm.newSessionItem.starts_at_raw) {
                    vm.newSessionItem.starts_at_editable = vm.createMomentFromRawTime(
                        vm.newSessionItem.starts_at_raw);

                    vm.newSessionItem.starts_at_editable_end = moment(new Date(
                        vm.newSessionItem.starts_at_raw.year,
                        vm.newSessionItem.starts_at_raw.month - 1,
                        vm.newSessionItem.starts_at_raw.day,
                        23,
                        59
                    ));
                }

                if (vm.newSessionItem.ends_at_raw) {
                    vm.newSessionItem.ends_at_editable = vm.createMomentFromRawTime(
                        vm.newSessionItem.ends_at_raw);
                }

                vm.newSessionItem.session_day = moment(vm.newSessionItem.starts_at).set({ hour: 0, minute: 0 });

                vm.newSession = true;
                vm.newSessionItem.session_type = vm.getSessionTypeName(vm.newSessionItem.session_type);
                _.each(vm.conference.continuing_educations, function(conference_ce){
                    var has_ce = _.find(vm.newSessionItem.continuing_educations, function(session_ce){
                        return session_ce.id == conference_ce.id;
                    });
                    if(has_ce == undefined){
                        vm.all_continuing_educations.push(conference_ce);
                    }
                });
            }

            // After registering payment via stripe the conference payment method is updated in the backend
            vm.updatePaymentMethodAfterRegisteringStripe = function() {
                vm.conference.payment_type = 3;
                vm.paymentType = 'conference';
                ConferenceService.updateConference(vm.conference)
                    .then(function(){
                        toastr.success('The conference payment method has been updated successfully.', 'Conference Updated!');
                        vm.paymentType='conference';
                        vm.editMode = false;
                    })
                    .catch(function(error){

                        toastr.error('Error!', 'There has been a problem with your credit card.');
                    })
            }

            // Change the conference country via the dropdown
            // If the country is not the USA the state field becomes a text field
            vm.changeCountry = function() {
                if (vm.conference.country != vm.all_countries[0]) {
                    vm.conference.state = "";
                } else {
                    vm.conference.state = vm.united_states[0];
                }
            }

            vm.enterBenefit = function(benefit) {
                vm.currentLevelItem.registrationlevelbenefit_set.push({});
                $timeout(function() {
                    $(".level-benefit-list:last > input").focus();
                }, 100);
            }

            // Select payment method for the conference
            // Updates the setting in the backend
            // @param type
            // If the type is 3 : paid by conference owner  and if the conference owner has no payment registered on the backend
            // it will prompt the owner to setup the payment via stripe
            vm.checkPaymentMethod = function(type) {
                 if (vm.lockPaymentType) {
                    toastr.warning('You cannot change the payment type once an exhibitor made its lead upgrade payment');
                    return;
                 }
                 if (type == "advanced" && !vm.conference.business_data.merchant_id_verified) {
                    merchantService.startMerchantAgreement($scope)
                    return;
                 } else if (type == "advanced") {
                    if (!vm.markupFee) {
                        $("#markupInputFee").focus();
                        return;
                    }

                 }
                 if ((type == 'standard' && vm.conference.payment_type == 1)
                    || (type == 'conference' && vm.conference.payment_type == 3)) return;

                 if (!vm.conference.business_data.has_registered_payment && type == 'conference') {
                    vm.openConferencePaymentForm(true);
                 }
                 else {
                    vm.paymentType = type;
                    if (type == 'standard') {
                        vm.conference.payment_type = 1;
                    } else if (type == 'conference') {
                        vm.conference.payment_type = 3;
                    } else if (type == 'advanced') {
                        vm.conference.payment_type = 2;
                        vm.conference.markup_fee = parseInt(vm.markupFee);
                    }

                    ConferenceService.updateConference(vm.conference)
                        .then(function(){
                            toastr.success('The conference payment method has been updated successfully.', 'Conference Updated!');
                            vm.editMode = false;
                        })
                        .catch(function(error){

                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error('Error!', 'There has been a problem with your request.');
                            }
                        });
                 }
            };

            vm.cancelNewSession = function() {
							 vm.newSessionItem = {}; //clear fields on cancel
               vm.newSession = false;
            };

            vm.updateExhibitorBooth = function(new_exhibitor) {
                var exhibitor = _.find(vm.exhibitors, {id:new_exhibitor.id});
                if (exhibitor.booth == new_exhibitor.booth) return;
                ConferenceExhibitorsService.setExhibitorBooth(exhibitor, new_exhibitor.booth)
                    .then(function(response){
                        exhibitor.booth = response.booth;
                        exhibitor.name = response.name;
                        exhibitor.company_name = response.name;
                        exhibitor.booth_contact_first_name = response.booth_contact_first_name;
                        exhibitor.booth_contact_last_name = response.booth_contact_first_name;
                    })
                    .catch(function(error){

                    });
            }

            vm.processSpeakerPostUpdate = function(speaker, session_edited) {
                var foundSpeaker = _.find(vm.conferenceSpeakersList.speakers, function(speakerItem){
                    return (speakerItem.id == speaker.speaker);
                });
                if (foundSpeaker) {
                    speaker.speaker = foundSpeaker;
                    if (speaker.is_moderator) {
                        vm.newSessionItem.is_moderator = foundSpeaker.id;
                        if (session_edited) {
                            _.set(session_edited, 'is_moderator', foundSpeaker.id);
                        }
                    }
                }

                return speaker;
            }

            // Create a new session / Updates an existing sessions
            // @param {Object} session - the session payload
            // @param {Bool} add_another - true/false, if it opens the create session form again on complete
            vm.saveSessionButtonStatus = true;
            vm.createNewSession = function(newSession, add_another){
                var update = false;
                var updatedSpeaker = {};
								 //clear empty options before save
									if (vm.newSessionItem.sessionquestion_set && vm.newSessionItem.sessionquestion_set.length > 0) {
										_.each(vm.newSessionItem.sessionquestion_set, function(question) {
											question.sessionquestionoption_set = _.reject(question.sessionquestionoption_set, ['value', ""]);
										})
								}
                var session = _.cloneDeep(newSession);
                session.speakers = [];
                if (!lib.validateCreateSession(session, toastr)) return;
                if (!lib.validateSpeakers(session.speakers, toastr)) return;
                if (!session.ce_hours){
                    session.ce_hours = 0;
                } else{
                    session.ce_hours = new Number(session.ce_hours);
                }

                var cloneSeletedDay = _.clone(vm.sessionSelectedDay);

                _.each(session.sessionspeaker_set, function(added_speaker){
                    session.speakers.push(added_speaker.id);
                });

                session = _.omit(session, ['sessionspeaker_set']);

                if(session.id && _.find(vm.sessions, {'id':session.id})){
                    update = true;
                } else{
                    session.conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                    update = false;
                }
                if (session.materials && session.materials.length > 0) {
                    session.sessionfile_set = [];
                    _.each(session.materials, function(material){
                        session.sessionfile_set.push(material);
                    });
                }
                var customsessionfieldvalue_set = [];
                _.each(session.custom_fields, function(value, key) {
                    if (Array.isArray(value)) {
                        value = value.join();
                        // session.custom_fields[key] = value;
                    }
                    customsessionfieldvalue_set.push({
                        value: value,
                        custom_session_field: key
                    });
                });

                session.customsessionfieldvalue_set = customsessionfieldvalue_set;
                vm.saveSessionButtonStatus = false;
                session.ends_at = new Date(session.ends_at);
                session.starts_at = new Date(session.starts_at);

                if (session.starts_at_editable) {
                    var starts_at_editable = new Date(session.starts_at_editable);
                    session.starts_at_raw = {
                        "year": starts_at_editable.getFullYear(),
                        "month": starts_at_editable.getMonth() + 1,
                        "day": starts_at_editable.getDate(),
                        "hour": starts_at_editable.getHours(),
                        "minute": starts_at_editable.getMinutes(),
                    }
                }

                if (session.ends_at_editable) {
                    var ends_at_editable = new Date(session.ends_at_editable);
                    session.ends_at_raw = {
                        "year": ends_at_editable.getFullYear(),
                        "month": ends_at_editable.getMonth() + 1,
                        "day": ends_at_editable.getDate(),
                        "hour": ends_at_editable.getHours(),
                        "minute": ends_at_editable.getMinutes(),
                    }
                }

                ConferenceSessionsService
                    .addConferenceSession(session, update)
                    .then(function(data){
                        vm.saveSessionButtonStatus = true;
                        var existingSession = _.find(vm.sessions, {'id':data.id});
                        toastr.success('The session has been created successfully.', 'Session Created!');
                        SharedProperties.registrationSessions = null;
                        if (!existingSession) {
                            vm.sessions.push(data);
                            SharedProperties.sessions = {
                                'sessions': vm.sessions
                            }

                            vm.newSessionItem = {speakers: [{}], continuing_educations: []};
                            var dateDay = _.cloneDeep(new Date(data.starts_at));
                            var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                            dateDay.setTime( dateDay.getTime() + (dateDay.getTimezoneOffset() + offsetInMinutes)*60*1000 );
                            dateDay.setHours(0, 0, 0, 0);

                            data.dateDay = dateDay;
                            lib.postSaveSessionSessionDayProcessing(data, vm.sessionDays);
                            var speakers = [];
                            _.each(data.sessionspeaker_set, function(speaker){
                                speaker = vm.processSpeakerPostUpdate(speaker, data);
                                speakers.push(speaker.speaker);
                            });
                            _.set(data, 'speakers', speakers);
                            if(add_another === true){
                                vm.sessionSelectDay(dateDay);
                                vm.moveDetails=false;
                                vm.viewSessionDetails(null);
                                vm.orderSessions(true);
                                vm.newSession = false;

                                $timeout(function() {
                                  vm.addNewSession(cloneSeletedDay);
                                }, 10);
                            } else{

                                vm.sessionSelectDay(dateDay);
                                vm.moveDetails=false;
                                vm.viewSessionDetails(null);
                                vm.orderSessions(true);
                                vm.newSession = false;
                            }
                        } else {
                            vm.saveSessionButtonStatus = true;
                            existingSession = data;
                            vm.newSessionItem = {speakers: [{}], continuing_educations: []};
                            var day = new Date(data.starts_at).getDate();
                            var existingSession2 = lib.initSessionDayAndReturn(data, vm.sessionDays, data);
                            existingSession2 = data;
                            _.each(vm.sessionsForDay, function(session_edited){
                                if(session_edited.id == data.id){
                                    _.set(session_edited, 'session_continuing_educations', data.sessioncontinuingeducation_set);
                                    _.set(session_edited, 'sessionquestion_set', data.sessionquestion_set);
                                    var speakers = [];
                                    _.each(data.sessionspeaker_set, function(speaker){
                                        speaker = vm.processSpeakerPostUpdate(speaker, session_edited);
                                        speakers.push(speaker.speaker);
                                    });
                                    _.set(session_edited, 'speakers', speakers);
                                    _.set(session_edited, 'max_capacity', data.max_capacity);
                                    _.set(session_edited, 'ce_hours', data.ce_hours);
                                    _.set(session_edited, 'title', data.title);
                                    _.set(session_edited, 'starts_at', data.starts_at);
                                    _.set(session_edited, 'ends_at', data.ends_at);
                                    _.set(session_edited, 'description', data.description);
                                    _.set(session_edited, 'location', data.location);
                                    _.set(session_edited, 'session_type', data.session_type);
                                    _.set(session_edited, 'custom_fields', session.custom_fields);
                                    return;
                                }
                            });
                            if(add_another === true){
                                vm.addNewSession(cloneSeletedDay);
                            } else{
                                vm.viewSessionDetails(null);
                                vm.orderSessions(true);
                                vm.newSession = false;
                            }
                        }
                    })
                    .catch(function(error){
												vm.saveSessionButtonStatus = true; //unblocks button

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr)
                            vm.saveSessionButtonStatus = true;
                        }
                    });
            };

            vm.verifyCurrentUser = function(){
                var isFound, usersChecked = [], ok = true;
                isFound = _.find(vm.conference.conference_users, function(conference_user){
                    return (conference_user.conference_user_rights.user_id === vm.user.id);
                }) || undefined;
                _.each(vm.conferenceUsers, function(current_user){
                    if(current_user.checked){
                        usersChecked.push(current_user);
                    }
                });
                _.each(usersChecked, function(userChecked){
                    if((isFound !== undefined) && isFound.conference_user_rights.user_id === userChecked.conference_user_rights.user_id){
                        ok = false;
                        return;
                    }
                });
               return ok;
            };

            // Ads a conference user via email
            // validates if the email does not belong to the current user or the conference owner
            // also validates if there already is a conference user with that email
            // on success it will either add the new conference user (if an user exists with that email) or the active invitation to the view
            vm.addConferenceUserLock = false;
            vm.addUser = function() {
                if (vm.addConferenceUserLock) return;
                var ok = true;
                var invalidUser = false;
                _.forEach(vm.conferenceUsers, function(userFromTable){
                    if(vm.invitedUser.invited_user_email === userFromTable.invited_user_email){
                        ok = false;
                    }
                });
                if((vm.invitedUser.invited_user_email == vm.user.email) || (vm.conferenceOwner.email == vm.invitedUser.invited_user_email)){
                    invalidUser = true;
                }
                if(ok && !invalidUser){
                    vm.addConferenceUserLock = true;
                    ConferenceService.inviteUser(vm.invitedUser, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(data){
                            if (data) {
                                vm.addConferenceUserLock = false;
                                toastr.success('The user has been invited to your conference.', 'Invitation Sent!');
                                var foundUser = _.find(vm.conferenceUsers, function(userItem){
                                    return userItem.id === data.id;
                                });
                                if (!foundUser) {
                                    vm.conferenceUsers.push(data);
                                }
                                vm.invitedUser = {
                                    rights: [],
                                    invited_user_email: ''
                                };
                                vm.searchEmail = false;
                            }
                        })
                        .catch(function(error){
                            vm.addConferenceUserLock = false;
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error(error.data.detail);
                            }
                        });
                } else if(!ok){
                    toastr.warning('This user has been already invited to your conference.');
                } else if(invalidUser){
                    toastr.warning("You can't perform this action!")
                }
            }

            // The user qr code is a combination of it's attendee id, first name and last name
            vm.getUserQrCode = function(user) {
                return user.attendee_id + ';' + user.first_name + ';' + user.last_name;
            }

            // Create the new support contact
            // On success it adds the new support contact in the view
            vm.createNewSupportContact = function(newSupportContact, closeThisDialog, eraseInputs){
                var ok = true;
                newSupportContact.conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                _.each(newSupportContact, function(contact, val){
                    if(contact != 'phone_number' && contact != 'email' && _.isEmpty(val)){
                        ok = false;
                        return false;
                    }
                });
                if(!ok){
                    toastr.error('Field required!');
                    return;
                }
                ConferenceService
                    .createSupportContact(newSupportContact)
                    .then(function(data){
                        if(data) vm.supportContacts.push(data);
                        if (closeThisDialog) {
                            closeThisDialog();
                        }
                        if (eraseInputs) {
                            eraseInputs();
                        }
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr)
                        }
                    });
            }

            vm.createNewCESupportContact = function(newSupportContact, closeThisDialog, eraseInputs){
                newSupportContact.conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                ConferenceService
                    .createCESupportContact(newSupportContact)
                    .then(function(data){
                        if(data) vm.conferenceexhibitorSupportContacts.push(data);
                        if (closeThisDialog) {
                            closeThisDialog();
                        }
                        if (eraseInputs) {
                            eraseInputs();
                        }
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr);
                        }
                    });
            }

            // The function to delete the support contact after confirmation
            // on success the support contact view will removed the said contact from the page
            vm.deleteCESupportContactConfirm = function(contact_id){
                ConferenceService
                    .deleteCESupportContact(contact_id)
                    .then(function(data){
                        vm.conferenceexhibitorSupportContacts = _.reject(vm.conferenceexhibitorSupportContacts, ['id', contact_id]);
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // The function to delete the support contact after confirmation
            // on success the support contact view will removed the said contact from the page
            vm.deleteSupportContactConfirm = function(contact_id){
                ConferenceService
                    .deleteSupportContact(contact_id)
                    .then(function(data){
                        vm.supportContacts = _.reject(vm.supportContacts, ['id', contact_id]);
                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            // Opens a confirmation popup
            // The function to delete the support contact after confirmation is sent to the modal
            vm.deleteSupportContact = function(contact_id){
                $scope.deleteSupportContactConfirm = vm.deleteSupportContactConfirm;
                $scope.contact_id = contact_id;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_delete_conference_support.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }

            // Opens a confirmation popup
            // The function to delete the support contact after confirmation is sent to the modal
            vm.deleteCESupportContact = function(contact_id){
                $scope.deleteSupportContactConfirm = vm.deleteCESupportContactConfirm;
                $scope.contact_id = contact_id;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_delete_conference_support.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }

            // Update the support contact data
            vm.updateSupportContact = function(closeThisDialog){
                ConferenceService
                    .updateSupportContact(vm.currentContact)
                    .then(function(data){
                        var supportContacts = [];
                        _.each(vm.supportContacts, function(support_contact){
                            if (support_contact.id == data.id) support_contact = data
                            supportContacts.push(support_contact);
                        });
                        vm.supportContacts = supportContacts;
                        closeThisDialog();
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr);
                        }
                    });
            }

            // Update the support contact data
            vm.updateCESupportContact = function(closeThisDialog){
                ConferenceService
                    .updateCESupportContact(vm.currentContact)
                    .then(function(data){
                        var supportContacts = [];
                        _.each(vm.conferenceexhibitorSupportContacts, function(support_contact){
                            if (support_contact.id == data.id) support_contact = data
                            supportContacts.push(support_contact);
                        });
                        vm.conferenceexhibitorSupportContacts = supportContacts;
                        closeThisDialog();
                    })
                    .catch(function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            lib.processValidationError(error, toastr);
                        }
                    });
            }

            // Upload photo for support contact
            // using the same signature v4 functionality
            vm.uploadSupportContactPhoto = function(files, contact) {
                FileService
                    .getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        if(contact.id) {
                            promises.push(FileService.uploadFile(files[0], response));
                        } else {
                            promises.push(FileService.uploadFile(files[0], response));
                        }

                        $q.all(promises).then(function(response){
                            contact.photo = file_link;
                        }).catch(function(error){

                        });

                    })
                    .catch(function(error){

                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
            }

            vm.scrollBottomSpeakers = function() {
                $("#session-speaker-right-panel").animate({
                  scrollTop: $("#session-speaker-right-panel").height()
                }, {
                    duration: 200
                });
            }

            vm.scrollRight = function() {
                var scrollLeft = $("#days-list-scroll").scrollLeft();
                $("#days-list-scroll").animate({
                  scrollLeft: scrollLeft + 171
                }, 300);
            }

            vm.scrollLeft = function() {
                var scrollLeft = $("#days-list-scroll").scrollLeft();
                $("#days-list-scroll").animate({
                  scrollLeft: scrollLeft - 171
                }, 300);
            }

            var promise;
            vm.scrollLeftHold = function() {
                if(promise){
                    $interval.cancel(promise);
                }
                promise = $interval(function () {
                    var scrollLeft = $("#days-list-scroll").scrollLeft();
                    $("#days-list-scroll").animate({
                      scrollLeft: scrollLeft - 2
                    }, 0);
                }, 5);
            }

            vm.stopScroll = function() {
                $interval.cancel(promise);
                promise = null;
            }

            vm.scrollRightHold = function() {
                if(promise){
                    $interval.cancel(promise);
                }
                promise = $interval(function () {
                    var scrollLeft = $("#days-list-scroll").scrollLeft();
                    $("#days-list-scroll").animate({
                      scrollLeft: scrollLeft + 2
                    }, 0);
                }, 5);
            }

            vm.selectedAttendees = [];
            vm.csvUrl = {};

            vm.JSON2CSV = function() {
                lib.JSON2CSV(vm.attendees, vm.selectedAttendees, vm.csvUrl, vm.conference);
            };

            //cover screen controller

            vm.defaultBackgroundImagesFromAppSolution = [
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_0.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_1.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_2.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_3.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_4.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_5.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_6.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_7.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_8.png'
                },
                {
                    'background_url': 'assets/images/cover-screen-default/conference_background_9.png'
                }
            ];

            SharedProperties.coverScreenOptions = vm.coverScreen || {};
            SharedProperties.coverScreensImagesOptions = (vm.coverScreen.background_url == null ?
                // (default_cover_screens != null ? default_cover_screens.default_cover_screen_images : [])
                vm.defaultBackgroundImagesFromAppSolution : [{'background_url': vm.coverScreen.background_url}]);
            SharedProperties.coverScreenOptions.defaultImageIndex = (vm.coverScreen.default_image_id == null ? 1 : vm.coverScreen.default_image_id);
            SharedProperties.coverScreenOptions.coverScreenOverlay = vm.coverScreen.overlay_type == 'dark' ? false : (vm.coverScreen.overlay_type == 'light');
            SharedProperties.coverScreenOptions.displayOverlay = !(vm.coverScreen.overlay_type == 'off');
            SharedProperties.coverScreenOptions.coverScreenTextColor = vm.coverScreen.text_color_type == 'dark' ? false : (vm.coverScreen.text_color_type == 'light');
            vm.coverScreenOverlay = SharedProperties.coverScreenOptions.coverScreenOverlay;
            vm.displayOverlay = SharedProperties.coverScreenOptions.displayOverlay;
            vm.coverScreenTextColor = SharedProperties.coverScreenOptions.coverScreenTextColor;
            vm.defaultImageIndex = SharedProperties.coverScreenOptions.defaultImageIndex;
            vm.coverScreensImagesOptions =  SharedProperties.coverScreensImagesOptions;
            vm.temporaryBackgroundUrlExists = (vm.coverScreen.background_url != null);

            vm.setConferenceCoverScreen = function() {
                vm.disableSaveCoverScreenButton = true;
                var cover_screen = {};
                var uploadedConferenceLogoFromCoverScreen = '';
                var stringOverlayMode = mapOverlayToString(vm.coverScreenOverlay);
                var textColorMode = mapOverlayToString(vm.coverScreenTextColor);
                var logo = SharedProperties.coverScreenOptions.logo;
                var background = SharedProperties.coverScreenOptions.background;
                var promises = [];

                if (logo) {
                    promises.push(uploadCoverScreenResources(logo));
                    var logoUploaded = true;
                }
                if (background != undefined) {
                    promises.push(uploadCoverScreenResources(background));
                    var backgroundUploaded = true;
                }

                $q.all(promises).then(function(response){
                    if (response.length == 0) {
                        if (vm.coverScreen.background_url) {
                            cover_screen.background_url = vm.coverScreen.background_url;
                            cover_screen.default_image_id = null;
                        } else {
                            cover_screen.background_url = null;
                            cover_screen.default_image_id = SharedProperties.coverScreenOptions.defaultImageIndex;
                        }
                    }
                    else if (response.length == 1){
                        if(logoUploaded){
                            uploadedConferenceLogoFromCoverScreen = response[0];
                        } else if(backgroundUploaded){
                            cover_screen.background_url = response[0];
                        }
                    } else if (response.length == 2){
                        uploadedConferenceLogoFromCoverScreen = response[0];
                        cover_screen.background_url = response[1];
                    }

                    cover_screen.overlay_type = !vm.displayOverlay ? 'off' : stringOverlayMode;
                    cover_screen.text_color_type = textColorMode;
                    if (!cover_screen.background_url) {
                        cover_screen.default_image_id = SharedProperties.coverScreenOptions.defaultImageIndex;
                    }

                    ConferenceService
                        .updateConference({'cover_screen': cover_screen,
                            'logo': !_.isEmpty(uploadedConferenceLogoFromCoverScreen) ? uploadedConferenceLogoFromCoverScreen : vm.conference.logo,
                            'conference_id': ConferenceService.hash_conference_id.decode(vm.conference_id)[0]})
                        .then(function(response){
                            toastr.success('Your Conference Cover Screen was successfully updated!');
                            vm.disableSaveCoverScreenButton = false;
                            vm.temporaryLogoUrlExists = false;
                        }, function(error){
                            toastr.error('There was a problem with your upload!');
                            vm.disableSaveCoverScreenButton = false;
                        });
                }, function(error) {
                    vm.disableSaveCoverScreenButton = false;
                    toastr.error("Error on uploading images! Please try another to upload other images.")
                    console.error(error);
                })
            };

            vm.uploadLogo = function(files) {
                if (files.length > 0) {
                    SharedProperties.coverScreenOptions.temporaryLogoUrl = files[0].$ngfBlobUrl;
                    SharedProperties.coverScreenOptions.logo = files[0];
                    vm.temporaryLogoUrlExists = true;
                }
            };

            vm.uploadBackground = function(files) {
                if (files.length > 0) {
                    SharedProperties.coverScreenOptions.temporaryBackgroundUrl = files[0].$ngfBlobUrl;
                    SharedProperties.coverScreenOptions.background = files[0];
                    SharedProperties.coverScreenOptions.removeSlider = true;

                    vm.defaultImageIndex = 1;
                    vm.temporaryBackgroundUrlExists = true;
                }
            };

            vm.revertTemporaryLogo = function() {
                delete SharedProperties.coverScreenOptions.temporaryLogoUrl;
                delete SharedProperties.coverScreenOptions.logo;

                vm.temporaryLogoUrlExists = false;
            };

            vm.revertTemporaryBackground = function() {
                delete SharedProperties.coverScreenOptions.temporaryBackgroundUrl;
                delete SharedProperties.coverScreenOptions.background_url;

                SharedProperties.coverScreenOptions.removeSlider = false;
                SharedProperties.coverScreensImagesOptions = vm.defaultBackgroundImagesFromAppSolution;
                // (default_cover_screens != null ?  default_cover_screens.default_cover_screen_images : []);
                SharedProperties.coverScreenOptions.background = null;
                vm.temporaryBackgroundUrlExists = false;
            };

            function uploadCoverScreenResources(image) {
                var deferred = $q.defer();

                if (image) {
                    FileService.getUploadSignature(image)
                        .then(function(response){
                            var file_link = response.data.file_link;
                            FileService.uploadFile(image, response)
                                .then(function(uploadResponse) {
                                    deferred.resolve(file_link)
                                }, function(error) {
                                    deferred.reject(error);
                                })
                    }, function(error){
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }

                        deferred.reject(error);
                    });
                } else {
                    deferred.reject('No image found');
                }

                return deferred.promise;
            }


            function mapOverlayToString(booleanMode) {
                if (booleanMode == true) {
                    return 'light';
                } else {
                    return 'dark';
                }
            }

            $scope.$watch(function() { // because ng-change isn't fired on md-switch
                $rootScope.$broadcast('overlayModeChanged', {overlayMode: vm.coverScreenOverlay});
                SharedProperties.coverScreenOptions.coverScreenOverlay = vm.coverScreenOverlay;
            });

            //setupNotificationsController

            vm.currentNotificationsNumber = 1000;

            vm.notificationCreditsDropdown = [
                '1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '15000', '20000',
                '25000', '30000', '40000', '50000', '75000', '100000'
            ];
            vm.finalPrice = 50;


            // vm.decrementNotificationsNumber = function() {
            //     if (vm.currentNotificationsNumber > 1000) {
            //         vm.currentNotificationsNumber = vm.currentNotificationsNumber - 1000;
            //     }
            //
            //     vm.finalPrice = calculatePriceForNotificationsCredits(vm.currentNotificationsNumber);
            // }
            //
            // vm.incrementNotificationsNumber = function() {
            //     vm.currentNotificationsNumber = vm.currentNotificationsNumber + 1000;
            //     vm.finalPrice = calculatePriceForNotificationsCredits(vm.currentNotificationsNumber);
            // }

            vm.checkNotificationStatus = function(notification) {
                var conferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                manageNotificationsService.getScheduledNotification(conferenceId, notification)
                    .then(function(response) {
                        notification = response.data;
                        _.each(vm.scheduledNotifications, function(value, key, obj) {
                            if (value.id === notification.id) {
                                notification.nr_sent = notification.cost;
                                vm.scheduledNotifications[key] = notification;
                            }
                        });
                        if (!notification.was_processed) {
                            $timeout(function() {
                              vm.checkNotificationStatus(notification);
                            }, 5000);
                        }
                    });
            }


            vm.buyNotifications = function(numberOfCredits, finalPrice) {
                ngDialog.open({
                    plain: true,
                    template: "<credit-card-modal></credit-card-modal>",
                    className: 'app/stylesheets/_plusAttendee.scss',
                    data: {
                        amountOfCredits: numberOfCredits,
                        finalPrice: finalPrice
                    }
                });
            }

            vm.calculatePriceForNotifications = function(){
                vm.finalPrice = calculatePriceForNotificationsCredits(vm.currentNotificationsNumber);
            }

			vm.selectThisPriceForNotificationDropDown = function(price){
				vm.currentNotificationsNumber = price;
				vm.finalPrice = calculatePriceForNotificationsCredits(vm.currentNotificationsNumber);
			};


            function calculatePriceForNotificationsCredits(numberOfDesiredCredits) {
                var priceForOneBatch = 50; // batch = 1000 credits
                var numberOfBatches = numberOfDesiredCredits / 1000;

                var discountPercentage = numberOfDesiredCredits >= 5000 ? vm.discountPercentage : 0;
                var finalPrice = numberOfBatches * priceForOneBatch - (discountPercentage/100 * numberOfBatches * priceForOneBatch);


                return finalPrice;
            }



            vm.loadingNotificatioStatus = true;
            function checkStatusOfNotificationSetup(conferenceId) {
                manageNotificationsService.checkStatusOfNotificationSetup(conferenceId).then(function(response) {
                    if (!response.data.has_setup_notifications) {
                        vm.discountPercentage = response.data.bulk_discount_percentage;
                        vm.oneBatchPrice = response.data.price_per_1000_credits;
                        vm.loadingNotificatioStatus = false;
                    } else {
                        vm.totalCredits = response.data.nr_remaining_credits;
                        vm.remainingCredits = vm.totalCredits;
                        if ($state.current.name === 'home.notifications') {
                            $state.go('home.manage_notifications', {
                                conference_id: $stateParams.conference_id
                            })
                        }

                    }
                }, function(error) {

                })
            }

            var regex = new RegExp(':(' + EmojiGroups.all.join('|') + '):', 'g');
            var regexHex = new RegExp('(' + getUnicodes().join('|') + ')', 'g');

            function imagify(input) {
              if (input == null) {
                return '';
              }

              return input.replace(regex, function (match, text) {
                var className = text.replace(/_/g, '-');
                var output = ['<i class="emoji-picker emoji-', className, '" alt="', text, '" title=":', text, ':"></i>'];

                return output.join('') + " ";
              });
            }

            function getUnicodes() {
              var swappedHex = {};
              var unicodes = [];

              angular.forEach(EmojiHex.emoji, function (value, key) {
                swappedHex[value] = key;
                unicodes.push(value);
              });

              return unicodes.reverse();
            }


            function getSwappedHex() {
              var swappedHex = {};

              angular.forEach(EmojiHex.emoji, function (value, key) {
                swappedHex[value] = key;
              });

              return swappedHex;
            }

            var swappedHex = getSwappedHex();


            function hexify(text) {
              if (text == null) {
                return '';
              }

              var emojiRegex = /\:([a-z0-9_+-]+)(?:\[((?:[^\]]|\][^:])*\]?)\])?\:/g;
              var matches = text.match(emojiRegex);

              if (matches === null) {
                return text;
              }

              for (var i = 0; i < matches.length; i++) {
                var emojiString = matches[i];
                var property = emojiString.replace(/\:/g, '');

                if (EmojiHex.emoji.hasOwnProperty(property)) {
                  text = text.replace(emojiString, EmojiHex.emoji[property]);
                }
              }

              return text;
            }

            function unicodify(text) {
              if (text == null) {
                return '';
              }

              var matches = text.match(regexHex);

              if (matches === null) {
                return text;
              }

              for (var i = 0, len = matches.length; i < len; i++) {
                var hexString = matches[i];

                if (hexString.indexOf('-') > -1) {
                  var codePoints = hexString.split('-');
                  var unicode = eval('String.fromCodePoint(0x' + codePoints.join(', 0x') + ')');
                } else {
                  var codePoint = ['0x', hexString].join('');
                  var unicode = String.fromCodePoint(codePoint);
                }

                text = text.replace(hexString, unicode);
              }

              return text;
            }

            vm.stringifyEmoji = function(text) {
                return swappedHex[hexify(text)];
            }

            checkStatusOfNotificationSetup(ConferenceService.hash_conference_id.decode(vm.conference_id)[0]);

            // end setupNotificationsController

            var linkId = 0;

            // start manageNotificationsController

            vm.verifySendTime = function() {
                if (vm.sendDate < vm.notificationMinTime) {
                    vm.sendDate = vm.notificationMinTime;
                }
            }

            vm.hasTargetSelectorNotification = function() {

                if ((!vm.createdRegistrationLevels || vm.createdRegistrationLevels.length <= 0) &&
                        (!vm.customFieldSendToOptions || vm.customFieldSendToOptions.length <= 0)) {
                    return false;
                }
                return true;
            }

            vm.initNotification = function() {
                vm.linkOptionActive = false;
                vm.selectedTarget = 'session';
                vm.editScheduledNotificationId = null;
                vm.audienceSize = 0;
                vm.necessaryCreditsToCompleteSchedule = 0;
                vm.sendDate = null;
                vm.sendDateEditable = null;
                var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
                setInterval(function(){
                  vm.notificationMinTime = new Date(new Date().getTime() + 5*60000);
                  vm.notificationMinTime.setTime(
                        vm.notificationMinTime.getTime() + (vm.notificationMinTime.getTimezoneOffset() + offsetInMinutes)*60*1000
                  );
                  vm.notificationMinTime.setSeconds(0);
                  vm.notificationMinTime.setMilliseconds(0);
                  vm.notificationMinTimeMoment = moment(vm.notificationMinTime);
                  $scope.$apply();
                }, 60000);

                vm.sendNotificationToAll = false;
                vm.notificationTextContent = "";
                vm.notificationMinTime = new Date(new Date().getTime() + 5*60000);
                vm.notificationMinTime.setTime(
                    vm.notificationMinTime.getTime() + (vm.notificationMinTime.getTimezoneOffset() + offsetInMinutes)*60*1000
                );
                vm.notificationMinTime.setSeconds(0);
                vm.notificationMinTime.setMilliseconds(0);

                vm.notificationMinTimeMoment = moment(vm.notificationMinTime);

                vm.customFieldSendToOptions = [];
                vm.customFieldSendToList = [];
                vm.customFieldOptions = {};
                vm.customFieldKeys = [];

                _.each(vm.conference.custom_fields, function(custom_field) {
                    if (custom_field.type != 1) {
                        vm.customFieldSendToList.push(
                            {
                                "id": custom_field.id,
                                "label": custom_field.name
                            }
                        );

                        vm.customFieldKeys.push('custom_' + custom_field.id);
                        vm.customFieldOptions['custom_' + custom_field.id] = [];
                        _.each(custom_field.options, function(option) {
                            vm.customFieldSendToOptions.push(
                                {
                                    "label": custom_field.name + ': ' + option.value,
                                    "id": option.id,
                                }
                            );

                            vm.customFieldOptions['custom_' + custom_field.id].push(
                                {
                                    "label": option.value,
                                    "id": option.id,
                                }
                            );
                        })
                    }
                });

                $scope.plusBtnLeft = '307px';

                linkId = 0;

                vm.targetSelectors = [
                    {
                        id: 1
                    }
                ];
            }

            vm.initNotification();

            vm.calculateCharacterCounter = function() {
                if (!vm.notificationTextContent) return;
                vm.checkCharacterCounter(vm.notificationTextContent, 140);
            }


            vm.checkCharacterCounter = function(string, maximumLength) {
                vm.characterCount = _.size(string);

                if (_.size(string) <= maximumLength) {
                    var leftCharacters = maximumLength - _.size(string);

                    if (leftCharacters < 10) {
                        vm.lessThanTenCharactersLeft = true;
                    } else {
                        vm.lessThanTenCharactersLeft = false;
                    }
                } else {
                    var exceedCharactersCount = maximumLength - _.size(string);
                    string = string.slice(0, exceedCharactersCount);

                    vm.notificationTextContent = string;
                    vm.characterCount = _.size(string);
                    vm.lessThanTenCharactersLeft = true;
                }
            }

            vm.changeLinkType = function() {
                if (vm.selectedTarget == 'session') {
                    vm.selectedTargetItemId = vm.sessions[0].id;
                } else if (vm.selectedTarget == 'exhibitor') {
                    vm.selectedTargetItemId = vm.exhibitors[0].id;
                } else {
                    vm.selectedTargetItemId = vm.conferenceSpeakersList.speakers[0].id;
                }
                linkId = vm.selectedTargetItemId;
            }
            vm.checkLinkOption = function(optionState) {
                if (vm.sessions[0]) {
                    vm.selectedTargetItemId = vm.sessions[0].id;
                } else if (vm.exhibitors[0]) {
                    vm.selectedTarget = 'exhibitor';
                    vm.selectedTargetItemId = vm.exhibitors[0].id
                } else if (vm.conferenceSpeakersList.speakers[0]) {
                    vm.selectedTarget = 'speaker';
                    vm.selectedTargetItemId = vm.conferenceSpeakersList.speakers[0].id;
                }
                vm.linkOptionActive = !optionState;
                linkId = vm.selectedTargetItemId;
            }

            vm.addNewTargetSelector = function() {
                var newObject = {
                    id: vm.targetSelectors.length + 1
                };
                $scope.plusBtnLeft = '307px';
                vm.targetSelectors.push(newObject)
            }

            vm.removeNewTargetSelector = function(target) {
                angular.forEach(vm.targetSelectors, function(targetSelector, index) {
                    if (target.$$hashKey === targetSelector.$$hashKey) {
                        vm.targetSelectors.splice(index, 1);
                        vm.requestCost(vm.targetSelectors);
                    }
                });

                if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'registration_level') {
                    $scope.plusBtnLeft = '436px';
                }

                if (vm.customFieldKeys.indexOf(vm.targetSelectors[vm.targetSelectors.length - 1].sendToType) > -1) {
                    $scope.plusBtnLeft = '436px';
                }

                if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'custom_field_value') {
                    $scope.plusBtnLeft = '436px';
                }
            }

            vm.processPushNotificationPayments = function(transactions) {
                var payments = [];

                _.each(transactions, function(transaction) {
                    payments.push({
                        'checked': transaction.checked,
                        'Date': $filter('date')(transaction.created_at, "MM/dd/yyyy - hh:mm a"),
                        'Order': transaction.nr_credits + " Push Notification Credits",
                        'Last 4': transaction.payment.last4,
                        'Amount': $filter('currency')(transaction.payment.amount / 100, '$')
                    });
                });

                return payments;
            }

            vm.downloadPushPaymentsAsCsv = function(transactions) {
                lib.downloadTransactionsAsCsv(vm.processPushNotificationPayments(transactions), vm.conference);
            }

            vm.printPaymentsAsCsv = function(transactions) {
                lib.printTransactionsAsCsv(vm.processPushNotificationPayments(transactions), vm.conference);
            }

            vm.openPushNotificationPopup = function() {
                $scope.checkOne = vm.checkOne;
                $scope.checkAll = vm.checkAll;
                $scope.checkAllBool = vm.checkAllPushNotificationPayments;
                $scope.printTransactionsAsCsv = vm.printPaymentsAsCsv;
                $scope.downloadTransactionsAsCsv = vm.downloadPushPaymentsAsCsv;
                $scope.conferencePushNotificationPayments = vm.conferencePushNotificationPayments;
                ngDialog.open({template: 'app/views/partials/conference/custom_push_notification_payments.html',
                   className: 'app/stylesheets/_createLabelModal.scss',
                   scope: $scope
                });
            }

            vm.conferencePushNotificationPayments = null;
            vm.loadPushNotificationPayments = function() {
                if (vm.conferencePushNotificationPayments) {
                    vm.openPushNotificationPopup();
                } else {
                    manageNotificationsService.getPayments(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            vm.conferencePushNotificationPayments = response.data.custom_push_notification_payments;
                            vm.openPushNotificationPopup();
                        }).catch(function(error){

                        });
                }

            }

            vm.showTargetSelectorNotification = function(){
                if (vm.targetSelectors && vm.targetSelectors.length <= 0) {
                    return true;
                }
                var showLevel = vm.showNotificationLevelOption(vm.targetSelectors[vm.targetSelectors.length - 1]);
                var showOption = vm.showNotificationFieldOption(vm.targetSelectors[vm.targetSelectors.length - 1]);
                if (!showLevel && !showOption) {
                    return false;
                }

                return true;
            }

            vm.canAddTargetSelectorNotification = function() {
                if (vm.targetSelectors && vm.targetSelectors.length <= 0) {
                    return true;
                }

                var showLevel = vm.hasRemainingLevel();
                var showOption = vm.hasRemainingOptions();
                if (!showLevel && !showOption) {
                    return false;
                }


                if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'registration_level') {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1].selectedLevel) {
                        return true;
                    }
                }

                if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'custom_field_value') {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1].selectedOption) {
                        return true;
                    }
                }

                if (vm.customFieldKeys.indexOf(vm.targetSelectors[vm.targetSelectors.length - 1].sendToType) > -1) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1].selectedOption) {
                        return true;
                    }
                }

                if (vm.customFieldKeys.indexOf(vm.targetSelectors[vm.targetSelectors.length - 1].sendToType) > -1) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1].selectedOption) {
                        return true;
                    }
                }
            }



            vm.scheduleNotification = function(notificationText, time, receivers) {
                if (!vm.sendDateEditable) {
                    toastr.error('You need to set a date for the notification!');
                    return;
                }

                var sendDate = new Date(vm.sendDateEditable);
                var objectToSend = {
                    text: unicodify(hexify(notificationText)),
                    send_at: sendDate,
                    send_at_raw: {
                        "year": sendDate.getFullYear(),
                        "month": sendDate.getMonth() + 1,
                        "day": sendDate.getDate(),
                        "hour": sendDate.getHours(),
                        "minute": sendDate.getMinutes(),
                    },
                    id: vm.editScheduledNotificationId,
                    custom_push_send_to: [],
                    send_to_all: false
                };

                if (!objectToSend.text || objectToSend.length < 5) {
                    toastr.error('The notification message should have at least 5 characters!');
                    return;
                }

                if (!objectToSend.send_at_raw) {
                    toastr.error('You need to set a date for the notification!');
                    return;
                }

                angular.forEach(receivers, function(receiver) {
                    if (receiver.sendToType == 'all_attendees') {
                        objectToSend.send_to_all = true;
                    }
                    else if (receiver.selectedLevel) {
                        var serverSideTypeObject = {
                            send_type: 'registration_level',
                            send_resource_id: parseInt(receiver.selectedLevel)
                        }
                        objectToSend.custom_push_send_to.push(serverSideTypeObject);
                    } else if (receiver.selectedOption) {
                        var serverSideTypeObject = {
                            send_type: 'custom_field_value',
                            send_resource_id: parseInt(receiver.selectedOption)
                        }
                        objectToSend.custom_push_send_to.push(serverSideTypeObject);
                    }

                });

                if (vm.sendNotificationToAll) {
                    objectToSend.send_to_all = true;
                }

                if (objectToSend.custom_push_send_to.length == 0 && !objectToSend.send_to_all) {
                    toastr.error("You need to select at least one 'Send To' option!");
                    return;
                }

                if (vm.linkOptionActive) {
                    objectToSend.link_type = vm.selectedTarget;
                    objectToSend.link_id = linkId;
                }

                validateNotificationContent(objectToSend);


                if (vm.moreCreditsAreNecessary) {
                    toastr.error('Please buy more credits to schedule this notification.')
                } else {
                    manageNotificationsService.scheduleNotification(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], objectToSend).then(function(response) {

                        var notification = response.data;
                        vm.characterCount = 0;
                        vm.lessThanTenCharactersLeft = false;
                        vm.errorOnText = null;
                        notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a');

                        if (objectToSend.id) {
                            _.each(vm.scheduledNotifications, function(value, key, obj) {
                                if (value.id === objectToSend.id) {
                                    vm.scheduledNotifications[key] = notification;
                                    toastr.success('The scheduled custom push notification was updated!');
                                }
                            });
                        } else {
                            notification.nr_sent = response.cost;
                            notification.nr_seen = 0;
                            vm.usedCreditsPercentage = 0;
                            vm.totalCredits -= vm.audienceSize;
                            vm.remainingCredits = vm.totalCredits;
                            vm.audienceSize = 0;
                            vm.scheduledNotifications.push(notification);
                            vm.scheduledNotifications = _.orderBy(vm.scheduledNotifications, 'send_at', 'desc');
                            if (!notification.was_processed) {
                                $timeout(function() {
                                  vm.checkNotificationStatus(notification);
                                }, 5000);
                            }
                            toastr.success('The custom push notification was scheduled!');
                        }

                        vm.editScheduledNotificationId = null;
                        vm.initNotification();

                    }, function(error) {
                        console.log(error);
                    });
                }

            }

            vm.deleteNotificationConfirmed = function(notification) {
                var conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                manageNotificationsService.removeScheduledNotification(conference_id, notification)
                    .then(function(response){
                        vm.scheduledNotifications  = _.reject(vm.scheduledNotifications, ['id', notification.id]);
                    }).catch(function(error){

                    });
            }

            vm.cancelEditNotification = function() {
                vm.initNotification();
            }

            vm.hasRemainingOptions = function() {
                if (!vm.targetSelectors || !vm.createdRegistrationLevels) {
                    return true;
                }

                var doNotShow = false;
                var levelIds = [];
                _.each(vm.targetSelectors, function(savedSelector) {
                    if (vm.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
                        if (savedSelector.selectedOption) {
                            levelIds.push(savedSelector.selectedOption);
                        }
                    }
                });
                if (levelIds.length == vm.customFieldSendToOptions.length) {
                    return false;
                }
                return true;
            }

            vm.hasRemainingLevel = function() {
                if (!vm.targetSelectors || !vm.createdRegistrationLevels) {
                    return true;
                }
                var doNotShow = false;
                var levelIds = [];
                _.each(vm.targetSelectors, function(savedSelector) {
                    if (savedSelector.sendToType == 'registration_level') {
                        if (savedSelector.selectedLevel && savedSelector.selectedLevel) {
                            levelIds.push(savedSelector.selectedLevel);
                        }
                    }

                });

                if (levelIds.length == vm.createdRegistrationLevels.length) {
                    return false;
                }

                return true;
            }

            vm.showAllNotificationOption = function(targetSelector) {
                if (!vm.targetSelectors) {
                    return false;
                }

                var doNotShow = false;
                var levelIds = [];
                _.each(vm.targetSelectors, function(savedSelector) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector) {
                        if (savedSelector.sendToType == 'registration_level') {
                            if (savedSelector.selectedLevel && savedSelector.selectedLevel) {
                                levelIds.push(savedSelector.selectedLevel);
                            }
                        }
                    }

                    if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector) {
                        if (vm.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
                            if (savedSelector.selectedOption) {
                                levelIds.push(savedSelector.selectedOption);
                            }
                        }
                    }


                });

                if (levelIds.length > 0) {
                    return false;
                }

                return true;
            }

            vm.showNotificationLevelOption = function(targetSelector) {
                if (!vm.targetSelectors || !vm.createdRegistrationLevels) {
                    return false;
                }

                if (targetSelector.sendToType && targetSelector.selectedLevel) {
                    return true;
                }

                var doNotShow = false;
                var levelIds = [];
                _.each(vm.targetSelectors, function(savedSelector) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector || !targetSelector.selectedLevel) {
                        if (savedSelector.sendToType == 'registration_level') {
                            if (savedSelector.selectedLevel && savedSelector.selectedLevel) {
                                levelIds.push(savedSelector.selectedLevel);
                            }
                        }
                    }
                });

                if (levelIds.length == vm.createdRegistrationLevels.length) {
                    return false;
                }

                return true;
            }



            vm.showNotificationLevel = function(targetSelector, level) {
                var levelIds = [];
                var found = false;
                if (targetSelector.sendToType && targetSelector.selectedLevel) {
                    if (level.id == targetSelector.selectedLevel) {
                        found = true;
                    }

                    if (found) {
                        return found;
                    }
                }

                if (targetSelector == vm.targetSelectors[vm.targetSelectors.length - 1] || !found) {
                    var doNotShow = false;
                    _.each(vm.targetSelectors, function(savedSelector) {
                        if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector || !found) {
                            if (savedSelector.sendToType == 'registration_level') {
                                if (savedSelector.selectedLevel && savedSelector.selectedLevel == level.id) {
                                    doNotShow = true;
                                }
                            }
                        }

                    });

                    return !doNotShow;
                } else {
                    return true;
                }

            }

            vm.showNotificationOption = function(targetSelector, option) {

                if (targetSelector == vm.targetSelectors[vm.targetSelectors.length - 1]) {
                    var doNotShow = false;
                    _.each(vm.targetSelectors, function(savedSelector) {
                        if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector) {
                            if (savedSelector.sendToType == 'custom_field_value') {
                                if (savedSelector.selectedOption && savedSelector.selectedOption == option.id) {
                                    doNotShow = true;
                                }
                            }
                        }

                    });

                    return !doNotShow;
                } else {
                    return true;
                }

            }

            vm.showNotificationCustomOption = function(targetSelector, option) {

                var levelIds = [];
                var found = false;
                if (targetSelector.sendToType && targetSelector.selectedOption) {
                    if (option.id == targetSelector.selectedOption) {
                        found = true;
                    }

                    if (found) {
                        return found;
                    }
                }

                if (targetSelector == vm.targetSelectors[vm.targetSelectors.length - 1] || !found) {
                    var doNotShow = false;
                    _.each(vm.targetSelectors, function(savedSelector) {
                        if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector || !found) {
                            if (vm.customFieldKeys.indexOf(targetSelector.sendToType) > -1) {
                                if (savedSelector.selectedOption && savedSelector.selectedOption == option.id) {
                                    doNotShow = true;
                                }
                            }
                        }

                    });

                    return !doNotShow;
                } else {
                    return true;
                }

            }

            vm.showSendToOption = function(targetSelector) {
                if (!vm.showNotificationFieldOption(targetSelector) && !vm.showNotificationLevelOption(targetSelector)) {
                    return false;
                }

                return true;
            }

            vm.showNotificationFieldOption = function(targetSelector) {
                if (!vm.targetSelectors || !vm.customFieldSendToOptions) {
                    return true;
                }

                var doNotShow = false;
                var levelIds = [];
                _.each(vm.targetSelectors, function(savedSelector) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector) {
                        if (savedSelector.sendToType == 'custom_field_value') {
                            if (savedSelector.selectedOption) {
                                levelIds.push(savedSelector.selectedOption);
                            }
                        }
                    }
                });
                if (levelIds.length == vm.customFieldSendToOptions.length) {
                    return false;
                }

                return true;
            }

            vm.showCustomFieldSendToOption = function(targetSelector, custom_field) {
                var sendToType = 'custom_' + custom_field.id;
                if (!vm.targetSelectors
                        || !vm.customFieldSendToOptions
                        || !vm.customFieldOptions[sendToType]) {
                    return true;
                }

                var levelIds = [];
                var found = false;
                if (targetSelector.sendToType && targetSelector.selectedOption) {
                    _.each(vm.customFieldOptions[sendToType], function(item) {
                        if (item.id == targetSelector.selectedOption) {
                            found = true;
                        }
                    });

                    if (found) {
                        return found;
                    }
                }

                _.each(vm.targetSelectors, function(savedSelector) {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1] != savedSelector  || !found) {
                        if (vm.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
                            if (savedSelector.selectedOption) {
                                _.each(vm.customFieldOptions[sendToType], function(item) {
                                    if (item.id == savedSelector.selectedOption) {
                                        levelIds.push(savedSelector.selectedOption);
                                    }
                                });
                            }
                        }
                    }
                });

                if (levelIds.length == vm.customFieldOptions[sendToType].length) {
                    return false;
                }

                return true;
            }

            vm.editScheduledNotification = function(notification) {
                vm.notificationTextContent = notification.text;
                vm.targetSelectors = [];
                vm.editScheduledNotificationId = notification.id;
                vm.sendNotificationToAll = false;
                if (notification.send_to_all) {
                    vm.targetSelectors.push(
                        {
                            sendToType: 'all_attendees',
                            selectedLevel: null
                        }
                    );

                    vm.sendNotificationToAll = true;
                } else {
                    _.each(notification.custom_push_send_to, function(send_to_item) {
                        if (send_to_item.send_type == 'registration_level') {
                            vm.targetSelectors.push(
                                {
                                    sendToType: 'registration_level',
                                    selectedLevel: send_to_item.send_resource_id
                                });
                        } else if (send_to_item.send_type == 'custom_field_value') {
                            var customFieldItem = null;
                            _.each(vm.conference.custom_fields, function(custom_field) {
                                if (custom_field.type != 1) {
                                    _.each(custom_field.options, function(option) {
                                        if (option.id === send_to_item.send_resource_id) {
                                            customFieldItem = custom_field;
                                        }
                                    })
                                }
                            });
                            vm.targetSelectors.push(
                                {
                                    sendToType: 'custom_' + customFieldItem.id,
                                    selectedOption: send_to_item.send_resource_id
                                });
                        }
                    });
                }


                if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'registration_level'
                        ||
                    vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'custom_field_value'
                        ||
                    vm.customFieldKeys.indexOf(vm.targetSelectors[vm.targetSelectors.length - 1].sendToType) > -1) {
                    $scope.plusBtnLeft = '436px';
                } else {
                    $scope.plusBtnLeft = '307px';
                }

                vm.sendDate = new Date(notification.send_at);

                // vm.sendDateEditable = moment(new Date(
                //     notification.send_at_raw.year,
                //     notification.send_at_raw.month - 1,
                //     notification.send_at_raw.day,
                //     notification.send_at_raw.hour,
                //     notification.send_at_raw.minute
                // ));

                vm.sendDateEditable = vm.createMomentFromRawTime(notification.send_at_raw);

                if (notification.link_type != 'none') {
                    vm.linkOptionActive = true;
                    vm.selectedTarget = notification.link_type;
                    vm.selectedTargetItemId = notification.link_id;
                }

                $("#notifications-main-view").animate({
                  scrollTop: 0
                }, 400);
            }

            vm.removeScheduledNotification = function(notification) {
                $scope.notification = notification;
                $scope.deleteNotificationConfirmed = vm.deleteNotificationConfirmed;
                ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_notification.html',
                      className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                      scope: $scope
                });
            }
            $scope.plusBtnLeft = '307px';
            vm.selectSendTotType = function(targetSelector) {
                if (targetSelector.sendToType != 'all_attendees') {
                    if (vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'registration_level'
                            ||
                        vm.targetSelectors[vm.targetSelectors.length - 1].sendToType == 'custom_field_value'
                            ||
                        vm.customFieldKeys.indexOf(vm.targetSelectors[vm.targetSelectors.length - 1].sendToType) > -1) {
                        $scope.plusBtnLeft = '436px';
                    } else {
                        $scope.plusBtnLeft = '307px';
                    }
                } else {
                    if (vm.targetSelectors.length > 1) {
                        vm.targetSelectors.splice(-1,1);
                    }
                    $scope.plusBtnLeft = '307px';
                    vm.requestCostAll()
                }
            }

            vm.requestCostAll = function(levels) {
                var objectToSend = {send_to_all: true};

                manageNotificationsService.calculateCost(ConferenceService.hash_conference_id.decode(vm.conference_id)[0],objectToSend).then(function(cost) {
                    vm.remainingCredits = vm.totalCredits - cost;
                    vm.audienceSize = cost;
                    vm.usedCreditsPercentage = 100 - (vm.remainingCredits/vm.totalCredits * 100);

                    if (vm.remainingCredits < 0) {
                        vm.moreCreditsAreNecessary = true;
                        vm.necessaryCreditsToCompleteSchedule = vm.necessaryCreditsToCompleteSchedule - vm.remainingCredits;
                        vm.remainingCredits = 0;
                    }
                })
            }

            vm.requestCost = function(levels) {
                var objectToSend = {custom_push_send_to: []};

                angular.forEach(levels, function(level) {
                    if (level.selectedLevel) {
                        var serverSideTypeObject = {
                            send_type: 'registration_level',
                            send_resource_id: parseInt(level.selectedLevel)
                        }
                    } else if (level.selectedOption) {
                        var serverSideTypeObject = {
                            send_type: 'custom_attendee_field_option',
                            send_resource_id: parseInt(level.selectedOption)
                        }
                    }
                    if (serverSideTypeObject) {
                        objectToSend.custom_push_send_to.push(serverSideTypeObject);
                    }
                });

                manageNotificationsService.calculateCost(ConferenceService.hash_conference_id.decode(vm.conference_id)[0],objectToSend).then(function(cost) {
                    vm.remainingCredits = vm.totalCredits - cost;
                    vm.audienceSize = cost;
                    vm.usedCreditsPercentage = 100 - (vm.remainingCredits/vm.totalCredits * 100);

                    if (vm.remainingCredits < 0) {
                        vm.moreCreditsAreNecessary = true;
                        vm.necessaryCreditsToCompleteSchedule = vm.necessaryCreditsToCompleteSchedule - vm.remainingCredits;
                        vm.remainingCredits = 0;
                    }
                })
            }

            vm.checkSelectedTarget = function(id, $event) {
                linkId = id;
                vm.selectedTargetItemId = id;
                $($event.target).addClass('active');
                $($event.target).siblings().removeClass('active');
            }

            vm.buyMoreCredits = function(creditsNumber) {
                var ceiledCreditsNumber = Math.ceil(creditsNumber/ 1000.0) * 1000;
                var price = calculatePriceForNotificationsCredits(ceiledCreditsNumber);

                ngDialog.open({
                    plain: true,
                    template: "<credit-card-modal></credit-card-modal>",
                    className: 'app/stylesheets/_plusAttendee.scss',
                    data: {
                        amountOfCredits: ceiledCreditsNumber,
                        finalPrice: price
                    }
                });
            }

            var getRegistrationLevels = function(conferenceId) {
                manageNotificationsService.getRegistrationLevels(conferenceId).then(function(response) {
                    vm.createdRegistrationLevels = response;
                    vm.initNotification()
                })
            };

            var getScheduledNotifications = function(conferenceId) {
                manageNotificationsService.getScheduledNotifications(conferenceId).then(function(response) {
                    vm.scheduledNotifications = response.custom_push_notifications;

                    vm.scheduledNotifications = _.orderBy(vm.scheduledNotifications, 'send_at', 'desc');

                    setInterval(function(){
                        vm.remainingScheduledNotification = [];

                        _.each(vm.scheduledNotifications, function(notification) {
                            if (new Date(notification.send_at) < new Date()) {
                                var conferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                                manageNotificationsService.getScheduledNotification(conferenceId, notification)
                                    .then(function(response) {
                                        notification = response.data;
                                        notification.nr_sent = notification.cost;
                                        notification.nr_seen = 0;
                                        vm.sentNotifications.push(notification);
                                    }).catch(function(error) {
                                        console.log(error);
                                    });

                            } else {
                                vm.remainingScheduledNotification.push(notification);
                            }
                        });
                        vm.scheduledNotifications = vm.remainingScheduledNotification;
                        $scope.$apply();
                    }, 60000)

                    angular.forEach(vm.scheduledNotifications, function(notification) {
                        if (!notification.was_processed) {
                            $timeout(function() {
                              vm.checkNotificationStatus(notification);
                            }, 5000);
                        }
                        notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a');
                    })
                })
            };

            var getSentNotifications = function(conferenceId) {
                manageNotificationsService.getSentNotifications(conferenceId).then(function(response) {
                    vm.sentNotifications = response.custom_push_notifications;
                    angular.forEach(vm.sentNotifications, function(notification) {
                        notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a')
                    })
                })
            }

            var validateNotificationContent = function (notification) {
                var isText = false;
                var sendAtDateIsAfterToday = false;
                var sendToExists = false;

                var hasLinkTarget = false;


                if (!notification.text) {
                    isText = false;
                } else {
                    isText = true;
                }
                vm.errorOnText = !isText;

                if (!moment(notification.send_at).isAfter(moment())) {
                    sendAtDateIsAfterToday = false;
                } else {
                    sendAtDateIsAfterToday = true;
                }

                vm.errorOnDate = !sendAtDateIsAfterToday;
            }

            getRegistrationLevels(ConferenceService.hash_conference_id.decode(vm.conference_id)[0]);
            getScheduledNotifications(ConferenceService.hash_conference_id.decode(vm.conference_id)[0]);
            getSentNotifications(ConferenceService.hash_conference_id.decode(vm.conference_id)[0]);


            $rootScope.$on('BOUGHT_CREDITS', function(event, data) {
                vm.totalCredits = vm.totalCredits + data.numberOfCredits;
                vm.remainingCredits = vm.remainingCredits + data.numberOfCredits;

                vm.usedCreditsPercentage = 100 - (vm.remainingCredits/vm.totalCredits * 100);
                if (vm.remainingCredits < 0) {
                    vm.moreCreditsAreNecessary = true;
                    vm.necessaryCreditsToCompleteSchedule = vm.necessaryCreditsToCompleteSchedule - vm.remainingCredits;
                    vm.remainingCredits = 0;
                } else {
                    vm.moreCreditsAreNecessary = false;
                    vm.necessaryCreditsToCompleteSchedule = 0;
                }
            })

            // end manageNotificationsController
            vm.setRecommendedRegistrationLevel = function(currentLevelItem) {
                var data = {
                    "recommended_level": currentLevelItem.id,
                    "recommandation_reason": null
                }
                if (vm.conference.registration_form.recommended_level == currentLevelItem.id) {
                    data = {
                        "recommended_level": null,
                        "recommandation_reason": null
                    }
                }

                ConferenceRegistrationService.registrationFormRecommendedLevel(vm.registration_form_id, data)
                    .then(function(response){
                        vm.conference.registration_form.recommended_level = response.recommended_level;
                    })
                    .catch(function(error){
                        toastr.error("There was a problem with your request");
                    });
            }

            vm.updateRecommandationLabel = function() {
                var data = {
                    "recommandation_reason": vm.conference.registration_form.recommandation_reason
                }


                ConferenceRegistrationService.registrationFormRecommendedLevel(vm.registration_form_id, data)
                    .then(function(response){
                        console.log(response);
                    })
                    .catch(function(error){
                        toastr.error("There was a problem with your request");
                    });
            }
						vm.getAttendeeMaterials = function() {
							var conferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];

							ConferenceAttendeesService.getAttendeeMaterials(conferenceId).then(function(response) {
								vm.attendee_materials = response;
							}, function(error) {
								toastr.error("An error has occured. Please try again.")
							});
						}

						vm.uploadAttendeeMaterial = function(file) {
							vm.attendeeMaterialFile = file[0];
						}

						vm.uploadAttendeeMaterialConfirm = function() {
							if (!vm.attendeeMaterialFile) return;

							FileService.getUploadSignature(vm.attendeeMaterialFile)
									.then(function(response){
											var file_link = response.data.file_link;
											var promises = [];

											var data = {
													conference_id: ConferenceService.hash_conference_id.decode(vm.conference_id)[0],
													file_url: file_link,
													name: vm.attendeeMaterialFile.name,
													type: vm.attendeeMaterialFile.type
											}

											promises.push(FileService.uploadFile(vm.attendeeMaterialFile, response));
											promises.push(ConferenceAttendeesService.addAttendeeMaterial(data));

											$q.all(promises).then(function(response){
													vm.attendee_materials.push(response[1]);
													vm.attendeeMaterialFile = null;
											}).catch(function(error){

											});

									})
									.catch(function(error){

											if (error.status == 403) {
													toastr.error('Permission Denied!');
											} else {
													toastr.error('Error!', 'There has been a problem with your request.');
											}
									});
						}

						vm.removeAttendeeMaterial = function(attendee_material) {
							var conferenceId = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
							ConferenceAttendeesService.removeAttendeeMaterial(conferenceId, attendee_material.id)
									.then(function(response){
											var index = vm.attendee_materials.indexOf(attendee_material);
											if (index > -1) {
													vm.attendee_materials.splice(index, 1);
											}
									})
									.catch(function(error){
											if (error.status == 403) {
													toastr.error('Permission Denied!');
											} else {
													toastr.error('Error!', 'There has been a problem with your request.');
											}
									});
						}
		}

        function initiateCustomFieldsData(vm) {

            vm.initAttendeeFields = presets.initAttendeeFields();
            vm.removableAttendeeFields = presets.removableAttendeeFields();
            vm.editAttendeeFields = presets.editAttendeeFields();
            vm.small_form_for_travel = presets.smallFormForTravel();
            vm.large_form_for_travel = presets.largeFormForTravel();
            vm.attendeeFields = [
                {
                    id: -1,
                    code: 'full_name',
                    name: 'Attendee'
                }
            ].concat(vm.removableAttendeeFields);

            vm.memberMappingFields = presets.memberMappingFields();
            vm.attendeeMappingFields = presets.attendeeMappingFields();
            vm.attendeeMappingFields2 = presets.attendeeMappingFields2();
            vm.attendeeMappingFields = vm.attendeeMappingFields2.concat(vm.removableAttendeeFields);
            vm.exhibitorMappingFields = presets.exhibitorMappingFields();
            vm.tempAttendeeFields = presets.tempAttendeeFields();
            vm.sessionMappingFields = presets.sessionInputFields();
        }


})();
