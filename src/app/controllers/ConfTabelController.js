(function () {
	'use strict'
	angular
		.module('app')
		.controller('ConfTabelController', ['$scope', '$http', 'ConferenceService', 'SharedProperties', 'conferences', 'localStorageService', confTabelController]);
	function confTabelController($scope, $http, ConferenceService, SharedProperties, conferences, localStorageService) {
		var vm = this,
			sharedData = SharedProperties.sharedObject;
		sharedData.displayConference = true;
		sharedData.currentPageTitle = 'dashboard';
		SharedProperties.conferenceSearchQuery = '';

		vm.upcomingConferences = [];
		vm.previousConferences = [];
		vm.conferences = conferences.conferences;

		var conferencesAllPage = 1;

		vm.orderDesc = true;

		vm.isSuperAdmin = localStorageService.get('is_superuser');


		vm.formatRole = function (role, user_role, upcoming) {
			if (upcoming) {
				if (role == 'admin' && user_role == 'admin') {
					return 'Conference Owner';
				}
				else if (user_role == 'conference_user') {
					return 'Conference User';
				}
				else if (role == 'exhibitor') {
					return 'Exhibitor';
				}
				else if (role == "both") {
					return 'Exhibitor & Conference Owner';
				}
			} else {
				if (role == 'admin' && user_role == 'admin') {
					return 'Conference Owner';
				}
				else if (user_role == 'conference_user') {
					return 'Conference User';
				}
				else if (role == 'exhibitor') {
					return 'Exhibitor';
				}
				else if (role == "both") {
					return 'Exhibitor & Conference Owner';
				}
			}
		}

		if (conferences) {
			SharedProperties.conference = null;
			_.each(conferences.conferences, function (conference) {
				var now = new Date();
				if (!conference.time_zone) {
					conference.time_zone = '-0500'; // New york time
				} else if ((conference.time_zone[0] != '-') && (conference.time_zone[0] != '+')) {
					conference.time_zone = '+' + conference.time_zone;
				}
				conference.conference_id = ConferenceService.hash_conference_id.encode(conference.conference_id);
				var conference_date = new Date(conference.date_to);
				conference_date.setDate(conference_date.getDate() + 1);
				if (now <= conference_date) {
					vm.upcomingConferences.push(conference);
				} else {
					vm.previousConferences.push(conference);
				}
			});
			vm.previousConferences = _.orderBy(vm.previousConferences, 'date_from', 'desc');
		}

		vm.sortByDate = function () {
			if (vm.orderDesc) {
				vm.previousConferences = _.orderBy(vm.previousConferences, ['date_from', 'date_to'], ['asc', 'asc']);
				vm.conferences = _.orderBy(vm.conferences, ['date_from', 'date_to'], ['asc', 'asc']);

				vm.orderDesc = false;
			} else {
				vm.previousConferences = _.orderBy(vm.previousConferences, ['date_from', 'date_to'], ['desc', 'desc']);
				vm.conferences = _.orderBy(vm.conferences, ['date_from', 'date_to'], ['desc', 'desc']);

				vm.orderDesc = true;
			}
		};

		vm.goToNewTab = function() {
			alert('goto');
		}

		vm.render = function () {
			if (vm.upcomingConferences.length === 0) {
				vm.tabFilled = false;
				vm.tabEmpty = true;
			} else {
				vm.tabFilled = true;
				vm.tabEmpty = false;
			}

			if (vm.previousConferences.length === 0) {
				vm.tabFilled2 = false;
				vm.tabEmpty2 = true;
			} else {
				vm.tabFilled2 = true;
				vm.tabEmpty2 = false;
			}
		};

		setTimeout(function () {
			$('.grid-table-super-admin > table > tbody').on('scroll', function () {
				if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
					conferencesAllPage++;
					ConferenceService
						.getAllConferences(conferencesAllPage, SharedProperties.conferenceSearchQuery)
						.then(function (conferences) {
							angular.forEach(conferences.conferences, function(conference) {
								conference.conference_id = ConferenceService.hash_conference_id.encode(conference.conference_id)
							})

							vm.conferences.push.apply(vm.conferences, conferences.conferences);
							vm.conferences = _.uniqBy(vm.conferences, 'conference_id');
						});
				}
			})
		}, 300)

		$scope.$watch(function(){
            return SharedProperties.conferenceSearchQuery;
            }, function(image){
            	conferencesAllPage = 0;
                ConferenceService
					.getAllConferences(conferencesAllPage, SharedProperties.conferenceSearchQuery)
					.then(function (conferences) {

						angular.forEach(conferences.conferences, function(conference) {
							conference.conference_id = ConferenceService.hash_conference_id.encode(conference.conference_id)
						})
						vm.conferences = conferences.conferences;
					});
        });


	}
})();
