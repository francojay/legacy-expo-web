(function(){
	'use strict';
		angular
			.module('app')
			.controller('conferenceRegistrationController2', ['$scope', '$state', '$stateParams', 'ngDialog',
			'ConferenceAttendeesService', 'ConferenceRegistrationService', 'FileService', '$q', '$window', 'toastr', conferenceRegistrationController2]);

			function conferenceRegistrationController2($scope, $state, $stateParams, ngDialog, ConferenceAttendeesService, ConferenceRegistrationService,
				FileService, $q, $window, toastr) {
				var vm = this;
				vm.getConferenceRegistrationForm = function(conference_id) {
					console.log(conference_id);
					ConferenceRegistrationService.getRegistrationForm(conference_id)
						.then(function(response){
							console.log(response);
						})
						.catch(function(error){
							console.log(error);
						})
				}
				vm.conference_id = $stateParams.conference_id;
				vm.getConferenceRegistrationForm(vm.conference_id);
			}
})();
