(function(){
	'use strict';
		angular
			.module('app')
			.controller('ConferenceExhibitorsController', ['$scope', '$state', '$stateParams', 'ngDialog',
			'ConferenceExhibitorsService', 'ConferenceUsersService', 'ConferenceService', 'FileService', '$q', 'toastr', 'API', conferenceExhibitorsService]);

			function conferenceExhibitorsService(
			    $scope, $state, $stateParams, ngDialog, ConferenceExhibitorsService, ConferenceUsersService, ConferenceService,
			FileService, $q, toastr, API){
				Stripe.setPublishableKey(API.STRIPE_KEY);
				var vm = this;
				vm.conference_id = $stateParams.conference_id;
				vm.editModeAttendee = false;
				vm.chka = vm.chkr = false;

				vm.exhibitor = _.cloneDeep($scope.$parent.infoExhibitor);
                vm.exhibitorToEdit = _.cloneDeep($scope.$parent.exhibitorToEdit);
                vm.conferenceExhibitorModel = {
                    'name' : '',
                    'booth_contact_first_name' : '',
                    'booth_contact_last_name' : '',
                    'booth' : '',
                    'booth_contact_email' : ''
                };
				vm.conference = $scope.$parent.conference;
				vm.promo_codes = $scope.$parent.promo_codes;
                vm.updateUser = $scope.$parent.updateUser;

				vm.paymentStep = 1;
				vm.buyUsersPaymentStep = 1;

                vm.newSupportContact = {};

                vm.contactLabels = [
                    {label:'First Name', code: 'first_name'},
                    {label:'Last Name', code: 'last_name'},
                    {label:'Company Name', code: 'company_name'},
                    {label:'Email Address', code: 'email'},
                    {label:'Phone Number', code: 'phone_number'},
                    {label:'Area of Support', code: 'area_of_support'}
                ];

                vm.eraseInputs = function(){
                   vm.newSupportContact = {};
                };

				vm.testIfPromoCodeChecked = function() {
                    var checkedPromoCodesList = [];

                    _.each(vm.promo_codes, function(promo_code) {
                        if (checkedPromoCodesList.checked == true) {
                            checkedUsersList.push(user);
                        }
                    });


                    if (checkedPromoCodesList.length > 0) {
                        vm.promoCodeChecked = true;
                    }
                    else {
                        vm.promoCodeChecked = false;
                    }
                }

                function stripeResponseHandlerTempExhibitor(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    var token = response.id;

                    ConferenceExhibitorsService.exhibitorAddedUserPayNew($scope.$parent.exhibitor, token, vm.nrUsers)
                        .then(function(response){
                            $scope.$parent.updateUsersSlotsCount(vm.nrUsers);
                            vm.closeFunction();
                            console.log(response);
                        })
                        .catch(function(error){
                            if (error.status == 400) {
                                toastr.error(error.data.detail);
                            } else {
                                toastr.error(error.data);
                            }
                            console.log(error);
                        });
                  }
                };

                vm.addConferenceExhibitor = function(type){
                    var ok = true;
                    if (!vm.conferenceExhibitorModel.booth) {
                        toastr.error('Booth is required!');
                        ok=false;
                    }

                    if (!vm.conferenceExhibitorModel.name) {
                        toastr.error('Company name is required!');
                        ok=false;
                    }
                    if(ok == true){
                        ConferenceExhibitorsService
                            .uploadExhibitors(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], [vm.conferenceExhibitorModel])
                            .then(function(response){
                                vm.conferenceExhibitorModel = {
                                    'name' : '',
                                    'booth_contact_first_name' : '',
                                    'booth_contact_last_name' : '',
                                    'booth' : '',
                                    'booth_contact_email' : ''
                                };
                                console.log($scope);
                                $scope.$parent.addConferenceExhibitor(response, type);
                                ngDialog.close();
                            })
                            .catch(function(error){
                                lib.processValidationError(error, toastr);
                            });
                    } else{
                        // toastr.error('Required field empty!');
                        return;
                    }
                };

                vm.editConferenceExhibitor = function(){
                    console.log(vm.exhibitorToEdit);
                    ConferenceExhibitorsService
                        .updateConferenceExhibitor(vm.exhibitorToEdit)
                        .then(function(response){
                            console.log(response);
                            $scope.$parent.updateExhibitor(response);
                            ngDialog.close();
                        })
                        .catch(function(error){
                            lib.processValidationError(error, toastr);
                        });
                };

                vm.buyExhibitorUsers = function(closeFunction) {
                    vm.closeFunction = closeFunction;
                    var form = angular.element('#payment-form');
                    Stripe.card.createToken(form, stripeResponseHandlerTempExhibitor);
                }

				vm.checkPromoCode = function(promo_code){
                    if (promo_code.checked != true) {
                        promo_code.checked = true;
                    } else {
                        promo_code.checked = false;
                    }

                    vm.testIfPromoCodeChecked();
                };

                vm.allChecked = false;

                vm.checkAllCodes = function() {
                    vm.allChecked = !vm.allChecked;
                    _.each(vm.promo_codes, function(promo_code) {
                        promo_code.checked = vm.allChecked;
                    });
                }

				vm.paymentCodes = $scope.$parent.paymentCodes;

				function stripeResponseHandler(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    var token = response.id;

                    ConferenceExhibitorsService.registerStripe(token)
                    .then(function(response){
                        vm.updateUser(response);
                        vm.paymentStep = 2;
                    })
                    .catch(function(error){
                        if (error.status == 400) {
                            var data = error.data;
                            data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />["','');
                            data = data.replace('"]','');
                            toastr.error(data);
                        }
                        console.log(error);
                    });

                  }
                };

                function stripeResponseHandlerTemp(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    var token = response.id;

                    ConferenceUsersService.buyConferenceUsers(ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.nrUsers, token)
                        .then(function(response){
                            $scope.$parent.updateUsersSlotsCount(vm.nrUsers);
                            vm.closeFunction();
                            console.log(response);
                        })
                        .catch(function(error){
                            if (error.status == 400) {
                                toastr.error(error.data.detail);
                            }
                        });
                  }
                };

                function stripeResponseHandlerTempPromoCode(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    var token = response.id;
                    ConferenceExhibitorsService.createPromoCodesUpdated(
                            vm.paymentCodes.value, vm.paymentCodes.nr, ConferenceService.hash_conference_id.decode(vm.conference_id)[0], token)
                        .then(function(response){
                            toastr.success('The promo codes were generated.', 'Promo codes created!');
                            ConferenceExhibitorsService.getPromoCodes(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                                  .then(function(promo_codes){
                                      vm.promo_codes = promo_codes.promo_codes;
                                      $scope.$parent.updatePromoCodes(vm.promo_codes);
                                      closeThisDialog();
                                  })
                                  .catch(function(error){
                                    closeThisDialog();
                                  });
                        })
                        .catch(function(error){
                            console.log(error);
                            toastr.error('Error!', 'There has been a problem with your request.');
                        });
                  }
                };

                vm.upperCaseF = function(a) {
                    setTimeout(function(){
                        a.value = a.value.toUpperCase();
                    }, 1);
                }

                function stripeResponseHandlerTemp2(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    console.log(response);
                    vm.last4 = response.card.last4;
                    var token = response.id;
                    vm.tempStripeToken = token;
                    console.log(vm.tempStripeToken);
                    vm.paymentStep = 2;
                    $scope.$apply();

                  }
                };

                vm.buyUsers = function(closeFunction) {
                  vm.closeFunction = closeFunction;
                  var form = angular.element('#payment-form');
                  Stripe.card.createToken(form, stripeResponseHandlerTemp);
                }

                vm.generateTemporaryToken = function() {
                    var form = angular.element('#payment-form');
                    Stripe.card.createToken(form, stripeResponseHandlerTemp);
                }

                vm.generateTempToken = function() {
                    var form = angular.element('#payment-form');
                    Stripe.card.createToken(form, stripeResponseHandlerTemp2);
                }

                vm.generateTokenForPromoCodes = function(closeThisDialog) {

                    ConferenceExhibitorsService.createPromoCodesUpdated(vm.paymentCodes.value, vm.paymentCodes.nr, ConferenceService.hash_conference_id.decode(vm.conference_id)[0], vm.tempStripeToken)
                        .then(function(response){
                            toastr.success('The promo codes were generated.', 'Promo codes created!');
                            ConferenceExhibitorsService.getPromoCodes(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                                  .then(function(promo_codes){
                                      vm.promo_codes = promo_codes.promo_codes;
                                      $scope.$parent.updatePromoCodes(vm.promo_codes);
                                      closeThisDialog();
                                  })
                                  .catch(function(error){
                                    closeThisDialog();
                                  });
                        })
                        .catch(function(error){
                            console.log(error);
                            toastr.error(error.data.detail);
                        });
                }

				vm.generateToken = function() {
                    var form = angular.element('#payment-form');
                    Stripe.card.createToken(form, stripeResponseHandler);
                }

                vm.CCNType = '';
                vm.checkTypeOfCard = function(){
                    var number = document.getElementById('CCN').value;
                    number = number.toString();
                    if(_.isEmpty(number) === false){
                        if((number.indexOf(4) === 0) && (number.length === 13 || number.length === 16)){
                            vm.CCNType = 'visa';
                        } else if((number.indexOf(5) === 0) && (number.charAt(1) >= 1 || number.charAt(1) <= 5) && (number.length === 16)){
                            vm.CCNType = 'master';
                        } else if((number.length === 16) && ((_.inRange(number.substring(0, 8), 60110000,  60119999)) ||
                            (_.inRange(number.substring(0, 8), 65000000,  65999999)) ||
                            (_.inRange(number.substring(0, 8), 62212600,  62292599)) ) ){
                            vm.CCNType = 'discover';
                        } else if((number.indexOf(3) === 0) && ((number.indexOf(4) === 1 || number.indexOf(7) === 1)) && (number.length === 15)){
                            vm.CCNType = 'american_exp'
                        } else if(((number.substring(0,4) == 3088) || (number.substring(0,4) == 3096) || (number.substring(0,4) == 3112) ||
                            (number.substring(0,4) == 3158) || (number.substring(0,4) == 3337) || (_.inRange(number.substring(0,8),35280000, 35899999))) &&
                            (number.length === 16)){
                            vm.CCNType = 'jcb';
                        } else if((number.indexOf(3) === 0) && (number.indexOf(0) === 1 || number.indexOf(6) === 1 || number.indexOf(8) === 1) && (number.length === 14)){
                            vm.CCNType = 'diners';
                        } else {
                            vm.CCNType = '';
                        }
                    } else{
                        vm.CCNType = '';
                    }

                }

                vm.createPromoCodes = function(closeThisDialog) {
                    if (!vm.paymentCodes || !vm.paymentCodes.value || !vm.paymentCodes.nr) {
                        toastr.error("Please fill in the form data correctly!"); return;
                    }
                    ConferenceExhibitorsService.createPromoCodes(vm.paymentCodes.value, vm.paymentCodes.nr, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                        .then(function(response){
                            toastr.success('The promo codes were generated.', 'Promo codes created!');
                            ConferenceExhibitorsService.getPromoCodes(ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                                  .then(function(promo_codes){
                                      vm.promo_codes = promo_codes.promo_codes;
                                      $scope.$parent.updatePromoCodes(vm.promo_codes);
                                      closeThisDialog();
                                  })
                                  .catch(function(error){
                                    closeThisDialog();
                                  });
                        })
                        .catch(function(error){
                            console.log(error);
                            toastr.error('Error!', 'There has been a problem with your request.');
                        });
                }

                function stripeResponseHandler2(status, response) {
                  if (response.error) { // Problem!
                    toastr.error(response.error.message);
                  } else {
                    var token = response.id;
                    ConferenceExhibitorsService.registerStripe(token)
                        .then(function(response){
                            vm.updateUser(response);
                            if ($scope.$parent.changeConferencePayment) {
                                $scope.$parent.updatePaymentMethodAfterRegisteringStripe();
                            }

                            vm.closeFunction();
                        })
                        .catch(function(error){
                            if (error.status == 400) {
                                console.log(error);
                                toastr.error(error.data.detail);
                            }
                        });
                  }
                };

                vm.registerConferencePayment = function(closeThisDialog) {
                    var form = angular.element('#payment-form');
                    vm.closeFunction = closeThisDialog;
                    Stripe.card.createToken(form, stripeResponseHandler2);
                }

                vm.uploadSupportPhoto = function(files, support_contact) {
                    lib.squareifyImage($q, files)
                        .then(function(response){
                            var imgGile = response.file;

                            FileService.getUploadSignature(imgGile)
                                .then(function(response){
                                    var file_link = response.data.file_link;
                                    var promises = [];

                                    promises.push(FileService.uploadFile(imgGile, response));

                                    $q.all(promises).then(function(response){
                                        support_contact.photo = file_link;
                                    }).catch(function(error){
                                        console.log(error);
                                    });

                                })
                                .catch(function(error){
                                    console.log(error);
                                    if (error.status == 403) {
                                        toastr.error('Permission Denied!');
                                    } else {
                                        toastr.error('Error!', 'There has been a problem with your request.');
                                    }
                                });
                        });
                }

                vm.removePromoCode = function() {
                    vm.addedPromoCode = null;
                    vm.promoCodeValue = 0;
                }

                vm.promoCodeValue = 0;

                vm.addPromoCode = function() {
                    console.log(vm.addedPromoCode);
                    if(_.isEmpty(vm.addedPromoCode)){
                        return;
                    }
                    vm.loadPromoCodeResponse = true;
                    ConferenceExhibitorsService
                        .assignPromoCode($scope.$parent.exhibitor, vm.addedPromoCode)
                        .then(function(response){
                            toastr.info('Promo code added!');
                            vm.promoCodeValue = response.value;
                            console.log(vm.promoCodeValue);
                            vm.loadPromoCodeResponse = false;
                        })
                        .catch(function(error){
                            vm.loadPromoCodeResponse = false;
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error('That Promo code is no longer available.');
                            }
                        });
                };

                vm.conference_pass_key = [];

                vm.joinConference = function(){
                    var ok = true;
                    var code = '';

                    _.each(vm.conference_pass_key, function(digit){
                        if(digit === '' || digit == null || digit == undefined){
                            ok = false;
                        } else{
                            code = code + digit;
                        }
                    });

                    console.log('joining conference here');

                    if(ok){
                        ConferenceExhibitorsService
                            .joinConference(code)
                            .then(function(response){
                                console.log(response);
                            })
                            .catch(function(error){
                                console.log(error);
                                toastr.error(error.message.detail);
                            });
                    }
                };

                vm.loadingExhibitorPayment = false;
                vm.purchaseExhibitorUpgrade = function(closeThisDialog) {
                    vm.loadingExhibitorPayment = true;
                    if ($scope.leadRetrievalSelected) {
                        ConferenceExhibitorsService
                            .exhibitorInitialPayNew($scope.$parent.exhibitor, vm.tempStripeToken, vm.addedPromoCode)
                            .then(function(response){
                                $scope.$parent.onSuccessfulLeadRetrievalPayment();
                                vm.loadingExhibitorPayment = false;
                                closeThisDialog();
                            })
                            .catch(function(error){
                                vm.loadingExhibitorPayment = false;
                                toastr.error(error.data.detail);
                            });
                    }

                    if ($scope.additionalUsers) {
                        ConferenceExhibitorsService
                            .exhibitorAddedUserPayNew($scope.$parent.exhibitor, vm.tempStripeToken, $scope.$parent.additionalUsers)
                            .then(function(response){
                                closeThisDialog();
                                vm.loadingExhibitorPayment = false;
                            })
                            .catch(function(error){
                                toastr.error(error.data.detail);
                                vm.loadingExhibitorPayment = false;
                            });
                    }
                }

				vm.updateExhibitor = function(updatedAttendee){
                    ConferenceAttendeesService
                        .validateNewAttendee(updatedAttendee)
                        .then(function(attendee){
                            attendee.conference_id = ConferenceService.hash_conference_id.decode(vm.conference_id)[0];
                            ConferenceAttendeesService
                                .updateAttendee(_.pickBy(attendee))
                                .then(function(postedData){
                                    vm.newAttendeeInfo = {};
                                    $scope.$parent.updatedStuff(attendee);
                                    $scope.$parent.updatedExhibitor = attendee;
                                    $scope.closeThisDialog();
                                })
                                .catch(function(error){
                                    console.log(error);
                                    if (error.status == 403) {
                                        toastr.error('Permission Denied!');
                                    } else {
                                        toastr.error('Error!', 'There has been a problem with your request.');
                                    }
                                });
                        });
                };
        	}
})();
