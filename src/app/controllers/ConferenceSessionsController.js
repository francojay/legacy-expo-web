(function(){
	'use strict';
		angular
			.module('app')
			.controller('ConferenceSessionsController', ['$stateParams', '$state', '$scope', 'ngDialog', 'ConferenceSessionsService', 'ConferenceService',
				'ConferenceAttendeesService',
				conferenceSessionsController]);

			function conferenceSessionsController($stateParams, $state, $scope, ngDialog, ConferenceSessionsService, ConferenceService, ConferenceAttendeesService){
				var vm = this;
				vm.conference_id = ConferenceService.hash_conference_id.decode($stateParams.conference_id)[0];
				vm.conferenceSessions = {};
				vm.getConferenceSessions = function(){
					console.log(vm.conference_id);
					ConferenceSessionsService
						.getAllSessions(vm.conference_id)
						.then(function(response){
							vm.conferenceSessions = response.data;
						});
				};

             	vm.formLabels = [
	                 {label:"First Name"},
	                 {label:"Last Name"},
	                 {label:"Job Title"},
	                 {label:"Company Name"},
	                 {label:"Email Address"},
	                 {label:"Phone Number"},
	                 {label:"Street Address"},
	                 {label:"City"},
	                 {label:"State", placeH: "--"},
	                 {label:"Zip Code"},
	                 {label:"Country", placeH: "Choose a Country"},
	                 {label:"Attendee ID"}
	            ];
	            vm.idsForAttendeesSessionChecked = [];
	            vm.idsForScannedSessionChecked = [];
	            vm.tempLabels = [];
	            vm.tempInfo = [];
	            vm.editAttendeeInfo = [];
	            vm.switchArrowA = true;
	            vm.switchArrowR = true;
	            $scope.cool = false;

	            vm.checkSessionAttendees = function(selectedAttendee){
	            	if($scope.$parent.sessionRegistrantsView){
		            	if(selectedAttendee.hasOwnProperty('sessionRegistrantChecked') === false){
	        				selectedAttendee.sessionRegistrantChecked = true;
		            	} else if(selectedAttendee.sessionRegistrantChecked === true){
		            		selectedAttendee.sessionRegistrantChecked = false;
		            	} else{
		            		selectedAttendee.sessionRegistrantChecked = true;
		            	}
		            } else if($scope.$parent.sessionAttendeesView){
		            	if(selectedAttendee.hasOwnProperty('sessionScannedChecked') === false){
	        				selectedAttendee.sessionScannedChecked = true;
		            	} else if(selectedAttendee.sessionScannedChecked === true){
		            		selectedAttendee.sessionScannedChecked = false;
		            	} else{
		            		selectedAttendee.sessionScannedChecked = true;
		            	}
		            }
	            }

	            vm.detectCtrl = function(e){
	            	var ctrlActive = false;
	            	var altActive = false;
	            	console.log(e.keyCode);
	            	if(e.keyCode == 17){
	            		ctrlActive = true;
	            	}
	            	if(e.keyCode == 18){
	            		altActive = true;
	            	}
	            	var start_interval = -1;
	            	var end_interval = -1;
	            	var i = 0;
	            	_.forEachRight(vm.attendeesListForThisSession, function(attendee, index){
	            		if($scope.$parent.sessionRegistrantsView && attendee.hasOwnProperty('sessionRegistrantChecked') &&
	            			attendee.sessionRegistrantChecked === true){
	            			start_interval = index;
	            			return;
	            		} else if($scope.$parent.sessionAttendeesView && attendee.hasOwnProperty('sessionScannedChecked') &&
	            			attendee.sessionScannedChecked === true){
	            			start_interval = index;
	            			return;
	            		}
	            	});
	            	_.forEach(vm.attendeesListForThisSession, function(attendee, index){
	            		if($scope.$parent.sessionRegistrantsView && attendee.hasOwnProperty('sessionRegistrantChecked') &&
	            			attendee.sessionRegistrantChecked === true){
	            			end_interval = index;
	            			return;
	            		} else if($scope.$parent.sessionAttendeesView && attendee.hasOwnProperty('sessionScannedChecked') &&
	            			attendee.sessionScannedChecked === true){
	            			end_interval = index;
	            			return;
	            		}
	            	});
	        		if(ctrlActive && (start_interval > -1) && (end_interval > -1)){
	        			for(i = start_interval+1; i < end_interval; i++){
	        				if($scope.$parent.sessionRegistrantsView){
	        					vm.attendeesListForThisSession[i].sessionRegistrantChecked = true;
	        				} else if($scope.$parent.sessionAttendeesView){
	        					vm.attendeesListForThisSession[i].sessionScannedChecked = true;
	        				}
	        			}
	        		} else if(altActive && (start_interval > -1) && (end_interval > -1)){
	        			for(i = start_interval; i <= end_interval; i++){
	        				if($scope.$parent.sessionRegistrantsView){
	        					vm.attendeesListForThisSession[i].sessionRegistrantChecked = false;
	        				} else if($scope.$parent.sessionAttendeesView){
	        					vm.attendeesListForThisSession[i].sessionScannedChecked = false;
	        				}
	        			}
	        		}
	            }

	            vm.addSessionAttendees = function(){
	            	vm.tempArr = [];
	            	vm.attendeesForSessionJustAdded = [];
	            	vm.idsForAttendeesSessionChecked = [];
	            	$scope.cool = false;
	            	var i, j, ok = false, nr = 0;
	            	_.forEach(vm.attendeesListForThisSession, function(attendee){
	            		if(attendee && attendee.sessionRegistrantChecked === true){
	            			vm.idsForAttendeesSessionChecked.push(attendee.id);
	            		}
	            	});
	            	if(_.isEmpty(vm.idsForAttendeesSessionChecked) === false){
		            	ConferenceSessionsService
		            		.addSessionAttendeesToList($scope.$parent.session_id, _.uniq(vm.idsForAttendeesSessionChecked))
		            		.then(function(response){
		            			for(i = 0; i < vm.attendeesListForThisSession.length; i++){
		            				for(j = 0; j < vm.idsForAttendeesSessionChecked.length; j++){
		            					if(vm.attendeesListForThisSession[i].id !== vm.idsForAttendeesSessionChecked[j]){
		            						ok = true;
		            					} else{
		            						ok = false;
		            						break;
		            					}
		            				}
		            				if(ok === true){
		            					vm.tempArr.push(vm.attendeesListForThisSession[i]);
		            				} else{
		            					vm.attendeesForSessionJustAdded[nr] = {};
		            					vm.attendeesForSessionJustAdded[nr].attendee = {};
		            					vm.attendeesForSessionJustAdded[nr].attendee = vm.attendeesListForThisSession[i];
		            					nr++;
		            				}
		       						if(i === (vm.attendeesListForThisSession.length - 1)){
		       							$scope.cool = true;
		       						}
		            			}
		            			if($scope.cool === true && vm.attendeesListForThisSession !== vm.tempArr){
			    				    vm.attendeesListForThisSession = vm.tempArr;
			    				   	$scope.$parent.totalNumberOfRegistrants = $scope.$parent.totalNumberOfRegistrants + vm.attendeesForSessionJustAdded.length;
			    				   	if($scope.$parent.sessionAttendees.sessions_attendees.length < 30){
			                        	vm.updateSessionAttendeesTabel();
			                        }
			    				   	$scope.closeThisDialog();
			    				}
		            		})
		            		.catch(function(error){
	                            console.log(error);
	                            if (error.status == 403) {
	                                toastr.error('Permission Denied!');
	                            } else {
	                                toastr.error('Error!', 'There has been a problem with your request.');
	                            }
	                        });
	            	}
	            }

	            vm.addScannedAttendees = function(){
	            	vm.tempArr = [];
	            	vm.scannedForSessionJustAdded = [];
	            	vm.idsForScannedSessionChecked = [];
	            	$scope.coolScan = false;
	            	var i, j, ok = false, nr = 0;
	            	_.forEach(vm.attendeesListForThisSession, function(attendee){
	            		if(attendee && attendee.sessionScannedChecked === true){
	            			vm.idsForScannedSessionChecked.push(attendee.attendee_id);
	            		}
	            	});
	            	if(_.isEmpty(vm.idsForScannedSessionChecked) === false){
		            	ConferenceSessionsService
		            		.scanSessionAttendeesToList($scope.$parent.session_id, _.uniq(vm.idsForScannedSessionChecked))
		            		.then(function(response){

		            			for(i = 0; i < vm.attendeesListForThisSession.length; i++){
		            				for(j = 0; j < vm.idsForScannedSessionChecked.length; j++){
		            					if(vm.attendeesListForThisSession[i].attendee_id !== vm.idsForScannedSessionChecked[j]){
		            						ok = true;
		            					} else{
		            						ok = false;
		            						break;
		            					}
		            				}
		            				if(ok === true){
		            					vm.tempArr.push(vm.attendeesListForThisSession[i]);
		            				} else{
		            					vm.scannedForSessionJustAdded[nr] = {};
		            					vm.scannedForSessionJustAdded[nr].attendee = {};
		            					vm.scannedForSessionJustAdded[nr].attendee = vm.attendeesListForThisSession[i];
		            					nr++;
		            				}
		       						if(i === (vm.attendeesListForThisSession.length - 1)){
		       							$scope.coolScan = true;
		       						}
		       					}
		       					vm.attendeesListForThisSession = vm.tempArr;
		                        $scope.$parent.totalNumberOfRegistrants = $scope.$parent.totalNumberOfRegistrants + vm.scannedForSessionJustAdded.length;
		                        if($scope.$parent.sessionAttendees.scanned_attendees.length < 30){
		                        	vm.updateScannedFromSessionList();
		                        }
		       					$scope.closeThisDialog();
		            		})
		            		.catch(function(error){
	                            console.log(error);
	                            if (error.status == 403) {
	                                toastr.error('Permission Denied!');
	                            } else {
	                                toastr.error('Error!', 'There has been a problem with your request.');
	                            }
	                        });
		            }
	            }

                vm.updateScannedFromSessionList = function(){
            		$scope.$parent.updateScannedFromSessionList(vm.scannedForSessionJustAdded, $scope.$parent.totalNumberOfRegistrants);
	            };

	           	vm.updateSessionAttendeesTabel = function(){
	            	$scope.$parent.updateAttendeeFromSessionList(vm.attendeesForSessionJustAdded, $scope.$parent.totalNumberOfRegistrants);
	            };

	            vm.attendeeFilters = {
	                queryString: null,
	                sortBy: 'first_name',
	                sortOrder: "asc"
	            }
	            vm.currentPage = 1;
	            vm.requestedMoreAttendees = false;
	            vm.showMoreAttendees = function(elem) {
	                if (!vm.requestedMoreAttendees) {
	                    var page = parseInt(vm.currentPage) + 1;
	                    vm.requestedMoreAttendees = true;
	                    ConferenceAttendeesService
	                        .getAllAttendees(vm.conference_id, page, true, vm.attendeeFilters)
	                        .then(function(attendees) {
	                            vm.currentPage = page;
	                            vm.requestedMoreAttendees = false;
	                            vm.attendeesListForThisSession = _.uniqBy(vm.attendeesListForThisSession.concat(attendees.attendees), 'id');
	                        })
	                        .catch(function(error){
	                            vm.requestedMoreAttendees = false;
	                        });
	                }
	            }

	            vm.addManualSessionAttendee = function(){
	            	$scope.$parent.updateToggleForPopUp(true, false);
	            	$scope.closeThisDialog();
	            	setTimeout($scope.openForm, 1000);
	            }

	            vm.searchAttendeesForSession = function() {
	                if (vm.attendeeQuery && vm.attendeeQuery.length > 0) {
	                    vm.attendeeFilters.queryString = vm.attendeeQuery;
	                } else {
	                    vm.attendeeFilters.queryString = '';
	                }

	                ConferenceAttendeesService 
	                    .getAllAttendees(vm.conference_id, 1, false, vm.attendeeFilters)
	                    .then(function(attendees) {
                            vm.attendeesListForThisSession = attendees.attendees;
                            if(vm.attendeesListForThisSession.length < 10){
                            	vm.currentPage = 1;
		                    	vm.showMoreAttendees();
		                    }
	                    })
	            }

	            vm.initListWithAttendees = function(){
	            	vm.attendeeFilters.sortBy = 'first_name';
		            vm.attendeeFilters.sortOrder = 'asc';
		            vm.attendeeFilters.queryString = '';
		            if($scope.$parent.sessionAttendeesView){
		            	$scope.$parent.sessionRegistrantsView = false;
		            	vm.attendeeFilters.not_scanned_in_session = $scope.$parent.session_id;
		            } else if($scope.$parent.sessionRegistrantsView){
		            	$scope.$parent.sessionAttendeesView = false;
		            	vm.attendeeFilters.not_registered_to_session = $scope.$parent.session_id;
		            }
		            ConferenceAttendeesService
		                .getAllAttendees(vm.conference_id, vm.currentPage, false, vm.attendeeFilters)
		                .then(function(attendees){
		                    vm.attendeesListForThisSession = attendees.attendees;
		                    if(vm.attendeesListForThisSession.length < 10){
		                    	vm.currentPage = 1;
		                    	vm.showMoreAttendees();
		                    }
		                });
	            };

	            vm.registrantFields = [
	                {
	                    code: 'full_name',
	                    title: 'Registrant'
	                },
	                {
	                    code: 'email',
	                    title: 'Email'
	                },
	                {
	                    code: 'phone',
	                    title: 'Phone'
	                },
	                {
	                    code: 'company',
	                    title: 'Company'
	                },
	                {
	                    code: 'qr_code',
	                    title: 'QR Code'
	                }
	            ];

	            vm.attendeeFields = [
	                {
	                    code: 'full_name',
	                    title: 'Attendee'
	                },
	                {
	                    code: 'email',
	                    title: 'Email'
	                },
	                {
	                    code: 'phone',
	                    title: 'Phone'
	                },
	                {
	                    code: 'company',
	                    title: 'Company'
	                },
	                {
	                    code: 'qr_code',
	                    title: 'QR Code'
	                }
	            ];
			}
})();