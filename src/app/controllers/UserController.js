//UserController
(function(){
	'use strict';

	angular
		.module('app')
		.controller('UserController', [
			'$rootScope', '$scope', 'SharedProperties', 'UserService', 'ConferenceService', '$stateParams',
			'LoginService', 'toastr', '$location', 'localStorageService', 'API',
			userController
		]);

	function userController($rootScope, $scope, SharedProperties, UserService, ConferenceService, $stateParams,
		LoginService, toastr, $location, localStorageService, API){
		var sharedData = SharedProperties.sharedObject;

		var vm = this;
		vm.confirmAccount = false;
		vm.createAccount = false;
		vm.invalidUniq = false;
		vm.digits_code = [];
		vm.acc = {};
		vm.invalidPassConf = false;
		vm.code = '';
		vm.invalidPass = false;
		vm.invalidEmail = false;
		vm.trackSignup = false;

		vm.token = $stateParams.token;

		if (vm.token) {
		    if(LoginService.isAuthenticated()){
		        console.log('here');
                UserService.joinConference(vm.token)
                    .then(function(response){
                        console.log(response);
                    })
                    .catch(function(error){
                        toastr.error('This invitation was made for another account!.');
                        console.log(error);
                    })
              } else {
                UserService.storeInviteToken(vm.token);
                $location.path("/login");
              }
		}
		console.log(vm.token);

		vm.formatCode = function(){
			var element;
			for(element = 0; element < vm.digits_code.length; element++){
				if((vm.digits_code[element] !== undefined) && (_.isEmpty(vm.digits_code[element]) === false)){
					vm.digits_code = vm.digits_code[element].split('');
					break;
				}
			}
		};

		vm.focus = function(id){
			if(id != 0 && document.getElementById('digit' + (id - 1)).value !== ''){
				document.getElementById('digit' + id).focus();
			} else {
				document.getElementById('digit4').blur();
			}
		}

		vm.validateData = function(){
			if(vm.acc.pass && ((vm.acc.pass.length < 8 || vm.acc.pass.length > 16) || (vm.acc.pass.search(/[0-9]/) < 0))){
				vm.acc.pass = '';
				vm.invalidPass = true;
			} else {
				vm.invalidPass = false;
			}
			if((vm.acc.confPass && (vm.acc.confPass !== vm.acc.pass)) || !vm.acc.confPass){
				vm.acc.confPass = '';
                vm.invalidPassConf = true;
			} else {
				vm.invalidPassConf = false;
			}
		};

		vm.createAcc = function(){
			vm.validateData();
			var invalid_inputs =
			_.filter(vm.acc, function(val, key){
					vm.acc.key = '';
			});
			var user = {};

			if(vm.invalidPass === true){
				user = {};
				toastr.error('The password must contain at least 1 digit', 'Must be longer than 8 characters and shorter than 16 characters');

			} else if(vm.invalidEmail === true){
				user = {};
				toastr.error('Error!', 'Email is incorrect.');

			} else if(vm.invalidPassConf === true){
				user = {};
				toastr.error('Error!', "The password didn't match");

			} else if (_.isEmpty(invalid_inputs) && ((vm.invalidPass === false) && (vm.invalidEmail === false) && (vm.invalidPassConf === false))) {
				user = {
				    username: vm.acc.email,
				    first_name: vm.acc.fname,
				    last_name: vm.acc.lname,
				    email: vm.acc.email,
				    password: vm.acc.pass,
				    affid: $location.search().affid,
				    sid: $location.search().sid
				};
			}
				UserService
					.register(user)
					.then(function(data){
						vm.messageBeforeConfirmation = true;
						vm.createAccount = false;
						vm.userEmail = user.email;
						toastr.success('Your account has been created!');
						marketing.trackSignUp(API.ENV);
						marketing.trackSignUpFB(API.ENV);
						marketing.linkedinTracking(API.ENV);
						marketing.googleRemarketingTag(API.ENV);
						marketing.googleAdwordsTracking(API.ENV);
					})
					.catch(function(error){
					    console.log(error);
					    if (error.data.detail.indexOf('email')) {
					        toastr.error(error.data.detail);
					    } else {
					        toastr.error('Error!', 'Please fill the inputs with valid information!');
					    }

					});
		};

		vm.verifyIfAccountIsConfirmed = function(){
			console.log('verify if account confirmed');
			if(UserService.accountNotConfirmed === true){
				vm.createAccount = false;
				vm.confirmAccount = true;
			} else {
				vm.createAccount = true;
				vm.confirmAccount = false;
			}

			var verify_email =  $location.search().verify_email;
			if (verify_email && verify_email != '') {
				verify_email = verify_email.replace(' ', '+');
				vm.createAccount = false;
				vm.messageBeforeConfirmation = false;
				vm.trackSignup = true;
				marketing.facebookPixel(API.ENV);
				marketing.trackSignUp(API.ENV);
				marketing.trackSignUpFB(API.ENV);
				marketing.linkedinTracking(API.ENV);
				marketing.googleRemarketingTag(API.ENV);
				marketing.googleAdwordsTracking(API.ENV);
				vm.confirmAccount = true;
				vm.userEmail = verify_email;
			}

			marketing.googleRemarketingTag(API.ENV);
			marketing.facebookPixel(API.ENV);
			marketing.trackSignUpViewFB(API.ENV);
			

		}

		vm.resendVerificationEmail = function() {
		    var user_email = vm.userEmail;
            if (!user_email) {
                user_email = localStorageService.get('verifyEmail');
            }

		    UserService.resendVerificationCode(user_email)
		        .then(function(response){
		            toastr.success('The verification email was resent!');
		        })
		        .catch(function(error){
		            toastr.error('There was an error with your request');
		        });
		}

		vm.verifyAccount = function(){
			var user_email = vm.userEmail;
			if (!user_email) {
			    user_email = localStorageService.get('verifyEmail');
			}

			var validData = {};
			var code;
			_.forEach(vm.digits_code, function(digit){
					vm.code = vm.code + digit;
			});
			code = parseInt(vm.code);
			if(typeof code === 'number'){

				validData = {
					'email' : user_email,
					'code' : code
				}
				vm.code = '';
				UserService
					.verify(validData)
					.then(function(response){
						if(response){
							console.log(response);
							toastr.success('Account Validated.', 'Welcome to ExpoPass!');

                            localStorageService.set('access_token', response.data.oauth2_data.access_token);
                            localStorageService.set('refresh_token', response.data.oauth2_data.refresh_token);
                            localStorageService.set('expires_in', response.data.oauth2_data.expires);

                            var timeObject = new Date(response.data.oauth2_data.expires);

                            localStorageService.set('expires_at', timeObject);
							$location.path("/conferences");
						}
					})
					.catch(function(error){
						console.log(error);
						toastr.error('Error!', 'Validation code is incorrect. Please type the correct code.');
					});
			}
		};

		$scope.env = API.ENV;
	}
})();
