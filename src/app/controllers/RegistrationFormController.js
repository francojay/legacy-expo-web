(function(){
    'use strict';

    angular
        .module('app')
        .controller('RegistrationFormController', [
            '$rootScope', '$scope', '$state', 'SharedProperties', '$stateParams', 'ngDialog', 'API',
            'ConferenceRegistrationService', 'ConferenceService',
            registrationFormController
        ]);

    function registrationFormController($rootScope, $scope, $state, SharedProperties, $stateParams, ngDialog, API, ConferenceRegistrationService, ConferenceService){
        var vm = this;
        var sharedData = SharedProperties.sharedObject;
        sharedData.defaultPhoto = "/assets/images/login_bg.png";
        sharedData.currentPageTitle = "Registration";
        vm.customStyle = true;
        vm.conditions_agreed = false;
        vm.indexForAddOnSelectedDay = 0;
        vm.conference_id = ConferenceService.hash_conference_id.decode($stateParams.conference_id)[0];
        vm.registeredAttendee = {};
        vm.registeredAttendee.pre_register_session = [];
        vm.registeredAttendee.selected_level_add_on_sessions = [];
        vm.registeredAttendee.continuing_educations = [];
        vm.pre_register_sessions = [];
        vm.register_add_on_sessions = [];
        vm.register_countinuing_educations = [];
        vm.register_custom_fields_response = [];
        vm.CCNType = '';
        SharedProperties.registeredAttendee = vm.registeredAttendee;
        vm.registrationIframeLoader = true;
        vm.number_call = 1;

        vm.sendEventToStep1Controller = function(){
            ConferenceRegistrationService.getRegistrationFormUnauthorized(vm.conference_id, vm.number_call)
                .then(function(response) {
                    if(vm.number_call === 1){
                        SharedProperties.registration_form = response;
                        SharedProperties.initialCustomFieldsList = _.cloneDeep(response.registrationformcustomfield_set);
                        $rootScope.$emit('REGISTRATION_FORM_LOADED_FIRST');
                        vm.number_call++;
                        vm.sendEventToStep1Controller();
                    } else if(vm.number_call === 2){
                        var registration_form_complete_data = response;
                        _.each(SharedProperties.registration_form, function(val, key){
                            registration_form_complete_data[key] = val;
                        });
                        SharedProperties.registration_form = registration_form_complete_data;
                        $rootScope.$emit('REGISTRATION_FORM_LOADED_SECOND');
                    }
                });
        };

        vm.sendEventToStep1Controller();


        $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            vm.blurred = 'main-div-container';
        });

        $rootScope.$on('ngDialog.closed', function (e, $dialog) {
            vm.blurred = 'main-div-container1';
            $scope.$apply();
        });
    }
})();
