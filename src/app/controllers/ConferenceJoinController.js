(function(){
    'use strict';
        angular
            .module('app')
            .controller('ConferenceJoinController', ['$scope', 'API', '$state', '$stateParams', 'ngDialog', '$location',
            'ConferenceExhibitorsService', 'SharedProperties', 'ConferenceUsersService', 'ConferenceService', 'ConferenceSessionsService',
            'FileService', '$q', 'toastr', conferenceJoinController]);

            function conferenceJoinController(
                $scope, API, $state, $stateParams, ngDialog, $location, ConferenceExhibitorsService, SharedProperties, ConferenceUsersService,
                ConferenceService, ConferenceSessionsService,
            FileService, $q, toastr){

                var vm = this;
                var sharedData = SharedProperties.sharedObject;
                sharedData.displayConference = false;
                vm.united_states = API.UNITED_STATES;
                vm.all_countries = API.COUNTRIES;
                vm.conference_pass_key = [];
                vm.previousExhibitorData = {};
                vm.conference_id = new Number();

                vm.backToMyConferences = function(){
                    $location.path('conferences');
                };

                vm.exhibitorJoinStep = 1;
                vm.loadingCompanies = false;

                vm.loadPreviousExhibitor = function() {
                    ConferenceExhibitorsService.getPreviousExhibitor()
                        .then(function(response){
                            vm.previousExhibitorData = response;
                            vm.previousExhibitorData.company_name = response.name;
                        }).catch(function(error){
                            console.log(error);
                        });
                }

                vm.loadPreviousExhibitor();

                vm.loadExhibitorCompanies = function(conference_id) {
                    vm.loadingCompanies = true;
                    ConferenceExhibitorsService.listExhibitorCompanies(conference_id)
                        .then(function(response){
                            vm.loadingCompanies = false;
                            vm.exhibitorCompanies = response;
                            if (vm.exhibitorCompanies.length <= 0) {
                                vm.exhibitorJoinStep = 2;
                            }
                            console.log(response);
                        }).catch(function(error){
                            vm.loadingCompanies = false;
                            console.log(error);
                        });
                }

                vm.companySelected = false;
                vm.joinAsCompany = function() {
                    var exhibitor = _.find(vm.exhibitorCompanies, {'checked': true});
                    if (exhibitor) {
                        ConferenceExhibitorsService.joinAsCompany(exhibitor.id)
                            .then(function(response){
                                vm.exhibitorJoinStep = 2;
                                vm.companySelected = response;
                                // $location.path('/exhibitor/' + ConferenceSessionsService.hash_session_id.encode(exhibitor.conference));
                            }).catch(function(error){
                                console.log(error);
                                toastr.error("Permission Denied! Please contact the company administrator for access!");
                            });
                    }
                }

                vm.createExhibitor = function() {
                    if(_.isObject(vm.previousExhibitorData.country)){
                        var country = vm.previousExhibitorData.country.name;
                        if(country == 'United States'){
                            var state = vm.previousExhibitorData.state.abbreviation;
                            vm.previousExhibitorData.state = state;
                        }
                        vm.previousExhibitorData.country = country;
                    }
                    vm.previousExhibitorData = _.pickBy(vm.previousExhibitorData, function(infok, infov){
                        if(vm.previousExhibitorData[infov] != null && vm.previousExhibitorData[infov] != ''){
                            return vm.previousExhibitorData[infov];
                        }
                    });
                    vm.previousExhibitorData.id = sharedData.current_exhibitor_id;
                    vm.previousExhibitorData.name = vm.previousExhibitorData.company_name;
                    console.log(vm.previousExhibitorData);

                    if (vm.companySelected) {
                        vm.previousExhibitorData.id = vm.companySelected.id;
                        ConferenceExhibitorsService
                            .updateExhibitorCompanyProfile(vm.previousExhibitorData)
                            .then(function(response){
                                console.log(response);
                                toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                                $location.path('/exhibitor/' + ConferenceSessionsService.hash_session_id.encode(sharedData.current_exhibitor_conference_id) + '/lead-retrieval');
                            })
                            .catch(function(error){
                                toastr.error(error.data.detail);
                                console.log(error);
                            });
                    } else {
                        ConferenceExhibitorsService
                            .createExhibitor(sharedData.current_exhibitor_conference_id, vm.previousExhibitorData)
                            .then(function(response){
                                $location.path('/exhibitor/' + ConferenceSessionsService.hash_session_id.encode(sharedData.current_exhibitor_conference_id) + '/lead-retrieval');
                                toastr.success('You succesfully join the ' + response.conference_info.name + ' conference!');
                            })
                            .catch(function(error){
                                console.log(error);
                                toastr.error(error.data.detail);
                            });
                    }
                }

                if (sharedData.current_exhibitor_conference_id) {
                    vm.loadExhibitorCompanies(sharedData.current_exhibitor_conference_id);
                }

                vm.checkExhibitorCompany = function(exhibitorCompany) {
                    _.each(vm.exhibitorCompanies, function(company){
                        if (company.id != exhibitorCompany.id) {
                            company.checked = false;
                        }
                    });
                    exhibitorCompany.checked = !exhibitorCompany.checked;

                }

                vm.joinConference = function() {
                    var ok = true;
                    var code = '';

                    _.each(vm.conference_pass_key, function(digit){
                        if(digit === '' || digit == null || digit == undefined){
                            ok = false;
                        } else{
                            code = code + digit;
                        }
                    });

                    if(ok){
                        ConferenceExhibitorsService
                            .getConferenceByCode(code)
                            .then(function(response){
                                sharedData.current_exhibitor_conference_id = response.conference.id;
                                $state.go('home.exhibitor_data_conference',{
                                    notify:false,
                                    reload:false,
                                    location:'replace',
                                    inherit:false
                                });
                            })  
                            .catch(function(error){
                                toastr.error("There is no conference with this code!");
                            });
                    }
                }

                // vm.joinConference = function(){
                //     var ok = true;
                //     var code = '';

                //     _.each(vm.conference_pass_key, function(digit){
                //         if(digit === '' || digit == null || digit == undefined){
                //             ok = false;
                //         } else{
                //             code = code + digit;
                //         }
                //     });

                //     if(ok){

                //         ConferenceExhibitorsService
                //             .joinConferenceAsExhibitor(code)
                //             .then(function(response){
                //                 console.log(response);
                //                 vm.loadExhibitorCompanies(ConferenceService.hash_conference_id.decode(response.conference_info.conference_id)[0]);
                //                 sharedData.previous_exhibitor_data = response.previous_exhibitor;
                //                 sharedData.current_exhibitor_conference_id = response.conference_info.conference_id;
                //                 sharedData.current_exhibitor_id = response.exhibitor_info.id;
                //                 $state.go('home.exhibitor_data_conference',{
                //                     notify:false,
                //                     reload:false,
                //                     location:'replace',
                //                     inherit:false
                //                 });
                //                 console.log(vm.previousExhibitorData);
                //                 toastr.success('You succesfully join the ' + response.conference_info.name + ' conference!');
                //             })
                //             .catch(function(error){
                //                 console.log(error);
                //                 toastr.error('You enter a invalid Pass Key!');
                //             });
                //     }
                // };

                vm.joinConferenceEnter = function(e){
                    if(e.keyCode == 13){
                        vm.joinConference();
                    }
                };

                $scope.$watch(function(){
                    return  vm.previousExhibitorData.country;
                }, function(previous_state){
                    if (previous_state) {
                        if((previous_state.hasOwnProperty('name') && previous_state.name == 'United States') || 
                            previous_state.country == 'United States') {
                            _.each(vm.united_states, function(state){
                                if(state.abbreviation == vm.previousExhibitorData.state){
                                    vm.previousExhibitorData.state = state;
                                }
                            });
                        } else{
                            vm.previousExhibitorData.state = '';
                        }
                    }
                });

                $scope.$watch(function(){
                    return  sharedData.previous_exhibitor_data;
                },function(data){
                    if(data && !_.isEmpty(data)){
                        vm.previousExhibitorData = sharedData.previous_exhibitor_data;

                        if(vm.previousExhibitorData.country != null){
                            _.each(vm.all_countries, function(country){
                                if(country.name == vm.previousExhibitorData.country){
                                    vm.previousExhibitorData.country = country;
                                }
                            });
                            if(vm.previousExhibitorData.country.code == 'US'){
                                _.each(vm.united_states, function(state){
                                    if(state.abbreviation == vm.previousExhibitorData.state){
                                        vm.previousExhibitorData.state = state;
                                    }
                                });
                            }
                        }
                    }
                });

                $scope.$watch(function(){
                    return sharedData.current_exhibitor_conference_id;
                },function(conference_id){
                    console.log(conference_id);
                    if(conference_id !== null && conference_id != NaN){
                        vm.conference_id = conference_id;
                    }
                });

                vm.focus = function(id){
                    if(id != 0 && document.getElementById('passkey' + (id - 1)).value !== ''){
                        document.getElementById('passkey' + id).focus();
                    }
                };

                vm.uploadCompanyLogo = function(files){
                    lib.squareifyImage($q, files)
                    .then(function(response){
                        var imgGile = response.file;

                        FileService.getUploadSignature(imgGile)
                            .then(function(response){
                                var file_link = response.data.file_link;
                                FileService.uploadFile(imgGile, response)
                                    .then(function(uploadResponse){
                                        vm.previousExhibitorData.logo = file_link;
                                    })
                                    .catch(function(error){
                                        console.log(error)
                                    });
                            })
                            .catch(function(error){
                                console.log(error);
                                if (error.status == 403) {
                                    toastr.error('Permission Denied!');
                                } else {
                                    toastr.error('Error!', 'There has been a problem with your request.');
                                }
                            });
                    });                
                };

                vm.updateExhibitorProfile = function(){
                    if(_.isObject(vm.previousExhibitorData.country)){
                        var country = vm.previousExhibitorData.country.name;
                        if(country == 'United States'){
                            var state = vm.previousExhibitorData.state.abbreviation;
                            vm.previousExhibitorData.state = state;
                        }
                        vm.previousExhibitorData.country = country;
                    }
                    vm.previousExhibitorData = _.pickBy(vm.previousExhibitorData, function(infok, infov){
                        if(vm.previousExhibitorData[infov] != null && vm.previousExhibitorData[infov] != ''){
                            return vm.previousExhibitorData[infov];
                        }
                    });
                    vm.previousExhibitorData.id = sharedData.current_exhibitor_id;
                    ConferenceExhibitorsService
                        .updateExhibitorCompanyProfile(vm.previousExhibitorData)
                        .then(function(response){
                            console.log(response);
                            toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                            $location.path('exhibitor/' + vm.conference_id);
                        })
                        .catch(function(error){
                            toastr.error('Error!', 'There has been a problem with your request.');
                            console.log(error);
                        });
                };

                vm.mainExhibitorProfileFields = [
                    {
                        label: 'Company Name',
                        code: 'company_name'
                    },
                    {
                        label: 'Email Address',
                        code: 'admin_email'
                    },
                    {
                        label: 'Phone Number',
                        code: 'phone_number'
                    },
                    {
                        label: 'Street Address',
                        code: 'street_address'
                    },
                    {
                        label: 'City',
                        code: 'city'
                    },
                    {
                        label: 'State',
                        code: 'state',
                        placeholder: '--'
                    },
                    {
                        label: 'Zip Code',
                        code: 'zip_code'
                    },
                    {
                        label: 'Country',
                        code: 'country',
                        placeholder: 'Choose a country'
                    },
                    {
                        label: 'Website',
                        code: 'website'
                    },
                    {
                        label: 'Promo Code',
                        code: 'promo_code'
                    },
                    {
                        label: 'Description',
                        code: 'company_description'
                    },
                    {
                        label: 'Company Logo',
                        code: 'logo'
                    }
                ];
            }
})();