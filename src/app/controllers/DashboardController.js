//DashboardController
(function(){
	'use strict';

	angular
		.module('app')
		.controller('DashboardController', [
			'$rootScope', '$scope', 'SharedProperties',
			dashboardController
		]);

	function dashboardController($rootScope, $scope, SharedProperties){
		var sharedData = SharedProperties.sharedObject;
		sharedData.displayConference = true;
		sharedData.currentPageTitle = 'dashboard';
		var vm = this;
	}

})();