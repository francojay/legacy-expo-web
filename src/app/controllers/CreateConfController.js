(function(){
    'use strict';
    angular
        .module('app')
        .controller('CreateConfController', [
            '$scope', 'API', 'SharedProperties', 'ConferenceService', 'FileService', '$q', 'toastr', '$state', '$http', createConfController
        ]);
    function createConfController($scope, API, SharedProperties, ConferenceService, FileService, $q, toastr, $state, $http){
        var sharedData = SharedProperties.sharedObject;
        sharedData.currentPageTitle = 'create_conf';
        var vm = this;
        vm.show = false;
        vm.userInvalidTimeZone = false;
        vm.timeZoneIndex = -1;
        vm.united_states = API.UNITED_STATES;
        vm.all_countries = API.COUNTRIES;
        vm.apiKey = API.GOOGLE_API_KEY;
        vm.defaultTimezones = API.TIMEZONES;
        vm.conf = {
            min_date: format_date_for_validation(new Date()),
            country: vm.all_countries[0],
            state: vm.united_states[0],
            enable_attendees: true
        };

        (function(){
           var currentDate = new Date();
           vm.currentDate = currentDate.getMonth() + 1 + '/' + currentDate.getDate() + '/' + currentDate.getFullYear();
        })();

        vm.presetDate = function(){
            if(vm.conf.date_to === undefined || vm.conf.date_to === true) {
                vm.conf.date_to = _.cloneDeep(vm.conf.date_from);
                document.getElementById('conference_end_date').focus();
            }
        };

        vm.setTimeZone = function(time_zone){
            var index = -1;
            _.each(vm.defaultTimezones, function(tmz){
                index++;
                if(tmz.text == time_zone){
                    vm.conf.time_zone = tmz.offset;
                    vm.timeZoneIndex = index;
                    vm.conf.timezone_name = tmz.utc[0];
                    return;
                }
            });
        };

        $scope.$watch(function(){
            return vm.conf.date_from;
        }, function(date_from){
            vm.conf['max_date'] = format_date_for_validation(date_from);
        });

        $scope.$watch(function(){
            return vm.conf['country'];
        }, function(country){
            if(country !== vm.all_countries[0]){
                vm.conf['state'] = '';
            }else{
                vm.conf['state'] = vm.united_states[0];
            }
        });

        vm.getConferenceTimezone = function(){
          return $q(function(resolve,reject){
            var url = '';
            if(vm.conf.hasOwnProperty('zip_code') && vm.conf.zip_code != '' && vm.conf.hasOwnProperty('country') && vm.conf.country.code != ""){
                if (vm.conf.country.code != 'US') {
                    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' +
                        vm.conf.country.name + ',' +
                        vm.conf.zip_code + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
                } else {
                    if (vm.conf.hasOwnProperty('state') && vm.conf.state.abbreviation != "") {
                        url = 'https://maps.googleapis.com/maps/api/geocode/json?components=country:' + 
                            vm.conf.country.code + '|administrative_area:' + 
                            vm.conf.state.abbreviation +'|postal_code:' + 
                            vm.conf.zip_code + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
                    }
                }
            } else{ 
                vm.userInvalidTimeZone = false;
                vm.timeZoneIndex = -1;
                reject('No country or zip code entered.'); // This should never happen as country selector has a default value
            }
            $http({
                method: 'GET',
                url: url
            })
                .then(function(response){
                    response = response.data;
                    if(response.status == "OK"){
                        var latitude = response.results[0].geometry.location.lat;
                        var longitude = response.results[0].geometry.location.lng;
                        var timestamp = new Date().getTime().toString();
                        $http({
                            'method': "GET",
                            'url': 'https://maps.googleapis.com/maps/api/timezone/json?location=' + latitude + ',' + longitude + '&timestamp=' +
                            timestamp.substring(0, timestamp.length - 3) + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk'
                        })
                            .then(function(timezone_response){
                                timezone_response = timezone_response.data;
                                if(timezone_response.status == "OK"){
                                    vm.userInvalidTimeZone = false;
                                    vm.conf.timezone_name = timezone_response.timeZoneId;
                                    resolve(timezone_response.status); // Success
                                } else if(timezone_response.status != "OK"){
                                    vm.userInvalidTimeZone = true;
                                    vm.timeZoneIndex = -1;
                                    reject(timezone_response.status); // Bad timezone response
                                }
                            })
                            .catch(function(timezone_error){
                                vm.userInvalidTimeZone = true;
                                vm.timeZoneIndex = -1;
                                reject('NO_CONNECTION'); // Can't connect to Google timezone API
                            });
                    } else if(response.status != "OK"){
                        vm.userInvalidTimeZone = true;
                        vm.timeZoneIndex = -1;
                        reject(response.status); // Bad geocode response
                    }
                })
                .catch(function(error){
                    vm.userInvalidTimeZone = true;
                    vm.timeZoneIndex = -1;
                    reject('NO_CONNECTION'); // Can't connect to Google geocode API
                });
              });
            };


        function format_date_for_validation(date) {
            if (!date) return format_date_for_validation(new Date());

            var dateObj = new Date(date);
            var dd = dateObj.getDate();
            var mm = dateObj.getMonth()+1; //January is 0!
            var yyyy = dateObj.getFullYear();

            if(dd<10) {
                dd='0'+dd
            }

            if(mm<10) {
                mm='0'+mm
            }
            var today = yyyy+'-'+mm+'-'+dd;
            return today;
        }
        vm.createConferenceButtonClickable = true;
        vm.createConf = function(){
          
            var conf = _.cloneDeep(vm.conf);

            var dateFrom = new Date(vm.conf.date_from);
            var dateTo = new Date(vm.conf.date_to);

            conf.day_from = {
                "year": dateFrom.getFullYear(),
                "month": dateFrom.getMonth() + 1,
                "day": dateFrom.getDate()
            }

            conf.day_to = {
                "year": dateTo.getFullYear(),
                "month": dateTo.getMonth() + 1,
                "day": dateTo.getDate()
            }

            if (!conf.name) {
                toastr.error("The Conference Name is a required field");
                return;
            }

            if (!conf.venue) {
                toastr.error("The Conference Venue field is required");
                return;
            }

            if (!conf.street) {
                toastr.error("The Conference Street field is required");
                return;
            }

            if (!conf.city) {
                toastr.error("The Conference City field is required");
                return;
            }

            if (!conf.country) {
                toastr.error("The Conference Country field is required");
                return;
            }

            if (!conf.zip_code) {
                toastr.error("The Conference Zip Code field is required");
                return;
            }

            // if (!conf.timezone_name) {
            //     toastr.error("The Conference Time Zone field is required");
            //     return;
            // }

            if (!conf.state) {
                toastr.error("The Conference State field is required");
                return;
            }

            if (conf.state.abbreviation) {
                if (conf.state.abbreviation == '--') {
                    toastr.error("The conference state field is required");
                    return;
                }
                conf.state = conf.state.abbreviation;
            }

            if (conf.country.name) {
                conf.country = conf.country.name;
            }

            conf.zip_code = parseInt(conf.zip_code);

            vm.createConferenceButtonClickable = false;

            vm.getConferenceTimezone().then(function(data) {

            console.log(data); // DEBUGGING SUCCESS

            ConferenceService
                .validateForm(conf)
                .then(function(response){
                    ConferenceService
                        .createConference(response)
                        .then(function(data){
                            $scope.env = API.ENV;
                            if (data.is_user_first_conference) {
                                $scope.is_user_first_conference = true;
                                marketing.trackFirstConference(API.ENV);
                                marketing.trackFirstConferenceFB(API.ENV);
                                marketing.googleRemarketingTag(API.ENV);
                            } else {
                                $scope.is_user_first_conference = false;
                            }
                            toastr.success('The conference has been created successfully.', 'Conference Created!');
                            var conference_id =  ConferenceService.hash_conference_id.encode(data.id);
                            setTimeout(function () {
                                vm.createConferenceButtonClickable = false;
                                $state.transitionTo('home.conference_details', {'conference_id' : conference_id}, {location: "replace"});
                            }, 2000);
                        })
                        .catch(function(error){
                            vm.createConferenceButtonClickable = true;
                            lib.processValidationError(error, toastr);
                        });
                });
              },function(error) {
                vm.createConferenceButtonClickable = true;
                console.log(error); // DEBUGGING ERROR
                if(error=="ZERO_RESULTS"){
                  toastr.error('Can\'t find timezone for current zipcode.');
                } else {
                  toastr.error("Could not get timezone. Please try again later.");
                }
              });
        };

        vm.changeConferenceLogo = function(files) {
            var img = new Image;
            img.onload = function() {
                FileService.getUploadSignature(files[0])
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];
                        promises.push(FileService.uploadFile(files[0], response));

                        $q.all(promises).then(function(response){
                            vm.conf.logo = file_link;
                        }).catch(function(error){
                            console.log(error);
                        });

                    })
                    .catch(function(error){
                        console.log(error);
                    });
            }
            img.src = files[0].$ngfBlobUrl;
        }
    }
})();
