(function(){
	'use strict';
		angular
			.module('app')
			.service('ConferenceExhibitorsService', ['Request2', 'API', '$q', 'localStorageService', 'ConferenceService', conferenceExhibitorsService
				]);
			function conferenceExhibitorsService(Request2, API, $q, localStorageService, ConferenceService){
				var service = {};

				//super-admin

                service.getExhibitorUsers = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_USERS_CR + exhibitor_id + '/',
                        {},
                        false
                    )
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            deferred.reject(error);
                        });

                    return deferred.promise;
                };

                service.createExhibitorUsers = function(exhibitor_id, exhibitor_user){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_USERS_CR + exhibitor_id + '/',
                        exhibitor_user,
                        false
                    )
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            deferred.reject(error);
                        });

                    return deferred.promise;
                };

                service.updateExhibitorUsers = function(exhibitor_user){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_USERS_UD + exhibitor_user.id + '/',
                        exhibitor_user,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };


                service.deleteExhibitorUsers = function(exhibitor_user_id){
                    var deferred = $q.defer();
                    Request2.remove(
                        API.EXHIBITOR_USERS_UD + exhibitor_user_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.getExhibitorQualifiers = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_QUALIFIERS_CR + exhibitor_id + '/',
                        {},
                        false
                    )
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            deferred.reject(error);
                        });

                    return deferred.promise;
                };

                service.modifyExhibitorQualifiers = function(exhibitor_id, new_qualifier_model){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_QUALIFIERS_CR + exhibitor_id + '/',
                        new_qualifier_model,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                //

				service.validateNewExhibitor = function(newExhibitor){
                    return $q.when(newExhibitor);
                };

                service.getExhibitorFiles = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_FILES_CRUD + exhibitor_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.getConferenceExhibitorPaymentList = function(conference_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.CONFERENCE_EXHIBITOR_PAYMENTS + conference_id + '/',
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.createExhibitorFiles = function(exhibitor_file){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_FILES_CRUD + exhibitor_file.exhibitor + '/',
                        exhibitor_file,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.deleteExhibitorFile = function(exhibitor_file_id){
                    var deferred = $q.defer();
                    Request2.remove(
                        API.EXHIBITOR_FILE_CRUD + exhibitor_file_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.createExhibitorContacts = function(exhibitor_user){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_CONTACTS_CURD + exhibitor_user.exhibitor + '/',
                        exhibitor_user,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.getExhibitorContacts = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_CONTACTS_CURD + exhibitor_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.revokeExhibitors = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_CONTACTS_CURD + exhibitor_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.updateExhibitorContact = function(exhibitor_contact){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_CONTACT_CURD + exhibitor_contact.id + '/',
                        exhibitor_contact,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.deleteExhibitorUser = function(exhibitor_id, contacts_ids){
                    var deferred = $q.defer();
                    var contacts_ids_string = _.join(contacts_ids, ',');
                    Request2.remove(
                        API.EXHIBITOR_CONTACTS_CURD + exhibitor_id + '?exhibitor_support_contact_ids=' + contacts_ids_string,
                        {},
                        false
                    )
                    .then(function(response){
                        console.log(response);
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

				service.getAllExhibitors = function(conference_id, page, noLoader, attendeeFilters){
				    noLoader = noLoader || false;
                    attendeeFilters = attendeeFilters || {
                        queryString: null,
                        sortBy: null,
                        sortOrder: null
                    }

					var deferred = $q.defer();

					var query_url = API.GET_ALL_CONFERENCES_EXHIBITORS_URL + "?conference_id=" 	+ conference_id + '&page=' + page;
                    if (attendeeFilters.sortBy) {
                        if (attendeeFilters.queryString) {
                            query_url = API.GET_ALL_CONFERENCES_EXHIBITORS_URL + "?conference_id=" 	+ conference_id + '&page=' + page
                                                    + "&sort_by=" + attendeeFilters.sortBy
                                                    + "&sort_order=" + attendeeFilters.sortOrder
                                                    + "&q=" + attendeeFilters.queryString;
                        }
                        else {
                            query_url = API.GET_ALL_CONFERENCES_EXHIBITORS_URL + "?conference_id=" 	+ conference_id + '&page=' + page
                                                    + "&sort_by=" + attendeeFilters.sortBy
                                                    + "&sort_order=" + attendeeFilters.sortOrder;
                        }

                    } else {
                        if (attendeeFilters.queryString) {
                            query_url = API.GET_ALL_CONFERENCES_EXHIBITORS_URL + "?q=" + attendeeFilters.queryString
                        }
                    }

					Request2.get(query_url, {}, false, noLoader)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            console.log(error);
                            deferred.reject(error);
                        });
					return deferred.promise;
				};

                service.exhibitorAddedUserPayNew = function(exhibitor, stripe_token_extra_users, nr_users){
                    var deferred = $q.defer();
                    var url = API.EXHIBITOR_PAY_NEW + exhibitor.id + '/';

                    var data = {
                        "type": 'exhibitor_user_fee',
                        "stripe_token": stripe_token_extra_users,
                        "nr_users": nr_users
                    }

                    Request2.post(url, data, false, false)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

				service.registerStripe = function(stripeToken) {
					var deferred = $q.defer();
					Request2.post(
						API.USER_REGISTER_PAYMENT,
						{
							stripeToken: stripeToken
						},
						false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});

					return deferred.promise;
				}

				service.createPromoCodes = function(value, number, conference_id) {
                    var deferred = $q.defer();
                    Request2.post(
                        API.CREATE_PROMO_CODES,
                        {
                            value: parseInt(value),
                            conference_id: conference_id,
                            number: parseInt(number),
                            description: "Generated Promo Codes"
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.uploadExhibitors = function(conference_id, exhibitors_list){
                    var deferred = $q.defer();
                    var data = {
                        exhibitor_set: exhibitors_list
                    }
                    Request2.post(
                        API.EXHIBITOR_LIST_BULK_UPLOAD + conference_id + '/',
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.listExhibitorCompanies = function(conference_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.EXHIBITOR_LIST_BULK_UPLOAD + conference_id + '/',
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.getPreviousExhibitor = function(){
                    var deferred = $q.defer();
                    Request2.get(
                        API.PREVIOUS_EXHIBITOR,
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.createExhibitor = function(conference_id, exhibitor, logo) {
                    var deferred = $q.defer();

										exhibitor.logo = logo;

                    Request2.post(
                        API.EXHIBITOR_CRUD + conference_id + '/',
                        exhibitor,
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.joinAsCompany = function(exhibitor_id){
                    var deferred = $q.defer();
                    Request2.post(
                        API.JOIN_AS_COMPANY + exhibitor_id + '/',
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.getConferenceByCode = function(code){
                    var deferred = $q.defer();
                    Request2.get(
                        API.GET_CONFERENCE_BY_CODE,
                        {
                            code: code
                        },
                        false,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };


                service.getExhibitorsForWeb = function(conference_id){
                    var deferred = $q.defer();

                    Request2.get(
                        API.EXHIBITOR_WEB_LIST + conference_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.setExhibitorBooth = function(exhibitor, booth) {
                    var deferred = $q.defer();

                    if (!booth) booth = "";

                    var boothData = {
                        "booth": booth
                    }

                    Request2.post(
                        API.EXHIBITOR_BOOTH + exhibitor.id,
                        boothData,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.createPromoCodesUpdated = function(value, number, conference_id, stripeToken) {
                    var deferred = $q.defer();
                    Request2.post(
                        API.CREATE_PROMO_CODES_UPDATED,
                        {
                            value: parseInt(value),
                            conference_id: conference_id,
                            number: parseInt(number),
                            description: "Generated Promo Codes",
                            stripeToken: stripeToken
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.revokeExhibitors = function(conference_id, exhibitor_ids) {
                    var deferred = $q.defer();

                    var data = {
                        exhibitor_ids: exhibitor_ids
                    }

                    Request2.post(
                        API.EXHIBITOR_REVOKE_ACCESS + conference_id + '/',
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.deleteConferenceExhibitors = function(conference_id, exhibitor_ids) {
                    var deferred = $q.defer();

                    Request2.remove(
                        API.EXHIBITOR_CRUD + conference_id + '/' + '?exhibitor_ids=' + exhibitor_ids.join(),
                        {},
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

				service.deleteAllConferenceExhibitors = function(conference_id, exhibitor_ids) {
                    var deferred = $q.defer();

                    var data = {
                        exhibitor_ids: [],
                        revoke_all:true
                    }

                    Request2.remove(
                        API.EXHIBITOR_CRUD + conference_id + '/' + '?exhibitor_ids=' + exhibitor_ids.join(),
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.revokeAllExhibitors = function(conference_id) {
                    var deferred = $q.defer();

                    var data = {
                        conference_id: conference_id,
                        exhibitor_ids: [],
                        revoke_all:true
                    }

                    Request2.post(
                        API.REVOKE_EXHIBITOR,
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.setConferenceHours = function(conference_id, conference_hours) {
                    var deferred = $q.defer();

                    var url = API.CONFERENCE_HOURS_LIST + conference_id + '/';

                    var hours = [];

                    _.each(conference_hours, function(conference_hour){
                        conference_hour.conference = conference_id;
                        hours.push(conference_hour);
                    });

                    Request2.post(
                        url,
                        {
                            conference_id: conference_id,
                            conferencehour_set: hours
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.updateConferenceExhibitor = function(conference_exhibitor_model){ // change when get endpoint
                    var deferred = $q.defer();
                    conference_exhibitor_model['name'] = conference_exhibitor_model['company_name']
                    Request2.post(
                        API.EXHIBITOR_BOOTH + conference_exhibitor_model.id + '/',
                        conference_exhibitor_model,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.updateExhibitorCompanyProfile = function(exhibitor_profile){
                    var deferred = $q.defer();
                    Request2.post(
                        API.JOIN_AS_COMPANY + exhibitor_profile.id + '/',
                        exhibitor_profile,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.joinConferenceAsExhibitor = function(code){
                    var deferred = $q.defer();
                    Request2.post(
                        API.EXHIBITOR_JOIN_CONFERENCE,
                        {
                            'code' : code
                        },
                        false
                    )
                    .then(function(response){
                        //console.log('join conference response');
                        //console.log(response);
                        response.data.conference_info.conference_id = ConferenceService.hash_conference_id.encode(response.data.conference_info.conference_id);
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                };

                service.addExhibitorMaterial = function(material) {
                    var deferred = $q.defer();
                    Request2.post(
                        API.ADD_EXHIBITOR_MATERIAL,
                        material,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.listTransactions = function(conference_id) {
                    var deferred = $q.defer();
                    Request2.get(API.CONFERENCE_LIST_TRANSACTIONS + '?conference_id=' + conference_id, {}, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.listExhibitorMaterials = function(conference_id) {
                    var deferred = $q.defer();
                    Request2.get(
                        API.LIST_EXHIBITOR_MATERIALS + '?conference_id=' + conference_id,
                        {
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.removeExhibitorMaterial = function(exhibitor_material_id) {
                    var deferred = $q.defer();
                    Request2.remove(
                        API.REMOVE_EXHIBITOR_MATERIALS + '?exhibitor_material_id=' + exhibitor_material_id,
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.getPromoCodes = function(conference_id) {
                    var deferred = $q.defer();
                    Request2.get(
                        API.GET_PROMO_CODES + '?conference_id=' + conference_id,
                        {
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.inviteUser = function(invitedUser, exhibitor_id) {
                    var deferred = $q.defer();

                    if(invitedUser.invited_user_email !== localStorageService.get('email')){
                        invitedUser.exhibitor_id = parseInt(exhibitor_id);
                    }

                    invitedUser = _.pickBy(invitedUser);
                    invitedUser.rights = [];

                    invitedUser.exhibitor_user_id = invitedUser.id;
                    if (invitedUser.edit_company_profile) {
                        invitedUser.rights.push('edit_company_profile');
                    }

                    if (invitedUser.scan_leads) {
                        invitedUser.rights.push('scan_leads');
                    }

                    if (invitedUser.manage_leads) {
                        invitedUser.rights.push('manage_leads');
                    }

                    if (invitedUser.download_leads) {
                        invitedUser.rights.push('download_leads');
                    }

                    Request2.post(
                        API.CREATE_EXHIBITOR_USER,
                        invitedUser,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                }

                service.exhibitorInitialPayNew = function(exhibitor, stripe_token, code){
                    var deferred = $q.defer();
                    var url = API.EXHIBITOR_PAY_NEW + exhibitor.id + '/';

                    var data = {
                        "type": 'initial_fee',
                        "stripe_token": stripe_token
                    }

                    if (code) {
                        data['code'] = code;
                    }

                    Request2.post(url, data, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

                service.exhibitorAddedUserPayNew = function(exhibitor, stripe_token, nr_users){
                    var deferred = $q.defer();
                    var url = API.EXHIBITOR_PAY_NEW + exhibitor.id + '/';

                    var data = {
                        "type": 'exhibitor_user_fee',
                        "stripe_token": stripe_token,
                        "nr_users": nr_users
                    }

                    Request2.post(url, data, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

				service.addExhibitor = function(exhibitor){
                    var deferred = $q.defer();
                    var url = API.CREATE_CONFERENCES_EXHIBITORS_URL;

                    exhibitor = _.pickBy(exhibitor);

                    Request2.post(url, exhibitor, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

                service.assignPromoCode = function(exhibitor, promoCode){
                    var deferred = $q.defer();

                    var data = {
                        "exhibitor_id": exhibitor.id,
                        "code": promoCode
                    }

                    Request2.post(API.ASSIGN_PROMO_CODE, data, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

                service.removeExhibitorUsersNew = function(exhibitor_id, exhibitor_users_ids, boolAll){
                    var deferred = $q.defer();
                    var url = API.DELETE_EXHIBITOR_USER + exhibitor_id + '/';

                    var exhibitor_user = {
                        'exhibitor_user_ids': exhibitor_users_ids,
                        'delete_all': boolAll
                    }

                    Request2.remove(url, exhibitor_user, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

                service.removeExhibitorUsers = function(exhibitor_id, exhibitor_users_ids, boolAll){
                    var deferred = $q.defer();
                    var url = API.DELETE_EXHIBITOR_USER + exhibitor_id;

                    if (boolAll) {
                        url = url + '?delete_all=true'
                    } else {
                        url = url + '?exhibitor_user_ids=' + exhibitor_users_ids.join()
                    }

                    Request2.remove(url, {}, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

                service.updateExhibitorUser = function(exhibitor_user){
					var deferred = $q.defer();
					var url = API.UPDATE_EXHIBITOR_USER;

					exhibitor_user = _.pickBy(exhibitor_user);
                    exhibitor_user.rights = [];

                    exhibitor_user.exhibitor_user_id = exhibitor_user.id;
                    if (exhibitor_user.edit_company_profile) {
                        exhibitor_user.rights.push('edit_company_profile');
                    }

                    if (exhibitor_user.scan_leads) {
                        exhibitor_user.rights.push('scan_leads');
                    }

                    if (exhibitor_user.manage_leads) {
                        exhibitor_user.rights.push('manage_leads');
                    }

                    if (exhibitor_user.download_leads) {
                        exhibitor_user.rights.push('download_leads');
                    }

					Request2.post(url, exhibitor_user, false, true)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            //console.log(error);
                            deferred.reject(error);
                        });
					return deferred.promise;
				};

				return service;
			}
})();
