//SharedProperties.js
(function(){
	'use strict';
	angular
		.module('app')
		.service('SharedProperties',
			SharedProperties
		);

	function SharedProperties(){
		var service = {};
		service.sharedObject = {};
		return service;
	}	
})();