//RequestFactory.js
(function(){
	'use strict';

	angular
		.module('app')
		.factory('Request2', [
			'$http', 'LoginService', 'BASE_URL', '$q', '$location',
			requestFactory2
		]);

	function requestFactory2($http, LoginService, BASE_URL, $q, $location){

		function joinEndpoints(parts){
			var separator='/';
			var replace = new RegExp(separator+'{1,}', 'g');
			return parts.join(separator).replace(replace, separator);
		}

		function addTrailingSlash(uri) {
			if (uri.indexOf('?') != -1) {
				return uri.replace('//','/');
			} else {
				var lastChar = uri.substr(-1); 
				if (lastChar != '/') {
				   uri = uri + '/';
				}
				return uri.replace('//','/');
			}
		}

		function urlEncode(data){
			var kvp = [];
			angular.forEach(data, function(value, key){
				kvp.push([key, encodeURIComponent(value)]);
        	});
        	var encodedData = kvp.map(function(pair) {
          		return pair.join('=');
        	}).join('&');

        	return encodedData;
		}

		return {
			get: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.get(uri + "?" + data, {
					    ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					})
					.catch(function(error){
						console.log(error.status);
					});
				} else {
					return $http.get(uri, {
					    ignoreLoadingBar: noLoader,
						params: data,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					})
					.then(function(response){
					    return response;
					})
					.catch(function(error){
                        if (error.status == 401) {
                        	LoginService.resetAuthData();
							$location.path("/login");
                        }
                        throw error;
                    });

				}
			},

			post: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.post(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
					return $http.post(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					});
				}
			},

			patch: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.patch(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
					return $http.patch(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					});
				}
			},

			remove: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
				var uri = BASE_URL + endpoint;
				uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
				
				if(encode){
					return $http.delete(uri, {
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
				    if (data && !_.isEmpty(data)) {
				        return $http.delete(uri, {
                            data: data,
														ignoreLoadingBar: noLoader,
                            headers: {
                                'Authorization': 'Bearer ' + LoginService.getAccessToken(),
                                'Accept': '*/*'
                            }
                        });
				    } else {
				        return $http.delete(uri, {
														ignoreLoadingBar: noLoader,
                            headers: {
                                'Authorization': 'Bearer ' + LoginService.getAccessToken(),
                                'Accept': '*/*'
                            }
                        });
				    }

				}
			}

		}
	}

}());