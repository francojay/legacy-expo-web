(function () {
    'use strict'
    var serviceId = 'manageNotificationsService';

    angular
        .module('app')
        .service(serviceId, ['$q', 'Request2', 'Upload', 'API', manageNotificationsService]);

    function manageNotificationsService($q, Request2, Upload, API) {
        var service = {};

        service.getRegistrationLevels = function (conferenceId) {
            var deferred = $q.defer();
            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/registration_levels/';
    
            Request2.get(url, {}, false, true)
                .then(function (response) {
                    deferred.resolve(response.data);
                }, function (error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.getScheduledNotifications = function (conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/?type=scheduled' ;

            Request2.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getSentNotifications = function (conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/?type=sent' ;

            Request2.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }


        service.buyCredits = function(conferenceId, amountOfNotificationCredits, stripeToken) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId  + '/notifications/payments/';

            amountOfNotificationCredits = parseInt(amountOfNotificationCredits)

            Request2.post(url, {
                stripe_token: stripeToken,
                nr_credits: amountOfNotificationCredits
            }, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(response);
            })

            return deferred.promise;
        }


        service.getPayments = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId  + '/notifications/payments/';

            Request2.get(url, {
            }, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(response);
            })

            return deferred.promise;
        }


        service.checkStatusOfNotificationSetup = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/status/'

            Request2.get(url, {}, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }

        service.scheduleNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            if (notification.id) {
                var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';
                Request2.patch(url, notification, false, true).then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                })
            } else {
                var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/';
                Request2.post(url, notification, false, true).then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                })
            }

            

            return deferred.promise;
        };

        service.removeScheduledNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';

            Request2.remove(url, notification, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.getScheduledNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';

            Request2.get(url, {}, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.calculateCost = function(conferenceId, levels) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/cost_calculator';

            Request2.post(url, levels, false, true).then(function(response) {
                deferred.resolve(response.data.cost);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }


        return service;
    }
})();