(function() {
    'use strict';

    var serviceId = 'ConferenceCoverScreensService';
    angular
        .module('app')
        .service(serviceId, ['Request2', 'API', '$q', ConferenceCoverScreensService]);
    function ConferenceCoverScreensService(Request2, API, $q) {
        var service = {};
        
        service.getDefaultScreens = function(){
            var deferred = $q.defer();
            Request2.get(API.GET_DEFAULT_COVER_SCREENS, {}, false)
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        return service;
    }
})();
