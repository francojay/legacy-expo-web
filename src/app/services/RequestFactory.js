//RequestFactory.js
(function(){
	'use strict';

	angular
		.module('app')
		.factory('Request', [
			'$http', '$auth', 'BASE_URL', '$q',
			requestFactory
		]);

	function requestFactory($http, $auth, BASE_URL, $q){
		
		function joinEndpoints(parts){
			var separator='/';
			var replace = new RegExp(separator+'{1,}', 'g');
			return parts.join(separator).replace(replace, separator);
		}

		function urlEncode(data){
			var kvp = [];
			angular.forEach(data, function(value, key){
				kvp.push([key, encodeURIComponent(value)]);
        	});
        	var encodedData = kvp.map(function(pair) {
          		return pair.join('=');
        	}).join('&');

        	return encodedData;
		}

		return {
			get: function(endpoint, data, url_encoded){
				var encode = url_encoded ? true : false;
				if(encode){ 
					var data = urlEncode(data);
				}
            	var uri = joinEndpoints([BASE_URL, endpoint + "/"])
            	
            	if(encode){

					$http.get(uri + "?" + data, {						
						headers: {
							'Authorization': 'Bearer ' + $auth.getToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*' 
						}
					})
					.then(function(response){						
						return $q.when(response.data);						
					})
					.catch(function(error){
						return $q.reject(error);
					});					
				} else {
					$http.get(uri, {
						params: data,						
						headers: {
							'Authorization': 'Bearer ' + $auth.getToken(),							
							'Accept': '*/*' 
						}
					})
					.then(function(response){						
						return $q.when(response.data);						
					})
					.catch(function(error){
						return $q.reject(error);
					});
				}
			},

			post: function(endpoint, data, url_encoded){
				var encode = url_encoded ? true : false;
				if(encode){ 
					var data = urlEncode(data);
				}
            	var uri = joinEndpoints([BASE_URL, endpoint + "/"])
            	

            	if(encode){

					$http.get(uri, data, {						
						headers: {
							'Authorization': 'Bearer ' + $auth.getToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*' 
						}
					})
					.then(function(response){						
						return $q.when(response.data);						
					})
					.catch(function(error){
						return $q.reject(error);
					});					
				} else {
					$http.get(uri, data, {				
						headers: {
							'Authorization': 'Bearer ' + $auth.getToken(),							
							'Accept': '*/*' 
						}
					})
					.then(function(response){						
						return $q.when(response.data);						
					})
					.catch(function(error){
						return $q.reject(true);
					});
				}
			}

		}
	}

})();