(function(){
	'use strict';

	angular
		.module('app')
		.service('LoginService', [
			'$q', '$http', 'API', 'localStorageService', '$location', loginService
		]);
		function loginService($q, $http, API, localStorageService, $location){
			var service = {};

			service.isAuthenticated = function() {
			    var access_token = localStorageService.get('access_token'),
                expires_date = localStorageService.get('expires_in'),
                date = new Date();
			    if (access_token == null) {
                    return false
                }
                var expires_at = localStorageService.get('expires_at');
                if (!expires_at) {
                    return false;
                }
                expires_at = new Date(expires_at);
                if (Date() >= expires_at) {
                    this.refresh()
                        .then(function(refreshResponse){
                            return true;
                        })
                        .catch(function(error){
                            return false;
                        })
                }
                else {
                    return true;
                }
			}

            service.resetAuthData = function() {
                localStorageService.clearAll();
            }

			service.authenticate = function(username, password) {
        var deferred = $q.defer();
		    var user = {
          username: username,
          password: password,
          client_id: API.CLIENT_ID_DEV,
          grant_type: 'password'
	      };

        $http.post(API.AUTH_URL_DEV, JSON.stringify(user),{
        	headers: {
						'Content-Type': 'application/json'
          }
        }).then(function (response) {
          if (response.data.is_not_verified === true) {

					} else {
          	localStorageService.clearAll();
            localStorageService.set('access_token', response.data.access_token);
            localStorageService.set('refresh_token', response.data.refresh_token);
            localStorageService.set('expires_in', response.data.expires_in);
            if (response.data.is_superuser) {
                localStorageService.set('is_superuser', response.data.is_superuser);
            }
            service.email_address_of_current_user = localStorageService.get('email');
            var timeObject = new Date();
            timeObject = new Date(timeObject.getTime() + 1000*(response.data.expires_in - 1800));
          	localStorageService.set('expires_at', timeObject);
        	}
          deferred.resolve(response.data);
        }, function(error) {
					deferred.reject(error);
				});
        return deferred.promise;
      }

      service.getAccessToken = function() {
          var access_token = localStorageService.get('access_token');
          return access_token;
      }

      service.getRefreshToken = function() {
          var refresh_token = localStorageService.get('refresh_token');
          return refresh_token;
      }

      service.getUsername = function() {
          var user = JSON.parse(localStorage.currentUser);
          return user.username;
      }

      service.refresh = function(user) {
        var deferred = $q.defer();
        var refresh_token = localStorageService.get('refresh_token');
        var request_data  = {
            username: user.username,
            refresh_token: refresh_token,
            grant_type: 'refresh_token',
            client_id: API.CLIENT_ID_DEV
        }
        $http.post(API.AUTH_URL_DEV, JSON.stringify(request_data), {
        	headers: {
						'Content-Type': 'application/json'
					}
				}).then(function (data) {
          deferred.resolve(data.data);
        }, function(error) {
					deferred.reject(error);
				});

        return deferred.promise;
      }

      service.forgotPassword = function(email_address){
          var deferred = $q.defer();

          $http.post(
          API.BASE_URL + API.FORGOT_PASSWORD_URL,
          {'email_address' : email_address},
          {
              headers: {
                  'Content-Type': 'application/json'
              }
          }
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });

          return deferred.promise;
      }

      service.resetPassword = function(newData){
          var deferred = $q.defer();

          $http.post(
          API.BASE_URL + API.RESET_ACCOUNT_PASSWORD_URL,
          newData,
          {
              headers: {
                  'Content-Type': 'application/json'
              }
          })
          .then(function(response){
              localStorageService.set('access_token', response.data.access_token);
              localStorageService.set('refresh_token', response.data.refresh_token);
              localStorageService.set('expires_in', response.data.expires_in);
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });

          return deferred.promise;
      }

			return service;
		}
})();
