(function() {
    'use strict';
    angular
        .module('app')
        .service('ConferenceRegistrationService', ['Request2', 'API', '$q', '$http', conferenceRegistrationService]);

    function conferenceRegistrationService(Request2, API, $q, $http) {
        var service = {};

        service.updateRegisteredAttendee = function(updated_regitered_attendee) {
            var deferred = $q.defer();
            $http.post(
                    API.BASE_URL + API.UPDATE_REGISTERED_ATTENDEE + updated_regitered_attendee.id + '/', updated_regitered_attendee, false, true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.continueRegistration = function(pk) {
            var deferred = $q.defer();
            $http.get(
                    API.BASE_URL + API.CONTINUE_REGISTRATION + pk + '/', {}, {})
                .then(function(response) {
                    deferred.resolve(response.data);
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        //not used
        service.getRegistrationForm = function(conference_id) {
            var deferred = $q.defer();
            Request2.get(
                    API.GET_REGISTRATION_FORM + "?conference_id=" + conference_id, {}, false, true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.getRegistrationFormV1 = function(conference_id) {
            var deferred = $q.defer();
            Request2.get(
                    API.REGISTRATION_FORMS + conference_id + '/', {}, false, true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.registerStripe = function(stripeToken) {
            var deferred = $q.defer();
            Request2.post(
                    API.USER_REGISTER_PAYMENT, {
                        stripeToken: stripeToken
                    },
                    false
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        service.bulkCreateMembers = function(payload, registration_level_id) {
            var deferred = $q.defer();

            var data = {
                members: payload
            }

            Request2.post(
                    API.REGISTRATION_LEVEL_MEMBERS_CRUD + registration_level_id + '/',
                    data,
                    false
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.registerAttendee = function(payload, conference_id) {
            var deferred = $q.defer();

            $http.post(
                    API.BASE_URL + API.REGISTRATION_FORM_CALL + conference_id + '/', payload, { ignoreLoadingBar: true })
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });


            // Request2.post(
            //   API.REGISTRATION_FORM_CALL + conference_id + '/',
            //   payload,
            //   false
            // )
            // .then(function(response){
            //   deferred.resolve(response.data);
            // })
            // .catch(function(error){
            //   deferred.reject(error);
            // });
            return deferred.promise;
        };

        service.processPromoCode = function(promo_code, level_id) {
            var deferred = $q.defer();

            $http.get(
                    API.BASE_URL + API.VALIDATE_PROMO_CODE + level_id + '/?promo_code=' + promo_code, {}, { ignoreLoadingBar: true })
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });

            // Request2.get(
            //   API.VALIDATE_PROMO_CODE + conference_id + '/',
            //   {
            //     promo_code: promo_code
            //   },
            //   false
            // )
            // .then(function(response){
            //   deferred.resolve(response.data);
            // })
            // .catch(function(error){
            //   deferred.reject(error);
            // });
            return deferred.promise;
        };

        service.getRegistrationTransfers = function(conference_id, page, q) {
            var deferred = $q.defer();

            var payload = {
                page: page
            }

            if (q) {
                payload.q = q;
            }

            Request2.get(
                    API.REGISTRATION_TRANSFERS + conference_id + '/',
                    payload,
                    false,
                    true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.registrationStats = function(conference_id) {
            var deferred = $q.defer();

            Request2.get(API.REGISTRATION_FORM_STATS + conference_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.getRegistrationMembers = function(registration_level_id) {
            var deferred = $q.defer();

            Request2.get(
                    API.REGISTRATION_LEVEL_MEMBERS_CRUD + registration_level_id + '/', {},
                    true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.removeRegistrationMembers = function(registration_level_id, members_ids) {
            var deferred = $q.defer();
            members_ids = members_ids.replace('/,/g', '%2C');
            Request2.remove(
                    API.REGISTRATION_LEVEL_MEMBERS_CRUD + registration_level_id + '/' + '?member_ids=' + members_ids, {},
                    false
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getRegistrationFormUnauthorized = function(conference_id, call_number) {
            var deferred = $q.defer();
            // Request2.get(
            //     API.REGISTRATION_OPEN + "?conference_id=" + conference_id, {}, false
            // )
            // .then(function(response){
            //     deferred.resolve(response.data);
            // })
            // .catch(function(error){
            //     deferred.reject(error);
            // });
            // return deferred.promise;

            var endpoint_call = call_number === 1 ? API.REGISTRATION_FIRST_CALL : API.REGISTRATION_SECOND_CALL;

            $http.get(API.BASE_URL + endpoint_call + conference_id + '/', { ignoreLoadingBar: true })
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        service.getRegistrationLevelsMemberTestUnauthorized = function(registration_id, email) {
            var deferred = $q.defer();

            var data = {
                "email": email
            }

            $http.post(API.BASE_URL + API.REGISTRATION_LEVEL_MEMBER_TEST + registration_id + '/', data, { ignoreLoadingBar: true })
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        service.startRegistrationForm = function(conference_id) {
            var deferred = $q.defer();
            var data = {
                conference_id: conference_id
            };
            Request2.post(API.START_REGISTRATION_FORM, data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.uploadRegistrationFormAdditionalLegalAgreements = function(registration_form_id, file_link, file_name, file_type) {
            var deferred = $q.defer();
            var data = {
                'file_url': file_link
            };

            if (file_name) {
                data['file_name'] = file_name;
            }

            if (file_type) {
                data['file_type'] = file_type;
            }
            Request2.post(API.REGISTRATION_FORM_DOCUMENTS_CRUD + registration_form_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.uploadRegistrationFormLegalAgreements = function(registration_form_id, legal_agreement, type, file_name, file_type) {
            var deferred = $q.defer();
            if (type == 'terms_and_conditions') {
                var data = {
                    'terms_of_service': legal_agreement
                };
                if (file_name) {
                    data['terms_of_service_file_name'] = file_name;
                }
                if (file_type) {
                    data['terms_of_service_file_type'] = file_type;
                }
            } else if (type == 'refund_policy') {
                var data = {
                    'refund_policy': legal_agreement
                };
                if (file_name) {
                    data['refund_policy_file_name'] = file_name;
                }
                if (file_type) {
                    data['refund_policy_file_type'] = file_type;
                }
            }
            Request2.post(API.REGISTRATION_FORM_UPDATE + registration_form_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.registrationFormRecommendedLevel = function(registration_form_id, data) {
            var deferred = $q.defer();
            Request2.post(API.REGISTRATION_FORM_UPDATE + registration_form_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.registratioFormComplete = function(registration_form_id) {
            var deferred = $q.defer();
            var data = {};
            Request2.post(API.REGISTRATION_FORM_COMPLETE + registration_form_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.setRegistrationCSS = function(registration_form, custom_css, custom_css_file_name, custom_css_file_date) {
            var deferred = $q.defer();

            var data = {
                'custom_css': custom_css,
                'custom_css_file_name': custom_css_file_name,
                'custom_css_file_date': custom_css_file_date,
                'terms_of_service': registration_form.terms_of_service,
                'terms_of_service_file_name': registration_form.terms_of_service_file_name,
                'terms_of_service_file_type': registration_form.terms_of_service_file_type,
                'refund_policy': registration_form.refund_policy,
                'refund_policy_file_name': registration_form.refund_policy_file_name,
                'refund_policy_file_type': registration_form.refund_policy_file_type,
                'tracking_code': registration_form.tracking_code
            };

            Request2.post(API.REGISTRATION_FORM_UPDATE + registration_form.id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.setTrackingCode = function(registration_form) {
            var deferred = $q.defer();

            var data = {
                'tracking_code': registration_form.tracking_code
            };

            Request2.post(API.REGISTRATION_FORM_UPDATE + registration_form.id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getFileContent = function(file_link) {
            var deferred = $q.defer();
            $http({
                    method: "GET",
                    url: file_link
                })
                .then(function(data) {
                    deferred.resolve(data);
                }).catch(function(error) {
                    console.log(error);
                    deferred.reject(data);
                });

            return deferred.promise;
        }

        service.setRegistrationFormFields = function(registration_form_id, standard_fields, custom_fields) {
            var deferred = $q.defer();
            var data = {
                'registration_form_id': registration_form_id,
                'standard_fields': standard_fields,
                'custom_fields': custom_fields
            };
            Request2.post(API.SET_REGISTRATION_FIELDS, data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getRegistrationLevels = function(registration_form_id) {
            var deferred = $q.defer();

            Request2.get(API.CREATE_REGISTRATION_FORM_LEVEL + registration_form_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.deleteRegistrationLevels = function(registration_level_id) {
            var deferred = $q.defer();

            Request2.remove(API.REGISTRATION_LEVEL + registration_level_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.updateRegistrationLevel = function(level) {
            var deferred = $q.defer();

            Request2.post(API.REGISTRATION_LEVEL + level.id + '/', level, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }
        service.addSessionToRegistrationLevel = function(level_id, sessions) {

            var deferred = $q.defer();
            Request2.post(API.SESSIONS_REGISTRATION_LEVEL + level_id + '/', sessions, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.removeSessionToRegistrationLevel = function(level_id, sessions) {

            var deferred = $q.defer();
            Request2.remove(API.SESSIONS_REGISTRATION_LEVEL + level_id + '/?session_ids=' + sessions.join(), {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.addBenefit = function(level_id, benefit) {
            var deferred = $q.defer();

            Request2.post(API.BENEFITS_CRUD + level_id + '/', benefit, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.addPricing = function(level_id, pricing) {
            var deferred = $q.defer();

            var url = API.PRICINGS_CRUD + level_id + '/';

            if (pricing.id) {
                url = API.PRICING_CRUD + pricing.id + '/';
            }

            Request2.post(url, pricing, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.updatePricing = function(pricing) {
            var deferred = $q.defer();

            Request2.post(API.PRICING_CRUD + pricing.id + '/', pricing, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.deletePricing = function(level_pricing_id) {
            var deferred = $q.defer();

            Request2.remove(API.PRICING_CRUD + level_pricing_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.updateBenefit = function(benefit) {
            var deferred = $q.defer();

            Request2.post(API.BENEFIT_CRUD + benefit.id + '/', benefit, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.removeBenefit = function(benefit) {
            var deferred = $q.defer();

            Request2.remove(API.BENEFIT_CRUD + benefit.id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.createRegsitrationFormLevels = function(registration_form_id, level) {
            var deferred = $q.defer();
            var data = {
                'registration_form': registration_form_id,
                'name': level.name
            };

            Request2.post(API.CREATE_REGISTRATION_FORM_LEVEL + registration_form_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.createRegistrationFormBenefits = function(registration_level_id, benefit) {
            var deferred = $q.defer();

            Request2.post(API.CREATE_REGISTRATION_FORM_BENEFITS + registration_level_id + '/', benefit, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getRegistrationFormBenefits = function(registration_level_id) {
            var deferred = $q.defer();

            Request2.get(API.CREATE_REGISTRATION_FORM_BENEFITS + registration_level_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.createRegistrationAddOnSession = function(registration_level_id, add_on_session_id) {
            var deferred = $q.defer();
            var data = {
                'session': add_on_session_id
            };
            Request2.post(API.ADD_ON_SESSIONS_CRUD + registration_level_id + '/', data, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.removeRegistrationAddOnSession = function(add_on_session_id) {
            var deferred = $q.defer();

            Request2.remove(API.ADD_ON_SESSION_CRUD + add_on_session_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.createRegistrationAddOnSessionPricing = function(add_on_session_id, add_on_session_model) {
            var deferred = $q.defer();

            Request2.post(API.ADD_ON_SESSION_PRICINGS_CRUD + add_on_session_id + '/', add_on_session_model, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.updateRegistrationAddOnSessionPricing = function(add_on_session_pricing_id, add_on_session_model) {
            var deferred = $q.defer();
            Request2.post(API.ADD_ON_SESSION_PRICING_CRUD + add_on_session_pricing_id + '/', add_on_session_model, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.removeRegistrationAddOnSessionPricing = function(add_on_session_pricing_id) {
            var deferred = $q.defer();
            Request2.remove(API.ADD_ON_SESSION_PRICING_CRUD + add_on_session_pricing_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.addRegistrationPromoCodes = function(level_id, promo_code) {
            var deferred = $q.defer();
            Request2.post(API.REGISTRATION_LEVEL_PROMO_CODES_CRUD + level_id + '/', promo_code, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.removeRegistrationPromoCode = function(promo_code_id) {
            var deferred = $q.defer();
            Request2.remove(API.REGISTRATION_LEVEL_PROMO_CODE_CRUD + promo_code_id + '/', {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.updateRegistrationPromoCode = function(promo_code_id, promo_code) {
            var deferred = $q.defer();
            Request2.post(API.REGISTRATION_LEVEL_PROMO_CODE_CRUD + promo_code_id + '/', promo_code, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.createRegistrationOrder = function(conference_id, data) {
            var deferred = $q.defer();
            $http.post(
                    API.BASE_URL + 'api/v1.1/conferences/' + conference_id + '/registration_form/orders/',
                    data,
                    false,
                    true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.updateRegistrationOrder = function(conference_id, order, data) {
            var deferred = $q.defer();
            $http.put(
                    API.BASE_URL + 'api/v1.1/conferences/' + conference_id + '/registration_form/orders/' + order.id + '/',
                    data, {
                        headers: {
                            'Authorization': order.code
                        }
                    },
                    true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.get_FIS_HPPage = function(conference_id, order) {
            var deferred = $q.defer();
            $http.put(
                    API.BASE_URL + 'api/v1.1/conferences/' + conference_id + '/registration_form/orders/' + order.id + '/complete/', {
                        "attendee_registrations": order.order_data,
                        "return_url": order.return_url,
                        "payment_mode": order.payment_mode
                    }, {
                        headers: {
                            'Authorization': order.code
                        }
                    },
                    true
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.getRegistrationOrder = function(conference_id, order_code) {
            var deferred = $q.defer();
            $http.get(
                    API.BASE_URL + 'api/v1.1/conferences/' + conference_id + '/registration_form/orders', {
                        headers: {
                            'Authorization': order_code
                        }
                    }, {}
                )
                .then(function(response) {
                    console.log(response);
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        return service;
    }
})();
