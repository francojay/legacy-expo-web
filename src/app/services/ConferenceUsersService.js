(function(){
	'use strict';
		angular
			.module('app')
			.service('ConferenceUsersService', ['Request2', 'API', '$q', conferenceUsersService
				]);
			function conferenceUsersService(Request2, API, $q){
				var service = {};

				service.getAllUsers = function(conference_id){
					return Request2.get(
	                    API.GET_ALL_CONFERENCE_USERS_URL + "?conference_id=" + conference_id, {
	                    },
	                    false
	                )
	                .then(function(response){
	                    return  response.data;
	                })
	                .catch(function(error){
	                    console.log(error);
	                    return error;
	                });
				};

				service.removeUser = function(array_users_id){
                    var deferred = $q.defer();
                    var join_ids = _.join(array_users_id, ',');
                    Request2.remove(
                        API.REMOVE_CONFERENCE_USER_URL + '?conference_user_id=' + array_users_id[0],
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.buyConferenceUsers = function(conference_id, nr_users, stripeToken) {
                    var deferred = $q.defer();
                    console.log(stripeToken);
                    Request2.post(
                        API.BUY_CONFERENCE_USERS,
                        {
                            conference_id: conference_id,
                            nr_users: nr_users,
                            stripeToken: stripeToken
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                }


                service.removeMultipleUsers = function(conference_id, array_users_id){
                    var deferred = $q.defer();
                    var data = {
                        conference_id: conference_id,
                        conference_user_ids: array_users_id,
                        remove_all:false
                    }
                    Request2.post(
                        API.CONFERENCE_REMOVE_MULTIPLE_USERS,
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.removeAllUsers = function(conference_id){
                    var deferred = $q.defer();
                    var data = {
                        conference_id: conference_id,
                        conference_user_ids: [],
                        remove_all:true
                    }
                    Request2.post(
                        API.CONFERENCE_REMOVE_MULTIPLE_USERS,
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.updateUserRights = function(conference_user_id, rights){
                	var deferred = $q.defer();
                    Request2.post(
                        API.UPDATE_USER_RIGHTS,
                        {
                        	'conference_user_id': conference_user_id,
                        	'rights': rights
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				return service;
			}
})();