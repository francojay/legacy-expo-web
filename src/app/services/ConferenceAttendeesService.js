(function(){
	'use strict';
		angular
			.module('app')
			.service('ConferenceAttendeesService',['Request2', 'API', '$q', 'toastr', conferenceAttendeesService
			]);
			function conferenceAttendeesService(Request2, API, $q, toastr){
				var service = {};

				service.getAttendeeById = function(attendee_id){
                    var deferred = $q.defer();
                    Request2.get(API.GET_ATTENDEE_BY_ID + attendee_id + '/', {}, false)
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            console.log(error);
                            deferred.reject(error);
                        });
                    return deferred.promise;
                };

				service.getAllAttendees = function(conference_id, page, noLoader, attendeeFilters){
				    noLoader = noLoader || false;
				    attendeeFilters = attendeeFilters || {
				        queryString: null,
				        sortBy: null,
				        sortOrder: null
				    };
					var deferred = $q.defer();
					var query_url = API.GET_ALL_CONFERENCES_ATTENDEES_URL + "?conference_id=" 	+ conference_id + '&page=' + page;
					if (attendeeFilters.sortBy) {
					    if (attendeeFilters.queryString) {
					        query_url = API.GET_ALL_CONFERENCES_ATTENDEES_URL + "?conference_id=" 	+ conference_id + '&page=' + page
                            					    + "&sort_by=" + attendeeFilters.sortBy
                            					    + "&sort_order=" + attendeeFilters.sortOrder
                            					    + "&q=" + attendeeFilters.queryString
                                                    + '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
                                                    + '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
					    }
					    else {
					        query_url = API.GET_ALL_CONFERENCES_ATTENDEES_URL + "?conference_id=" 	+ conference_id + '&page=' + page
                            					    + "&sort_by=" + attendeeFilters.sortBy
                            					    + "&sort_order=" + attendeeFilters.sortOrder
                                                    + '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
                                                    + '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
					    }

					} else {
                        if (attendeeFilters.queryString) {
                            query_url = API.GET_ALL_CONFERENCES_ATTENDEES_URL + "?q=" + attendeeFilters.queryString
                        }
					}

					Request2.get(query_url, {}, false, noLoader)
	                .then(function(response){
	                    deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    console.log(error);
	                    deferred.reject(error);
	                });
	                return deferred.promise;
				};

                service.getAttendeePayments = function(attendee_id){
                    var deferred = $q.defer();
                    Request2.get(API.ATTENDEE_PAYMENTS + attendee_id + '/', {}, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.refundAttendeePayment = function(attendee_id, amount){
                    var deferred = $q.defer();
                    Request2.post(API.ATTENDEE_REFUND_PAYMENT + attendee_id + '/', {'amount' : amount}, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.downloadAttendees = function(conference_id, attendee_ids, extra, page) {
                        var deferred = $q.defer();
                        var url = API.DOWNLOAD_ATTENDEES + conference_id + '/';
                        if (page) {
                                url = url + '?page=' + page;
                        } else {
                                url = url + '?page=1';
                        }

                        if (attendee_ids && attendee_ids.length > 0) {
                                url = url + "&attendee_ids=" + attendee_ids;
                                if (extra) {
                                        url = url + "&" + extra;
                                }
                        } else {
                                if (extra) {
                                        url = url + "&" + extra;
                                }
                        }

                        Request2.get(url)
                        .then(function(response){
                                deferred.resolve(response.data);
                        })
                        .catch(function(error){
                                console.log(error);
                                deferred.reject(error);
                        });
                        return deferred.promise;
                }

				service.getAllCustomFields = function(conference_id){
                    var deferred = $q.defer();
                    Request2.get(API.GET_ALL_CUSTOM_FIELDS_URL + "?conference_id=" + conference_id, {}, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.listAttendeeSessions = function(attendee_id){
                    var deferred = $q.defer();
                    Request2.get(API.LIST_ATTENDEE_SESSIONS + "?attendee_id=" + attendee_id, {}, false, true)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.listScannedSessions = function(attendee_id){
                    var deferred = $q.defer();
                    Request2.get(API.LIST_SCANNED_SESSIONS + "?attendee_id=" + attendee_id, {}, false, true)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.deleteCustomField = function(conference_id){
                    var deferred = $q.defer();
                    Request2.remove(API.DELETE_CUSTOM_FIELDS_URL + "?id=" + conference_id, {}, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.createCustomField = function(custom_field){
                    var url = API.CREATE_CUSTOM_FIELDS_URL;
                    if (custom_field.id) {
                        url = API.UPDATE_CUSTOM_FIELDS_URL;
                    }
                    var deferred = $q.defer();
                    Request2.post(url, custom_field, false)
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				service.getDefaultAttendeeFields = function() {
				    return $q.when([
                        {
                            code: 'full_name',
                            title: 'Attendee'
                        },
                        {
                            code: 'email_address',
                            title: 'Email'
                        },
                        {
                            code: 'phone_number',
                            title: 'Phone'
                        },
                        {
                            code: 'company_name',
                            title: 'Company'
                        },
                        {
                            code: 'qr_code',
                            title: 'QR Code'
                        }
                    ]);
				}

                var formLabels = [
                    {label:"First Name", code:'first_name', type: "text", valid: "true"},
                    {label:"Last Name", code:'last_name', type: "text", valid: "true" },
                    {label:"Job Title", code:'job_title', type: "text"},
                    {label:"Company Name", code:'company_name', type: "text"},
                    {label:"Email Address", code:'email_address', type: "text"},
                    {label:"Phone Number", code:'phone_number', type: "text"},
                    {label:"Street Address", code:'street_address', type: "text"},
                    {label:"City", code:'city', type: "text"},
                    {label:"Zip Code", code:'zip_code', zipCodeType: "[0-9]{5}"},
                    {label:"State", code:'state', placeH: "--", type: "text"},
                    {label:"Country", code:'country', placeH: "Choose a Country", type: "text"},
                    {label:"Attendee ID", code:'attendee_id', type: "text"}
               ];

				service.getCreateAttendeeFields = function() {
				    return $q.when(formLabels);
				};

				service.validateNewAttendee = function(newAttendee, conference){

                    if(newAttendee.first_name && newAttendee.last_name && !(conference.rule_type=='custom_id' && !newAttendee.attendee_id)){
                        var firstLetter = '';
                        if(newAttendee.first_name.charAt(0) !== newAttendee.first_name.charAt(0).toUpperCase()){
                                firstLetter = newAttendee.first_name.charAt(0);
                                newAttendee.first_name = newAttendee.first_name.replace(firstLetter, firstLetter.toUpperCase());
                            }
                        if(newAttendee.last_name.charAt(0) !== newAttendee.last_name.charAt(0).toUpperCase()){
                                firstLetter = newAttendee.last_name.charAt(0);
                                newAttendee.last_name = newAttendee.last_name.replace(firstLetter, firstLetter.toUpperCase());
                        }
                        if(newAttendee.email_address && newAttendee.email_address.charAt(0) === newAttendee.email_address.charAt(0).toUpperCase()){
                                firstLetter = newAttendee.email_address.charAt(0);
                                newAttendee.email_address = newAttendee.email_address.replace(firstLetter, firstLetter.toLowerCase());
                        }
                        if(newAttendee.attendee_id && (newAttendee.attendee_id.length < 2)){
                            toastr.error("The attendee id must contain minimum 2 characters!"); return;
                        }
                        return $q.when(newAttendee);
                    } else {
                        if (!newAttendee.first_name) {
                            toastr.error("The first name field is required"); return;
                        }
                        if (!newAttendee.last_name) {
                            toastr.error("The last name field is required"); return;
                        }
                        if(!newAttendee.email_address && conference.exhibitor_only == false){
                            toastr.error("Email Adress field is required"); return;
                        }
                        if (conference.rule_type=='custom_id' && !newAttendee.attendee_id) {
                            toastr.error("The attendee id is a required field for this conference!"); return;
                        }
                        if(newAttendee.attendee_id && (newAttendee.attendee_id.length < 2)){
                            toastr.error("The attendee id must contain minimum 2 characters!"); return;
                        }
                        newAttendee = {};
                        return $q.when(newAttendee);
                    }

				};

				service.addAttendee = function(newValidAttendee){
					var deferred = $q.defer();
					if (!newValidAttendee.first_name || !newValidAttendee.last_name) return;
					Request2.post(
						API.CREATE_CONFERENCE_ATTENDEE_URL,
						newValidAttendee,
						false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
				};

				service.bulkCreateAttendees = function(payload, conference_id){
                    var deferred = $q.defer();

                    var data = {
                        conference_id: conference_id,
                        attendees: payload
                    }

                    Request2.post(
                        API.CONFERENCE_BULK_CREATE_ATTENDEE,
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.bulkCreateAttendeesInSession = function(payload, conference_id, session_id){
					var deferred = $q.defer();

					var data = {
					    conference_id: conference_id,
					    attendees: payload
					}

					Request2.post(
						API.CONFERENCE_BULK_CREATE_ATTENDEE_IN_SESSION + session_id + '/',
						data,
						false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
				};


				service.removeAttendee = function(array_attendees_id, conference_id){

                    var deferred = $q.defer();
                    Request2.post(
                        API.REMOVE_CONFERENCE_ATTENDEE_URL,
                        {
                        'conference_id' : conference_id,
                        'attendee_ids': array_attendees_id,
                        'remove_all': false
                        },
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.removeAllAttendee = function(conference_id){

                    var deferred = $q.defer();

                    var data = {
                        conference_id: conference_id,
                        attendee_ids: [],
                        remove_all:true
                    }

                    Request2.post(
                        API.CONFERENCE_REMOVE_MULTIPLE_ATTENDEES,
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				service.updateAttendee = function(newValidAttendee){
                    var deferred = $q.defer();
                    if (!newValidAttendee.first_name) return;

                    Request2.post(
                        API.UPDATE_CONFERENCE_ATTENDEE_URL,
                        newValidAttendee,
                        false
                    )
                    .then(function(response){
                        toastr.success('The Attendee\'s data was updated successfully.', 'Attendee Updated!');
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        // toastr.error('There was a problem with your request');
                        toastr.error(error.data.detail);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				service.addAttendeeMaterial = function(material) {
					var deferred = $q.defer();
					var url = '/api/v1.1/conferences/' + material.conference_id + '/attendee_materials/';
					Request2.post(
							url,
							material,
							false
					)
					.then(function(response){
							deferred.resolve(response.data);
					})
					.catch(function(error){
							deferred.reject(error);
					});

					return deferred.promise;
				};

				service.getAttendeeMaterials = function(conferenceId) {
					var deferred = $q.defer();
					var url = '/api/v1.1/conferences/' + conferenceId + '/attendee_materials/';

					Request2.get(url, {}, false)
							.then(function(response){
									deferred.resolve(response.data);
							})
							.catch(function(error){
									deferred.reject(error);
							});
					return deferred.promise;
				};

				service.removeAttendeeMaterial = function(conferenceId, attendeeMaterialId) {
						var deferred = $q.defer();
						var url = '/api/v1.1/conferences/' + conferenceId + '/attendee_materials/' + attendeeMaterialId + '/';
						Request2.remove(
								url,
								{},
								false
						)
						.then(function(response) {
								deferred.resolve(response.data);
						})
						.catch(function(error){
								deferred.reject(error);
						});

						return deferred.promise;
				}

				return service;
			}
})();
