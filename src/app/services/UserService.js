(function() {
    'use strict'

    angular
        .module('app')
        .service('UserService', [
            '$q', 'API', 'CLIENT_ID_DEV', '$http', 'localStorageService', 'requestFactory', 'SharedProperties', userService
        ]);

    function userService($q, API, CLIENT_ID_DEV, $http, localStorageService, requestFactory, SharedProperties) {
        var service = {};
        service.accountNotConfirmed = false;

        service.getProfile = function() {
          var deferred = $q.defer();
          var url = 'api/account/profile/';
            requestFactory.get(url, {}, false).then(function(response) {
                    var user = response.data;
                    SharedProperties.user = user;
                    if (user.hasOwnProperty('is_superuser')) {
                        localStorageService.set('is_superuser', user.is_superuser);
                    }
                    var token = localStorageService.get('token');
                    if (token) {
                        service.joinConference(token)
                            .then(function(response) {
                                localStorageService.set('token', null);
                            })
                            .catch(function(error) {
                                console.log(error);
                            });
                    }
                    deferred.resolve(user);
                })
                .catch(function(error) {
                    deferred.reject(error.status);
                });

            return deferred.promise;
        }

        service.joinConference = function(token) {
            var deferred = $q.defer();
            requestFactory.post(
                    API.CONFERENCE_USER_JOIN, {
                        token: token
                    },
                    false
                )
                .then(function(response) {
                    deferred.resolve(response);
                })
                .catch(function(error) {
                    deferred.reject(error.status);
                });

            return deferred.promise;
        }

        service.resendVerificationCode = function(email) {
            var deferred = $q.defer();

            var userData = {
                email: email
            }

            $http.post(
                API.BASE_URL + API.RESEND_VERIFICATION_CODE,
                JSON.stringify(userData), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            ).then(function(response) {
                deferred.resolve(response);
            }).catch(function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        service.updateProfile = function(user) {
            var deferred = $q.defer();
            requestFactory.post(
                    API.UPDATE_USER_PROFILE,
                    user,
                    false
                )
                .then(function(response) {
                    deferred.resolve(response.data);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        service.storeInviteToken = function(token) {
            localStorageService.set('token', token);
        }

        service.register = function(user) {
            var deferred = $q.defer();
            user = validateAccountInformation(user);
            var userData = {
                username: user.username,
                password: user.password,
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email,
                client_id: CLIENT_ID_DEV,
                affid: user.affid,
                sid: user.sid,
                has_agreed_to_terms : true
            };
            $http.post(
                API.REGISTER_URL_DEV,
                JSON.stringify(userData), {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            ).then(function(response) {
                localStorageService.set('username', response.data.username);
                localStorageService.set('email', response.data.email);
                localStorageService.set('first_name', response.data.first_name);
                localStorageService.set('last_name', response.data.last_name);
                deferred.resolve(response.data);
            }).catch(function(error) {
                console.log(error);
                deferred.reject(error);
            });

            return deferred.promise;
        }

        service.verify = function(infoAccount) {
            var deferred = $q.defer();

            $http.post(
                    API.BASE_URL + API.VALIDATE_ACCOUNT_URL,
                    JSON.stringify(infoAccount), {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }
                )
                .then(function(response) {
                    deferred.resolve(response);
                })
                .catch(function(error) {
                    deferred.reject(error);

                });
            return deferred.promise;
        }

        function validateAccountInformation(user) {
            if (user.first_name && user.last_name && user.email) {
                var firstLetter = '';
                if (user.first_name.charAt(0) !== user.first_name.charAt(0).toUpperCase()) {
                    firstLetter = user.first_name.charAt(0);
                    user.first_name = user.first_name.replace(firstLetter, firstLetter.toUpperCase());
                }
                if (user.last_name.charAt(0) !== user.last_name.charAt(0).toUpperCase()) {
                    firstLetter = user.last_name.charAt(0);
                    user.last_name = user.last_name.replace(firstLetter, firstLetter.toUpperCase());
                }
                if (user.email && user.email.charAt(0) === user.email.charAt(0).toUpperCase()) {
                    firstLetter = user.email.charAt(0);
                    user.email = user.email.replace(firstLetter, firstLetter.toLowerCase());
                }
                user.username = user.email;
                return user;
            }
        }

        return service;
    }
})();
