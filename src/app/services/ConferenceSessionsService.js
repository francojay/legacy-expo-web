(function(){
	'use strict';
		angular
			.module('app')
			.service('ConferenceSessionsService', ['ConferenceService', 'Request2', 'API', '$q', conferenceSessionsService]);
			
			function conferenceSessionsService(ConferenceService, Request2, API, $q){
				var service = {};
				service.hash_session_id = new Hashids('Expo', 7);

				service.getAllSessions = function(conference_id){
					conference_id = ConferenceService.hash_conference_id.decode(conference_id)[0];
					var deferred = $q.defer();
					Request2.get(
						API.GET_ALL_CONFERENCE_SESSIONS_URL + "?conference_id=" + conference_id, {}, false, true
                    )
	                .then(function(response){
	                	deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

                service.reloadAllSessions = function(conference_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.GET_ALL_CONFERENCE_SESSIONS_URL + "?conference_id=" + conference_id, {}, false, true, true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                // service.getAllSessions = function(conference_id){
                //     console.log(conference_id);
                //     conference_id = ConferenceService.hash_conference_id.decode(conference_id)[0];
                //     var deferred = $q.defer();
                //     Request2.get(
                //         API.GET_BATCH_CONFERENCE_SESSIONS_URL + conference_id + "/", {}, false
                //     )
                //     .then(function(response){
                //         deferred.resolve(response.data);
                //     })
                //     .catch(function(error){
                //         deferred.reject(error);
                //     });
                //     return deferred.promise;
                // };

				service.listSessionAttendees = function(session_id, page, noLoader, options){
					var query = '';
					noLoader = noLoader || false;
					if(!_.isEmpty(options)){
						query = API.SESSION_ATTENDEE_LIST_URL + "?session_id=" + session_id + '&page=' + page +
						'&sort_by=' + options.sortBy + '&sort_order=' + options.sortOrder + '&q=' + options.queryString;
					} else{
						query = API.SESSION_ATTENDEE_LIST_URL + "?session_id=" + session_id + '&page=' + page;
					}
					var deferred = $q.defer();
					Request2.get(
						query,
					   	{}, false, noLoader
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                	console.log(error);
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

                service.bulkCreateSession = function(payload, conference_id){
                    var deferred = $q.defer();

                    var data = {
                        session_set: payload
                    }

                    Request2.post(
                        API.CONFERENCE_BULK_CREATE_SESSION + conference_id + '/',
                        data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				service.listSessionScanned = function(session_id, page, noLoader, options){
                    var query = '';
                    noLoader = noLoader || false;
                    if(!_.isEmpty(options)){
                        query = API.LIST_SCANNED_ATTENDEES + "?session_id=" + session_id + '&page=' + page +
                        '&sort_by=' + options.sortBy + '&sort_order=' + options.sortOrder + '&q=' + options.queryString;
                    } else{
                        query = API.LIST_SCANNED_ATTENDEES + "?session_id=" + session_id + '&page=' + page;
                    }
                    var deferred = $q.defer();
                    Request2.get(
                        query,
                        {}, false, noLoader
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        console.log(error);
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.listSessionFeedbacks = function(session_id){
					var query = API.SESSION_FEEDBACK_LIST + session_id + '/';
					
					var deferred = $q.defer();
					Request2.get(
						query,
					   	{}, false
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                	console.log(error);
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};
				service.removeSessionAttendeesFromList = function(session_id, attendee_ids, remove_all){
					var deferred = $q.defer();
					Request2.post(
						API.SESSION_REMOVE_ATTENDEE_URL,
						{
							'session_id' : session_id,
							'attendee_ids' : attendee_ids,
							'remove_all' : remove_all 
						}
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

				service.removeSessionAttendeesScannedFromList = function(session_id, attendee_ids, remove_all){
					var deferred = $q.defer();
					Request2.post(
						API.SESSION_REMOVE_ATTENDEE_SCANNED_URL,
						{
							'session_id' : session_id,
							'attendee_ids' : attendee_ids,
							'remove_all' : remove_all
						}
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

				service.addSessionAttendeesToList = function(session_id, attendee_ids){
					var deferred = $q.defer();
					Request2.post(
						API.SESSION_ADD_ATTENDEE_URL,
						{
							'session_id' : session_id,
							'attendee_ids': attendee_ids
						}
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

				service.scanSessionAttendeesToList = function(session_id, attendee_ids){
					var deferred = $q.defer();
					var data = [];
					_.each(attendee_ids, function(attendee_id){
					    data.push({
					        'attendee_id': attendee_id,
					        'scanned_at': new Date()
					    });
					});

					Request2.post(
						API.SESSION_SCAN_ATTENDEE_URL,
						{
							'session_id' : session_id,
							'attendees': data
						}
                    )
	                .then(function(response){
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });
                    return deferred.promise;
				};

				service.getSession = function(session_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.GET_SESSIONS_URL + "?session_id=" + session_id, {
                        	sort_by : 'full_name',
                        	sort_order: 'asc'
                        },
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.deleteSession = function(session_id){
                    var deferred = $q.defer();
                    Request2.remove(
                        API.REMOVE_CONFERENCE_SESSION_URL + "?session_id=" + session_id,
                        null,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.addSessionFile = function(data){
					var deferred = $q.defer();
					Request2.post(
						API.SESSION_ADD_FILE + data.session_id, data, false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
				};

                service.removeSessionFile = function(session_file_id){
                    var deferred = $q.defer();
                    Request2.remove(
                        API.SESSION_REMOVE_FILE + "?session_file_id=" + session_file_id, null, false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				// service.addConferenceSession = function(newSession, update){
    //                 var url = API.CREATE_CONFERENCE_SESSION_URL;
    //                 if (update) {
    //                     url = API.UPDATE_CONFERENCE_SESSION_URL;
    //                 }

    //                 newSession = _.cloneDeep(newSession);

    //                 // var newSpeakers = [];
    //                 // _(newSession.speakers).forEach(function(speaker) {
    //                 //   if (speaker.first_name && speaker.last_name) {
    //                 //     newSpeakers.push(speaker);
    //                 //   }
    //                 // });

    //                 var ce_ids = [];

    //                 _.each(newSession.continuing_educations, function(ce){
    //                     ce_ids.push(ce.id);
    //                 });

    //                 newSession.continuing_educations = ce_ids;
    //                 newSession.session_type = newSession.session_type.id;

    //                 // newSession.speakers = newSpeakers;

    //                 var deferred = $q.defer();
    //                 Request2.post(
    //                     url,
    //                     newSession,
    //                     false
    //                 )
    //                 .then(function(response){
    //                     var obj = JSON.stringify(response.data);
    //                     deferred.resolve(response.data);
    //                 })
    //                 .catch(function(error){
    //                     deferred.reject(error);
    //                 });

    //                 return deferred.promise;
    //             };

                service.addConferenceSession = function(newSession, update){
				    var url = API.SESSION_CREATE_COMPLETE + newSession.conference_id + '/';
				    if (update) {
				        url = API.SESSION_UPDATE_COMPLETE + newSession.id + '/';
				  	}

                    var ce_ids = [];

                    _.each(newSession.continuing_educations, function(ce){
                    	ce_ids.push(ce.id);
                    });

                    newSession.continuing_educations = ce_ids;
                    newSession.session_type = newSession.session_type.id;
                    // newSession.customsessionfieldvalue_set = [];
                    if (!newSession.sessionfile_set) newSession.sessionfile_set = [];


                    _.each(newSession.session_files, function(sessionFile){
                        newSession.sessionfile_set.push(sessionFile);
                    });
                    
					var deferred = $q.defer();
					Request2.post(
	                    url,
	                    newSession,
	                    false
	                )
	                .then(function(response){
	                	var obj = JSON.stringify(response.data);
                        deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    deferred.reject(error);
	                });

	                return deferred.promise;
				};

                service.createSessionCustomFields = function(conference_id, session_custom_field){
                    var deferred = $q.defer();
                    Request2.post(
                        API.SESSION_CUSTOM_FIELDS_CRUD + conference_id + '/',
                        session_custom_field,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.updateSessionCustomField = function(session_custom_field){
                    var deferred = $q.defer();
                    Request2.post(
                        API.SESSION_CUSTOM_FIELD_CRUD + session_custom_field.id + '/',
                        session_custom_field,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.deleteSessionCustomField = function(session_custom_field_id){
                    var deferred = $q.defer();
                    Request2.remove(
                        API.SESSION_CUSTOM_FIELD_CRUD + session_custom_field_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

                service.getSessionCustomFields = function(conference_id){
                    var deferred = $q.defer();
                    Request2.get(
                        API.SESSION_CUSTOM_FIELDS_CRUD + conference_id + '/',
                        {},
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                    return deferred.promise;
                };

				return service
			}
})();