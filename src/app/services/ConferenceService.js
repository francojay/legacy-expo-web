(function(){
	'use strict';

	angular
		.module('app')
		.service('ConferenceService', [
            '$q', '$http', 'API', 'localStorageService', 'Request2', 'toastr', '$filter', '$location', conferenceService
		]);
		function conferenceService($q, $http, API, localStorageService, Request2, toastr, $filter, $location){
		    var service = {};
            var confData = {};
            service.hash_conference_id = new Hashids('Expo', 7);

            service.getConferenceSpeakers = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.CONFERENCE_SPEAKERS_LIST + conference_id + '/?page=1' ,
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            };


            service.getConferenceTz = function(conference_id, sendDate){
                var deferred = $q.defer();
                Request2.get(
                    API.GET_CONFERENCE_TZ_V11_URL + conference_id + '/?send_at=' + sendDate.toISOString(),
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            service.deleteConferenceSpeaker = function(speaker_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.CONFERENCE_SPEAKER_CURD + speaker_id + '/',
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            service.getSuperAdminRevenue = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.SUPER_ADMIN_REVENUE_GET + conference_id + '/revenue' ,
                    {},
                    false
                )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                return deferred.promise;
            }

            service.saveConferenceSpeakerData = function(conference_id, speaker_data){
                var deferred = $q.defer();
                if(speaker_data.hasOwnProperty('id')){
                    Request2.post(
                        API.CONFERENCE_SPEAKER_CURD + speaker_data.id + '/',
                        speaker_data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                } else{
                    Request2.post(
                            API.CONFERENCE_SPEAKERS_LIST + conference_id + '/',
                            speaker_data,
                            false
                        )
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                }
            };

            service.updateSupportContact = function(contact){
                var deferred = $q.defer();
                if (contact.conference_support_contact_id) {
                    contact.id = contact.conference_support_contact_id;
                }

                Request2.post(
                        API.UPDATE_CONFERENCE_SUPPORT_CONTACT_V1_1_URL + contact.id + '/',
                        JSON.stringify(contact),
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.updateCESupportContact = function(contact){
                var deferred = $q.defer();

                Request2.post(
                    API.CONFERENCE_EXHIBITOR_CONTACT_CRUD + contact.conference_support_contact_id + '/',
                    JSON.stringify(contact),
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getSignedUrl = function() {
                var deferred = $q.defer();

                Request2.get(
                        API.SIGN_MQTT,
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.getCognitoCredentials = function() {
                var deferred = $q.defer();

                Request2.get(
                        API.COGNITO_GET,
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.deleteCESupportContact = function(contact_id){
                var deferred = $q.defer();

                Request2.remove(
                    API.CONFERENCE_EXHIBITOR_CONTACT_CRUD + contact_id + '/',
                    {},
                    true
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            service.deleteSupportContact = function(contact_id){
                var deferred = $q.defer();

                Request2.remove(
                    API.DELETE_CONFERENCE_SUPPORT_CONTACT_URL + '?conference_support_contact_id=' + contact_id,
                    {},
                    true
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createSupportContact = function(contact){
                var deferred = $q.defer();

                Request2.post(
                        API.CREATE_CONFERENCE_SUPPORT_CONTACT_V1_1_URL + contact.conference_id,
                        contact,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.createCESupportContact = function(contact){
                var deferred = $q.defer();

                Request2.post(
                        API.CONFERENCE_EXHIBITOR_CONTACTS_CRUD + contact.conference_id + '/',
                        contact,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

			service.getUpcomingConferences = function() {

			    return Request2.get(
			        API.GET_UPCOMING_CONFERENCES_URL,
			        {},
			        false
                )
                .then(function(response){
                    return  response.data;
                })
                .catch(function(error){
                    console.log(error);
                    return error;
                });
            }

            service.getAllConferences = function(page, q) {

                var url = API.GET_UPCOMING_CONFERENCES_URL;

                var data = {
                    page: page,
                    for_web_exhibitor: true
                }

                if (q && q.length > 2) {
                    data.q = q;
                } else {

                }

                return Request2.get(
                    url, data, true
                )
                .then(function(response){
                    return response.data;
                })
                .catch(function(error){
                    console.log(error);
                    return error;
                });
            }

            service.inviteUser = function(invitedUser, conference_id) {
                var deferred = $q.defer();

                if(invitedUser.invited_user_email !== localStorageService.get('email')){
                    invitedUser.conference_id = conference_id;
                }

                Request2.post(
                    API.CREATE_CONFERENCE_USER_URL,
                    invitedUser,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }


            service.getConferenceById = function(conference_id) {
                if(conference_id.length == 7){
                    conference_id = service.hash_conference_id.decode(conference_id);
                }
                return Request2.get(
                    API.GET_CONFERENCE_BY_ID,
                    {
                        'conference_id': conference_id[0],
                        'for_web' : 1
                    },
                    false
                )
                .then(function(response){
                    var eventData = {};
                    eventData['eventName'] = response.data.name;
                    var starts_at_txt = $filter('date')(response.data.date_from,
                        "MM/dd/yyyy", response.data.time_zone);

                    var ends_at_txt = $filter('date')(response.data.date_to,
                        "MM/dd/yyyy", response.data.time_zone);
                    eventData['eventDate'] = starts_at_txt + ' - ' + ends_at_txt;
                    eventData['accountType'] = 'Organizer';
                    var locationUrl = window.location.href.split('#!')[0] + '#!/conference/' +
                        service.hash_conference_id.encode(response.data.conference_id);

                    window.Intercom('update', {
                            "eventDate": eventData.eventDate,
                            "accountType": eventData.accountType,
                            "eventName": eventData.eventName,
                            "eventCreated": eventData.eventCreated,
                            "superAdminLink": locationUrl
                        }
                    );
                    return response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
												$location.path("/conferences");
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.reloadConferenceById = function(conference_id) {
                return Request2.get(
                    API.GET_CONFERENCE_BY_ID,
                    {
                        'conference_id': conference_id,
                        'for_web' : 1
                    },
                    false,
                    true
                )
                .then(function(response){
                    console.log(response.data);
                    return response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.getExhibitorCompleteData = function(conference_id) {
                if(conference_id.length == 7){
                    console.log(conference_id);
                    conference_id = service.hash_conference_id.decode(conference_id);
                }
                return Request2.get(
                    API.GET_EXHIBITOR_COMPLETE,
                    {
                        conference_id: conference_id[0],
                        for_web: true,
                    },
                    false
                )
                .then(function(response){
                    var eventData = {};
                    eventData['eventName'] = response.data.conference_info.name;
                    var starts_at_txt = $filter('date')(response.data.conference_info.date_from,
                        "MM/dd/yyyy", response.data.conference_info.time_zone);

                    var ends_at_txt = $filter('date')(response.data.conference_info.date_to,
                        "MM/dd/yyyy", response.data.conference_info.time_zone);
                    eventData['eventDate'] = starts_at_txt + ' - ' + ends_at_txt;
                    eventData['accountType'] = 'Exhibitor';
                    eventData['companyName'] = response.data.exhibitor_info.company_name;
                    var locationUrl = window.location.href.split('#!')[0] + '#!/conference/' +
                        service.hash_conference_id.encode(response.data.conference_info.conference_id);

                    window.Intercom('update', {
                            "eventDate": eventData.eventDate,
                            "eventName": eventData.eventName,
                            "accountType": eventData.accountType,
                            "eventCreated": eventData.eventCreated,
                            "superAdminLink": locationUrl
                        }
                    );
                    return  response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.reloadExhibitorCompleteData = function(conference_id) {
                return Request2.get(
                    API.GET_EXHIBITOR_COMPLETE,
                    {
                        conference_id: conference_id,
                        for_web: true
                    },
                    false,
                    true
                )
                .then(function(response){
                    return  response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.validateForm = function(formData){
                var reg = /^[A-z]/g;
                var tempFormData = formData;

               _.forIn(formData,function(val, key){
                    if((key === 'name') && (formData[key].charAt(0) !== formData[key].charAt(0).toUpperCase())){
                        formData[key] = formData[key];
                    } else if((key != 'logo' && key != 'website' && key != 'venue_url' && key != 'venue_phone_number' &&
                        key != 'enable_attendees') && (formData[key] === '')){
                        formData[key] = null;
                    }
                });
                if(tempFormData === formData){
                   confData = formData;
                   var data = _.cloneDeep(confData);
                   return $q.when(confData);
                } else{
                    confData = {};
                    return $q.when(confData)
                }
            };

            service.createConference = function(data) {
               var deferred = $q.defer();
               var clonedData = _.cloneDeep(data);
               Request2.post(
                    API.CREATE_CONFERENCE_V11_URL,
                    clonedData,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.addTemplate= function(data) {
               var deferred = $q.defer();
               Request2.post(
                    API.CONFERENCE_ADD_TEMPLATE,
                    data,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.removeTemplate = function(conference_template_id) {
               var deferred = $q.defer();
               Request2.remove(
                    API.CONFERENCE_REMOVE_TEMPLATE + '?conference_template_id=' + conference_template_id,
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.updateConference = function(data) {
                if(data.country && data.state) {
                    if (data.country.name) {
                        data.country = data.country.name;
                    }
                    if (data.state.abbreviation) {
                        data.state = data.state.abbreviation;
                    }
                    data = _.omit(data, 'user');
                    var exhibitor_only = data.exhibitor_only;
                    data = _.pickBy(data);
                    data.exhibitor_only = exhibitor_only;
                }

                if (data.date_from_editable) {
                    var date_from_editable = new Date(data.date_from_editable);
                    data.day_from = {
                        "year": date_from_editable.getFullYear(),
                        "month": date_from_editable.getMonth() + 1,
                        "day": date_from_editable.getDate()
                    }
                }

                if (data.date_to_editable) {
                    var date_to_editable = new Date(data.date_to_editable);
                    data.day_to = {
                        "year": date_to_editable.getFullYear(),
                        "month": date_to_editable.getMonth() + 1,
                        "day": date_to_editable.getDate()
                    }
                }

                var deferred = $q.defer();
                Request2.post(
                    API.UPDATE_CONFERENCE_V11_URL + data.conference_id,
                    data,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createConferenceContinuingEducation = function(conference_id, continuing_education){
                var deferred = $q.defer();
                Request2.post(
                    API.CONFERENCE_CONTINUING_EDUCATIONS_CRUD + conference_id + '/',
                    continuing_education,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.removeConferenceContinuingEducation = function(continuing_education_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.CONFERENCE_CONTINUING_EDUCATION_CRUD + continuing_education_id + '/',
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.updateConferenceContinuingEducation = function(continuing_education){
                var deferred = $q.defer();
                Request2.post(
                    API.CONFERENCE_CONTINUING_EDUCATION_CRUD + continuing_education.id + '/',
                    continuing_education,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.deleteConference = function(data, conference_id) {
                var deferred = $q.defer();
                Request2.remove(
                    API.DELETE_CONFERENCE_URL + '?conference_id=' + conference_id,
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }


            service.submitMerchantAgreement = function(merchant_profile){
                var deferred = $q.defer();
                Request2.post(
                    API.MERCHANT_AGREEMENT_CRUD,
                    merchant_profile,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getMerchantAgreementResponse = function(){
                var deferred = $q.defer();
                Request2.get(
                    API.MERCHANT_AGREEMENT_CRUD,
                    {},
                    false,
                    true
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.updateSocialTags = function(conference_id, socialTags){
                var deferred = $q.defer();
                Request2.post(
                    API.CONFERENCE_SOCIAL_TAGS_CRUD + conference_id + '/',
                    socialTags,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

             service.getSocialTags = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.CONFERENCE_SOCIAL_TAGS_CRUD + conference_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createHotel = function(conference_id, travelModel){

                travelModel.country = travelModel.country.trim();

                var deferred = $q.defer();
                Request2.post(
                    API.HOTELS_CRUD + conference_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createCarRental = function(conference_id, travelModel){

                travelModel.country = travelModel.country.trim();

                var deferred = $q.defer();
                Request2.post(
                    API.CAR_RENTALS_CRUD + conference_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createAirline = function(conference_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.AIRLINES_CRUD + conference_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createTransportation = function(conference_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.TRANSPORTATIONS_CRUD + conference_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getHotels = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.HOTELS_CRUD + conference_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getCarRentals = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.CAR_RENTALS_CRUD + conference_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getAirlines = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.AIRLINES_CRUD + conference_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getTransportations = function(conference_id){
                var deferred = $q.defer();
                Request2.get(
                    API.TRANSPORTATIONS_CRUD + conference_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getHotel = function(travel_id){
                var deferred = $q.defer();
                Request2.get(
                    API.HOTEL_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getCarRental = function(travel_id){
                var deferred = $q.defer();
                Request2.get(
                    API.CAR_RENTAL_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getAirline = function(travel_id){
                var deferred = $q.defer();
                Request2.get(
                    API.AIRLINE_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getTransportation = function(travel_id){
                var deferred = $q.defer();
                Request2.get(
                    API.TRANSPORTATION_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.updateHotel = function(travel_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.HOTEL_CRUD + travel_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.updateCarRental = function(travel_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.CAR_RENTAL_CRUD + travel_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.updateAirline = function(travel_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.AIRLINE_CRUD + travel_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.updateTransportation = function(travel_id, travelModel){
                var deferred = $q.defer();
                Request2.post(
                    API.TRANSPORTATION_CRUD + travel_id + '/',
                    travelModel,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.deleteHotel = function(travel_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.HOTEL_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.deleteCarRental = function(travel_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.CAR_RENTAL_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.deleteAirline = function(travel_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.AIRLINE_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

            service.deleteTransportation = function(travel_id){
                var deferred = $q.defer();
                Request2.remove(
                    API.TRANSPORTATION_CRUD + travel_id + '/',
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

			return service;
		}


})();
