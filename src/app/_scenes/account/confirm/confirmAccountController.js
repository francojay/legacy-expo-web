(function() {

  'use strict';

  var controllerId = 'confirmAccountController';
  var dependenciesArray = [
    '$scope',
    '$state',
    '$stateParams',
    'localStorageService',
    'toastr',
    'UserService',
    '$location',
    'API',
    confirmAccountController
  ];

  var app = angular.module('app');

  app.controller(controllerId, dependenciesArray);

  function confirmAccountController($scope, $state, $stateParams, localStorageService, toastr, UserService, $location, API) {
    $scope.environment = API.ENV;
    var vm = this;

    var userEmail = localStorageService.get('createdAccountEmail');
    if (!userEmail) {
      userEmail = localStorageService.get('verifyEmail');
    }
    vm.verificationCodeModel = [];


    vm.verifyAccount = function(verificationCode) {
      if (verificationCode.length < 4) {
        toastr.error('Please fill in all the code digits.')
      } else {
        var verificationCodeInteger;
        var verificationCodeString = '';

        _.forEach(verificationCode, function(digit) {
          verificationCodeString = verificationCodeString + digit;
        });

        verificationCodeInteger = parseInt(verificationCodeString);

        var validData = {
          'email': userEmail,
          'code': verificationCodeInteger
        };
        var zapierParams = localStorageService.get('zapierParams') || null;

        UserService
          .verify(validData)
          .then(function(response) {
            if (response) {
              toastr.success('Account Validated.', 'Welcome to ExpoPass!');

              localStorageService.set('access_token', response.data.oauth2_data.access_token);
              localStorageService.set('refresh_token', response.data.oauth2_data.refresh_token);
              localStorageService.set('expires_in', response.data.oauth2_data.expires);

              var timeObject = new Date(response.data.oauth2_data.expires);

              localStorageService.set('expires_at', timeObject);
              UserService.getProfile().then(function(user) {
                localStorageService.set('currentUser', JSON.stringify(user));
                if(!$stateParams.redirect_to) {
                  localStorageService.remove('zapierParams');
                  $state.go('conferences.overview');
                } else {
                  zapierParams.redirect_to = $stateParams.redirect_to;
                  $state.go('account.zapier',zapierParams);
                }
              }, function(error) {
                console.log(error);
              })

            }
          }, function(error) {
            toastr.error('Error!', 'Validation code is incorrect. Please type the correct code.');
            vm.verificationCodeModel = [];
          });
      }


    };

    vm.resendVerificationEmail = function() {
      UserService.resendVerificationCode(userEmail)
        .then(function(response) {
          toastr.success('The verification email was resent!');
        })
        .catch(function(error) {
          toastr.error('There was an error with your request');
        });
    };

    vm.focusInput = function(inputNumber) {
      if (inputNumber != 0 && document.getElementById('digit' + (inputNumber - 1)).value !== '') {
        document.getElementById('digit' + inputNumber).focus();
      } else {
        document.getElementById('digit4').blur();
      }
    }
  }
})();
