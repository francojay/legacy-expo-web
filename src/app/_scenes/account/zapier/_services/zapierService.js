
(function() {

  'use strict';

  var serviceId         = 'zapierService';
  var app               = angular.module('app');
  var dependenciesArray = [
      '$q',
      'requestFactory',
      zapierService
  ];

  app.service(serviceId, dependenciesArray);

  function zapierService($q, requestFactory) {

    var service = {};

    service.getApplications = function(clientId) {
			var deferred = $q.defer();
			var url = 'api/v1.1/applications/' + clientId + '/';

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

    service.grantZapierAccess = function(clientId, data) {
			var deferred = $q.defer();
			var url = 'api/v1.1/applications/' + clientId + '/authorize/';

			requestFactory.post(url, data, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

    return service;
  }
})();
