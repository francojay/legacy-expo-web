(function () {

    'use strict';

    var controllerId      = 'zapierAccessController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$stateParams',
      'zapierService',
      'localStorageService',
      '$state',
      zapierAccessController
    ]

    app.controller(controllerId, dependenciesArray);

    function zapierAccessController($scope, $stateParams, zapierService, localStorageService, $state) {

      if(!$stateParams.client_id || !$stateParams.state || !$stateParams.redirect_uri || !$stateParams.response_type) {
        $state.go('conferences.overview');
      }

      $scope.zapierAppName = "";
      $scope.deniedUrl = $stateParams.redirect_uri + '?error=access_denied';

      if(!$stateParams.redirect_to) {
        localStorageService.set('zapierParams', $stateParams);
        $state.go('account.login',{'redirect_to':'zapier'});
      } else {
        localStorageService.set('zapierParams', $stateParams);

        getApplication();
      }
      $scope.grantZapierAccess = function() {
        if($scope.zapierAppName) {
          var data= {
            "redirect_url": $stateParams.redirect_uri
          };

          zapierService.grantZapierAccess($stateParams.client_id, data).then(function(response) {
            localStorageService.remove('zapierParams');
            window.location.href = $stateParams.redirect_uri + '?state=' + $stateParams.state + '&code=' + response.code;
          },function(error){
            if(error.status === 401) {
              window.location.href = $stateParams.redirect_uri + '?error=access_denied';
            }
          });
        }
      }

      function getApplication() {
        zapierService.getApplications($stateParams.client_id).then(function(response) {
          $scope.zapierAppName = response.name;
        },function(error){

        });
      }

    }
})();
