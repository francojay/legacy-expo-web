(function () {

  'use strict';

  var controllerId = 'forgotPasswordController';
  var app          = angular.module('app');
  var dependenciesArray = [
    '$scope',
    '$state',
    '$stateParams',
    'LoginService',
    'toastr',
    'localStorageService',
    forgotPasswordController
  ];
  app.controller(controllerId, dependenciesArray);

  function forgotPasswordController($scope, $state, $stateParams, LoginService, toastr, localStorageService) {
      var vm = this;

      vm.emailAddress = '';

      vm.resetDone = false;

      vm.forgotPassword = function(email) {
          if(email) {
              LoginService
                  .forgotPassword(email)
                  .then(function(data) {
                      localStorageService.set('resettedUserEmailAddress', email);
                      vm.resetDone = true;
                  })
                  .catch(function(error){
                      toastr.error('This email address was not found in our community!');
                      vm.invalid_email = true;
                  });
          }
      }

      vm.returnToLogin = function() {
          $state.go('account.login');
      }
    }
})();
