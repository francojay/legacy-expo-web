(function () {
  'use strict';

  var controllerId      = 'loginController';
  var app               = angular.module("app");
  var dependenciesArray = [
    '$scope',
    '$state',
    '$stateParams',
    '$location',
    'LoginService',
    'localStorageService',
    'toastr',
    '$rootScope',
    '$timeout',
    'API',
    'UserService',
    '$q',
    loginController
  ];

  app.controller(controllerId, dependenciesArray);


  function loginController($scope, $state, $stateParams, $location, LoginService, localStorageService, toastr, $rootScope, $timeout, API, UserService, $q) {
    $scope.environment         = API.ENV;
    $scope.invalidEmailData    = false;
    $scope.invalidPasswordData = false;
    $scope.user        = {
        'email': '',
        'password': ''
    };

    $timeout(function() {
      $('.user.inp').focus();
      $('.container-center.login-container').keypress(function(event) {
        if (event.which === 13) {
          $scope.login($scope.user.email, $scope.user.password)
        }
      })
    }, 0)

    $scope.login = function (email, password) {
        var zapierParams = localStorageService.get('zapierParams') || null;

        LoginService.authenticate(email, password)
            .then(function (data) {
              $rootScope.conference = null;
                if (data.hasOwnProperty('message')) {
                  localStorageService.set('resettedUserEmailAddress', email);
                  localStorageService.set('generatedPassword', password);
                  $state.go('account.reset');
                } else {
                    if (data.is_not_verified) {
                        localStorageService.set('verifyEmail', $scope.user.email);
                        $location.path("/confirm");
                    }
                    else {
                      var promises = [];

                      promises.push(UserService.getProfile());

                      $q.all(promises).then(function(user) {
                        localStorageService.set('currentUser',JSON.stringify(user[0]));

                        if(!$stateParams.redirect_to) {
                          localStorageService.remove('zapierParams');
                          $state.go('conferences.overview');
                        } else {
                          zapierParams.redirect_to = $stateParams.redirect_to;
                          $state.go('account.zapier',zapierParams);
                        }
                      }).catch(function(error){

                      });

                    }
                }
            }, function(error) {
              if(error.data.detail){
                toastr.error(error.data.detail);
                return ;
              }
              toastr.error("Your password or email address is not valid.")
            });
    }

    $scope.checkIfEnterKeyAndLogin = function($event) {
      if ($event.which === 13) {
        $scope.login($scope.user.email, $scope.user.password);
      }
    }

    var checkIfEmailIsValid = function(emailAddress) {
      var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
        if (!regexExpression.test(emailAddress)) {
          toastr.error("Please enter a valid email address!");
        }
        return regexExpression.test(emailAddress);
    };

    var checkIfPasswordIsValid = function(password) {
      if(!password || ((password.length < 8 || password.length > 16) || (password.search(/[0-9]/) < 0))) {
        toastr.error("Your password should have between 8 and 16 characters and contain at least one number!");
        return false;
      } else {
        return true;
      }
    }
  }
})();
