(function () {

    'use strict';

    var controllerId = 'createUserController';
    var app          = angular.module('app');

    var dependenciesArray = [
      'UserService',
      'toastr',
      'localStorageService',
      '$scope',
      'API',
      createUserController
    ]

    app.controller(controllerId, dependenciesArray);

    function createUserController(UserService, toastr, localStorageService, $scope, API) {
      $scope.environment = API.ENV;
      var vm = this;
      vm.showAccountVerificationMessage = false;
      vm.termsHaveBeenAccepted          = false;
      vm.newUser = {
        firstName: '',
        lastName: '',
        emailAddress: '',
        password: '',
        confirmedPassword: ''
      };

      var links = {
        'terms'  : 'https://www.expopass.com/terms-of-use',
        'policy' : 'https://www.expopass.com/privacy-policy'
      };

      vm.createAccountHandler = function(newUser) {
        var passwords = {
            password: newUser.password,
            confirmedPassword: newUser.confirmedPassword
        };


        var passwordsAreValid = checkIfUserPasswordsAreValid(passwords);
        var emailAddressIsValid = checkIfEmailIsValid(newUser.emailAddress);
        var namesAreValid = checkIfNamesExist(newUser.firstName, newUser.lastName);

        if (namesAreValid && emailAddressIsValid && passwordsAreValid) {
          var userData = {
            username   : newUser.emailAddress,
            first_name : newUser.firstName,
            last_name  : newUser.lastName,
            email      : newUser.emailAddress,
            password   : newUser.password
		      };

          UserService.register(userData).then(function(data) {
  						vm.showAccountVerificationMessage = true;
              localStorageService.set('createdAccountEmail', newUser.emailAddress);
              marketing.trackSignUp(API.ENV);
  						toastr.success('Your account has been created!');
  					}, function(error) {
              if (error.data.detail.indexOf('email')) {
                  toastr.error('This email is already taken!');
              }

              else {
                  toastr.error('Error!', 'Please fill the inputs with valid information!');
              }
            });
          }
        };

      vm.openLink = function(type) {
        var win = window.open(links[type], '_blank');
        win.focus();
      }

      vm.toggleAcceptanceOfTerms = function(acceptanceState) {
        vm.termsHaveBeenAccepted = !acceptanceState;
      }

      var checkIfUserPasswordsAreValid = function(newUserPasswords) {
        if(!newUserPasswords.password || ((newUserPasswords.password.length < 8 || newUserPasswords.password.length > 16) || (newUserPasswords.password.search(/[0-9]/) < 0))) {
          vm.newUser.password = '';
          var invalidPassword = true;
          toastr.error("Your password should be at least 8 characters long, maximum 16 characters and should contain at least one number!")
         } else {
           var invalidPassword = false;
        }

  			if(!newUserPasswords.confirmedPassword || (newUserPasswords.confirmedPassword !== newUserPasswords.password)){
  				vm.newUser.confirmedPassword = '';
                  var invalidPasswordConfirmation = true;
                  toastr.error("Passwords are not matching!")
  			} else {
  				var invalidPasswordConfirmation = false;
  			}

          return (!invalidPassword && !invalidPasswordConfirmation);
      };

      var checkIfEmailIsValid = function(emailAddress) {
          var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;

          return regexExpression.test(emailAddress);
      };

      var checkIfNamesExist = function(firstName, lastName) {
          return firstName !== '' && lastName !== '';
      }
    }
})();
