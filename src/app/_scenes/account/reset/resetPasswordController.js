(function () {

  'use strict';

  var controllerId = 'resetPasswordController';
  var app          = angular.module('app');
  var dependenciesArray = [
    '$scope',
    '$state',
    '$stateParams',
    'LoginService',
    'toastr',
    'localStorageService',
    resetPasswordController
  ];
  app.controller(controllerId, dependenciesArray);

  function resetPasswordController($scope, $state, $stateParams, LoginService, toastr, localStorageService) {
    var vm               = this;

    vm.newPassword       = '';
    vm.confirmedPassword = '';

    vm.userEmail         = localStorageService.get('resettedUserEmailAddress');
    vm.generatedPassword = localStorageService.get('generatedPassword');

    vm.resetPassword     = function (newPassword, confirmedPassword) {
      if (newPassword !== confirmedPassword) {
          toastr.error('The passwords are different! Please match the passwords.');
      } else {
        var newData = {
          'email_address' : vm.userEmail,
          'token_code': vm.generatedPassword,
          'password': newPassword
        };
        LoginService.resetPassword(newData).then(function (data) {
          toastr.success('Your password has been reseted! Please login with the new password');
          $state.go('account.login');
        }, function(error) {
          toastr.error('The password must contain at least 1 digit', 'Must be longer than 8 characters and shorter than 16 characters');
        });
      }
    }
  }
})();
