(function () {
  'use strict';

  var controllerId      = 'accountController';
  var app               = angular.module('app');
  var dependenciesArray = [
    '$scope',
    '$state',
    '$stateParams',
    '$timeout',
    accountController
  ];

  app.controller(controllerId, dependenciesArray);

  function accountController($scope, $state, $stateParams, $timeout) {
    console.log($stateParams)
    $timeout(function() {
      $scope.appReady = true;
      $scope.errorMessage = $stateParams.errorMessage
    }, 0);

    $scope.dismissErrorMessage = function() {
      $scope.errorMessageDismissed = true;
      $timeout(function() {
        $scope.errorMessage = null;
      }, 200);
    }
  }
})();
