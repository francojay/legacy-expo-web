(function(){
	'use strict';
	var app 							= angular.module("app");
  var directiveId 			= 'stripeConnectInfoModal';
	var dependenciesArray = [
		'$rootScope',
		'$state',
		'ngDialog',
		'StripeConnectDataService',
		stripeConnectInfoModal
	];

	app.directive(directiveId, dependenciesArray);

	function stripeConnectInfoModal($rootScope, $state, ngDialog, StripeConnectDataService) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/stripe-connect/_components/stripeConnectInfoModal/stripeConnectInfoModalView.html',
			replace: true,
      link: link
		};

    function link(scope) {
			scope.info = [];
			StripeConnectDataService.getStripeConnectAccount().then(function(response) {
				scope.info = response.data.fields;
			}, function(error) {
				console.log(error);
			});
		}
	}
}());
