(function(){
	'use strict';
  var serviceId = 'StripeConnectDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'requestFactory',
		'$timeout',
		StripeConnectDataService
	];
	app.service(serviceId, dependenciesArray);

	function StripeConnectDataService($q, requestFactory, $timeout) {
		var service = {};

		service.confirmAccountLinkage = function(code) {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/account/connect/standard";
			var data 		 = {
				"code" : code
			}

			requestFactory.post(url, data, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.getStripeConnectAccount = function() {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/account/connect/standard/";

			requestFactory.get(url, null, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});
			
			return deferred.promise;
		}



    return service;
	}
})();
