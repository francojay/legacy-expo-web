(function () {
  'use strict';
  var controllerId 			= 'stripeConnectController';
	var app 				 			= angular.module("app");

	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$stateParams',
		'StripeConnectDataService',
    'toastr',
    '$state',
		stripeConnectController
	];

	app.controller(controllerId, dependenciesArray);

	function stripeConnectController($rootScope, $scope, $stateParams, StripeConnectDataService, toastr, $state) {
		$scope.currentUser								= JSON.parse(localStorage.getItem('currentUser'));
		$scope.currentUserMerchantDetails = $scope.currentUser.merchant_account;

    if (!$scope.currentUserMerchantDetails.stripe_connect_button) {
      $state.go("conferences.overview");
    }

		if ($stateParams && $stateParams.code && $stateParams.code.length > 1) {
			StripeConnectDataService.confirmAccountLinkage($stateParams.code).then(function(response) {
				toastr.success("Your Stripe account was connected succesfully!");
        $state.go("conferences.overview");
			}, function(error) {
				console.log(error);
			});
		}
  }
}());
