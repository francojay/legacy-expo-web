(function(){
	'use strict'

	var directiveId = 'expoButton';
		angular
			.module('app')
			.directive(directiveId, expoButton);
			function expoButton (){
				return {
					restrict: 'E',
					replace: true,
					scope: {
                        width: '@',
                        height: '@',
                        radius: '@',
						border: '@',
                        fontSize: '@',
                        fontColor: '@',
                        color: '@',
                        text: '@',
                        action: '&'
					},
					templateUrl: 'app/_scenes/conferences/_components/button/buttonView.html',
                    link: link
				}

                function link(scope){
                    scope.width = '150px';
					scope.height = '40px';
					scope.radius = '20px';
					scope.fontSize = '14px';
					scope.fontColor = '#4a4a4a';
					scope.border = '1px solid #cdcdcd';

					console.log(scope);
                };
		}
})();
