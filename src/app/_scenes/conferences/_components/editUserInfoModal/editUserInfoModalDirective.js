(function(){
	'use strict';
    var directiveId = 'editUserInfoModal';
		var app = angular.module('app');

		var dependenciesArray = [
			'UserService',
			'ngDialog',
			'SharedProperties',
			'toastr',
			'$q',
			'fileService',
			'API',
			'intlTelInputOptions',
			'$rootScope',
			'$timeout',
			editUserInfoModal
		];

	app.directive(directiveId, dependenciesArray);

	function editUserInfoModal(UserService, ngDialog, SharedProperties, toastr, $q, fileService, API, intlTelInputOptions, $rootScope, $timeout) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_components/editUserInfoModal/editUserInfoModalView.html',
      link: link,
      replace: true
		};

  function link(scope) {
			var currentDialogId = scope.ngDialogId;
			scope.currentUser = scope.ngDialogData.currentUser;
			scope.currentUserImageBackup = scope.currentUser.profile_url;
			scope.modelForEdit = [
				{
					shownValue : "First Name",
					key				 : "first_name",
					value			 : scope.currentUser.first_name,
					mandatory  : true,
					error      : false
				},
				{
					shownValue : "Last Name",
					key				 : "last_name",
					value			 : scope.currentUser.last_name,
					mandatory  : true,
					error      : false
				},
				{
					shownValue : "Job Title",
					key				 : "job_title",
					value			 : scope.currentUser.job_title
				},
				{
					shownValue : "Email Address",
					key				 : "email",
					value			 : scope.currentUser.email,
					mandatory  : true,
				},
				{
					shownValue : "Company Name",
					key				 : "company_name",
					value			 : scope.currentUser.company_name
				},
				{
					shownValue : "Phone Number",
					key				 : "phone_number",
					value			 : scope.currentUser.phone_number
				},
				{
					shownValue : "Street Address",
					key				 : "street_address",
					value			 : scope.currentUser.street_address
				},
				{
					shownValue : "City",
					key 			 : "city",
					value			 : scope.currentUser.city
				},
				{
					shownValue : "State",
					key				 : "state",
					value			 : scope.currentUser.state,
					states     : API.UNITED_STATES,
				},
				{
					shownValue : "Zip Code",
					key				 : "zip_code",
					value			 : scope.currentUser.zip_code
				},
				{
					shownValue : "Country",
					key				 : "country",
					value			 : scope.currentUser.country,
					countries  : API.COUNTRIES,
				},
				{
					shownValue : "Password",
					key				 : "password",
					visible		 : true,
					error      : false
				},
			];
			scope.defaultCountryFlag = 'us';
			var userCountry = scope.currentUser.country?scope.currentUser.country:null;
			if(userCountry != null) {
				angular.forEach(API.COUNTRIES, function(country) {
					if(country.name === userCountry){
						scope.defaultCountryFlag = country.code.toLowerCase();
					}
				});
			}

			scope.closeDialog = function() {
				ngDialog.close(currentDialogId);
				scope.currentUser.profile_url = scope.currentUserImageBackup;
				ngDialog.open(
                    {
                        plain: true,
                        template: '<user-info-modal></user-info-modal>',
                        className: 'app/directives/userInfo/userInfo.scss',
                        data: {
                            currentUser: scope.currentUser
                        }
                    });
			}

			scope.saveUserData = function(model) {
				var errorOnMandatoryFields = false;
				for (var i = 0; i <  model.length; i++) {
					var field = model[i];
					if(field){
						if (field.hasOwnProperty('mandatory')  && !field.value) {
							toastr.error(field.shownValue + " is a required field. Please input a value!");
							model[i].error = true;
							errorOnMandatoryFields = true;
							break;
						}
						else if(field.key == "password"){
							if(field.value){
								if(!validateNewPassword(field.value)){
									toastr.error('Must be longer than 8 characters and shorter than 16 characters','The password must contain at least 1 digit');
									model[i].error = true;
									errorOnMandatoryFields = true;
									break;
								}
							}
							else{
								delete model[i];
							}
						}
						else if(field.key == "phone_number" && field.value === undefined){
								toastr.warning(field.shownValue + " need to have the correct format!");
								errorOnMandatoryFields = true;
						}
						else {
							model[i].error = false;
						}
					}
				};
				if(!errorOnMandatoryFields) {
					scope.currentUser = processFields(model,scope.currentUser);
					UserService
						.updateProfile(scope.currentUser )
						.then(function(response){
							SharedProperties.user = response;
							scope.closeDialog();
							toastr.success("Your account has been updated!");
						})
						.catch(function(error){
							toastr.error("There was an error with your request!", "Error!");
						})
				}
			}

			scope.uploadProfilePhoto = function(file) {
				lib.squareifyImage($q, file)
					.then(function(response){
						var imgGile = response.file;

						fileService.getUploadSignature(imgGile)
							.then(function(response){
								var file_link = response.data.file_link;
								fileService.uploadFile(imgGile, response)
									.then(function(uploadResponse){
										scope.currentUser.profile_url = file_link;
									})
									.catch(function(error){
										console.log(error)
									});
							})
							.catch(function(error){
								console.log(error);
							});
					});
			}
			scope.updateCountryForPhoneInput = function(field) {
				if (field !== undefined) {
					var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
	        var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
	        number = number.replace("+"+countryData.dialCode,"");
					number = number.replace(/[^0-9\\.]+/g,"");
	        $rootScope.phoneIntlTelInputCtrl.setCountry(field.toLowerCase());
	        countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
					if(number){
						$timeout(function() {
							scope.modelForEdit[5].value = "+"+countryData.dialCode+number;
						}, 100);
					}

				}
			}
        };

		function validateNewPassword(password) {
			var numberExp = /[0-9]/g;
            if((password.length < 8 || password.length > 16) || (!numberExp.test(password))){
                return false;
            } else {
                return true;
            }
		}
		function processFields(model,currentUser){
			angular.forEach(model, function(field) {
				 if(field.key == "state" && model[10].value != "US"){
					currentUser[field.key] = null;
				}
				else if(field.value){
					currentUser[field.key] = field.value;
				}
				else{
					currentUser[field.key] = null;
				}
			});
			angular.forEach(currentUser, function(field,key) {
				 if(_.isEmpty(field) && key != "password"){
					 currentUser[key] = null;
				 }
			});
			return currentUser;
		}
	};
}());
