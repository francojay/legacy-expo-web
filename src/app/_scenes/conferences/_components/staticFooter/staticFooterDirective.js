(function(){
	'use strict';
  var directiveId 			= 'staticFooter';
	var app 							= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'toastr',
		staticFooter
	];

	app.directive(directiveId, dependenciesArray);

	function staticFooter($rootScope, toastr){
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'app/_scenes/conferences/_components/staticFooter/staticFooterView.html'
		}
	};
}());
