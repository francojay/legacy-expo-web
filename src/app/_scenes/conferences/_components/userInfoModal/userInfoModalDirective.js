(function () {
	'use strict';
	var directiveId = 'userInfoModal';

	angular
		.module('app')
		.directive(directiveId, ['UserService', 'ngDialog', 'localStorageService', '$location', 'conferenceService', 'merchantService', '$state', 'merchantAgreementDataService', userInfoModal]);

	function userInfoModal(UserService, ngDialog, localStorageService, $location, conferenceService, merchantService, $state, merchantAgreementDataService) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_components/userInfoModal/userInfoModalView.html',
			link: link,
			replace: true
		};

		function link(scope) {
			scope.merchantAgreementStatus = "";
			scope.editProfileLabels = [
				{ label: "First Name", code: 'first_name', type: "text", valid: "true" },
				{ label: "Last Name", code: 'last_name', type: "text", valid: "true" },
				{ label: "Job Title", code: 'job_title', type: "text" },
				{ label: "Email Address", code: 'email', type: "email" },
				{ label: "Company Name", code: 'company_name', type: "text" },
				{ label: "Phone Number", code: 'phone_number', type: "text" },
				{ label: "Street Address", code: 'street_address', type: "text" },
				{ label: "City", code: 'city', type: "text" },
				{ label: "State", code: 'state', placeH: "--", type: "text" },
				{ label: "Zip Code", code: 'zip_code', zipCodeType: "[0-9]{5}" },
				{ label: "Country", code: 'country', placeH: "Choose a Country", type: "text" }
			];

			getMerchantAgreementStatus();
			var currentDialogId = scope.currentDialogId;
			scope.passedData = scope.ngDialogData;
			scope.currentUser = scope.passedData.currentUser;
			scope.profileTabDetails = _.takeRight(scope.editProfileLabels, 7);
			scope.logOut = function () {
				if (localStorageService.clearAll()) {
					ngDialog.closeAll();
					window.Intercom("shutdown");
					$state.transitionTo('account.login');
				}
			};

			scope.switchTabs = function (tab) {
				if (tab === 'profile') {
					scope.profileTabOpened = true;
				} else if (tab === 'merchant') {
					scope.profileTabOpened = false;
				}
			};

			scope.editProfileHandler = function () {
				ngDialog.close(currentDialogId);
				ngDialog.open(
					{
						plain: true,
						template: '<edit-user-info-modal></edit-user-info-modal>',
						className: 'app/_scenes/conferences/_components/editUserInfoModal/editUserInfoModalStyle.scss',
						data: {
							currentUser: scope.currentUser
						}
					}
				);
			}

			scope.openMerchantAgreementForm = function () {
				ngDialog.close(currentDialogId);
				ngDialog.open({
					plain: true,
					template: "<merchant-agreement-modal></merchant-agreement-modal>",
					className: 'app/stylesheets/_merchantAgreementModal.scss'
				})
			}

			scope.openStripeConnectInfoModal = function() {
				ngDialog.open({
					plain: true,
					template: "<stripe-connect-info-modal></stripe-connect-info-modal>",
					className: 'app/stylesheets/_merchantAgreementModal.scss'
				})
			}

			function getMerchantAgreementStatus() {
				merchantAgreementDataService
					.getMerchantAgreement()
					.then(function (response) {
						if (response.hasOwnProperty('status')) {
							scope.merchantAgreementStatus = response.status;
						}

						if (response.hasOwnProperty('verification')) {
							scope.verification = response.verification;
						}

					}, function(error) {
						if(error.data.detail) {
							// do nothing
						} else {
							toastr.error('Error!', 'There has been a problem with your request.');
						}
					});
			}



		};


	};


}());
