(function(){
	'use strict';
  var directiveId 			= 'staticHeader';
	var app 							= angular.module('app');
	var dependenciesArray = [
		'$localStorage',
		'UserService',
		'ngDialog',
		'localStorageService',
		'$rootScope',
		'toastr',
		'$state',
		staticHeader
	];

	app.directive(directiveId, dependenciesArray);

	function staticHeader($localStorage, UserService, ngDialog, localStorageService, $rootScope, toastr, $state) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_components/staticHeader/staticHeaderView.html',
      link: link
		};

    function link(scope) {
			scope.isOnNormalOverview = ($state.current.name == "conferences.overview" ? true : false);
			console.log(scope.isOnNormalOverview)
			scope.isSuperAdmin = localStorageService.get("is_superuser");
			scope.headerTitle = scope.isSuperAdmin ? "All Events" : "My Events";
      getUserProfile().then(function(currentUser) {
          scope.currentUser = currentUser;
					$rootScope.isSuperAdmin = currentUser.is_superuser;
					var eventData = {
	            'eventCreated': false,
	            'eventName': "",
	            'superAdminLink': "",
	            'eventDate': ""
	        }
					window.Intercom('boot', {
				     app_id: 'ij3yrbte',
				     email: scope.currentUser.email,
				     user_hash: scope.currentUser.user_signed,
				     expo_user_id: scope.currentUser.id,
				     name: scope.currentUser.first_name + ' ' + scope.currentUser.last_name,
				     created_at: 1234567890,
				     custom_launcher_selector: '#my_custom_link',
				     "eventCreated": eventData.eventCreated
				  });
      });

      scope.openAccountProfile = function() {
				ngDialog.open(
					{
							plain: true,
							template: '<user-info-modal></user-info-modal>',
							className: 'app/directives/userInfo/userInfo.scss',
							data: {
									currentUser: scope.currentUser
							}
					});
			}

			scope.searchConferences = function(conferenceSearchQuery) {
					if (conferenceSearchQuery.length > 2) {
							$rootScope.conferenceSearchQuery = conferenceSearchQuery;
					} else if ($rootScope.conferenceSearchQuery.length > 0 && conferenceSearchQuery == "") {
							$rootScope.conferenceSearchQuery = "";
					}
			}
    }

    function getUserProfile() {
     return UserService.getProfile().then(function(user) {
        localStorage.currentUser = JSON.stringify(user);
				localStorageService.set('currentUser',JSON.stringify(user));
        return user;
      }, function(error) {
				toastr.error("An error has occured while trying to fetch your profile.");
			});
  	}
	}
}());
