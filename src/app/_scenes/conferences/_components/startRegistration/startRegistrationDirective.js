(function(){
	'use strict';
    var directiveId = 'startRegistrationModal';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', '$state', 'ngDialog', 'conferenceRegistrationDataService', 'merchantAgreementDataService', startRegistrationModal]);

	function startRegistrationModal($rootScope, $state, ngDialog, conferenceRegistrationDataService, merchantAgreementDataService) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_components/startRegistration/startRegistrationView.html',
			replace: true,
      link: link
		};

    function link(scope) {

			var conference = $rootScope.conference;

			scope.hasMerchantAccount = false;

      getMerchantAgreement();

      function getMerchantAgreement() {
        merchantAgreementDataService
          .getMerchantAgreement()
          .then(function(data){
            scope.hasMerchantAccount = true;
          }, function(error) {
            if(error.data.detail){
  						scope.hasMerchantAccount = false;
  					} else {
  						toastr.error('Error!', 'There has been a problem with your request.');
  					}
          })
      }

      scope.openMerchantAgreement = function () {
				ngDialog.open({
					plain: true,
					template: "<merchant-agreement-modal></merchant-agreement-modal>",
					className: 'app/stylesheets/_merchantAgreementModal.scss'
				})
        scope.closeThisDialog();
			}

			scope.startRegistrationForm = function () {
				conferenceRegistrationDataService
					.createRegistrationForm(conference.conference_id)
					.then(function(data){
						$rootScope.conference.has_registration_form = true;
						$state.go('conferences.dashboard.registration.fields');
						scope.closeThisDialog();
					}, function(error){
						toastr.error('Try again!', 'There has been a problem with your request.');
					})




			}



    }

	}

}());
