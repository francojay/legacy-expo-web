(function(){
  'use strict';

  var directiveId = 'merchantAgreementModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', '$state', 'API', 'toastr', 'fileService', 'conferenceRegistrationDataService', 'merchantAgreementDataService', merchantAgreementModal]);

    function merchantAgreementModal($rootScope, $state, API, toastr, fileService, conferenceRegistrationDataService, merchantAgreementDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_components/merchantAgreementModal/merchantAgreementModalView.html',
          link: link
      }

      function link(scope){
        scope.merchantAgreementStep = 1;
        scope.loadingData = false;
        scope.merchantIdentityVerified = false;

        getMerchantAgreement();

        scope.merchantAgreementBack = function() {
          if(scope.merchantAgreementStep == 2 && scope.merchantIdentityVerified == true){
            toastr.warning('You have already accepted the merchant agreement terms.');
          } else {
            scope.merchantAgreementStep--;
          }
        }

        scope.merchantAgreementNext = function() {
          if(scope.merchantAgreementStep == 1){
            if(!scope.agreeMerchantTerms){
              toastr.warning('You must agree with the merchant agreement by checking the box after you have read the agreement.');
            } else {
              scope.merchantAgreementStep++;
            }
          } else if(scope.merchantAgreementStep == 2){
            if(validateCurrentForm()){
              scope.merchantAgreementStep++;
            }
          }
        }

        scope.submitMerchantAgreement = function() {
          scope.loadingData = true;
          if (scope.merchantIdentityVerified) {
            updateMerchantAccount();
          } else {
            var merchant_form = angular.element('#bank_information_form');
            Stripe.setPublishableKey(API.STRIPE_KEY);
            Stripe.bankAccount.createToken(merchant_form, stripeResponseHandlerBankAccount);
            scope.loadingData = false;
          }
        }

        function updateMerchantAccount() {
          var model;
          if(scope.merchantIndividualForm){
            model = scope.merchantIndividualProfileModel;
          } else if(scope.merchantCompanyForm){
            model = scope.merchantCompanyProfileModel
          }
          merchantAgreementDataService
            .submitMerchantAgreement(model)
            .then(function(response) {
              scope.loadingData = false;
              toastr.success('Merchant agreement updated.');
              scope.closeThisDialog();
            }, function(error) {
              scope.loadingData = false;
              toastr.error('Error!', 'There has been a problem with your request.');
            });
        }

        function stripeResponseHandlerBankAccount(status, response) {
          if (response.error) {
            scope.loadingData = false;
            toastr.error(response.error.message);
          } else {
            var model;
            if(scope.merchantIndividualForm){
              model = scope.merchantIndividualProfileModel;
            } else if(scope.merchantCompanyForm){
              model = scope.merchantCompanyProfileModel
            }
            model.stripe_token = response.id;
            merchantAgreementDataService
              .submitMerchantAgreement(model)
              .then(function(response) {
                scope.loadingData = false;
                toastr.success('Merchant agreement submitted.');
                scope.closeThisDialog();
              }, function(error) {
                scope.loadingData = false;
                toastr.error('Error!', 'There has been a problem with your request.');
              })
          }
        }

        function getMerchantAgreement() {
          scope.loadingData = true;
          merchantAgreementDataService
            .getMerchantAgreement()
            .then(function(data) {
              handleExistingMerchantAgreement(data);
            }, function(error) {
              if(error.data.detail){
                // do nothing
              } else {
                toastr.error('Error!', 'There has been a problem with your request.');
              }
            })
            .then(function(){
              scope.loadingData = false;
            })
        }

        function handleExistingMerchantAgreement(data) {
          scope.merchantIdentityVerified = true;
          scope.merchantAgreementStep = 2;
          var model = {
            'business_name'              : data.business_name,
            'line1'                      : data.line1,
            'city'                       : data.city,
            'state'                      : data.state,
            'country'                    : data.country,
            'postal_code'                : data.postal_code,
            'business_tax_id'            : data.business_tax_id_provided == true ? '*********' : '',
            'legal_entity_type'          : data.legal_entity_type,
            'last_name'                  : data.last_name,
            'first_name'                 : data.first_name,
            'dob_day'                    : data.dob_day,
            'dob_month'                  : data.dob_month,
            'dob_year'                   : data.dob_year,
            'ssn_last_4'                 : data.ssn_last_4_provided == true ? '****' : '',
            'personal_id_number_provided': data.ssn_last_4_provided ? true : false,
            'verification_document'      : data.verification_document,
            'has_verification_document'  : data.verification_document ? true : false
          };
          if(data.legal_entity_type == 'company'){
            scope.merchantCompanyProfileModel = model;
            scope.setFormType('company');
          } else if(data.legal_entity_type == 'individual'){
            scope.merchantIndividualProfileModel = model;
            scope.setFormType('individual');
          }
          scope.bankAccountData = {
            'bank_account_number' : '*****' + data.account.account_number_last4,
            'routing_number'      : data.account.routing_number,
            'bank_name'           : data.account.bank_name
          }
        }

        // STEP 1

        scope.agreeMerchantTerms = false;
        scope.toggleAgreeMerchantTerms = function() {
          scope.agreeMerchantTerms = !scope.agreeMerchantTerms;
        }

        // STEP 3

        scope.bankAccountData = {};

        // STEP 2

        scope.editingExistingForm     = true;
        scope.merchantCompanyForm     = true;
        scope.merchantIndividualForm  = false;
        scope.merchantCompanyFields = [
          {label:'Company Name',        code : 'business_name'},
          {label:'Address',             code : 'line1'},
          {label:'City',                code : 'city'},
          {label:"State",               code : 'state'},
          {label:"Country",             code : 'country'},
          {label:"Zip Code",            code : 'postal_code'},
          {label:"Business Tax ID",     code : 'business_tax_id'},
          {label:"Legal Entity Type",   code : 'legal_entity_type'},
          {label:"Your Last Name",      code : 'last_name'},
          {label:"Your First Name",     code : 'first_name'},
          {label:"Your Date of Birth",  code : ['dob_day', 'dob_month', 'dob_year']},
          {label:"SSN Last 4 Digits",   code : 'ssn_last_4'}
        ]
        scope.merchantIndividualFields = [
          {label:"Last Name",               code : 'last_name'},
          {label:"First Name",              code : 'first_name'},
          {label:"Legal Entity Type",       code : 'legal_entity_type'},
          {label:'Address',                 code : 'line1'},
          {label:'City',                    code : 'city'},
          {label:"State",                   code : 'state'},
          {label:"Country",                 code : 'country'},
          {label:"Zip Code",                code : 'postal_code'},
          {label:"Date of Birth",           code : ['dob_day', 'dob_month', 'dob_year']},
          {label:"Social Security Number",  code : 'personal_id_number'}
        ]
        scope.merchantCompanyProfileModel = {
          'country' : 'US'
        };
        scope.merchantIndividualProfileModel  = {
          'country' : 'US'
        }
        scope.unitedStatesList                = API.UNITED_STATES;
        scope.dobIsValid                      = false;

        scope.setFormType = function(type) {
          if(type == 'company') {
            scope.merchantCompanyForm = true;
            scope.merchantIndividualForm = false;
          } else if (type == 'individual') {
            scope.merchantCompanyForm = false;
            scope.merchantIndividualForm = true;
          }
        }

        scope.validateDate = function(profileModel){
          if (profileModel['dob_day'] != null && profileModel['dob_month'] != null && profileModel['dob_year'] != null){
            if(isValid(profileModel['dob_day'], profileModel['dob_month'], profileModel['dob_year']) == true){
              scope.dobIsValid = true;
            } else{
              scope.dobIsValid = false;
            }
          }
        }

        scope.validateDobDay = function(profileModel) {
          var daysMax = 31;
          if (profileModel['dob_month']) {
            daysMax = daysInMonth(profileModel['dob_month'], profileModel['dob_year']);
          }
          if (profileModel['dob_day'] < 1) profileModel['dob_day'] = 1;
          else if (profileModel['dob_day'] > daysMax) profileModel['dob_day'] = daysMax;
        }

        scope.validateDobMonth = function(profileModel) {
    			if (profileModel['dob_month'] < 1) profileModel['dob_month'] = 1;
    			else if (profileModel['dob_month'] > 12) profileModel['dob_month'] = 12;
    		}

        scope.validateDobYear = function(profileModel) {
    			if (profileModel['dob_year'] < 1900) profileModel['dob_year'] = 1900;
    			else if (profileModel['dob_year'] > 2017) profileModel['dob_year'] = 2017;
    		}

        scope.validateSSNlast4 = function() {
          scope.merchantCompanyProfileModel['ssn_last_4'] = scope.merchantCompanyProfileModel['ssn_last_4'].replace(/\D/g,'');
          scope.merchantCompanyProfileModel['ssn_last_4'] = scope.merchantCompanyProfileModel['ssn_last_4'].substring(0, 4);
        }

        scope.uploadMerchantDocument = function(files, type) {
          fileService
            .getUploadSignature(files[0])
            .then(function(response){
              var file_link = response.data.file_link;
              fileService
                .uploadFile(files[0], response)
                .then(function(uploadResponse){
                  if(type == 'company'){
                    scope.merchantCompanyProfileModel.verification_document = file_link;
                  } else {
                    scope.merchantIndividualProfileModel.verification_document = file_link;
                  }
                }, function(error) {
                  console.log(error)
                });
            }, function(error) {
              console.log(error);
              if (error.status == 403) {
                toastr.error('Permission Denied!');
              } else {
                toastr.error('Error!', 'There has been a problem with your request.');
              }
            });
        };

        function daysInMonth(m, y) {
          switch (m) {
            case 2 :
              return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
            case 9 : case 4 : case 6 : case 11 :
              return 30;
            default :
              return 31;
          }
        }

        function isValid(d, m, y) {
            return m >= 1 && m <= 12 && d <= daysInMonth(m, y);
        }

        function validateCurrentForm() {
          var ok = true;
          if(scope.merchantIndividualForm){
            if (!scope.merchantIndividualProfileModel.verification_document && !scope.merchantCompanyProfileModel.has_verification_document) {
              toastr.error('The verification document is required!');
              ok = false;
              return false;
            }
            if (!scope.merchantIndividualProfileModel.personal_id_number && !scope.merchantIndividualProfileModel.personal_id_number_provided) {
              toastr.error('The social security number is required!');
              ok = false;
              return false;
            }
            scope.merchantIndividualProfileModel['legal_entity_type'] = 'individual';
            _.each(scope.merchantIndividualFields, function(detail){
              if(detail.code != 'personal_id_number'
              && detail.code != 'verification_document'
              && detail.code != 'business_name'
              && detail.code != 'business_tax_id'){
                if(_.isArray(detail.code)){
                  _.each(detail.code, function(prop){
                    if(scope.merchantIndividualProfileModel[prop] == '' || scope.merchantIndividualProfileModel[prop] == null){
                      toastr.error(detail.label + ' field is required!');
                      ok = false;
                      return false;
                    }
                  });
                  return false;
                } else if(scope.merchantIndividualProfileModel[detail.code] == '' || scope.merchantIndividualProfileModel[detail.code] == null){
                  toastr.error(detail.label + ' field is required!');
                  ok = false;
                  return false;
                }
              }
            });
          } else if(scope.merchantCompanyForm){
              scope.merchantCompanyProfileModel['legal_entity_type'] = 'company';
              if (!scope.merchantCompanyProfileModel.verification_document && !scope.merchantCompanyProfileModel.has_verification_document) {
                toastr.error('The verification document is required!');
                ok = false;
                return false;
              }
              if (!scope.merchantCompanyProfileModel.business_tax_id && !scope.merchantCompanyProfileModel.has_business_tax_id) {
                toastr.error('The business tax id is required!');
                ok = false;
                return false;
              }
              if (!scope.merchantCompanyProfileModel.ssn_last_4 && !scope.merchantCompanyProfileModel.ssn_last_4_provided) {
                toastr.error('The SSN Last 4 Digits field is required!');
                ok = false;
                return false;
              } else if (scope.merchantCompanyProfileModel.ssn_last_4 && scope.merchantCompanyProfileModel.ssn_last_4.length < 4){
                toastr.error('The SSN Last 4 Digits field must have at least 4 digits!');
                ok = false;
                return false;
              }
              _.each(scope.merchantCompanyFields, function(detail){
                  if (detail.code == 'verification_document') {
                    if (!scope.merchantCompanyProfileModel.verification) {
                      toastr.error(detail.label + ' field is required!');
                      ok = false;
                      return false;
                    }
                  }
                  if(detail.code != 'business_tax_id' && detail.code != 'verification_document'){
                    if(_.isArray(detail.code)){
                      _.each(detail.code, function(prop){
                        if(scope.merchantCompanyProfileModel[prop] == '' || scope.merchantCompanyProfileModel[prop] == null){
                          toastr.error(detail.label + ' field is required!');
                          ok = false;
                          return false;
                        }
                      });
                      return false;
                    } else if(scope.merchantCompanyProfileModel[detail.code] == '' || scope.merchantCompanyProfileModel[detail.code] == null){
                      toastr.error(detail.label + ' field is required!');
                      ok = false;
                      return false;
                    }
                  }
              });
          }
          return ok;
        }




      }

    }
})();
