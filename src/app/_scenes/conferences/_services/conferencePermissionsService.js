(function(){
	'use strict';
  var serviceId = 'userPermissionsService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'requestFactory',
		'$timeout',
		conferencePermissionsService
	];
	app.service(serviceId, dependenciesArray);
	function conferencePermissionsService($q, requestFactory, $timeout) {
			var service = {};
      service.hasConferencePermission = function(hasPermissionTo) {
        var userPermissions = [
            'edit_conference', 'edit_conference_users', 'edit_attendee_fields', 'download_attendees',
            'manage_attendees', 'manage_exhibitors', 'manage_sessions', 'scan_attendees', 'can_refund', 'edit_registration'
        ];

        if(hasPermissionTo) {
					return true;
				} else {
					return false;
				}
      }

			service.findIfPermissionExists = function(permissionsList, permissionToCheck) {
				if (permissionToCheck === 'admin' && permissionsList.indexOf('admin') > -1) {
					return true;
				} else if (permissionsList.indexOf(permissionToCheck) > -1) {
					return true;
				}
				else {
					return false;
				}

			}
      return service;
      }
})();
