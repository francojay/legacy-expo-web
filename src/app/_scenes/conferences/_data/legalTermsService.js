(function(){
	'use strict';

  var serviceId = 'legalTermsService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', legalTermsService
		]);
		function legalTermsService($q, requestFactory){

  		var service = {};

      service.getLegalTerms = function(){
          var deferred = $q.defer();
          requestFactory.get(
              'api/v1.2/legal/terms/?policies=terms_of_service,privacy_policy'
          )
          .then(function(response){
               deferred.resolve(response.data);
          })
          .catch(function(error){
               deferred.reject(error);
          });

          return deferred.promise;
      }
			service.acceptLegalTerms = function(){
          var deferred = $q.defer();
          requestFactory.post(
              'api/v1.2/legal/terms/accept/',
							{"policies": ["terms_of_service", "privacy_policy"]},
							false

          )
          .then(function(response){
               deferred.resolve(response);
          })
          .catch(function(error){
               deferred.reject(error);
          });

          return deferred.promise;
      }

			return service;
		}


})();
