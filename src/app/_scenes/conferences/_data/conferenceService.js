(function(){
	'use strict';

  var serviceId 				= 'conferenceService';
	var app			 					= angular.module('app');
	var dependenciesArray = [
		'$q',
		'$http',
		'$filter',
		'toastr',
		'localStorageService',
		'requestFactory',
		conferenceService
	];

	app.service(serviceId, dependenciesArray);
	function conferenceService($q, $http, $filter, toastr, localStorageService, requestFactory){
				// BELOW IS WHAT SHOULD BE IN THE SERVICE
		var service = {};
    service.hash_conference_id = new Hashids('Expo', 7);

		service.createConference = function(data) {
	     var deferred = $q.defer();
	     var clonedData = _.cloneDeep(data);
	     requestFactory.post(
	          'api/v1.1/conferences/',
	          clonedData,
	          false
	      )
	      .then(function(response){
	          deferred.resolve(response.data);
	      })
	      .catch(function(error){
	          deferred.reject(error);
	      });
	      return deferred.promise;
	  }

		service.getConferenceById = function(conference_id) {
			var deferred = $q.defer();

			if(conference_id.length == 7) {
          conference_id = service.hash_conference_id.decode(conference_id)[0];
      }

			var url 		 = "api/conference/get/?conference_id=" + conference_id + "&for_web=1"

			requestFactory.get(url, {}, false).then(function(response) {
        var eventData 					 = {};
        eventData['eventName'] 	 = response.data.name;
        var starts_at_txt 			 = $filter('date')(response.data.date_from, "MM/dd/yyyy", response.data.time_zone);
				var ends_at_txt 				 = $filter('date')(response.data.date_to, "MM/dd/yyyy", response.data.time_zone);
        eventData['eventDate'] 	 = starts_at_txt + ' - ' + ends_at_txt;
        eventData['accountType'] = 'Organizer';
        var locationUrl = window.location.href.split('#!')[0] + '#!/conference/' + service.hash_conference_id.encode(response.data.conference_id);

        window.Intercom('update', {
					"eventDate" 		 : eventData.eventDate,
					"accountType" 	 : eventData.accountType,
					"eventName"			 : eventData.eventName,
					"eventCreated" 	 : eventData.eventCreated,
					"superAdminLink" : locationUrl
        });

				deferred.resolve(response.data);
      }, function(error) {
					// if (error.status == 403) {
					// 		toastr.error('Permission Denied!');
					// } else {
					// 		toastr.error('Error!', 'There has been a problem with your request.');
					// }
					deferred.reject(error);
			});

			return deferred.promise;
    }

						service.getAllConferences = function(page, q) {
                var url = 'api/conference/upcoming/';
								url += "?page=" + page + "&for_web_exhibitor=true";

                if (q && q.length > 2) {
                    url += "&q=" + q;
                }

                return requestFactory.get(
                    url, {}, true
                )
                .then(function(response){
                    return response.data;
                })
                .catch(function(error){
                    console.log(error);
                    return error;
                });
            }

						service.updateConference = function(data) {
                if(data.country && data.state) {
                    if (data.country.name) {
                        data.country = data.country.name;
                    }
                    if (data.state.abbreviation) {
                        data.state = data.state.abbreviation;
                    }
                    data = _.omit(data, 'user');
                    var exhibitor_only = data.exhibitor_only;
                    data = _.pickBy(data);
                    data.exhibitor_only = exhibitor_only;
                }

                if (data.date_from_editable) {
                    var date_from_editable = new Date(data.date_from_editable);
                    data.day_from = {
                        "year": date_from_editable.getFullYear(),
                        "month": date_from_editable.getMonth() + 1,
                        "day": date_from_editable.getDate()
                    }
                }

                if (data.date_to_editable) {
                    data.day_to = {
                        "year"	: parseInt(data.date_to_editable.format('Y')),
                        "month"	: parseInt(data.date_to_editable.format('M')),
                        "day"		: parseInt(data.date_to_editable.format('D'))
                    }
                }

                var deferred = $q.defer();
                requestFactory.post(
                    'api/v1.1/conference/' + data.conference_id,
                    data,
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

						service.deleteConference = function(data, conference_id) {
                var deferred = $q.defer();

                requestFactory.remove(
                    'api/conference/delete/' + '?conference_id=' + conference_id,
                    {},
                    false
                )
                .then(function(response){
                     deferred.resolve(response.data);
                })
                .catch(function(error){
                     deferred.reject(error);
                });

                return deferred.promise;
            }

						service.getUsersPermissions = function(conference_user_id){
								var deferred = $q.defer();
								requestFactory.get(
										'/api/v1.1/conference_user/' + conference_user_id,
										false
								)
								.then(function(response) {
										deferred.resolve(response.data);

								}).catch(function(error){
										console.log(error);
										deferred.reject(error);
								});

								return deferred.promise;
						};

				// ABOVE IS WHAT SHOULD BE IN THE SERVICE

            service.getConferenceSpeakers = function(conference_id){
                var deferred = $q.defer();
                requestFactory.get(
                    'api/v1.1/conference_speakers/' + conference_id + '/?page=1' ,
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            service.deleteConferenceSpeaker = function(speaker_id){
                var deferred = $q.defer();
                requestFactory.remove(
                    'api/v1.1/speaker/' + speaker_id + '/',
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            };

            service.getSuperAdminRevenue = function(conference_id){
                var deferred = $q.defer();
                requestFactory.get(
                    '/api/v1.1/super_admin/conference/' + conference_id + '/revenue' ,
                    {},
                    false
                )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });
                return deferred.promise;
            }

            service.saveConferenceSpeakerData = function(conference_id, speaker_data){
                var deferred = $q.defer();
                if(speaker_data.hasOwnProperty('id')){
                    requestFactory.post(
                        'api/v1.1/speaker/' + speaker_data.id + '/',
                        speaker_data,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
                } else{
                    requestFactory.post(
                            'api/v1.1/conference_speakers/' + conference_id + '/',
                            speaker_data,
                            false
                        )
                        .then(function(response){
                            deferred.resolve(response.data);
                        })
                        .catch(function(error){
                            deferred.reject(error);
                        });

                        return deferred.promise;
                }
            };

            service.updateCESupportContact = function(contact){
                var deferred = $q.defer();

                requestFactory.post(
                    'api/v1.1/conference_exhibitor_support_contact/' + contact.conference_support_contact_id + '/',
                    JSON.stringify(contact),
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            service.getSignedUrl = function() {
                var deferred = $q.defer();

                requestFactory.get(
                        'api/v1.1/get_signed_url/',
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.getCognitoCredentials = function() {
                var deferred = $q.defer();

                requestFactory.get(
                        'api/v1.1/get_cognito_identity/',
                        {},
                        false,
                        true
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.deleteCESupportContact = function(contact_id){
                var deferred = $q.defer();

                requestFactory.remove(
                    'api/v1.1/conference_exhibitor_support_contact/' + contact_id + '/',
                    {},
                    true
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            service.createCESupportContact = function(contact){
                var deferred = $q.defer();

                requestFactory.post(
                        'api/v1.1/conference_exhibitor_support_contacts/' + contact.conference_id + '/',
                        contact,
                        false
                    )
                    .then(function(response){
                        deferred.resolve(response.data);
                    })
                    .catch(function(error){
                        deferred.reject(error);
                    });

                    return deferred.promise;
            }

            service.reloadConferenceById = function(conference_id) {
                return requestFactory.get(
                    'api/conference/get/',
                    {
                        'conference_id': conference_id,
                        'for_web' : 1
                    },
                    false,
                    true
                )
                .then(function(response){
                    console.log(response.data);
                    return response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.getExhibitorCompleteData = function(conference_id) {
                if(conference_id.length == 7){
                    console.log(conference_id);
                    conference_id = service.hash_conference_id.decode(conference_id);
                }
                return requestFactory.get(
                    'api/exhibitor/complete_data/',
                    {
                        conference_id: conference_id[0],
                        for_web: true,
                    },
                    false
                )
                .then(function(response){
                    var eventData = {};
                    eventData['eventName'] = response.data.conference_info.name;
                    var starts_at_txt = $filter('date')(response.data.conference_info.date_from,
                        "MM/dd/yyyy", response.data.conference_info.time_zone);

                    var ends_at_txt = $filter('date')(response.data.conference_info.date_to,
                        "MM/dd/yyyy", response.data.conference_info.time_zone);
                    eventData['eventDate'] = starts_at_txt + ' - ' + ends_at_txt;
                    eventData['accountType'] = 'Exhibitor';
                    eventData['companyName'] = response.data.exhibitor_info.company_name;
                    var locationUrl = window.location.href.split('#!')[0] + '#!/conference/' +
                        service.hash_conference_id.encode(response.data.conference_info.conference_id);

                    window.Intercom('update', {
                            "eventDate": eventData.eventDate,
                            "eventName": eventData.eventName,
                            "accountType": eventData.accountType,
                            "eventCreated": eventData.eventCreated,
                            "superAdminLink": locationUrl
                        }
                    );
                    return  response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.reloadExhibitorCompleteData = function(conference_id) {
                return requestFactory.get(
                    'api/exhibitor/complete_data/',
                    {
                        conference_id: conference_id,
                        for_web: true
                    },
                    false,
                    true
                )
                .then(function(response){
                    return  response.data;
                })
                .catch(function(error){
                    console.log(error);
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }

            service.validateForm = function(formData){
                var reg = /^[A-z]/g;
                var tempFormData = formData;
								var confData = {};

               _.forIn(formData,function(val, key){
                    if((key === 'name') && (formData[key].charAt(0) !== formData[key].charAt(0).toUpperCase())){
                        formData[key] = formData[key];
                    } else if((key != 'logo' && key != 'website' && key != 'venue_url' && key != 'venue_phone_number' &&
                        key != 'enable_attendees') && (formData[key] === '')){
                        formData[key] = null;
                    }
                });
                if(tempFormData === formData){
                   confData = formData;
                   var data = _.cloneDeep(confData);
                   return $q.when(confData);
                } else{
                    confData = {};
                    return $q.when(confData)
                }
            };

            service.addTemplate= function(data) {
               var deferred = $q.defer();
               requestFactory.post(
                    'api/conference/template_add/',
                    data,
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }

            service.removeTemplate = function(conference_template_id) {
               var deferred = $q.defer();
               requestFactory.remove(
                    'api/conference/template_remove/' + '?conference_template_id=' + conference_template_id,
                    {},
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });
                return deferred.promise;
            }




			return service;
		}


})();
