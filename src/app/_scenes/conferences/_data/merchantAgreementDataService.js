(function(){
	'use strict';

  var serviceId = 'merchantAgreementDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', merchantAgreementDataService
		]);
		function merchantAgreementDataService($q, requestFactory){

  		var service = {};

      service.getMerchantAgreement = function(){
          var deferred = $q.defer();
          requestFactory.get(
              'api/v1.1/merchant_agreement/',
              {},
              false,
              true
          )
          .then(function(response){
               deferred.resolve(response.data);
          })
          .catch(function(error){
               deferred.reject(error);
          });

          return deferred.promise;
      }

      service.submitMerchantAgreement = function(merchant_profile){
          var deferred = $q.defer();
          requestFactory.post(
              'api/v1.1/merchant_agreement/',
              merchant_profile,
              false
          )
          .then(function(response){
               deferred.resolve(response.data);
          })
          .catch(function(error){
               deferred.reject(error);
          });

          return deferred.promise;
      }

			return service;
		}


})();
