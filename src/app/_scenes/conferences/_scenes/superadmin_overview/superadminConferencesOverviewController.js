(function() {
	'use strict'

  var controllerId = 'superadminConferencesOverviewController';
	var app					 = angular.module("app");

	var dependenciesArray = [
		'$scope',
		'$rootScope',
		'$state',
		'$location',
		'dataTableConfigGetter',
		superadminConferencesOverviewController
	];

	app.controller(controllerId, dependenciesArray);
	function superadminConferencesOverviewController($scope, $rootScope, $state, $location, dataTableConfigGetter) {
		$scope.isSuperAdmin = (localStorage.getItem("expoPass.is_superuser") == 'true');
		$scope.sharedInfo 						= $location.$$search.share;
		if ($scope.isSuperAdmin) {
			dataTableConfigGetter.getTableConfig(null, 'super_admin/conferences').then(function(response) {
				$scope.dataTableConfiguration = response;
				$rootScope.conference 				= null;
			}, function(error) {
				console.log(error);
			})
		} else {
			$state.go("conferences.overview");
		}
	}
})();
