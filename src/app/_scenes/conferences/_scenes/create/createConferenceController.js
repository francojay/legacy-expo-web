(function() {
	'use strict';
	var app = angular.module('app');
  var controllerId = 'createConferenceController';
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'API',
		'SharedProperties',
		'conferenceService',
		'fileService',
		'$q',
		'toastr',
		'$state',
		'$http',
		'intlTelInputOptions',
		'$filter',
		'$timeout',
		createConferenceController
	];

	app.controller(controllerId, dependenciesArray);

	function createConferenceController($rootScope, $scope, API, SharedProperties, conferenceService, fileService, $q, toastr, $state, $http, intlTelInputOptions, $filter, $timeout) {
      var sharedData = SharedProperties.sharedObject;
      sharedData.currentPageTitle = 'create_conf';
			$scope.show = false;
      $scope.userInvalidTimeZone = false;
      $scope.timeZoneIndex = -1;
      $scope.united_states = API.UNITED_STATES;
      $scope.countries = API.COUNTRIES;
      $scope.apiKey = API.GOOGLE_API_KEY;
			$scope.defaultTimezones = $filter('orderBy')(API.TIMEZONES, "value");
      $scope.conference = {
          min_date: format_date_for_validation(new Date()),
          country: $scope.countries[0],
          state: $scope.united_states[0],
          enable_attendees: true,
					venue_phone_number:""
      };
      (function(){
         var currentDate = new Date();
         $scope.currentDate = currentDate.getMonth() + 1 + '/' + currentDate.getDate() + '/' + currentDate.getFullYear();
      })();
			$scope.defaultCountryFlag = 'us';
      $scope.presetDate = function(){
          if($scope.conference.date_to === undefined || $scope.conference.date_to === true) {
              $scope.conference.date_to = _.cloneDeep($scope.conference.date_from);
              document.getElementById('conference_end_date').focus();
          }
      };

      $scope.setTimeZone = function(time_zone) {
          var index = -1;
          _.each($scope.defaultTimezones, function(tmz){
              index++;
              if(tmz.text == time_zone){
                  $scope.conference.time_zone = tmz.offset;
                  $scope.timeZoneIndex = index;
                  $scope.conference.timezone_name = tmz.utc[0];
                  return;
              }
          });
      };

      $scope.$watch(function(){
          return $scope.conference.date_from;
      }, function(date_from){
          $scope.conference['max_date'] = format_date_for_validation(date_from);
      });

      $scope.$watch(function(){
          return $scope.conference['country'];
      }, function(country){
          if(country !== $scope.countries[0]){
              $scope.conference['state'] = '';
          }else{
              $scope.conference['state'] = $scope.united_states[0];
          }
      });

      $scope.getConferenceTimezone = function() {
          var url = '';
          if($scope.conference.hasOwnProperty('zip_code') && $scope.conference.zip_code != '' && $scope.conference.hasOwnProperty('country') && $scope.conference.country.code != ""){
              if ($scope.conference.country.code != 'US') {
                  url = 'https://maps.googleapis.com/maps/api/geocode/json?address=country:' +
                      $scope.conference.country.name + '|postal_code:' + $scope.conference.zip_code +
											'&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
              } else {
                  if ($scope.conference.hasOwnProperty('state') && $scope.conference.state.abbreviation != "") {
                      url = 'https://maps.googleapis.com/maps/api/geocode/json?components=country:' +
                          $scope.conference.country.code + '|administrative_area:' +
                          $scope.conference.state.abbreviation +'|postal_code:' +
                          $scope.conference.zip_code + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
                  }
              }
          } else {
              $scope.userInvalidTimeZone = false;
              $scope.timeZoneIndex = -1;
              return;
          }

					$http.get(url).then(function(response) {
                  response = response.data;
                  if(response.status == "OK"){
                      var latitude = response.results[0].geometry.location.lat;
                      var longitude = response.results[0].geometry.location.lng;
                      var timestamp = new Date().getTime().toString();
                      $http({
                          'method': "GET",
                          'url': 'https://maps.googleapis.com/maps/api/timezone/json?location=' + latitude + ',' + longitude + '&timestamp=' +
                          timestamp.substring(0, timestamp.length - 3) + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk'
                      })
                          .then(function(timezone_response){
                              timezone_response = timezone_response.data;
                              if(timezone_response.status == "OK"){
                                  $scope.userInvalidTimeZone = false;
                                  $scope.conference.timezone_name = timezone_response.timeZoneId;
                              } else if(timezone_response.status != "OK"){
                                  $scope.userInvalidTimeZone = true;
																	$scope.conference.timezone_name = $scope.defaultTimezones[0].utc[0];

																	toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                                  $scope.timeZoneIndex = -1;
                              }
                          })
                          .catch(function(timezone_error){
                              $scope.userInvalidTimeZone = true;
															$scope.conference.timezone_name = $scope.defaultTimezones[0].utc[0];
															console.log($scope.conference.timezone_name);
															toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                              $scope.timeZoneIndex = -1;
                          });
                  } else if(response.status != "OK"){
                      $scope.userInvalidTimeZone = true;
											$scope.conference.timezone_name = $scope.defaultTimezones[0].utc[0];
											toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                      $scope.timeZoneIndex = -1;
											$scope.showTimezoneList = true;
                  }
              })
              .catch(function(error) {
                  $scope.userInvalidTimeZone = true;
									$scope.conference.timezone_name = $scope.defaultTimezones[0].utc[0];
									console.log($scope.conference.timezone_name);
									toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                  $scope.timeZoneIndex = -1;
              });
          };


      function format_date_for_validation(date) {
          if (!date) return format_date_for_validation(new Date());

          var dateObj = new Date(date);
          var dd = dateObj.getDate();
          var mm = dateObj.getMonth()+1; //January is 0!
          var yyyy = dateObj.getFullYear();

          if(dd<10) {
              dd='0'+dd
          }

          if(mm<10) {
              mm='0'+mm
          }
          var today = yyyy+'-'+mm+'-'+dd;
          return today;
      }
      $scope.createConferenceButtonClickable = true;
      $scope.createConference = function() {
				$scope.createConferenceButtonClickable = false;
          var conf = _.cloneDeep($scope.conference);

          var dateFrom = new Date($scope.conference.date_from);
          var dateTo = new Date($scope.conference.date_to);

          conf.day_from = {
              "year": dateFrom.getFullYear(),
              "month": dateFrom.getMonth() + 1,
              "day": dateFrom.getDate()
          }

          conf.day_to = {
              "year": dateTo.getFullYear(),
              "month": dateTo.getMonth() + 1,
              "day": dateTo.getDate()
          }

          if (!conf.name) {
              toastr.error("The Event Name is a required field");
							$scope.createConferenceButtonClickable = true;
              return;
          }

          if (!conf.venue) {
              toastr.error("The Event Venue field is required");
							$scope.createConferenceButtonClickable = true;
              return;
          }

					if (conf.hasOwnProperty('venue_phone_number') && conf.venue_phone_number === undefined ) {
						$scope.createConferenceButtonClickable = true;
            toastr.error("Please fill the Event Venue Phone Number field with a correct format");
            return;
          }

          if (!conf.street) {
						$scope.createConferenceButtonClickable = true;
            toastr.error("The Event Street field is required");
          	return;
          }

          if (!conf.city) {
						$scope.createConferenceButtonClickable = true;

            toastr.error("The Event City field is required");
            return;
          }

          if (!conf.country) {
						$scope.createConferenceButtonClickable = true;
            toastr.error("The Event Country field is required");
            return;
          }

          if (!conf.zip_code) {
						$scope.createConferenceButtonClickable = true;
            toastr.error("The Event Zip Code field is required");
            return;
          }

          if (!conf.timezone_name) {
						$scope.createConferenceButtonClickable = true;
            toastr.error("Please review Event's Country, Zip Code and state fields and correct them or select one from the dropdown.");
            return;
          }
					if (conf.country) {
						if (conf.country.code !== "US") {
							conf.state = "";
						} else {
							if (!conf.state) {
								$scope.createConferenceButtonClickable = true;
                toastr.error("The Event State field is required");
                return;
	            }

	            if (conf.state.abbreviation) {
	                if (conf.state.abbreviation == '--') {
										$scope.createConferenceButtonClickable = true;
                    toastr.error("The Event state field is required");
                    return;
	                }
	                conf.state = conf.state.abbreviation;
	            }
						}
					}
          if (conf.country.name) {
						$scope.createConferenceButtonClickable = true;
            conf.country = conf.country.name;
          }

          conf.zip_code = parseInt(conf.zip_code);


          conferenceService.validateForm(conf).then(function(response) {
						if (response.state === null) {
							response.state = "";
						}

						conferenceService.createConference(response).then(function(data) {
              $scope.env = API.ENV;
              if (data.is_user_first_conference) {
                  $scope.is_user_first_conference = true;
                  marketing.trackFirstConference(API.ENV);
                  marketing.trackFirstConferenceFB(API.ENV);
                  marketing.googleRemarketingTag(API.ENV);
              } else {
                  $scope.is_user_first_conference = false;
              }
              toastr.success('The Event has been created successfully.', 'Event Created!');
              var conference_id =  conferenceService.hash_conference_id.encode(data.id);
							$state.go('conferences.dashboard.details.overview',{conference_id: conference_id });
            }, function(error) {
							$scope.createConferenceButtonClickable = true;
							lib.processValidationError(error, toastr);
						});
          });
      };

      $scope.changeConferenceLogo = function(files) {
          var img = new Image;
          img.onload = function() {
              fileService.getUploadSignature(files[0])
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];
                      promises.push(fileService.uploadFile(files[0], response));

                      $q.all(promises).then(function(response){
                          $scope.conference.logo = file_link;
                      }).catch(function(error){
                          console.log(error);
                      });

                  })
                  .catch(function(error){
                      console.log(error);
                  });
          }
          img.src = files[0].$ngfBlobUrl;
      }
			$scope.updateCountryForPhoneInput = function(field) {
				if (field.code !== undefined) {
					var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
					var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
					number = number.replace("+"+countryData.dialCode,"");
					$rootScope.phoneIntlTelInputCtrl.setCountry(field.code.toLowerCase());
					countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
					if(number){
						$timeout(function() {
							$scope.conference.venue_phone_number = "+"+countryData.dialCode+number;
						}, 100);
					}


				}
			}
  }
})();
