(function(){
	'use strict';

  var serviceId = 'exhibitorDashboardDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'requestFactory',
		exhibitorDashboardDataService
	];

	app.service(serviceId, dependenciesArray);
	function exhibitorDashboardDataService($q, requestFactory) {
		var service = {};
		service.hashConferenceId = new Hashids('Expo', 7);

		service.getConferenceAsExhibitor = function(conferenceId) {
			var deferred = $q.defer();
			var url 		 = "api/exhibitor/complete_data/?conference_id=" + conferenceId + "&for_web=true";

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

    return service;
  }
})();
