(function(){
	'use strict'

	var directiveId 			= 'fiveFreeScansCardModal';
	var app 							= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'API',
		'conferenceExhibitorCompanyDataService',
		'toastr',
		'ngDialog',
		fiveFreeScansCardModal
	];
	app.directive(directiveId, dependenciesArray);
		function fiveFreeScansCardModal ($rootScope, API, conferenceExhibitorCompanyDataService, toastr, ngDialog) {
			return {
				restrict		: 'E',
				replace			: true,
				templateUrl	: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_components/fiveFreeScansCardModal/fiveFreeScansCardModalView.html',
				link				: link
			}

			function link(scope) {
				Stripe.setPublishableKey(API.STRIPE_KEY);

				var conference 			 = $rootScope.exhibitorConference.conference_info;
				var leadRetrievalFee = conference.initial_fee + conference.markup_fee;
				var promoCodeValue 	 = 0;

				scope.exhibitor 		 = scope.ngDialogData.exhibitor;
				scope.promoCode 		 = "";
				scope.typeOfCard 		 = "";
				scope.closeDialog = function() {
					scope.closeThisDialog();
				};

				scope.formatExpirationDate = function(date) {
					if (!date) {
							return
					}
					if (date.length >= 2) {
							var indexOfSlash = date.indexOf('/');
							if (indexOfSlash == -1) {
									date += '/';
							}
					}

					scope.expirationDate = date.replace('//', '/')
				};

				scope.purchaseFiveFreeScans = function() {
					if (leadRetrievalFee - promoCodeValue > 0) {
						var form = angular.element('#payment-form');
						Stripe.card.createToken(form, stripeResponseHandlerFiveFreeScans);
					}
				};

				scope.applyPromoCode = function(promoCode) {
					if(_.isEmpty(promoCode)){
							return;
					}
					conferenceExhibitorCompanyDataService.assignPromoCode(scope.ngDialogData.exhibitor.id, promoCode).then(function(response) {
						toastr.info('Promo code added!');
						var promoCodeValue = response.value;
					}, function(error) {
						scope.promoCodeError = true;
						scope.promoCode 			= null;
					});
				};

				scope.checkTypeOfCard = function(){
					scope.typeOfCard = lib.checkCreditCard(document);
				}

				scope.closeDialog = function() {
					ngDialog.closeAll();
				}

				function stripeResponseHandlerFiveFreeScans(status, response) {
					if (response.error) {
						toastr.error(response.error.message);
					} else {
						var token = response.id;
						var addedPromoCode = scope.promoCode;
						if (!promoCodeValue || promoCodeValue <= 0) {
								addedPromoCode = null;
						}
						conferenceExhibitorCompanyDataService.createExhibitorInitialPay(scope.exhibitor.id, token, addedPromoCode).then(function(response){
							scope.exhibitor.payment_status = 'card_stored';
							$rootScope.exhibitorConference.exhibitor_info.payment_status = 'card_stored';
							toastr.success("Congratulations, You have signed up for 'Five Free Scans'!");
							$rootScope.$broadcast("FIVE_FREE_SCANS_SIGNED_UP_DONE");
							ngDialog.closeAll();
							// $timeout(function() {
							// 		vm.showExibitorPromo();
							// }, 2000);
						}, function(error) {
							toastr.error(error.data.detail);
						});
					}
				};
			}
		}
})();
