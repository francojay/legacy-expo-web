(function(){
	'use strict'

	var directiveId 			= 'exhibitorSideMenu';
	var app 							= angular.module('app');
	var dependenciesArray = [
		exhibitorSideMenu
	];
	app.directive(directiveId, dependenciesArray);
		function exhibitorSideMenu (){
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_components/exhibitorSideMenu/exhibitorSideMenuView.html'
			}
		}
})();
