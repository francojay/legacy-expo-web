(function(){
	'use strict'

	var directiveId 			= 'fiveFreeScansModal';
	var app 							= angular.module('app');
	var dependenciesArray = [
		'ngDialog',
		'$timeout',
		fiveFreeScansModal
	];
	app.directive(directiveId, dependenciesArray);
	function fiveFreeScansModal(ngDialog, $timeout) {
		return {
			restrict		: 'E',
			replace			: true,
			templateUrl	: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_components/fiveFreeScansCTAModal/fiveFreeScansCTAModalView.html',
			link				: link
		}

		function link(scope) {
			scope.closeDialog = function() {
				scope.closeThisDialog();
			};

			scope.openInitialPaymentModal = function() {
				$timeout(function() {
					ngDialog.open({
						plain       : true,
						template    : '<five-free-scans-card-modal></five-free-scans-card-modal>',
						className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
						data 				: {
							exhibitor 	: scope.ngDialogData.exhibitor
						}
					});
				}, 200)

			}
		}
	}
})();
