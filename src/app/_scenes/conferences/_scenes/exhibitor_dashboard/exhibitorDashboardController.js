(function () {
  'use strict';

  var controllerId 			= 'exhibitorDashboardController';
	var app 				 			= angular.module('app');
	var dependenciesArray = [
		"$rootScope",
		exhibitorDashboardController
	];

	app.controller(controllerId, dependenciesArray);

  function exhibitorDashboardController($rootScope) {
  }
})();
