(function () {
    'use strict'

    var directiveId = 'deleteUsersConfirmModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
			'conferenceExhibitorCompanyDataService',
			deleteUsersConfirmModal
		];

		app.directive(directiveId, dependenciesArray);

    function deleteUsersConfirmModal($rootScope, $q, toastr, conferenceExhibitorCompanyDataService) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/users/_components/deleteUsersConfirmModal/deleteUsersConfirmModalView.html',
            link: link,
        }

        function link(scope) {
          	scope.numberOfUsers = scope.ngDialogData.numberOfUsers;

						scope.sendEventToDeleteSelectedUsers = function($event) {
							$rootScope.$broadcast("DELETE_SELECTED_EXHIBITOR_USERS", {});
						}
        }
    }
})();
