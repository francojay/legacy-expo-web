(function () {
  'use strict';

  var controllerId 			= 'exhibitorConferenceUsersController';
	var app 				 			= angular.module('app');
	var dependenciesArray = [
		"$rootScope",
		'$scope',
    '$timeout',
    'exhibitorConferenceUserDataService',
    'toastr',
    'ngDialog',
		exhibitorConferenceUsersController
	];

	app.controller(controllerId, dependenciesArray);

  function exhibitorConferenceUsersController($rootScope, $scope, $timeout, exhibitorConferenceUserDataService, toastr, ngDialog) {
    var exhibitorId               = $rootScope.exhibitorConference.exhibitor_info.id;
    $scope.conference 		        = $rootScope.exhibitorConference.conference_info;
    $scope.exhibitor              = $rootScope.exhibitorConference.exhibitor_info;
    $scope.users                  = $rootScope.exhibitorConference.exhibitor_users;
    $scope.numberOfUserSlotsLeft  = $scope.exhibitor.user_slots - 1;
    $scope.exhibitorUserFee       = $rootScope.exhibitorConference.conference_info.exhibitor_user_fee;
    $scope.userEmail              = "";
		$scope.userMailIsValid        = false;
    $scope.permissionsPopupIsOpen = false;
    $scope.selectedOneUser        = false;
    $scope.errorOnEmail           = false;
    $scope.selectedUsers          = [];

    $scope.permissions     = [
      {
        shownValue : "Scan Leads",
        key        : "scan_leads",
        value      : true
      },
      {
        shownValue : "Edit Qualifiers",
        key        : "edit_qualifiers",
        value      : true
      },
      {
        shownValue : "Edit User Permissions",
        key        : "manage_users",
        value      : true
      },
      {
        shownValue : "Edit Company Profile",
        key        : "edit_company_profile",
        value      : true
      },
      {
        shownValue : "Download Leads",
        key        : "download_leads",
        value      : true
      }
    ];

    $timeout(function() {
      $('.exhibitor-toolkit-users-wrapper .list .list-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
    })

		$scope.checkIfEmailAddressIsValid = function(emailAddress) {
			var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
			$scope.userMailIsValid = regexExpression.test(emailAddress);
		}

    $scope.toggleCheckForPermission   = function(field) {
      field.value = !field.value;
      if ($scope.selectedOneUser) {
        var permissionsArray = [];
        var obj              = {
          "exhibitor_user_id": $scope.selectedUsers[0].id,
          "rights" : []
        };

        angular.forEach($scope.permissions, function(permission) {
          if (permission.value === true) {
            permissionsArray.push(permission.key);
          }
        });

        obj.rights = permissionsArray;

        exhibitorConferenceUserDataService.editOne(obj).then(function(result) {
          toastr.success("Updated user rights with success!");
          for (var i = 0; i < $scope.users.length; i++) {
            if ($scope.users[i].id === result.id) {
              $scope.users[i].download_leads       = result.download_leads;
              $scope.users[i].edit_company_profile = result.edit_company_profile;
              $scope.users[i].manage_leads         = result.manage_leads;
              $scope.users[i].scan_leads           = result.scan_leads;
              $scope.users[i].permissions          = result.permissions;
              break
            }
          }
        }, function(error) {
          toastr.error("An error has occured while trying to update the rights!");
        });
      }
    }

    $scope.togglePermissionsPopup     = function(state) {
      if ($scope.userMailIsValid || $scope.selectedOneUser) {
        $scope.permissionsPopupIsOpen = !state;
      }
    }

    $scope.addUser                    = function(userEmail, permissions) {
      if ($scope.userMailIsValid) {
        exhibitorConferenceUserDataService.createOne(exhibitorId, userEmail.toLowerCase(), permissions).then(function(response) {
          $scope.users.push(response);
          $scope.errorOnEmail = false;
          $scope.userMailIsValid = false;
          $scope.permissionsPopupIsOpen = false;
          for (var i = 0; i < $scope.permissions.length; i++) {
            $scope.permissions[i].value = true;
          }
          $scope.userEmail = "";
        }, function(error) {
          toastr.error(error.data.detail);
          $scope.errorOnEmail = true;
        });
      } else {
        toastr.error("Please input a correct email address");
        $scope.errorOnEmail = true;
      }
    };

    $scope.selectUser                = function(user) {
      user.selected = !user.selected;
      if (user.selected) {
        $scope.selectedUsers.push(user);
      } else {
        for (var i = 0; i < $scope.selectedUsers.length; i++) {
          if ($scope.selectedUsers[i].id === user.id) {
            $scope.selectedUsers.splice(i, 1);
            break;
          }
        }
      };

      if ($scope.selectedUsers.length === 1) {
        $scope.selectedOneUser = true;
        angular.forEach($scope.permissions, function(permission) {
          var key = permission.key;
          permission.value = $scope.selectedUsers[0][key];
        });
      } else {
        $scope.selectedOneUser = false;
        if ($scope.selectedUsers.length === 0) {
          $scope.permissionsPopupIsOpen = false;
        }
      }
    }

    $scope.selectAllUsers            = function() {
      var selectAllUser = false;
      if($scope.selectedUsers.length != $scope.users.length){
        selectAllUser = true;
        $scope.selectedUsers = angular.copy($scope.users);
      }
      else{
        $scope.selectedUsers = [];
      }

      angular.forEach($scope.users, function(user) {
        user.selected = selectAllUser;
      });
    }

    $scope.openDeleteUsersModal = function($event, selectedUsers) {
      var numberOfUsers = 0;
      if (selectedUsers.length === $scope.users.length) {
        numberOfUsers = -1;
      } else {
        numberOfUsers = selectedUsers.length
      }
      $event.stopPropagation();
      ngDialog.open({
        plain       : true,
        template    : '<delete-users-confirm-modal></delete-users-confirm-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          numberOfUsers: numberOfUsers
        }
      });
    }

    $scope.deleteUsers               = function(selectedUsers) {
      var userIds = [];

      angular.forEach(selectedUsers, function(user) {
        userIds.push(user.id);
      });

      exhibitorConferenceUserDataService.deleteUsers(exhibitorId, userIds).then(function(response) {
        if (userIds.length === $scope.users.length) {
          $scope.selectedUsers = [];
          $scope.users         = [];
        } else {
          var copyOfUsers = angular.copy($scope.users);
          angular.forEach(userIds, function(id) {
            angular.forEach(copyOfUsers, function(user, index){
              if(user.id == id) {
                copyOfUsers.splice(index,1);
              }
            })
          });
          $scope.users = copyOfUsers;
          $scope.selectedUsers = [];
          for (var i = 0; i < $scope.permissions.length; i++) {
            $scope.permissions[i].value = true;
          }
        }
      }, function(error) {

      })
    }

    $scope.openBuyUsersModal         = function($event) {
      $event.stopPropagation();
      ngDialog.open({
        plain       : true,
        template    : '<lead-retrieval-buy-users-card-modal></lead-retrieval-buy-users-card-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitor               : $rootScope.exhibitorConference.exhibitor_info,
          numberOfUsersToPurchase : 1,
          exhibitorUserFee        : $scope.exhibitorUserFee
        }
      });
    }

    $rootScope.$on("SUCCESSFULLY_BOUGHT_EXHIBITOR_USERS", function(event, data) {
      $scope.numberOfUserSlotsLeft = $scope.numberOfUserSlotsLeft + data.numberOfUsers;
    });

    $rootScope.$on("DELETE_SELECTED_EXHIBITOR_USERS", function(event, data) {
      $scope.deleteUsers($scope.selectedUsers);
    })
  }
})();
