(function(){
	'use strict';

  var serviceId = 'exhibitorConferenceUserDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'requestFactory',
		exhibitorConferenceUserDataService
	];

	app.service(serviceId, dependenciesArray);
	function exhibitorConferenceUserDataService($q, requestFactory) {
		var service = {};

		service.createOne = function(exhibitorId, userEmail, permissions) {
			var deferred = $q.defer();
			var url			 = "api/exhibitor/user_add/"
			var data = {
				exhibitor_id			: exhibitorId,
				invited_user_email: userEmail,
				rights						: service._processPermissions(permissions),
			};

			var permissionKeysWithBooleanValues = service._mapPermissionsToBooleanValues(permissions);
			data = service._extendData(data, permissionKeysWithBooleanValues);

			requestFactory.post(url, data, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.deleteUsers = function(exhibitorId, userIds) {
			var deferred = $q.defer();
			var stringUserIds = userIds.toString();
			var url 		 = 'api/v1.1/exhibitor_users/' + exhibitorId + '?exhibitor_user_ids=' + stringUserIds;

			requestFactory.remove(url, {}, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.editOne = function(obj) {
			var deferred = $q.defer();
			var url			 = "api/exhibitor/user_update/";

			requestFactory.post(url, obj, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service._processPermissions = function(permissions) {
			var arr = [];

			angular.forEach(permissions, function(permission) {
				if (permission.value === true) {
					arr.push(permission.key);

				}
			});

			return arr;
		};

		service._mapPermissionsToBooleanValues = function(permissions) {
			var obj = {};

			angular.forEach(permissions, function(permission) {
				obj[permission.key] = permission.value;
			});

			return obj;
		}

		service._extendData 									= function(data, permissions) {
	    for (var key in permissions) {
	        if (permissions.hasOwnProperty(key)) data[key] = permissions[key];
	    }
	    return data;
		}

    return service;
  }
})();
