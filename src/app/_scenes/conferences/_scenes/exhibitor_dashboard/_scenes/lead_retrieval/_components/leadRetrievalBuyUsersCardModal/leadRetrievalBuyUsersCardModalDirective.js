(function(){
	'use strict'

	var directiveId 			= 'leadRetrievalBuyUsersCardModal';
	var app 							= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'API',
		'conferenceExhibitorCompanyDataService',
		'toastr',
		leadRetrievalBuyUsersCardModal
	];
	app.directive(directiveId, dependenciesArray);
		function leadRetrievalBuyUsersCardModal ($rootScope, API, conferenceExhibitorCompanyDataService, toastr) {
			return {
				restrict		: 'E',
				replace			: true,
				templateUrl	: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/lead_retrieval/_components/leadRetrievalBuyUsersCardModal/leadRetrievalBuyUsersCardModalView.html',
				link				: link
			}

			function link(scope) {
				var stripeToken 				= "";
				var conferenceId 				= $rootScope.exhibitorConference.conference_info.conference_id;

				Stripe.setPublishableKey(API.STRIPE_KEY);

				scope.model 					  = {
					numberOfUsers		 : scope.ngDialogData.numberOfUsersToPurchase,
					exhibitorUserFee : scope.ngDialogData.exhibitorUserFee,
					leadRetrievalFee : scope.ngDialogData.leadRetrievalFee,
					promoCode 			 : ""
				};

				scope.promoCode 				= {
					"code" 	: null,
					"id"	 	: null,
					"value" : 0
				};

				scope.exhibitorId 			= scope.ngDialogData.exhibitor.id;

				scope.formatExpirationDate = function(date) {
					if (!date) {
							return
					}
					if (date.length >= 2) {
							var indexOfSlash = date.indexOf('/');
							if (indexOfSlash == -1) {
									date += '/';
							}
					}

					scope.expirationDate = date.replace('//', '/')
				};

				scope.checkTypeOfCard = function(){
					scope.typeOfCard = lib.checkCreditCard(document);
				}

				scope.payForUpgrades = function() {
					if (scope.promoCode.value === 0) {
						scope.purchaseUpgrades(scope.model.numberOfUsers);
					} else if (scope.promoCode.value > 0) {
						buyLeadRetrievalWithPartialPromoCode(scope.promoCode.code)
					}
				}

				scope.revertToCardView = function() {
					scope.stepTwoActive = false;
				}

				scope.applyPromoCode = function(code) {
					conferenceExhibitorCompanyDataService.applyPromoCodeForLeadRetrievalPurchases(conferenceId, code).then(function(response) {
						scope.promoCode.code 	= response.code;
						scope.promoCode.id 	 	= response.id;
						scope.promoCode.value = response.value;
					}, function(error) {
						if (error.status === 404) {
							toastr.error("Please enter a valid promo code!")
						} else {
							toastr.error(error.data.detail);
						}
					});
				};

				scope.deletePromoCode = function() {
					scope.promoCode = {
						"code" 	: null,
						"id"	 	: null,
						"value" : 0
					};
					scope.model.promoCode = "";
				}

				scope.checkIfCreditCardIsNeeded = function() {
					if ((scope.model.leadRetrievalFee && scope.model.leadRetrievalFee - scope.promoCode.value > 0) || (scope.model.exhibitorUserFee * scope.model.numberOfUsers > 0)) {
						scope.stepTwoActive = true;
					} else if (scope.model.leadRetrievalFee - scope.promoCode.value <= 0) {
						conferenceExhibitorCompanyDataService.getLeadRetrievalWithoutFee(scope.exhibitorId, scope.promoCode.code).then(function(response) {
							toastr.success("Successfully purchased lead retrieval for free!");
							$rootScope.$broadcast("LEAD_RETRIEVAL_PURCHASE_SUCCESS", {});
							scope.closeThisDialog();
						}, function(error) {

						});
					}
				}

				scope.purchaseUpgrades = function(numberOfUsers) {
					if (!numberOfUsers) {
						toastr.error("Please select a value more than 1 users to purchase!");
						return
					}
					if (scope.model.leadRetrievalFee && scope.model.exhibitorUserFee) {
						purchaseUsers(numberOfUsers);
						purchaseLeadRetrieval();
					} else if (scope.model.leadRetrievalFee && !scope.model.exhibitorUserFee) {
						purchaseLeadRetrieval();
					} else if (!scope.model.leadRetrievalFee && scope.model.exhibitorUserFee) {
						purchaseUsers(numberOfUsers);
					}
				}

				var purchaseUsers = function(numberOfUsers) {
					var form = angular.element('#payment-form');
					if (!stripeToken) {
						Stripe.card.createToken(form, function(status, response) {
							stripeToken = response.id;
							conferenceExhibitorCompanyDataService.purchaseAdditionalUsers(scope.exhibitorId, stripeToken, numberOfUsers).then(function(response) {
								var usersPurchaseToastrString = "";
								if (numberOfUsers === 1) {
									usersPurchaseToastrString = "You successfully purchased one additional user!";
								} else {
									usersPurchaseToastrString = "You successfully purchased " + numberOfUsers + " additional users!";
								}
								toastr.success(usersPurchaseToastrString);
								$rootScope.$broadcast("SUCCESSFULLY_BOUGHT_EXHIBITOR_USERS", {numberOfUsers:  numberOfUsers});
								scope.closeThisDialog();
							}, function(error) {
								toastr.error(error.data.detail);
							})
						});
					} else if (stripeToken) {
						conferenceExhibitorCompanyDataService.purchaseAdditionalUsers(scope.exhibitorId, stripeToken, numberOfUsers).then(function(response) {
							var usersPurchaseToastrString = "";
							if (numberOfUsers === 1) {
								usersPurchaseToastrString = "You successfully purchased one additional user!";
							} else {
								usersPurchaseToastrString = "You successfully purchased " + numberOfUsers + " additional users!";
							}
							toastr.success(usersPurchaseToastrString);
							$rootScope.$broadcast("SUCCESSFULLY_BOUGHT_EXHIBITOR_USERS", {numberOfUsers:  numberOfUsers});
							scope.closeThisDialog();
						}, function(error) {
							toastr.error(error.data.detail);
						})
					}

				}

				var purchaseLeadRetrieval = function() {
					var form = angular.element('#payment-form');
					Stripe.card.createToken(form, function(status, response) {
						stripeToken = response.id;
						conferenceExhibitorCompanyDataService.purchaseInitialFee(scope.exhibitorId, stripeToken).then(function(response) {
							toastr.success("Successfully purchased lead retrieval for $" + scope.model.leadRetrievalFee);
							$rootScope.$broadcast("LEAD_RETRIEVAL_PURCHASE_SUCCESS", {});
							scope.closeThisDialog();
						}, function(error) {
							toastr.error(error.data.detail);
						})
					});
				}

				var buyLeadRetrievalWithPartialPromoCode = function (promoCode) {
					var form = angular.element('#payment-form');
					Stripe.card.createToken(form, function(status, response) {
						stripeToken = response.id;
						conferenceExhibitorCompanyDataService.buyLeadRetrievalWithPartialPromoCode(scope.exhibitorId, promoCode, stripeToken).then(function(response) {
							toastr.success("Successfully purchased lead retrieval for $" + (scope.model.leadRetrievalFee - scope.promoCode.value));
							$rootScope.$broadcast("LEAD_RETRIEVAL_PURCHASE_SUCCESS", {});
							scope.closeThisDialog();
						}, function(error) {
							toastr.error(error.data.detail);
						});
					});

				}

				function handleTokenResponse(status, response) {
					if (response.error) { // Problem!
						toastr.error(response.error.message);
					} else {
						stripeToken = response.id;
					}
				};
			}
		}
})();
