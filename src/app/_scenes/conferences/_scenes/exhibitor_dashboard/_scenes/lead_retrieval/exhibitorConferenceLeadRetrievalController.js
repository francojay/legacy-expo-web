(function () {
  'use strict';

  var controllerId 			= 'exhibitorConferenceLeadRetrievalController';
	var app 				 			= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'API',
    '$timeout',
    'ngDialog',
    'toastr',
    'exhibitorDashboardDataService',
    '$stateParams',
		exhibitorConferenceLeadRetrievalController
	];

	app.controller(controllerId, dependenciesArray);

  function exhibitorConferenceLeadRetrievalController($scope, $rootScope, API, $timeout, ngDialog, toastr, exhibitorDashboardDataService, $stateParams) {
    var hasInitialPayment = $rootScope.exhibitorConference.made_initial_payment;
    var paymentStatus     = $rootScope.exhibitorConference.exhibitor_info.payment_status;
    var hasFiveFreeScans  = $rootScope.exhibitorConference.exhibitor_info.five_free_scans;

    $scope.shouldShowLeadRetrievalPage = false;
    $scope.hasInitialPayment           = hasInitialPayment;
    $scope.hasFiveFreeScans            = hasFiveFreeScans;
    $scope.paymentStatus               = paymentStatus;
    $scope.exhibitorUserFee            = $rootScope.exhibitorConference.conference_info.exhibitor_user_fee;
    $scope.initialFee                  = $rootScope.exhibitorConference.conference_info.initial_fee;
    $scope.markupFee                   = $rootScope.exhibitorConference.conference_info.markup_fee;
    $scope.usersPurchaseSelected       = false;
    $scope.numberOfUsersToPurchase     = 1;

    if (!(!hasInitialPayment && (paymentStatus === 'none' || paymentStatus === 'payment_error') && hasFiveFreeScans)) {
      $scope.shouldShowLeadRetrievalPage = true;
    } else {
      ngDialog.open({
        plain       : true,
        template    : '<five-free-scans-modal></five-free-scans-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitor : $rootScope.exhibitorConference.exhibitor_info
        }
      });
    }

    $scope.toggleSelectionForUsersPurchase = function(state) {
      $scope.usersPurchaseSelected = !state;
    }

    $scope.toggleSelectionForLeadRetrieval  = function(state) {
      $scope.leadRetrievalSelected = !state;
    }

    $scope.openCardPopupForBuyingUpgrades    = function(numberOfUsersToPurchase) {
      if (!numberOfUsersToPurchase) {
        toastr.error("Please select a value more than 1 users to purchase!");
        return
      }

      ngDialog.open({
        plain       : true,
        template    : '<lead-retrieval-buy-users-card-modal></lead-retrieval-buy-users-card-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitor               : $rootScope.exhibitorConference.exhibitor_info,
          numberOfUsersToPurchase : numberOfUsersToPurchase,
          exhibitorUserFee        : $scope.usersPurchaseSelected ? $scope.exhibitorUserFee : null,
          leadRetrievalFee        : $scope.leadRetrievalSelected ? $scope.initialFee + $scope.markupFee : null
        }
      });
    }

    $rootScope.$on("LEAD_RETRIEVAL_PURCHASE_SUCCESS", function(event, data) {
      $scope.hasInitialPayment = true;
      $scope.leadRetrievalSelected = false;
      $rootScope.exhibitorConference.made_initial_payment = true;
    });

    $rootScope.$on("SUCCESSFULLY_BOUGHT_EXHIBITOR_USERS", function(event, data) {
      $scope.usersPurchaseSelected = false;
      $scope.numberOfUsersToPurchase = 1;
    });

    $rootScope.$on("FIVE_FREE_SCANS_SIGNED_UP_DONE", function(event, data) {
      var conferenceId = exhibitorDashboardDataService.hashConferenceId.decode($stateParams.conference_id)[0];
      exhibitorDashboardDataService.getConferenceAsExhibitor(conferenceId).then(function(data) {
        $rootScope.exhibitorConference = data;
        hasInitialPayment = $rootScope.exhibitorConference.made_initial_payment;
        paymentStatus     = $rootScope.exhibitorConference.exhibitor_info.payment_status;
        hasFiveFreeScans  = $rootScope.exhibitorConference.exhibitor_info.five_free_scans;

        $scope.hasInitialPayment           = hasInitialPayment;
        $scope.hasFiveFreeScans            = hasFiveFreeScans;
        $scope.paymentStatus               = paymentStatus;

        if (!(!hasInitialPayment && (paymentStatus === 'none' || paymentStatus === 'payment_error') && hasFiveFreeScans)) {
          $scope.shouldShowLeadRetrievalPage = true;
        }
      }, function(error) {
        console.log(error);
      });
    });
  }
})();
