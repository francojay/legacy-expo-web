(function(){
	'use strict';

  var serviceId = 'conferenceExhibitorCompanyDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'requestFactory',
		conferenceExhibitorCompanyDataService
	];

	app.service(serviceId, dependenciesArray);
	function conferenceExhibitorCompanyDataService($q, requestFactory) {
		var service = {};

		service.getExhibitorMaterials = function(exhibitorId) {
			var deferred = $q.defer();
			var url = "api/v1.1/exhibitor_files/" + exhibitorId + "/";

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

 			return deferred.promise;
		};

		service.getExhibitorContacts = function(exhibitorId) {
			var deferred = $q.defer();
			var url = "api/v1.1/exhibitor_support_contacts/" + exhibitorId + "/";

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

 			return deferred.promise;
		};

		service.deleteExhibitorContacts = function(exhibitorId, contactsIds) {
			var deferred = $q.defer();
			var url ="api/v1.1/exhibitor_support_contacts/" + exhibitorId + "?exhibitor_support_contact_ids=" + contactsIds.toString();

			requestFactory.remove(url, {}, false).then(function(response) {
				deferred.resolve(contactsIds);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.createExhibitorContact = function(exhibitorId, contact) {
			var deferred = $q.defer();
			var url = "api/v1.1/exhibitor_support_contacts/" + exhibitorId + "/";

			var model = {};

			angular.forEach(contact, function(field) {
				model[field.key] = field.value || null;
			});

			requestFactory.post(url, model, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.editExhibitorContact = function(exhibitorId, contact) {
			var deferred = $q.defer();
			var url = "api/v1.1/exhibitor_support_contact/" + exhibitorId + "/";

			var model = {};

			angular.forEach(contact, function(field) {
				model[field.key] = field.value || null;
			});

			requestFactory.post(url, model, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.assignMaterialToExhibitor = function(exhibitorId, data) {
			var deferred = $q.defer();
			var url = "api/v1.1/exhibitor_files/" + exhibitorId + "/";

			requestFactory.post(url, data, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.assignPromoCode = function(exhibitorId, promoCode){
				var deferred = $q.defer();
				var url 		 = "api/exhibitor/promo_codes_assign_updated/";
				var data 		 = {
						"exhibitor_id" : exhibitorId,
						"code"				 : promoCode
				};

				requestFactory.post(url, data, false, true).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
		};

		service.createExhibitorInitialPay = function(exhibitorId, token, code) {
				var deferred = $q.defer();
				var url ='api/v1.1/exhibitor_payments/' + exhibitorId + '/';

				var data = {
						"type": 'initial_fee',
						"stripe_token": token
				}

				if (code) {
						data['code'] = code;
				}

				requestFactory.post(url, data, false, true).then(function(response){
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
		};

		service.purchaseAdditionalUsers = function(exhibitorId, token, numberOfUsers) {
			var deferred = $q.defer();
			var url ='api/v1.1/exhibitor_payments/' + exhibitorId + '/';

			var data = {
				"type": 'exhibitor_user_fee',
				"stripe_token": token,
				"nr_users": numberOfUsers
			};

			requestFactory.post(url, data, false, true).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.purchaseInitialFee = function(exhibitorId, token) {
			var deferred = $q.defer();
			var url ='api/v1.1/exhibitor_payments/' + exhibitorId + '/';

			var data = {
				"type": "initial_fee",
				"stripe_token": token
			};

			requestFactory.post(url, data, false, true).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.applyPromoCodeForLeadRetrievalPurchases = function(conferenceId, code) {
			//THIS API NEEDS TO BE RE-DONE
			var deferred = $q.defer();
			var url 		 = "api/v1.1/promo_code_get_by_code/" + conferenceId +"/?code=" + code;

			requestFactory.get(url, {}, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.getLeadRetrievalWithoutFee = function(exhibitorId, code) {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/exhibitor_payments/" + exhibitorId + "/";
			var data 		 = {
				type : "initial_fee",
				code : code
			};

			requestFactory.post(url, data, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.buyLeadRetrievalWithPartialPromoCode = function(exhibitorId, promoCode, stripeToken) {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/exhibitor_payments/" + exhibitorId + "/";
			var data 		 = {
				"type"				 : "initial_fee",
				"stripe_token" : stripeToken,
				"code" 				 : promoCode
			};

			requestFactory.post(url, data, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.editExhibitor = function(exhibitorId, model, logo) {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/exhibitor/" + exhibitorId + "/";

			var obj 		 = {};
			angular.forEach(model, function(field) {
				if (field.value !== null) {
					if (field.key === 'state' && model[10].value === 'US') {
						obj[field.key] = field.value;
					} else if (field.key === 'state' && model[10].value !== 'US') {
						obj[field.key] = "";
					} else if(field.value.length >= 0){
						obj[field.key] = field.value;
					}
				}
			});
			obj.logo = logo;

			requestFactory.post(url, obj, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.deleteMaterial = function(materialId) {
			var deferred = $q.defer();
			var url 		 = "api/v1.1/exhibitor_file/" + materialId + "/";

			requestFactory.remove(url, {}, false).then(function(response) {
				deferred.resolve(materialId);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}

    return service;
  }
})();
