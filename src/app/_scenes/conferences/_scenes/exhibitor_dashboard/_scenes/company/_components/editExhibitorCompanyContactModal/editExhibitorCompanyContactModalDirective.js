(function () {
    'use strict'

    var directiveId = 'editExhibitorCompanyContactModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
			'conferenceExhibitorCompanyDataService',
      'intlTelInputOptions',
			editExhibitorCompanyContactModal
		];

		app.directive(directiveId, dependenciesArray);

    function editExhibitorCompanyContactModal($rootScope, $q, toastr, conferenceExhibitorCompanyDataService, intlTelInputOptions) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/company/_components/editExhibitorCompanyContactModal/editExhibitorCompanyContactModalView.html',
            link: link
        }

        function link(scope) {
          scope.errorOnEmailAddress = false;
          scope.model = [
            {
              "key"                     : "first_name",
              "mandatory"               : true,
              "shownValue"              : "First Name",
              "value"                   : scope.ngDialogData.contact.first_name || ""
            },
            {
              "key"                     : "last_name",
              "mandatory"               : true,
              "shownValue"              : "Last Name",
              "value"                   : scope.ngDialogData.contact.last_name || ""
            },
            {
              "key"                     : "area_of_support",
              "mandatory"               : true,
              "shownValue"              : "Area of Support",
              "value"                   : scope.ngDialogData.contact.area_of_support || ""
            },
            {
              "key"                      : "contact_email_address",
              "mandatory"                : true,
              "shownValue"               : "Email",
              "value"                    : scope.ngDialogData.contact.contact_email_address || ""
            },
            {
              "key"                      : "phone_number",
              "mandatory"                : false,
              "shownValue"               : "Phone Number",
              "value"                    : scope.ngDialogData.contact.phone_number || ""
            }
          ];

					var exhibitorId = scope.ngDialogData.exhibitorId;
          scope.defaultCountryFlag = scope.ngDialogData.defaultCountryFlag;

          scope.closeModal = function() {
            scope.closeThisDialog()
          };

          scope.saveContact = function(model, closeModal) {
            var errorOnMandatoryFields = false;
            var errorOnEmailAddress    = false;
            for (var i = 0; i <  model.length; i++) {
              var field = model[i];
              if (field.mandatory && !field.value) {
                toastr.error(field.shownValue + " is a required field. Please input a value!");
                errorOnMandatoryFields = true;
                break;
              } else if (field.key === "contact_email_address" && field.value) {
                errorOnEmailAddress = checkEmailValidity(field.value);
                if(errorOnEmailAddress) break;
              } else if(field.key === 'phone_number' && field.value === undefined){
                toastr.error(field.shownValue + " is not correct. Please input a phone number with correct format!");
                errorOnMandatoryFields = true;
                break;
              }
            };

            if (!errorOnEmailAddress && !errorOnMandatoryFields) {
              conferenceExhibitorCompanyDataService.editExhibitorContact(scope.ngDialogData.contact.id, model).then(function(response) {
                //console.log(response);
                $rootScope.$broadcast("EDIT_EXHIBITOR_CONTACT", response);
                if (closeModal) {
                  scope.closeThisDialog();
                } else {
                  angular.forEach(model, function(field) {
                    field.value = "";
                  });
                }
              }, function(error) {

              });


            }
          };

          var checkEmailValidity = function(emailAddress) {
            var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
            scope.errorOnEmailAddress = !(regexExpression.test(emailAddress));
            if (scope.errorOnEmailAddress) {
              toastr.error("Email address is not valid!");
            }
            return scope.errorOnEmailAddress;
          };
        }
    }
})();
