(function () {
  'use strict';

  var controllerId 			= 'exhibitorConferenceCompanyController';
	var app 				 			= angular.module('app');
	var dependenciesArray = [
		"$rootScope",
    "$scope",
    '$timeout',
    'API',
    'conferenceExhibitorCompanyDataService',
    'ngDialog',
    'fileService',
    '$q',
    'toastr',
    'intlTelInputOptions',
		exhibitorConferenceCompanyController
	];

	app.controller(controllerId, dependenciesArray);

  function exhibitorConferenceCompanyController($rootScope, $scope, $timeout, API, conferenceExhibitorCompanyDataService, ngDialog, fileService, $q, toastr, intlTelInputOptions) {
    var hasMadeInitialPayment          = !$rootScope.exhibitorConference.made_initial_payment;
    var paymentStatusHasNotBeenDone    = ($rootScope.exhibitorConference.exhibitor_info.payment_status === "none" ? true : false);
    var hasFiveFreeScansActivated      = $rootScope.exhibitorConference.exhibitor_info.five_free_scans;
    $scope.showFiveFreeScansWarning    = false;
    $scope.editMode                    = false;
    $scope.companyEmailIsValid         = true;
    $scope.exhibitor                   = $rootScope.exhibitorConference.exhibitor_info;
    $scope.exhibitorMaterials          = [];
    $scope.exhibitorContacts           = [];
    $scope.selectedContacts            = [];
    $scope.backupDataForCompanyProfile = [];
    $scope.backupForCompanyLogo        = $scope.exhibitor.logo;

    var logoLink                    = "";
    getExhibitorMaterials($scope.exhibitor.id);
    getExhibitorContacts($scope.exhibitor.id);

    $scope.model                    = [
      {
        shownValue : "Description",
        value      : $scope.exhibitor.description || "",
        key        : 'description',
        visible    : false
      },
      {
        shownValue : "Company Name",
        value      : $scope.exhibitor.company_name || "",
        key        : 'name',
        visible    : false
      },
      {
        shownValue : "Booth Number",
        value      : $scope.exhibitor.booth || "",
        key        : 'booth',
        visible    : false
      },
      {
        shownValue : "Exbhibitor Id",
        value      : $scope.exhibitor.id || "",
        key        : 'conference_id',
        visible    : false
      },
      {
        shownValue : "Admin Email",
        value      : $scope.exhibitor.admin_email || "",
        key        : 'admin_email',
        visible    : false
      },
      {
        shownValue : "Phone Number",
        value      : $scope.exhibitor.phone_number || "",
        key        : "phone_number",
        visible    : true,
      },
      {
        shownValue : "Street Address",
        value      : $scope.exhibitor.street_address || "",
        key        : "street_address",
        visible    : true,
      },
      {
        shownValue : "City",
        value      : $scope.exhibitor.city || "",
        key        : "city",
        visible    : true,
      },
      {
        shownValue : "State",
        value      : $scope.exhibitor.state || "",
        key        : "state",
        visible    : true,
        options    : API.UNITED_STATES
      },
      {
        shownValue : "Zip Code",
        value      : $scope.exhibitor.zip_code || "",
        key        : "zip_code",
        visible    : true,
      },
      {
        shownValue : "Country",
        value      : $scope.exhibitor.country || "",
        key        : "country",
        visible    : true,
        options    : API.COUNTRIES
      },
      {
        shownValue : "Website",
        value      : $scope.exhibitor.website || "",
        key        : "website",
        visible    : true,
      },
      {
        shownValue : "Promo Code",
        value      : $scope.exhibitor.promo_code || "No promo code issued",
        key        : "promo_code",
        visible    : true,
      }
    ];
    $scope.defaultCountryFlag = 'us';
    if(!$scope.exhibitor.country){
      angular.forEach(API.COUNTRIES, function(country) {
        if(country.name === $rootScope.exhibitorConference.conference_info.country){
          $scope.defaultCountryFlag = country.code.toLowerCase();
        }
      });
    }
    else{
        $scope.defaultCountryFlag = $scope.exhibitor.country.toLowerCase();
    }

    $timeout(function() {
      if (hasMadeInitialPayment && paymentStatusHasNotBeenDone && hasFiveFreeScansActivated) {
        $scope.showFiveFreeScansWarning = true;
      } else {
        $scope.showFiveFreeScansWarning = false;
      }
      $('.exhibitor-toolkit-profile-wrapper .profile-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
    }, 200);

    $scope.toggleContactSelection  = function($event, contact) {
      $event.stopPropagation();
      contact.selected = !contact.selected;
      if (contact.selected) {
        $scope.selectedContacts.push(contact.id);
      } else {
        for (var i = 0; i < $scope.selectedContacts.length; i++) {
          if ($scope.selectedContacts[i] === contact.id) {
            $scope.selectedContacts.splice(i, 1);
          }
        }
      }
    }

    $scope.toggleEditMode = function($event, state) {
      $event.stopPropagation();
      if (!state) {
        $scope.backupDataForCompanyProfile = angular.copy($scope.model);
      }

      $scope.editMode                    = !state;

      if (state) {
        $scope.model          = angular.copy($scope.backupDataForCompanyProfile)
        $scope.exhibitor.logo = $scope.backupForCompanyLogo;

      }
    }

    $scope.openAddContactModal = function() {
      ngDialog.open({
        plain       : true,
        template    : '<add-exhibitor-company-contact-modal></add-exhibitor-company-contact-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitorId : $scope.exhibitor.id,
          defaultCountryFlag: $scope.defaultCountryFlag
        }
      });
    }

    $scope.openFiveFreeScansCTA = function() {
      ngDialog.open({
        plain       : true,
        template    : '<five-free-scans-modal></five-free-scans-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitor : $scope.exhibitor
        }
      });
    }

    $scope.selectAllContacts = function(contacts) {
      $scope.selectedContacts = [];
      angular.forEach(contacts, function(contact) {
        contact.selected = true;
        $scope.selectedContacts.push(contact.id);
      })
    }

    $scope.removeContacts = function(contacts) {
      conferenceExhibitorCompanyDataService.deleteExhibitorContacts($scope.exhibitor.id, contacts).then(function(response) {
        for (var i = 0; i < $scope.exhibitorContacts.length; i++) {
          for (var j = 0; j < contacts.length; j++) {
            if ($scope.exhibitorContacts[i].id === contacts[j]) {
              $scope.exhibitorContacts.splice(i, 1);
            }
          }
        }

        $scope.selectedContacts = [];
      }, function(error) {
        toastr.error("An error has occured while trying to remove the contacts. Please try again later.")
      })
    }

    $scope.uploadMaterial = function(files) {
      if (files) {
        var file = files[0];
        fileService.getUploadSignature(file, false)
            .then(function(response) {
                var fileLink = response.data.file_link;
                var promises = [];
                var data = {
                    exhibitor: $scope.exhibitor.id,
                    file_url: fileLink,
                    name: file.name,
                    type: file.type
                };

                promises.push(fileService.uploadFile(file, response));
                promises.push(conferenceExhibitorCompanyDataService.assignMaterialToExhibitor($scope.exhibitor.id, data));

                $q.all(promises).then(function(response) {
                  var uploadedFileResponse = response[0];
                  var materialResponse     = response[1];
                  $scope.exhibitorMaterials.push(materialResponse);
                }).catch(function(error){
                  toastr.error('An error ocurred.');
                });
            }, function(error) {
              toastr.error('An error ocurred.');
            })
      }
    }

    $scope.saveChanges = function(editedExhibitor) {
      var errorsFound = false;

      angular.forEach(editedExhibitor, function(field) {
        if ((field.key === 'company_name' || field.key === 'admin_email') && !field.value) {
          toastr.error(field.shownValue + " is mandatory! Please fill it in before saving!");
          errorsFound = true;
        }
        else if(field.key === 'admin_email') {
          field.value = field.value.toLowerCase();
        }
        if (field.key === 'phone_number' && field.value == undefined) {
          toastr.error(field.shownValue + " should have a correct format!");
          errorsFound = true;
        }
      });

      if (!errorsFound && $scope.companyEmailIsValid) {
        conferenceExhibitorCompanyDataService.editExhibitor($scope.exhibitor.id, editedExhibitor, logoLink).then(function(response) {
          $scope.exhibitor = response;
          $scope.editMode  = false;
          toastr.success("Information updated successfully!");
        }, function(error) {
          if(error.data.detail){
            toastr.error(error.data.detail);
            return ;
          }
          toastr.error("An error has occured while trying to update your information!");
        })
      } else {
        toastr.error("The email address is not valid!");
      }
    }

    $scope.deleteMaterial = function(materialId) {
      conferenceExhibitorCompanyDataService.deleteMaterial(materialId).then(function(response) {
        angular.forEach($scope.exhibitorMaterials, function(material, index) {
          if (material.id === response) {
            $scope.exhibitorMaterials.splice(index, 1);
            toastr.success("Material deleted successfully!");
          }
        });
      }, function(error) {
        toastr.error("An error occured while trying to delete the material! Please try again later...");
      })
    }

    $scope.uploadNewLogo = function(files) {
      if (files) {
        fileService.getUploadSignature(files[0])
            .then(function (response) {
                fileService.uploadFile(files[0], response)
                    .then(function (uploadResponse) {
                      logoLink = response.data.file_link;
                      $scope.exhibitor.logo = logoLink;
                    }, function(error) {
                      toastr.error("An error has occured while trying to upload the company logo!")
                    });
            }, function(error) {
              if (error.status == 403) {
                  toastr.error('Permission Denied!');
              } else {
                  toastr.error('Error!', 'There has been a problem with your request.');
              }
            });
      }
    }

    $scope.verifyIfEmailIsValid = function(emailAddress) {
      var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
      $scope.companyEmailIsValid = regexExpression.test(emailAddress);
    }

    $scope.updateCountryForPhoneInput = function(field) {
      if (field.key === "country") {
        var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
        var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
        number = number.replace("+"+countryData.dialCode,"");
        $rootScope.phoneIntlTelInputCtrl.setCountry(field.value.toLowerCase());
        countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
        if(number){
          $timeout(function() {
            $scope.model[5].value = "+"+countryData.dialCode+number;
          }, 100);
        }
      }
    }

    function getExhibitorMaterials(id) {
      conferenceExhibitorCompanyDataService.getExhibitorMaterials(id).then(function(response) {
        $scope.exhibitorMaterials = response;
      }, function(error) {
        toastr.error("An error has occured while trying to get your materials.")
      })
    }

    function getExhibitorContacts(id) {
      conferenceExhibitorCompanyDataService.getExhibitorContacts(id).then(function(response) {
        $scope.exhibitorContacts = response;
      }, function(error) {
        toastr.error("An error has occured while trying to get your contacts");
      })
    }

    $rootScope.$on('ADDED_NEW_EXHIBITOR_CONTACT', function(event, data) {
      $scope.exhibitorContacts.push(data);
    })

    $rootScope.$on('EDIT_EXHIBITOR_CONTACT', function(event, data) {
      for(var i = 0; i < $scope.exhibitorContacts.length; i++ ){
        if($scope.exhibitorContacts[i].id == data.id) {
          $scope.exhibitorContacts[i].first_name = data.first_name;
          $scope.exhibitorContacts[i].last_name = data.last_name;
          $scope.exhibitorContacts[i].area_of_support = data.area_of_support;
          $scope.exhibitorContacts[i].contact_email_address = data.contact_email_address;
          $scope.exhibitorContacts[i].phone_number = data.phone_number;
        }
      }
    })

    $rootScope.$on("FIVE_FREE_SCANS_SIGNED_UP_DONE", function(event, data) {
      $scope.showFiveFreeScansWarning = false;
    });

    $scope.editExhibitorContact = function($event, contact) {
      $event.stopPropagation();
      ngDialog.open({
        plain       : true,
        template    : '<edit-exhibitor-company-contact-modal></edit-exhibitor-company-contact-modal>',
        className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        data        : {
          exhibitorId : $scope.exhibitor.id,
          defaultCountryFlag: $scope.defaultCountryFlag,
          contact           : contact
        }
      });
    }

   }
})();
