(function () {
  'use strict';

  var controllerId 			= 'exhibitorConferenceDetailsController';
	var app 				 			= angular.module('app');
	var dependenciesArray = [
		"$rootScope",
		'$scope',
		'API',
    '$timeout',
		exhibitorConferenceDetailsController
	];

	app.controller(controllerId, dependenciesArray);

  function exhibitorConferenceDetailsController($scope, $rootScope, API, $timeout) {
		$scope.conference 		 = angular.copy($rootScope.exhibitorConference.conference_info);
    $scope.contacts        = $scope.conference.conference_exhibitor_support_contacts;
    $scope.googleMap 			 = 'https://www.google.com/maps/place/' + $scope.conference.country + ',' + $scope.conference.street + ',' + $scope.conference.city;
		$scope.googleStaticMap = 'https://maps.googleapis.com/maps/api/staticmap?key=' + API.GOOGLE_API_KEY +
		 '&size=582x256&sensor=false&zoom=15&markers=color%3Ared%7Clabel%3A.%7C' + $scope.conference.country
			+ '%20' + $scope.conference.city + '%20' + $scope.conference.street;

		$scope.fieldsForView = [
			{
				nameForDisplay : "Date",
				value 				 : moment($scope.conference.date_from).tz($scope.conference.timezone_name).format('MM/DD/YYYY') + ' - ' + moment($scope.conference.date_to).tz($scope.conference.timezone_name).format('MM/DD/YYYY')
			},
			{
				nameForDisplay : "Venue",
				value 				 : $scope.conference.venue
			},
			{
				nameForDisplay : "Street Address",
				value 				 : $scope.conference.street
			},
			{
				nameForDisplay : "City",
				value 				 : $scope.conference.city
			},
			{
				nameForDisplay : "State",
				value 				 : $scope.conference.state.abbreviation || $scope.conference.state
			},
			{
				nameForDisplay : "Zip Code",
				value 				 : $scope.conference.zip_code
			},
			{
				nameForDisplay : "Country",
				value 				 : $scope.conference.country.name || $scope.conference.country
			},
			{
				nameForDisplay : "Time Zone",
				value 				 : $scope.conference.timezone_name
			},
			{
				nameForDisplay : "Website",
				value 				 : sanitizeUrl($scope.conference.website)
			},
			{
				nameForDisplay : "Event Code",
				value 				 : $scope.conference.code
			},
		];

    $timeout(function() {
      $('.exhibitor-toolkit-details-wrapper .conference-details-upper-part .half.details').mCustomScrollbar({'theme': 'minimal-dark'});
      $('.exhibitor-toolkit-details-wrapper .contacts-details-bottom-part .list').mCustomScrollbar({'theme': 'minimal-dark'});

    }, 0)

		function sanitizeUrl(url) {
				if (url && url.substring(0, 7) != "http://" && url.substring(0, 8) != "https://") {
						return "http://" + url;
				} else if(url && url.search('https://') == 0){
						return url.replace('https://', 'http://');
				}

				return url;
		}

  }
})();
