(function () {
    'use strict';

    var controllerId = 'averyTemplatesController';
		var app 				 = angular.module('app');
		var dependenciesArray = [
			'$scope',
			'$rootScope',
			'$state',
			'$stateParams',
      'averyTemplatesDataService',
      'FileService',
      '$q',
			averyTemplatesController
		]

		app.controller(controllerId, dependenciesArray);

    function averyTemplatesController($scope, $rootScope, $state, $stateParams, averyTemplatesDataService, FileService, $q) {
      var currentConferenceId = $rootScope.conference.conference_id;
      var fileTypes           = {
        'badge' : 1,
        'label' : 2
      }
			$scope.chosenType = 'badge';
			$scope.uploadedFile = null;
			$scope.uploadedTemplates = $rootScope.conference.templates;

			$scope.chooseType = function(type) {
				$scope.chosenType = type;
			}

			$scope.uploadAveryTemplate = function(files) {
				if (files) {
					$scope.uploadedFile = files[0];
				}
			};

      $scope.deleteTemplate     = function($event, templateId) {
        averyTemplatesDataService.deleteOne(templateId).then(function(response) {
          angular.forEach($scope.uploadedTemplates, function(template, index) {
            if (template.id === response) {
              $scope.uploadedTemplates.splice(index, 1);
            }
          });
        }, function(error) {

        })
      };

      $scope.uploadAveryTemplateToServer = function($event, template) {
        FileService.getUploadSignature(template, true)
            .then(function(response) {
                var fileLink = response.data.file_link;
                var promises = [];

                var data = {
                    conference_id: currentConferenceId,
                    file_url: fileLink,
                    name: template.name,
                    type: fileTypes[$scope.chosenType]
                }
                promises.push(FileService.uploadFile(template, response));
                promises.push(averyTemplatesDataService.assignAveryTemplateToConference(data));

                $q.all(promises).then(function(response) {
                  var uploadedFileResponse = response[0];
                  var templateResponse     = response[1];
                  $scope.uploadedTemplates.push(templateResponse);
                  $scope.uploadedFile      = null;
                  $scope.chosenType        = 'badge';
                }).catch(function(error){
                  toastr.error('An error ocurred.');
                });

            }, function(error) {
              toastr.error('An error ocurred.');
            })
      }
    }
})();
