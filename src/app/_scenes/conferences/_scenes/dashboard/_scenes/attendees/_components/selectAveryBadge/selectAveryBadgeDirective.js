(function(){
	'use strict'

	var directiveId 		= 'selectAveryBadge';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'actionCallerService',
		'ngDialog',
		selectAveryBadgeDirective
	];

	app.directive(directiveId, depdenciesArray);

  function selectAveryBadgeDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, actionCallerService, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/selectAveryBadge/selectAveryBadgeView.html',
		};
    function link (scope) {
			var conferenceId 						= $rootScope.conference.conference_id;

			scope.modalData 			 = scope.ngDialogData._data;
			scope.templates 			 = extractTemplatesFromData(scope.modalData.params);
			scope.selectedTemplate = scope.templates[0];

			genericDataTableInfoStore.setActiveAveryBadge(scope.selectedTemplate);

			scope.notifyChange = function(model) {
				genericDataTableInfoStore.setActiveAveryBadge(model);
			};

			scope.callAction = function(action) {
				actionCallerService.callAction(conferenceId, action).then(function(response) {
					var data 	 = response.data;
					var status = response.status;

					if (data.result_type === 'redirect_to') {
						$rootScope.$broadcast("NEW_PROCESS_HAS_BEEN_CREATED", data);
						scope.closeThisDialog();
					}
				}, function(error) {

				})
			}


			function extractTemplatesFromData(params) {
				var templates = null;

				for (var i = 0; i < params.length; i++) {
					if (params[i].name === "avery_template") {
						templates = params[i].options;
					}
				}

				return templates;
			}


    }
  }
})();
