(function () {
    'use strict'

    var directiveId = 'attendeeTableItem';
		var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'ngDialog',
      '$timeout',
      attendeeTableItem
    ];

		app.directive(directiveId, dependenciesArray);

		function attendeeTableItem($rootScope, attendeesEventsPropagator, ngDialog, $timeout) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeTableItem/attendeeTableItemView.html',
        link: link,
				attendee: "@"
      }

      function link(scope) {
        if (scope.$last) {
          $timeout(function() {
            $rootScope.$broadcast("LAST_ATTENDEE_TABLE_ITEM_DIRECTIVE_HAS_RENDERED")
          })
        }
				var availableEvents = attendeesEventsPropagator.availableEvents;

        scope.attendeeSelected = false;

				scope.toggleAttendeeSelection = function ($event, attendee, attendeeSelectionState) {
          $event.stopPropagation();
					scope.attendeeSelected = !attendeeSelectionState;

					var data = {
						attendee				: attendee,
						addedInTheList	: scope.attendeeSelected
					}

					attendeesEventsPropagator.propagateEvent(availableEvents.UPDATE_SELECTED_ATTENDEES_LIST, data)
				}

				scope.computeAttendeeQrCode   = function (attendee) {
          return attendee.attendee_id + ';' + unescape(encodeURIComponent(attendee.first_name)) + ';' + unescape(encodeURIComponent(attendee.last_name));
        }

        scope.openAttendeeQR = function (attendee) {
          var data = {};
          data.qrCode = scope.computeAttendeeQrCode(attendee);
          ngDialog.open({
              plain     : true,
              template  : '<attendee-qr-code-modal></attendee-qr-code-modal>',
              className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
              data      : data
          });
        }

        scope.openAttendeeDetails = function (attendee) {
          ngDialog.open({
            plain     : true,
            template  : '<attendee-details-modal-legacy></attendee-details-modal-legacy>',
            className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
            data      : attendee
             //data      : { conferenceId: $rootScope.conference.conference_id, attendeeId: attendee.id }
          });
        }

        $rootScope.$on(availableEvents.UPDATE_SELECTED_ALL_ATTENDEES, function(event, data){
          scope.attendeeSelected = data.state;
        })
  		}
		}
})();
