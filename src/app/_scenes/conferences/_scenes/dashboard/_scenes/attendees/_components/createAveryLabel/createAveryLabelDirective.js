(function(){
	'use strict'

	var directiveId 		= 'createAveryLabel';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'actionCallerService',
		'ngDialog',
		createAveryLabelDirective
	];

	app.directive(directiveId, depdenciesArray);

  function createAveryLabelDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, actionCallerService, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/createAveryLabel/createAveryLabelView.html',
		};
    function link (scope) {
			var conferenceId = $rootScope.conference.conference_id;
			scope.modalData	 = scope.ngDialogData._data;

			scope.callAction = function(action) {
				actionCallerService.callAction(conferenceId, action).then(function(response) {
					// ce-i de facut?
					var data 	 = response.data;
					var status = response.status;

					$rootScope.$broadcast("NEW_PROCESS_HAS_BEEN_CREATED", data);
					scope.closeThisDialog();
				}, function(error) {

				})
			}
    }
  }
})();
