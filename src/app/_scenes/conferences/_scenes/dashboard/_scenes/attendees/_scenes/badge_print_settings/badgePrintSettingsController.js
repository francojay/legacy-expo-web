(function () {
    var controllerId = 'badgePrintSettingsController';
    var module = angular.module('app');

    module.controller(controllerId, ['$scope', '$rootScope', '$q', '$state', '$stateParams', '$timeout', 'toastr', 'badgePrintSettingsDataService', 'FileService', badgePrintSettingsController]);

    function badgePrintSettingsController($scope, $rootScope, $q, $state, $stateParams, $timeout, toastr, badgePrintSettingsDataService, FileService) {
			$scope.selfCheckIn  = false;
			$scope.customImage  = null;
      $scope.badgePrintSettings = {};
      var conferenceId = $rootScope.conference.conference_id;

      $timeout(function(){
        $scope.displayLoader = true;
        badgePrintSettingsDataService.getBadgePrintSettings(conferenceId)
        .then(function(response){
          response = response.data;

          if(_.isEmpty(response)) {
            initDefaultSettings();
          } else {
            $scope.badgePrintSettings = response;
            $scope.customImage = response.coverImage;
            $scope.colorPickerButton = response.checkInButtonColor;
            $scope.colorPickerText = response.checkInButtonTextColor;

            $scope.colorPickerButtonChanged($scope.colorPickerButton)
            $scope.colorPickerTextChanged($scope.colorPickerText)
            $scope.updateButtonColor($scope.buttonColorRGB)
            $scope.updateTextColor($scope.textColorRGB)
          }

          $scope.displayLoader = false;

        }, function(error){
          initDefaultSettings();
          $scope.displayLoader = false;
        });
      }, 0);

      $scope.saveBadgePrintSettings = function() {
        $scope.badgePrintSettings.checkInButtonTextColor = $scope.colorPickerText;
        $scope.badgePrintSettings.checkInButtonColor = $scope.colorPickerButton;

        if($scope.customFileImage) {
            var promise = uploadCoverImageInS3($scope.customFileImage);

            promise.then(
              saveSettingsRequest,
              function(error) {
              }
            );
        } else {
          saveSettingsRequest()
        }
      }

      function saveSettingsRequest(response) {
        var hasSettings = $scope.badgePrintSettings.hasOwnProperty('createdAt');

        if(!hasSettings) {

          badgePrintSettingsDataService.createBadgePrintSettings(conferenceId, $scope.badgePrintSettings)
          .then(function(response) {
            toastr.success('Badge Print Settings Saved!');
          }, function(error) {
          });
        } else {
          badgePrintSettingsDataService.updateBadgePrintSettings(conferenceId, $scope.badgePrintSettings)
          .then(function(response) {
            toastr.success('Badge Print Settings Updated!');
          }, function(error) {
          });
        }
      }

      function uploadCoverImageInS3(image) {
        var deferred = $q.defer();

        FileService.getUploadSignature(image)
        .then(function(response){
            var file_link = response.data.file_link;
            var promises = [];

            promises.push(FileService.uploadFile($scope.customFileImage, response));

            $q.all(promises)
            .then(
               function(response){
                deferred.resolve(response);
                $scope.customImage = file_link;
                $scope.badgePrintSettings.coverImage = file_link;
            }, function(error) {
              deferred.reject(error)
            })
        }, function(error) {
          if (error.status == 403) {
              toastr.error('Permission Denied!');
          } else {
              toastr.error('Error!', 'There has been a problem with your image upload.');
          }
        });

        return deferred.promise;
      }

			$scope.changeSelfCheckInOption = function(value) {
				$scope.selfCheckIn = value;
			}

			$scope.uploadFiles = function(files, invalidFiles) {
        if (files) {
          $scope.customFileImage = files;
          $scope.customImage = files.$ngfBlobUrl;
				} else if (!_.isEmpty(invalidFiles)) {
          if (invalidFiles[0].$error === 'maxSize') {
            toastr.error('Image is too big to upload! Upload an image smaller than 4MB.');
          } else if (invalidFiles[0].$error === 'maxHeight' || invalidFiles[0].$error === 'maxWidth') {
            toastr.error('Your image is too large!');
          }
        }
			};

			$scope.clearCustomImage = function() {
				$scope.customImage = null;
        $scope.customFileImage = null;
        $scope.badgePrintSettings.coverImage = null;
			}

			$scope.colorPickerButtonChanged = function(value) {
        var result;
        if (value.length === 7) {
          result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value);
        } else if(value.length === 4) {
          result = /^#?([a-f\d]{1})([a-f\d]{1})([a-f\d]{1})$/i.exec(value);
          result[1] += result[1];
          result[2] += result[2];
          result[3] += result[3];
        }

		    $scope.buttonColorRGB = {
		        red 	: parseInt(result[1], 16),
		        green : parseInt(result[2], 16),
		        blue 	: parseInt(result[3], 16)
		    };
			}

			$scope.colorPickerTextChanged = function(value) {
        var result;
        if (value.length === 7) {
          result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value);
        } else if(value.length === 4) {
          result = /^#?([a-f\d]{1})([a-f\d]{1})([a-f\d]{1})$/i.exec(value);
          result[1] += result[1];
          result[2] += result[2];
          result[3] += result[3];
        }

		    $scope.textColorRGB = {
		        red 	: parseInt(result[1], 16),
		        green : parseInt(result[2], 16),
		        blue 	: parseInt(result[3], 16)
		    };
			}

			$scope.verifyHexColor = function(value) {
				var colorRegex = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;
				if (!colorRegex.test(value) && value.length === 7) {
					toastr.error("Invalid Color", "Please enter a valid color")
				}
			};

			$scope.updateButtonColor = function(rgb) {
				$scope.colorPickerButton = "#" + ((1 << 24) + (rgb.red << 16) + (rgb.green << 8) + rgb.blue).toString(16).slice(1);
			};

			$scope.updateTextColor = function(rgb) {
				$scope.colorPickerText = "#" + ((1 << 24) + (rgb.red << 16) + (rgb.green << 8) + rgb.blue).toString(16).slice(1);
			}

      function initDefaultSettings(){
    			$scope.colorPickerButton = "#ffffff";
    			$scope.colorPickerText	 = "#000000";
    			$scope.buttonColorRGB = {
    				red		: 255,
    				green : 255,
    				blue 	: 255
    			};

    			$scope.textColorRGB 	= {
    				red		: 0,
    				green	: 0,
    				blue	: 0
    			};
      }
		}
})();
