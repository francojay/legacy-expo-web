(function(){
  'use strict';

  var controllerId = 'expoBadgesController';
  var app          = angular.module('app');

  var dependenciesArray = [
    '$scope',
    '$rootScope',
    '$timeout',
    '$window',
    '$state',
    '$stateParams',
    'toastr',
    'ngDialog',
    'expoBadgesDataService',
    'conferenceRegistrationDataService',
    'registrationReviewDataService',
    expoBadgesController
  ];

  app.controller(controllerId, dependenciesArray);

  function expoBadgesController($scope, $rootScope, $timeout, $window, $state, $stateParams, toastr, ngDialog, expoBadgesDataService, conferenceRegistrationDataService, registrationReviewDataService) {
    var badgesFeatureIsActivated = $rootScope.conference.features.badge;
    if (!badgesFeatureIsActivated) {
      $state.go('conferences.dashboard.attendees.templates', {conference_id: $stateParams.conference_id});
    }
    var conferenceId = $rootScope.conference.conference_id;
    $scope.conferenceId = $state.params.conference_id;
    $scope.noExpoBadgesCreated = true;
    $scope.expoBadges = [];
    $scope.duplicateBadgeMark = -1;
    $scope.badgeOptionsMenu = -1;
    getConferenceRegistrationForm();
    getCustomAttendeeFields();
    $timeout(
      function(){
        expoBadgesDataService.getAllConferenceBadges(conferenceId)
        .then(function(response) {
          if(!_.isEmpty(response.data)) {
            $scope.noExpoBadgesCreated = false;
            $scope.expoBadges = _.orderBy(response.data, function(badge) {
              return badge.metadata.title;
            });

            getAllErrorMessages();
          } else {
            $scope.noExpoBadgesCreated = true;
          }

        }, function(error){
        });
      }, 0
    );

    $scope.renderBadge = function(){
      var badgeItemsTabs = document.getElementsByClassName('badge-item-preview-image');

      _.each(badgeItemsTabs, function(badgeView) {
        $timeout(function() {
          badgeView.className += ' display-and-grow';
        }, 500)
      });
    };

    $scope.openDeleteBadgeWarningPopUp = function(badgeId, index) {
      ngDialog.open({
        template: 'app/views/manage_templates/manage-expo-badges/delete-badge-pop-up/deleteBadgeWarningView.html',
        className: 'app/views/manage_templates/manage-expo-badges/delete-badge-pop-up/deleteBadgeWarningStyle.scss',
        data: {
          indexOfBadge: index,
          badgeId: badgeId,
          deleteThisBadge: $scope.deleteThisBadge
        }
      });
    }

    $scope.deleteThisBadge = function(badgeId, index, ngDialogId){
      expoBadgesDataService.deleteBadge(conferenceId, badgeId)
      .then(function(response){
        $scope.expoBadges.splice(index, 1);
        ngDialog.close(ngDialogId);
        toastr.success('Your Badge has been successfully deleted!');
        if ($scope.expoBadges.length === 0) {
          $scope.noExpoBadgesCreated = true;
        }
      }, function(error){
      });
    };

    $scope.duplicateThisBadge = function(badgeId, index){
      expoBadgesDataService.duplicateBadge(conferenceId, badgeId)
      .then(function(response){
        var badgeToDuplicate = $scope.expoBadges[index];
        toastr.success('Your Badge has been successfully duplicated!');
        badgeToDuplicate.id = response.data.badge_id;
        $scope.expoBadges.splice(index + 1, 0, badgeToDuplicate);

        $scope.duplicateBadgeMark = index + 1;
        $timeout(function(){
          $scope.duplicateBadgeMark = -1;
        }, 1000);

      }, function(error){
      });
    };

    $scope.openBadgeAssignment = function(badge) {
      expoBadgesDataService.getBadgeAssignmentRules(conferenceId, badge.id)
      .then(
        function(response) {
          ngDialog.open({
            template: 'app/views/manage_templates/manage-expo-badges/badge-assignment-pop-up/badgeAssignmentView.html',
            className: 'src/app/views/manage_templates/manage-expo-badges/badge-assignment-pop-up/badgeAssignmentStyle.scss',
            controller: 'badgeAssignmentController',
            data: {
              badgeToAssign: badge,
              alreadySettedUpBadgeAssignmentRules: response.data,
              registrationLevels: $scope.registrationLevels,
              attendeeCustomFields: $scope.savedCustomFields
            }
          });
      }, function(error) {
      });
    };

    $scope.editThisBadge = function(badgeId){
      var encodedfConferenceId = new Hashids("Expo", 7).encode(conferenceId)
      $state.go('template_editor.define_badge', {'conference_id': encodedfConferenceId, 'badge_id': badgeId});
    };

    $scope.toggleBadgeOptionsMenu = function(index){
      $scope.badgeOptionsMenu = $scope.badgeOptionsMenu !== index ? index : -1;
    };

    $scope.goToDefineStepForEditor = function() {
      $state.go('template_editor.define', {"conference_id": $stateParams.conference_id})
    }

    function getAllErrorMessages() {
      $scope.notificationsFromBadgeAssignments = $scope.expoBadges.filter(function(badge){
          return badge.hasOwnProperty('rules') && badge.rules.error_messages !== null && !_.isEmpty(badge.rules.error_messages)
      }).map(function(badge, index){
          return badge.rules.error_messages[index]
      }).map(function(error_message){
        if(error_message && error_message.hasOwnProperty('message'))
          return error_message.message
      });
    }

    function getConferenceRegistrationForm() {
      conferenceRegistrationDataService
        .getRegistrationForm(conferenceId)
        .then(function(data) {
          $scope.registrationLevels = data[0].registrationlevel_set;
        }, function(error) {
          toastr.error('Error!', 'There has been a problem with your request.');
        })
    }

    function getCustomAttendeeFields() {
      registrationReviewDataService
        .getCustomFieldsList(conferenceId)
        .then(function(data) {
          $scope.savedCustomFields = data.custom_fields;
        }, function(error) {
          toastr.error('Error!', 'There has been a problem with your request.');
        });
    }

  }
})();
