(function(){
  'use strict';

  var directiveId = 'attendeePaymentIframe';
  var app = angular.module('app');

  var dependenciesArray = [
    '$rootScope',
    'attendeesEventsPropagator',
    'toastr',
    'attendeesDataService',
    '$sce',
    attendeePaymentIframe
  ];
  app.directive(directiveId, dependenciesArray);

    function attendeePaymentIframe($rootScope, attendeesEventsPropagator, toastr, attendeesDataService, $sce) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeePaymentIframe/attendeePaymentIframeView.html',
          link: link
      }

      function link(scope){
        scope.iframe_url  = scope.ngDialogData.iframe_url;

        scope.trustSrc = function(src) {
          return $sce.trustAsResourceUrl(src);
        }

      }


    }
})();
