// api/conference/template_remove/?conference_template_id=46
(function() {
    'use strict';

    var serviceId = 'averyTemplatesDataService';
    var app = angular.module('app');
    var dependenciesArray = [
        "$q",
        "API",
        "requestFactory",
        averyTemplatesDataService
    ]

    app.service(serviceId, dependenciesArray);

    function averyTemplatesDataService($q, API, requestFactory) {
        var service = {};

        service.createOne = function(conferenceId, fieldName, fieldOptions, fieldType) {
            var deferred = $q.defer();
            var url = 'api/conference/template_add/';

            var data = {
                conference_id: conferenceId,
                name: fieldName,
                type: fieldType,
                options: fieldOptions
            }

            requestFactory.post(url, data, false).then(function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        service.editOne = function(oldField, conferenceId, fieldName, fieldOptions, fieldType) {
            var deferred = $q.defer();
            var url = 'api/field/update/';
            oldField.conference_id = conferenceId;
            oldField.name = fieldName;
            oldField.type = fieldType;
            oldField.options = fieldOptions;

            requestFactory.post(url, oldField, false).then(function(response) {
                deferred.resolve(oldField);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.deleteOne = function(templateId) {
            var deferred = $q.defer();
            var url = 'api/conference/template_remove/?conference_template_id=' + templateId;

            requestFactory.remove(url, {}, false).then(function(response) {
                deferred.resolve(templateId);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }

        service.assignAveryTemplateToConference = function(data) {
            var deferred = $q.defer();
            var url = "api/conference/template_add/";

            requestFactory.post(url, data, false).then(function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        return service;
    }
})();