(function () {
    'use strict';

    var controllerId = 'customAttendeeFieldsController';
		var app 				 = angular.module('app');

		var dependenciesArray = [
			'$scope',
			'$rootScope',
			'$stateParams',
			'$localStorage',
			'SharedProperties',
			'conferenceDetailsService',
			'attendeesDataService',
			'toastr',
			'attendeesEventsPropagator',
      '$state',
      'customAttendeeFieldsDataService',
			customAttendeeFieldsController
		];

    app.controller(controllerId, dependenciesArray);

    function customAttendeeFieldsController($scope, $rootScope, $stateParams, $localStorage, SharedProperties, conferenceDetailsService, attendeesDataService, toastr, attendeesEventsPropagator, $state, customAttendeeFieldsDataService) {
      var fieldToBeEdited         = "";
      var conferenceId            = $rootScope.conference.conference_id;
      $scope.customAttendeeFields = $rootScope.conference.custom_fields;
      $scope.visibleSelector      = false;
      $scope.fieldName            = "";
      $scope.fieldNameError       = "";
      $scope.selectedType         = 'text';
      $scope.choiceOptions        = [
        {
          "value": ""
        }
      ]

      $scope.types           = {
        "multipleChoice"  : "Multiple Choice",
        "singleChoice"    : "Single Choice",
        "text"            : "Text"
      }

      $scope.toggleTypeSelector = function() {
        $scope.visibleSelector = !$scope.visibleSelector;
      }

      $scope.selectType         = function(type) {
        $scope.selectedType = type;
        if(type === 'multipleChoice' || type === 'singleChoice') {
          $scope.choiceOptions        = [
            {
              "value": ""
            }
          ];
        }
      }

      $scope.addAnotherOption   = function(option) {
        var obj = {
          "value": ""
        };

        if (option.value) {
          $scope.choiceOptions.push(obj);
          option.error = false;
        } else {
          option.error = true;
        }
      }

      $scope.removeOption       = function(index) {
        $scope.choiceOptions.splice(index, 1);
      }

      $scope.saveCustomField    = function (name, options, type) {
        var typesToSendToServer = {
          "text"           : 1,
          "singleChoice"   : 2,
          "multipleChoice" : 3
        }
        var foundErrorInOptions = false;
        if (!name) {
          $scope.fieldNameError = true;
        } else {
          $scope.fieldNameError = false;
        }

        if (type !== "text") {
          if (options.length < 2) {
            toastr.error("Please insert at least 2 options!")
            foundErrorInOptions = true;
          } else {
            angular.forEach(options, function(option) {
              if (!option.value) {
                option.error = true;
                foundErrorInOptions = true;
              } else {
                option.error = false;
              }
            });
          }

        }

        if (!$scope.fieldNameError && !foundErrorInOptions) {
          var type = typesToSendToServer[type];
          if (!fieldToBeEdited) {
            customAttendeeFieldsDataService.createOne(conferenceId, name, options, type).then(function(response) {
              $scope.fieldName = "";
              $scope.choiceOptions = [{
                value: ""
              }];
              $scope.selectedType = "text";
              $scope.customAttendeeFields.push(response);
            }, function(error) {
                if(error.data.detail) {
                  toastr.error(error.data.detail);
                  return;
                }
            })
          } else {
            //edit field
            customAttendeeFieldsDataService.editOne(fieldToBeEdited, conferenceId, name, options, type).then(function(response) {
              $scope.fieldName = "";
              $scope.choiceOptions = [{
                value: ""
              }];
              $scope.selectedType = "text";
              angular.forEach($scope.customAttendeeFields, function(field, index) {
                if (field.id === fieldToBeEdited.id) {
                  $scope.customAttendeeFields[index] = response;
                }
              })
              fieldToBeEdited = null;
            }, function(error) {
                if(error.data.detail) {
                  toastr.error(error.data.detail);
                  return;
                }
            })

          }
        } else {
        }
      }

      $scope.selectField = function(field) {
        fieldToBeEdited = field;
        var fieldsTypeMapping = {
          1: "text",
          2: "singleChoice",
          3: "multipleChoice"
        };
        $scope.fieldName = field.name;
        $scope.choiceOptions = field.options;
        $scope.selectedType = fieldsTypeMapping[field.type];
      }

      $scope.deleteField = function($event, fieldId) {
        $event.stopPropagation();
        customAttendeeFieldsDataService.deleteOne(fieldId).then(function(response) {
          angular.forEach($scope.customAttendeeFields, function(field, index) {
            if (field.id === response) {
              $scope.customAttendeeFields.splice(index, 1);
            }
          })
        })
      }
  	}
})();
