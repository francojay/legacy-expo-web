(function () {
    'use strict'

    var directiveId = 'attendeeDetailsModal';
		var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'attendeesDataService',
      '$timeout',
      '$q',
      'toastr',
      'API',
      'ngDialog',
      'fileService',
      'intlTelInputOptions',
      'transactionsDataService',
      attendeeDetailsModal
    ];

		app.directive(directiveId, dependenciesArray);

		function attendeeDetailsModal($rootScope, attendeesEventsPropagator, attendeesDataService, $timeout, $q, toastr, API, ngDialog, fileService, intlTelInputOptions, transactionsDataService) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeDetailsModal/attendeeDetailsModalView.html',
        link: link,
				attendee: "@"
      }

      function link(scope) {
        $timeout(function() {
          //$('.modal-wrapper.attendee-details .table-view .tables-contents-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
          $('.modal-wrapper.attendee-details .details-list-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
        }, 0);
        var conferenceId                  = $rootScope.conference.conference_id;
				var availableEvents               = attendeesEventsPropagator.availableEvents;
        var copyAttendee                  = null;
        scope.activeCustomAttendeeId      = $rootScope.conference.rule_type === "custom_id" ? true : false;
        scope.attendeeCustomFields        = angular.copy($rootScope.conference.custom_fields);
        scope.defaultCountryFlag          = 'us';
        scope.listIsOpened                = false;
        scope.countries                   = API.COUNTRIES;
        scope.states                      = API.UNITED_STATES;
        scope.detailsTabActive            = true;
        scope.sessionsTabActive           = false;
        scope.editModeActive              = false;
        scope.removedItem                 = scope.ngDialogData.removedItem || false;

        attendeesDataService.getAttendee(conferenceId, scope.ngDialogData._id, scope.removedItem).then(function(response) {
          scope.attendee                    = response._data.details;
          scope.sessionsForCurrentAttendee  = response._data.sessions;
          scope.fieldsForView               = response._data.meta.fields;
          initVars();
          // scope.currentAttendeeCustomFields = angular.copy(scope.attendee.customattendeefieldvalue_set);
          // scope.customFieldsWithAnswers     = createCustomFieldsForView(scope.attendeeCustomFields, scope.currentAttendeeCustomFields);
          //
          // initVars();
        }, function(error) {
          toastr.error("An error has occured when trying to fetch the attendee details.");
        })

        $timeout(function() {
          scope.activeTable = true;
        })

        scope.switchTab = function(type) {
          if (type === "details") {
            scope.detailsTabActive = true;
            scope.sessionsTabActive = false;
          } else if (type === "sessions") {
            scope.sessionsTabActive = true;
            scope.detailsTabActive = false;
          }
        }

        scope.editMode = function(state) {
          scope.activeTable = false;
          $timeout(function() {
          if(state) {
            copyAttendee = {
                        attendee: angular.copy(scope.attendee),
                        fields  : angular.copy(scope.fieldsForView),
                        sessions: angular.copy(scope.sessionsForCurrentAttendee)
                    };
            for(var j = 0; j < scope.fieldsForView.length; j++) {
                if(scope.fieldsForView[j].is_multi_select && scope.attendee[scope.fieldsForView[j].name]){
                  scope.attendee[scope.fieldsForView[j].name] = scope.attendee[scope.fieldsForView[j].name].split(",");
                }
            }
          } else {
            scope.attendee                   = copyAttendee.attendee;
            scope.fieldsForView              = copyAttendee.fields;
            scope.sessionsForCurrentAttendee = copyAttendee.sessions;
            initVars();
          }
          scope.activeTable = true;
          scope.listIsOpened = false;
          scope.editModeActive = state;
          }, 500);
        }

        scope.toggleRegistrationForSession = function(session) {
          session.is_registered = !session.is_registered;
        }

        scope.toggleAttendeedStatusForSessions = function(session) {
          session.was_scanned = !session.was_scanned;
        }

        scope.updateAttendee = function(attendee, fields, sessions) {
          var errorProofedEditedAttendee     = checkAttendeeForErrors(attendee);
          var foundErrorsInAttendeeObject    = errorProofedEditedAttendee.foundError;
          if (!foundErrorsInAttendeeObject) {
            var editedAttendee              = errorProofedEditedAttendee.attendee;
            editedAttendee                  = processCustomFields(fields, editedAttendee);
            editedAttendee                  = processStateCountryFields(fields, editedAttendee);
            var sessionsToSendToServer      = processSessions(sessions);
            editedAttendee.sessions         = {changes:{items : sessionsToSendToServer}};
            scope.activeTable = false;
            scope.editModeActive = false;
            attendeesDataService.updateAttendee(conferenceId, scope.ngDialogData._id, editedAttendee).then(function(response) {
              $rootScope.$broadcast('GET_DATA_WITH_EXISTING_FILTERS');
              toastr.success("Attendee was updated successfully!");
              scope.closeThisDialog();
            }, function(error) {
              toastr.error("An error occured while trying to update the attendee. Please try again later!");
            })
          }

        }

        scope.openAttendeeRefundModal = function ($event,attendee){
          $event.stopPropagation();
          ngDialog.open({
              plain     : true,
              template  : '<attendee-refund-modal></attendee-refund-modal>',
              className : 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeRefundModal/attendeeRefundModalStyles.scss',
              data      : {attendee:attendee,refundAmount:scope.paymentForCurrentAttendee.amountToRefund,attendeePayments:scope.paymentForCurrentAttendee}
          });
        }

        scope.uploadAttendeePhoto = function(files) {
          lib.squareifyImage($q, files)
              .then(function (response) {
                  var imgGile = response.file;

                  fileService.getUploadSignature(imgGile)
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];

                      promises.push(fileService.uploadFile(imgGile, response));

                      $q.all(promises).then(function(response){
                        scope.attendee.attendee_photo = file_link;

                      }).catch(function(error){

                      });

                  })
                      .catch(function (error) {

                          if (error.status == 403) {
                              toastr.error('Permission Denied!');
                          } else {
                              toastr.error('Error!', 'There has been a problem with your request.');
                          }
                      });
              });
				}

        scope.paymentWithOtherCard = function(attendeeId){
          attendeesDataService
            .paymentWithOtherCard(attendeeId).then(function(response) {
              scope.closeThisDialog();
              ngDialog.open({
                plain: true,
                template: '<attendee-payment-iframe></attendee-payment-iframe>',
                className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeePaymentIframe/attendeePaymentIframeStyles.scss',
                data: {iframe_url:response.consumer_url}
             });
            }, function(error) {
              toastr.error(error.data.detail);
            });

        }

        scope.retryPayWithFIS = function(attendeeId){
          attendeesDataService.retryPayWithFIS(attendeeId).then(function(response) {
            if(response.status == 200){
              toastr.success("The request was successfully processed. When the payment will be made the user will receive an email.");
            }
          }, function(error) {
            toastr.error(error.data.detail);
          });

        }

        scope.openAttendeeModalForInvitee = function(inviteeId) {
          toastr.warning('Getting invitee details, please wait...');
          attendeesDataService.getOne(inviteeId).then(function(response) {
            ngDialog.open({
              plain     : true,
              template  : '<attendee-details-modal></attendee-details-modal>',
              className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
              data      : response
            });
          }, function(error) {
            toastr.error("An error has occured while trying to get the invitee details!")
          });
        }

        scope.updateCountryForPhoneInput = function(field) {
          if (field.name === "country") {
            var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
            number = number.replace("+"+countryData.dialCode,"");
            angular.forEach(API.COUNTRIES, function(country) {
              if(country.name === scope.attendee['country']){
                $rootScope.phoneIntlTelInputCtrl.setCountry(country.code.toLowerCase());
              }
            });
            countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            if(number){
              $timeout(function() {
                scope.attendee['phone_number'] = "+"+countryData.dialCode+number;
              }, 100);
            }
          }
        }

        scope.toggleActionsList = function(event, state) {
          event.stopPropagation();
          if(scope.editModeActive){
            return
          }
          scope.listIsOpened = !state;
        }

        scope.hideDropdown = function(event) {
          scope.listIsOpened 					= false;
        }

        scope.resendReceipt = function() {
          if(scope.attendee.email_address && scope.attendee.registration_transaction_id){
            transactionsDataService.resendReceipt(conferenceId, scope.attendee.registration_transaction_id).then(function(){
              toastr.success("The receipt was sent to the user");
            },
            function(error){
              toastr.error(error.data.details);
            });
          }
        }

        scope.handleItemClick = function(further_action) {
          if (further_action) {
  						ngDialog.open({
  							plain: true,
  							template: further_action.modal_name,
  							className: '',
  							data: {
    							"_id" : further_action.id
    						}
  						});
        }
  			}

        $rootScope.$on(availableEvents.ATTENDEE_REFUND_SUCCESS, function(event, refundData) {
          scope.paymentForCurrentAttendee = refundData.attendeePayments;
          var amountToRefund = refundData.attendeePayments.amount;
          //calculation the remain amount to see if we can show the refund button or not
          angular.forEach(refundData.attendeePayments.refunds,function(refund){
            amountToRefund -= refund.amount;
          });
          scope.paymentForCurrentAttendee.amountToRefund = amountToRefund;
        });

        function processCustomFields(fields, attendee) {
          for(var j = 0; j < fields.length; j++) {
              if(fields[j].is_multi_select && scope.attendee[fields[j].name]){
                attendee[fields[j].name] = attendee[fields[j].name].join(",");
              }
          }
          return attendee;
        }

        function processSessions(sessions) {
          var arr = [];

          for (var i = 0; i < sessions.length; i++) {
            var obj = {};
            obj.id            = sessions[i].id;
            obj.is_registered = sessions[i].is_registered;
            obj.was_scanned   = sessions[i].was_scanned;

            arr.push(obj);
          }

          return arr;
        }

        function checkAttendeeForErrors(editedAttendee) {
          var obj = {
            foundError : false,
            attendee   : []
          }

          for (var i = 0; i < scope.fieldsForView.length; i++) {
            if (scope.fieldsForView[i].is_required && !editedAttendee[scope.fieldsForView[i].name]) {
              scope.fieldsForView[i].error = true;
              obj.foundError = true;
              toastr.error(scope.fieldsForView[i].label + ' is a mandatory field!');
            } else if(scope.fieldsForView[i].name === 'phone_number' && (editedAttendee[scope.fieldsForView[i].name] === undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1)) {
              scope.fieldsForView[i].error = true;
              obj.foundError = true;
              toastr.error('Please fill the '+scope.fieldsForView[i].label + ' with a correct format!');
            }
            else {
              scope.fieldsForView[i].error = false;
            }
          }

          obj.attendee = editedAttendee;
          return obj;

        }

        function processStateCountryFields(fields, attendee) {
          for(var j = 0; j < fields.length; j++) {
              if(fields[j].name === 'country' && attendee.country != 'United States'){
                attendee.state = null;
                break;
              }
          }

          return attendee;
        }

        function initVars() {
          scope.headerFields               = [
            {
              key           : "first_name"
            },
            {
              key           : "last_name"
            },
            {
              key           : "job_title"
            },
            {
              key           : "company_name"
            },
            {
              key           : "registration_level"
            }
          ];
          for(var i = 0; i < scope.headerFields.length; i++) {
            for(var j = 0; j < scope.fieldsForView.length; j++) {
                if(scope.headerFields[i].key === scope.fieldsForView[j].name){
                  scope.headerFields[i] = scope.fieldsForView[j];
                }
            }
          }


        }
  		}
		}
})();
