(function(){
	'use strict';

  var serviceId = 'attendeesDataService';
	var app 			= angular.module('app');

	app.service(serviceId, ['$q', 'API', 'requestFactory', attendeesDataService]);

	function attendeesDataService($q, API, requestFactory) {
    var service = {};

		//legacy
		service.getAll = function(conference_id, page, noLoader, attendeeFilters) {
			var deferred = $q.defer();
			noLoader = noLoader || false;
			attendeeFilters = attendeeFilters || {
	        queryString: null,
	        sortBy: null,
	        sortOrder: null
	    };
			var query_url = '/api/attendee/list/' + "?conference_id=" 	+ conference_id + '&page=' + page;
			if (attendeeFilters.sortBy) {
				if (attendeeFilters.queryString) {
	        query_url = '/api/attendee/list/'
						+ "?conference_id=" + conference_id
						+ '&page=' + page
						+ "&sort_by=" + attendeeFilters.sortBy
						+ "&sort_order=" + attendeeFilters.sortOrder
						+ "&q=" + attendeeFilters.queryString
						+ '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
						+ '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
				} else {
	        query_url = '/api/attendee/list/'
						+ "?conference_id=" + conference_id
						+ '&page=' + page
						+ "&sort_by=" + attendeeFilters.sortBy
						+ "&sort_order=" + attendeeFilters.sortOrder
						+ '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
						+ '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
				}
				if (attendeeFilters.queryString) {
					query_url = '/api/attendee/list/' + "?q=" + attendeeFilters.queryString
				}
			}
				requestFactory.get(query_url, {}, false, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};

		service.getOne = function(attendeeId) {
			var deferred = $q.defer();
			var url = 'api/v1.1/attendee/' + attendeeId + '/';

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.deleteSelectedAttendees = function(conferenceId, attendeesIds){
			var deferred = $q.defer();
			requestFactory.post(
				'api/attendee/multi_remove',
				{
				'conference_id' : conferenceId,
				'attendee_ids': attendeesIds,
				'remove_all': false
				},
				false
			).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};

		service.deleteAllAttendees = function(conferenceId) {
			var deferred = $q.defer();
			requestFactory.post(
				'api/attendee/multi_remove',
				{
				'conference_id' : conferenceId,
				'attendee_ids': [],
				'remove_all': true
				},
				false
			).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}

		service.getFilteredAttendees = function (conferenceId, page, noLoader, filters) {
			var queryUrl = '';
			noLoader = noLoader || false;
			if(!_.isEmpty(filters)){
				queryUrl = '/api/attendee/list'
					+ "?conference_id=" + conferenceId
					+ "&page=" + page
					+ "&sort_by=" + filters.sortBy
					+ "&sort_order=" + filters.sortOrder
					+ "&q=" + filters.queryString;
			} else {
				queryUrl = 'api/attendee/list'
					+ "?conference_id=" + conferenceId
					+ "&page" + page;
			}
			var deferred = $q.defer();
			requestFactory.get(
				queryUrl,
				{},
				false,
				false
			).then(function(response){
				deferred.resolve(response.data);
			}, function(error){
				deferred.reject(error);
			})
			return deferred.promise;
		}

		service.getSessionScannedAttendees = function(sessionId, page, noLoader, options){
			var query = '';
			noLoader = noLoader || false;
			if(!_.isEmpty(options)){
				query = 'api/session/scanned_attendees_list/' + "?session_id=" + sessionId + '&page=' + page +
					'&sort_by=' + options.sortBy + '&sort_order=' + options.sortOrder + '&q=' + options.queryString;
			} else{
				query = 'api/session/scanned_attendees_list/' + "?session_id=" + sessionId + '&page=' + page;
			}
			var deferred = $q.defer();
			requestFactory.get(
				query,
				{},
				false,
				noLoader
			).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};

		service.getPaymentsForOneAttendee			 	 = function(attendeeId) {
			var deferred = $q.defer();

			var url = 'api/v1.1/attendee_payments/' + attendeeId + '/';

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;

		}

		service.getSessionsForOneAttendee 			 = function(attendeeId) {
			var deferred = $q.defer();

			var url = 'api/session/sessions_list/?attendee_id=' + attendeeId;

			requestFactory.get(url, {}, false, true).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		};

		service.getScannedSessionsForOneAttendee = function(attendeeId) {
			var deferred = $q.defer();

			var url = 'api/session/scanned_list/?attendee_id=' + attendeeId;

			requestFactory.get(url, {}, false, true).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.bulkUploadAttendees 						 = function(conferenceId, attendees) {
			var deferred = $q.defer();
			var url 		 = 'api/attendee/bulk_create/';
			var data		 = {
					conference_id : conferenceId,
					attendees			: attendees
			}

			requestFactory.post(url, data, false).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.updateOne												 = function(originalAttendee, editedAttendee, fields, sessions) {
			var deferred			 = $q.defer();
			var url 		 			 = "api/attendee/update/";
			angular.forEach(editedAttendee, function(field) {
				if (field.value !== undefined) {
					originalAttendee[field.key] = field.value;
				}
				else if (field.key == 'state') {
					originalAttendee[field.key] = null;
				}
			});
			var propNames = Object.getOwnPropertyNames(originalAttendee); //deleting all the null/empty keys
			for (var i = 0; i < propNames.length; i++) {
				var propName = propNames[i];
				if ((originalAttendee[propName] === null && !propName != 'state') || originalAttendee[propName] === undefined || originalAttendee[propName] === "") {
					originalAttendee[propName] = null;
				}
			}
			originalAttendee.attendee_sessions = sessions;
			originalAttendee.custom_fields 		 = fields;

			requestFactory.post(url, originalAttendee, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.createOne 											= function(conferenceId, data) {
			var deferred = $q.defer();
			var url 		 = "api/attendee/create/";
			data.conference_id = conferenceId;

			requestFactory.post(url, data, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.downloadAttendees = function(conference_id, attendee_ids, extra, page) {
			var deferred = $q.defer();

			var url = "api/v1.1/download_attendees/" + conference_id + "/";

			if (page) {
				url = url + '?page=' + page;
			} else {
				url = url + '?page=1';
			}

			if (attendee_ids && attendee_ids.length > 0) {
				url = url + "&attendee_ids=" + attendee_ids;
				if (extra) {
					url = url + "&" + extra;
				}
			} else {
				if (extra) {
					url = url + "&" + extra;
				}
			}

			requestFactory.get(url).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.refundPayment = function(conferenceId, transactionId, refundAmount) {
			var deferred = $q.defer();

			var url  = "api/v1.1/conferences/" + conferenceId + "/transactions/" + transactionId + "/refund/";
			var data = {'amount' : refundAmount};
			requestFactory.post( url, data, false).then(function(response){
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		service.paymentWithOtherCard = function(attendeeId){
			var deferred = $q.defer();

			var url = "api/v1.1/super_admin/update_order_payment/" + attendeeId + "/";

			requestFactory.post(url).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}
		service.retryPayWithFIS = function(attendeeId){
			var deferred = $q.defer();

			var url = "api/v1.1/super_admin/retry_charge/" + attendeeId + "/";

			requestFactory.post(url).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.getAttendee = function(conferenceId, attendeeId, removedItem) {
			var deferred = $q.defer();
			var url = 'api/v1.1/conferences/' + conferenceId + '/attendees/' + attendeeId + '/';
			if (removedItem) {
				url += '?show_removed=true'
			}

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.updateAttendee = function(conferenceId, attendeeId, data) {
			var deferred = $q.defer();
			var url = 'api/v1.1/conferences/' + conferenceId + '/attendees/' + attendeeId + '/';

			requestFactory.post(url, data, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

    return service;
	}
})();
