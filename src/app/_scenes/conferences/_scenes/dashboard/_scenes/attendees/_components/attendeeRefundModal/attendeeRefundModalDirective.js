(function(){
  'use strict';

  var directiveId = 'attendeeRefundModal';
  var app = angular.module('app');

  var dependenciesArray = [
    '$rootScope',
    'attendeesEventsPropagator',
    'toastr',
    'attendeesDataService',
    attendeeRefundModal
  ];
  app.directive(directiveId, dependenciesArray);

    function attendeeRefundModal($rootScope, attendeesEventsPropagator, toastr, attendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeRefundModal/attendeeRefundModalView.html',
          link: link
      }

      function link(scope){

        var initialRefundAmount           = scope.ngDialogData.refundAmount;

        scope.attendee                    = scope.ngDialogData.attendee;
        scope.attendeePayments            = scope.ngDialogData.attendeePayments;
        scope.refundAmount                = Math.round(scope.ngDialogData.refundAmount * 100) / 100;
        scope.sendNotificationForRefund   = false;
        scope.lockRefundButton            = false;

        scope.forceMaxRefundAmount = function() {
          var maxAmount = initialRefundAmount;
          if (scope.refundAmount > maxAmount) {
            scope.refundAmount = maxAmount;
          }
        }

        scope.refundPayment = function(){
          if(!scope.lockRefundButton){
            if(scope.refundAmount > 0){
                scope.lockRefundButton = true;
                attendeesDataService
                  .refundAttendeePayment(scope.attendee.id, scope.refundAmount).then(function(response) {
                    postRefundProcessing(response, scope.attendee);
                    toastr.success("$" + scope.refundAmount + " have been successfully refunded!");
                    scope.closeThisDialog();
                    scope.lockRefundButton = false;
                  }, function(error) {
                    if(error.data.err_code == 117){
                      toastr.error('For this type of payment the refund process is not available at this moment. Please contact Support.');
                    }
                    scope.lockRefundButton = false;
                    scope.closeThisDialog();
                  });
              } else {
                toastr.error('You can not refund $' + scope.refundAmount + ' !');
                return;
              }

            }
            else {
              return;
            }
        };

        function postRefundProcessing(financial_data, attendee) {
          var refundData              = {};
          refundData.attendee         = attendee;
          refundData.attendeePayments = financial_data;
          attendeesEventsPropagator.propagateEvent(attendeesEventsPropagator.availableEvents.ATTENDEE_REFUND_SUCCESS, refundData);
        }


      }

    }
})();
