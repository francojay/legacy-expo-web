(function(){
    'use strict';

    var directiveId = 'deleteConferenceAttendeesModal';
    var app = angular.module('app');
    
    var dependenciesArray = [
      '$rootScope',
      deleteConferenceAttendeesModal
    ];

    app.directive(directiveId, dependenciesArray);

    function deleteConferenceAttendeesModal ($rootScope){

        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/deleteConferenceAttendeesModal/deleteConferenceAttendeesView.html',
            link: link
        };

        function link(scope){
          scope.numberOfAttendees = scope.ngDialogData.numberOfAttendees;
          scope.sendEventToDeleteSelectedAttendees = function($event) {
            $event.stopPropagation();
            $event.preventDefault();
            $rootScope.$broadcast('DELETE_CONFERENCE_ATTENDEES');
          }

          scope.sendEventToDeleteAllAttendees = function($event) {
            $event.stopPropagation();
            $event.preventDefault();
            $rootScope.$broadcast('DELETE_ALL_CONFERENCE_ATTENDEES');
          }
        }

    }
})();
