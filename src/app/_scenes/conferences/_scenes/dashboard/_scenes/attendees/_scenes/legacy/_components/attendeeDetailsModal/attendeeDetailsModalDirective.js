(function () {
    'use strict'

    var directiveId = 'attendeeDetailsModalLegacy';
		var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'attendeesDataService',
      '$timeout',
      '$q',
      'toastr',
      'API',
      'ngDialog',
      'fileService',
      'intlTelInputOptions',
      attendeeDetailsModalLegacy
    ];

		app.directive(directiveId, dependenciesArray);

		function attendeeDetailsModalLegacy($rootScope, attendeesEventsPropagator, attendeesDataService, $timeout, $q, toastr, API, ngDialog, fileService, intlTelInputOptions) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/legacy/_components/attendeeDetailsModal/attendeeDetailsModalView.html',
        link: link,
				attendee: "@"
      }

      function link(scope) {
        $timeout(function() {
          $('.modal-wrapper.attendee-details .table-view .tables-contents-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
          $('.modal-wrapper.attendee-details .details-list-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
        }, 0);
				var availableEvents               = attendeesEventsPropagator.availableEvents;
        scope.activeCustomAttendeeId      = $rootScope.conference.rule_type === "custom_id" ? true : false;
        scope.attendeeCustomFields        = angular.copy($rootScope.conference.custom_fields);
        scope.defaultCountryFlag          = 'us';

        if (scope.ngDialogData._id) {
          attendeesDataService.getOne(scope.ngDialogData._id).then(function(response) {
            scope.attendee                    = angular.copy(response);
            scope.currentAttendeeCustomFields = angular.copy(scope.attendee.customattendeefieldvalue_set);
            scope.customFieldsWithAnswers     = createCustomFieldsForView(scope.attendeeCustomFields, scope.currentAttendeeCustomFields);

            initVars();
          }, function(error) {
            toastr.error("An error has occured when trying to fetch the attendee details.");
          })
        } else {
          scope.attendee                    = angular.copy(scope.ngDialogData);
          scope.currentAttendeeCustomFields = angular.copy(scope.attendee.custom_fields);
          scope.customFieldsWithAnswers     = createCustomFieldsForView(scope.attendeeCustomFields, scope.currentAttendeeCustomFields);
          initVars();
        }
        scope.paymentsTabActive           = true;
        scope.sessionsTabActive           = false;
        scope.editModeActive              = false;

        $timeout(function() {
          scope.activeTable = true;
        })

        scope.switchTab = function(type) {
          if (type === "payments") {
            scope.paymentsTabActive = true;
            scope.sessionsTabActive = false;
          } else if (type === "sessions") {
            scope.sessionsTabActive = true;
            scope.paymentsTabActive = false;
          }
        }

        scope.activateEditMode = function() {
          scope.editModeActive = true;
          scope.switchTab('sessions');
        }

        scope.cancelEditMode = function() {
          scope.editModeActive = false;
          scope.closeThisDialog();
        }

        scope.toggleRegistrationForSession = function(session) {
          session.hasRegistered = !session.hasRegistered;
        }

        scope.toggleAttendeedStatusForSessions = function(session) {
          session.hasAttendeed = !session.hasAttendeed;
        }

        scope.updateAttendee = function(attendee, fields, sessions) {
          var errorProofedEditedAttendee     = checkAttendeeForErrors(attendee);
          var foundErrorsInAttendeeObject    = errorProofedEditedAttendee.foundError;
          scope.fieldsForEdit                = angular.copy(errorProofedEditedAttendee.attendee);
          if (!foundErrorsInAttendeeObject) {
            var fieldsToSendToServer        = processCustomFields(fields);
            var sessionsToSendToServer      = processSessions(sessions);
            var editedAttendee              = errorProofedEditedAttendee.attendee;
            editedAttendee                  = processStateCountryFields(editedAttendee);
            attendeesDataService.updateOne(scope.attendee, editedAttendee, fieldsToSendToServer, sessionsToSendToServer).then(function(response) {
              $rootScope.$broadcast('UPDATE_EDIT_ATTENDEES_LIST', response);
              toastr.success("Attendee was updated successfully!");
              scope.closeThisDialog();
            }, function(error) {
              toastr.error("An error occured while trying to update the attendee. Please try again later!");
            })
          }

        }

        scope.openAttendeeRefundModal = function ($event,attendee){
          $event.stopPropagation();
          ngDialog.open({
              plain     : true,
              template  : '<attendee-refund-modal></attendee-refund-modal>',
              className : 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeRefundModal/attendeeRefundModalStyles.scss',
              data      : {attendee:attendee,refundAmount:scope.paymentForCurrentAttendee.amountToRefund,attendeePayments:scope.paymentForCurrentAttendee}
          });
        }

        scope.uploadAttendeePhoto = function(files) {
          lib.squareifyImage($q, files)
              .then(function (response) {
                  var imgGile = response.file;

                  fileService.getUploadSignature(imgGile)
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];

                      promises.push(fileService.uploadFile(imgGile, response));

                      $q.all(promises).then(function(response){
                        scope.attendee.attendee_photo = file_link;

                      }).catch(function(error){

                      });

                  })
                      .catch(function (error) {

                          if (error.status == 403) {
                              toastr.error('Permission Denied!');
                          } else {
                              toastr.error('Error!', 'There has been a problem with your request.');
                          }
                      });
              });
				}

        scope.paymentWithOtherCard = function(attendeeId){
          attendeesDataService
            .paymentWithOtherCard(attendeeId).then(function(response) {
              scope.closeThisDialog();
              ngDialog.open({
                plain: true,
                template: '<attendee-payment-iframe></attendee-payment-iframe>',
                className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeePaymentIframe/attendeePaymentIframeStyles.scss',
                data: {iframe_url:response.consumer_url}
             });
            }, function(error) {
              toastr.error(error.data.detail);
            });

        }

        scope.retryPayWithFIS = function(attendeeId){
          attendeesDataService.retryPayWithFIS(attendeeId).then(function(response) {
            if(response.status == 200){
              toastr.success("The request was successfully processed. When the payment will be made the user will receive an email.");
            }
          }, function(error) {
            toastr.error(error.data.detail);
          });

        }

        scope.openAttendeeModalForInvitee = function(inviteeId) {
          toastr.warning('Getting invitee details, please wait...');
          attendeesDataService.getOne(inviteeId).then(function(response) {
            ngDialog.open({
              plain     : true,
              template  : '<attendee-details-modal></attendee-details-modal>',
              className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
              data      : response
            });
          }, function(error) {
            toastr.error("An error has occured while trying to get the invitee details!")
          });
        }

        scope.updateCountryForPhoneInput = function(field) {
          if (field.key === "country") {
            var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
            number = number.replace("+"+countryData.dialCode,"");
            angular.forEach(API.COUNTRIES, function(country) {
              if(country.name === field.value){
                $rootScope.phoneIntlTelInputCtrl.setCountry(country.code.toLowerCase());
              }
            });
            countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            if(number){
              $timeout(function() {
                scope.fieldsForEdit[6].value = "+"+countryData.dialCode+number;
              }, 100);
            }
          }
        }

        $rootScope.$on(availableEvents.ATTENDEE_REFUND_SUCCESS, function(event, refundData) {
          scope.paymentForCurrentAttendee = refundData.attendeePayments;
          var amountToRefund = refundData.attendeePayments.amount;
          //calculation the remain amount to see if we can show the refund button or not
          angular.forEach(refundData.attendeePayments.refunds,function(refund){
            amountToRefund -= refund.amount;
          });
          scope.paymentForCurrentAttendee.amountToRefund = amountToRefund;
        });

        function getAttendeePayments(attendeeId) {
          attendeesDataService
            .getPaymentsForOneAttendee(attendeeId).then(function(response) {
              if (response.ammount) {
                scope.paymentForCurrentAttendee            = angular.copy(response);
                var amountToRefund = response.amount;
                //calculation the remain amount to see if we can show the refund button or not
                angular.forEach(response.refunds,function(refund){
                  amountToRefund -= refund.amount;
                });
                scope.paymentForCurrentAttendee.amountToRefund = amountToRefund;
              }
            }, function(error) {
              toastr.error(error.data.detail);
            });
        }

        function getAttendeeSessions(attendeeId) {
          var sessionsThatAreOnlyAttended           = [];
          var promises = [];

          promises.push(attendeesDataService.getSessionsForOneAttendee(attendeeId));
          promises.push(attendeesDataService.getScannedSessionsForOneAttendee(attendeeId));

          $q.all(promises).then(function(response) {
            var registeredSessions = response[0].sessions_attendees;
            var scannedSessions    = response[1].scanned_attendees;
            var attendeeSessions   = angular.copy(registeredSessions);

            var attendeeSessionsIds = [];
            for (var i = 0; i < attendeeSessions.length; i++) {
              attendeeSessions[i].hasRegistered = true;
              attendeeSessionsIds.push(attendeeSessions[i].session.id);
            };

            for (var i = 0; i < scannedSessions.length; i++) {
              if (attendeeSessionsIds.indexOf(scannedSessions[i].session.id) < 0) {
                scannedSessions[i].hasAttendeed   = true;
                scannedSessions[i].hasRegistered  = false;

                attendeeSessions.push(scannedSessions[i]);
                attendeeSessionsIds.push(scannedSessions[i].session.id);
              } else {
                scannedSessions[i].hasRegistered  = true;
                scannedSessions[i].hasAttendeed   = true;

                attendeeSessions[attendeeSessionsIds.indexOf(scannedSessions[i].session.id)] = scannedSessions[i];
              }
            }

            scope.sessionsForCurrentAttendee = angular.copy(attendeeSessions)

          }, function(error) {
            toastr.error("An error has occured while trying to fetch the sessions for this attendee. Please try again later!")
          })
        }

        function createCustomFieldsForView(attendeeCustomFields, currentAttendeeCustomFields) {
          var arr = [];
          angular.forEach(attendeeCustomFields, function(field) {
            var obj = {};

            obj.options = field.options;
            obj.type    = field.type;
            obj.name    = field.name;
            obj.id      = field.id;
            obj.value   =  "";
            obj.validOption = false;

            arr.push(obj);
          });


          angular.forEach(arr, function(field) {
            angular.forEach(currentAttendeeCustomFields, function(answeredField) {
              if (answeredField.custom_attendee_field.id === field.id) {
                field.value = (typeof answeredField.value == "string")?answeredField.value:answeredField.value.toString();

                if(field.options && field.options.length > 0){
                  angular.forEach(field.options, function(option) {
                      if(option.value == field.value ) {
                        field.validOption = true;
                        return false;
                      }
                  });
                }
                else{
                  field.validOption = true;
                }
              }
            });
          });

          return arr;
        }

        function processCustomFields(fields) {
          var obj = {};

          for (var i = 0; i < fields.length; i++) {
            if (typeof(fields[i].value) === "object" && fields[i].value.length > 0) {
              var key = fields[i].id;

              obj[key] = {
                value: fields[i].value
              };
            } else if (typeof(fields[i].value) !== "object" && fields[i].value) {
              var key = fields[i].id;

              obj[key] = {
                value: fields[i].value
              };
            }
          };
          return obj;
        }

        function processSessions(sessions) {
          var arr = [];

          for (var i = 0; i < sessions.length; i++) {
            var obj = {};
            obj.session_id     = sessions[i].session.id;
            obj.check_register = sessions[i].hasRegistered;
            obj.check_scanned  = sessions[i].hasAttendeed;

            arr.push(obj);
          }

          return arr;
        }

        function checkAttendeeForErrors(editedAttendee) {
          var obj = {
            foundError : false,
            attendee   : []
          }

          for (var i = 0; i < editedAttendee.length; i++) {
            if (editedAttendee[i].mandatory && !editedAttendee[i].value) {
              editedAttendee[i].error = true;
              obj.foundError = true;
              toastr.error(editedAttendee[i].nameToDisplay + ' is a mandatory field!');
            } else if(editedAttendee[i].key === 'phone_number' && (editedAttendee[i].value === undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1)) {
              editedAttendee[i].error = true;
              obj.foundError = true;
              toastr.error('Please fill the '+editedAttendee[i].nameToDisplay + ' with a correct format!');
            }
            else {
              editedAttendee[i].error = false;
            }
          }

          obj.attendee = editedAttendee;
          return obj;

        }

        function processStateCountryFields(attendee) {
          for (var i = 0; i < attendee.length; i++) {
            if (attendee[i].key === 'country' && attendee[i].value != 'United States') {
              for (var j = 0; i < attendee.length; j++) {
                if (attendee[j].key === 'state') {
                  attendee[j].value = null;
                  break;
                }
              }
            }
          }
          return attendee;
        }

        function initVars() {
          if($rootScope.userPermissions.admin || $rootScope.userPermissions.manageAttendees){
            getAttendeePayments(scope.attendee.id);
            getAttendeeSessions(scope.attendee.id);
          }

          if (!scope.attendee.country || scope.attendee.country.length > 2) {
            var searchCountry = (!scope.attendee.country)?$rootScope.conference.country:scope.attendee.country;
            angular.forEach(API.COUNTRIES, function(country) {
              if(country.name === searchCountry){
                scope.defaultCountryFlag = country.code.toLowerCase();
              }
            });
          } else {
            scope.defaultCountryFlag = scope.attendee.country.toLowerCase();
          }

          scope.fieldsForView               = [
            {
              nameToDisplay : "Email Address",
              key           : "email_address"
            },
            {
              nameToDisplay : "Phone Number",
              key           : "phone_number"
            },
            {
              nameToDisplay : "Street Address",
              key           : "street_address"
            },
            {
              nameToDisplay : "City",
              key           : "city"
            },
            {
              nameToDisplay : "State",
              key           : "state"
            },
            {
              nameToDisplay : "Zip Code",
              key           : "zip_code"
            },
            {
              nameToDisplay : "Country",
              key           : "country"
            },
            {
              nameToDisplay : "Attendee ID",
              key           : "attendee_id"
            },
            {
              nameToDisplay : "Guest of",
              key           : "guest_of"
            }
          ];
          scope.fieldsForEdit               = [
            {
              nameToDisplay : "First Name",
              key           : "first_name",
              value         : scope.attendee.first_name,
              mandatory     : true,
              error         : false

            },
            {
              nameToDisplay : "Last Name",
              key           : "last_name",
              value         : scope.attendee.last_name,
              mandatory     : true,
              error         : false
            },
            {
              nameToDisplay : "Job Title",
              key           : "job_title",
              value         : scope.attendee.job_title,
            },
            {
              nameToDisplay : "Attendee Id",
              key           : "attendee_id",
              value         : scope.attendee.attendee_id,
            },
            {
              nameToDisplay : "Guest of",
              key           : "guest_of",
              value         : scope.attendee.guest_of?(scope.attendee.guest_of.first_name +" "+scope.attendee.guest_of.last_name):"",
            },
            {
              nameToDisplay : "Email",
              key           : "email_address",
              value         : scope.attendee.email_address,
              mandatory     : !scope.activeCustomAttendeeId?true:false,
              error         : false
            },
            {
              nameToDisplay : "Phone",
              key           : "phone_number",
              value         : scope.attendee.phone_number,
            },
            {
              nameToDisplay : "Company",
              key           : "company_name",
              value         : scope.attendee.company_name
            },
            {
              nameToDisplay : "Street Address",
              key           : "street_address",
              value         : scope.attendee.street_address
            },
            {
              nameToDisplay : "City",
              key           : "city",
              value         : scope.attendee.city
            },
            {
              nameToDisplay : "State",
              key           : "state",
              value         : scope.attendee.state || "",
              states        : API.UNITED_STATES,
            },
            {
              nameToDisplay : "Country",
              key           : "country",
              value         : scope.attendee.country || "",
              countries     : API.COUNTRIES,
            },
            {
              nameToDisplay : "Zip Code",
              key           : "zip_code",
              value         : scope.attendee.zip_code
            }
          ];
        }
  		}
		}
})();
