(function(){
	'use strict'

	var directiveId 		= 'sendTicketEmail';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'actionCallerService',
		sendTicketEmailDirective
	];

	app.directive(directiveId, depdenciesArray);

  function sendTicketEmailDirective($rootScope, genericDataTableInfoStore, actionCallerService) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/sendTicketEmail/sendTicketEmailView.html'
		};

    function link (scope) {
			var conferenceId = $rootScope.conference.conference_id;
			var selectedIds 							= genericDataTableInfoStore.ids;

			scope.modalData	 				= scope.ngDialogData._data;
			scope.pluralisations 		= scope.modalData.modal_params.pluralisations;
			scope.confirmButtonText = scope.modalData.modal_params.button_value;

			var numberOfSelectedAttendees = computeAttendeesNumber(selectedIds);

			scope.messageToPrompt 				= computeMessage(scope.pluralisations, numberOfSelectedAttendees);

			scope.callAction = function(actionData) {
				actionCallerService.callAction(conferenceId, actionData).then(function(response) {
					scope.closeThisDialog();
				}, function(error) {
				})
			}

			function computeMessage(pluralisations, numberOfSelectedAttendees) {
				var message = "";
				if (numberOfSelectedAttendees === 1) {
					for (var i = 0; i < pluralisations.length; i++) {
						if (pluralisations[i].name === 'single') {
							message = pluralisations[i].value;
						}
					}
				} else if (numberOfSelectedAttendees > 1) {
					for (var i = 0; i < pluralisations.length; i++) {
						if (pluralisations[i].name === 'multiple') {
							message = pluralisations[i].value.replace('__number_of_items__', numberOfSelectedAttendees);
						}
					}
				} else if (numberOfSelectedAttendees === 'all') {
					for (var i = 0; i < pluralisations.length; i++) {
						if (pluralisations[i].name === 'all') {
							message = pluralisations[i].value;
						}
					}
				}

				return message;
			};

			function computeAttendeesNumber(idsArray) {
				var number = null;

				if (idsArray === 'all') {
					number = idsArray;
				} else if (typeof(idsArray) === 'object') {
					number = idsArray.length;
				}

				return number;
			}
    }
  }
})();
