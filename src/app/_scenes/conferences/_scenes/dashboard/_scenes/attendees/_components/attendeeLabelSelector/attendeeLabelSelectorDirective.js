(function () {
    'use strict'

    var directiveId = 'attendeeLabelSelector';
    var app         = angular.module('app');
    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'conferenceRegistrationDataService',
      '$q',
      attendeeLabelSelector
    ];

    app.directive(directiveId, dependenciesArray);

    function attendeeLabelSelector($rootScope, attendeesEventsPropagator,conferenceRegistrationDataService,$q) {
      return {
          restrict    : 'E',
          replace     : true,
          templateUrl : 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/attendeeLabelSelector/attendeeLabelSelectorView.html',
          link        : link,
          scope       : {
            "opened"        : "=opened",
            "customFields"  : "="
          }
      }

      function link(scope) {
        var selectedFieldsNumber = 3;
        var availableEvents      = attendeesEventsPropagator.availableEvents;

        scope.allPossibleFields = [
          {
            name     : 'Attendee ID',
            code     : 'attendee_id',
            selected : false
          },
          {
            name     : 'Email',
            code     : 'email_address',
            selected : true
          },
          {
            name     : 'Phone',
            code     : 'phone_number',
            selected : true
          },
          {
            name     : 'Company',
            code     : 'company_name',
            selected : true
          },
          {
            name     : 'Street Address',
            code     : 'street_address',
            selected : false
          },
          {
            name     : 'City',
            code     : 'city',
            selected : false
          },
          {
            name     : 'State',
            code     : 'state',
            selected : false
          },
          {
            name     : 'Country',
            code     : 'country',
            selected : false
          },
          {
            name     : 'Zip Code',
            code     : 'zip_code',
            selected : false
          },
          {
            name     : 'Job Title',
            code     : 'job_title',
            selected : false
          },
          {
            name     : 'First Name',
            code     : 'first_name',
            selected : false
          },
          {
            name     : 'Last Name',
            code     : 'last_name',
            selected : false
          },
          {
            name     : 'Badge Title',
            code     : 'badge_title',
            selected : false,
            show     : ($rootScope.conference.features.badge) ? true : false
          }
        ];

        angular.forEach(scope.customFields, function(field) {
          var obj = {
            id       : field.id,
            name     : field.name,
            code     : field.name,
            selected : false
          };
          scope.allPossibleFields.push(obj);
        });

        getConferenceRegistrationForm();

        scope.$watch('opened', function(newVal, oldVal) {
          scope.opened = newVal;
        });

        scope.selectField = function(field) {
          var arrayHasChanged = false;
          if (selectedFieldsNumber < 5 && field.selected === false) {
            field.selected = !field.selected;
            selectedFieldsNumber++;
            arrayHasChanged = true;
          } else if (field.selected) {
            arrayHasChanged = true;
            field.selected = !field.selected;
            selectedFieldsNumber--;
          }

          updateArrayInAttendees(arrayHasChanged);
        }

        function updateArrayInAttendees(arrayHasChanged) {
          if (arrayHasChanged) {
            var arr = [];
            angular.forEach(scope.allPossibleFields, function(field) {
              if (field.selected) arr.push(field);
            });
            attendeesEventsPropagator.propagateEvent(availableEvents.UPDATE_SELECTED_VIEW_FIELDS, arr);
          }
        }

        function getConferenceRegistrationForm() {
          conferenceRegistrationDataService.getRegistrationForm($rootScope.conference.conference_id).then(function(response) {
            if(response) {
              var obj = {
                name      : 'Registration Level',
                code      : 'attendee_registration_level_name',
                selected  : false
              };
              scope.allPossibleFields.push(obj);
            }
          }, function(error) {
            console.log(error);
          });

    		}
      }
    }
})();
