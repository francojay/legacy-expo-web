(function () {
    'use strict'

    var directiveId = 'averyRedirectModal';
		var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'ngDialog',
      '$timeout',
			'$window',
      averyRedirectModal
    ];

		app.directive(directiveId, dependenciesArray);

		function averyRedirectModal($rootScope, attendeesEventsPropagator, ngDialog, $timeout, $window) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/averyRedirectModal/averyRedirectModalView.html',
        link: link,
      }

      function link(scope) {
        var averyProductCode          = scope.ngDialogData.averyProductCode;
        var attendeesFileLinkForAvery = scope.ngDialogData.attendeesFileLinkForAvery;
        var requiredServiceFromAvery  = scope.ngDialogData.requiredServiceFromAvery;
        var averyLabelTemplates       = scope.ngDialogData.averyLabelTemplates;
        var averyBadgeTemplates       = scope.ngDialogData.averyBadgeTemplates;

        var initData                  = init(requiredServiceFromAvery, averyLabelTemplates, averyBadgeTemplates)

        scope.modalData               = {
          "displayTemplateSelector" : initData.displayTemplateSelector,
          "templatesToRender"       : initData.templatesToRender,
          "selectedTemplate"        : {}
        }

				scope.goToAvery = function(templateFile) {
          var averyUrl = "http://services.print.avery.com/dpp/public/v1/open/US_en/"
          if (templateFile && templateFile.file_url) {
            averyUrl += "?averyFileName=" + templateFile.file_url + "&mergeDataURL=" + attendeesFileLinkForAvery + "&provider=expopass";
          } else {
            averyUrl += "?sku=" + averyProductCode +"&mergeDataURL=" + attendeesFileLinkForAvery + "&provider=expopass";
          }

					$window.open(averyUrl, "_blank");
				}

        function init(service, labelTemplates, badgeTemplates) {
          var obj = {
            displayTemplateSelector : false,
            templatesToRender       : []
          };

          if (service === 'label' && labelTemplates.length > 0) {
            obj.templatesToRender = angular.copy(labelTemplates);
          } else if (service === 'badge' && badgeTemplates.length > 0) {
            obj.templatesToRender = angular.copy(badgeTemplates);
          }

          obj.displayTemplateSelector = obj.templatesToRender.length > 0 ? true : false;

          return obj;
        }
			}
		}
})();
