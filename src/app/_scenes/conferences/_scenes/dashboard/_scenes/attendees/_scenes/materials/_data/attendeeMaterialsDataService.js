(function() {
	'use strict';
  var serviceId = 'attendeeMaterialsDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'$http',
		'$filter',
		'toastr',
		'localStorageService',
		'requestFactory',
		attendeeMaterialsDataService
	];
	app.service(serviceId, dependenciesArray);
	function attendeeMaterialsDataService($q, $http, $filter, toastr, localStorageService, requestFactory) {
		var service = {};

		service.getAll = function(conferenceId) {
			var deferred = $q.defer();
			var url = '/api/v1.1/conferences/' + conferenceId + '/attendee_materials/';

			requestFactory.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.assignAttendeeMaterial = function(material) {
			var deferred = $q.defer();
			var url = '/api/v1.1/conferences/' + material.conference_id + '/attendee_materials/';

			requestFactory.post(url, material, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.removeMaterial = function(conferenceId, materialId) {
			var deferred = $q.defer();
			var url = '/api/v1.1/conferences/' + conferenceId + '/attendee_materials/' + materialId + '/';

			requestFactory.remove(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
