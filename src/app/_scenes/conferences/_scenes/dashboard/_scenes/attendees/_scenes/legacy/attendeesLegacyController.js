(function () {
    'use strict';

    var controllerId = 'attendeesLegacyController';
    var app          = angular.module('app');

    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      '$localStorage',
      'SharedProperties',
      'conferenceDetailsService',
      'attendeesDataService',
      'toastr',
      'attendeesEventsPropagator',
      '$timeout',
      'ngDialog',
      'fileService',
      '$q',
      '$filter',
      '$state',
      attendeesLegacyController
    ];

    app.controller(controllerId, dependenciesArray);

    function attendeesLegacyController($scope, $rootScope, $stateParams, $localStorage, SharedProperties, conferenceDetailsService, attendeesDataService, toastr, attendeesEventsPropagator, $timeout, ngDialog, fileService, $q, $filter, $state) {
      var conference 								= $rootScope.conference;
  		var conferenceId 							= conference.conference_id;

      var vm                        = this;
      var availableEvents           = attendeesEventsPropagator.availableEvents;
      var requestInProgress         = false;
      var templateTypeForAvery      = "";
      var averyBadgeTemplates       = [];
      var averyLabelTemplates       = [];
      $scope.attendees                   = [];
      $scope.selectedAttendees           = [];
      $scope.attendeesTotalCount         = 0;
      $scope.currentPageOfAttendees      = 1;
      $scope.attendeesSearchQuery        = "";
      $scope.attendeesViewOptionsVisible = false;
      $scope.showMoreOptions             = false;
      $scope.allAttendeesSelected        = false;
      $scope.queryObject              = {
          queryString: null,
          sortBy: 'first_name',
          sortOrder: 'asc'
      };
      $scope.selectedFields           = [
          {
              name: 'Email',
              code: 'email_address',
          },
          {
              name: 'Phone',
              code: 'phone_number',
          },
          {
              name: 'Company',
              code: 'company_name'
          },
      ];
      $scope.sortType = 'first_name';
      $scope.sortReverse = false;
      $scope.isProcessing = false;
      $scope.isFiltering = false;
      $scope.query = false;

      var currentConference = {};
      var currentConferenceId = $rootScope.conference.conference_id;
      var page = 1;
      var loader = false;
      $rootScope.totalFilteredCount = -1;

      $timeout(function() {
        $('.attendees-list-wrapper').scroll(function(e) {

          if (requestInProgress) {
            return
          } else {
            var element = document.getElementsByClassName('attendees-list-wrapper')[0];

            if (element.scrollTop + element.offsetHeight > (element.scrollHeight - 200)) {
              requestInProgress = true; //sets a processing AJAX request flag
              if(!$scope.isFiltering) {
                getMoreAttendees(element, $scope.currentPageOfAttendees);
              } else {
                getMoreFilteredAttendees(element, $scope.query, $scope.currentPageOfAttendees);
              }
            }
          }
        });
      });

      getAttendees(currentConferenceId);

      $scope.orderByField = function(field) {
        if($scope.sortType == field){
          $scope.sortReverse = !$scope.sortReverse;
        } else {
          $scope.sortType = field;
          $scope.sortReverse = false;
        }
      }

      $scope.searchAttendees = function (query) {
        resetAttendees();
        var query = {
          queryString   : query || '',
          sortBy        : 'first_name',
          sortOrder     : "asc"
        };

        if(query.queryString === '')
          $scope.isFiltering = false;
        $scope.query = query;
        getFilteredAttendees(query);
      }

      $scope.toggleSelectAll = function() {
        $scope.selectedAttendees = [];
        $scope.allAttendeesSelected = !$scope.allAttendeesSelected;
        angular.forEach($scope.attendees, function(attendee){
          var data = {
            attendee       : attendee,
            addedInTheList : $scope.allAttendeesSelected
          }
          $rootScope.$broadcast(availableEvents.UPDATE_SELECTED_ATTENDEES_LIST, data);
        })
        var data = {
          state: $scope.allAttendeesSelected
        }
        $rootScope.$broadcast(availableEvents.UPDATE_SELECTED_ALL_ATTENDEES, data);
      }

      $scope.deleteAttendees = function(selectedAttendees) {
        var data = {};
        if ($scope.allAttendeesSelected == true && !$scope.isFiltering) {
            data.numberOfAttendees = $scope.selectedAttendees.length;
        }
        else{
          data.numberOfAttendees = $scope.selectedAttendees.length;
        }
        ngDialog.open({
              plain: true,
              template: '<delete-conference-attendees-modal></delete-conference-attendees-modal>',
              className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/deleteConferenceAttendeesModal/deleteConferenceAttendeesView.scss',
              data: data
        });
      }

      $scope.toggleLabelSelector = function(state) {
        $scope.attendeesViewOptionsVisible = !state;
      }

      $scope.toggleMoreOptionsSelector = function(state) {
        $scope.showMoreOptions = !state;
      }

      $scope.createAveryService = function($event, typeofService) {
        // typeofService : "badge, label"
        $event.stopPropagation();
        var allTemplates = $rootScope.conference.templates;
        averyLabelTemplates = [];
        averyBadgeTemplates = [];
        angular.forEach(allTemplates, function(template) {
          if (template.type == 2) {
            averyLabelTemplates.push(template);
          } else if (template.type == 1) {
            averyBadgeTemplates.push(template);
          }
        });

        templateTypeForAvery = typeofService;
        uploadAttendeesAsCSV($scope.allAttendeesSelected, $scope.attendeesSearchQuery);
        return;
      }

      $scope.downloadAsCSV = function($event) {
        $event.stopPropagation();
        var attendee_ids = [];
        if (!$scope.allAttendeesSelected) {
          _.each($scope.selectedAttendees, function(attendee) {
            attendee_ids.push(attendee.id);
          });
        }

        var promises = [];
        if (attendee_ids.length == 0) {
          var nr_pages = parseInt($scope.attendeesTotalCount / 30) + 2;
          for (var idx = 1; idx <= nr_pages + 1; idx ++) {
            promises.push(attendeesDataService.downloadAttendees(currentConferenceId, attendee_ids, null, idx));
          }
        } else {
          promises.push(attendeesDataService.downloadAttendees(currentConferenceId, attendee_ids));
        }


        $q.all(promises).then(function(responses) {
          var allAttendees = [];
          _.each(responses, function(attendees){
            allAttendees = allAttendees.concat(attendees);
          });

          var checkedAttendeeList = [];
          var customFields = [];
          var sessionTitles = [];

          _.each(allAttendees, function(attendee) {
            var mainProfileAttendee = _.omit(attendee,
            ['custom_fields' , 'sessionattendee_set', 'scannedattendee_set', 'customattendeefieldvalue_set',
            'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);

            _.forEach(attendee.customattendeefieldvalue_set, function(customField) {
              mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
              customFields.push(customField.custom_attendee_field.name);
            });

            _.forEach(attendee.scannedattendee_set, function(scannedAttendee){
              var itemDate = new Date(scannedAttendee.session.starts_at);
              var scannedAt = new Date(scannedAttendee.scanned_at);
              var actualDate = $filter('date')(itemDate, "MM/dd/yyyy - hh:mm a", $rootScope.conference.time_zone);
              var scannedDate = $filter('date')(scannedAt, "MM/dd/yyyy hh:mm a", $rootScope.conference.time_zone);

              var session_txt = scannedDate + ' - ' + scannedAttendee.added_by.first_name + ' ' + scannedAttendee.added_by.last_name;

              mainProfileAttendee[scannedAttendee.session.title + ': ' + actualDate] = session_txt;
              sessionTitles.push(scannedAttendee.session.title + ': ' + actualDate);

              mainProfileAttendee[scannedAttendee.session.title + ': ' + 'CE'] = String(scannedAttendee.session.ce_hours);
              sessionTitles.push(scannedAttendee.session.title + ': ' + 'CE');

              mainProfileAttendee[scannedAttendee.session.title + ': ' + 'CE Type'] = (scannedAttendee.session.ce_type != null)?String(scannedAttendee.session.ce_type):'';
              sessionTitles.push(scannedAttendee.session.title + ': ' + 'CE Type');
            });

            delete mainProfileAttendee["id"];
            mainProfileAttendee['{BCT#QRCode}'] = getUserQrCode(attendee);
            if(attendee.badge_print_date == null || attendee.badge_print_date === undefined)
              mainProfileAttendee['badge_print_date'] = ' ';
            else{
                var badgeDate = new Date(attendee.badge_print_date);
                mainProfileAttendee['badge_print_date'] = $filter('date')(badgeDate, "MM/dd/yyyy  hh:mm a", $rootScope.conference.time_zone);
            }
            if(attendee.attendee_registration_date == null || attendee.attendee_registration_date === undefined)
              mainProfileAttendee['registration_date'] = ' ';
            else{
                var registrationDate = new Date(attendee.attendee_registration_date);
                mainProfileAttendee['registration_date'] = $filter('date')(registrationDate, "MM/dd/yyyy  hh:mm a", $rootScope.conference.time_zone);
            }
            mainProfileAttendee['registration_level_name'] = attendee.attendee_registration_level_name;
            mainProfileAttendee['registration_promo_code'] = attendee.attendee_registration_promo_code;

            checkedAttendeeList.push(mainProfileAttendee);
          });
          var fields = ['company_name', 'first_name', 'last_name', 'job_title', 'email_address', 'phone_number',
          'street_address', 'city', 'state', 'zip_code', 'country', 'attendee_id', '{BCT#QRCode}','badge_print_date','registration_date',
          'registration_level_name','registration_promo_code'];
          customFields = _.uniq(customFields);
          sessionTitles = _.uniq(sessionTitles);
          fields = fields.concat(customFields);
          fields = fields.concat(sessionTitles);
          printCsvListFile(checkedAttendeeList, fields);
        }, function(error) {
          toastr.error("There has been a problem with your request!");
        })
      }

      $scope.openCreateAttendeeModal = function($event) {
        $event.stopPropagation();
        ngDialog.open({
            plain     : true,
            template  : '<create-attendee-modal></create-attendee-modal>',
            className : 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/createAttendeeModal/createAttendeeModal.scss',
        });
      }

      function printCsvListFile(checkedAttendeeList, fields) {
        var csvData =  lib.ConvertToCSVImproved(checkedAttendeeList, fields);

        var date = new Date();

        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
        var filename = $rootScope.conference.name + actualDate + ".csv";
        var blob = new Blob([csvData], {type: 'application/csv;charset=utf-8'});
        saveAs(blob, filename);
      }

      function uploadAttendeesAsCSV(areAllAttendeesSelected, queryString) {
        if (areAllAttendeesSelected) {
          var nr_pages = parseInt($scope.attendeesTotalCount / 30 + 1);
          var promises = [];
          for (var idx = 1; idx <= nr_pages + 1; idx ++) {
            if (queryString === "") {
              promises.push(attendeesDataService.getAll(currentConferenceId, idx));
            } else {
              console.log('s')
              var obj = {
                "queryString" : queryString,
      	        "sortBy"      : null,
      	        "sortOrder"   : null
              }
              promises.push(attendeesDataService.getFilteredAttendees(currentConferenceId, idx, false, obj));
            }
          }

          $q.all(promises).then(function(responses) {
            var allAttendees = [];
            _.each(responses, function(response){
                allAttendees = allAttendees.concat(response.attendees);
            });
            var processedAttendees = processAttendeesForCSVUpload(allAttendees);
            uploadAttendeesFileAndGoToAvery(processedAttendees);
          }, function(error) {
            toastr.error("An error has occured while trying to fetch all attendees!")
          })
        } else {
          var processedAttendees = processAttendeesForCSVUpload($scope.selectedAttendees);
          uploadAttendeesFileAndGoToAvery(processedAttendees);
        }
      }

      function uploadAttendeesFileAndGoToAvery(attendees) {
        var date = new Date();
        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
        var filename = $rootScope.conference.name + actualDate + ".csv";

        var csvData =  lib.ConvertToCSV2(attendees);
        var file = new File([csvData], filename, {type: "text/csv", lastModified: date});

        fileService.getUploadSignature(file, false)
            .then(function(response) {
                var file_link = response.data.file_link;
                fileService.uploadFile(file, response)
                    .then(function(uploadResponse) {
                        var sku = 5392;

                        if (templateTypeForAvery == 'label') {
                            sku = 22805;
                        }

                        var attendeesFileLinkForAvery = file_link;
                        var averyProductCode          = sku;

                        ngDialog.open({
                          plain     : true,
                          template  : "<avery-redirect-modal></avery-redirect-modal>",
                          className : 'app/stylesheets/_createLabelModal.scss',
                          data      : {
                            "averyProductCode"          : averyProductCode,
                            "attendeesFileLinkForAvery" : attendeesFileLinkForAvery,
                            "requiredServiceFromAvery"  : templateTypeForAvery,
                            "averyLabelTemplates"       : averyLabelTemplates,
                            "averyBadgeTemplates"       : averyBadgeTemplates
                          }
                        });
                    })
                    .catch(function(error){

                    });
            })
            .catch(function(error){

            });
      }

      function processAttendeesForCSVUpload(attendees) {
        var arr = [];
        angular.forEach(attendees, function(attendee) {
          var mainProfileAttendee = _.omit(attendee,
          ['custom_fields' , 'attendee_photo', 'id', 'attendee_id', 'guest_of',
           'attendee_registration_level_name','created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
          _.forEach(attendee.custom_fields, function(customField){
              if (customField.custom_attendee_field.type == 3) {
                  try {
                      mainProfileAttendee[customField.custom_attendee_field.name] = customField.value.join();
                  }
                  catch(err) {
                      mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                  }
              } else {
                  mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
              }
          });
          mainProfileAttendee['Conference Name'] = $rootScope.conference.name;
          mainProfileAttendee['{BCT#QRCode}'] = getUserQrCode(attendee);
          arr.push(mainProfileAttendee);
        });

        return arr;
      }

      function getUserQrCode(attendee) {
        return attendee.attendee_id + ';' + attendee.first_name + ';' + attendee.last_name;
      }

      function getAttendees(id) {
        var sortingObject = {
          sortBy: $scope.sortType,
          sortOrder : $scope.sortReverse ? 'desc' : 'asc',
          queryString : null
        };
        attendeesDataService.getAll(id, $scope.currentPageOfAttendees, true, sortingObject).then(function (response) {
            $scope.attendees           = response.attendees;
            $scope.attendeesTotalCount = response.total_count;
            $scope.currentPageOfAttendees++;
            var x = $rootScope.$on('LAST_ATTENDEE_TABLE_ITEM_DIRECTIVE_HAS_RENDERED', function(event, data) {
              var element = document.getElementsByClassName('attendees-list-wrapper')[0];
              element.scrollTop = 0;
              toastr.success("Attendees retrieved successfully!");
              x();
            });
        }, function (error) {
            toastr.error('There was a problem with retrieving the attendees. Please check again later.');
        });
      }

      function updateSelectedAttendeesList(currentList, attendee, selectionState) {
        var updatedList = angular.copy(currentList);
        if (selectionState === true) {
          updatedList.push(attendee);
        } else {
          updatedList = angular.forEach(currentList, function(attendeeInList, index) {
            if (attendeeInList.id === attendee.id) {
              currentList.splice(index, 1);
            }
          })
        }

        return updatedList;
      }

      function getMoreAttendees(element, page) {
        var sortingObject = {
          sortBy: $scope.sortType,
          sortOrder : $scope.sortReverse ? 'desc' : 'asc'
        };
        $scope.isProcessing = true;
        if ($scope.attendeesTotalCount > 0 ) {

          attendeesDataService.getAll(currentConferenceId, page, true, sortingObject).then(function (response) {
            for (var i = 0; i < response.attendees.length; i++) {
              $scope.attendees.push(response.attendees[i]);
            }
            $scope.attendeesTotalCount = (response.attendees.length > 0) ? $scope.attendeesTotalCount - response.attendees.length:0;
            $scope.currentPageOfAttendees++;
            $scope.isProcessing = false;
            if (response.attendees.length === 0) {
              toastr.success("No more attendees to retrieve!")
            }
            $rootScope.$on('LAST_ATTENDEE_TABLE_ITEM_DIRECTIVE_HAS_RENDERED', function(event, data) {
              $timeout(function() {
                requestInProgress = false;
                var data = {
                  state: $scope.allAttendeesSelected
                }
                $rootScope.$broadcast(availableEvents.UPDATE_SELECTED_ALL_ATTENDEES, data);
              }, 500);
            });

          }, function (error) {
              toastr.error('There was a problem with retrieving the attendees. Please check again later.');
              requestInProgress = false;
          });
        } else {
          toastr.success("No more attendees to retrieve!")
        }
      }

      function deleteAllAttendees() {
        attendeesDataService
          .deleteAllAttendees($rootScope.conference.conference_id)
          .then(function(result){
            resetAttendees();
            $rootScope.conference.rule_type = 'none';
            $scope.allAttendeesSelected = false;
            $rootScope.conference.attendees.total_count = 0;
            $rootScope.totalFilteredCount = 0;

          }, function(error){
            toastr.error("An error has occured. Please try again later.")
          })
      }

      function deleteSelectedAttendees(selectedAttendees) {
        var selectedAttendeesIds = selectedAttendees.map(function(selectedAttendee){
          return selectedAttendee.id;
        })
        attendeesDataService
          .deleteSelectedAttendees($rootScope.conference.conference_id, selectedAttendeesIds)
          .then(function(result) {
            var updatedAttendees = angular.copy($scope.attendees);
            if($scope.attendees.length != $scope.selectedAttendees.length){
              angular.forEach($scope.selectedAttendees, function(selectedAttendee){
                angular.forEach(updatedAttendees, function(attendee, index){
                  if(attendee.id == selectedAttendee.id){
                    updatedAttendees.splice(index,1);
                  }
                })
              })
            }
            else{
              updatedAttendees = [];
              $rootScope.conference.rule_type = 'none';
            }
            var element = document.getElementsByClassName('attendees-list-wrapper')[0];
            if(updatedAttendees.length < 20){
              if(!$scope.isFiltering){
                getMoreAttendees(element, $scope.currentPageOfAttendees);
              }
              else{
                getMoreFilteredAttendees(element, $scope.query, $scope.currentPageOfAttendees);
              }
            }

            $scope.attendees = updatedAttendees;

            $scope.selectedAttendees = [];
            $rootScope.totalFilteredCount = $rootScope.totalFilteredCount - selectedAttendees.length;
            $rootScope.conference.attendees.total_count = $rootScope.conference.attendees.total_count- selectedAttendees.length;
            toastr.success("The selected attendees have been deleted successfully!");

        }, function(error) {
          toastr.error("An error has occured. Please try again later.")
        })
      }

      function resetAttendees() {
        $scope.attendees = [];
        $scope.selectedAttendees = [];
        $scope.attendeesTotalCount = 0;
        $scope.currentPageOfAttendees = 1;

      }

      function uncheckAll() {
        angular.forEach($scope.attendees, function(attendee){
          attendee.checked = false;
        });

        if($scope.attendeesTotalCount != $rootScope.conference.attendees.total_count)
          $scope.isFiltering = true;
        else {
            $scope.isFiltering = false;
        }
      }

      function getFilteredAttendees(filters) {
          if(!$scope.isProcessing){
            $scope.isProcessing = true;
            if(!page){
              var page = 1;
            }
            var sortingObject = {
              sortBy: $scope.sortType,
              sortOrder : $scope.sortReverse ? 'desc' : 'asc',
              queryString : encodeURIComponent($scope.attendeesSearchQuery)
            };

            attendeesDataService.getFilteredAttendees(currentConferenceId, page, false, sortingObject).then(function(data) {
              $scope.attendees = $scope.attendees.concat(data.attendees);
              $scope.attendeesTotalCount = data.total_count;
              $rootScope.totalFilteredCount = data.total_count;
              $scope.currentPageOfAttendees++;
              $scope.isProcessing = false;
              requestInProgress = false;
              $scope.allAttendeesSelected = false;
              var x = $rootScope.$on('LAST_ATTENDEE_TABLE_ITEM_DIRECTIVE_HAS_RENDERED', function(event, data) {
                var element = document.getElementsByClassName('attendees-list-wrapper')[0];
                element.scrollTop = 0;
                toastr.success("Attendees retrieved successfully!");
                x();
              });
              var data = {
                state: $scope.allAttendeesSelected
              }
              $rootScope.$broadcast(availableEvents.UPDATE_SELECTED_ALL_ATTENDEES, data);
              //$rootScope.conference.attendees.total_count = data.total_count;
              uncheckAll();
            }, function(error) {
              $scope.isProcessing = false;
              toastr.error('There was a problem with your request.', 'Try again.');
            });
          }
      }

      function getMoreFilteredAttendees(element, filters, page) {
        if ($scope.attendeesTotalCount > 0 ) {
          var sortingObject = {
            sortBy: $scope.sortType,
            sortOrder : $scope.sortReverse ? 'desc' : 'asc',
            queryString : $scope.attendeesSearchQuery
          };

          if(!$scope.isProcessing) {
            $scope.isProcessing = true;
            if(!page){
              var page = 1;
            }
            attendeesDataService
             .getFilteredAttendees(currentConferenceId, page, false, sortingObject)
                .then(function(data) {
                  for (var i = 0; i < data.attendees.length; i++) {
                    $scope.attendees.push(data.attendees[i]);
                  }
                  if(data.attendees.length > 0) {
                    $scope.attendeesTotalCount = $scope.attendeesTotalCount - data.attendees.length;
                  }
                  else {
                    $scope.attendeesTotalCount = 0;
                  }
                  $rootScope.totalFilteredCount = data.total_count;
                  $scope.currentPageOfAttendees++;
                  $scope.isProcessing = false;
                  requestInProgress = false;
                  $scope.allAttendeesSelected = false;
                  var dataToSend = {
                    state: $scope.allAttendeesSelected
                  }
                  if (data.attendees.length === 0) {
                    toastr.success("No more attendees to retrieve!");
                  }
                  $rootScope.$on('LAST_ATTENDEE_TABLE_ITEM_DIRECTIVE_HAS_RENDERED', function(event, data) {
                    $timeout(function() {
                      requestInProgress = false;
                      //element.scrollTop = elementScrollBeforeApiCall;
                    }, 500);
                  });
                  $rootScope.$broadcast(availableEvents.UPDATE_SELECTED_ALL_ATTENDEES, dataToSend);
              }, function(error) {
                $scope.isProcessing = false;
                toastr.error('There was a problem with your request.', 'Try again.');
              });
          }
        } else {
          toastr.success("No more attendees to retrieve!");
        }
      }

      $scope.$watch('sortReverse', function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.currentPageOfAttendees = 1;
          $scope.attendees = [];
          toastr.warning("Rebuilding list, please wait...");
          if (!$scope.attendeesSearchQuery) {
            getAttendees(currentConferenceId);
          } else {
            $scope.searchAttendees($scope.attendeesSearchQuery);
          }

        }
      });

      $scope.$watch('sortType', function(newValue, oldValue) {
        if (newValue != oldValue) {
          $scope.currentPageOfAttendees = 1;
          $scope.attendees = [];
          toastr.warning("Rebuilding list, please wait...");
          if (!$scope.attendeesSearchQuery) {
            getAttendees(currentConferenceId);
          } else {
            $scope.searchAttendees($scope.attendeesSearchQuery);
          }
        }
      });

      $rootScope.$on(availableEvents.UPDATE_SELECTED_ATTENDEES_LIST, function(event, data) {
        if(data.addedInTheList == false && $scope.allAttendeesSelected == true) {
          $scope.allAttendeesSelected = false;
        }
        $scope.selectedAttendees = updateSelectedAttendeesList($scope.selectedAttendees, data.attendee, data.addedInTheList);

        if ($scope.selectedAttendees.length === $scope.attendeesTotalCount) {
          $scope.allAttendeesSelected = true;
        }
      });

      $rootScope.$on(availableEvents.UPDATE_SELECTED_VIEW_FIELDS, function(event, data) {
        $scope.selectedFields = angular.copy(data);
      });

      $rootScope.$on(availableEvents.UPDATE_ADDED_ATTENDEES_LIST, function(event, data) {
        $scope.attendees.push(data.addedAttendee);
        $scope.attendeesTotalCount++;
        $rootScope.conference.attendees.total_count = $rootScope.conference.attendees.total_count + 1;

      });

      $rootScope.$on(availableEvents.UPDATE_EDIT_ATTENDEES_LIST, function(event, data) {
        var keepGoing = true;
        angular.forEach($scope.attendees, function(attendee, index){
          if(keepGoing){
            if(attendee.id == data.id){
              $scope.attendees[index] = data;
              keepGoing = false;
            }
            }
        })

      });

      $rootScope.$on(availableEvents.DELETE_ALL_CONFERENCE_ATTENDEES, function(){
          deleteAllAttendees();
      });

      var myListener = $rootScope.$on(availableEvents.DELETE_CONFERENCE_ATTENDEES, function(event, data) {
        deleteSelectedAttendees($scope.selectedAttendees);
      });

      $scope.$on('$destroy', myListener); // delete event listener on page leave

  }
})();
