(function () {
    'use strict'

    var directiveId = 'createAttendeeModal';
		var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      'attendeesEventsPropagator',
      'attendeesDataService',
      '$timeout',
      '$q',
      'toastr',
      'API',
      'fileService',
      'intlTelInputOptions',
      createAttendeeModalDirective
    ];

		app.directive(directiveId, dependenciesArray);

		function createAttendeeModalDirective($rootScope, attendeesEventsPropagator, attendeesDataService, $timeout, $q, toastr, API, fileService, intlTelInputOptions) {
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/createAttendeeModal/createAttendeeModalView.html',
        link: link
      }

      function link(scope) {
        $timeout(function() {
          $('.custom-modal-wrapper.add-exhibitor.add-attendee .add-form-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
        }, 0);

        scope.defaultCountryFlag = 'us';
        angular.forEach(API.COUNTRIES, function(country) {
          if(country.name === $rootScope.conference.country){
            scope.defaultCountryFlag = country.code.toLowerCase();
          }
        });


        scope.attendeeCustomFields        = angular.copy($rootScope.conference.custom_fields);
        scope.customFields                = createCustomFieldsForView(scope.attendeeCustomFields);


        scope.attendeeFields              = [
          {
            nameToDisplay : "First Name",
            key           : "first_name",
            value         : "",
            defaultValue  : "",
            mandatory     : true,
            visible       : true,
          },
          {
            nameToDisplay : "Last Name",
            key           : "last_name",
            value         : "",
            defaultValue  : "",
            visible       : true,
            mandatory      : true
          },
          {
            nameToDisplay : "Job Title",
            key           : "job_title",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "Company Name",
            key           : "company_name",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "Email Address",
            key           : "email_address",
            value         : "",
            defaultValue  : "",
            visible       : true,
            mandatory     : $rootScope.conference.rule_type === 'custom_id'  ? false : true
          },
          {
            nameToDisplay : "Phone Number",
            key           : "phone_number",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "Street Address",
            key           : "street_address",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "City",
            key           : "city",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "Zip Code",
            key           : "zip_code",
            value         : "",
            defaultValue  : "",
            visible       : true,
          },
          {
            nameToDisplay : "State",
            key           : "state",
            value         : "",
            defaultValue  : "",
            states        : API.UNITED_STATES,
            visible       : true,
          },
          {
            nameToDisplay : "Country",
            key           : "country",
            value         : "",
            defaultValue  : "",
            countries     : API.COUNTRIES,
            visible       : true,
          },
          {
            nameToDisplay : "Attendee ID",
            key           : "attendee_id",
            value         : "",
            defaultValue  : "",
            visible       : $rootScope.conference.rule_type != 'no_custom_id' ? true : false,
            mandatory     : $rootScope.conference.rule_type === 'custom_id' ? true : false
          }
        ];
        scope.activeCustomAttendeeId     =  false;

        scope.newAttendeeInfo = [];

        scope.createAttendee              = function(attendeeFields, customFields, closeDialog) {
          if(validateNewAttendeeFields(attendeeFields)){
              var processedAttendeeFields = processAttendeeFields(attendeeFields);
              var processedCustomFields   = processCustomFields(customFields);
              var attendee                = processedAttendeeFields;
              attendee.attendee_photo = scope.newAttendeeInfo.attendee_photo;

              attendee.custom_fields      = processedCustomFields;

              attendeesDataService.createOne($rootScope.conference.conference_id, attendee).then(function(response) {
                $rootScope.$broadcast('UPDATE_ADDED_ATTENDEES_LIST', {addedAttendee: response});
                var message = "The Attendee: " + attendee.first_name + " " + attendee.last_name + " was created successfully!";
                if (!attendee.attendee_id && $rootScope.conference.rule_type === 'none') {
                  $rootScope.conference.rule_type = 'no_custom_id';
                } else if (attendee.attendee_id && $rootScope.conference.rule_type === 'none') {
                  $rootScope.conference.rule_type = 'custom_id';
                }
                toastr.success(message);
                if (closeDialog) {
                  scope.closeThisDialog();
                } else {
                  scope.attendeeFields = resetAttendeeFieldsToDefault(scope.attendeeFields);
                  scope.customFields   = createCustomFieldsForView(scope.attendeeCustomFields);
                  $('.mCSB_container')[0].style.top = 0; // library's scrollTo parameter doesn't work(only scrolls to top by 1px);
                }
              }, function(error) {
                  if(error.data.detail){
                    toastr.error(error.data.detail);
                    return;
                  }
                toastr.error("An error has occured while trying to create the attendee");
              });
            }
        }
        scope.uploadAttendeePhoto = function(files) {
          lib.squareifyImage($q, files)
              .then(function (response) {
                  var imgGile = response.file;

                  fileService.getUploadSignature(imgGile)
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];

                      promises.push(fileService.uploadFile(imgGile, response));

                      $q.all(promises).then(function(response){
                        scope.newAttendeeInfo.attendee_photo = file_link;

                      }).catch(function(error){

                      });

                  })
                      .catch(function (error) {

                          if (error.status == 403) {
                              toastr.error('Permission Denied!');
                          } else {
                              toastr.error('Error!', 'There has been a problem with your request.');
                          }
                      });
              });
				}
        scope.updateCountryForPhoneInput = function(field) {
          if (field.key === "country") {
            var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
            number = number.replace("+"+countryData.dialCode,"");
            number = number.replace(/[^0-9\\.]+/g,"");
            $rootScope.phoneIntlTelInputCtrl.setCountry(field.value.toLowerCase());
            countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
            if(number){
              $timeout(function() {
                  scope.attendeeFields[5].value = "+"+countryData.dialCode+number;
              }, 100);
            }
          }
        }

        function validateNewAttendeeFields(attendeeFields){
          var ok = true;
          _.forEach(attendeeFields,function(attendeeField){
            if(attendeeField.mandatory){
              if(attendeeField.value == ''){
                toastr.warning(attendeeField.nameToDisplay + " is required!");
                ok = false;
              }
              if(attendeeField.key == 'email_address' && attendeeField.value != ''){
                if(attendeeField.value.toLowerCase().search(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) < 0){
                  toastr.warning(attendeeField.nameToDisplay + ' is invalid!');
                  ok = false;
                }
              }
            }
            if(attendeeField.key == 'phone_number' && attendeeField.value == undefined ){
              toastr.warning(attendeeField.nameToDisplay + " need to have the correct format!");
              ok = false;
            }
            if(attendeeField.key == 'attendee_id' && scope.activeCustomAttendeeId && attendeeField.value == ''){
              toastr.warning(attendeeField.nameToDisplay + " is required!");
              ok = false;
            }
          })
          return ok;
        }

        function createCustomFieldsForView(attendeeCustomFields) {
          var arr = [];
          angular.forEach(attendeeCustomFields, function(field) {
            var obj = {};

            obj.options = field.options;
            obj.type    = field.type;
            obj.name    = field.name;
            obj.id      = field.id;
            obj.value   = field.type === 3 ? [] : "";

            arr.push(obj);
          });
          return arr;
        }

        function processAttendeeFields(fields) {
          var obj = {};

          angular.forEach(fields, function(field) {
            if (field.value) {
              if (field.key === 'state' && fields[10].value === 'US') {
                obj[field.key] = field.value;
              } else if (field.key === 'state' && fields[10].value !== 'US') {
                obj[field.key] = "";
              } else {
                obj[field.key] = field.value;
              }
            }
          });

          return obj;
        }

        function processCustomFields(fields) {
          var obj = {};

          for (var i = 0; i < fields.length; i++) {
            if (typeof(fields[i].value) === "object" && fields[i].value.length > 0) {
              var key = fields[i].id;

              obj[key] = {
                value: fields[i].value
              };
            } else if (typeof(fields[i].value) !== "object" && fields[i].value) {
              var key = fields[i].id;

              obj[key] = {
                value: fields[i].value
              };
            }
          };
          return obj;
        }

        function resetAttendeeFieldsToDefault(fields) {
          angular.forEach(fields, function(field) {
            field.value = field.defaultValue;
          });

          return fields;
        }

        scope.activeCustomAttendeeIdFunction = function(field) {
          if($rootScope.conference.rule_type == "none"){
            if (field) {
              if (field.key == 'email_address' ) {
                for (var i = 0; i < scope.attendeeFields.length; i++) {
                  if (scope.attendeeFields[i].key === 'attendee_id' && scope.attendeeFields[i].value == '') {
                    scope.activeCustomAttendeeId = false;
                    field.mandatory = true;
                    scope.attendeeFields[i].mandatory = false;
                  }
                }
              } else if (field.key == 'attendee_id' ) {
                for (var i = 0; i < scope.attendeeFields.length; i++) {
                  if (scope.attendeeFields[i].key === 'email_address' && scope.attendeeFields[i].value == '') {
                    scope.attendeeFields[i].mandatory = false;
                    scope.activeCustomAttendeeId = true;
                    field.mandatory = true;
                  }
                }
              }
            }
            else if (scope.attendeeFields[4].value == '' && scope.attendeeFields[11].value == '') {
              scope.activeCustomAttendeeId = false;
              scope.attendeeFields[4].mandatory = true;
              scope.attendeeFields[11].mandatory = false;
            }
          }
        }
	    }
		}
})();
