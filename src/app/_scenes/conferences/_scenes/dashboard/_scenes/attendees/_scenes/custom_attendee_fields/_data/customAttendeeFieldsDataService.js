(function() {
    'use strict';

    var serviceId = 'customAttendeeFieldsDataService';
    var app = angular.module('app');
    var dependenciesArray = [
        "$q",
        "API",
        "requestFactory",
        customAttendeeFieldsDataService
    ]

    app.service(serviceId, dependenciesArray);

    function customAttendeeFieldsDataService($q, API, requestFactory) {
        var service = {};

        service.createOne = function(conferenceId, fieldName, fieldOptions, fieldType) {
            var deferred = $q.defer();
            var url = 'api/field/create/';

            var data = {
                conference_id: conferenceId,
                name: fieldName,
                type: fieldType,
                options: fieldOptions
            }

            if (data.type == 1) delete data.options;

            requestFactory.post(url, data, false).then(function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        service.editOne = function(oldField, conferenceId, fieldName, fieldOptions, fieldType) {
            var deferred = $q.defer();
            var url = 'api/field/update/';
            var data = angular.copy(oldField);
            data.conference_id = conferenceId;
            data.name = fieldName;
            data.type = fieldType;
            data.options = fieldOptions;

            requestFactory.post(url, data, false).then(function(response) {
                oldField.conference_id = conferenceId;
                oldField.name = fieldName;
                oldField.type = fieldType;
                oldField.options = fieldOptions;
                deferred.resolve(oldField);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.deleteOne = function(fieldId) {
            var deferred = $q.defer();
            var url = 'api/field/delete/?id=' + fieldId;

            requestFactory.remove(url, {}, false).then(function(response) {
                deferred.resolve(fieldId);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }

        return service;
    }
})();
