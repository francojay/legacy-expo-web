(function(){
	'use strict'
  var serviceId = 'badgePrintSettingsDataService';

  var module = angular.module('app');
	var dependenciesArray = [
		'$q',
		'$http',
		'localStorageService',
		'constructBaseUrlService',
		badgePrintSettingsDataService
	]

  module.service(serviceId, dependenciesArray);

	function badgePrintSettingsDataService($q, $http, localStorageService, constructBaseUrlService) {
		var service = {};

    service.getBadgePrintSettings 	 = function(conferenceId) {
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/settings/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.createBadgePrintSettings = function(conferenceId, badgePrintSettings) {
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/settings/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: badgePrintSettings

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.updateBadgePrintSettings = function(conferenceId, badgePrintSettings) {
      var deferred = $q.defer();

      $http({
        method: 'PUT',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/settings/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: badgePrintSettings

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    return service;
	}
})();
