(function(){
  'use strict';

  var serviceId = 'expoBadgesDataService';
  var module = angular.module('app');

  module.service(serviceId, ['$rootScope', '$q', '$http', 'API', 'localStorageService', 'constructBaseUrlService', expoBadgesDataService]);

  function expoBadgesDataService($rootScope, $q, $http, API, localStorageService, constructBaseUrlService) {
    var service = {};

    service.getAllConferenceBadges = function(conferenceId){
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.deleteBadge = function(conferenceId, badgeId){
      var deferred = $q.defer();

      $http({
        method: 'DELETE',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.duplicateBadge = function(conferenceId, badgeId){
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/duplicate/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: {}

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.getBadgeAssignmentRules = function(conferenceId, badgeId){
      var deferred = $q.defer();

      $http({
        method: 'GET',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/assignment/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.createBadgeAssignment = function(conferenceId, badgeId, assignmentRules){
      var deferred = $q.defer();

      $http({
        method: 'POST',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/assignment/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: assignmentRules

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.updateBadgeAssignment = function(conferenceId, badgeId, assignmentRules){
      var deferred = $q.defer();

      $http({
        method: 'PUT',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/assignment/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: assignmentRules

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    service.calculateNumberOfAttendeesAssigned = function(conferenceId, badgeId, assignmentRules){
      var deferred = $q.defer();

      $http({
        method: 'PUT',
        url: constructBaseUrlService.getBaseUrl() + '/api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/assignment/calculator/',
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        },
        data: assignmentRules

      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

    return service;
  }
})();
