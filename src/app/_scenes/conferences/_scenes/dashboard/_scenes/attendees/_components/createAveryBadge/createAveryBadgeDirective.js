(function(){
	'use strict'

	var directiveId 		= 'createAveryBadge';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'actionCallerService',
		'ngDialog',
		createAveryBadgeDirective
	];

	app.directive(directiveId, depdenciesArray);

  function createAveryBadgeDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, actionCallerService, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/createAveryBadge/createAveryBadgeView.html',
		};
    function link (scope) {
			var conferenceId = $rootScope.conference.conference_id;
			scope.modalData	 = scope.ngDialogData._data;

			scope.callAction = function(action) {
				actionCallerService.callAction(conferenceId, action).then(function(response) {
					var data 	 = response.data;
					var status = response.status;

					$rootScope.$broadcast("NEW_PROCESS_HAS_BEEN_CREATED", data);
					scope.closeThisDialog();
				}, function(error) {

				})
			}
    }
  }
})();
