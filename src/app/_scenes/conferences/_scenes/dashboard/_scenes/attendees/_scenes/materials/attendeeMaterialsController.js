(function () {
    'use strict';

    var controllerId      = 'attendeeMaterialsController';
    var app               = angular.module('app');

		var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'attendeeMaterialsDataService',
      'fileService',
      '$q',
      '$timeout',
      attendeeMaterialsController
    ]

    app.controller(controllerId, dependenciesArray);

    function attendeeMaterialsController($scope, $rootScope, $stateParams, toastr, ngDialog, attendeeMaterialsDataService, fileService, $q, $timeout) {
      var currentConferenceId  = $rootScope.conference.conference_id;
      $scope.uploadedFile      = null;
      $scope.uploadedMaterials = [];

      $timeout(function() {
        $('.attendee-fields-wrapper .half.list .list-wrapper.materials').mCustomScrollbar({'theme': 'minimal-dark'});
      })

      getAttendeeMaterials(currentConferenceId);

      $scope.uploadLocally = function(file) {
        $scope.uploadedFile = file[0];
      }

      $scope.uploadToServer = function(file) {
        if (file) {
          fileService.getUploadSignature(file, false)
              .then(function(response) {
                  var fileLink = response.data.file_link;
                  var promises = [];

                  var data = {
                      conference_id: currentConferenceId,
                      file_url: fileLink,
                      name: file.name,
                      type: file.type
                  }
                  promises.push(fileService.uploadFile(file, response));
                  promises.push(attendeeMaterialsDataService.assignAttendeeMaterial(data));

                  $q.all(promises).then(function(response) {
                    var uploadedFileResponse = response[0];
                    var materialResponse     = response[1];
                    $scope.uploadedMaterials.push(materialResponse);
                    $scope.uploadedFile      = null;
                  }, function(error) {
                    toastr.error("An error has occured while trying to create your exhibitor material. Please try again!");
                  })

              }, function(error) {
                toastr.error('An error ocurred.');
              })
        } else {
          toastr.error("Please select a file from your computer first!")
        }
      }

      $scope.removeLocalFile = function() {
        $scope.uploadedFile = null;
      }

      $scope.removeMaterial = function($event, id) {
        $event.stopPropagation();
        attendeeMaterialsDataService.removeMaterial(currentConferenceId, id).then(function(response) {
          angular.forEach($scope.uploadedMaterials, function(material, index) {
            if (material.id === id) {
              $scope.uploadedMaterials.splice(index, 1);
              // $scope.$apply();
            }
          })
        }, function(error) {

        })
      }

      function getAttendeeMaterials(id) {
        attendeeMaterialsDataService.getAll(id).then(function(response) {
          $scope.uploadedMaterials = angular.copy(response);
        }, function(error) {
          toastr.error("An error has occured while trying to retrieve your uploaded attendee materials. Please try again later!");
        })
      }

    }
})();
