(function(){
	'use strict'

	var directiveId 		= 'selectAveryLabel';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'actionCallerService',
		'ngDialog',
		selectAveryLabelDirective
	];

	app.directive(directiveId, depdenciesArray);

  function selectAveryLabelDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, actionCallerService, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_components/selectAveryLabel/selectAveryLabelView.html',
		};
    function link (scope) {
			var conferenceId 						= $rootScope.conference.conference_id;

			scope.modalData 			 = scope.ngDialogData._data;
			scope.templates 			 = extractTemplatesFromData(scope.modalData.params);
			scope.selectedTemplate = scope.templates[0];

			genericDataTableInfoStore.setActiveAveryLabel(scope.selectedTemplate);

			scope.notifyChange = function(model) {
				genericDataTableInfoStore.setActiveAveryLabel(model);
			};

			scope.callAction = function(action) {
				actionCallerService.callAction(conferenceId, action).then(function(response) {
					var data 	 = response.data;
					var status = response.status;

					if (data.result_type === 'redirect_to') {
						$rootScope.$broadcast("NEW_PROCESS_HAS_BEEN_CREATED", data);
						scope.closeThisDialog();
					}
				}, function(error) {

				})
			}


			function extractTemplatesFromData(params) {
				var templates = null;

				for (var i = 0; i < params.length; i++) {
					if (params[i].name === "avery_template") {
						templates = params[i].options;
					}
				}

				return templates;
			}


    }
  }
})();
