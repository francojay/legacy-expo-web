(function(){
	'use strict';

  var serviceId = 'attendeesEventsPropagator';
	var app = angular.module('app');
	app.service(serviceId, ['$rootScope', attendeesEventsPropagator]);

	function attendeesEventsPropagator($rootScope) {
    var service = {};

		service.availableEvents = {
			UPDATE_SELECTED_ATTENDEES_LIST : "UPDATE_SELECTED_ATTENDEES_LIST",
			UPDATE_SELECTED_ALL_ATTENDEES	 : "UPDATE_SELECTED_ALL_ATTENDEES",
			UPDATE_SELECTED_VIEW_FIELDS		 : "UPDATE_SELECTED_VIEW_FIELDS",
			UPDATE_ADDED_ATTENDEES_LIST		 : "UPDATE_ADDED_ATTENDEES_LIST",
			UPDATE_EDIT_ATTENDEES_LIST		 : "UPDATE_EDIT_ATTENDEES_LIST",
			DELETE_CONFERENCE_ATTENDEES    : 'DELETE_CONFERENCE_ATTENDEES',
			DELETE_ALL_CONFERENCE_ATTENDEES: "DELETE_ALL_CONFERENCE_ATTENDEES",
			ATTENDEE_REFUND_SUCCESS        : "ATTENDEE_REFUND_SUCCESS"
		};

		service.propagateEvent = function(typeOfEvent, data) {
			var dispatchedEventIsRegistered = false;

			if (typeOfEvent) {
					dispatchedEventIsRegistered = service._searchEventInGivenSet(service.availableEvents, typeOfEvent);
					if (dispatchedEventIsRegistered) {
							if (!$rootScope.$$listeners[typeOfEvent]) {
									console.warn('THE EVENT: [', typeOfEvent, '] HAS NO LISTENERS! PLEASE IMPLEMENT THE LISTENER WHERE IT IS NEEDED! EVENT HAS NOT BEEN DISPATCHED!');
							} else {
									$rootScope.$broadcast(typeOfEvent, data);
							}
					} else {
							alert('EVENT: [' + typeOfEvent + '] HAS NOT BEEN DEFINED! USE EVENTS ONLY FROM THE ONES DECLARED');
					}
			} else {
					alert('SEND EVENT NAME!');
			}
		}

		service._searchEventInGivenSet = function(set, givenEvent) {
			var foundEventInSet = false;
			angular.forEach(set, function(event) {
					if (event === givenEvent) {
							foundEventInSet = true;
					}
			});

			return foundEventInSet;
		}

		var exposedService = {
			availableEvents: service.availableEvents,
			propagateEvent : service.propagateEvent
		}

    return exposedService;
  }
})();
