(function () {
    'use strict';

    var controllerId      = 'superAdminConferenceRevenue';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'conferenceService',
      '$state',
      superAdminConferenceRevenue
    ]

    app.controller(controllerId, dependenciesArray);

    function superAdminConferenceRevenue($scope, $rootScope, $stateParams, toastr, ngDialog, conferenceService, $state) {
			getRevenueForSuperAdmin($rootScope.conference.conference_id);

			function getRevenueForSuperAdmin(conference_id) {
					conferenceService
							.getSuperAdminRevenue(conference_id)
							.then(function(response){
									$scope.exhibitor_revenue = response.exhibitor_data;
																					$scope.exhibitor_revenue.payment_method = lib.capitalizeFirstLetter($scope.exhibitor_revenue.payment_method);
									$scope.conference_revenue = response.conference_data;
									$scope.registration_revenue = response.registration_data;
									$scope.totalNumberOfRegistrants = getTotalNumberOfRegistrants($scope.registration_revenue.levels);
									$scope.overall_revenue = response.overall_data;
									setOverallConferenceRevenueGraph($scope.overall_revenue);
							}, function(error){
									console.log(error)
							});
			};

			function getTotalNumberOfRegistrants(levels) {
					var total_number = 0;
					_.each(levels, function (level) {
							total_number += level.nr_registrants;
					});
					return total_number;
			}

			function setOverallConferenceRevenueGraph(overall_revenue_data){
					var exhibitor_rev = overall_revenue_data.total_exhibitor_revenue;
					var organizer_rev = overall_revenue_data.total_conference_organizer_revenue;
					var registration_rev = overall_revenue_data.total_registration_expo_revenue;

					var arr =
							[
									{
											'label': 'Exhibitor $' + (Math.ceil(exhibitor_rev) >= 1000 ? Math.ceil(exhibitor_rev)/1000 : Math.ceil(exhibitor_rev) ) + (Math.ceil(exhibitor_rev) >= 1000 ? 'k' : ''),
											'value': exhibitor_rev,
											'color': '#8bd0ef'
									},
									{
											'label': 'Organizer $' + (Math.ceil(organizer_rev) >= 1000 ? Math.ceil(organizer_rev)/1000 : Math.ceil(organizer_rev) ) + (Math.ceil(organizer_rev) >= 1000 ? 'k' : ''),
											'value': organizer_rev,
											'color': '#dbf4ff'
									},
									{
											'label': 'Registration $' + (Math.ceil(registration_rev) >= 1000 ? Math.ceil(registration_rev)/1000 : Math.ceil(registration_rev) ) + (Math.ceil(registration_rev) >= 1000 ? 'k' : ''),
											'value': registration_rev,
											'color': '#26a7de'
									}
							];
					lib.overallConferenceRevenueGraph(arr);
			}
    }
})();
