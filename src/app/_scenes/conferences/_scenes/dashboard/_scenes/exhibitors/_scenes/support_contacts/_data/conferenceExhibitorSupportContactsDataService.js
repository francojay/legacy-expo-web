(function(){
	'use strict';

    var serviceId 				= 'conferenceExhibitorSupportContactsDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			conferenceExhibitorSupportContactsDataService
		];

		app.service(serviceId, dependenciesArray);

		function conferenceExhibitorSupportContactsDataService($q, requestFactory) {
	    var service = {};

			service.getAll = function(conferenceId) {
				var deferred = $q.defer();

				var url = "api/v1.1/conferences/" + conferenceId + "/exhibitor_contacts/";

				requestFactory.get(url, {}, false, false).then(function(response) {
					deferred.resolve(response.data.exhibitors);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			service.deleteOne = function(conferenceId, supportContactId) {
				var deferred = $q.defer();
				var url 		 = "api/v1.1/conferences/" + conferenceId + "/exhibitor_contacts/" + supportContactId + "/";

				requestFactory.remove(url, {}, false, false).then(function(response) {
					deferred.resolve(response.data)
				}, function(error) {
					deferred.reject(error);
				})

				return deferred.promise;
			};

			service.addOne = function(conferenceId, model, photo) {
				var deferred = $q.defer();
				var url 		 = "api/v1.1/conferences/" + conferenceId + "/exhibitor_contacts/";

				var obj = {};

				angular.forEach(model, function(field) {
					obj[field.key] = field.value || null;
				});

				obj.photo 	= photo;

				requestFactory.post(url, obj, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}


			service.editOne = function(conferenceId, model, photo, supportContactId) {
				var deferred = $q.defer();
				var url 		 = "api/v1.1/conferences/" + conferenceId + "/exhibitor_contacts/" + supportContactId + "/";
				var obj = {};
				obj.photo 	= photo;

				angular.forEach(model, function(field) {
					obj[field.key] = field.value || null;
				});

				requestFactory.put(url, obj, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});
				return deferred.promise;
			}


      return service;
    }
})();
