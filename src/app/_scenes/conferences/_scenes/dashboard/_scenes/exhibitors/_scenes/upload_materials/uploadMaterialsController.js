(function () {
    'use strict';

    var controllerId      = 'uploadMaterialsController';
    var app               = angular.module('app');

		var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'exhibitorMaterialsDataService',
      'fileService',
      '$q',
      '$timeout',
      uploadMaterialsController
    ]

    app.controller(controllerId, dependenciesArray);

    function uploadMaterialsController($scope, $rootScope, $stateParams, toastr, ngDialog, exhibitorMaterialsDataService, fileService, $q, $timeout) {
      var currentConferenceId  = $rootScope.conference.conference_id;
      $scope.uploadedFile      = null;
      $scope.uploadedMaterials = $rootScope.conference.exhibitor_materials;

      $timeout(function() {
        $('.attendee-fields-wrapper .half.list .list-wrapper.materials').mCustomScrollbar({'theme': 'minimal-dark'});
      })

      $scope.uploadLocally = function(file) {
        $scope.uploadedFile = file[0];
      }

      $scope.uploadToServer = function(file) {
        if (file) {
          fileService.getUploadSignature(file, false)
              .then(function(response) {
                  var fileLink = response.data.file_link;
                  var promises = [];

                  var data = {
                      conference_id: currentConferenceId,
                      file_url: fileLink,
                      name: file.name,
                      type: file.type
                  }
                  promises.push(fileService.uploadFile(file, response));
                  promises.push(exhibitorMaterialsDataService.assignExhibitorMaterial(data));

                  $q.all(promises).then(function(response) {
                    var uploadedFileResponse = response[0];
                    var materialResponse     = response[1];
                    $scope.uploadedMaterials.push(materialResponse);
                    $scope.uploadedFile      = null;
                  }, function(error) {
                    toastr.error("An error has occured while trying to create your exhibitor material. Please try again!");
                  })

              }, function(error) {
                toastr.error('An error ocurred.');
              })
        } else {
          toastr.error("Please select a file from your computer first!")
        }
      }

      $scope.removeMaterial = function($event, id) {
        $event.stopPropagation();
        exhibitorMaterialsDataService.removeMaterial(id).then(function(response) {
          angular.forEach($scope.uploadedMaterials, function(material, index) {
            if (material.id === id) {
              $scope.uploadedMaterials.splice(index, 1);
              // $scope.$apply();
            }
          })
        }, function(error) {

        })
      }

    }
})();
