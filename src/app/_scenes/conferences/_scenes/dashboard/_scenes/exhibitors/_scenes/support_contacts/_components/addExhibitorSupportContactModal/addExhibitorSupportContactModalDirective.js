(function () {
    'use strict'

    var directiveId = 'addExhibitorSupportContactModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
      'conferenceExhibitorSupportContactsDataService',
      'ngDialog',
      'fileService',
      'intlTelInputOptions',
      'API',
			addExhibitorSupportContactModal
		];

		app.directive(directiveId, dependenciesArray);

    function addExhibitorSupportContactModal($rootScope, $q, toastr, conferenceExhibitorSupportContactsDataService, ngDialog, fileService, intlTelInputOptions, API) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/support_contacts/_components/addExhibitorSupportContactModal/addExhibitorSupportContactModalView.html',
            link: link
        }

        function link(scope) {
          var conferenceId = $rootScope.conference.conference_id;
          scope.photoLink    = "";
          scope.model = [
            {
              "shownValue" : "First Name",
              "key"        : "first_name",
              "mandatory"  : true
            },
            {
              "shownValue" : "Last Name",
              "key"        : "last_name",
              "mandatory"  : true
            },
            {
              "shownValue" : "Company Name",
              "key"        : "company_name",
              "mandatory"  : true
            },
            {
              "shownValue" : "Email Address",
              "key"        : "email",
              "mandatory"  : false
            },
            {
              "shownValue" : "Phone Number",
              "key"        : "phone_number",
              "mandatory"  : false
            },
            {
              "shownValue" : "Area of Support",
              "key"        : "area_of_support",
              "mandatory"  : true
            }
          ];
          scope.defaultCountryFlag = 'us';
          angular.forEach(API.COUNTRIES, function(country) {
            if(country.name === $rootScope.conference.country){
              scope.defaultCountryFlag = country.code.toLowerCase();
            }
          });
          scope.saveExhibitorSupportContact = function(model, photo, closeDialog) {
            var errorsFound = false;
            for (var i = 0; i < model.length; i++) {
              var field = model[i];
              if (field.mandatory && !field.value) {
                toastr.error(field.shownValue + " is a mandatory field. Please complete it before saving!");
                field.error = true;
                errorsFound = true;
              } else if (field.mandatory && field.value ) {
                field.error = false;
              } else if (field.key === "email" && field.value) {
                var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                field.error = !regexExpression.test(field.value);
                if (field.error) {
                  errorsFound = true;
                  toastr.error("The Email Address is not valid!");
                }
              }
              else if((field.key == "phone_number" && field.hasOwnProperty('value') && field.value === undefined) || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1){
    							toastr.warning(field.shownValue + " need to have the correct format!");
                  errorsFound = true;
    					}
            };

            if (!errorsFound) {
              conferenceExhibitorSupportContactsDataService.addOne(conferenceId, model, photo).then(function(response) {
                $rootScope.$broadcast("ADDED_EXHIBITOR_SUPPORT_CONTACT", response);

                if (closeDialog) {
                  scope.closeThisDialog();
                } else {
                  angular.forEach(scope.model, function(field) {
                    field.value = "";
                  });
                }
              }, function(error) {
                console.log(error);
              });
            }
          }


          scope.addSupportContactPhoto      = function(files) {
            if (files) {
              fileService.getUploadSignature(files[0], false)
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];

                      promises.push(fileService.uploadFile(files[0], response));

                      $q.all(promises).then(function(response){
                        scope.photoLink = file_link;
                        console.log(scope.photoLink)
                      }).catch(function(error){

                      });

                  })
                  .catch(function(error){

                      if (error.status == 403) {
                          toastr.error('Permission Denied!');
                      } else {
                          toastr.error('Error!', 'There has been a problem with your request.');
                      }
                  });
            }
          }

          scope.closeModal                  = function() {
            scope.closeThisDialog();
          }
        }
    }
})();
