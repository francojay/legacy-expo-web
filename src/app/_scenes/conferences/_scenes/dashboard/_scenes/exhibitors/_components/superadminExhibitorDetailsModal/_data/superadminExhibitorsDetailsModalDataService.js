(function(){
	'use strict';

    var serviceId 				= 'superadminExhibitorDetailsModalDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			superadminExhibitorDetailsModalDataService
		];

		app.service(serviceId, dependenciesArray);

		function superadminExhibitorDetailsModalDataService($q, requestFactory) {
	    var service = {};

			service.addUser = function(exhibitorId, userEmail, permissions) {
				var deferred = $q.defer();
				var url			 = "api/exhibitor/user_add/"
				var data = {
					exhibitor_id			: exhibitorId,
					invited_user_email: userEmail,
					rights						: service._processPermissions(permissions),
				};

				var permissionKeysWithBooleanValues = service._mapPermissionsToBooleanValues(permissions);
				data = service._extendData(data, permissionKeysWithBooleanValues);

				requestFactory.post(url, data, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error.data);
				})

				return deferred.promise;
			};

			service.getUsersForCurrentExhibitor = function(exhibitorId) {
				var deferred 			= $q.defer();
				var url = "api/v1.1/exhibitor_users/" +  exhibitorId + "/";

				requestFactory.get(url, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

			service.getExhibitorQualifiers = function(exhibitorId) {
				var deferred = $q.defer();

				var url = "api/v1.1/exhibitor_questions/" + exhibitorId + "/";

				requestFactory.get(url, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

			service.saveQualifiers = function(exhibitorId, qualifiers) {
				var deferred = $q.defer();
				var url 		= "api/v1.1/exhibitor_questions/" + exhibitorId + "/";

				var obj = {
					"exhibitor_questions": qualifiers
				}

				requestFactory.post(url, obj, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

			service._processPermissions = function(permissions) {
				var arr = [];

				angular.forEach(permissions, function(permission) {
					if (permission.value === true) {
						arr.push(permission.key);

					}
				});

				return arr;
			};

			service._mapPermissionsToBooleanValues = function(permissions) {
				var obj = {};

				angular.forEach(permissions, function(permission) {
					obj[permission.key] = permission.value;
				});

				return obj;
			};

			service._extendData 									= function(data, permissions) {
		    for (var key in permissions) {
		        if (permissions.hasOwnProperty(key)) data[key] = permissions[key];
		    }
		    return data;
			};

      return service;
    }
})();
