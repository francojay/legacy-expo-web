(function() {
  'use strict'

  var directiveId = 'addExhibitorModal';
  var app = angular.module('app');

  var dependenciesArray = [
    '$rootScope',
    '$q',
    'toastr',
    'addExhibitorModalDataService',
    addExhibitorModal
  ];

  app.directive(directiveId, dependenciesArray);

  function addExhibitorModal($rootScope, $q, toastr, addExhibitorModalDataService) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_components/addExhibitorModal/addExhibitorModalView.html',
      link: link
    }

    function link(scope) {
      scope.errorOnEmailAddress = false;

      scope.model = [{
          "key": "name",
          "mandatory": true,
          "shownValue": "Company Name",
          "value": ""
        },
        {
          "key": "booth",
          "mandatory": true,
          "shownValue": "Booth Number",
          "value": ""
        },
        {
          "key": "booth_contact_email",
          "mandatory": false,
          "shownValue": "Booth Contact Email",
          "value": ""
        },
        {
          "key": "booth_contact_first_name",
          "mandatory": false,
          "shownValue": "Booth Contact First Name",
          "value": ""
        },
        {
          "key": "booth_contact_last_name",
          "mandatory": false,
          "shownValue": "Booth Contact Last Name",
          "value": ""
        }
      ];

      scope.closeModal = function() {
        scope.closeThisDialog()
      };

      scope.saveExhibitor = function(model, closeModal) {
        var errorOnMandatoryFields = false;
        var errorOnEmailAddress = false;
        for (var i = 0; i < model.length; i++) {
          var field = model[i];
          if (field.mandatory && !field.value) {
            toastr.error(field.shownValue + " is a required field. Please input a value!");
            field.error = true;
            errorOnMandatoryFields = true;
            break;
          } else if (field.key === "booth_contact_email" && field.value) {
            errorOnEmailAddress = checkEmailValidity(field.value);
            if(errorOnEmailAddress){
              field.error = true;
            }
            break;
          }
          else{
            field.error = false;
          }
        };

        if (!errorOnEmailAddress && !errorOnMandatoryFields) {
          var conferenceId = $rootScope.conference.conference_id;
          addExhibitorModalDataService.createExhibitor(conferenceId, model).then(function(response) {
            $rootScope.$broadcast("ADDED_NEW_EXHIBITOR", response);
            resetFieldsToDefault(scope.model);
          }, function(error) {
            if(error.data.detail){
              toastr.error(error.data.detail);
            }
          });

          if (closeModal) {
            scope.closeThisDialog();
          }
        }
      };

      function checkEmailValidity(emailAddress) {
        var regexExpression = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
        scope.errorOnEmailAddress = !(regexExpression.test(emailAddress));
        if (scope.errorOnEmailAddress) {
          toastr.error("Email address is not valid!");
        }
        return scope.errorOnEmailAddress;

          function resetFieldsToDefault(fields) {
            angular.forEach(fields, function(field) {
              field.value = "";
            });

            return fields;
          }

      }
      function resetFieldsToDefault(fields) {
        angular.forEach(fields, function(field) {
          field.value = "";
        });

        return fields;
      }
    }


  }
})();
