(function () {
    'use strict';

    var controllerId      = 'exhibitorsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'exhibitorsDataService',
      'exhibitorsEventsPropagator',
      '$timeout',
      exhibitorsController
    ]

    app.controller(controllerId, dependenciesArray);

    function exhibitorsController($scope, $rootScope, $stateParams, toastr, ngDialog, exhibitorsDataService, exhibitorsEventsPropagator, $timeout) {
      var exhibitorsPage  = 1;
      var availableEvents           = exhibitorsEventsPropagator.availableEvents;

      $scope.exhibitors   = [];
      $scope.exhibitorsTotalCount = 0;
      $scope.orderingType = ["desc"];
      $scope.checkedExhibitorIds = [];
      $scope.sortType = 'name';
      $scope.sortReverse = false;
      $scope.isProcessing = false;
      $scope.exhibitorsSearchQuery = '';

      getPaginatedExhibitors($rootScope.conference.conference_id, exhibitorsPage);

      $timeout(function() {
        $('.exhibitors-overview .exhibitors-list-wrapper .list').mCustomScrollbar({'theme': 'minimal-dark'});
      }, 0);

      $scope.openExhibitorAddModal = function() {
        ngDialog.open({
          plain       : true,
          template    : '<add-exhibitor-modal></add-exhibitor-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss'
        });
      };

      $scope.openExhibitorDetailsModal = function(exhibitor) {
        if ($rootScope.isSuperAdmin) {
          ngDialog.open({
            plain       : true,
            template    : '<superadmin-exhibitor-details-modal></superadmin-exhibitor-details-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
            data        : exhibitor
          });
        } else {
          ngDialog.open({
            plain       : true,
            template    : '<exhibitor-details-modal></exhibitor-details-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
            data        : exhibitor
          });
        }
      };

      $scope.searchExhibitors = function(query){
        resetExhibitors();
        var query = {
          queryString   : query || '',
          sortBy        : 'first_name',
          sortOrder     : "asc"
        };
        getFilteredExhibitors(query);
      }

      $scope.orderExhibitorsBy = function(field) {
        if($scope.sortType == field){
          $scope.sortReverse = !$scope.sortReverse;
        } else {
          $scope.sortType = field;
          $scope.sortReverse = false;
        }
      }

      $scope.toggleCheckAllExhibitors = function() {
        if($scope.allExhibitorsAreChecked == true){
          uncheckAllExhibitors();
        } else {
          checkAllExhibitors();
        }
      };

      $scope.toggleCheckedState = function($event, exhibitor) {
        $event.stopPropagation();
        exhibitor.checked = !exhibitor.checked;
        if (!exhibitor.checked) {
          $scope.allExhibitorsAreChecked = false;
          for (var i = 0; i < $scope.checkedExhibitorIds.length; i++) {
            if ($scope.checkedExhibitorIds[i] === exhibitor.id) {
              $scope.checkedExhibitorIds.splice(i, 1);
            }
          }
        } else {
          $scope.checkedExhibitorIds.push(exhibitor.id);
          if ($scope.checkedExhibitorIds.length === $scope.exhibitors.length) {
            $scope.allExhibitorsAreChecked = true;
          }
        }
      };

      $scope.deleteExhibitorsModal = function() {
        var data = {};
        if($scope.allExhibitorsAreChecked && $scope.checkedExhibitorIds.length == $rootScope.conference.exhibitors_count)
          data.numberOfExhibitors = -1;
        else {
            data.numberOfExhibitors = $scope.checkedExhibitorIds.length;
          }
        ngDialog.open({
              plain: true,
              template: '<delete-conference-exhibitors-modal></delete-conference-exhibitors-modal>',
              className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_components/deleteExhibitorModal/deleteExhibitorStyle.scss',
              data: data
        });
      }

      $scope.revokeAccessToExhibitors = function($event, checkedExhibitorIds) {
        exhibitorsDataService.revokeAccessToExhibitors($rootScope.conference.conference_id, checkedExhibitorIds).then(function(result) {
          toastr.success("Access for the selected exhibitors was revoked successfully");
        }, function(error) {
          toastr.error("An error has occured. Please try again later.")
        })
      }

      function getPaginatedExhibitors(conferenceId, page) {
        exhibitorsDataService.getPaginatedExhibitors(conferenceId, page).then(function(response) {
          $scope.exhibitors = response.exhibitors;
          $scope.exhibitorsTotalCount = response.total_count;
          $rootScope.conference.exhibitors_count = $scope.exhibitors.length;
          exhibitorsPage++;
        }, function(error) {
          toastr.error('There was a problem with your request.', 'Try again.');
        });
      }

      function getFilteredExhibitors(filters, page) {
        if(!$scope.isProcessing){
          $scope.isProcessing = true;
          if(!page){
            var page = 1;
          }
          exhibitorsDataService
           .getFilteredExhibitors($rootScope.conference.conference_id, 1, false, filters)
              .then(function(data) {
                $scope.exhibitors = $scope.exhibitors.concat(data.exhibitors);
                // THE CODE BELOW IS HERE BECAUSE THE ENDPOINTS NEEDS TO BE UPDATED
                // i get exhibitor.company_name instead of exhibitor.name
                angular.forEach($scope.exhibitors, function(exhibitor){
                  exhibitor.name = exhibitor.company_name;
                })
                // THE CODE ABOVE IS HERE BECAUSE THE ENDPOINTS NEEDS TO BE UPDATED
                $scope.exhibitorsTotalCount = data.total_count;
                $scope.isProcessing = false;
                uncheckAllExhibitors();
            }, function(error) {
              $scope.isProcessing = false;
              toastr.error('There was a problem with your request.', 'Try again.');
            });
        }
      }

      function deleteExhibitors() {
        var deleteAll = false;
        if ($scope.checkedExhibitorIds.length === $rootScope.exhibitors_count) {
           deleteAll = true;
         }
         exhibitorsDataService.deleteExhibitors($rootScope.conference.conference_id, $scope.checkedExhibitorIds, deleteAll).then(function(result) {
           $scope.exhibitors = $scope.exhibitors.filter(function(exhibitor) {
             return !exhibitor.checked;
           });
          $rootScope.conference.exhibitors_count -= $scope.checkedExhibitorIds.length;
           $scope.checkedExhibitorIds = [];
           $scope.allExhibitorsAreChecked = false;
           toastr.success("The selected exhibitors have been deleted successfully");
          if($rootScope.conference.exhibitors_count > 0){
            $scope.exhibitorsSearchQuery = '';
            $scope.searchExhibitors("");
          }
         }, function(error) {
           toastr.error("An error has occured. Please try again later.")
         })

      }

      function resetExhibitors() {
        $scope.exhibitors   = [];
        $scope.checkedExhibitorIds = [];
        exhibitorsPage = 1;
      }

      function checkAllExhibitors() {
        angular.forEach($scope.exhibitors, function(exhibitor) {
          exhibitor.checked = true;
        });
        $scope.allExhibitorsAreChecked = true;
        $scope.checkedExhibitorIds = [];
        angular.forEach($scope.exhibitors, function(exhibitor) {
          $scope.checkedExhibitorIds.push(exhibitor.id);
        })
      }

      function uncheckAllExhibitors() {
        angular.forEach($scope.exhibitors, function(exhibitor) {
          exhibitor.checked = false;
        });
        $scope.allExhibitorsAreChecked = false;
        $scope.checkedExhibitorIds = [];
      }

      $rootScope.$on(availableEvents.ADDED_NEW_EXHIBITOR, function(event, data) {
        $scope.exhibitors.push(data);
        $rootScope.conference.exhibitors_count = $scope.exhibitors.length;
      });

      $rootScope.$on(availableEvents.UPDATE_EDIT_EXHIBITORS_LIST, function(event, data) {
        angular.forEach($scope.exhibitors, function(exhibitor, key) {
          if (exhibitor.id === data.id) {
            angular.forEach(data, function(value, key) {
              exhibitor[key] = value;
            })
          }
        });
      });

      var myListener = $rootScope.$on(availableEvents.DELETE_CONFERENCE_EXHIBITORS, function(event, data) {
        deleteExhibitors();
      });

      $scope.$on('$destroy', myListener); // delete event listener on page leave
    }
})();
