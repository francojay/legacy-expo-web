(function() {
	'use strict';
  var serviceId = 'exhibitorPaymentsDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'$http',
		'$filter',
		'toastr',
		'localStorageService',
		'requestFactory',
		exhibitorPaymentsDataService
	];
	app.service(serviceId, dependenciesArray);
	function exhibitorPaymentsDataService($q, $http, $filter, toastr, localStorageService, requestFactory) {
		var service = {};

		service.addPaymentMethod = function(token) {
			var deferred = $q.defer();

			var url = "api/account/add_stripe/"
			var data = {
				"stripeToken": token
			};

			requestFactory.post(url, data, false).then(function(response) {
			 deferred.resolve(response.data);
		 	}, function(erorr) {
			 deferred.reject(error);
			})

			return deferred.promise;
		};

		service.updatePaymentType = function(conference, paymentType) {
			var deferred = $q.defer();
			var url = "api/v1.1/conference/" + conference.conference_id + "/";
			conference.payment_type = paymentType;

			requestFactory.post(url, conference, false).then(function(response) {
			 deferred.resolve(response.data);
		 }, function(error) {
			 deferred.reject(error);
			})

			return deferred.promise;
		};

		service.purchasePromoCodes = function (conferenceId, numberOfPromoCodes, costPerCode, stripeToken) {
			var deferred = $q.defer();
			var url = "api/exhibitor/promo_codes_create_updated/";
			var data = {
				conference_id : conferenceId,
				description : "Generated Promo Codes",
				number : numberOfPromoCodes,
				stripeToken : stripeToken,
				value : costPerCode
			};

			requestFactory.post(url, data, false).then(function(response) {
			 deferred.resolve(response.data);
			}, function(error) {
			 deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
