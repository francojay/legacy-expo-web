(function () {
    'use strict'

    var directiveId = 'deleteExhibitorSupportContactModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'conferenceExhibitorSupportContactsDataService',
      'toastr',
      '$timeout',
			deleteExhibitorSupportContactModal
		];

		app.directive(directiveId, dependenciesArray);

    function deleteExhibitorSupportContactModal($rootScope, conferenceExhibitorSupportContactsDataService, toastr, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/support_contacts/_components/deleteExhibitorSupportContactModal/deleteExhibitorSupportContactModalView.html',
            link: link
        }

        function link(scope) {
          var conferenceId = scope.ngDialogData.conferenceId;
          var exhibitorId  = scope.ngDialogData.exhibitorId;

          scope.closeDialog = function() {
            scope.closeThisDialog();
          }

          scope.confirmDelete = function() {
            conferenceExhibitorSupportContactsDataService.deleteOne(conferenceId, exhibitorId).then(function(response) {
              $rootScope.$broadcast("DELETED_EXHIBITOR_SUPPORT_CONTACT", {"exhibitorId": exhibitorId});
              $timeout(function() {
                scope.closeThisDialog();
              }, 100);
            }, function(error) {

            })
          }

        }
    }
})();
