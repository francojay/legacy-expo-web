(function(){
	'use strict';

  var serviceId = 'exhibitorsEventsPropagator';
	var app = angular.module('app');
	app.service(serviceId, ['$rootScope', exhibitorsEventsPropagator]);

	function exhibitorsEventsPropagator($rootScope) {
    var service = {};

		service.availableEvents = {
			ADDED_NEW_EXHIBITOR : 'ADDED_NEW_EXHIBITOR',
			DELETE_CONFERENCE_EXHIBITORS    : 'DELETE_CONFERENCE_EXHIBITORS',
			UPDATE_EDIT_EXHIBITORS_LIST : 'UPDATE_EDIT_EXHIBITORS_LIST',
			//DELETE_ALL_CONFERENCE_EXHIBITORS: "DELETE_ALL_CONFERENCE_EXHIBITORS"
		};

		service.propagateEvent = function(typeOfEvent, data) {
			var dispatchedEventIsRegistered = false;

			if (typeOfEvent) {
					dispatchedEventIsRegistered = service._searchEventInGivenSet(service.availableEvents, typeOfEvent);
					if (dispatchedEventIsRegistered) {
							if (!$rootScope.$$listeners[typeOfEvent]) {
									console.warn('THE EVENT: [', typeOfEvent, '] HAS NO LISTENERS! PLEASE IMPLEMENT THE LISTENER WHERE IT IS NEEDED! EVENT HAS NOT BEEN DISPATCHED!');
							} else {
									$rootScope.$broadcast(typeOfEvent, data);
							}
					} else {
							alert('EVENT: [' + typeOfEvent + '] HAS NOT BEEN DEFINED! USE EVENTS ONLY FROM THE ONES DECLARED');
					}
			} else {
					alert('SEND EVENT NAME!');
			}
		}

		service._searchEventInGivenSet = function(set, givenEvent) {
			var foundEventInSet = false;
			angular.forEach(set, function(event) {
					if (event === givenEvent) {
							foundEventInSet = true;
					}
			});

			return foundEventInSet;
		}

		var exposedService = {
			availableEvents: service.availableEvents,
			propagateEvent : service.propagateEvent
		}

    return exposedService;
  }
})();
