(function () {
    'use strict'

    var directiveId = 'superadminExhibitorDetailsModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
      'exhibitorsDetailsModalDataService',
			'$timeout',
      'superadminExhibitorDetailsModalDataService',
      'exhibitorConferenceUserDataService',
			superadminExhibitorDetailsModal
		];

		app.directive(directiveId, dependenciesArray);

    function superadminExhibitorDetailsModal($rootScope, $q, toastr, exhibitorsDetailsModalDataService, $timeout, superadminExhibitorDetailsModalDataService, exhibitorConferenceUserDataService) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_components/superadminExhibitorDetailsModal/superadminExhibitorDetailsModalView.html',
            link: link
        }

        function link(scope) {
          $timeout(function () {
						$('.custom-modal-wrapper.add-exhibitor .bottom-part .sections-rail .section').mCustomScrollbar({'theme': 'minimal-dark'});
					}, 0);
					scope.exhibitor               = angular.copy(scope.ngDialogData);
          scope.errorOnEmailAddress     = false;
          scope.translateValue          = 0;
          scope.userEmailToAdd          = "";
          scope.emailToAddIsValid       = false;
          scope.permissionsPanelVisible = false;
          scope.users                   = [];


          scope.userPermissions     = [
            {
              shownValue : "Edit Company Profile",
              key        : "edit_company_profile",
              value      : true
            },
            {
              shownValue : "Scan Leads",
              key        : "scan_leads",
              value      : true
            },
            {
              shownValue : "Manage Leads",
              key        : "manage_leads",
              value      : true
            },
            {
              shownValue : "Download Leads",
              key        : "download_leads",
              value      : true
            }
          ];

          scope.modelForView = [
	           {
							shownValue : "Exhibitor Admin",
							value 		 : scope.exhibitor.admin ? scope.exhibitor.admin.first_name + " " + scope.exhibitor.admin.last_name : ""
						},
						{
							shownValue : "Email Address",
							value 		 : scope.exhibitor.admin_email
						},
						{
							shownValue : "Phone Number",
							value 		 : scope.exhibitor.phone_number
						},
						{
							shownValue : "Street Address",
							value 		 : scope.exhibitor.street_address
						},
						{
							shownValue : "City",
							value 		 : scope.exhibitor.city
						},
						{
							shownValue : "State",
							value 		 : scope.exhibitor.state
						},
						{
							shownValue : "Zip Code",
							value 		 : scope.exhibitor.zip_code
						},
						{
							shownValue : "Country",
							value 		 : scope.exhibitor.country
						},
						{
							shownValue : "Booth Contact",
							value 		 : (scope.exhibitor.booth_contact_first_name||"") + " " + (scope.exhibitor.booth_contact_last_name||"")
						},
						{
							shownValue : "Booth Contact Email",
							value 		 : scope.exhibitor.booth_contact_email
						},
						{
							shownValue : "Booth Number",
							value 		 : scope.exhibitor.booth
						},
						{
							shownValue : "Users",
							value 		 : scope.exhibitor.users_count
						},
						{
							shownValue : "Promo Code",
							value 		 : scope.exhibitor.promo_code ? scope.exhibitor.promo_code : "No promo code issued."
						}
					];

					scope.modelForEdit = [
						{
							"shownValue" : "Company Name",
							"key"				 : "name",
							"value"			 : scope.exhibitor.name,
						},
						{
							"shownValue" : "Admin Name",
							"value"			 : scope.exhibitor.admin ? scope.exhibitor.admin.first_name + " " + scope.exhibitor.admin.last_name : "Admin has not joined yet.",
						},
						{
							"shownValue" : "Company Email",
							"value"			 : scope.exhibitor.admin_email
						},
						{
							"shownValue" : "Phone number",
							"value"			 : scope.exhibitor.phone_number
						},
						{
							"shownValue" : "Street Address",
							"value"			 : scope.exhibitor.street_address
						},
						{
							"shownValue" : "City",
							"value"			 : scope.exhibitor.city
						},
						{
							"shownValue" : "Zip Code",
							"value"			 : scope.exhibitor.zip_code
						},
						{
							"shownValue" : "State",
							"value"			 : scope.exhibitor.state || "--"
						},
						{
							"shownValue" : "Country",
							"value"			 : scope.exhibitor.country
						},
						{
							"shownValue" : "Booth Contact First Name",
							"key"				 : "booth_contact_first_name",
							"value"			 : scope.exhibitor.booth_contact_first_name
						},
						{
							"shownValue" : "Booth Contact Last Name",
							"key"				 : "booth_contact_last_name",
							"value"			 : scope.exhibitor.booth_contact_last_name
						},
						{
							"shownValue" : "Booth Contact Email",
							"key"				 : "booth_contact_email",
							"value"			 : scope.exhibitor.booth_contact_email
						},
						{
							"shownValue" : "Booth number",
							"key"				 : "booth",
							"value"			 : scope.exhibitor.booth
						},
					];

          scope.qualifierTypes = [
            {
              "name": "Answer Type: Freeform Text",
              "value": 1
            },
            {
              "name": "Answer Type: Single Select",
              "value": 2
            },
            {
              "name": "Answer Type: Multiple Select",
              "value": 3
            }
          ]

          scope.qualifierModel = {
            "deleted_at"                  : null,
            "exhibitor"                   : scope.exhibitor.id,
            "exhibitorquestionoption_set" : [
              {
                "value": "",
                "order": 1
              },
              {
                "value": "",
                "order": 1
              }
            ],
            "order"                       : 1,
            "text"                        : "",
            "type"                        : 1
          }

          getUsersForCurrentExhibitor(scope.exhibitor.id)
          getExhibitorQualifiers(scope.exhibitor.id);

          scope.closeModal = function() {
            scope.closeThisDialog()
          };

          scope.validateEmail = function(emailAddress) {
            scope.emailToAddIsValid = checkEmailValidity(emailAddress);
          }

          scope.translateTo = function(value) {
            scope.translateValue = value;
          }

          scope.saveExhibitor = function(model, closeModal) {
            var errorOnMandatoryFields = false;
            var errorOnEmailAddress    = false;
            for (var i = 0; i <  model.length; i++) {
              var field = model[i];
              if (field.mandatory && !field.value) {
                toastr.error(field.shownValue + " is a required field. Please input a value!");
                errorOnMandatoryFields = true;
                break;
              } else if (field.key === "booth_contact_email" && field.value) {
                errorOnEmailAddress = checkEmailValidity(field.value);
                break;
              }
            };

            if (!errorOnEmailAddress && !errorOnMandatoryFields) {
              var conferenceId = $rootScope.conference.conference_id;


              addExhibitorModalDataService.createExhibitor(conferenceId, model).then(function(response) {
                $scope.exhibitors = angular.copy(response.exhibitor_set);
              }, function(error) {

              });

              if (closeModal) {
                scope.closeThisDialog();
              }
            }
          };

					scope.activateEditMode = function() {
						scope.inEdit = true;
            scope.translateValue = 0;
					}

					scope.exitEditMode = function() {
						scope.inEdit = false;
					}

					scope.updateExhibitor = function(modelValues) {
						angular.forEach(modelValues, function(field) {
							if (field.key) {
								scope.exhibitor[field.key] = field.value;
							}
						});

						exhibitorsDetailsModalDataService.editExhibitor(scope.exhibitor).then(function(response) {
              toastr.success("Exhibitor data was updated successfully!");
              $rootScope.$broadcast('UPDATE_EDIT_EXHIBITORS_LIST', response);
              scope.closeThisDialog();
						}, function(error) {
						})

					}

          scope.togglePermissionsPanel = function() {
            scope.permissionsPanelVisible = !scope.permissionsPanelVisible;
          }

          scope.togglePermissionValue = function($event, permission) {
            $event.preventDefault();
            $event.stopPropagation();

            permission.value = !permission.value;
          }

          scope.toggleUserPermissionsPanel = function($event, user) {
            $event.preventDefault();
            $event.stopPropagation();

            user.permissionsPanelVisible = !user.permissionsPanelVisible;
          }

          scope.togglePermissionValueForUser = function($event, permission, user) {
            $event.stopPropagation()
            user[permission.key] = !user[permission.key];

            var permissionsArray = [];
            var obj              = {
              "exhibitor_user_id": user.id,
              "rights" : []
            };

            angular.forEach(scope.userPermissions, function(permission) {
              if (user[permission.key]) {
                permissionsArray.push(permission.key);
              }
            });
            obj.rights = permissionsArray;

            exhibitorConferenceUserDataService.editOne(obj).then(function(result) {
              toastr.success("Updated user rights with success!");
            }, function(error) {
              toastr.error("An error has occured while trying to update the rights!");
            });
          }

          scope.deleteUser = function(user) {
            var arr = [];
            arr.push(user.id);
            exhibitorConferenceUserDataService.deleteUsers(scope.exhibitor.id, arr).then(function(response) {
              for (var i = 0; i < scope.users.length; i++) {
                if (scope.users[i].id === user.id) {
                  scope.users.splice(i, 1);
                }
              }
            }, function(error) {
              toastr.error("An error has occured! Please try again later!");
            })
          }

          scope.addQualifier = function() {
            var qualifierModelCopy = angular.copy(scope.qualifierModel);
            scope.qualifiers.push(qualifierModelCopy);
          }

          scope.deleteQualifier = function(qualifierIndex) {
            scope.qualifiers.splice(qualifierIndex, 1);
          }

          scope.saveQualifiers = function(qualifiers) {
            // sanitize the model if is freeform text (should not have options that are sent to server)
            for (var i = 0; i < qualifiers.length; i++) {
              if (qualifiers[i].type === 1) {
                qualifiers[i].exhibitorquestionoption_set = [];
              }
            }
            superadminExhibitorDetailsModalDataService.saveQualifiers(scope.exhibitor.id, qualifiers).then(function(response) {
              toastr.success("Qualifiers updated successfully!");
            }, function(error) {
              toastr.error("An error has occured! Please make sure that all the inputs are filled!");
            });
          }

          scope.addNewOption = function(qualifier) {
            var obj = {
              "value": "",
              "order": 1
            };
            qualifier.exhibitorquestionoption_set.push(obj);
          }

          scope.removeOption = function(qualifier, index) {
            if (qualifier.exhibitorquestionoption_set.length > 2) {
              qualifier.exhibitorquestionoption_set.splice(index, 1);
            }
          }

          scope.addUser = function(userEmail) {
            superadminExhibitorDetailsModalDataService.addUser(scope.exhibitor.id, userEmail, scope.userPermissions).then(function(response) {
              scope.users.push(response);
              scope.userEmailToAdd          = "";
              scope.emailToAddIsValid       = false;
              scope.permissionsPanelVisible = false;
            }, function(error) {
              toastr.error(error.detail);
            });
          }

          var checkEmailValidity = function(emailAddress) {
            var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return regexExpression.test(emailAddress);
          };

          function getUsersForCurrentExhibitor(exhibitorId) {
            superadminExhibitorDetailsModalDataService.getUsersForCurrentExhibitor(exhibitorId).then(function(response) {
              scope.users = response;

            }, function(error) {
              toastr.error("An error has occured while trying to fetch the users!");
            });
          }

          function getExhibitorQualifiers(exhibitorId) {
            superadminExhibitorDetailsModalDataService.getExhibitorQualifiers(exhibitorId).then(function(response) {
              scope.qualifiers = response;
            }, function(error) {
              toastr.error("An error has occured while trying to fetch the users!");
            });
          }
        }
    }
})();
