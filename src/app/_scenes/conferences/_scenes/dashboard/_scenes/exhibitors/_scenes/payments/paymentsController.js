(function () {
    'use strict';

    var controllerId = 'exhibitorPaymentsController';
		var app 				 = angular.module("app");

		var dependenciesArray = [
      '$rootScope',
			'$scope',
			'$stateParams',
			'$localStorage',
			'SharedProperties',
			'attendeesDataService',
			'toastr',
      '$timeout',
      'ngDialog',
      'exhibitorPaymentsDataService',
			exhibitorPaymentsController
		];

		app.controller(controllerId, dependenciesArray);

    function exhibitorPaymentsController($rootScope, $scope, $stateParams, $localStorage, SharedProperties, attendeesDataService, toastr, $timeout, ngDialog, exhibitorPaymentsDataService) {
      var paymentTypeInteger = 1;

      $scope.conference         = angular.copy($rootScope.conference);
      $scope.costPerCode        = 0;
      $scope.numberOfPromoCodes = 0;
      $scope.markUpCost         = $scope.conference.markup_fee;
      $scope.paymentsTypes = {
        1: 'standard',
        2: 'advanced',
        3: 'conference'
      };
      $scope.currentActivePaymentType           = $scope.paymentsTypes[$rootScope.conference.payment_type];
      $scope.numberOfRedeemedCodes              = calculateNumberOfRedeemedPromoCodes($scope.conference.promo_codes);
      $scope.currentNumberOfPromoCodesGenerated = $scope.conference.promo_codes.length || 0;
      $timeout(function() {
        $('.half.action.payments').mCustomScrollbar({'theme': 'minimal-dark'});
      });



      $scope.checkStandardPayment = function() {
        paymentTypeInteger = 1;
        exhibitorPaymentsDataService.updatePaymentType($scope.conference, paymentTypeInteger).then(function(response) {
          toastr.success("Your payment type has been successfully updated");
          $scope.currentActivePaymentType = 'standard';
        })
      }
      $scope.checkAdvancedPayment = function(cost) {
        paymentTypeInteger = 2;
        $scope.conference.markup_fee = cost;
        exhibitorPaymentsDataService.updatePaymentType($scope.conference, paymentTypeInteger).then(function(response) {
          toastr.success("Your payment type has been successfully updated");
          $scope.currentActivePaymentType = 'advanced';
        }, function(error) {
          if (error.status === 403) {
            toastr.error(error.data.detail);
          }
        })
      }
      $scope.checkConferencePayment = function() {
        paymentTypeInteger = 3;
        if (!$scope.conference.business_data.has_registered_payment) {
          ngDialog.open({
            plain       : true,
            template    : '<credit-card-modal></credit-card-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
            data        : {fromExhibitorPayments: true}
          });
        } else {
          exhibitorPaymentsDataService.updatePaymentType($scope.conference, paymentTypeInteger).then(function(response) {
            toastr.success("Your payment type has been successfully updated");
            $scope.currentActivePaymentType = 'conference';
          }, function(error) {
            if (error.status === 403) {
              toastr.error(error.data.detail);
            }
          })
        }
      }

      $scope.openCreditCardModal = function($event) {
        $event.stopPropagation();
        ngDialog.open({
          plain       : true,
          template    : '<credit-card-modal></credit-card-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
          data        : {fromExhibitorPayments: true}
        });
      }

      $scope.openCreditCardModalForPromoCodes = function($event) {
        $event.stopPropagation();
        if ($scope.numberOfPromoCodes < 1) {
          toastr.error("Quantity of promo codes to purchase should be at least 1!");
          return;
        }
        if ($scope.costPerCode < 1) {
          toastr.error("Price of promo code should be at least $1 !");
          return
        }
        ngDialog.open({
          plain       : true,
          template    : '<credit-card-modal></credit-card-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
          data        : {fromExhibitorPromoCodes: true}
        });
      }

      $scope.openPromoCodesModal = function($event) {
        $event.stopPropagation();
        ngDialog.open({
          plain       : true,
          template    : '<promo-codes-status-modal></promo-codes-status-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
          data        : {
            promoCodes: $scope.conference.promo_codes
          }
        });
      }

      function calculateNumberOfRedeemedPromoCodes(codes) {
        var count = 0;

        angular.forEach(codes, function(code) {
          if (code.exhibitor) {
            count++;
          }
        });

        return count;
      }


      $rootScope.$on('PAYMENT_METHOD_FOR_EXHIBITOR_PAYMENTS_ACCEPTED', function(event, data) {
        exhibitorPaymentsDataService.addPaymentMethod(data.id).then(function(response) {
          exhibitorPaymentsDataService.updatePaymentType($scope.conference, paymentTypeInteger).then(function(response) {
            toastr.success("Your payment type has been successfully updated");
            $scope.currentActivePaymentType = 'conference';
            $scope.conference.conference_charges.last4 = response.last4;
          })
        });
      });

      $rootScope.$on('PAYMENT_METHOD_FOR_EXHIBITOR_PROMO_CODES_ACCEPTED', function(event, data) {
          ngDialog.open({
            plain       : true,
            template    : '<promo-codes-purchase-modal></promo-codes-purchase-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
            data        : {
              numberOfPromoCodes : $scope.numberOfPromoCodes,
              costPerCode        : $scope.costPerCode,
              stripeToken        : data.id
            }
          });
      });

      $rootScope.$on('SUCCESSFULLY_PURCHASED_PROMO_CODES', function(event, data) {
        for (var i = 0; i < data.promoCodes.length; i++) {
          $rootScope.conference.promo_codes.push(data.promoCodes[i]);
          $scope.conference.promo_codes.push(data.promoCodes[i]);
        }
        $scope.currentNumberOfPromoCodesGenerated = $scope.conference.promo_codes.length;
        $scope.costPerCode        = 0;
        $scope.numberOfPromoCodes = 0;
      });

    }
})();
