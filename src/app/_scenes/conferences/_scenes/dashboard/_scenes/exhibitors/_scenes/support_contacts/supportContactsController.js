(function () {
    'use strict';

    var controllerId      = 'supportContactsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'conferenceExhibitorSupportContactsDataService',
      supportContactsController
    ]

    app.controller(controllerId, dependenciesArray);

    function supportContactsController($scope, $rootScope, $stateParams, toastr, ngDialog, conferenceExhibitorSupportContactsDataService) {
			var conference  = $rootScope.conference;
			$scope.contacts = conference.conference_exhibitor_support_contacts;
      $scope.searchContact = '';

      $scope.deleteContact = function($event, id) {
        $event.stopPropagation();
        $event.preventDefault();
        ngDialog.open({
          "plain": true,
          "template": "<delete-exhibitor-support-contact-modal></delete-exhibitor-support-contact-modal>",
          "className"   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
          "data": {
            "conferenceId" : conference.conference_id,
            "exhibitorId"  : id
          }
        });
      };

      $scope.addContact = function() {
        ngDialog.open({
          "plain": true,
          "template": "<add-exhibitor-support-contact-modal></add-exhibitor-support-contact-modal>",
          "className"   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
        });
      }

      $scope.openContactDetails = function(contact) {
        if($rootScope.userPermissions.manageExhibitors) {
          ngDialog.open({
            "plain"       : true,
            "template"    : "<edit-exhibitor-support-contact-modal></edit-exhibitor-support-contact-modal>",
            "className"   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss',
            "data": {
              "conferenceId" : conference.conference_id,
              "contact"      : contact
            }
          });
        }
        else{
          toastr.error("You don\'t have permissions to perform this action");
        }
      }

      $rootScope.$on('DELETED_EXHIBITOR_SUPPORT_CONTACT', function(event, data) {
        $scope.contacts = angular.forEach($scope.contacts, function(contact, index) {
          if (contact.id === data.exhibitorId) {
            $scope.contacts.splice(index, 1);
          }
        });
      });

      $rootScope.$on('ADDED_EXHIBITOR_SUPPORT_CONTACT', function(event, data) {
        $scope.contacts.push(data);
      });

      $rootScope.$on('EDITED_EXHIBITOR_SUPPORT_CONTACT', function(event, data) {
        for (var i = 0; i < $scope.contacts.length; i++) {
          if ($scope.contacts[i].id === data.id) {
            $scope.contacts[i] = data;
          }
        }
      });
    }
})();
