(function () {
    'use strict'

    var directiveId = 'exhibitorDetailsModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
      'exhibitorsDetailsModalDataService',
			'$timeout',
			exhibitorDetailsModal
		];

		app.directive(directiveId, dependenciesArray);

    function exhibitorDetailsModal($rootScope, $q, toastr, exhibitorsDetailsModalDataService, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_components/exhibitorDetailsModal/exhibitorDetailsModalView.html',
            link: link
        }

        function link(scope) {
					scope.exhibitor = angular.copy(scope.ngDialogData);
          scope.errorOnEmailAddress = false;
					$timeout(function () {
						$('.custom-modal-wrapper.add-exhibitor .bottom-part').mCustomScrollbar({'theme': 'minimal-dark'});
					}, 0);

          scope.modelForView = [
						{
							shownValue : "Exhibitor Admin",
							value 		 : scope.exhibitor.admin ? scope.exhibitor.admin.first_name + " " + scope.exhibitor.admin.last_name : ""
						},
						{
							shownValue : "Email Address",
							value 		 : scope.exhibitor.admin_email
						},
						{
							shownValue : "Phone Number",
							value 		 : scope.exhibitor.phone_number
						},
						{
							shownValue : "Street Address",
							value 		 : scope.exhibitor.street_address
						},
						{
							shownValue : "City",
							value 		 : scope.exhibitor.city
						},
						{
							shownValue : "State",
							value 		 : scope.exhibitor.state
						},
						{
							shownValue : "Zip Code",
							value 		 : scope.exhibitor.zip_code
						},
						{
							shownValue : "Country",
							value 		 : scope.exhibitor.country
						},
						{
							shownValue : "Booth Contact",
							value 		 : (scope.exhibitor.booth_contact_first_name||"") + " " + (scope.exhibitor.booth_contact_last_name||"")
						},
						{
							shownValue : "Booth Contact Email",
							value 		 : scope.exhibitor.booth_contact_email
						},
						{
							shownValue : "Booth Number",
							value 		 : scope.exhibitor.booth
						},
						{
							shownValue : "Users",
							value 		 : scope.exhibitor.users_count
						},
						{
							shownValue : "Promo Code",
							value 		 : scope.exhibitor.promo_code ? scope.exhibitor.promo_code : "No promo code issued."
						}
					];

					scope.modelForEdit = [
						{
							"shownValue" : "Company Name",
							"key"				 : "name",
							"value"			 : scope.exhibitor.name,
						},
						{
							"shownValue" : "Admin Name",
							"value"			 : scope.exhibitor.admin ? scope.exhibitor.admin.first_name + " " + scope.exhibitor.admin.last_name : "Admin has not joined yet.",
						},
						{
							"shownValue" : "Company Email",
							"value"			 : scope.exhibitor.admin_email
						},
						{
							"shownValue" : "Phone number",
							"value"			 : scope.exhibitor.phone_number
						},
						{
							"shownValue" : "Street Address",
							"value"			 : scope.exhibitor.street_address
						},
						{
							"shownValue" : "City",
							"value"			 : scope.exhibitor.city
						},
						{
							"shownValue" : "Zip Code",
							"value"			 : scope.exhibitor.zip_code
						},
						{
							"shownValue" : "State",
							"value"			 : scope.exhibitor.state || "--"
						},
						{
							"shownValue" : "Country",
							"value"			 : scope.exhibitor.country
						},
						{
							"shownValue" : "Booth Contact First Name",
							"key"				 : "booth_contact_first_name",
							"value"			 : scope.exhibitor.booth_contact_first_name
						},
						{
							"shownValue" : "Booth Contact Last Name",
							"key"				 : "booth_contact_last_name",
							"value"			 : scope.exhibitor.booth_contact_last_name
						},
						{
							"shownValue" : "Booth Contact Email",
							"key"				 : "booth_contact_email",
							"value"			 : scope.exhibitor.booth_contact_email
						},
						{
							"shownValue" : "Booth number",
							"key"				 : "booth",
							"value"			 : scope.exhibitor.booth
						},
					];

          scope.closeModal = function() {
            scope.closeThisDialog()
          };

          scope.saveExhibitor = function(model, closeModal) {
            var errorOnMandatoryFields = false;
            var errorOnEmailAddress    = false;
            for (var i = 0; i <  model.length; i++) {
              var field = model[i];
              if (field.mandatory && !field.value) {
                toastr.error(field.shownValue + " is a required field. Please input a value!");
                errorOnMandatoryFields = true;
                break;
              } else if (field.key === "booth_contact_email" && field.value) {
                errorOnEmailAddress = checkEmailValidity(field.value);
                break;
              }
            };

            if (!errorOnEmailAddress && !errorOnMandatoryFields) {
              var conferenceId = $rootScope.conference.conference_id;


              addExhibitorModalDataService.createExhibitor(conferenceId, model).then(function(response) {
                $scope.exhibitors = angular.copy(response.exhibitor_set);
              }, function(error) {

              });

              if (closeModal) {
                scope.closeThisDialog();
              }
            }
          };

          var checkEmailValidity = function(emailAddress) {
            var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            scope.errorOnEmailAddress = !(regexExpression.test(emailAddress));
            if (scope.errorOnEmailAddress) {
              toastr.error("Email address is not valid!");
            }
            return scope.errorOnEmailAddress;
          };

					scope.activateEditMode = function() {
						scope.inEdit = true;
					}

					scope.exitEditMode = function() {
						scope.inEdit = false;
					}

					scope.updateExhibitor = function(modelValues) {
						angular.forEach(modelValues, function(field) {
							if (field.key) {
								scope.exhibitor[field.key] = field.value;
							}
						});

						exhibitorsDetailsModalDataService.editExhibitor(scope.exhibitor).then(function(response) {
              toastr.success("Exhibitor data was updated successfully!");
              $rootScope.$broadcast('UPDATE_EDIT_EXHIBITORS_LIST', response);
              scope.closeThisDialog();
						}, function(error) {
						})

					}
        }
    }
})();
