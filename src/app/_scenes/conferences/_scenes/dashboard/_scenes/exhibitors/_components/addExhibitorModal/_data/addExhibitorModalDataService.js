(function(){
	'use strict';

    var serviceId 				= 'addExhibitorModalDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			addExhibitorModalDataService
		];

		app.service(serviceId, dependenciesArray);

		function addExhibitorModalDataService($q, requestFactory) {
	    var service = {};
			service.createExhibitor = function(conferenceId, model) {
				var deferred 			= $q.defer();
				var obj 					= {};
				var data 					= {
					"exhibitor_set": []
				};

				var url = "api/v1.1/exhibitor_companies/" + conferenceId + "/";

				angular.forEach(model, function(field) {
					obj[field.key] = field.value;
				});

				data.exhibitor_set.push(obj);

				requestFactory.post(url, data, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}
      return service;
    }
})();
