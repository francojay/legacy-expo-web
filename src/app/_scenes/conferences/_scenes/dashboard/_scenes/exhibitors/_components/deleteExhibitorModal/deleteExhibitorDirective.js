(function(){
    'use strict';

    var directiveId = 'deleteConferenceExhibitorsModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', deleteConferenceExhibitorsModal]);

    function deleteConferenceExhibitorsModal ($rootScope){

        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_components/deleteExhibitorModal/deleteExhibitorView.html',
            link: link
        };

        function link(scope){

          scope.numberOfExhibitors = scope.ngDialogData.numberOfExhibitors;

          scope.deleteEvent = function() {
            $rootScope.$emit('DELETE_CONFERENCE_EXHIBITORS');
          }

          // scope.deleteAllEvent = function() {
          //   $rootScope.$emit('DELETE_ALL_CONFERENCE_EXHIBITORS');
          // }



        }

    }
})();
