(function() {
	'use strict';

  var directiveId = 'promoCodesStatusModal';
	var app 				= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'$state',
		'$stateParams',
		'ngDialog',
		'toastr',
		'API',
		'exhibitorPaymentsDataService',
		'$timeout',
		promoCodesStatusModalDirective
	];

	app.directive(directiveId, dependenciesArray);

	function promoCodesStatusModalDirective($rootScope, $state, $stateParams, ngDialog, toastr, API, exhibitorPaymentsDataService, $timeout) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/payments/_components/promoCodesStatusModal/promoCodesStatusModalView.html',
			replace: true,
			link: link
		}

    function link(scope) {
			scope.promoCodes = scope.ngDialogData.promoCodes;
			scope.checkedPromoCodes = [];

			scope.checkCode = function(code) {
				code.checked = !code.checked;

				if (code.checked) {
					scope.checkedPromoCodes.push(code);
				}
			}

			scope.checkAllPromoCodes = function() {
				var selectAllPromoCodes = false;
	      if(scope.checkedPromoCodes.length != scope.promoCodes.length){
	        selectAllPromoCodes = true;
	        scope.checkedPromoCodes = scope.promoCodes;
	      }
	      else{
	        scope.checkedPromoCodes = [];
	      }
				angular.forEach(scope.promoCodes, function(code) {
					code.checked = selectAllPromoCodes;
				});
				//scope.checkedPromoCodes = scope.promoCodes;
			}

			scope.printPromoCodes = function(codes) {
				if (codes.length > 0) {
					lib.printTransactionsAsCsv(codes);
				} else {
					toastr.error("Please select at least one code to download!");
				}
			}

			scope.downloadPromoCodes = function(codes) {
				if (codes.length > 0) {
					lib.downloadTransactionsAsCsv(codes, $rootScope.conference);
				} else {
					toastr.error("Please select at least one code to print!");
				}
			}

			$timeout(function() {
        $('.custom-modal-wrapper.add-exhibitor.promo-codes .promo-codes-list-wrapper .list').mCustomScrollbar({'theme': 'minimal-dark'});
      });
    }
	}

}());
