(function() {
	'use strict';

  var directiveId = 'promoCodesPurchaseModal';
	var app 				= angular.module('app');
	var dependenciesArray = [
		'$rootScope',
		'$state',
		'$stateParams',
		'ngDialog',
		'toastr',
		'API',
		'exhibitorPaymentsDataService',
		promoCodesPurchaseModalDirective
	];

	app.directive(directiveId, dependenciesArray);
	function promoCodesPurchaseModalDirective($rootScope, $state, $stateParams, ngDialog, toastr, API, exhibitorPaymentsDataService) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/payments/_components/promoCodesPurchaseModal/promoCodesPurchaseModalView.html',
			replace: true,
			link: link
		}

    function link(scope) {
			$rootScope.pageBlurred = true;

			scope.numberOfPromoCodes = scope.ngDialogData.numberOfPromoCodes;
			scope.costPerCode 			 = scope.ngDialogData.costPerCode;
			scope.stripeToken 			 = scope.ngDialogData.stripeToken;

			scope.closeModal = function() {
				scope.closeThisDialog()
			}

			scope.purchasePromoCodes = function(number, cost) {
				if (!number || number < 1) {
					toastr.error("Quantity of promo codes to purchase should be at least 1!");
					return;
				}
				if (!cost || cost < 1) {
					toastr.error("Price of promo code should be at least $1 !");
					return;
				}
				exhibitorPaymentsDataService.purchasePromoCodes($rootScope.conference.conference_id, number, cost, scope.stripeToken).then(function(response) {
					toastr.success("You successfully bought " + scope.numberOfPromoCodes + " promo codes, each with a value of: $" + scope.costPerCode);
					$rootScope.$broadcast("SUCCESSFULLY_PURCHASED_PROMO_CODES", { promoCodes: response.promo_codes });
					ngDialog.closeAll();
				}, function(error) {
					toastr.error("An error has occured while trying to register your order. Please try again later.");
					toastr.error(error.data.detail);
				});
			}
    }
	}

}());
