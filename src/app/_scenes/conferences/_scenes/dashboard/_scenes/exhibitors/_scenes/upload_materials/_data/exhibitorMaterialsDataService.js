(function() {
	'use strict';
  var serviceId = 'exhibitorMaterialsDataService';
	var app 			= angular.module('app');
	var dependenciesArray = [
		'$q',
		'$http',
		'$filter',
		'toastr',
		'localStorageService',
		'requestFactory',
		exhibitorMaterialsDataService
	];
	app.service(serviceId, dependenciesArray);
	function exhibitorMaterialsDataService($q, $http, $filter, toastr, localStorageService, requestFactory) {
		var service = {};

		service.assignExhibitorMaterial = function(material) {
			var deferred = $q.defer();
			var url 		 = "api/exhibitor/material_add/";

			requestFactory.post(url, material, false).then(function(response) {
			 deferred.resolve(response.data);
		 	}, function(erorr) {
			 deferred.reject(error);
			})

			return deferred.promise;

		}

		service.removeMaterial = function(materialId) {
			var deferred = $q.defer();
			var url 		 = "api/exhibitor/material_remove/?exhibitor_material_id=" + materialId;

			requestFactory.remove(url, {}, false).then(function(response) {
			 	deferred.resolve(response.data);
		 	}, function(erorr) {
			 	deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
