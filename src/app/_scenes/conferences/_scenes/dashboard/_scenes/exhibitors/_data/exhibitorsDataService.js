(function(){
	'use strict';

    var serviceId 				= 'exhibitorsDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			exhibitorsDataService
		];

		app.service(serviceId, dependenciesArray);

		function exhibitorsDataService($q, requestFactory) {
	    var service = {};

			service.getPaginatedExhibitors = function(conferenceId, page) {
				var deferred = $q.defer();
				var url = "api/v1.1/conferences/" + conferenceId + "/exhibitors?page=" + page;

				requestFactory.get(url, {}, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			service.getFilteredExhibitors = function (conferenceId, page, noLoader, filters) {
				var queryUrl = '';
				noLoader = noLoader || false;
				if(!_.isEmpty(filters)){
					queryUrl = '/api/exhibitor/list'
						+ "?conference_id=" + conferenceId
						+ "&page=" + page
						+ "&sort_by=" + filters.sortBy
						+ "&sort_order=" + filters.sortOrder
						+ "&q=" + filters.queryString;
				} else {
					queryUrl = 'api/exhibitor/list'
						+ "?conference_id=" + conferenceId
						+ "&page" + page;
				}
				var deferred = $q.defer();
				requestFactory.get(
					queryUrl,
					{},
					false,
					noLoader
				).then(function(response){
					deferred.resolve(response.data);
				}, function(error){
					deferred.reject(error);
				})
				return deferred.promise;
			}

			service.bulkUploadExhibitors = function(conferenceId, data) {
				var deferred = $q.defer();

				var url  = "api/v1.1/exhibitor_companies/" + conferenceId + "/";
				var data = {
					"exhibitor_set" : data
				};

				requestFactory.post(url, data, false, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

			service.deleteExhibitors = function(conferenceId, exhibitorIds, deleteAll) {
				var deferred = $q.defer();
				var data 		 = {};
				var url  		 = "api/v1.1/exhibitors/" + conferenceId + "/";

				if (deleteAll) {
					url +="?delete_all=true";
				} else {
					url += "?exhibitor_ids=" + exhibitorIds.toString();
				}
				requestFactory.remove(url, false, false).then(function(response) {
					deferred.resolve(response);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			service.revokeAccessToExhibitors = function(conferenceId, data) {
				var deferred = $q.defer();
				var url = "api/v1.1/exhibitors_revoke/" + conferenceId + "/";

				var data = {
					"exhibitor_ids" : data
				};

				requestFactory.post(url, data, false, false).then(function(response) {
					deferred.resolve(response);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}

      return service;
    }
})();
