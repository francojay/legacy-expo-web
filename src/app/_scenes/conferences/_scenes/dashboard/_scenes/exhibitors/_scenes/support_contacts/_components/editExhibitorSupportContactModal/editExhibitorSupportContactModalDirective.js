(function () {
    'use strict'

    var directiveId       = 'editExhibitorSupportContactModal';
		var app 				      = angular.module('app');
		var dependenciesArray = [
			'$rootScope',
			'conferenceExhibitorSupportContactsDataService',
      'toastr',
      '$timeout',
      '$q',
      'fileService',
      'intlTelInputOptions',
      'API',
			editExhibitorSupportContactModalDirective
		];

		app.directive(directiveId, dependenciesArray);

    function editExhibitorSupportContactModalDirective($rootScope, conferenceExhibitorSupportContactsDataService, toastr, $timeout, $q, fileService, intlTelInputOptions, API) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/support_contacts/_components/editExhibitorSupportContactModal/editExhibitorSupportContactModalView.html',
            link: link
        }

        function link(scope) {
          var conferenceId 			= scope.ngDialogData.conferenceId;
          var supportContactId  = scope.ngDialogData.contact.id;
          scope.photoLink       = scope.ngDialogData.contact.photo;
          scope.model           = [
            {
              "shownValue" : "First Name",
              "key"        : "first_name",
              "mandatory"  : true,
              "value"      : scope.ngDialogData.contact.first_name
            },
            {
              "shownValue" : "Last Name",
              "key"        : "last_name",
              "mandatory"  : true,
              "value"      : scope.ngDialogData.contact.last_name
            },
            {
              "shownValue" : "Company Name",
              "key"        : "company_name",
              "mandatory"  : true,
              "value"      : scope.ngDialogData.contact.last_name
            },
            {
              "shownValue" : "Email Address",
              "key"        : "email",
              "mandatory"  : false,
              "value"      : scope.ngDialogData.contact.email
            },
            {
              "shownValue" : "Phone Number",
              "key"        : "phone_number",
              "mandatory"  : false,
              "value"      : scope.ngDialogData.contact.phone_number
            },
            {
              "shownValue" : "Area of Support",
              "key"        : "area_of_support",
              "mandatory"  : true,
              "value"      : scope.ngDialogData.contact.area_of_support
            }
          ];
          scope.defaultCountryFlag = 'us';
          angular.forEach(API.COUNTRIES, function(country) {
            if(country.name === $rootScope.conference.country){
              scope.defaultCountryFlag = country.code.toLowerCase();
            }
          });
          scope.editExhibitorSupportContact = function(model, photo) {
            var errorsFound = false;
            for (var i = 0; i < model.length; i++) {
              var field = model[i];
              if (field.mandatory && !field.value) {
                toastr.error(field.shownValue + " is a mandatory field. Please complete it before saving!");
                field.error = true;
                errorsFound = true;
              } else if (field.mandatory && field.value ) {
                field.error = false;
              } else if (field.key === "email" && field.value) {
                var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                field.error = !regexExpression.test(field.value);
                if (field.error) {
                  errorsFound = true;
                  toastr.error("The Email Address is not valid!");
                }
              }
              else if(field.key == "phone_number" && field.value === undefined){
    							toastr.warning(field.shownValue + " need to have the correct format!");
                  errorsFound = true;
    					}
            }
            if (!errorsFound) {
              conferenceExhibitorSupportContactsDataService.editOne(conferenceId, model, photo, supportContactId).then(function(response) {
                $rootScope.$broadcast("EDITED_EXHIBITOR_SUPPORT_CONTACT", response);
                scope.closeModal();
              }, function(error) {
                console.log(error);
              });
            }
          }


          scope.addSupportContactPhoto      = function(files) {
            if (files) {
              fileService.getUploadSignature(files[0], false).then(function(response) {
                var file_link = response.data.file_link;
                var promises = [];
                promises.push(fileService.uploadFile(files[0], response));
                $q.all(promises).then(function(response){
                  scope.photoLink = file_link;
                }, function(error) {
                  console.log(error);
                });
              }, function(error) {
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                    toastr.error('Error!', 'There has been a problem with your request.');
                }
              });
            }
          }

          scope.closeModal                  = function() {
            scope.closeThisDialog();
          }


        }
    }
})();
