(function(){
	'use strict';

    var filterId = 'supportContactSearchFilter';

    angular
  		.module('app')
  		.filter(filterId, function() {

        return function (exhibitorSupportContacts, support_contact_info) {
            var info = [], i,
                searchedWordMatch = new RegExp(support_contact_info, 'i');
            for (i = 0; i < exhibitorSupportContacts.length; i++) {
                var lastName = exhibitorSupportContacts[i].last_name,
                    firstName = exhibitorSupportContacts[i].first_name,
                    email = exhibitorSupportContacts[i].email;
                if (lastName == null) {
                    lastName = '';
                }
                if (email === null) {
                    email = '';
                }
                if (searchedWordMatch.test(firstName.substr(0, support_contact_info.length)) ||
                    searchedWordMatch.test(lastName.substr(0, support_contact_info.length)) ||
                    searchedWordMatch.test(email.substr(0, support_contact_info.length))) {

                    info.push(exhibitorSupportContacts[i]);
                }
            }
            return info;
        };

      });
})();
