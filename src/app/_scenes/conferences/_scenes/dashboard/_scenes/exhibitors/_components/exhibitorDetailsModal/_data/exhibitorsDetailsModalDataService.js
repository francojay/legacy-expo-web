(function(){
	'use strict';

    var serviceId 				= 'exhibitorsDetailsModalDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			exhibitorsDetailsModalDataService
		];

		app.service(serviceId, dependenciesArray);

		function exhibitorsDetailsModalDataService($q, requestFactory) {
	    var service = {};
			service.editExhibitor = function(exhibitor) {
				var deferred 			= $q.defer();

				var url = "api/v1.1/exhibitor_actions/" +  exhibitor.id + "/";



				requestFactory.post(url, exhibitor, false).then(function(response) {
					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}
      return service;
    }
})();
