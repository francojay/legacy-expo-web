(function () {
    'use strict';

    var controllerId      = 'exhibitingDetailsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      '$timeout',
      '$window',
      '$interval',
      '$state',
      'exhibitingDetailsDataService',
      exhibitingDetailsController
    ]

    app.controller(controllerId, dependenciesArray);

    function exhibitingDetailsController($scope, $rootScope, $stateParams, toastr, ngDialog, $timeout, $window, $interval, $state, exhibitingDetailsDataService) {
      var vm = this;
      vm.conference = $rootScope.conference;

      // TODO : This will need to be redone ASAP because it is at least the dumbest shit i've ever seen
      vm.createMomentFromRawTime = function(rawDatetime) {
          if (!rawDatetime || !rawDatetime.year) {
              return null;
          }
          return moment(new Date(
              rawDatetime.year,
              rawDatetime.month - 1,
              rawDatetime.day,
              rawDatetime.hour,
              rawDatetime.minute
          ));
      };
      vm.conference.date_from = new Date(vm.conference.date_from);
      vm.conference.date_to = new Date(vm.conference.date_to);
      vm.conference_date_from_moment = moment(vm.conference.date_from);
      vm.conference_date_to_moment = moment(vm.conference.date_to);
      vm.conference.date_from_editable = vm.createMomentFromRawTime(vm.conference.date_from_raw);
      vm.conference.date_to_editable = vm.createMomentFromRawTime(vm.conference.date_to_raw);
      vm.conferenceDays = [];
      var test_starts_at  = _.clone(vm.conference.date_from);

      if (!vm.conference.time_zone) {
          vm.conference.time_zone = "-0500"; // New york time
      } else if((vm.conference.time_zone[0] != '-') && (vm.conference.time_zone[0] != '+')){
          vm.conference.time_zone = '+' + vm.conference.time_zone;
      }

      var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);

      var test_ends_at  = _.clone(vm.conference.date_to);
      test_starts_at.setTime( test_starts_at.getTime() + (test_starts_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );
      test_ends_at.setTime( test_ends_at.getTime() + (test_ends_at.getTimezoneOffset() + offsetInMinutes)*60*1000 );

      var today = new Date();
      today.setTime( today.getTime() + (today.getTimezoneOffset() + offsetInMinutes)*60*1000 );
      today.setHours(0,0,0,0);

      vm.currentExhibitingDayDate = null;
      vm.currentExhibitingDayNr = 0;

      if (today <= test_ends_at && today >= test_starts_at) {
          for (var iteratedDay = test_starts_at; iteratedDay <= test_ends_at; iteratedDay.setDate(iteratedDay.getDate() + 1)) {
              iteratedDay.setHours(0, 0, 0, 0);

              vm.conferenceDays.push(new Date(iteratedDay));

              var currentIdx = vm.conferenceDays.length;
              var newD = _.clone(iteratedDay);
              var x = _.clone(newD);
              var y = _.clone(new Date(newD.setDate(newD.getDate() + 1)));
              var z = _.clone(x);
              var t = _.clone(y);
              z.setHours(0,0,0,0);
              t.setHours(0,0,0,0);
              if (today > z && today <= t) {
                  vm.currentExhibitingDayDate = y;
                  vm.currentExhibitingDayNr = currentIdx;
              }
          }
      } else {
          for (var iteratedDay = test_starts_at; iteratedDay <= test_ends_at; iteratedDay.setDate(iteratedDay.getDate() + 1)) {
              iteratedDay.setHours(0, 0, 0, 0);
              vm.conferenceDays.push(new Date(iteratedDay));
          }
      }

      if (vm.currentExhibitingDayDate) {
          vm.sessionSelectedDay = vm.currentExhibitingDayDate.getTime();
      } else {
          vm.sessionSelectedDay = vm.conferenceDays[0].getTime();
      }
      vm.exhibitingDetails = [];
      vm.exhibitingDays = {};
      vm.exhibitingInfo = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          room: null,
          description: null
      };
      vm.moveDetails = false;
      vm.moveInfo = [];
      vm.moveDays = {};

      vm.moveInDetails = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          room: null,
          description: null
      };
      vm.showMoveIn = false;
      vm.showMoveOut = false;
      vm.moveOutDetails = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          room: null,
          description: null
      };
      var auxDate = new Date(vm.sessionSelectedDay);
      vm.currentExhibitingDay = auxDate.getDate() + '/' + (auxDate.getMonth()+1) + '/' + auxDate.getFullYear();
      vm.exhibitingDays[vm.currentExhibitingDay] = [];
      vm.exhibitingMoveDays = {};


      var today = new Date();
      var starts_at_date = vm.moveInDetails.starts_at_date;
      if (!starts_at_date) {
          starts_at_date = new Date(vm.conference.date_from);
      }
      var currentDay = starts_at_date.getDate();
      var currentMonth = starts_at_date.getMonth();
      var currentYear = starts_at_date.getFullYear();
      vm.defaultHourStart = new Date(currentYear, currentMonth, currentDay, 12, 0, 0);
      vm.defaultHourEnd = new Date(currentYear, currentMonth, currentDay, 13, 0, 0);

      vm.moveInHours = [];
      vm.moveOutHours = [];
      vm.conferenceHours = [];
      _.each(vm.conference.hours, function(conferenceHour) {

          conferenceHour.starts_at_editable = vm.createMomentFromRawTime(
              conferenceHour.starts_at_raw);
          conferenceHour.starts_at_editable_end = moment(
              conferenceHour.starts_at_editable).endOf('day');
          conferenceHour.ends_at_editable = vm.createMomentFromRawTime(
              conferenceHour.ends_at_raw);

          vm.conferenceHours.push(conferenceHour);
      });

      _.each(vm.conferenceHours, function(conferenceHour) {
          var dateData = _.cloneDeep(new Date(conferenceHour.starts_at));
          var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
          dateData.setTime( dateData.getTime() + (dateData.getTimezoneOffset() + offsetInMinutes)*60*1000 );
          var day = dateData.getDate();
          var month = dateData.getMonth()+1;
          var year = dateData.getFullYear();

          if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
              vm.exhibitingDays[day + '/' + month + '/' + year] = [];
          }
          conferenceHour.starts_at = new Date(conferenceHour.starts_at);
          conferenceHour.ends_at = new Date(conferenceHour.ends_at);
          conferenceHour.exhibiting_day = moment(dateData).set({ hour: 0, minute: 0 });

          if (conferenceHour.type == 1) {
              vm.exhibitingDays[day + '/' + month + '/' + year].push(conferenceHour);
          }
          else if (conferenceHour.type == 2) {
              vm.moveInHours.push(conferenceHour);
          }
          else if (conferenceHour.type == 3) {
              vm.moveOutHours.push(conferenceHour);
          }
      });
      setTimeout(function(){
        var scrollLeft = $('#days-list-scroll').find('.active').offset().left;
        $("#days-list-scroll").animate({
          scrollLeft: scrollLeft
        }, 300);
      },300);

      vm.updateHourTime = function(room) {
          var foundItem = null;

          var starts_at_editable_date = new Date(room.starts_at_editable);
          room.starts_at_editable_end = moment(room.starts_at_editable).endOf('day');

          $timeout(function() {
              if (room.ends_at_editable <= room.starts_at_editable) {
                  room.ends_at_editable = moment(room.starts_at_editable).add(1, 'hour');
              }
          }, 100);
          if (room.id) {
              var index = _.findIndex(vm.conferenceHours, _.pick(room, 'id'));
              if( index !== -1) {
                  vm.conferenceHours.splice(index, 1, room);
              }
          } else {
              var index = vm.conferenceHours.indexOf(room);
              if (index > -1) {
                  vm.conferenceHours[index] = room;
              }
          }
          var index = vm.conferenceHours.indexOf(room);

          if (index == -1) {
              vm.conferenceHours.push(room);
          }
      }

      vm.addExhibitingMoveDetails = function(){
          if (false && (!vm.moveInDetails || !vm.moveInDetails.starts_at_date || !vm.moveInDetails.location)) {

          } else {
              vm.moveInDetails.type = 2;
              vm.moveInDetails.title = "move in hour";

              vm.conferenceHours.push(vm.moveInDetails);
              vm.moveInHours.push(vm.moveInDetails);

              vm.moveInDetails = {
                  starts_at: vm.defaultHourStart,
                  ends_at: vm.defaultHourEnd,
                  starts_at_editable: moment(vm.defaultHourStart),
                  ends_at_editable: moment(vm.defaultHourEnd),
                  room: null,
                  description: null
              };

              vm.showMoveIn = false;
          }
      }

      // Adds a move out hour to the dataset
      vm.addExhibitingMoveOutDetails = function() {
          if (false && (!vm.moveOutDetails || !vm.moveOutDetails.starts_at_date || !vm.moveOutDetails.location)) {

          } else {
              vm.moveOutDetails.type = 3;
              vm.moveOutDetails.title = "move in hour";

              vm.conferenceHours.push(vm.moveOutDetails);
              vm.moveOutHours.push(vm.moveOutDetails);

              vm.moveOutDetails = {
                  starts_at: vm.defaultHourStart,
                  ends_at: vm.defaultHourEnd,
                  starts_at_editable: moment(vm.defaultHourStart),
                  ends_at_editable: moment(vm.defaultHourEnd),
                  room: null,
                  description: null
              };

              vm.showMoveOut = false;
          }
      }

      vm.addExhibitingInfoPrev = function() {
          if (!vm.exhibitingInfo || !vm.exhibitingInfo.starts_at || !vm.exhibitingInfo.ends_at
              || !vm.exhibitingInfo.location) {
              vm.exhibitingInfo.complete = false;
          } else {
              vm.exhibitingInfo.complete = true;
          }
      }

      vm.addExhibitingInfo = function(){
          var ok = true;

          if (false && (!vm.exhibitingInfo || !vm.exhibitingInfo.starts_at || !vm.exhibitingInfo.ends_at
              || !vm.exhibitingInfo.location)) {

          } else {
              vm.exhibitingDetails.push(vm.exhibitingInfo);
              vm.exhibitingDays[vm.currentExhibitingDay] = _.concat(vm.exhibitingDays[vm.currentExhibitingDay], vm.exhibitingDetails);

              var currentDay = vm.currentExhibitingDay.split("/")[0];
              var currentMonth = vm.currentExhibitingDay.split("/")[1];
              var currentYear = vm.currentExhibitingDay.split("/")[2];

              var hour_start = vm.exhibitingInfo.starts_at.getHours();
              var minute_start = vm.exhibitingInfo.starts_at.getMinutes();

              var hour_end = vm.exhibitingInfo.ends_at.getHours();
              var minute_end = vm.exhibitingInfo.ends_at.getMinutes();

              var diff = Math.abs(vm.exhibitingInfo.ends_at - vm.exhibitingInfo.starts_at)/1000;

              vm.exhibitingInfo.starts_at = new Date(currentYear, currentMonth, currentDay, hour_start, minute_start, 0);
              vm.exhibitingInfo.ends_at = _.cloneDeep(vm.exhibitingInfo.starts_at);
              vm.exhibitingInfo.ends_at.setSeconds(vm.exhibitingInfo.starts_at.getSeconds() + diff);
              vm.exhibitingInfo.starts_at_editable_end = vm.conference.date_to_editable;
              var date = new Date(vm.sessionSelectedDay);

              vm.exhibitingInfo.exhibiting_day = moment(date).set({ hour: 0, minute: 0 });
              vm.conferenceHours.push(vm.exhibitingInfo);

              vm.exhibitingInfo = {
                  starts_at: vm.defaultHourStart,
                  exhibiting_day: moment(date).set({ hour: 0, minute: 0 }),
                  ends_at: vm.defaultHourEnd,
                  starts_at_editable_end: vm.conference.date_to_editable,
                  room: null,
                  description: null
              };

              vm.exhibitingDetails = [];
              vm.activeMinus = true;
              $scope.exhibitingHourSaved = true;
          }
      }

      vm.addExhibitingMoveOut = function() {

      }

      $rootScope.$on('CurrentExhibitingDay', function(CurrentExhibitingDay, day){
          var currentDate = day;
          var day = currentDate.getDate();
          var month = currentDate.getMonth()+1;
          var year = currentDate.getFullYear();
          vm.currentExhibitingDay = day + '/' + month + '/' + year;
          vm.exhibitingDetails = [];
          if( vm.exhibitingDays.hasOwnProperty(vm.currentExhibitingDay) === false ){
              vm.exhibitingDays[vm.currentExhibitingDay] = [];
          }
      });

      $scope.exhibitingHourSaved = false;

      vm.addExhibitingMoveInButton = true;
      vm.addExhibitingMoveOutButton = true;

      vm.lockSetExhibitingDay = false;

      // When clicking on the save button on the exhibiting info view - it will update the exhibiting hours on the backend
      vm.setExhibitingDay = function(){
          if (vm.exhibitingInfo.location && vm.exhibitingInfo.location != '') {
              vm.addExhibitingInfo();
              vm.addExhibitingHoursButton = false;
          }
          if (vm.moveInDetails && vm.moveInDetails.location && vm.moveInDetails.starts_at_date) {
              vm.addExhibitingMoveDetails();
              vm.addExhibitingMoveInButton = false;
          }
          if (vm.moveOutDetails && vm.moveOutDetails.location && vm.moveOutDetails.starts_at_date) {
              vm.addExhibitingMoveOutDetails();
              vm.addExhibitingMoveOutButton = false;
          }
          vm.lockSetExhibitingDay = true;
          var conferenceHours = [];
          var isCorrect = true;
          _.each(vm.conferenceHours, function(conferenceHour) {
              if (!conferenceHour || !conferenceHour.starts_at_editable || !conferenceHour.ends_at_editable || !conferenceHour.location) {
                if(conferenceHour.type == 2){
                    toastr.error("Please review your details about Move-In hours.");
                    isCorrect = false;
                    return false;
                }
                else if(conferenceHour.type == 3){
                  toastr.error("Please review your details about Move-Out hours.");
                  isCorrect = false;
                  return false;
                }
                else{
                  toastr.error("Please review your details about Exhibiting Hours from tab "+((conferenceHour.starts_at_editable != undefined)?conferenceHour.starts_at_editable.format('dddd M/D'):" where you try to add exhibiting hours")+".");
                  isCorrect = false;
                  return false;
                }
              } else {
                  var starts_at = new Date(conferenceHour.starts_at_editable);
                  var ends_at = new Date(conferenceHour.ends_at_editable);

                  conferenceHour.starts_at_raw = {
                      "year": starts_at.getFullYear(),
                      "month": starts_at.getMonth() + 1,
                      "day": starts_at.getDate(),
                      "hour": starts_at.getHours(),
                      "minute": starts_at.getMinutes(),
                  }
                  conferenceHour.ends_at_raw = {
                      "year": ends_at.getFullYear(),
                      "month": ends_at.getMonth() + 1,
                      "day": ends_at.getDate(),
                      "hour": ends_at.getHours(),
                      "minute": ends_at.getMinutes(),
                  }
                  conferenceHours.push(conferenceHour);
              }
          });
          if(!isCorrect){
            vm.lockSetExhibitingDay = false;
            return false;
          }
          exhibitingDetailsDataService.setConferenceHours(vm.conference.conference_id, conferenceHours)
              .then(function(response){
                  vm.addExhibitingMoveInButton = true;
                  vm.addExhibitingMoveOutButton = true;
                  location.reload();
              }, function(error) {
                vm.lockSetExhibitingDay = false;
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                    toastr.error('Error!', 'There has been a problem with your request.');
                }
              })
      }

      vm.showExhibitingDay = {};

      _.each(vm.conferenceDays, function(day) {
          var dateData = _.cloneDeep(new Date(day));
          var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
          var day = dateData.getDate();
          var month = dateData.getMonth()+1;
          var year = dateData.getFullYear();
          var defaultHourStart = new Date(year, month, day, 12, 0, 0);
          var defaultHourEnd = new Date(year, month, day, 13, 0, 0);
          if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
              vm.showExhibitingDay[day + '/' + month + '/' + year] = true;
          }
          if (!vm.exhibitingDays[day + '/' + month + '/' + year] || vm.exhibitingDays[day + '/' + month + '/' + year].length < 1) {

              vm.exhibitingDays[day + '/' + month + '/' + year] = [
                  {
                      starts_at: vm.defaultHourStart,
                      ends_at: vm.defaultHourEnd,
                      exhibiting_day: moment(dateData).set({ hour: 0, minute: 0 }),
                      ends_at_min: vm.conference.date_from_editable,
                      ends_at_max: vm.conference.date_to_editable,
                      room: null,
                      description: null
                  }
              ];
          }
      });

      _.forOwn(vm.exhibitingDays, function(exhibitingDay, key){
          if (exhibitingDay.length == 0) {
              vm.showExhibitingDay[key] = true;
          } else {
              vm.showExhibitingDay[key] = false;
          }

          if (exhibitingDay.length == 0) {
              exhibitingDay.push({});
          }
      });

      if (vm.moveInHours.length == 0) {
          vm.showMoveIn = true;
          vm.moveInHours.push(
              {
                  starts_at: vm.defaultHourStart,
                  ends_at: vm.defaultHourEnd,
                  ends_at_min: vm.conference.date_from_editable,
                  ends_at_max: vm.conference.date_to_editable,
                  type: 2,
                  room: null,
                  description: null
              }
          );
      }

      if (vm.moveOutHours.length == 0) {
          vm.showMoveOut = true;
          vm.moveOutHours.push(
              {
                  starts_at: vm.defaultHourStart,
                  ends_at: vm.defaultHourEnd,
                  ends_at_min: vm.conference.date_from_editable,
                  ends_at_max: vm.conference.date_to_editable,
                  type: 3,
                  room: null,
                  description: null
              }
          );
      }

      vm.moveInOutHours = {
          min: lib.format_date_for_validation(vm.conference.date_from),
          max: lib.format_date_for_validation(vm.conference.date_to)
      }

      vm.conferenceHours = _.cloneDeep(vm.conference.hours);

      vm.newExhibitingHour
      vm.exhibitingInfo = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          ends_at_min: vm.conference.date_from_editable,
          ends_at_max: vm.conference.date_to_editable,
          room: null,
          description: null
      };

      vm.moveInDetails = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          ends_at_min: vm.conference.date_from_editable,
          ends_at_max: vm.conference.date_to_editable,
          room: null,
          description: null
      };

      vm.moveOutDetails = {
          starts_at: vm.defaultHourStart,
          ends_at: vm.defaultHourEnd,
          ends_at_min: vm.conference.date_from_editable,
          ends_at_max: vm.conference.date_to_editable,
          room: null,
          description: null
      };

      $scope.$watch('exhibitingHourSaved', function(){
          if($scope.exhibitingHourSaved === true){
              $scope.exhibitingHourSaved = false;
          }
      });

      // When deleting exhibiting hours this will remove them from the payload to be sent to the backend
      vm.removeExhibitingInfo = function(info){
          if (info.type == 1 || !info.type) {
              var details = vm.exhibitingDays[vm.currentExhibitingDay];
              var index = details.indexOf(info);
              if(index > -1){
                  details.splice(index, 1);
              }
              vm.exhibitingDays[vm.currentExhibitingDay] = details;
              if (vm.exhibitingDays[vm.currentExhibitingDay].length == 0) {
                  vm.showExhibitingDay[vm.currentExhibitingDay] = true;
              }

              if (vm.exhibitingDays[vm.currentExhibitingDay].length <= 0) {
                  vm.exhibitingDays[vm.currentExhibitingDay].push({
                      starts_at: vm.defaultHourStart,
                      ends_at: vm.defaultHourEnd,
                      room: null,
                      description: null
                  });
              }
          } else if (info.type == 2) {
              var index = vm.moveInHours.indexOf(info);
              if(index > -1){
                  vm.moveInHours.splice(index, 1);
              }

              if (vm.moveInHours.length == 0) {
                  vm.showMoveIn = true;
              }

              if (vm.moveInHours.length <= 0) {
                  vm.moveInHours.push({
                      starts_at: vm.defaultHourStart,
                      ends_at: vm.defaultHourEnd,
                      room: null,
                      description: null
                  });
              }
          } else if (info.type == 3) {
              var index = vm.moveOutHours.indexOf(info);
              if(index > -1){
                  vm.moveOutHours.splice(index, 1);
              }

              if (vm.moveOutHours.length == 0) {
                  vm.showMoveOut = true;
              }

              if (vm.moveOutHours.length <= 0) {
                  vm.moveOutHours.push({
                      starts_at: vm.defaultHourStart,
                      ends_at: vm.defaultHourEnd,
                      room: null,
                      description: null
                  });
              }
          }

          _.remove(vm.conferenceHours, function (conferenceHour) {
            return conferenceHour.id === info.id;
          });

          var index = vm.conferenceHours.indexOf(info);
          if(index > -1){
              vm.conferenceHours.splice(index, 1);
          }
      }

      // On cancel the updates that were not saved are reset
      vm.cancelSaveExhibitingDays = function() {
          vm.conferenceHours = _.cloneDeep(vm.conference.hours);
          vm.exhibitingDays = {};
          vm.moveInHours = [];
          vm.moveOutHours = [];

          _.each(vm.conferenceHours, function(conferenceHour) {
              var startsAt = _.clone(new Date(conferenceHour.starts_at));
              var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);
              startsAt.setTime( startsAt.getTime() + (startsAt.getTimezoneOffset() + offsetInMinutes)*60*1000 );

              var day = startsAt.getDate();
              var month = startsAt.getMonth();
              var year = startsAt.getFullYear();
              if (!vm.exhibitingDays[day + '/' + month + '/' + year]) {
                  vm.exhibitingDays[day + '/' + month + '/' + year] = [];
              }
              conferenceHour.starts_at = new Date(conferenceHour.starts_at);
              conferenceHour.ends_at = new Date(conferenceHour.ends_at);
              conferenceHour.starts_at_editable = moment(conferenceHour.starts_at);
              conferenceHour.ends_at_editable = moment(conferenceHour.ends_at);

              if (conferenceHour.type == 1) {
                  vm.exhibitingDays[day + '/' + month + '/' + year].push(conferenceHour);
              }
              else if (conferenceHour.type == 2) {
                  vm.moveInHours.push(conferenceHour);
              }
              else if (conferenceHour.type == 3) {
                  vm.moveOutHours.push(conferenceHour);
              }
          });
      }

      vm.verifyIfScrollIsRequired = function(for_hours) {
          var width = $window.innerWidth - 460;
          if (for_hours) {
              var width = $window.innerWidth - 450;
          }

          if (vm.conferenceDays && (vm.conferenceDays.length * 172 > width)) return true;
          return false;
      }

      vm.sessionSelectDay = function(date) {
          if (vm.exhibitingInfo && vm.exhibitingInfo.location && vm.exhibitingInfo.location != '') {
              vm.addExhibitingInfo();
          }
          vm.newSession = false;
          vm.sessionSelectedDay = date.getTime();
          var day = date.getDate();
          var month = date.getMonth()+1;
          var year = date.getFullYear();
      }

      vm.scrollRight = function() {
          var scrollLeft = $("#days-list-scroll").scrollLeft();
          $("#days-list-scroll").animate({
            scrollLeft: scrollLeft + 171
          }, 300);
      }

      vm.scrollLeft = function() {
          var scrollLeft = $("#days-list-scroll").scrollLeft();
          $("#days-list-scroll").animate({
            scrollLeft: scrollLeft - 171
          }, 300);
      }

      var promise;
      vm.scrollLeftHold = function() {
          if(promise){
              $interval.cancel(promise);
          }
          promise = $interval(function () {
              var scrollLeft = $("#days-list-scroll").scrollLeft();
              $("#days-list-scroll").animate({
                scrollLeft: scrollLeft - 2
              }, 0);
          }, 5);
      }

      vm.stopScroll = function() {
          $interval.cancel(promise);
          promise = null;
      }

      vm.scrollRightHold = function() {
          if(promise){
              $interval.cancel(promise);
          }
          promise = $interval(function () {
              var scrollLeft = $("#days-list-scroll").scrollLeft();
              $("#days-list-scroll").animate({
                scrollLeft: scrollLeft + 2
              }, 0);
          }, 5);
      }

      vm.permissionsForManageExhibitors = function(){
          if(_.isEmpty(vm.conference.permissions)) return false;
          if((vm.conference.permissions.indexOf("manage_exhibitors") <= -1) && (vm.conference.permissions.indexOf('admin') <= -1)) return false;
          else{
              return true;
          }
      };
    }
})();
