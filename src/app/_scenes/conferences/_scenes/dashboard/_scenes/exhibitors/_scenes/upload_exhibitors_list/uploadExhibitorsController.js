(function () {
    'use strict';

    var controllerId = 'uploadExhibitorsController';
		var app = angular.module('app');

		var dependenciesArray = [
			'$scope',
			'$rootScope',
			'exhibitorsDataService',
			'toastr',
      '$state',
      '$stateParams',
			uploadExhibitorsController
		];

    app.controller(controllerId, dependenciesArray);

    function uploadExhibitorsController($scope, $rootScope, exhibitorsDataService, toastr, $state, $stateParams) {
      var exhibitorsExtractedFromCSVFile = [];
      var conferenceId = $rootScope.conference.conference_id;

      $scope.uploadedCSVFile               = null;
      $scope.exhibitorCSVKeys              = [];
      $scope.visibleMappingInstructions    = true;
      $scope.instructionsDismissed         = false;
      $scope.activeView                    = false;
      $scope.exhibitorMappingFields        = [
          {
              code      : 'name',
              name      : 'Company Name',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'booth',
              name      : 'Booth Number',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'booth_contact_first_name',
              name      : 'Booth Contact First Name',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'booth_contact_last_name',
              name      : 'Booth Contact Last Name',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'booth_contact_email',
              name      : 'Booth Contact Email',
              mapping   : null,
              isHovered : null,
          }
      ];
      $scope.defaultExhibitorMappingFields = angular.copy($scope.exhibitorMappingFields);

      $scope.removeUploadedFile = function() {
        $scope.uploadedCSVFile = null;
      }

      $scope.uploadAnotherCSVFile = function(files){
        $scope.uploadCSVFile(files);
      }

      $scope.uploadCSVFile = function(files) {
        if (files && files.length) {
          $scope.exhibitorCSVKeys = [];
          $scope.exhibitorMappingFields = angular.copy($scope.defaultExhibitorMappingFields);
          $scope.uploadedCSVFile = files[0];
          var reader = new FileReader();
          reader.readAsText($scope.uploadedCSVFile, 'ISO-8859-1');
          reader.onload = function(e) {
            exhibitorsExtractedFromCSVFile = lib.CSV2JSON(reader.result);
            var foundCSVFieldsForUploadedExhibitors  = Object.keys(exhibitorsExtractedFromCSVFile[0]);
            angular.forEach(foundCSVFieldsForUploadedExhibitors, function(field) {
              var object = {
                'code': field,
                'mapping': null
              };

              $scope.exhibitorCSVKeys.push(object)
              $scope.$apply();
            });
           }

          $scope.$apply();
          $('.mapping-actions-wrapper .half').mCustomScrollbar({'theme': 'minimal-dark'});
          $scope.activeView = true;
        }
      };

      $scope.toggleInstructions = function(instructionsDimissalState) {
        $scope.instructionsDismissed = !instructionsDimissalState;
      }

      $scope.fieldDropped = function(event, draggable, mappedField) {
        var CSVKeyToBeMapped = draggable.draggable[0].innerText.trim(); //what has been dragged
        var exhibitorFieldToBeMapped = mappedField; //where it has been dragged

        if (!exhibitorFieldToBeMapped.mapping) {
          exhibitorFieldToBeMapped.mapping = CSVKeyToBeMapped;
          angular.forEach($scope.exhibitorMappingFields, function(field, index) {
            if (field.code === exhibitorFieldToBeMapped) {
              $scope.exhibitorMappingFields[index] = angular.copy(exhibitorFieldToBeMapped);
            }
          });

          angular.forEach($scope.exhibitorCSVKeys, function(csvKey, index){
            if(csvKey.code == CSVKeyToBeMapped) {
              $scope.exhibitorCSVKeys.splice(index,1);
            }
          })
        };

      };

			$scope.undoMapping = function(field) {
        var object = {
          'code': field.mapping,
          'mapping': null
        }
        $scope.exhibitorCSVKeys.unshift(object)
				field.mapping = null;
			}

      $scope.bulkUploadExhibitors = function() {
        var mappedKeysFromCSVtoFields = [];
        var mappedExhibitors           = [];

        var companyNameMappingFound     = false;
        var boothNumberMappingFound      = false;

        angular.forEach($scope.exhibitorMappingFields, function(field) {
          if (field.mapping) {
            var obj = {
              from : null,
              to   : null
            };

            obj.from = field.mapping;
            obj.to   = field.code;
            mappedKeysFromCSVtoFields.push(obj);
          }
        });

        angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
          if (mapping.to === 'name') {
            companyNameMappingFound = true;
          } else if (mapping.to === 'booth') {
            boothNumberMappingFound  = true;
          }
        });

        if (!companyNameMappingFound) {
          toastr.error("The Company Name field is mandatory so it must be mapped!")
        }
        if (!boothNumberMappingFound) {
          toastr.error('The Booth Number field is mandatory so it must be mapped!');
        }

        if (companyNameMappingFound && boothNumberMappingFound) {
          var foundABadEmail = false;

          for (var i = 0; i < exhibitorsExtractedFromCSVFile.length - 1; i++) {
            if (!foundABadEmail) {
              var exhibitorToBePushedToServer = {};
              angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
                exhibitorToBePushedToServer[mapping.to] = exhibitorsExtractedFromCSVFile[i][mapping.from].trim();
              });
              if (exhibitorToBePushedToServer.email_address) {
                foundABadEmail = !checkIfEmailIsValid(exhibitorToBePushedToServer.email_address);
              } else {
                exhibitorToBePushedToServer.email_address = null;
              }

              mappedExhibitors.push(exhibitorToBePushedToServer);
            } else {
              toastr.error(exhibitorToBePushedToServer.email_address + " is not a valid email address!");
              break;
            }
          }

          if (!foundABadEmail) {
            exhibitorsDataService.bulkUploadExhibitors(conferenceId, mappedExhibitors).then(function(response) {
              toastr.success('Uploaded exhibitors with success');
              $state.go('conferences.dashboard.exhibitors.overview', {conference_id: $stateParams.conference_id});
            }, function(error) {
              toastr.error(error.data.detail);
            })
          }
        }
      }

      function checkIfEmailIsValid(emailAddress) {
        var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

        return regexExpression.test(emailAddress);
      }
  }
})();
