(function(){
	'use strict';

    var serviceId 				= 'exhibitingDetailsDataService';
		var app 							= angular.module('app');
		var dependenciesArray = [
			'$q',
			'requestFactory',
			exhibitingDetailsDataService
		];

		app.service(serviceId, dependenciesArray);

		function exhibitingDetailsDataService($q, requestFactory) {
	    var service = {};

			service.setConferenceHours = function(conference_id, conference_hours) {
					var deferred = $q.defer();
					var url = 'api/v1.1/conference_hours/' + conference_id + '/';
					var hours = [];

					_.each(conference_hours, function(conference_hour){
							conference_hour.conference = conference_id;
							hours.push(conference_hour);
					});
					requestFactory.post(
							url,
							{
									conference_id: conference_id,
									conferencehour_set: hours
							},
							false
					)
					.then(function(response){
							deferred.resolve(response.data);
					})
					.catch(function(error){
							deferred.reject(error);
					});

					return deferred.promise;
			};

      return service;
    }
})();
