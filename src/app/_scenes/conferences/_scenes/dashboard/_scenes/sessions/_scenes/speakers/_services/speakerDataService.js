(function(){
	'use strict';

  var serviceId = 'speakerDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', speakerDataService
		]);
		function speakerDataService($q, requestFactory){

		    var service = {};

        service.getConferenceSpeakersList = function(conferenceId){
            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/conference_speakers/' + conferenceId + '/?page=1' ,
                {},
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        };

				service.createConferenceSpeaker = function(speakerData, conferenceId){
            var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/conference_speakers/' + conferenceId + '/',
							speakerData,
							false
						)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.updateConferenceSpeaker = function(speakerData, conferenceId){
            var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/speaker/' + speakerData.id + '/',
							speakerData,
							false
						)
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.deleteConferenceSpeaker = function(speakerData){
            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/speaker/' + speakerData.id + '/',
                {},
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        };

        return service;

      }
})();
