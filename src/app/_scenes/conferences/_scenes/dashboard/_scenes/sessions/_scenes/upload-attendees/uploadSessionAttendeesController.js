(function () {
    'use strict';

    var controllerId = 'uploadSessionAttendeesController';
		var app = angular.module('app');

		var dependenciesArray = [
			'$scope',
			'$rootScope',
			'uploadSessionAttendeesDataService',
			'toastr',
      '$state',
      '$stateParams',
			uploadSessionAttendeesController
		];

    app.controller(controllerId, dependenciesArray);

    function uploadSessionAttendeesController($scope, $rootScope, uploadSessionAttendeesDataService, toastr, $state, $stateParams) {
      var attendeesExtractedFromCSVFile = [];
      var conferenceId = $rootScope.conference.conference_id;
      var sessionId    = $stateParams.session_id;
      var customAttendeeIdValuesMapping = {
        "none"         : true,
        "no_custom_id" : false,
        "custom_id"    : false
      };
      var conference                    = $rootScope.conference;

      $scope.customAttendeeFields         = $rootScope.conference.custom_fields;
      $scope.enableCustomAttendeeOption   = customAttendeeIdValuesMapping[conference.rule_type];
      $scope.uploadedCSVFile            = null;
      $scope.attendeeCSVKeys            = [];
      $scope.visibleMappingInstructions = true;
      $scope.instructionsDismissed      = false;
      $scope.activeView                 = false;
      $scope.activeCustomAttendeeId       = conference.rule_type === "custom_id" ? true : false;
      $scope.attendeeMappingFields        = [
          {
              code      : 'first_name',
              name      : 'First Name',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'last_name',
              name      : 'Last Name',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'job_title',
              name      : 'Job Title',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'attendee_id',
              name      : 'Attendee Id',
              mapping   : null,
              isHovered : null,
              visible   : $scope.activeCustomAttendeeId
          },
          {
              code      : 'email_address',
              name      : 'Email',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'phone_number',
              name      : 'Phone',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'company_name',
              name      : 'Company',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'street_address',
              name      : 'Street Address',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'city',
              name      : 'City',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'state',
              name      : 'State',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'country',
              name      : 'Country',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'zip_code',
              name      : 'Zip Code',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'guest_of',
              name      : 'Guest of',
              mapping   : null,
              isHovered : null,
          },
          {
              code      : 'attendee_registration_level_name',
              name      : 'Registration Level',
              mapping   : null,
              isHovered : null,
          }
      ];

      $scope.customAttendeeFields = angular.copy(createMappingFieldFromCustomAttendeeFields($scope.customAttendeeFields));

      $scope.defaultAttendeeMappingFields = angular.copy($scope.attendeeMappingFields);

      $scope.removeUploadedFile = function() {
        $scope.uploadedCSVFile = null;
      }

      $scope.uploadAnotherCSVFile = function(files){
        $scope.uploadCSVFile(files);
      }

      $scope.uploadCSVFile = function(files) {
        if (files && files.length) {
          $scope.attendeeCSVKeys = [];
          $scope.attendeeMappingFields = angular.copy($scope.defaultAttendeeMappingFields);
          $scope.uploadedCSVFile = files[0];
          var reader = new FileReader();
          reader.readAsText($scope.uploadedCSVFile, 'ISO-8859-1');
          reader.onload = function(e) {
            attendeesExtractedFromCSVFile = lib.CSV2JSON(reader.result);
            var foundCSVFieldsForUploadedAttendees  = Object.keys(attendeesExtractedFromCSVFile[0]);
            angular.forEach(foundCSVFieldsForUploadedAttendees, function(field) {
              var object = {
                'code': field,
                'mapping': null
              };
              $scope.attendeeCSVKeys.push(object)
              $scope.$apply();
            });
           }

          $scope.attendeeFieldMapping      = {};
          $scope.attendeeFieldMappingObj   = {};
          $scope.attendeeFieldMappingCode  = {};

          $scope.$apply();
          $('.mapping-actions-wrapper .half').mCustomScrollbar({'theme': 'minimal-dark'});
          $scope.activeView = true;
        }
      };

      $scope.toggleInstructions = function(instructionsDimissalState) {
        $scope.instructionsDismissed = !instructionsDimissalState;
      }

      $scope.fieldDropped = function(event, draggable, mappedField) {
        var CSVKeyToBeMapped = draggable.draggable[0].innerText.trim(); //what has been dragged
        var attendeeFieldToBeMapped = mappedField; //where it has been dragged
        if(!attendeeFieldToBeMapped.mapping) {
          attendeeFieldToBeMapped.mapping = CSVKeyToBeMapped;
          angular.forEach($scope.attendeeMappingFields, function(field, index) {
            if (field.code === attendeeFieldToBeMapped) {
              $scope.attendeeMappingFields[index] = angular.copy(attendeeFieldToBeMapped);
            }
          });

          angular.forEach($scope.attendeeCSVKeys, function(csvKey, index){
            if(csvKey.code == CSVKeyToBeMapped){
              $scope.attendeeCSVKeys.splice(index,1);
            }
          })
        }
      };

      $scope.undoMapping = function(field) {
        var object = {
          'code': field.mapping,
          'mapping': null
        }
        $scope.attendeeCSVKeys.unshift(object)
				field.mapping = null;
			}

      $scope.uploadSessionAttendees = function() {
        var mappedKeysFromCSVtoFields = [];
        var mappedAttendees           = [];

        var firstNameMappingFound     = false;
        var lastNameMappingFound      = false;
        var emailMappingFound         = false;
        var attendeeIdMappingFound  = true;
        if ($scope.activeCustomAttendeeId) {
          attendeeIdMappingFound  = false;
        }
        angular.forEach($scope.attendeeMappingFields, function(field) {
          if (field.mapping) {
            var obj = {
              from : null,
              to   : null
            };

            obj.from = field.mapping;
            obj.to   = field.code;
            mappedKeysFromCSVtoFields.push(obj);
          }
        });

        angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
          if (mapping.to === 'first_name') {
            firstNameMappingFound = true;
          } else if (mapping.to === 'last_name') {
            lastNameMappingFound  = true;
          } else if (mapping.to === 'email_address') {
            emailMappingFound    = true;
          } else if (mapping.to === 'attendee_id' && $scope.activeCustomAttendeeId) {
            attendeeIdMappingFound = true;
          }
        });

        if (!firstNameMappingFound) {
          toastr.error("The First Name field is mandatory so it must be mapped!")
        }
        if (!lastNameMappingFound) {
          toastr.error('The Last Name field is mandatory so it must be mapped!');
        }
        if (!emailMappingFound && !$scope.activeCustomAttendeeId) {
          toastr.error('The Email field is mandatory so it must be mapped!');
        }
        if (!attendeeIdMappingFound && $scope.activeCustomAttendeeId) {
          toastr.error('The Attendee Id field is mandatory so it must be mapped!');
        }

        if (firstNameMappingFound && lastNameMappingFound && (emailMappingFound || $scope.activeCustomAttendeeId) && attendeeIdMappingFound) {
          var foundABadEmail = false;

          for (var i = 0; i < attendeesExtractedFromCSVFile.length; i++) {
            if (!foundABadEmail) {
              var attendeeToBePushedToServer = {};
              angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
                attendeeToBePushedToServer[mapping.to] = attendeesExtractedFromCSVFile[i][mapping.from];
              });
              if (attendeeToBePushedToServer.email_address) {
                foundABadEmail = !checkIfEmailIsValid(attendeeToBePushedToServer.email_address);
              } else {
                attendeeToBePushedToServer.email_address = null;
              }

              if(Object.keys(attendeesExtractedFromCSVFile[i]).length >= mappedKeysFromCSVtoFields.length) {
                mappedAttendees.push(attendeeToBePushedToServer);
              }

            } else {
              toastr.error(attendeeToBePushedToServer.email_address + " is not a valid email address!");
              break;
            }
          }

          if (!foundABadEmail) {
            uploadSessionAttendeesDataService
              .uploadSessionAttendees(conferenceId, sessionId, mappedAttendees)
              .then(function(response) {
                toastr.success('Uploaded attendees with success');
                $rootScope.conference.rule_type = response.rule_type;
                $state.go('conferences.dashboard.sessions.attendees', {session_id: sessionId, attendees_type: 'registered'});
              }, function(error) {
                toastr.error(error.data.detail);
              })
          }
        }
      }
      $scope.toggleCustomAttendeeIdOption = function(value) {
        $scope.activeCustomAttendeeId = !value;

          angular.forEach($scope.attendeeMappingFields, function(field) {
              if (field.code === 'attendee_id') {
                field.visible = $scope.activeCustomAttendeeId;
              }
          });
      }
      function checkIfEmailIsValid(emailAddress) {
        var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

        return regexExpression.test(emailAddress);
      }
      function createMappingFieldFromCustomAttendeeFields(customAttendeeFields) {
        angular.forEach(customAttendeeFields, function(field, index) {
          var camelCaseForFieldToBeMapped = "";
          var obj = {
            code: "",
            name: field.name,
            mapping: null,
            isHovered: null
          }
          angular.forEach(field.name.split(" "), function(text, index) {
            if (index != field.name.split(" ").length - 1) {
              camelCaseForFieldToBeMapped = camelCaseForFieldToBeMapped + text.toLowerCase() + "_";
            } else {
              camelCaseForFieldToBeMapped = camelCaseForFieldToBeMapped + text.toLowerCase();
            }
            obj.code = camelCaseForFieldToBeMapped;
          });

          $scope.attendeeMappingFields.push(obj);
        });

        return $scope.customAttendeeFields;
      }
  }
})();
