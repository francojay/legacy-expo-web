(function () {
    'use strict';

    var controllerId = 'createSessionController';
    var dependenciesArray = [
        '$scope',
        '$rootScope',
        '$q',
        '$state',
        '$stateParams',
        'ngDialog',
        'toastr',
        'formatRawTimeService',
        'fileService',
        'createSessionDataService',
        'sessionDetailsDataService',
        '$timeout',
        createSessionController
    ];
    angular.module('app').controller(controllerId, dependenciesArray);

    function createSessionController($scope, $rootScope, $q, $state, $stateParams, ngDialog, toastr, formatRawTimeService, fileService, createSessionDataService, sessionDetailsDataService, $timeout) {
        var conference                          = $rootScope.conference;
        var selectedDay                         = $rootScope.selectedDay || {
                                                  'unformattedDay': conference.date_from
                                                };
        var deleteMaterials                     = [];

        $scope.date_from_edited                 = moment(conference.date_from);
        $scope.date_to_edited                   = moment(conference.date_to);
        $scope.date_from_editable               = formatRawTimeService.createMomentFromRawTime(conference.date_from_raw);
        $scope.date_to_editable                 = formatRawTimeService.createMomentFromRawTime(conference.date_to_raw);
        $scope.conferenceContinuingEducations   = [];
        $scope.newContinuingEducation           = {};
        $scope.conferenceCustomSessionFields    = [];
        $scope.conferenceSpeakers               = [];
        $scope.redunancyConferenceSpeakersList  = $state.params.speakersList || [];
        $scope.newSession                       = {
          'ce_hours'                    : 0,
          'conference_id'               : conference.conference_id,
          'continuing_educations'       : [],
          'customsessionfieldvalue_set' : [],
          'description'                 : '',
          'ends_at'                     : '',
          'ends_at_editable'            : '',
          'ends_at_raw'                 : '',
          'is_moderator'                : '',
          'location'                    : '',
          'materials'                   : [],
          'max_capacity'                : 0,
          'session_day'                 : computeStartDate(selectedDay.unformattedDay) || '',
          'session_type'                : {id: 'individual_presentation', name: 'Presentation'},
          'sessionfile_set'             : [],
          'sessionquestion_set'         : [],
          'speakers'                    : [],
          'starts_at'                   : '',
          'starts_at_editable'          : '',
          'starts_at_raw'               : '',
          'title'                       : '',
          'validate_registrants'        : false
        };

        var defaultNewSessionFields = angular.copy($scope.newSession);

        $scope.sessionTypes               = [
          {id: 'individual_presentation',           name: 'Presentation'},
          {id: 'interactive_roundtable_discussion', name: 'Interactive Roundtable Discussion'},
          {id: 'panel_discussion',                  name: 'Panel Discussion'},
          {id: 'workshop',                          name: 'Workshop'},
          {id: 'pre_conference_workshop',           name: 'Pre-Conference Workshop'},
          {id: 'breakout',                          name: 'Breakout'},
          {id: 'networking',                        name: 'Networking'},
          {id: 'reception',                         name: 'Reception'},
          {id: 'breakfast',                         name: 'Breakfast'},
          {id: 'lunch',                             name: 'Lunch'},
          {id: 'dinner',                            name: 'Dinner'},
          {id: 'gala',                              name: 'Gala'},
          {id: 'keynote',                           name: 'Keynote'},
          {id: 'fitness',                           name: 'Fitness'},
          {id: 'registration',                      name: 'Registration'},
          {id: 'showcase',                          name: 'Showcase'},
          {id: 'general',                           name: 'General'}
        ];

        $scope.sessionQuestionOptionType  = [
          {'name' : 'Answer type: Freeform text', 'id' : 1},
          {'name' : 'Answer type: Single-select', 'id': 2},
          {'name' : 'Answer type: Multi-select', 'id' : 3}
        ]
        $scope.inProgress                 = false;
        $scope.isUpdate                   = false;

        getSessionDetails($stateParams.session_id);

        $scope.createNewSession = function(newSession, addAnother) {
          var validData = true;
          if(!$scope.inProgress) {
            $scope.inProgress = true;
            //convert starts_at and ends_at to conference timezone from local offset (for example from +03:00 on local machine to -04:00 to conference timezone) with keeping of the hour (if selected 15:00 it will remain 15:00 but with another timezone)
            if ($scope.newSession.starts_at && $scope.newSession.ends_at && $scope.newSession.location && $scope.newSession.title) {
              var temporaryDateToBeConvertedToCorrectTimezone = newSession.starts_at.clone();
              temporaryDateToBeConvertedToCorrectTimezone.tz(conference.timezone_name);
              temporaryDateToBeConvertedToCorrectTimezone.add(newSession.starts_at.utcOffset() - temporaryDateToBeConvertedToCorrectTimezone.utcOffset(), 'minutes');
              newSession.starts_at = temporaryDateToBeConvertedToCorrectTimezone;

              temporaryDateToBeConvertedToCorrectTimezone = newSession.ends_at.clone();
              temporaryDateToBeConvertedToCorrectTimezone.tz(conference.timezone_name);
              temporaryDateToBeConvertedToCorrectTimezone.add(newSession.ends_at.utcOffset() - temporaryDateToBeConvertedToCorrectTimezone.utcOffset(), 'minutes');
              newSession.ends_at = temporaryDateToBeConvertedToCorrectTimezone;

              newSession = createDateTimes(newSession);

              newSession.customsessionfieldvalue_set = joinArrayValues(newSession.customsessionfieldvalue_set, $scope.conferenceCustomSessionFields);
              newSession.customsessionfieldvalue_set = removeEmptyCustomFields(newSession.customsessionfieldvalue_set);


              if (!lib.validateCreateSession(newSession, toastr)) {
                $scope.inProgress = false;
                validData =  false;
              }

              if (!lib.validateSpeakers(newSession.speakers, toastr)) {
                $scope.inProgress = false;
                validData =  false;
              }
              if(validData) {
                if(deleteMaterials.length > 0){
                  for(var i = 0; i < deleteMaterials.length; i++) {
                    createSessionDataService.removeSessionFile(deleteMaterials[i]);
                  }
                }
                createSessionDataService
                  .addConferenceSession(conference.conference_id, newSession, $scope.isUpdate).then(function(data) {
                    $scope.inProgress = false;
                    if(addAnother) {
                      $state.go("conferences.dashboard.sessions.create", {speakersList:$scope.redunancyConferenceSpeakersList}, {reload: true});
                    } else {
                      $state.go("conferences.dashboard.sessions.overview")
                    }
                  }, function(error) {
                    $scope.inProgress = false;
                  });
                }
            } else {
              toastr.error("Please make sure you completed all the mandatory fields!");
              $scope.inProgress = false;
            }
          }
        }

        // MOMENT PICKER

        $scope.recalculateSessionEndsAtMax = function(newValue, oldValue) {
          if (newValue) {
            $scope.newSession.starts_at_end = moment(newValue).endOf('day');
            if(!$scope.newSession.ends_at) {
              $scope.newSession.ends_at = _.cloneDeep($scope.newSession.starts_at);
              document.getElementById('ends_at').focus();
            }
            if($scope.newSession.ends_at <= $scope.newSession.starts_at) {
              $scope.newSession.ends_at = moment(newValue).add(1, 'hours');
            }
          }

        }

        // MATERIALS

        $scope.uploadMaterial = function(files, invalidFiles) {
          if (files.length > 0) {
            fileService.getUploadSignature(files[0]).then(function(response){
                var file_link = response.data.file_link;
                var promises = [];
                if ($scope.newSession.id) {
                    var data = {
                        session_id: $scope.session.id,
                        file_url: file_link,
                        name: files[0].name,
                        type: files[0].type
                    }
                    promises.push(fileService.uploadFile(files[0], response));
                    promises.push(createSessionDataService.addSessionFile($scope.session.id, data));
                    $q.all(promises).then(function(response){
                      $scope.newSession.sessionfile_set.push(response[1]);
                    }, function(error) {
                      toastr.error("An error has occured while trying to upload your material!");
                    });
                } else {
                  var data = {
                    file_url: file_link,
                    name: files[0].name,
                    type: files[0].type
                  }
                  promises.push(fileService.uploadFile(files[0], response));
                  if (!$scope.newSession.sessionfile_set) {
                      $scope.newSession.sessionfile_set = [];
                  }
                  $q.all(promises).then(function(response) {
                    $scope.newSession.sessionfile_set.push(data);
                  }, function(error) {
                  });
                }
            }, function(error) {
              if (error.status == 403) {
                  toastr.error('Permission Denied!');
              } else {
                  toastr.error('Error!', 'There has been a problem with your request.');
              }
            });
          } else if (!_.isEmpty(invalidFiles)) {
            if (invalidFiles[0].$error === 'maxSize') {
              toastr.error('Upload a file smaller than 10MB.', 'File is too big to upload!');
            }
          }
        }

        $scope.removeMaterial = function(file){
          if(file.id) {
            deleteMaterials.push(file.id);
            $scope.newSession.sessionfile_set = _.reject($scope.newSession.sessionfile_set, ['id', file.id]);

          } else {
            $scope.newSession.sessionfile_set = _.reject($scope.newSession.sessionfile_set, ['name', file.name]);
          }

        };

        // SPEAKERS

        $scope.speakerNotAlreadyInSession = function(conferenceSpeaker) {
            var existingItem = _.find($scope.newSession.speakers, {"id": conferenceSpeaker.id})
            if (existingItem) return false;
            return true;
        }

        $scope.checkSessionSpeaker = function(conferenceSpeaker) {
          if (!conferenceSpeaker.checked) {
            conferenceSpeaker.checked = true;
          } else {
            conferenceSpeaker.checked = false;
          }
        }

        $scope.addConferenceSpeakerToSession = function(){
          _.each($scope.conferenceSpeakers, function(conferenceSpeaker){
            if(conferenceSpeaker.hasOwnProperty('checked') && conferenceSpeaker.checked == true){
              conferenceSpeaker.checked = false;
              $scope.newSession.speakers.push(conferenceSpeaker);
              $scope.conferenceSpeakers = _.reject($scope.conferenceSpeakers, ['id', conferenceSpeaker.id]);
            }
          });
        }

        $scope.removeConferenceSpeakerFronSession = function(){
          _.each($scope.newSession.speakers, function(sessionSpeaker){
            if(sessionSpeaker.hasOwnProperty('checked') && sessionSpeaker.checked == true){
              sessionSpeaker.checked = false;
              $scope.conferenceSpeakers.push(sessionSpeaker);
              $scope.newSession.speakers = _.reject($scope.newSession.speakers, function(speaker){
                return speaker.id == sessionSpeaker.id;
              });
            }
          });
        }

        $scope.setModeratorSpeaker = function(speakerId){
            if($scope.newSession.is_moderator == speakerId){
                $scope.newSession.is_moderator = null;
            } else{
                $scope.newSession.is_moderator = speakerId;
            }
        };

        $scope.openAddSpeakerModal = function(){
          var data = {};
          data.conference = conference;
          ngDialog.open({
            plain: true,
            template: '<add-speaker-modal></add-speaker-modal>',
            className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/create/_components/addSpeakerModal/addSpeakerModalStyles.scss',
            data: data
          });
        };

        // CONTINUING EDUCATION

        $scope.checkContinuingEducation = function(continuingEducation) {
            if(continuingEducation.hasOwnProperty('checked')){
                continuingEducation.checked = !continuingEducation.checked;
            } else{
                continuingEducation.checked = true;
            }
        }

        $scope.addContinuingEducationToSession = function() {
            _.each($scope.conferenceContinuingEducations, function(continuingEducation){
                if(continuingEducation.hasOwnProperty('checked') && continuingEducation.checked == true){
                    continuingEducation.checked = false;
                    $scope.newSession.continuing_educations.push(continuingEducation);
                    $scope.conferenceContinuingEducations = _.reject($scope.conferenceContinuingEducations, ['id', continuingEducation.id]);
                }
            });
        }

        $scope.removeContinuingEducationFromSession = function(){
            _.each($scope.newSession.continuing_educations, function(continuingEducation){
                if(continuingEducation.hasOwnProperty('checked') && continuingEducation.checked == true){
                    continuingEducation.checked = false;
                    $scope.conferenceContinuingEducations.push(continuingEducation);
                    $scope.newSession.continuing_educations = _.reject($scope.newSession.continuing_educations, function(sessionContinuingEducation){
                        return sessionContinuingEducation.id == continuingEducation.id;
                    });
                }
            });
        }

        $scope.addContinuingEducation = function(){
          if($scope.newContinuingEducation.name != '')
            createSessionDataService
              .createConferenceContinuingEducation($scope.newContinuingEducation, conference.conference_id)
              .then(function(response){
                $scope.conferenceContinuingEducations.push(response);
                $scope.newContinuingEducation.name = '';
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
        }

        // SESSION FEEDBACK

        $scope.setQuestionType = function(item){
          if(item.type == 1){
            item.sessionquestionoption_set = [];
          } else if(item.type == 2){
            if(item.sessionquestionoption_set.length == 0) {
              item.sessionquestionoption_set.push({'value': '', 'order': 1});
            }
          } else if(item.type == 3){
            if(item.sessionquestionoption_set.length == 0) {
              item.sessionquestionoption_set.push({'value': '', 'order': 1});
            }
          }
        };

        $scope.removeQuestionFromCurrentSession = function(index){
          $scope.newSession.sessionquestion_set = _.reject($scope.newSession.sessionquestion_set, function(question){
            return question == $scope.newSession.sessionquestion_set[index];
          });
        }

        $scope.removeOptionFromQuestion = function(item, index){
          item.sessionquestionoption_set = _.reject(item.sessionquestionoption_set, function(option){
            return option == item.sessionquestionoption_set[index];
          });
        };

        $scope.addOptionToQuestion = function(item){
            item.sessionquestionoption_set.push({'value': '', 'order': 1});
        };

        $scope.setNewQuestionForCurrentSession = function(){
          $scope.newSession.sessionquestion_set.push({
            'type': 1,
            'type_code': $scope.sessionQuestionOptionType[0],
            'text': '',
            'sessionquestionoption_set': []
          });
        };

        $scope.speakerDropOut = function(field, field2) {
          var fieldJSON = JSON.parse(field2.draggable.attr('data-content'));
          if (!$scope.newSession.speakers) {
              $scope.newSession.speakers = [];
          }

          $scope.newSession.speakers = _.reject($scope.newSession.speakers, ['id', fieldJSON.id ]);

          $scope.newSession.speakers = _.differenceBy($scope.newSession.speakers, $scope.conferenceSpeakers, 'id');

          var existingItem = _.find($scope.conferenceSpeakers, {"id": fieldJSON.id})
          if (!existingItem) {
              if($scope.newSession.is_moderator == fieldJSON.id){
                  $scope.newSession.is_moderator = null;
              }
              $scope.conferenceSpeakers.push(fieldJSON);
          }
        };

        $scope.changeDragStyle2Stop = function(element) {
            setTimeout(function () {
              element.target.style.display = 'flex';
            }, 100);
        }

        $scope.changeDragStyle2 = function(element){
            // element.target.style.cursor = 'move';
            // element.target.style.display = 'flex';
            // element.target.style.alignItems = 'center';
            // element.target.children[1].style.display = 'flex';
            // element.target.children[1].style.alignItems = 'center';
            // element.target.children[1].style.flexDirection = 'column';
            // element.target.style.display = 'none';
        }

        $scope.speakerDropIn = function(field, field2) {
            if (!$scope.newSession.speakers) {
                $scope.newSession.speakers = [];
            }
            var newItem = JSON.parse(field2.draggable.attr('data-content'));
            var existingItem = _.find($scope.newSession.speakers, {"id": newItem.id})
            if (!existingItem) {
                $scope.newSession.speakers.push(newItem);
            }
            $scope.conferenceSpeakers = _.differenceBy($scope.conferenceSpeakers, $scope.newSession.speakers, 'id');
            scrollBottomSpeakers();
        };

        // FUNCTIONS
        function scrollBottomSpeakers() {
            $("#session-speaker-right-panel").animate({
              scrollTop: $("#session-speaker-right-panel").height()
            }, {
                duration: 200
            });
        }

        function getSessionDetails(sessionId) {
          if($stateParams.session_id) {
            sessionDetailsDataService.getSessionById(conference.conference_id, sessionId).then(function(response) {
              $scope.session = response;
              replaceNewSessionFields();
              $scope.isUpdate = true;
              getConferenceContinuingEducations();
              getConferenceCustomSessionFields();
              getConferenceSpeakers();
            }, function(error) {
              toastr.error("There was a problem with your request.", "Try again.");
            });
          } else {
            getConferenceContinuingEducations();
            getConferenceCustomSessionFields();
            getConferenceSpeakers();
          }
        }

        function getConferenceContinuingEducations() {
          createSessionDataService
            .getContinuingEducationList(conference.conference_id).then(function(data) {

              var allEd = data;
              var existingEd = $scope.newSession.continuing_educations;
              var diff = [];

              allEd.forEach(function(x){
                var found = false;
                existingEd.forEach(function(y){
                  if(x.id == y.id){
                    found = true;
                  }
                })
                if(!found){
                  diff.push(x);
                }
              })

              $scope.conferenceContinuingEducations = diff;

            }, function(error) {
              toastr.error('There has been a problem with your request.', 'Try again.');
            })
        }

        function getConferenceCustomSessionFields() {
          createSessionDataService.getCustomSessionFieldsList(conference.conference_id).then(function(data) {
            $scope.conferenceCustomSessionFields = angular.copy(data);
            for (var i = 0; i < $scope.conferenceCustomSessionFields.length; i++) {
              $scope.conferenceCustomSessionFields[i].value = "";
              for (var j = 0; j < $scope.newSession.customsessionfieldvalue_set.length; j++) {
                if ($scope.newSession.customsessionfieldvalue_set[j].custom_session_field.id === $scope.conferenceCustomSessionFields[i].id) {
                  $scope.conferenceCustomSessionFields[i].value = $scope.newSession.customsessionfieldvalue_set[j].value;
                  if ($scope.conferenceCustomSessionFields[i].type === 2) {
                    $scope.conferenceCustomSessionFields[i].value = $scope.conferenceCustomSessionFields[i].value.split(",").map(function(item) {
                      return item.trim();
                    });
                  }
                  break
                }
              }
            }
          }, function(error) {
            toastr.error('There has been a problem with your request.', 'Try again.');
          });
        }

        function getConferenceSpeakers() {
          createSessionDataService
            .getConferenceSpeakersList(conference.conference_id).then(function(data) {
              $scope.conferenceSpeakers = data.speakers;
              $scope.redunancyConferenceSpeakersList = angular.copy(data.speakers);
            }, function(error) {
              toastr.error('There has been a problem with your request.', 'Try again.');
            })
        }

        function replaceNewSessionFields() {
          $scope.newSession                     = {
            'ce_hours'                    : $scope.session.ce_hours,
            'conference_id'               : $scope.session.conference,
            'continuing_educations'       : createContinuingEducationArray($scope.session.sessioncontinuingeducation_set),
            'customsessionfieldvalue_set' : $scope.session.customsessionfieldvalue_set,
            'description'                 : $scope.session.description,
            'id'                          : $scope.session.id,
            'ends_at'                     : makeDateReadableForMomentPicker($scope.session.ends_at).tz(conference.timezone_name),
            'ends_at_editable'            : $scope.session.ends_at,
            'ends_at_raw'                 : '',
            'is_moderator'                : findModerator($scope.session.sessionspeaker_set),
            'location'                    : $scope.session.location,
            'materials'                   : [],
            'max_capacity'                : $scope.session.max_capacity,
            'session_day'                 : '',
            'session_type'                : { id:$scope.session.session_type },
            'sessionfile_set'             : $scope.session.sessionfile_set,
            'sessionquestion_set'         : $scope.session.sessionquestion_set,
            'speakers'                    : createSpeakerArray($scope.session.sessionspeaker_set),
            'starts_at'                   : makeDateReadableForMomentPicker($scope.session.starts_at).tz(conference.timezone_name),
            'starts_at_editable'          : $scope.session.starts_at,
            'starts_at_raw'               : '',
            'title'                       : $scope.session.title,
            'validate_registrants'        : $scope.session.validate_registrants
          };
          var temporaryDateToBeConvertedToCorrectTimezone = $scope.newSession.starts_at.clone();
          temporaryDateToBeConvertedToCorrectTimezone.tz(conference.timezone_name);
          $scope.newSession.starts_at = makeDateReadableForMomentPicker(temporaryDateToBeConvertedToCorrectTimezone);
          temporaryDateToBeConvertedToCorrectTimezone = $scope.newSession.ends_at.clone();
          temporaryDateToBeConvertedToCorrectTimezone.tz(conference.timezone_name);
          $scope.newSession.ends_at = makeDateReadableForMomentPicker(temporaryDateToBeConvertedToCorrectTimezone);
          $scope.newSession.session_day = moment($scope.newSession.starts_at).set({ hour: 0, minute: 0 });
        }

        function computeStartDate(date){
          return moment(date).tz(conference.timezone_name).format('MMM D, YYYY LT');
        }

        function makeDateReadableForMomentPicker(date) {
          date = formatRawTimeService.createRawTimeObject(date);
          date = formatRawTimeService.createMomentFromRawTime(date);
          // date = date.clone().tz(conference.timezone_name)

          return date;
        }

        function findModerator(speakerArray){
          var moderator;
          angular.forEach(speakerArray, function(speaker){
            if(speaker.is_moderator == true){
              moderator = speaker.speaker.id;
            }
          })
          return moderator;
        }

        function createContinuingEducationArray(continuingEducationSet) {
          var continuingEducationArray = [];
          angular.forEach(continuingEducationSet, function(continuingEducation){
            continuingEducationArray.push(continuingEducation.continuing_education);
          })
          return continuingEducationArray;
        }

        function createSpeakerArray(speakerSet) {
          var speakerArray = [];
          angular.forEach(speakerSet, function(speaker){
            speakerArray.push(speaker.speaker);
          })
          return speakerArray;
        }

        function createDateTimes(newSession){
          newSession.starts_at_raw = formatRawTimeService.createRawTimeObject(newSession.starts_at);
          newSession.ends_at_raw = formatRawTimeService.createRawTimeObject(newSession.ends_at);
          newSession.starts_at_editable = newSession.starts_at;
          newSession.ends_at_editable = newSession.ends_at;
          return newSession;
        }

        function joinArrayValues(originalCustomFields, customFields) {
          var arrToSendToServer = [];
          for (var i = 0; i < customFields.length; i++) {
            var obj = {
              "custom_session_field"  : null,
              "value"                 : null
            }
            obj.custom_session_field = customFields[i].id;
            obj.value                = customFields[i].value.toString() || "";
            arrToSendToServer.push(obj);
          }
          return arrToSendToServer;
        }

        function removeEmptyCustomFields(customFields) {
          var validCustomFields = [];
          angular.forEach(customFields, function(customField, index) {
            if(customField.value && customField.value != ''){
              validCustomFields.push(customField);
            }
          });
          return validCustomFields;
        }

        $rootScope.$on('SPEAKER_ADDED', function(event, data) {
          $scope.conferenceSpeakers.push(data)
          $scope.redunancyConferenceSpeakersList.push(data);
        })
    }
})();
