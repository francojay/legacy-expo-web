(function(){
	'use strict';

    var serviceId = 'uploadSessionsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', uploadSessionsDataService
		]);
		function uploadSessionsDataService($q, requestFactory){

		    var service = {};

				service.getCustomSessionFieldsList = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/custom_session_fields/' + conferenceId + '/',
            {},
            false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

				service.bulkCreateSession = function(payload, conference_id){
						var deferred = $q.defer();
						var data = {
							session_set: payload
						}
						requestFactory.post(
							'api/v1.1/sessions_bulk/' + conference_id + '/',
							data,
							false
						).then(function(response){
							deferred.resolve(response.data);
						}, function(error){
							deferred.reject(error);
						});
						return deferred.promise;
				};

        return service;
    }
})();
