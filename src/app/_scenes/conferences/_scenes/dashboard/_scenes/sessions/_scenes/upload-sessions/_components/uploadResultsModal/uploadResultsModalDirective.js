(function(){
  'use strict';

  var directiveId = 'uploadResultsModal';
	var app 				= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$state',
		'toastr',
		'sessionDetailsDataService',
		uploadResultsModal
	];

	app.directive(directiveId, dependenciesArray);

  function uploadResultsModal($rootScope, $state, toastr, sessionDetailsDataService) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/upload-sessions/_components/uploadResultsModal/uploadResultsModalView.html',
        link: link
    }

    function link(scope) {
			scope.downloadResults = function(sessions) {
				var json = sessions;
        var fields = Object.keys(json[0]);

        if (fields[fields.length - 1] === "Expo Upload Status") {
          var expoStatus = fields.splice(fields.length - 1, 1)
          fields.unshift(expoStatus.toString());
        }
        
        var csv = json.map(function(row){
          return fields.map(function(fieldName){
            return JSON.stringify(row[fieldName] || '');
          });
        });
        csv.unshift(fields); // add header column

        var csv = csv.join('\r\n');

        var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document

        var date = new Date();
        var filename = "Sessions_Upload_" + moment().format() + "_With_Statuses.csv";

        anchor.attr({
            href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csv),
            target: '_blank',
            download: filename
        })[0].click();

        anchor.remove();
				scope.closeThisDialog();
			}
    }
  }
})();
