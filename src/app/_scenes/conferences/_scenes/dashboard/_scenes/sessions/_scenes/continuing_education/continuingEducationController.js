(function () {
    'use strict';

    var controllerId = 'continuingEducationController';

    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', 'toastr', 'continuingEducationDataService',
            continuingEducationController
        ]);

    function continuingEducationController($rootScope, $scope, toastr, continuingEducationDataService) {

      var conference                  = $rootScope.conference;

      $scope.continuingEducationList  = [];
      $scope.continuingEducationModel = {
        "name" : '',
        "id"   : null
      };

      getConferenceContinuingEducationList(conference.conference_id);

      $scope.resetFields = function() {
        $scope.continuingEducationModel = {
          'name'  : '',
          'id'    : null
        };
      };

      $scope.updateContinuingEducationModel = function(continuingEducation) {
        var newContinuingEducationModel = {
          "name"  : continuingEducation.name,
          "id"    : continuingEducation.id
        };
        $scope.continuingEducationModel = angular.copy(newContinuingEducationModel);
      };

      $scope.createOrUpdateContinuingEducation = function(continuingEducationModel) {
        if (continuingEducationModel.name) {
          if (continuingEducationModel.id) {
            updateContinuingEducation(continuingEducationModel, conference.conference_id);
          } else {
            createContinuingEducation(continuingEducationModel, conference.conference_id);
          }
        } else {
          toastr.warning("Fill in the name of the Continuing Education Type before saving!");
        }
      }

      $scope.deleteContinuingEducation = function(continuingEducationModel) {
        continuingEducationDataService
          .removeConferenceContinuingEducation(continuingEducationModel, conference.conference_id).then(function(response) {
            var index = $scope.continuingEducationList.indexOf(continuingEducationModel);
            $scope.continuingEducationList.splice(index,1);
            if($scope.continuingEducationModel.id == continuingEducationModel.id){
              $scope.resetFields();
            }
            toastr.success('Continuing Education Type removed.');
          }, function(error) {
            toastr.error('There has been a problem with your request.', 'Try again.');
          });
      };

      function createContinuingEducation(continuingEducationModel, conferenceId) {
        continuingEducationDataService
          .createConferenceContinuingEducation(continuingEducationModel, conferenceId).then(function(response) {
            $scope.continuingEducationList.push(response);
            $scope.resetFields();
            toastr.success('Continuing Education Type added.');
        }, function(error) {
          toastr.error('There has been a problem with your request.', 'Try again.');
        });
      }

      function updateContinuingEducation(continuingEducationModel, conferenceId) {
        continuingEducationDataService
          .updateConferenceContinuingEducation(continuingEducationModel, conferenceId).then(function(response) {
            angular.forEach($scope.continuingEducationList, function(value, key, model) {
              if(value.id == continuingEducationModel.id){
                model[key] = continuingEducationModel;
              }
            })
            $scope.resetFields();
            toastr.success('Continuing Education Type updated.');
        }, function(error) {
          toastr.error('There has been a problem with your request.', 'Try again.');
        });
      }

      function getConferenceContinuingEducationList(conferenceId) {
        continuingEducationDataService.getContinuingEducationList(conferenceId).then(function(response) {
          $scope.continuingEducationList = angular.copy(response);
        }, function(error) {
          toastr.error("There was a problem with your request.", "Try again.");
        })
      }


    }
})();
