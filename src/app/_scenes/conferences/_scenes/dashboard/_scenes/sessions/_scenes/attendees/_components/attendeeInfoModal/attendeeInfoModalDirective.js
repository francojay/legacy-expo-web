(function(){
  'use strict';

  var directiveId = 'attendeeInfoModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', 'ngDialog', 'toastr', 'sessionAttendeesDataService', attendeeInfoModal]);

    function attendeeInfoModal($rootScope, ngDialog, toastr, sessionAttendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/attendeeInfoModal/attendeeInfoModalView.html',
          link: link
      }

      function link(scope){

        scope.conference                  = scope.ngDialogData.conference;
        scope.permissions                 = scope.conference.permissions_user;
        scope.attendeeId                  = scope.ngDialogData.attendeeId;
        scope.attendee                    = {};
        scope.attendeeSessions            = {
          attendeeRegisteredSessions  : [],
          attendeeScannedSessions     : []
        };
        scope.attendeeRegisteredSessions  = [];
        scope.attendeeScannedSessions     = [];
        scope.attendeePayments            = {};
        scope.refundAmount                = 0;
        scope.attendee_session_info       = true;

        scope.attendee_sessions = []; // legacy

        scope.formLabels  = [
          {label:"Attendee ID",     code:'attendee_id'},
          {label:"Email Address",   code:'email_address'},
          {label:"Phone Number",    code:'phone_number'},
          {label:"Street Address",  code:'street_address'},
          {label:"City",            code:'city'},
          {label:"State",           code:'state'},
          {label:"Country",         code:'country'},
          {label:"Zip Code",        code:'zip_code'},
          {label:"Guest of",        code:'guest_of'}
        ];

        getAttendeeInfo(scope.attendeeId);
        getAttendeePayments(scope.attendeeId);
        getAttendeeRegisteredSessions(scope.attendeeId);
        getAttendeeScannedSessions(scope.attendeeId);

        getLegacyAttendeeSessions(scope.attendeeId); // legacy

        scope.openRefundPaymentModal = function() {
          var data = {};
          data.attendee = scope.attendee;
          data.attendeePayments = scope.attendeePayments;
          data.refundAmount = scope.refundAmount;
          ngDialog.open({
            plain       : true,
            template    : '<refund-payment-modal></refund-payment-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/refundPaymentModal/refundPaymentModalStyles.scss',
            data        : data
          })
        };

        $rootScope.$on('ATTENDEE_REFUNDED', function(event, data) {
          scope.attendeePayments = data.attendeePayments;
          scope.refundAmount = calculateMaxAmount(scope.attendeePayments);
        })

        function calculateMaxAmount(attendeePayments) {
          var maxAmount = attendeePayments.amount;
          _.each(attendeePayments.refunds, function(refund){
              maxAmount -= refund.amount;
          })
          return maxAmount;
        }

        function getAttendeeInfo(attendeeId) {
          sessionAttendeesDataService
            .getAttendeeById(attendeeId).then(function(data) {
              scope.attendee = data;
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        function getAttendeeRegisteredSessions(attendeeId) {
          sessionAttendeesDataService
            .getAttendeeRegisteredSessions(attendeeId).then(function(data) {
              scope.attendeeRegisteredSessions = data;
              if(scope.attendeeScannedSessions.scanned_attendees){
                scope.attendeeRegisteredSessions.sessions_attendees.concat(scope.attendeeScannedSessions.scanned_attendees)
              }
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        function getAttendeeScannedSessions(attendeeId) {
          sessionAttendeesDataService
            .getAttendeeScannedSessions(attendeeId).then(function(data) {
              scope.attendeeScannedSessions = data;
              scope.attendeeSessions.attendeeScannedSessions = data;
              if(scope.attendeeRegisteredSessions.sessions_attendees){
                scope.attendeeRegisteredSessions.sessions_attendees.concat(scope.attendeeScannedSessions.scanned_attendees)
              }
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        function getAttendeePayments(attendeeId) {
          sessionAttendeesDataService
            .getAttendeePayments(attendeeId).then(function(data) {
              if(data.created_at){
                scope.attendeePayments = data;
                scope.refundAmount = calculateMaxAmount(scope.attendeePayments)
              } else {
                scope.attendeePayments = [];
              }
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        // LEGACY CODE:

        function getLegacyAttendeeSessions(attendeeId){
          sessionAttendeesDataService.getAttendeeRegisteredSessions(attendeeId).then(function(attendee_sessions) {
            scope.attendee_sessions = attendee_sessions.sessions_attendees;
            _.each(scope.attendee_sessions, function(session){
                session.session_id = session.session.id;
                session.checkRegister = true;
            });

            sessionAttendeesDataService.getAttendeeScannedSessions(attendeeId).then(function(scanned_sessions) {
              scope.attendee_scanned = scanned_sessions.scanned_attendees;
              _.each(scope.attendee_scanned, function(session){
                  var existing_scanned_session = _.find(scope.attendee_sessions, {session_id: session.session.id});
                  if (existing_scanned_session) {
                      existing_scanned_session.scanned_at = session.scanned_at;
                      existing_scanned_session.checkScanned = true;
                  }
                  else {
                      session.checkScanned = true;
                      session.checkRegister = false;
                      scope.attendee_sessions.push(session);
                  }
              });
            }, function(error) {
              console.log(error)
            });
          }, function(error) {
            console.log(error)
          });
        }









      }

    }
})();
