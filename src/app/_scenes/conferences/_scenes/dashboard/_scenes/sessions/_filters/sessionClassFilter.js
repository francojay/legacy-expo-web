(function() {
  'use strict';

  var filterId = 'sessionClassFilter';
  var app = angular.module('app');

  app.filter(filterId, function() {

    return function(sessionType) {
      var className;
      var sessionTypes = [{
          id: 'individual_presentation',
          name: 'Presentation',
          class: 'fa-male'
        },
        {
          id: 'interactive_roundtable_discussion',
          name: 'Interactive Roundtable Discussion',
          class: 'fa-comments'
        },
        {
          id: 'panel_discussion',
          name: 'Panel Discussion',
          class: 'fa-comments'
        },
        {
          id: 'workshop',
          name: 'Workshop',
          class: 'fa-slideshare'
        },
        {
          id: 'pre_conference_workshop',
          name: 'Pre-Conference Workshop',
          class: 'fa-slideshare'
        },
        {
          id: 'breakout',
          name: 'Breakout',
          class: 'fa-arrows-alt'
        },
        {
          id: 'networking',
          name: 'Networking',
          class: 'fa-sitemap'
        },
        {
          id: 'reception',
          name: 'Reception',
          class: 'fa-glass'
        },
        {
          id: 'breakfast',
          name: 'Breakfast',
          class: 'fa-cutlery'
        },
        {
          id: 'lunch',
          name: 'Lunch',
          class: 'fa-cutlery'
        },
        {
          id: 'dinner',
          name: 'Dinner',
          class: 'fa-cutlery'
        },
        {
          id: 'gala',
          name: 'Gala',
          class: 'fa-certificate'
        },
        {
          id: 'keynote',
          name: 'Keynote',
          class: 'fa-child'
        },
        {
          id: 'fitness',
          name: 'Fitness',
          class: 'fa-heartbeat'
        },
        {
          id: 'registration',
          name: 'Registration',
          class: 'fa-ticket'
        },
        {
          id: 'showcase',
          name: 'Showcase',
          class: 'fa-trophy'
        },
        {
          id: 'general',
          name: 'General',
          class: 'fa-calendar-o'
        }
      ];

      angular.forEach(sessionTypes, function(value, key, model) {
        if (value.id == sessionType) {
          className = value.class;
        }
      })

      return className;
    }

  });
})();
