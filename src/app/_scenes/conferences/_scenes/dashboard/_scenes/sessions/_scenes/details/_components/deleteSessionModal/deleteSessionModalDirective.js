(function(){
  'use strict';

  var directiveId = 'deleteSessionModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', '$state', 'toastr', 'sessionDetailsDataService', deleteSessionModal]);

    function deleteSessionModal($rootScope, $state, toastr, sessionDetailsDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/details/_components/deleteSessionModal/deleteSessionModalView.html',
          link: link
      }

      function link(scope){

        var session = scope.ngDialogData.session;

        scope.confirmDeleteSession = function() {
          sessionDetailsDataService.deleteSession(session)
            .then(function(response) {
              toastr.success('The session has been deleted successfully.', 'Session Deleted!');
              $state.go('conferences.dashboard.sessions.overview');
            }, function(error) {
              toastr.error('Error!', 'There has been a problem with your request.');
            });
        }



      }

    }
})();
