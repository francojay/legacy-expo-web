(function(){
	'use strict';

    var serviceId = 'sessionAttendeesDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', sessionAttendeesDataService
		]);
		function sessionAttendeesDataService($q, requestFactory){

		    var service = {};

				service.getAttendeeById = function(attendeeId){
          var deferred = $q.defer();
          requestFactory.get(
						'/api/v1.1/attendee/' + attendeeId,
						{},
						false
					).then(function(response) {
            deferred.resolve(response.data);
          },function(error) {
						deferred.reject(error);
					})
          return deferred.promise;
        };

				service.getAttendeeRegisteredSessions = function(attendeeId){
					var deferred = $q.defer();
					requestFactory.get(
						'api/session/sessions_list/' + "?attendee_id=" + attendeeId,
						{},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					})
					return deferred.promise;
				};

				service.getAttendeeScannedSessions = function(attendeeId){
					var deferred = $q.defer();
					requestFactory.get(
						'api/session/scanned_list/' + "?attendee_id=" + attendeeId,
						{},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					})
					return deferred.promise;
				};

				service.getSessionRegistrantAttendees = function(sessionId, page, noLoader, options){
					var query = '';
					noLoader = noLoader || false;
					if(!_.isEmpty(options)){
						query = 'api/session/attendees_list' + "?session_id=" + sessionId + '&page=' + page +
							'&sort_by=' + options.sortBy + '&sort_order=' + options.sortOrder + '&q=' + options.queryString;
					} else{
						query = 'api/session/attendees_list' + "?session_id=" + sessionId + '&page=' + page;
					}
					var deferred = $q.defer();
					requestFactory.get(
						query,
						{},
						false,
						noLoader
					).then(function(response) {
            deferred.resolve(response.data);
          }, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				};

				service.getSessionScannedAttendees = function(sessionId, page, noLoader, options){
          var query = '';
          noLoader = noLoader || false;
          if(!_.isEmpty(options)){
            query = 'api/session/scanned_attendees_list/' + "?session_id=" + sessionId + '&page=' + page +
              '&sort_by=' + options.sortBy + '&sort_order=' + options.sortOrder + '&q=' + options.queryString;
          } else{
						query = 'api/session/scanned_attendees_list/' + "?session_id=" + sessionId + '&page=' + page;
					}
					var deferred = $q.defer();
					requestFactory.get(
						query,
						{},
						false,
						noLoader
					).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
        };

				service.deleteSessionRegistrantAttendees = function(sessionId, attendeeList, removeAll){
					var deferred = $q.defer();
					requestFactory.post(
					'api/session/attendees_remove',
					{
						'session_id' 		: sessionId,
						'attendee_ids' 	: attendeeList,
						'remove_all' 		: removeAll
					}).then(function(response) {
          	deferred.resolve(response.data);
          },function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				service.deleteSessionScannedAttendees = function(sessionId, attendeeList, removeAll){
					var deferred = $q.defer();
					requestFactory.post(
					'api/session/scanned_attendees_remove',
					{
						'session_id' 		: sessionId,
						'attendee_ids' 	: attendeeList,
						'remove_all' 		: removeAll
					}).then(function(response) {
          	deferred.resolve(response.data);
          },function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				service.downloadAttendees = function(conferenceId, attendeeIds, extra, page) {
					var deferred = $q.defer();
					var url = 'api/v1.1/download_attendees/' + conferenceId + '/';
					if (page) {
						url = url + '?page=' + page;
					} else {
						url = url + '?page=1';
					}
					if (attendeeIds && attendeeIds.length > 0) {
						url = url + "&attendee_ids=" + attendeeIds;
					}
					if (extra) {
						url = url + "&" + extra;
					}
					requestFactory.get(url).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.getAttendeePayments = function(attendeeId){
					var deferred = $q.defer();
					requestFactory.get(
						'api/v1.1/attendee_payments/' + attendeeId,
						{},
						false
					).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				};

				service.refundAttendeePayment = function(attendeeId, refundAmount) {
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/refund_attendee_payment/' + attendeeId + '/',
						{
							'amount' : refundAmount
						},
						false
				 	).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.addAttendeeToSession = function(sessionId, attendeeIds){
					var deferred = $q.defer();
					requestFactory.post(
						'api/session/attendees_add',
						{
							'session_id' : sessionId,
							'attendee_ids': attendeeIds
						}
          ).then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				service.addScannedAttendeeToSession = function(sessionId, attendeeIds){
					var deferred = $q.defer();
					var data = [];
					_.each(attendeeIds, function(attendeeId){
				    data.push({
				        'attendee_id': attendeeId,
				        'scanned_at': new Date()
				    });
					});
					requestFactory.post(
						'api/session/attendees_scan',
						{
							'session_id' : sessionId,
							'attendees': data
						}
          ).then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				// LEGACY code

				service.getAllAttendees = function(conference_id, page, noLoader, attendeeFilters){
				    noLoader = noLoader || false;
				    attendeeFilters = attendeeFilters || {
				        queryString: null,
				        sortBy: null,
				        sortOrder: null
				    };
					var deferred = $q.defer();
					var query_url = '/api/attendee/list/' + "?conference_id=" 	+ conference_id + '&page=' + page;
					if (attendeeFilters.sortBy) {
					    if (attendeeFilters.queryString) {
					        query_url = '/api/attendee/list/' + "?conference_id=" 	+ conference_id + '&page=' + page
                            					    + "&sort_by=" + attendeeFilters.sortBy
                            					    + "&sort_order=" + attendeeFilters.sortOrder
                            					    + "&q=" + attendeeFilters.queryString
                                                    + '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
                                                    + '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
					    }
					    else {
					        query_url = '/api/attendee/list/' + "?conference_id=" 	+ conference_id + '&page=' + page
                            					    + "&sort_by=" + attendeeFilters.sortBy
                            					    + "&sort_order=" + attendeeFilters.sortOrder
                                                    + '&not_scanned_in_session=' + (attendeeFilters.not_scanned_in_session || new Number())
                                                    + '&not_registered_to_session=' + (attendeeFilters.not_registered_to_session || new Number());
					    }

					} else {
                        if (attendeeFilters.queryString) {
                            query_url = '/api/attendee/list/' + "?q=" + attendeeFilters.queryString
                        }
					}

					requestFactory.get(query_url, {}, false, noLoader)
	                .then(function(response){
	                    deferred.resolve(response.data);
	                })
	                .catch(function(error){
	                    console.log(error);
	                    deferred.reject(error);
	                });
	                return deferred.promise;
				};




        return service;
    }
})();
