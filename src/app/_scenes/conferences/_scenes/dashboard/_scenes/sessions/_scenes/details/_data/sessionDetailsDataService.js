(function(){
	'use strict';

  var serviceId = 'sessionDetailsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', sessionDetailsDataService
		]);
		function sessionDetailsDataService($q, requestFactory){

		    var service = {};

				// service.getSessionById2 = function(sessionId){
        //   var deferred = $q.defer();
				// 	requestFactory.get(
				// 		'api/session/get' + "?session_id=" + sessionId,
				// 		{
				// 			sort_by : 'full_name',
				// 			sort_order: 'asc'
				// 		},
				// 		false,
				// 		true
				// 	)
        //   .then(function(response){
        //       deferred.resolve(response.data);
        //   })
        //   .catch(function(error){
        //       deferred.reject(error);
        //   });
        //   return deferred.promise;
        // };

				service.getSessionById = function(conferenceId, sessionId){
          var deferred = $q.defer();
					requestFactory.get(
						'api/v1.1/conferences/' + conferenceId + '/sessions/' + sessionId,
						{},
						false
					)
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

				service.getSessionFeedback = function(sessionId){
          var deferred = $q.defer();
					requestFactory.get(
						'api/v1.1/attendee_feedbacks/' + sessionId + '/',
					 	{},
						false
					)
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

				service.deleteSession = function(session){
					var deferred = $q.defer();
					requestFactory.remove(
							'api/session/remove' + "?session_id=" + session.id,
							{},
							false
					)
					.then(function(response){
							deferred.resolve(response.data);
					})
					.catch(function(error){
							deferred.reject(error);
					});
					return deferred.promise;
				}

        return service;

      }
})();
