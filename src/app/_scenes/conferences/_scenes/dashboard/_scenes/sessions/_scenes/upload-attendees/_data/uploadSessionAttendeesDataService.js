(function(){
	'use strict';

    var serviceId = 'uploadSessionAttendeesDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', uploadSessionAttendeesDataService
		]);
		function uploadSessionAttendeesDataService($q, requestFactory){

		    var service = {};

				service.uploadSessionAttendees = function(conference_id, session_id, payload){
					var deferred = $q.defer();
					var data = {
							conference_id: conference_id,
							attendees: payload
					}
					requestFactory.post(
						'api/v1.1/session_upload_attendees/' + session_id + '/',
						data,
						false
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				};

        return service;
    }
})();
