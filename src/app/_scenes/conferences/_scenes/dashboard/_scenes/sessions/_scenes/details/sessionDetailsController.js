(function () {
    'use strict';

    var controllerId = 'sessionDetailsController';

    angular
        .module('app')
        .controller(controllerId, [
            '$scope', '$rootScope', '$stateParams', '$window', 'toastr', 'ngDialog', 'urlService', 'sessionFeedbackService', 'sessionDetailsDataService', 'formatRawTimeService',
            sessionDetailsController
        ]);

    function sessionDetailsController($scope, $rootScope, $stateParams, $window, toastr, ngDialog, urlService, sessionFeedbackService, sessionDetailsDataService, formatRawTimeService) {

        var conference          = $rootScope.conference;

        $scope.session          = {};
        $scope.timezone         = conference.time_zone;
        $scope.hasPermission    = conference.permissions_user.admin || conference.permissions_user.manage_sessions;
        $scope.isLoading        = false;

        getSessionDetails($stateParams.session_id);

        $scope.sanitizeUrl = function(url) {
          return urlService.sanitizeUrl(url);
        }

        $scope.openSessionFile = function(file) {
            $window.open(file.file_url, '_blank');
        }

        $scope.deleteSession = function() {
          var data = {
            "session": $scope.session,
          }
          ngDialog.open({
            plain       : true,
            template    : '<delete-session-modal></delete-session-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/details/_components/deleteSessionModal/deleteSessionModalStyle.scss',
            data        : data
          });
        }

        $scope.downloadSessionFeedback = function(session) {
          sessionFeedbackService.downloadSessionFeedback(session, conference);
        }

        function getSessionDetails(sessionId) {
          $scope.isLoading = true;
          sessionDetailsDataService
            .getSessionById(conference.conference_id, sessionId).then(function(response) {
              $scope.session = response;
              $scope.session.starts_at_time = makeDateReadableForMomentPicker($scope.session.starts_at).tz(conference.timezone_name);
              $scope.session.starts_at_time = $scope.session.starts_at_time.format('h:mm A').toString();
              $scope.session.ends_at_time = makeDateReadableForMomentPicker($scope.session.ends_at).tz(conference.timezone_name);
              $scope.session.ends_at_time = $scope.session.ends_at_time.format('h:mm A').toString();

            }, function(error) {
              toastr.error("There was a problem with your request.", "Try again.");
            })
            .then(function(){
              $scope.isLoading = false;
            });
        }

        function makeDateReadableForMomentPicker(date) {
          date = formatRawTimeService.createRawTimeObject(date);
          date = formatRawTimeService.createMomentFromRawTime(date);

          return date;
        }


    }
})();
