(function(){
	'use strict';

    var serviceId = 'createSessionDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', createSessionDataService
		]);
		function createSessionDataService($q, requestFactory){

		    var service = {};

				service.addConferenceSession = function(conferenceId, newSession, isUpdate){
					if (isUpdate == true) {
						var url = 'api/v1.1/session_complete_update/' + newSession.id + '/';
					} else {
						var url = 'api/v1.1/session_complete/' + conferenceId + '/';
					}

					var speakerIds = [];
					_.each(newSession.speakers, function(speaker) {
						speakerIds.push(speaker.id);
					})
					newSession.speakers = speakerIds;

					var ce_ids = [];
					_.each(newSession.continuing_educations, function(ce){
						ce_ids.push(ce.id);
					});
					newSession.continuing_educations = ce_ids;

					newSession.session_type = newSession.session_type.id;

					_.each(newSession.session_files, function(sessionFile){
							newSession.sessionfile_set.push(sessionFile);
					});

					var deferred = $q.defer();
					requestFactory.post(
						url,
						newSession,
						false
					).then(function(response) {
						var obj = JSON.stringify(response.data);
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				};

        service.getCustomSessionFieldsList = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/custom_session_fields/' + conferenceId + '/',
            {},
            false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

				service.addSessionFile = function(sessionId, data){
					var deferred = $q.defer();
					requestFactory.post(
						'api/session/add_file/' + sessionId,
						data,
						false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
				};

				service.removeSessionFile = function(fileId){
					var deferred = $q.defer();
					requestFactory.remove(
						'api/session/delete_file/' + "?session_file_id=" + fileId,
						null,
						false
					)
					.then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
				};

        service.getContinuingEducationList = function(conferenceId){
            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/',
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.createConferenceContinuingEducation = function(continuingEducation, conferenceId){
            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/',
                continuingEducation,
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

				service.getConferenceSpeakersList = function(conferenceId){
            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/conference_speakers/' + conferenceId + '/?page=1' ,
                {},
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        };

				service.saveConferenceSpeakerData = function(conferenceId, speaker){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/conference_speakers/' + conferenceId + '/',
							speaker,
							false
						).then(function(response){
							deferred.resolve(response.data);
						}, function(error) {
							deferred.reject(error);
						});
						return deferred.promise;
				};

        return service;
    }
})();
