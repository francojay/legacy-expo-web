(function(){
	'use strict';

    var serviceId = 'conferenceSessionsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', conferenceSessionsDataService
		]);
		function conferenceSessionsDataService($q, requestFactory){

		    var service = {};

				service.getSessionsList = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/session/list' + '?conference_id=' + conferenceId,
            false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        }

        return service;
    }
})();
