(function () {
    'use strict';

    var controllerId = 'conferenceSessionsController';
    var app          = angular.module("app");
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$window',
      '$state',
      'toastr',
      'conferenceSessionsDataService',
      '$timeout',
      'formatRawTimeService',
      conferenceSessionsController
    ];

    app.controller(controllerId, dependenciesArray);

    function conferenceSessionsController($scope, $rootScope, $window, $state, toastr, conferenceSessionsDataService, $timeout, formatRawTimeService) {
        var conference              = $rootScope.conference;
        var dayTabWidth             = 0;
        var dayTabHolderWidth       = 0;
        var dayTabHolderTranslate   = 0;
        var sessionsSliderRailWidth = 0;
        var $sessionsDayHolder      = null;
        var $sessionsSliderRail     = null;

        $scope.timezone             = conference.time_zone;
        $scope.sessionsPerDay       = [];
        $rootScope.selectedDay      = {};
        $scope.scrollPosition       = 0;
        $scope.sortType             = 'starts_at';
        $scope.sortReverse          = false;
        $scope.hideNavigationArrows = true;

        calculateConferenceDays(conference.date_from, conference.date_to);
        getSessionsList(conference.conference_id);
        $(document).ready(function() {
          initializeResizeBehaviour();
        })

        $timeout(function() {
        }, 0);

        $scope.scrollLeft = function() {
          if ($scope.scrollPosition > 0) {
            $scope.scrollPosition--;
            dayTabHolderTranslate = (-1) * dayTabWidth * $scope.scrollPosition;
            $sessionsDayHolder.css('transform', 'translateX(' + dayTabHolderTranslate + 'px)');
          }
        }

        $scope.scrollRight = function() {
          if($scope.scrollPosition < $scope.sessionsPerDay.length - 5){
            $scope.scrollPosition++;
            dayTabHolderTranslate = (-1) * dayTabWidth * $scope.scrollPosition;
            $sessionsDayHolder.css('transform', 'translateX(' + dayTabHolderTranslate + 'px)');
          }
        }

        $scope.selectDay = function(day) {
          $rootScope.selectedDay = day;
        }

        $scope.orderSessionsByField = function(field) {
          if($scope.sortType == field){
            $scope.sortReverse = !$scope.sortReverse;
          } else {
            $scope.sortType = field;
            $scope.sortReverse = false;
          }
        }

        function getSessionsList(conferenceId) {
          conferenceSessionsDataService
            .getSessionsList(conferenceId).then(function(response) {
              var sessionsList = response.sessions;
              mapSessionsToDays(sessionsList);
              if (sessionsList.length > 0) {
                $timeout(function() {
                  $('.session-table-body').mCustomScrollbar({'theme': 'minimal-dark'});
                }, 0)
              }
            }, function(error) {
              toastr.error('There has been a problem with your request.', 'Try again.');
            })
        }

        function mapSessionsToDays(sessionsList) {
          angular.forEach(sessionsList, function(session, index, model) {
            var sessionFormattedDay = moment(model[index].starts_at).tz($rootScope.conference.timezone_name).format('dddd MM/D');
            angular.forEach($scope.sessionsPerDay, function(day, dayIndex) {
              if(day.formattedDay == sessionFormattedDay) {
                session.starts_at_time = makeDateReadableForMomentPicker(session.starts_at).tz($rootScope.conference.timezone_name);
                session.starts_at_time = session.starts_at_time.format('h:mm A').toString();
                session.ends_at_time = makeDateReadableForMomentPicker(session.ends_at).tz($rootScope.conference.timezone_name);
                session.ends_at_time = session.ends_at_time.format('h:mm A').toString();
                day.sessions.push(session);
              }
            });
          })
        }

        function calculateConferenceDays(startDate, endDate) {
          var momentStartDate = moment(startDate).tz(conference.timezone_name);
          var momentEndDate   = moment(endDate).tz(conference.timezone_name);
          var momentEndDateFormat = momentEndDate.format('YYYY/MM/DD')
          for (var day = momentStartDate,dayFormat = day.format('YYYY/MM/DD');
            dayFormat <= momentEndDateFormat;day.add(1, 'd'),dayFormat=day.format('YYYY/MM/DD')) {

            var unformattedDay = angular.copy(day).format();
            var formattedDay   = day.format('dddd MM/D');

            $scope.sessionsPerDay.push({
              'unformattedDay': unformattedDay,
              'formattedDay'  : formattedDay,
              'sessions'      : []
            });

          }

          selectCurrentDay();
        }

        function selectCurrentDay() {
          var today = moment(new Date()).format('dddd MM/D');
          var isTodayFound = false;
          angular.forEach($scope.sessionsPerDay, function(day, index, model) {
            if(!isTodayFound && day.formattedDay == today) {
              $rootScope.selectedDay = day;
              if ($scope.sessionsPerDay.length <= 4) {
                $scope.scrollPosition = 0;
              } else {
                $scope.scrollPosition = index;
              }
              isTodayFound = true;
            }
          })
          if(!isTodayFound){
            $rootScope.selectedDay = $scope.sessionsPerDay[0];
          }
        }

        function initializeResizeBehaviour() {
          $sessionsDayHolder      = angular.element(document.getElementsByClassName('session-days-holder')[0]);
          $sessionsSliderRail     = angular.element(document.getElementsByClassName('session-days-slider-rail')[0]);

          sessionsSliderRailWidth = $sessionsSliderRail.width();
          dayTabWidth             = 0.2 * ($window.innerWidth - 200 - 80 - 240);
          dayTabHolderWidth       = $scope.sessionsPerDay.length * dayTabWidth * 2;
          dayTabHolderTranslate   = (-1) * dayTabWidth * $scope.scrollPosition;

          angular.element($window).on('resize', function () {
            dayTabWidth           = 0.2 * ($window.innerWidth - 200 - 80 - 240);
            dayTabHolderWidth     = $scope.sessionsPerDay.length * dayTabWidth * 2;
            dayTabHolderTranslate = (-1) * dayTabWidth * $scope.scrollPosition;
            angular.element(document.getElementsByClassName('session-day')).css('width', dayTabWidth);
            $sessionsDayHolder.css({
              'width'     : dayTabHolderWidth,
              'transform' : 'translateX(' + dayTabHolderTranslate + 'px)'
            });
          });
          $sessionsDayHolder.css('width', dayTabHolderWidth);
          $sessionsDayHolder.css('transform', 'translateX(' + dayTabHolderTranslate + 'px)');
          if (sessionsSliderRailWidth / dayTabWidth > $scope.sessionsPerDay.length) {
            $scope.hideNavigationArrows = true;
          } else {
            $scope.hideNavigationArrows = false;
          }
        }

        function makeDateReadableForMomentPicker(date) {
          date = formatRawTimeService.createRawTimeObject(date);
          date = formatRawTimeService.createMomentFromRawTime(date);

          return date;
        }

    }
})();
