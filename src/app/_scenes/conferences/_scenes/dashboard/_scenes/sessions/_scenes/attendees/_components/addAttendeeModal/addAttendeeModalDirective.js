(function(){
  'use strict';

  var directiveId = 'addAttendeeModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', 'ngDialog', 'toastr', 'sessionAttendeesDataService', addAttendeeModal]);

    function addAttendeeModal($rootScope, ngDialog, toastr, sessionAttendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/addAttendeeModal/addAttendeeModalView.html',
          link: link
      }

      function link(scope){

        var conference  = scope.ngDialogData.conference;
        var session     = scope.ngDialogData.session;
        var defaultFilters = {
          queryString   : '',
          sortBy        : 'first_name',
          sortOrder     : "asc"
        }

        scope.isRegistrantAttendeesView = scope.ngDialogData.isRegistrantAttendeesView;
        scope.isScannedAttendeesView = scope.ngDialogData.isScannedAttendeesView;

        scope.sessionAttendees = [];
        scope.sessionAttendeesTotal = 0;
        scope.sessionAttendeesCount = 0;
        scope.sessionAttendeesPage = 1;
        scope.isProcessing = false;
        scope.queryModel = '';
        scope.checkedAttendees = [];

        scope.initialAttendees = scope.ngDialogData.initialSessionAttendee;

        getAllAttendees();

        scope.$watch('sessionAttendees.length', function() {
          scope.sessionAttendeesCount = scope.sessionAttendees.length;
        })

        scope.getMoreAttendees = function() {
            var filters = {
               queryString: scope.queryModel || '',
               sortBy: 'first_name',
               sortOrder: "asc"
            }
            var page = scope.sessionAttendeesPage;
            getAllAttendees(filters, page);
        }

        scope.searchSessionAttendees = function() {
            resetAttendees();
            var filters = {
              queryString   : scope.queryModel || '',
              sortBy        : 'first_name',
              sortOrder     : "asc"
            };
            getAllAttendees(filters);
        }

        scope.checkOne = function(attendee) {
          if(attendee.checked == true){
            var index = scope.checkedAttendees.indexOf(attendee);
            scope.checkedAttendees.splice(index, 1);
          } else {
            scope.checkedAttendees.push(attendee);
          }
          attendee.checked = !attendee.checked;
        }

        scope.addSessionAttendees = function(checkedAttendees) {
          var checkedAttendeeIds = generateIdsForAttendees(checkedAttendees);
          sessionAttendeesDataService
            .addAttendeeToSession(session.id, checkedAttendeeIds).then(function(data) {
              toastr.success('Attendees have been added to session.');
              $rootScope.$broadcast('ATTENDEES_ADDED', checkedAttendees);
              scope.closeThisDialog();
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        scope.addScannedSessionAttendees = function(checkedAttendees) {
          var checkedAttendeeIds = generateIdsForScannedAttendees(checkedAttendees);
          sessionAttendeesDataService
            .addScannedAttendeeToSession(session.id, checkedAttendeeIds).then(function(data) {
              toastr.success('Attendees have been scanned.');
              $rootScope.$broadcast('ATTENDEES_ADDED', checkedAttendees);
              scope.closeThisDialog();
            }, function(error) {
              toastr.error('There was a problem with your request.', 'Try again.');
            })
        }

        scope.addManualSessionAttendee = function(){
          ngDialog.open({
              plain     : true,
              template  : '<create-attendee-modal></create-attendee-modal>',
              className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
          });
          scope.closeThisDialog();
        }

        function generateIdsForAttendees(checkedAttendees) {
          var attendeesIds = [];
          angular.forEach(checkedAttendees, function(checkedAttendee) {
            attendeesIds.push(checkedAttendee.id);
          })
          return attendeesIds;
        }

        function generateIdsForScannedAttendees(checkedAttendees) {
          var attendeesIds = [];
          angular.forEach(checkedAttendees, function(checkedAttendee) {
            attendeesIds.push(checkedAttendee.attendee_id);
          })
          return attendeesIds;
        }

        function resetAttendees() {
          scope.sessionAttendees = [];
          scope.sessionAttendeesTotal = 0;
          scope.sessionAttendeesPage = 1;
        }

        function getAllAttendees(filters, page) {
          if(!scope.isProcessing){
            scope.isProcessing = true;
          if(!filters){
            var filters = defaultFilters;
          }
          if(!page){
            var page = 1;
          }

          if(scope.isScannedAttendeesView) {
            filters.not_scanned_in_session = session.id;
          }
          else if(scope.isRegistrantAttendeesView) {
            filters.not_registered_to_session = session.id;
          }
          sessionAttendeesDataService
            .getAllAttendees(conference.conference_id, page, false, filters).then(function(data) {
              scope.sessionAttendees = scope.sessionAttendees.concat(data.attendees)
              if(scope.initialAttendees.length > 0){
                angular.forEach(scope.initialAttendees, function(attendee) {
                  angular.forEach(scope.sessionAttendees, function(sessionattendee,index) {
                    if(attendee.attendee.id == sessionattendee.id){
                      scope.sessionAttendees.splice(index,1);
                    }
                  })
                })

              }
              scope.sessionAttendeesTotal = data.total_count - scope.initialAttendees.length;
              scope.initialAttendees = [];
              scope.sessionAttendeesPage++;
              scope.isProcessing = false;
            }, function(error) {
              scope.isProcessing = false;
              toastr.error('There was a problem with your request.', 'Try again.');
            });
          }
        }



      }

    }
})();
