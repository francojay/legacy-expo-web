(function(){
  'use strict';

  var directiveId = 'deleteAttendeeFromSessionModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', 'toastr', 'sessionAttendeesDataService', deleteAttendeeFromSessionModal]);

    function deleteAttendeeFromSessionModal($rootScope, toastr, sessionAttendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/deleteAttendeeFromSessionModal/deleteAttendeeFromSessionModalView.html',
          link: link
      }

      function link(scope){

        scope.checkedAttendees          = scope.ngDialogData.checkedAttendees;
        scope.attendeeType              = scope.ngDialogData.attendeeType;
        scope.areAllChecked             = scope.ngDialogData.areAllChecked;
        scope.sessionId                 = scope.ngDialogData.sessionId;

        scope.deleteAttendeesFromSession = function() {
          var checkedAttendeesIds = [];
          angular.forEach(scope.checkedAttendees, function(checkedAttendee, index, model) {
            checkedAttendeesIds.push(checkedAttendee.id);
          })
          if(scope.attendeeType == 'registered'){
            sessionAttendeesDataService
              .deleteSessionRegistrantAttendees(scope.sessionId, checkedAttendeesIds, scope.areAllChecked).then(function(data) {
                if(scope.areAllChecked){
                  $rootScope.$broadcast('ALL_ATTENDEES_DELETED');
                } else {
                  $rootScope.$broadcast('ATTENDEES_DELETED', scope.checkedAttendees);
                }
              }, function(error) {
                toastr.error('Error!', 'There has been a problem with your request.');
              })
          } else if(scope.attendeeType == 'scanned'){
            sessionAttendeesDataService
              .deleteSessionScannedAttendees(scope.sessionId, checkedAttendeesIds, scope.areAllChecked).then(function(data) {
                if(scope.areAllChecked){
                  $rootScope.$broadcast('ALL_ATTENDEES_DELETED');
                } else {
                  $rootScope.$broadcast('ATTENDEES_DELETED', scope.checkedAttendees);
                }
              }, function(error) {
                toastr.error('Error!', 'There has been a problem with your request.');
              })
          }
        }


      }

    }
})();
