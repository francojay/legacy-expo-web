(function(){
  'use strict';

  var directiveId = 'attendeeQrCodeModal';

  angular
    .module('app')
    .directive(directiveId, [attendeeQrCodeModal]);

    function attendeeQrCodeModal() {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/attendeeQrCodeModal/attendeeQrCodeModalView.html',
          link: link
      }

      function link(scope){

        

      }

    }
})();
