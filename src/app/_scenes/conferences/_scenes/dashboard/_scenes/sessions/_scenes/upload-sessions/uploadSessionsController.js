(function() {
    'use strict';

    var controllerId = 'uploadSessionsController'; // uploadSessionsController exists in old app
    var app = angular.module('app');

    var dependenciesArray = [
        '$scope',
        '$rootScope',
        'uploadSessionsDataService',
        'toastr',
        '$state',
        'ngDialog',
        'continuingEducationDataService',
        uploadSessionsController
    ];

    app.controller(controllerId, dependenciesArray);

    function uploadSessionsController($scope, $rootScope, uploadSessionsDataService, toastr, $state, ngDialog, continuingEducationDataService) {
        var sessionsExtractedFromCSVFile = [];
        var conference = $rootScope.conference;
        var conferenceId = $rootScope.conference.conference_id;

        $scope.uploadedCSVFile = null;
        $scope.sessionCSVKeys = [];
        $scope.visibleMappingInstructions = true;
        $scope.instructionsDismissed = false;
        $scope.activeView = false;
        $scope.sessionsMappingFields = [{
                code: 'title',
                name: 'Session Title',
                mapping: null,
                isHovered: null,
                mandatory: true
            },
            {
                code: 'session_date',
                name: 'Date',
                mapping: null,
                isHovered: null,
                mandatory: true
            },
            {
                code: 'starts_at',
                name: 'Start Time',
                mapping: null,
                isHovered: null,
                mandatory: true
            },
            {
                code: 'ends_at',
                name: 'End Time',
                mapping: null,
                isHovered: null,
                mandatory: true
            },
            {
                code: 'location',
                name: 'Location',
                mapping: null,
                isHovered: null,
                mandatory: true
            },
            {
                code: 'session_type',
                name: 'Session Type',
                mapping: null,
                isHovered: null,
            },
            {
                code: 'max_capacity',
                name: 'Maximum Capacity',
                mapping: null,
                isHovered: null,
            },
            {
                code: 'description',
                name: 'Session Description',
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_first_name',
                name: "Speaker's First",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_last_name',
                name: "Speaker's Last",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_company',
                name: "Speaker's Company",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_email',
                name: "Speaker's Email",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_phone_number',
                name: "Speaker's Phone Number",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_linkedin',
                name: "Speaker's Linkedin URL",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_twitter',
                name: "Speaker's Twitter URL",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_facebook',
                name: "Speaker's Facebook URL",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_youtube',
                name: "Speaker's Youtube URL",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'speaker_biography',
                name: "Speaker's Biography",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'continuing_education_data',
                name: "Continuing Education",
                mapping: null,
                isHovered: null,
            },
            {
                code: 'ce_hours',
                name: "CE Hours",
                mapping: null,
                isHovered: null,
            }
        ];
        $scope.defaultSessionMappingFields = angular.copy($scope.sessionsMappingFields);
        $scope.sessionTypeOptions = [{
                id: 'individual_presentation',
                name: 'Presentation'
            },
            {
                id: 'interactive_roundtable_discussion',
                name: 'Interactive Roundtable Discussion'
            },
            {
                id: 'panel_discussion',
                name: 'Panel Discussion'
            },
            {
                id: 'workshop',
                name: 'Workshop'
            },
            {
                id: 'pre_conference_workshop',
                name: 'Pre-Conference Workshop'
            },
            {
                id: 'breakout',
                name: 'Breakout'
            },
            {
                id: 'networking',
                name: 'Networking'
            },
            {
                id: 'reception',
                name: 'Reception'
            },
            {
                id: 'breakfast',
                name: 'Breakfast'
            },
            {
                id: 'lunch',
                name: 'Lunch'
            },
            {
                id: 'dinner',
                name: 'Dinner'
            },
            {
                id: 'gala',
                name: 'Gala'
            },
            {
                id: 'keynote',
                name: 'Keynote'
            },
            {
              id: 'fitness',
              name: 'Fitness'
            },
	          {
              id: 'registration',
              name: 'Registration'
            },
	          {
              id: 'showcase',
              name: 'Showcase'
            },
						{
              id: 'general',
              name: 'General'
            }
        ];
        $scope.sessionCustomFields = [];

        getCustomSessionFieldsList(conference.conference_id);
        getSessionCETypes(conference.conference_id);
        $scope.removeUploadedFile = function() {
            $scope.uploadedCSVFile = null;
        }

        $scope.uploadAnotherCSVFile = function(files) {
            $scope.uploadCSVFile(files);
        }

        $scope.uploadCSVFile = function(files) {
            if (files && files.length) {
                $scope.sessionCSVKeys = [];
                $scope.sessionsMappingFields = angular.copy($scope.defaultSessionMappingFields);
                $scope.sessionsMappingFields = getMappingFields();
                $scope.uploadedCSVFile = files[0];
                var reader = new FileReader();
                reader.readAsText($scope.uploadedCSVFile, 'ISO-8859-1');
                reader.onload = function(e) {
                    sessionsExtractedFromCSVFile = lib.CSV2JSON(reader.result, ",");
                    var foundCSVFieldsForUploadedSessions = Object.keys(sessionsExtractedFromCSVFile[0]);
                    angular.forEach(foundCSVFieldsForUploadedSessions, function(field) {
                        var object = {
                            'code': field,
                            'mapping': null
                        };

                        $scope.sessionCSVKeys.push(object)
                        $scope.$apply();
                    });
                }

                $scope.sessionFieldMapping = {};
                $scope.sessionFieldMappingObj = {};
                $scope.sessionFieldMappingCode = {};

                $scope.$apply();
                $('.mapping-actions-wrapper .half').mCustomScrollbar({ 'theme': 'minimal-dark' });
                $scope.activeView = true;
            } else {
                toastr.error("Please upload a file. File size cannot exceed 10MB");
            }
        };

        $scope.toggleInstructions = function(instructionsDimissalState) {
            $scope.instructionsDismissed = !instructionsDimissalState;
        }

        $scope.fieldDropped = function(event, draggable, mappedField) {
            var CSVKeyToBeMapped = draggable.draggable[0].innerText.trim(); //what has been dragged
            var sessionFieldToBeMapped = mappedField; //where it has been dragged
            if (!sessionFieldToBeMapped.mapping) {
                sessionFieldToBeMapped.mapping = CSVKeyToBeMapped;
                if (typeof(sessionFieldToBeMapped.code) === "string") {
                    if (sessionFieldToBeMapped.code.indexOf('speaker') > -1) {
                        for (var i = 0; i < $scope.sessionsMappingFields.length; i++) {
                            if ($scope.sessionsMappingFields[i].code === 'speaker_first_name' || $scope.sessionsMappingFields[i].code === 'speaker_last_name') {
                                $scope.sessionsMappingFields[i].mandatory = true;
                            }
                        }
                    }
                }
                angular.forEach($scope.sessionCSVKeys, function(csvKey, index) {
                    if (csvKey.code == CSVKeyToBeMapped) {
                        $scope.sessionCSVKeys.splice(index, 1);
                    }
                })
            }
        };

        $scope.undoMapping = function(field) {
                var object = {
                    'code': field.mapping,
                    'mapping': null
                }
                $scope.sessionCSVKeys.unshift(object)
                field.mapping = null;

                if (field.code === 'speaker_first_name') {
                    for (var i = 0; i < $scope.sessionsMappingFields.length; i++) {
                        if ($scope.sessionsMappingFields[i].code === 'speaker_last_name' && !$scope.sessionsMappingFields[i].mapping) {
                            $scope.sessionsMappingFields[i].mandatory = false;
                            $scope.sessionsMappingFields[i - 1].mandatory = false;
                        }
                    }
                }
                if (field.code === 'speaker_last_name') {
                    for (var i = 0; i < $scope.sessionsMappingFields.length; i++) {
                        if ($scope.sessionsMappingFields[i].code === 'speaker_first_name' && !$scope.sessionsMappingFields[i].mapping) {
                            $scope.sessionsMappingFields[i].mandatory = false;
                            $scope.sessionsMappingFields[i + 1].mandatory = false;
                        }
                    }
                }
            }
            // refactor of below function
        $scope.bulkUploadSessions = function() {
                var mappedKeysFromCSVtoFields = [];
                var mappedSessions = [];
                var allRequiredFieldsFound = true;
                var mappingsFound = {
                    sessionTitle: false,
                    sessionDate: false,
                    sessionLocation: false,
                    sessionStartsAt: false,
                    sessionEndsAt: false,
                    sessionType: false,
                    speakerFirstName: false,
                    speakerLastName: false,
                    speakerEmail: false,
                    sessionCE: false, //session continuing education type
                    speakerBiography: false,
                }

                angular.forEach($scope.sessionsMappingFields, function(field) {
                    if (field.mapping) {
                        var obj = {
                            from: null,
                            to: null
                        };
                        obj.from = field.mapping;
                        obj.to = field.code;
                        mappedKeysFromCSVtoFields.push(obj);
                    }
                });

                angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
                    if (mapping.to === 'title') {
                        mappingsFound.sessionTitle = true; //
                    } else if (mapping.to === 'session_date') {
                        mappingsFound.sessionDate = true; //
                    } else if (mapping.to === 'location') {
                        mappingsFound.sessionLocation = true; //
                    } else if (mapping.to === 'starts_at') {
                        mappingsFound.sessionStartsAt = true; //
                    } else if (mapping.to === 'ends_at') {
                        mappingsFound.sessionEndsAt = true; //
                    }

                    // check other mapping that need validation

                    if (mapping.to === 'session_type') {
                        mappingsFound.sessionType = true;
                    } else if (mapping.to === 'continuing_education_data') {
                        mappingsFound.sessionCE = true;
                    } else if (mapping.to === 'speaker_email') {
                        mappingsFound.speakerEmail = true;
                    } else if (mapping.to === 'speaker_first_name') {
                        mappingsFound.speakerFirstName = true;
                    } else if (mapping.to === 'speaker_last_name') {
                        mappingsFound.speakerLastName = true;
                    } else if (mapping.to === 'speaker_biography') {
                        mappingsFound.speakerBiography = true;
                    }
                });

                if (!mappingsFound.sessionTitle) {
                    toastr.error('The Session Title field is mandatory so it must be mapped!');
                    allRequiredFieldsFound = false;
                }
                if (!mappingsFound.sessionDate) {
                    toastr.error('The Date field is mandatory so it must be mapped!');
                    allRequiredFieldsFound = false;
                }
                if (!mappingsFound.sessionLocation) {
                    toastr.error('The Location field is mandatory so it must be mapped!');
                    allRequiredFieldsFound = false;
                }
                if (!mappingsFound.sessionStartsAt) {
                    toastr.error('The Start Time field is mandatory so it must be mapped!');
                    allRequiredFieldsFound = false;
                }
                if (!mappingsFound.sessionEndsAt) {
                    toastr.error('The End Time field is mandatory so it must be mapped!');
                    allRequiredFieldsFound = false;
                }



                if (allRequiredFieldsFound) {
                    var lastSession = sessionsExtractedFromCSVFile[sessionsExtractedFromCSVFile.length - 1];
                    var lastSessionKeys = Object.keys(lastSession);
                    var allKeysForLastSessionAreEmpty = true;
                    var maximumIterator = sessionsExtractedFromCSVFile.length;

                    for (var i = 0; i < lastSessionKeys.length; i++) {
                        var currentKey = lastSessionKeys[i];
                        if (lastSession[currentKey].trim() != "") {
                            allKeysForLastSessionAreEmpty = false;
                        }
                    }

                    if (allKeysForLastSessionAreEmpty) {
                        maximumIterator = sessionsExtractedFromCSVFile.length - 1;
                    }

                    for (var i = 0; i < maximumIterator; i++) {
                        var sessionToBePushedToServer = {};
                        angular.forEach(mappedKeysFromCSVtoFields, function(mapping) {
                            sessionToBePushedToServer[mapping.to] = sessionsExtractedFromCSVFile[i][mapping.from];
                        });

                        mappedSessions.push(sessionToBePushedToServer);
                    }

                    mappedSessions = populateSessionMissingFields(mappedSessions);
                    mappedSessions = validateSessionTitles(mappedSessions);
                    mappedSessions = validateSessionDates(mappedSessions, $scope.conference.date_from, $scope.conference.date_to, $scope.conference.timezone_name);
                    mappedSessions = validateSessionTimes(mappedSessions);
                    mappedSessions = validateLocation(mappedSessions);

                    if (mappingsFound.sessionType) {
                        mappedSessions = validateSessionType(mappedSessions, $scope.sessionTypeOptions);
                    }
                    if (mappingsFound.sessionCE) {
                        mappedSessions = validateSessionCEType(mappedSessions, $scope.conference.continuing_educations);
                    }
                    if (mappingsFound.speakerEmail) {
                        mappedSessions = validateSpeakerEmails(mappedSessions)
                    }

                    if (mappingsFound.speakerFirstName || mappingsFound.speakerLastName) {
                        mappedSessions = validateSpeakerRequiredFields(mappedSessions);
                    }

                    if (mappingsFound.speakerBiography) {
                        mappedSessions = validateSpeakerBiography(mappedSessions);
                    }


                    var sessionsWithErrors = exportSessionsWithErrors(mappedSessions);
                    var validSessions = exportValidSessions(mappedSessions);
                    var sessionsThatCanBeSent;
                    if (validSessions.length > 0) {
                        sessionsThatCanBeSent = createMultiLineEntries(validSessions);
                    } else {
                        sessionsThatCanBeSent = [];
                    }

                    for (var i = 0; i < mappedSessions.length; i++) {
                        if (mappedSessions[i].status === "") {
                            sessionsExtractedFromCSVFile[i]["Expo Upload Status"] = "Success";
                        } else {
                            sessionsExtractedFromCSVFile[i]["Expo Upload Status"] = mappedSessions[i].status;
                        }
                    }



                    if (sessionsThatCanBeSent.length > 0) {

                        var firstItemFromValidSessions = sessionsThatCanBeSent[0];

                        if (Object.keys(firstItemFromValidSessions).indexOf('continuing_education_data') > -1) {
                            sessionsThatCanBeSent = createContinuingEducationDataObjects(sessionsThatCanBeSent);
                        }

                        if (Object.keys(firstItemFromValidSessions).indexOf('speaker_first_name') > -1 && Object.keys(firstItemFromValidSessions).indexOf('speaker_last_name') > -1) {
                            sessionsThatCanBeSent = createSpeakersObjects(sessionsThatCanBeSent);
                        }

                        sessionsThatCanBeSent = createCustomFieldsObjects(sessionsThatCanBeSent);
                    }

                    mappedSessions = createSuccessMessages(mappedSessions);

                    if (sessionsThatCanBeSent.length > 0) {
                        uploadSessionsDataService.bulkCreateSession(sessionsThatCanBeSent, conferenceId).then(function(response) {
                            ngDialog.open({
                                plain: true,
                                template: '<upload-results-modal></upload-results-modal>',
                                className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                                data: {
                                    errors: sessionsWithErrors,
                                    sent: sessionsThatCanBeSent,
                                    all: sessionsExtractedFromCSVFile
                                }
                            });
                            $state.go('conferences.dashboard.sessions.overview');
                        }, function(error) {
                            var data = error.data;
                            if(!data.detail){
                              data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"', '');
                              data = data.replace('"}', '');
                              toastr.error(data);
                            }
                            toastr.error(data.detail);
                        });
                    } else {
                        ngDialog.open({
                            plain: true,
                            template: '<upload-results-modal></upload-results-modal>',
                            className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                            data: {
                                errors: sessionsWithErrors,
                                sent: sessionsThatCanBeSent,
                                all: sessionsExtractedFromCSVFile
                            }
                        });
                    }


                }
                // this function needs to be refactored..
                $scope.bulkUploadSessions2 = function() {
                    if (vm.startedBulkUpload || !vm.sessionsCsvFields) return;

                    var sessionBulkUploadPayload = [];
                    for (var item in vm.sessionsCsvFields) {
                        var sessionObject = {};
                        _.each(vm.sessionsCsvFields[item], function(k, v) {
                            if (vm.sessionFieldMappingCode[v] != undefined) {
                                sessionObject[vm.sessionFieldMappingCode[v]] = k;
                            }
                        });
                        sessionBulkUploadPayload.push(sessionObject);
                    }

                    sessionBulkUploadPayload.splice(-1, 1)

                    var firstItem = sessionBulkUploadPayload[0];

                    if (!firstItem) {
                        toastr.error('The Session Title field is mandatory so it must be mapped!');
                        return;
                    }

                    if (!firstItem.title) {
                        toastr.error('The Session Title field is mandatory so it must be mapped!');
                        return;
                    }

                    if (!firstItem.session_date) {
                        if (!firstItem.session_date) {
                            toastr.error('The Date field is mandatory so it must be mapped!');
                            return;
                        }

                        if (!firstItem.location) {
                            toastr.error('The Location field is mandatory so it must be mapped!');
                            return;
                        }

                        if (!firstItem.starts_at) {
                            toastr.error('The Start Time field is mandatory so it must be mapped!');
                            return;
                        }

                        if (!firstItem.ends_at) {
                            toastr.error('The End Time field is mandatory so it must be mapped!');
                            return;
                        }

                        if (!firstItem.session_type) {
                            toastr.error('The Session Type field is mandatory so it must be mapped!');
                            return;
                        }

                        if (!firstItem.speaker_first_name && firstItem.speaker_last_name) {
                            toastr.error("The Speaker's First field is mandatory so it must be mapped!");
                            return;
                        }

                        if (firstItem.speaker_first_name && !firstItem.speaker_last_name) {
                            toastr.error("The Speaker's Last field is mandatory so it must be mapped!");
                            return;
                        }

                        var badDate = false;

                        var offsetInMinutes = lib.timezoneToOffset(vm.conference.time_zone);

                        _.each(sessionBulkUploadPayload, function(item) {
                            var start_full_date = item.session_date + ' ' + item.starts_at;
                            try {
                                var starts_at = new Date(start_full_date);
                                starts_at.setTime(starts_at.getTime() - (starts_at.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);
                            } catch (err) {
                                toastr.error(start_full_date + " is not a valid date!");
                                badDate = true;
                                return false;
                            }

                            var end_full_date = item.session_date + ' ' + item.ends_at;
                            try {
                                var ends_at = new Date(end_full_date);
                                ends_at.setTime(ends_at.getTime() - (ends_at.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);
                            } catch (err) {
                                toastr.error(start_full_date + " is not a valid date!");
                                badDate = true;
                                return false;
                            }

                            var session_type = _.find(vm.sessionTypeOptions, { name: item.session_type });
                            if (!session_type) {
                                toastr.error(item.session_type + " is not a valid session type!");
                                badDate = true;
                                return false;
                            }

                            item.session_type = session_type.id;



                            if (!(new Date(item.session_date) >= new Date(vm.conferenceDays[0]))) {
                                toastr.error(item.session_date + " is before the event start date!");
                                badDate = true;
                                return false;
                            }

                            if (!(new Date(item.session_date) <= new Date(vm.conferenceDays[vm.conferenceDays.length - 1]))) {
                                toastr.error(item.session_date + " is after the event end date!");
                                badDate = true;
                                return false;
                            }

                            if (item.starts_at.search(/(pm|am)/i) < 2) {
                                toastr.error(item.starts_at + ": the start time of the " + item.title + " does not have a valid format!");
                                badDate = true;
                                return false;
                            } else if (item.ends_at.search(/(pm|am)/i) < 2) {
                                toastr.error(item.ends_at + ": the end time of the " + item.title + " does not have a valid format!");
                                badDate = true;
                                return false;
                            } else {
                                item.starts_at = starts_at.toISOString();
                                item.ends_at = ends_at.toISOString();
                            }

                            item.sessionquestion_set = [];

                            if (item.speaker_first_name && item.speaker_last_name) {
                                item.sessionspeaker_set = [{
                                    speaker: {
                                        first_name: item.speaker_first_name,
                                        last_name: item.speaker_last_name,
                                        email: item.speaker_email,
                                        company: item.speaker_company,
                                        phone_number: item.speaker_phone_number,
                                        facebook_url: item.speaker_facebook,
                                        twitter_url: item.speaker_twitter,
                                        linkedin_url: item.speaker_linkedin,
                                        youtube_url: item.speaker_youtube,
                                        biography: item.speaker_biography
                                    }
                                }];
                            } else {
                                item.sessionspeaker_set = [];
                            }
                            item.sessioncontinuingeducation_set = [];
                            if (item.continuing_education_data) {
                                var ce_items = item.continuing_education_data.split(';');
                                _.each(ce_items, function(ce_item) {
                                    ce_item = ce_item.trim()
                                    if (ce_item != "") {
                                        item.sessioncontinuingeducation_set.push({
                                            "continuing_education": {
                                                "name": ce_item
                                            }
                                        });
                                    }

                                });

                            }

                            item.max_capacity = item.maximum_capacity;
                            item.customsessionfieldvalue_set = [];
                            if (!item.ce_hours) item.ce_hours = 0;
                            item.sessionfile_set = [];

                            _.each(item, function(k, v) {
                                var custom_field = _.find(vm.sessionCustomFieldsPerConference, {
                                    name: v
                                });
                                if (custom_field) {
                                    item.customsessionfieldvalue_set.push({
                                        value: k,
                                        custom_session_field: custom_field.id
                                    });
                                }
                            });

                            if (item.session_date != undefined && item.session_date.trim() == '') {
                                item.session_date = null;
                            }
                        });

                        if (badDate) {
                            return;
                        }

                        vm.startedBulkUpload = true;

                        ConferenceSessionsService.bulkCreateSession(sessionBulkUploadPayload, ConferenceService.hash_conference_id.decode(vm.conference_id)[0])
                            .then(function(response) {
                                SharedProperties.conference = null;
                                vm.startedBulkUpload = false;
                                vm.sessionsCsvFields = null;
                                SharedProperties.sessions = null;
                                SharedProperties.registrationSessions = null;
                                $state.go('home.sessions', { conference_id: vm.conference_id });
                            })
                            .catch(function(error) {
                                vm.startedBulkUpload = false;
                                var data = error.data;
                                data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"', '');
                                data = data.replace('"}', '');
                                toastr.error(data);

                            });
                    }
                }
            }
            // Generate the session csv file and download to pc
        $scope.downloadSessionTemplate = function(sampleData) {
            var json = presets.sessionInputFieldsSampleData()
            var allFields = getMappingFields();
            var fields = [];

            for (var i = 0; i < allFields.length; i++) {
                fields.push(allFields[i].name)
            }

            if (fields[fields.length - 1] === "Expo Upload Status") {
                var expoStatus = fields.splice(fields.length - 1, 1)
                fields.unshift(expoStatus.toString());
            }

            var csv = json.map(function(row) {
                return fields.map(function(fieldName, index) {
                    return JSON.stringify(row[index] || '');
                });
            });
            csv.unshift(fields); // add header column

            var csv = csv.join('\r\n');

            var anchor = angular.element('<a/>');
            anchor.css({ display: 'none' }); // Make sure it's not visible
            angular.element(document.body).append(anchor); // Attach to document

            var date = new Date();
            var filename = "ExpoSessionTemplate.csv";

            anchor.attr({
                href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csv),
                target: '_blank',
                download: filename
            })[0].click();

            anchor.remove();
        };

        function validateSessionTitles(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].title === "") {
                    mappedSessions[i].status = "Error - Session is missing the Title; ";
                } else {
                    mappedSessions[i].status = "";
                }
            }

            return mappedSessions;
        }

        function validateLocation(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].location === "") {
                    mappedSessions[i].status += "Error - Session is missing the Location; ";
                }
            }
            return mappedSessions;
        }

        function validateSessionDates(mappedSessions, conferenceDateFrom, conferenceDateTo, conferenceTimezone) {
            for (var i = 0; i < mappedSessions.length; i++) {
                var session = mappedSessions[i];
                if (session.session_date != "") {
                    var momentConferenceDateFrom = moment(conferenceDateFrom).tz(conferenceTimezone);
                    var momentConferenceDateTo = moment(conferenceDateTo).tz(conferenceTimezone);
                    var momentSessionDate = moment(session.session_date);


                    if (momentSessionDate.isValid()) {
                        var momentSessionDate_Day = parseInt(momentSessionDate.format('D'));
                        var momentSessionDate_Month = parseInt(momentSessionDate.format('M'));
                        var momentSessionDate_Year = parseInt(momentSessionDate.format('Y'));

                        var momentConferenceDateFrom_Day = parseInt(momentConferenceDateFrom.format('D'));
                        var momentConferenceDateFrom_Month = parseInt(momentConferenceDateFrom.format('M'));
                        var momentConferenceDateFrom_Year = parseInt(momentConferenceDateFrom.format('Y'));

                        var momentConferenceDateTo_Day = parseInt(momentConferenceDateTo.format('D'));
                        var momentConferenceDateTo_Month = parseInt(momentConferenceDateTo.format('M'));
                        var momentConferenceDateTo_Year = parseInt(momentConferenceDateTo.format('Y'));

                        var dateIsAfterConferenceStart = true;
                        var dateIsBeforeConferenceEnd = true;

                        //check if session date is after conference start;
                        if (momentSessionDate_Year < momentConferenceDateFrom_Year) {
                            var dateIsAfterConferenceStart = false;
                        } else {
                            if (momentSessionDate_Year === momentConferenceDateFrom_Year) {
                                if (momentSessionDate_Month < momentConferenceDateFrom_Month) {
                                    dateIsAfterConferenceStart = false;
                                } else {
                                    if (momentSessionDate_Month === momentConferenceDateFrom_Month) {
                                        if (momentSessionDate_Day < momentConferenceDateFrom_Day) {
                                            dateIsAfterConferenceStart = false;
                                        }
                                    }
                                }
                            }
                        }

                        //check if session date is before conference end;
                        if (momentSessionDate_Year > momentConferenceDateTo_Year) {
                            var dateIsBeforeConferenceEnd = false;
                        } else {
                            if (momentSessionDate_Year === momentConferenceDateTo_Year) {
                                if (momentSessionDate_Month > momentConferenceDateTo_Month) {
                                    dateIsBeforeConferenceEnd = false;
                                } else {
                                    if (momentSessionDate_Month === momentConferenceDateTo_Month) {
                                        if (momentSessionDate_Day > momentConferenceDateTo_Day) {
                                            dateIsBeforeConferenceEnd = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (!dateIsAfterConferenceStart) {
                            session.status += "Error - Session Date is Before Event Start; ";
                        } else if (!dateIsBeforeConferenceEnd) {
                            session.status += "Error - Session Date is After Event End; ";
                        }
                    } else {
                        var indexForMinus = session.session_date.indexOf("-");
                        var indexForSlash = session.session_date.indexOf("/");
                        var indexForPoint = session.session_date.indexOf(".");
                        var correctFormatExample = "Correct format examples: "
                        if (indexForMinus > -1) {
                            correctFormatExample += "Month-Day-Year or Year-Month-Day; ";
                        } else if (indexForSlash > -1) {
                            correctFormatExample += "Month/Day/Year or Year/Month/Day; ";
                        } else if (indexForPoint > -1) {
                            correctFormatExample += "Month.Day.Year or Year.Month.Day; ";
                        } else {
                            correctFormatExample += "Month/Day/Year or Year/Month/Day; ";
                        }

                        session.status += "Error - Incorrect format for Session Date. ";
                        session.status += correctFormatExample;
                    }
                } else {
                    session.status = "Error - Session Date is Missing; ";
                }

                mappedSessions[i] = session;
            }

            return mappedSessions;
        }

        function validateSessionTimes(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                var session = mappedSessions[i];
                if (session.status.indexOf('Correct format examples') > -1) {

                } else {
                    if (session.session_date != "") {
                        if (session.starts_at != "") {
                            if (session.starts_at.indexOf('AM') > -1) {
                                var sessionStartDate = moment(session.session_date + " " + session.starts_at.substring(0, session.starts_at.indexOf('AM')).trim() + " " + "AM");
                            } else if (session.starts_at.indexOf('PM') > -1) {
                                var sessionStartDate = moment(session.session_date + " " + session.starts_at.substring(0, session.starts_at.indexOf('PM')).trim() + " " + "PM");
                            } else {
                                var sessionStartDate = moment(session.session_date + " " + session.starts_at);
                            }
                        } else {
                            var sessionStartDate = null;
                            session.status += "Error - Session Start Time is Missing; ";
                        }

                        if (session.ends_at != "") {
                            if (session.ends_at.indexOf('AM') > -1) {
                                var sessionEndsDate = moment(session.session_date + " " + session.ends_at.substring(0, session.ends_at.indexOf('AM')) + " " + "AM");
                            } else if (session.ends_at.indexOf('PM') > -1) {
                                var sessionEndsDate = moment(session.session_date + " " + session.ends_at.substring(0, session.ends_at.indexOf('PM')) + " " + "PM");
                            } else {
                                var sessionEndsDate = moment(session.session_date + " " + session.ends_at);
                            }
                        } else {
                            var sessionEndsDate = null;
                            session.status += "Error - Session End Time is Missing; ";
                        }
                    } else {
                        session.status += "Error - Session Date is Missing, can't compute session start and end times; "
                        var sessionStartDate = null;
                        var sessionEndsDate = null;
                    }

                    if (sessionStartDate && sessionEndsDate) {
                        if (!sessionStartDate.isValid() && !sessionEndsDate.isValid()) {
                            session.status += "Error - Session Start & End Times don't have a valid format. Valid formats: Hour:Minute(21:00), Hour:Minute Meridian(09:00 PM), Hour:MinuteMeridian(09:00PM)";
                        } else {
                            if (!sessionStartDate.isValid()) {
                                session.status += "Error - Session Start Time doesn't have a valid format. Valid formats: Hour:Minute(21:00), Hour:Minute Meridian(09:00 PM), Hour:MinuteMeridian(09:00PM)"
                            }

                            if (!sessionEndsDate.isValid()) {
                                session.status += "Error - Session End Time doesn't have a valid format. Valid formats: Hour:Minute(21:00), Hour:Minute Meridian(09:00 PM), Hour:MinuteMeridian(09:00PM)"
                            }

                            if (sessionStartDate.isValid() && sessionEndsDate.isValid()) {
                                if (!sessionStartDate.isBefore(sessionEndsDate)) {
                                    mappedSessions[i].status += "Error - Start time is after End Time; "
                                } else {
                                    var temporaryDateToBeConvertedToCorrectTimezone = sessionStartDate.clone();
                                    temporaryDateToBeConvertedToCorrectTimezone.tz($scope.conference.timezone_name);
                                    temporaryDateToBeConvertedToCorrectTimezone.add(sessionStartDate.utcOffset() - temporaryDateToBeConvertedToCorrectTimezone.utcOffset(), 'minutes');
                                    mappedSessions[i].starts_at = temporaryDateToBeConvertedToCorrectTimezone.format();

                                    temporaryDateToBeConvertedToCorrectTimezone = sessionEndsDate.clone();
                                    temporaryDateToBeConvertedToCorrectTimezone.tz($scope.conference.timezone_name);
                                    temporaryDateToBeConvertedToCorrectTimezone.add(sessionEndsDate.utcOffset() - temporaryDateToBeConvertedToCorrectTimezone.utcOffset(), 'minutes');
                                    mappedSessions[i].ends_at = temporaryDateToBeConvertedToCorrectTimezone.format();
                                }
                            }
                        }
                    }
                }
            }
            return mappedSessions;
        }

        function validateSessionType(mappedSessions, sessionTypes) {
            for (var i = 0; i < mappedSessions.length; i++) {
                var mappedSessionType = mappedSessions[i].session_type;
                var foundMatch = false;

                for (var j = 0; j < sessionTypes.length; j++) {
                    var sessionTypeName = sessionTypes[j].name;
                    var sessionTypeCode = sessionTypes[j].id;
                    if (mappedSessionType === sessionTypeName || mappedSessionType === sessionTypeCode) {
                        foundMatch = true;
                        mappedSessions[i].session_type = sessionTypeCode;
                        break
                    }
                }

                if (!foundMatch) {
                    if (mappedSessionType.trim() === "") {
                        mappedSessions[i].session_type = "individual_presentation"
                    } else {
                        mappedSessions[i].status += "Error - Session Type is Not Correct; ";
                    }
                }
            }
            return mappedSessions;
        }

        function validateSessionCEType(mappedSessions, continuingEducations) {
            var continuingEducationsString = "";

            for (var i = 0; i < continuingEducations.length; i++) {
                if (i < continuingEducations.length - 1) {
                    continuingEducationsString += continuingEducations[i].name + "; ";
                } else {
                    continuingEducationsString += continuingEducations[i].name;
                }
            }

            for (var i = 0; i < mappedSessions.length; i++) {
                mappedSessions[i].sessioncontinuingeducation_set = [];
                var mappedContinuingEducationArray = mappedSessions[i].continuing_education_data.split(";");
                var matchFound = true;
                for (var j = 0; j < mappedContinuingEducationArray.length; j++) {
                    var mappedCE = mappedContinuingEducationArray[j].trim();
                    if (continuingEducationsString.indexOf(mappedCE) < 0) {
                        matchFound = false;
                    }
                }

                if (!matchFound) {
                    mappedSessions[i].status += "Error - Invalid CE Type(s); "
                }
            }

            return mappedSessions
        }

        function validateSpeakerEmails(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].speaker_email && !checkIfEmailIsValid(mappedSessions[i].speaker_email.toLowerCase())) {
                    mappedSessions[i].status += "Error - Speaker Email is Not Valid; ";
                }
            }

            return mappedSessions;
        }

        function validateSpeakerRequiredFields(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].speaker_first_name.length > 50) {
                    mappedSessions[i].status += "Error - Speaker First Name is longer than 50 characters; "
                }

                if (mappedSessions[i].speaker_last_name.length > 50) {
                    mappedSessions[i].status += "Error - Speaker Last Name is longer than 50 characters; "
                }
            }
            return mappedSessions;
        }

        function validateSpeakerBiography(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].speaker_biography.length > 2000) {
                    mappedSessions[i].status += "Error - Speaker Biography is longer than 2000 characters; "
                }
            }
            return mappedSessions;
        }

        function checkIfEmailIsValid(emailAddress) {
            var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            return regexExpression.test(emailAddress);
        }

        function populateSessionMissingFields(mappedSessions) {
            var currentSessionTitle = mappedSessions[0].title;
            var currentSessionDate = mappedSessions[0].session_date;
            var currentSessionStart = mappedSessions[0].starts_at;
            var currentSessionEnd = mappedSessions[0].ends_at;
            var currentSessionLocation = mappedSessions[0].location;
            var currentSessionType = mappedSessions[0].session_type;
            var currentSession = mappedSessions[0];

            for (var i = 1; i < mappedSessions.length; i++) {
                var session = mappedSessions[i];
                if (session.title === "" && session.session_date === "" && session.starts_at === "" && session.ends_at === "" && session.location === "" && session.session_type === "") {
                    session.title = currentSessionTitle;
                    session.session_date = currentSessionDate;
                    session.starts_at = currentSessionStart;
                    session.ends_at = currentSessionEnd;
                    session.location = currentSessionLocation;
                    session.session_type = currentSessionType;
                };

                var titleIsTheSame = (session.title === currentSessionTitle) ? true : false;
                var dateIsTheSame = (session.session_date === currentSessionDate) ? true : false;
                var startIsTheSame = (session.starts_at === currentSessionStart) ? true : false;
                var endIsTheSame = (session.ends_at === currentSessionEnd) ? true : false;
                var locationIsTheSame = (session.location === currentSessionLocation) ? true : false;

                var allMandatoryFieldsAreTheSame = titleIsTheSame && dateIsTheSame && startIsTheSame && endIsTheSame && locationIsTheSame;

                if (allMandatoryFieldsAreTheSame) {
                    mappedSessions[i].title = currentSession.title;
                    mappedSessions[i].session_date = currentSession.session_date;
                    mappedSessions[i].starts_at = currentSession.starts_at;
                    mappedSessions[i].ends_at = currentSession.ends_at;
                    mappedSessions[i].location = currentSession.location;
                } else {
                    currentSessionTitle = session.title;
                    currentSessionDate = session.session_date;
                    currentSessionStart = session.starts_at;
                    currentSessionEnd = session.ends_at;
                    currentSessionLocation = session.location;
                    currentSession = session;
                }
            }
            return mappedSessions;
        }

        function exportSessionsWithErrors(mappedSessions) {
            var arr = [];
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].status !== "") {
                    arr.push(mappedSessions[i]);
                }
            }

            return arr;
        }

        function exportValidSessions(mappedSessions) {
            var arr = [];

            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].status === "") {
                    arr.push(mappedSessions[i]);
                }
            }

            return arr;
        }

        function createMultiLineEntries(sessions) {
            var keys = Object.keys(sessions[0]);

            var groupedSessions = _.groupBy(sessions, function(session) {
                return session.title + "-" + session.session_date + "-" + session.starts_at + "-" + session.ends_at + "-" + session.location;
            });

            var sessionsForServer = [];

            angular.forEach(groupedSessions, function(group, index) {
                group[0].sessioncontinuingeducation_set = [];
                group[0].speakers = [];
                group[0].customsessionfieldvalue_set = [];

                if (group.length === 1) {
                    sessionsForServer.push(group[0]);
                    if (group[0].ce_hours === "") {
                        group[0].ce_hours = 0;
                    } else {
                        group[0].ce_hours = parseInt(group[0].ce_hours) || 0;
                    }
                    if (keys.indexOf('max_capacity') > -1) {
                        if (group[0].max_capacity === "") {
                            group[0].max_capacity = 0;
                        } else {
                            group[0].max_capacity = parseInt(group[0].max_capacity) || 0;
                        }
                    }
                } else {
                    for (var i = 1; i < group.length; i++) {
                        if (keys.indexOf('continuing_education_data') > -1) {
                            group[0].continuing_education_data += "; " + group[i].continuing_education_data;
                            if (keys.indexOf('ce_hours') > -1) {
                                if (group[0].ce_hours === "") {
                                    group[0].ce_hours = 0;
                                } else {
                                    group[0].ce_hours = parseInt(group[0].ce_hours);
                                }
                                if (group[i].ce_hours === "") {
                                    group[i].ce_hours = 0;
                                } else {
                                    group[i].ce_hours = parseInt(group[i].ce_hours);
                                }

                                if (group[i].ce_hours != 0) {
                                    group[0].ce_hours = group[i].ce_hours;
                                }
                            }
                        }

                        if (keys.indexOf('speaker_first_name') > -1 && keys.indexOf('speaker_last_name') > -1) {
                            group[0].speaker_first_name += "| " + group[i].speaker_first_name;
                            group[0].speaker_last_name += "| " + group[i].speaker_last_name;

                            if (keys.indexOf('speaker_email') > -1) {
                                group[0].speaker_email += "| " + group[i].speaker_email;
                            };

                            if (keys.indexOf('speaker_company') > -1) {
                                group[0].speaker_company += "| " + group[i].speaker_company;
                            }

                            if (keys.indexOf('speaker_phone_number') > -1) {
                                group[0].speaker_phone_number += "| " + group[i].speaker_phone_number;
                            }

                            if (keys.indexOf('speaker_linkedin') > -1) {
                                group[0].speaker_linkedin += "| " + group[i].speaker_linkedin;
                            }

                            if (keys.indexOf('speaker_twitter') > -1) {
                                group[0].speaker_twitter += "| " + group[i].speaker_twitter;
                            }

                            if (keys.indexOf('speaker_facebook') > -1) {
                                group[0].speaker_facebook += "| " + group[i].speaker_facebook;
                            }

                            if (keys.indexOf('speaker_youtube') > -1) {
                                group[0].speaker_youtube += "| " + group[i].speaker_youtube;
                            }

                            if (keys.indexOf('speaker_biography') > -1) {
                                group[0].speaker_biography += "| " + group[i].speaker_biography;
                            }
                        }

                        if (keys.indexOf('max_capacity') > -1) {
                            if (group[0].max_capacity != "") {
                                group[0].max_capacity = parseInt(group[0].max_capacity) || 0;
                                if (group[i].max_capacity != "") {
                                    group[0].max_capacity = parseInt(group[i].max_capacity);
                                }
                            } else {
                                group[0].max_capacity = 0;
                            }
                        }


                        if (keys.indexOf('description') > -1) {
                            if (group[i].description != "") {
                                group[0].description = group[i].description;
                            }
                        }

                        if (keys.indexOf('session_type') > -1) {
                            if (group[i].session_type != "") {
                                group[0].session_type = group[i].session_type;
                            }
                        }

                        if ($scope.sessionCustomFields.length > 0) {
                            for (var j = 0; j < $scope.sessionCustomFields.length; j++) {
                                var customField_id = $scope.sessionCustomFields[j].id.toString();
                                var customField_type = $scope.sessionCustomFields[j].type;
                                if (customField_id && keys.indexOf(customField_id) > -1) {
                                    if (customField_type === 2 && group[i][customField_id]) {
                                        group[0][customField_id] += "," + group[i][customField_id];
                                    } else {
                                        if (group[i][customField_id] && group[i][customField_id] != "") {
                                            group[0][customField_id] = group[i][customField_id];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    sessionsForServer.push(group[0]);
                }
            });
            return sessionsForServer;
        }

        function createContinuingEducationDataObjects(sessions) {
            for (var i = 0; i < sessions.length; i++) {
                var session = sessions[i];
                session.sessioncontinuingeducation_set = [];
                var continuingEducationsArray = session.continuing_education_data.split(';');

                for (var j = 0; j < continuingEducationsArray.length; j++) {
                    continuingEducationsArray[j] = continuingEducationsArray[j].trim()
                    if (continuingEducationsArray[j] != "") {
                        session.sessioncontinuingeducation_set.push({
                            "continuing_education": {
                                "name": continuingEducationsArray[j]
                            }
                        });
                    }
                }
                sessions[i] = session;
            };
            return sessions;
        }

        function createSpeakersObjects(sessions) {
            for (var i = 0; i < sessions.length; i++) {
                var session = sessions[i];
                var sessionSpeakerFirstNames = session.speaker_first_name.split("|");
                var sessionSpeakerLastNames = session.speaker_last_name.split("|");

                session.speakers = [];

                for (var j = 0; j < sessionSpeakerFirstNames.length; j++) {
                    var obj = {};
                    var firstName = sessionSpeakerFirstNames[j].trim();
                    var lastName = sessionSpeakerLastNames[j].trim();

                    if (firstName != "" && lastName != "") {
                        obj.first_name = firstName;
                        obj.last_name = lastName;
                        obj.email = session.speaker_email ? session.speaker_email.split('|')[j].trim() : null;
                        obj.company = session.speaker_company ? session.speaker_company.split('|')[j].trim() : null;
                        obj.phone_number = session.speaker_phone_number ? session.speaker_phone_number.split('|')[j].trim() : null;
                        obj.facebook_url = session.speaker_facebook ? session.speaker_facebook.split('|')[j].trim() : null;
                        obj.twitter_url = session.speaker_twitter ? session.speaker_twitter.split('|')[j].trim() : null;
                        obj.linkedin_url = session.speaker_linkedin ? session.speaker_linkedin.split('|')[j].trim() : null;
                        obj.youtube_url = session.speaker_youtube ? session.speaker_youtube.split('|')[j].trim() : null;
                        obj.biography = session.speaker_biography ? session.speaker_biography.split('|')[j].trim() : null;

                        session.speakers.push(obj);
                    }
                }
                sessions[i] = session;

            }
            return sessions;
        }

        function createCustomFieldsObjects(sessions) {
            for (var i = 0; i < sessions.length; i++) {
                var keys = Object.keys(sessions[i]);
                var session = sessions[i];
                session.customsessionfieldvalue_set = [];

                for (var j = 0; j < keys.length; j++) {
                    var key = keys[j];
                    if ($scope.sessionsCustomFieldsIds && $scope.sessionsCustomFieldsIds.indexOf(key) > -1) { //is custom field
                        if (session[key] != "") {
                            var obj = {
                                custom_session_field: parseInt(key),
                                value: session[key]
                            };

                            session.customsessionfieldvalue_set.push(obj);
                        }
                        delete session[key];
                    }
                }
                sessions[i] = session;
            }
            return sessions;
        }

        function createSuccessMessages(mappedSessions) {
            for (var i = 0; i < mappedSessions.length; i++) {
                if (mappedSessions[i].status === "") {
                    mappedSessions[i].status = "Success";
                }
            }

            return mappedSessions;
        }

        function getCustomSessionFieldsList(conferenceId) {
            uploadSessionsDataService.getCustomSessionFieldsList(conferenceId).then(function(response) {
                $scope.sessionCustomFields = response;
            }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
            });
        }

        function getMappingFields() {
            var mappingFields = angular.copy($scope.sessionsMappingFields);
            if ($scope.sessionCustomFields.length > 0) {
                angular.forEach($scope.sessionCustomFields, function(custom) {
                    $scope.sessionsCustomFieldsIds += custom.id + ",";
                    mappingFields.push({
                        code: custom.id,
                        name: custom.name
                    });
                });
            }
            return mappingFields;
        }

        function getSessionCETypes(conferenceId) {
            continuingEducationDataService.getContinuingEducationList(conferenceId).then(function(response) {
                $scope.conference.continuing_educations = response;
            }, function(error) {
                toastr.error("There was a problem with your request.", "Try again.");
            })
        }


        function createCSVFile(sessions) {
            var json = sessions;
            var fields = Object.keys(json[0]);

            var expoStatus = fields.splice(fields.length - 1, 1)
            fields.unshift(expoStatus.toString());
            var csv = json.map(function(row) {
                return fields.map(function(fieldName) {
                    return JSON.stringify(row[fieldName] || '');
                });
            });
            csv.unshift(fields); // add header column

            var csv = csv.join('\r\n');

            var anchor = angular.element('<a/>');
            anchor.css({ display: 'none' }); // Make sure it's not visible
            angular.element(document.body).append(anchor); // Attach to document

            var date = new Date();
            var filename = "Sessions_Upload_" + moment().format() + "_With_Statuses.csv";

            anchor.attr({
                href: 'data:attachment/csv;charset=utf-8,' + encodeURI(csv),
                target: '_blank',
                download: filename
            })[0].click();

            anchor.remove();
        }
    }
})();
