(function(){
	'use strict';

    var serviceId = 'continuingEducationDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', continuingEducationDataService
		]);
		function continuingEducationDataService($q, requestFactory){

		    var service = {};

        service.getContinuingEducationList = function(conferenceId){
            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/',
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.createConferenceContinuingEducation = function(continuingEducation, conferenceId){
            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/',
                continuingEducation,
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.updateConferenceContinuingEducation = function(continuingEducation, conferenceId){
            var deferred = $q.defer();
            requestFactory.put(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/' + continuingEducation.id + '/',
                continuingEducation,
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.removeConferenceContinuingEducation = function(continuingEducation, conferenceId){
            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/conferences/' + conferenceId + '/sessions/education/' + continuingEducation.id + '/',
                false
            )
            .then(function(response){
                deferred.resolve(response.data);
            })
            .catch(function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        return service;
    }
})();
