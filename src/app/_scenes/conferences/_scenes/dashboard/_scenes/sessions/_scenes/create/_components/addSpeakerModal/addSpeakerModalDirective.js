(function() {
  'use strict';

  var directiveId       = "addSpeakerModal";
  var app               = angular.module("app");
  var dependenciesArray = [
    '$rootScope',
    '$q',
    'ngDialog',
    'toastr',
    'fileService',
    'createSessionDataService',
    'intlTelInputOptions',
    'API',
    addSpeakerModal
  ];

  app.directive(directiveId, dependenciesArray);

  function addSpeakerModal($rootScope, $q, ngDialog, toastr, fileService, createSessionDataService, intlTelInputOptions, API) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/create/_components/addSpeakerModal/addSpeakerModalView.html',
        link: link
    }
    function link(scope) {
      var conference = scope.ngDialogData.conference;

      scope.conferenceSpeakerModel = {};

      scope.sessionSpeakerFields = [
        {
          label:'First Name',
          code: 'first_name',
          value:''
        },
        {
          label:'Last Name',
          code: 'last_name',
          value:''
        },
        {
          label:'Company',
          code: 'company',
          value:''
        },
        {
          label:'Job Title',
          code: 'job_title',
          value:''
        },
        {
          label:"Email Address",
          code : 'email',
          value:''
        },
        {
          label:"Phone Number",
          code : 'phone_number',
          value:''
        },
        {
          label:"LinkedIn url",
          code : 'linkedin_url',
          value:''
        },
        {
          label:"Facebook url",
          code : 'facebook_url',
          value:''
        },
        {
          label:"Twitter url",
          code : 'twitter_url',
          value:''
        },
        {
          label:"Youtube url",
          code : 'youtube_url',
          value:''},
        {
          label:"Biography",
          code : 'biography',
          value:''
        }
      ];
      scope.defaultCountryFlag = 'us';
      angular.forEach(API.COUNTRIES, function(country) {
        if(country.name === $rootScope.conference.country){
          scope.defaultCountryFlag = country.code.toLowerCase();
        }
      });
      scope.uploadSpeakerProfilePhoto = function(files) {
        lib.squareifyImage($q, files).then(function(response) {
          var imgGile = response.file;
          fileService.getUploadSignature(imgGile).then(function(response) {
            var file_link = response.data.file_link;
            var promises = [];
            promises.push(fileService.uploadFile(imgGile, response));
            $q.all(promises).then(function(response){
                scope.conferenceSpeakerModel.headshot_url = file_link;
            }, function(error) {
              toastr.error('Error!', 'There has been a problem with your request.');
            });
          }, function(error) {
            if (error.status == 403) {
              toastr.error('Permission Denied!');
            } else {
              toastr.error('Error!', 'There has been a problem with your request.');
            }
          });
        }, function(error) {
          toastr.error('Error!', 'There has been a problem with your request.');
        });
      };

      scope.saveConferenceSpeakerData = function() {
        var errors = false;
        _.forEach(scope.sessionSpeakerFields,function(speaker){
          if(speaker.code === 'first_name' || speaker.code === 'last_name'){
            if(speaker.value === ''){
              toastr.error("Fill in the required Speaker fields before saving!");
              errors = true;
              return false;
            }
            else{
              scope.conferenceSpeakerModel[speaker.code] = speaker.value;
            }
          }
          else if(speaker.code === 'phone_number' ){
            if(speaker.value === undefined){
              toastr.error("Please fill the phone number with a correct value!");
              errors = true;
              return false;
            }
            else{
              scope.conferenceSpeakerModel[speaker.code] = speaker.value;
            }
          }
          else if(speaker.value !== ''){
            scope.conferenceSpeakerModel[speaker.code] = speaker.value;
          }

        });

        if(!errors){
          createSessionDataService.saveConferenceSpeakerData(conference.conference_id, scope.conferenceSpeakerModel).then(function(response) {
            $rootScope.$broadcast('SPEAKER_ADDED', response)
            toastr.success('The speaker has been created successfully.', 'Speaker Created!');
            ngDialog.close();
          }, function(error) {
            lib.processValidationError(error, toastr);
          });
        }
      };
    }
  }
})();
