(function(){
	'use strict';

  var serviceId = 'sessionFeedbackService';

	angular
		.module('app')
		.service(serviceId, [
            'toastr', 'sessionDetailsDataService', sessionFeedbackService
		]);
		function sessionFeedbackService(toastr, sessionDetailsDataService){

		    var service = {};

        service.downloadSessionFeedback = function(session, conference) {
          sessionDetailsDataService
            .getSessionFeedback(session.id).then(function(response) {
              var feedbackList = [];
              var sessionTitles = []
              _.each(response.attendee_feedbacks, function(attendee_feedback) {
                var feedbackItem = {};
                feedbackItem['attendee'] = attendee_feedback.attendee.first_name + ' ' + attendee_feedback.attendee.last_name;
                feedbackItem['attendee_id'] = attendee_feedback.attendee.attendee_id;
                sessionTitles.push('attendee');
                sessionTitles.push('attendee_id');
                _.each(attendee_feedback.feedback_set, function(feedback_question){
                  if (feedback_question.session_question.type == 1) {
                    feedbackItem[feedback_question.session_question.text] = feedback_question.answer.answer;
                  } else if (feedback_question.session_question.type == 3) {
                    var options = feedback_question.answer.options;
                    var optionsTxt = "";
                    _.each(options, function(option){
                      var item = _.find(feedback_question.session_question.sessionquestionoption_set, {'id': option.integer_value});
                      if (item) {
                        optionsTxt += item.value + "; ";
                      }
                    });
                    feedbackItem[feedback_question.session_question.text] = optionsTxt;
                  } else if (feedback_question.session_question.type == 2){
                      var item = _.find(feedback_question.session_question.sessionquestionoption_set, {'id': feedback_question.answer.option});
                      feedbackItem[feedback_question.session_question.text] =  item.value;
                  }
                  sessionTitles.push(feedback_question.session_question.text);
                });
                feedbackList.push(feedbackItem);
              });
              sessionTitles = _.uniq(sessionTitles);
              var csvData =  lib.ConvertToCSVImproved(feedbackList, sessionTitles);
              var date = new Date();
              var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
              var filename = conference.name + actualDate + "_feedback.csv";
              lib.printCsvFile(csvData, filename);
            }, function(error) {
              toastr.error("There was a problem with your request.", "Try again.");
            });

        }

        return service;

      }
})();
