(function () {
    'use strict';

    var controllerId = 'customSessionFieldsController';

    angular
        .module('app')
        .controller(controllerId, [
            '$scope', '$rootScope', '$stateParams', '$q', 'toastr', 'ConferenceSessionsService', 'SharedProperties',
            'FileService', 'ConferenceService', 'customSessionFieldsDataService',
            customSessionFieldsController
        ]);

    function customSessionFieldsController($scope, $rootScope, $stateParams, $q, toastr, ConferenceSessionsService, SharedProperties,
        FileService, ConferenceService, customSessionFieldsDataService) {

          var conference                     = $rootScope.conference;

          $scope.customSessionFieldsList     = {};
          $scope.customSessionFieldsModel    = {
              'name'                          : '',
              'type'                          : 1,
              'customsessionfieldoption_set'  : [{
                'value' : ''
              }]
          };
          $scope.editCustomSessionFieldsMode  = false;
          $scope.fieldTypeDropdown            = false;

          getCustomSessionFieldsList(conference.conference_id);

          $scope.toggleFieldTypeDropdown = function() {
            $scope.fieldTypeDropdown = !$scope.fieldTypeDropdown;
          }

          $scope.editCustomSessionField = function(customSessionField){
            var newCustomSessionFieldsModel = {
              "id"                            : customSessionField.id,
              "name"                          : customSessionField.name,
              "type"                          : customSessionField.type,
              "customsessionfieldoption_set"  : customSessionField.customsessionfieldoption_set
            }
            $scope.customSessionFieldsModel = angular.copy(newCustomSessionFieldsModel);
            $scope.editCustomSessionFieldsMode = true;
          };

          $scope.resetFields = function() {
            $scope.customSessionFieldsModel = {
                'name'                          : '',
                'type'                          : 1,
                'customsessionfieldoption_set'  : [{
                  'value' : ''
                }]
            };
            $scope.editCustomSessionFieldsMode = false;
          }

          $scope.selectCustomSessionFieldOption = function(customSessionFieldType){
              $scope.customSessionFieldsModel.type = customSessionFieldType;
              $scope.toggleFieldTypeDropdown()
          };

          $scope.addCustomSessionFieldOption = function() {
            $scope.customSessionFieldsModel.customsessionfieldoption_set.push({
              'value' : ''
            });
          }

          $scope.removeCustomSessionFieldOption = function(index) {
            $scope.customSessionFieldsModel.customsessionfieldoption_set.splice(index,1)
          }

          $scope.createOrUpdateCustomSessionField = function(customSessionFieldsModel) {
            customSessionFieldsModel = removeEmptyOptions(customSessionFieldsModel);
            if(validateOptions(customSessionFieldsModel)){
              if(customSessionFieldsModel.id){
                updateCustomSessionField(customSessionFieldsModel);
              } else {
                createCustomSessionField(customSessionFieldsModel, conference.conference_id);
              }
            }
          };

          $scope.deleteCustomSessionField = function(customSessionField){
            customSessionFieldsDataService
              .deleteCustomSessionField(customSessionField)
              .then(function(response) {
                var index = $scope.customSessionFieldsList.indexOf(customSessionField);
                $scope.customSessionFieldsList.splice(index,1);
                if($scope.customSessionFieldsModel.id == customSessionField.id) {
                  $scope.resetFields();
                }
                toastr.success('Custom Session Field deleted.');
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          };

          function removeEmptyOptions(customSessionFieldsModel) {
            angular.forEach(customSessionFieldsModel.customsessionfieldoption_set, function(option, key, model) {
              if(!option.value) {
                model.splice(key,1);
              }
            })
            return customSessionFieldsModel;
          }

          function validateOptions(customSessionFieldsModel) {
            if(customSessionFieldsModel.name){
              if(customSessionFieldsModel.type == 1){
                return true;
              } else {
                var completedOptions = 0;
                angular.forEach(customSessionFieldsModel.customsessionfieldoption_set, function(option, key) {
                  if(option.value){
                    completedOptions++;
                  }
                });
                if(completedOptions >= 2 ) {
                  return true;
                } else {
                  toastr.warning("Fill in at least 2 possible choice options.");
                  return false;
                }
              }
            } else {
              toastr.warning("Fill in the Custom Session Field name before saving!");
              return false;
            }
          }

          function createCustomSessionField(customSessionFieldsModel, conferenceId) {
            customSessionFieldsDataService
              .createCustomSessionField(customSessionFieldsModel, conferenceId).then(function(response) {
                $scope.customSessionFieldsList.push(response);
                $scope.resetFields();
                toastr.success('Custom Sesion Field added.');
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              })
          }

          function updateCustomSessionField(customSessionFieldsModel) {
            customSessionFieldsDataService
              .updateCustomSessionField(customSessionFieldsModel).then(function(response) {
                angular.forEach($scope.customSessionFieldsList, function(value, key, model) {
                  if(value.id == customSessionFieldsModel.id){
                    model[key] = customSessionFieldsModel;
                  }
                });
                $scope.resetFields();
                toastr.success('Custom Session Field updated.');
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              })
          }

          function getCustomSessionFieldsList(conferenceId) {
            customSessionFieldsDataService
              .getCustomSessionFieldsList(conferenceId).then(function(response) {
                $scope.customSessionFieldsList = response;
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          }

    }
})();
