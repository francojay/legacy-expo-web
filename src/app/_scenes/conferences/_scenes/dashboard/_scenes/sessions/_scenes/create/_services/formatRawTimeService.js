(function(){
	'use strict';

  var serviceId = 'formatRawTimeService';

	angular
		.module('app')
		.service(serviceId, ['$q', formatRawTimeService]);
		function formatRawTimeService($q){

		    var service = {};

        service.createMomentFromRawTime = function(rawDatetime) {
          return moment(new Date(
            rawDatetime.year,
            rawDatetime.month - 1,
            rawDatetime.day,
            rawDatetime.hour,
            rawDatetime.minute
          ));
        }

				service.createRawTimeObject = function(dateTime) {
					dateTime = moment(dateTime);
					return {
						"year": dateTime.year(),
            "month": dateTime.month() + 1,
            "day": parseInt(dateTime.format("D")),
            "hour": dateTime.hour(),
            "minute": dateTime.minutes(),
					}
				}

        return service;

      }
})();
