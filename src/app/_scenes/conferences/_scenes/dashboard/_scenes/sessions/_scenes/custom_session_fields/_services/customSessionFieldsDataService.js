(function(){
	'use strict';

  var serviceId = 'customSessionFieldsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', customSessionFieldsDataService
		]);
		function customSessionFieldsDataService($q, requestFactory){

		    var service = {};

        service.getCustomSessionFieldsList = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/custom_session_fields/' + conferenceId + '/',
            {},
            false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

				service.createCustomSessionField = function(customSessionField, conferenceId) {
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/custom_session_fields/' + conferenceId + '/',
						customSessionField,
						false
					)
					.then(function(response){
							deferred.resolve(response.data);
					})
					.catch(function(error){
							deferred.reject(error);
					});
					return deferred.promise;
				}

				service.updateCustomSessionField = function(customSessionField) {
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/custom_session_field/' + customSessionField.id + '/',
						customSessionField,
						false
					)
					.then(function(response){
							deferred.resolve(response.data);
					})
					.catch(function(error){
							deferred.reject(error);
					});
					return deferred.promise;
				}

        service.deleteCustomSessionField = function(customSessionField){
          var deferred = $q.defer();
          requestFactory.remove(
            'api/v1.1/custom_session_field/' + customSessionField.id + '/',
            {},
            false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
        };

        return service;

      }
})();
