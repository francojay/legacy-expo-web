(function () {
    'use strict';

    var controllerId      = 'sessionAttendeesController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      '$q',
      'ngDialog',
      'toastr',
      'sessionAttendeesDataService',
      'sessionDetailsDataService',
      '$filter',
      sessionAttendeesController
    ];
    app.controller(controllerId, dependenciesArray);

    function sessionAttendeesController($scope, $rootScope, $stateParams, $q, ngDialog, toastr, sessionAttendeesDataService, sessionDetailsDataService, $filter) {

      var conference                    = $rootScope.conference;
      var defaultFilters                = {
        queryString   : '',
        sortBy        : 'first_name',
        sortOrder     : "asc"
      }

      $scope.attendeeFields             = [
        {code: 'full_name',   title: 'Registrant',  orderCode:  'first_name'},
        {code: 'email',       title: 'Email',       orderCode:  'email_address'},
        {code: 'phone',       title: 'Phone',       orderCode:  'phone_number'},
        {code: 'company',     title: 'Company',     orderCode:  'company_name'},
        {code: 'qr_code',     title: 'QR Code',     orderCode:  'attendee_id'}
      ];

      $scope.fieldCodeOrder             = $scope.attendeeFields[0].orderCode;
      $scope.sortReverse                = false;
      $scope.session                    = {};
      $scope.sessionAttendees           = [];
      $scope.sessionAttendeesTotal      = 0;
      $scope.sessionAttendeesCount      = 0;
      $scope.sessionAttendeesPage       = 1;
      $scope.permissions                = conference.permissions_user;
      $scope.registrant_info            = '';
      $scope.queryModel                 = '';
      $scope.isProcessing               = false;
      $scope.showMoreOptions            = false;
      $scope.checkedAttendees           = [];
      $scope.areAllChecked              = false;
      $scope.isSomethingChecked         = false;

      if($stateParams.attendees_type == 'registered') {
        $scope.isRegistrantAttendeesView = true;
      } else {
        $scope.isScannedAttendeesView = true;
        $scope.attendeeFields[0].title = "Attendee";
      }

      getSessionDetails($stateParams.session_id);

      $scope.$watch('sessionAttendees.length', function() {
        $scope.sessionAttendeesCount = $scope.sessionAttendees.length;
      })

      $scope.$watch('checkedAttendees.length', function() {
        if($scope.checkedAttendees.length > 0){
          $scope.isSomethingChecked = true;
        } else {
          $scope.isSomethingChecked = false;
        }
      })

      $scope.openMoreOptions = function() {
        $scope.showMoreOptions = true;
      }

      $scope.closeMoreOptions = function() {
        $scope.showMoreOptions = false;
      }

      $scope.orderAtendeesByField = function(field) {
        if($scope.fieldCodeOrder == field.orderCode){
          $scope.sortReverse = !$scope.sortReverse;
        } else {
          $scope.fieldCodeOrder = field.orderCode;
          $scope.sortReverse = false;
        }
      }

      $scope.getAttendeeQrCode = function(attendee) {
        return attendee.attendee_id + ';' + attendee.first_name + ';' + attendee.last_name;
      }

      $scope.openAttendeeInfoModal = function(attendee) {
        var data = {};
        data.conference = conference;
        data.attendeeId = attendee.id;
        ngDialog.open({
          plain       : true,
          template    : '<attendee-info-modal></attendee-info-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/attendeeInfoModal/attendeeInfoModalStyles.scss',
          data        : data
        })
      }

      $scope.openQrCodeModal = function(attendee) {
        var data = {};
        data.qrCode = $scope.getAttendeeQrCode(attendee);
        ngDialog.open({
          plain       : true,
          template    : '<attendee-qr-code-modal></attendee-qr-code-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/attendeeQrCodeModal/attendeeQrCodeModalStyle.scss',
          data        : data
        })
      }

      $scope.openAddAttendeeModal = function() {
        var data = {};
        data.isRegistrantAttendeesView = $scope.isRegistrantAttendeesView;
        data.isScannedAttendeesView = $scope.isScannedAttendeesView;
        data.session = $scope.session;
        data.conference = conference;
        data.initialSessionAttendee = $scope.sessionAttendees;
        ngDialog.open({
          plain       : true,
          template    : '<add-attendee-modal></add-attendee-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/addAttendeeModal/addAttendeeModalStyles.scss',
          data        : data
        })
      }

      $rootScope.$on('ATTENDEES_ADDED', function(event, addedAttendees) {
        angular.forEach(addedAttendees, function(addedAttendee) {
          addedAttendee.attendee = addedAttendee;
        })
        $scope.sessionAttendees = $scope.sessionAttendees.concat(addedAttendees);
        $scope.sessionAttendeesTotal += addedAttendees.length;
        uncheckAll();
      })

      $scope.deleteAttendeesFromSession = function() {
        var data = {};
        data.checkedAttendees = $scope.checkedAttendees;
        data.areAllChecked    = $scope.areAllChecked;
        data.attendeeType     = $stateParams.attendees_type;
        data.sessionId        = $scope.session.id;
        ngDialog.open({
          plain       : true,
          template    : '<delete-attendee-from-session-modal></delete-attendee-from-session-modal>',
          className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/deleteAttendeeFromSessionModal/deleteAttendeeFromSessionModalStyles.scss',
          data        : data
        })
      }

      $rootScope.$on('ATTENDEES_DELETED', function(event, deletedAttendees) {
        uncheckAll();
        $scope.sessionAttendeesTotal -= deletedAttendees.length;
        angular.forEach(deletedAttendees, function(deletedAttendee) {
          angular.forEach($scope.sessionAttendees, function(sessionAttendee, index, model) {
            if(deletedAttendee.id == sessionAttendee.attendee.id){
              model.splice(index, 1);
            }
          })
        })
      });

      $rootScope.$on('ALL_ATTENDEES_DELETED', function(event, data) {
        uncheckAll();
        resetAttendees();
      });

      $scope.checkOne = function(attendee) {
        if(attendee.attendee.checked == true){
          var index = $scope.checkedAttendees.indexOf(attendee.attendee);
          $scope.checkedAttendees.splice(index, 1);
        } else {
          $scope.checkedAttendees.push(attendee.attendee);
        }
        attendee.attendee.checked = !attendee.attendee.checked;
      }

      $scope.checkAllAttendees = function() {
        $scope.checkedAttendees = [];
        angular.forEach($scope.sessionAttendees, function(attendee, key, model) {
          if($scope.areAllChecked == true){
            attendee.attendee.checked = false;
          } else {
            attendee.attendee.checked = true;
            $scope.checkedAttendees.push(attendee.attendee);
          }
        })
        $scope.areAllChecked = !$scope.areAllChecked;
      }

      $scope.searchSessionAttendees = function() {
          resetAttendees();
          var filters = {
            queryString   : $scope.queryModel || '',
            sortBy        : 'first_name',
            sortOrder     : "asc"
          };
          getSessionAttendees(filters);
      }

      $scope.getMoreSessionAttendees = function() {
          var filters = {
             queryString: $scope.queryModel || '',
             sortBy: 'first_name',
             sortOrder: "asc"
          }
          var page = $scope.sessionAttendeesPage;
          getSessionAttendees(filters, page);
      }

      function getSessionAttendees(filters, page) {
        if(!$scope.isProcessing){
          $scope.isProcessing = true;
          if(!filters){
            var filters = defaultFilters;
          }
          if(!page){
            var page = 1;
          }
          if($scope.isScannedAttendeesView){
            getSessionScannedAttendees(filters, page);
          } else if ($scope.isRegistrantAttendeesView){
            getSessionRegistrantAttendees(filters, page);
          }
        }
      }

      function getSessionDetails(sessionId) {
        sessionDetailsDataService
          .getSessionById(conference.conference_id, sessionId).then(function(response) {
            $scope.session = response;
            getSessionAttendees();
          }, function(error) {
            toastr.error("There was a problem with your request.", "Try again.");
          });
      }

      function getSessionRegistrantAttendees(filters, page) {
        sessionAttendeesDataService
         .getSessionRegistrantAttendees($scope.session.id, page, false, filters).then(function(data) {
           $scope.sessionAttendees = $scope.sessionAttendees.concat(data.sessions_attendees);
           $scope.sessionAttendeesTotal = data.total_count;
           $scope.sessionAttendeesPage++;
           uncheckAll();
           $scope.isProcessing = false;
          }, function(error) {
            $scope.isProcessing = false;
            toastr.error('There was a problem with your request.', 'Try again.');
          });
      }

      function getSessionScannedAttendees(filters, page) {
        sessionAttendeesDataService
         .getSessionScannedAttendees($scope.session.id, page, false, filters).then(function(data) {
           $scope.sessionAttendees = $scope.sessionAttendees.concat(data.scanned_attendees);
           $scope.sessionAttendeesTotal = data.total_count;
           $scope.sessionAttendeesPage++;
           uncheckAll();
           $scope.isProcessing = false;
          }, function(error) {
            $scope.isProcessing = false;
            toastr.error('There was a problem with your request.', 'Try again.');
          });
      }

      function uncheckAll() {
        angular.forEach($scope.sessionAttendees, function(sessionAttendee) {
          if(sessionAttendee.attendee.checked == true) {
            sessionAttendee.attendee.checked = false;
          }
        })
        $scope.checkedAttendees = [];
        $scope.areAllChecked = false;
      }

      function resetAttendees() {
        $scope.sessionAttendees = [];
        $scope.sessionAttendeesTotal = 0;
        $scope.sessionAttendeesPage = 1;
      }

      // LEGACY CODE:

      $scope.downloadSessionAttendeesAsCsv = function() {
        if($scope.isRegistrantAttendeesView){
          var type = 'Registered At';
          var type_by = 'Registered By';
          var type_url = 'registered_to_session';
          var scanned_again_at = null;
        } else if ($scope.isScannedAttendeesView){
          var type = 'Scan 1';
          var type_by = 'Scan 1 by';
          var type_url = 'scanned_in_session';
          var scanned_again_at = 'Scanned Again At';
        }
          var attendee_ids = [];
          var checkedAttendeeList = [];
          var list = $scope.sessionAttendees;
          var total_number = $scope.sessionAttendeesTotal;
          var extra = type_url + "=" + $scope.session.id;

          var promises = [];
          if (attendee_ids.length == 0) {
            var nr_pages = parseInt(total_number / 30) + 2;
            for (var idx = 1; idx <= nr_pages + 1; idx ++) {
              promises.push(sessionAttendeesDataService
                              .downloadAttendees(conference.conference_id, attendee_ids, extra, idx));
            }
          } else {
              promises.push(sessionAttendeesDataService
                              .downloadAttendees(conference.conference_id, attendee_ids, extra));
          }

          $q.all(promises).then(function(responses){
                  var allAttendees = [];
                  _.each(responses, function(attendees){
                          allAttendees = allAttendees.concat(attendees);
                  });

                  var checkedAttendeeList = [];
                  var customFields = [];
                  var sessionTitles = [];
                  _.each(allAttendees, function(attendee) {
                          var mainProfileAttendee = _.omit(attendee,
                          ['custom_fields' , 'sessionattendee_set', 'scannedattendee_set', 'customattendeefieldvalue_set',
                          'created_at', 'deleted_at', 'updated_at', 'conference_id', 'added_by', 'checked', '$$hashKey']);
                          _.forEach(attendee.customattendeefieldvalue_set, function(customField){
                                  mainProfileAttendee[customField.custom_attendee_field.name] = customField.value;
                                  customFields.push(customField.custom_attendee_field.name);
                          });
                          _.forEach(attendee.scannedattendee_set, function(scannedAttendee){
                              if (scannedAttendee.session.id == $scope.session.id) {
                                  var itemDate = new Date(scannedAttendee.session.starts_at);
                                  var scannedAt = new Date(scannedAttendee.scanned_at);
                                  var actualDate = $filter('date')(itemDate, "MM/dd/yyyy - hh:mm a", $rootScope.conference.time_zone);
                                  var scannedDate = $filter('date')(scannedAt, "MM/dd/yyyy hh:mm a", $rootScope.conference.time_zone);

                                  mainProfileAttendee[type] = scannedDate;
                                  mainProfileAttendee[type_by] = scannedAttendee.added_by.first_name + ' ' + scannedAttendee.added_by.last_name;
                                  sessionTitles.push(type);
                                  sessionTitles.push(type_by);

                                  if (scanned_again_at != null) {

                                      var nr = 2;

                                      if (scannedAttendee.scannedattendeeadditional_set && scannedAttendee.scannedattendeeadditional_set.length == 0) {
                                          _.each(scannedAttendee.scanned_again_at_array, function(scannedAtAgain){
                                              var scannedAtAgainTXT = $filter('date')(scannedAtAgain, "MM/dd/yyyy - hh:mm a", $rootScope.conference.time_zone);
                                              var scannedAtAgainTXTBy =  ' ';
                                              var fieldName = "Scan " + nr;
                                              mainProfileAttendee[fieldName] = scannedAtAgainTXT;
                                              sessionTitles.push(fieldName);
                                              var fieldName2 = "Scan " + nr + " by";
                                              mainProfileAttendee[fieldName2] = scannedAtAgainTXTBy;
                                              sessionTitles.push(fieldName2);
                                              nr ++;
                                          });
                                      } else {
                                          _.each(scannedAttendee.scannedattendeeadditional_set, function(scannedAtAgain){
                                              var scannedAtAgainTXT = $filter('date')(scannedAtAgain.scanned_at, "MM/dd/yyyy - hh:mm a", $rootScope.conference.time_zone);
                                              var scannedAtAgainTXTBy =  scannedAtAgain.added_by.first_name + ' ' +
                                                  scannedAtAgain.added_by.last_name;
                                              var fieldName = "Scan " + nr;
                                              mainProfileAttendee[fieldName] = scannedAtAgainTXT;
                                              sessionTitles.push(fieldName);
                                              var fieldName2 = "Scan " + nr + " by";
                                              mainProfileAttendee[fieldName2] = scannedAtAgainTXTBy;
                                              sessionTitles.push(fieldName2);
                                              nr ++;
                                          });
                                      }
                                  }
                              }
                          });
                          delete mainProfileAttendee["id"]; // ???
                          mainProfileAttendee['{BCT#QRCode}'] = $scope.getAttendeeQrCode(attendee);
                          checkedAttendeeList.push(mainProfileAttendee);

                  });
                  var fields = ['company_name', 'first_name', 'last_name', 'job_title', 'email_address', 'phone_number',
                  'street_address', 'city', 'zip_code', 'state', 'country', 'attendee_id', '{BCT#QRCode}'];
                  customFields = _.uniq(customFields);
                  sessionTitles = _.uniq(sessionTitles);
                  fields = fields.concat(customFields);
                  fields = fields.concat(sessionTitles);
                  $scope.printCsvListFile(checkedAttendeeList, fields);
          },function(error) {
            toastr.error('There has been a problem with your request');
          });
      }

      $scope.printCsvListFile = function(checkedAttendeeList, fields) {
              var csvData =  lib.ConvertToCSVImproved(checkedAttendeeList, fields);

              var anchor = angular.element('<a/>');
              anchor.css({display: 'none'}); // Make sure it's not visible
              angular.element(document.body).append(anchor); // Attach to document

              var date = new Date();

              var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
              var filename = conference.name + actualDate + ".csv";

              var url = URL.createObjectURL( new Blob( [csvData], {type:'application/csv'} ) );
              anchor.attr({
                      href: url,
                      target: '_blank',
                      download: filename
              })[0].click();

              anchor.remove();
      }

    }
})();
