(function(){
	'use strict';

    var filterId = 'sessionTypeFilter';

    angular
  		.module('app')
  		.filter(filterId, function() {

        return function(sessionType) {
          var formattedType;
          var sessionTypes = [
						{id: 'individual_presentation',           name: 'Presentation'},
	          {id: 'interactive_roundtable_discussion', name: 'Interactive Roundtable Discussion'},
	          {id: 'panel_discussion',                  name: 'Panel Discussion'},
	          {id: 'workshop',                          name: 'Workshop'},
	          {id: 'pre_conference_workshop',           name: 'Pre-Conference Workshop'},
	          {id: 'breakout',                          name: 'Breakout'},
	          {id: 'networking',                        name: 'Networking'},
	          {id: 'reception',                         name: 'Reception'},
	          {id: 'breakfast',                         name: 'Breakfast'},
	          {id: 'lunch',                             name: 'Lunch'},
	          {id: 'dinner',                            name: 'Dinner'},
	          {id: 'gala',                              name: 'Gala'},
	          {id: 'keynote',                           name: 'Keynote'},
	          {id: 'fitness',                           name: 'Fitness'},
	          {id: 'registration',                      name: 'Registration'},
	          {id: 'showcase',                          name: 'Showcase'},
						{id: 'general',                           name: 'General'}
          ];

          angular.forEach(sessionTypes, function(value, key, model) {
            if(value.id == sessionType){
              formattedType =  value.name;
            }
          })

          return formattedType;
        }

      });
})();
