(function(){
  'use strict';

  var directiveId = 'refundPaymentModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', 'toastr', 'sessionAttendeesDataService', refundPaymentModal]);

    function refundPaymentModal($rootScope, toastr, sessionAttendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/_components/refundPaymentModal/refundPaymentModalView.html',
          link: link
      }

      function link(scope){

        var initialRefundAmount           = scope.ngDialogData.refundAmount;

        scope.attendee                    = scope.ngDialogData.attendee;
        scope.attendeePayments            = scope.ngDialogData.attendeePayments;
        scope.refundAmount                = scope.ngDialogData.refundAmount;
        scope.sendNotificationForRefund   = false;
        scope.lockRefundButton            = false;

        scope.forceMaxRefundAmount = function() {
          var maxAmount = scope.attendeePayments.amount;
          _.each(scope.attendeePayments.refunds, function(refund){
            maxAmount -= refund.amount;
          })
          if (scope.refundAmount > maxAmount) {
            scope.refundAmount = maxAmount;
          }
        }

        scope.refundPayment = function(){
          if(scope.refundAmount > 0){
            if (!scope.lockRefundButton) {
              scope.lockRefundButton = true;
              sessionAttendeesDataService
                .refundAttendeePayment(scope.attendee.id, scope.refundAmount).then(function(response) {
                  postRefundProcessing(response, scope.attendee);
                  scope.lockRefundButton = false;
                  scope.closeThisDialog();
                }, function(error) {
                  scope.lockRefundButton = false;
                  scope.closeThisDialog();
                });
              }
            } else {
              toastr.error('You can not refund $' + scope.refundAmount + ' !');
              return;
            }
        };

        function postRefundProcessing(financial_data, attendee) {
          var refundData              = {};
          refundData.attendee         = attendee;
          refundData.attendeePayments = financial_data;
          $rootScope.$broadcast('ATTENDEE_REFUNDED', refundData);
        }


      }

    }
})();
