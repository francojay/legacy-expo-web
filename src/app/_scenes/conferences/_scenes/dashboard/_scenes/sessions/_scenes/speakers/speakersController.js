(function () {
    'use strict';

    var controllerId = 'speakersController';
    var app          = angular.module('app');

    var dependenciesArray = [
                          '$scope',
                          '$rootScope',
                          '$q',
                          'toastr',
                          'fileService',
                          'speakerDataService',
                          'intlTelInputOptions',
                          'API',
                          speakersController
                        ];
    app.controller(controllerId, dependenciesArray);

    function speakersController($scope, $rootScope, $q, toastr, fileService, speakerDataService, intlTelInputOptions, API) {

          var conference                  = $rootScope.conference;

          $scope.conferenceSpeakersList   = {};
          $scope.conferenceSpeakerModel   = {};
          $scope.conferenceSpeakerFields  = [
            {label :'First Name',    code : 'first_name'},
            {label :'Last Name',     code : 'last_name'},
            {label :'Company',       code : 'company'},
            {label :'Job Title',     code : 'job_title'},
            {label :"Email Address", code : 'email'},
            {label :"Phone Number",  code : 'phone_number'},
            {label :"LinkedIn url",  code : 'linkedin_url'},
            {label :"Facebook url",  code : 'facebook_url'},
            {label :"Twitter url",   code : 'twitter_url'},
            {label :"Youtube url",   code : 'youtube_url'},
            {label :"Biography",     code : 'biography'}
          ];
          $scope.editSpeakerProfileMode   = false;
          $scope.defaultCountryFlag = 'us';
          angular.forEach(API.COUNTRIES, function(country) {
            if(country.name === $rootScope.conference.country){
              $scope.defaultCountryFlag = country.code.toLowerCase();
            }
          });
          getConferenceSpeakersList(conference.conference_id);

          $scope.uploadSpeakerProfilePhoto = function(files) {
            lib.squareifyImage($q, files).then(function(response) {
              var imgGile = response.file;

              fileService.getUploadSignature(imgGile).then(function(response) {
                var file_link = response.data.file_link;
                var promises = [];
                promises.push(fileService.uploadFile(imgGile, response));

                $q.all(promises).then(function(response) {
                  $scope.conferenceSpeakerModel.headshot_url = file_link;
                }, function(error) {
                  toastr.error('There has been a problem with your request.', 'Try again.');
                });

              }, function(error) {
                if (error.status == 403) {
                  toastr.error('Permission Denied!');
                } else {
                  toastr.error('There has been a problem with your request.', 'Try again.');
                }
              });

            }, function(error) {
              toastr.error('There has been a problem with your request.', 'Try again.');
            });
          };

          $scope.createOrUpdateSpeaker = function(conferenceSpeakerModel) {
            var errors = false;
            if (!conferenceSpeakerModel.first_name || !conferenceSpeakerModel.last_name) {
              toastr.error("Fill in the required Speaker fields before saving!");
              errors = true;
              return false;
            }
            if ((conferenceSpeakerModel.hasOwnProperty('phone_number') && conferenceSpeakerModel.phone_number === undefined) || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1){
              toastr.error("Please fill the phone number with a correct value!");
              errors = true;
              return false;
            }
            if(!errors){
              if (conferenceSpeakerModel.id) {
                updateSpeaker(conferenceSpeakerModel, conference.conference_id);
              } else {
                createSpeaker(conferenceSpeakerModel, conference.conference_id);
              }
            }
          };

          $scope.editSpeakerProfile = function(speaker) {
            $scope.conferenceSpeakerModel = angular.copy(speaker);
            $scope.editSpeakerProfileMode = true;
          };

          $scope.resetFields = function() {
            $scope.conferenceSpeakerModel = {};
            $scope.editSpeakerProfileMode = false;
          }

          $scope.removeConferenceSpeaker = function(speaker) {
            speakerDataService
              .deleteConferenceSpeaker(speaker).then(function(response) {
                var index = $scope.conferenceSpeakersList.speakers.indexOf(speaker);
                $scope.conferenceSpeakersList.speakers.splice(index,1);
                if($scope.conferenceSpeakerModel.id == speaker.id) {
                  $scope.resetFields();
                }
                toastr.success('Conference speaker removed.');
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          };

          function getConferenceSpeakersList(conferenceId) {
            speakerDataService
              .getConferenceSpeakersList(conferenceId).then(function(response) {
                $scope.conferenceSpeakersList = response;
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          };

          function createSpeaker(conferenceSpeakerModel, conferenceId) {
            speakerDataService
              .createConferenceSpeaker(conferenceSpeakerModel, conferenceId).then(function(response) {
                $scope.conferenceSpeakersList.speakers.push(response);
                $scope.resetFields();
                toastr.success('Conference Speaker added.')
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          };

          function updateSpeaker(conferenceSpeakerModel, conferenceId) {
            speakerDataService
              .updateConferenceSpeaker(conferenceSpeakerModel, conferenceId).then(function(response) {
                angular.forEach($scope.conferenceSpeakersList.speakers, function(value, key, model) {
                  if(value.id == conferenceSpeakerModel.id){
                    model[key] = conferenceSpeakerModel;
                  }
                });
                $scope.resetFields();
                toastr.success('Conference Speaker updated.')
              }, function(error) {
                toastr.error('There has been a problem with your request.', 'Try again.');
              });
          };

    }
})();
