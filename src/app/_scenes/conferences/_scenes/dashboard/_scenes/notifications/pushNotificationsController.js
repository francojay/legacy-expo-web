(function () {
    'use strict';

    var controllerId      = 'pushNotificationsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'pushNotificationsDataService',
      '$state',
      pushNotificationsController
    ]

    app.controller(controllerId, dependenciesArray);

    function pushNotificationsController($scope, $rootScope, $stateParams, toastr, ngDialog, pushNotificationsDataService, $state) {
      if (!$rootScope.userPermissions.editConference) {
        toastr.error("You don't have permissions to edit push notifications!");
        $state.go('conferences.dashboard.details.overview', {'conference_id': $stateParams.conference_id});
        return
      }
      $scope.loadingNotificationsStatus = true;
      var conferenceId = $rootScope.conference.conference_id;
      checkStatusOfNotificationSetup(conferenceId);
      function checkStatusOfNotificationSetup(conferenceId) {
          pushNotificationsDataService.checkStatusOfNotificationSetup(conferenceId).then(function(response) {
              if (!response.data.has_setup_notifications) {
                  var obj = {
                    discountPercentage: response.data.bulk_discount_percentage,
                    oneBatchPrice     : response.data.price_per_1000_credits,
                  }
                  $state.go('conferences.dashboard.notifications.setup', {
                      conference_id: $stateParams.conference_id,
                      obj: obj
                  })
              } else {
                  $scope.totalCredits = response.data.nr_remaining_credits;
                  $scope.remainingCredits = $scope.totalCredits;

                  var obj = {
                    totalCredits : response.data.nr_remaining_credits,
                    remainingCredits : response.data.nr_remaining_credits
                  }

                  $state.go('conferences.dashboard.notifications.manage', {
                      conference_id: $stateParams.conference_id,
                      obj: obj
                  })

              }
          }, function(error) {

          })
      }
    }
})();
