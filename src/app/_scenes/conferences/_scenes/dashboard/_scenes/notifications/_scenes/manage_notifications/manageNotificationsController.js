(function () {
    'use strict';

    var controllerId      = 'manageNotificationsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'pushNotificationsDataService',
      'conferenceSessionsDataService',
      'speakerDataService',
      'exhibitorsDataService',
      '$state',
			'EmojiGroups',
			'EmojiHex',
      '$timeout',
      'formatRawTimeService',
      manageNotificationsController
    ]

    app.controller(controllerId, dependenciesArray);

    function manageNotificationsController($scope, $rootScope, $stateParams, toastr, ngDialog, pushNotificationsDataService, conferenceSessionsDataService, speakerDataService, exhibitorsDataService, $state, EmojiGroups, EmojiHex, $timeout, formatRawTimeService) {
			if (!$stateParams.obj) {
				$state.go('conferences.dashboard.notifications.overview', {
						conference_id: $stateParams.conference_id
				});
			} else {
				$scope.remainingCredits                   = $stateParams.obj.remainingCredits;
				$scope.totalCredits                       = $stateParams.obj.totalCredits;
        $scope.conferencePushNotificationPayments = null;
				var regex                                 = new RegExp(':(' + EmojiGroups.all.join('|') + '):', 'g');
				var regexHex                              = new RegExp('(' + getUnicodes().join('|') + ')', 'g');
        var conference                            = $rootScope.conference;
				var conferenceId                          = $rootScope.conference.conference_id;
        var swappedHex                            = getSwappedHex();

        var sessionsRetrieved   = false;
        var speakersRetrieved   = false;
        var exhibitorsRetrieved = false;
        getRegistrationLevels(conferenceId);
        getScheduledNotifications(conferenceId);
        getSentNotifications(conferenceId);
        getSessions(conferenceId);
        getSpeakers(conferenceId);
        getExhibitors(conferenceId);

				$scope.stringifyEmoji = function(text) {
				    return swappedHex[hexify(text)];
				}

				var linkId = 0;

				$scope.verifySendTime = function() {
				    if ($scope.sendDate < $scope.notificationMinTime) {
				        $scope.sendDate = $scope.notificationMinTime;
				    }
				}

				$scope.hasTargetSelectorNotification = function() {

				    if ((!$scope.createdRegistrationLevels || $scope.createdRegistrationLevels.length <= 0) &&
				            (!$scope.customFieldSendToOptions || $scope.customFieldSendToOptions.length <= 0)) {
				        return false;
				    }
				    return true;
				}

				$scope.initNotification = function() {
				    $scope.linkOptionActive = false;
            $scope.model = {
              selectedTarget: 'session'
            }
				    $scope.editScheduledNotificationId = null;
				    $scope.audienceSize = 0;
				    $scope.necessaryCreditsToCompleteSchedule = 0;
				    $scope.sendDate = null;
				    $scope.sendDateEditable = null;
				    var offsetInMinutes = lib.timezoneToOffset($scope.conference.time_zone);
				    setInterval(function(){
				      $scope.notificationMinTime = new Date(new Date().getTime() + 5*60000);
				      $scope.notificationMinTime.setTime(
				            $scope.notificationMinTime.getTime() + ($scope.notificationMinTime.getTimezoneOffset() + offsetInMinutes)*60*1000
				      );
				      $scope.notificationMinTime.setSeconds(0);
				      $scope.notificationMinTime.setMilliseconds(0);
				      $scope.notificationMinTimeMoment = moment($scope.notificationMinTime);
				      $scope.$apply();
				    }, 60000);

				    $scope.sendNotificationToAll = false;
				    $scope.notificationTextContent = "";
				    $scope.notificationMinTime = new Date(new Date().getTime() + 5*60000);
				    $scope.notificationMinTime.setTime(
				        $scope.notificationMinTime.getTime() + ($scope.notificationMinTime.getTimezoneOffset() + offsetInMinutes)*60*1000
				    );
				    $scope.notificationMinTime.setSeconds(0);
				    $scope.notificationMinTime.setMilliseconds(0);

				    $scope.notificationMinTimeMoment = moment($scope.notificationMinTime);

            $scope.sendOptionsList = [];
				    $scope.customFieldSendToOptions = [];
				    $scope.customFieldSendToList = [];
				    $scope.customFieldOptions = {};
				    $scope.customFieldKeys = [];

				    _.each($scope.conference.custom_fields, function(custom_field) {
				        if (custom_field.type != 1) {
				            $scope.customFieldSendToList.push(
				                {
				                    "id": custom_field.id,
				                    "label": custom_field.name
				                }
				            );

				            $scope.customFieldKeys.push('custom_' + custom_field.id);
				            $scope.customFieldOptions['custom_' + custom_field.id] = [];
				            _.each(custom_field.options, function(option) {
				                $scope.customFieldSendToOptions.push(
				                    {
				                        "label": custom_field.name + ': ' + option.value,
				                        "id": option.id,
				                    }
				                );

				                $scope.customFieldOptions['custom_' + custom_field.id].push(
				                    {
				                        "label": option.value,
				                        "id": option.id,
				                    }
				                );
				            })
				        }
				    });

				    $scope.plusBtnLeft = '307px';

				    linkId = 0;

				    $scope.targetSelectors = [
				        {
				            id: 1
				        }
				    ];
				}

				$scope.initNotification();

				$scope.calculateCharacterCounter = function() {
				    if (!$scope.notificationTextContent) return;
				    $scope.checkCharacterCounter($scope.notificationTextContent, 140);
				}

				$scope.checkCharacterCounter = function(string, maximumLength) {
				    $scope.characterCount = _.size(string);

				    if (_.size(string) <= maximumLength) {
				        var leftCharacters = maximumLength - _.size(string);

				        if (leftCharacters < 10) {
				            $scope.lessThanTenCharactersLeft = true;
				        } else {
				            $scope.lessThanTenCharactersLeft = false;
				        }
				    } else {
				        var exceedCharactersCount = maximumLength - _.size(string);
				        string = string.slice(0, exceedCharactersCount);

				        $scope.notificationTextContent = string;
				        $scope.characterCount = _.size(string);
				        $scope.lessThanTenCharactersLeft = true;
				    }
				}

				$scope.changeLinkType = function(selectedTarget) {
			    if (selectedTarget == 'session') {
			        $scope.selectedTargetItemId = $scope.sessions[0].id;
			    } else if (selectedTarget == 'exhibitor') {
			        $scope.selectedTargetItemId = $scope.exhibitors[0].id;
			    } else {
			        $scope.selectedTargetItemId = $scope.speakers[0].id;
			    }
          linkId = $scope.selectedTargetItemId
				}

        $scope.checkLinkOption = function(optionState) {
          if (sessionsRetrieved && exhibitorsRetrieved && speakersRetrieved) {
            $scope.linkOptionActive = !optionState;
            $scope.model.selectedTarget = "session";
            $scope.selectedTargetItemId = $scope.sessions[0].id;
  			    linkId = $scope.sessions[0].id;
          }
				}

				$scope.addNewTargetSelector = function() {
				    var newObject = {
				        id: $scope.targetSelectors.length + 1
				    };
				    $scope.plusBtnLeft = '307px';
				    $scope.targetSelectors.push(newObject)
				}

        $scope.removeNewTargetSelector = function(target) {
				    angular.forEach($scope.targetSelectors, function(targetSelector, index) {
				        if (target.$$hashKey === targetSelector.$$hashKey) {
				            $scope.targetSelectors.splice(index, 1);
				            $scope.requestCostAll($scope.targetSelectors);
				        }
				    });

				    if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'registration_level') {
				        $scope.plusBtnLeft = '436px';
				    }

				    if ($scope.customFieldKeys.indexOf($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType) > -1) {
				        $scope.plusBtnLeft = '436px';
				    }

				    if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'custom_field_value') {
				        $scope.plusBtnLeft = '436px';
				    }
				}
				$scope.processPushNotificationPayments = function(transactions) {
				    var payments = [];

				    _.each(transactions, function(transaction) {
				        payments.push({
				            'checked': transaction.checked,
				            'Date': $filter('date')(transaction.created_at, "MM/dd/yyyy - hh:mm a"),
				            'Order': transaction.nr_credits + " Push Notification Credits",
				            'Last 4': transaction.payment.last4,
				            'Amount': $filter('currency')(transaction.payment.amount / 100, '$')
				        });
				    });

				    return payments;
				}
				$scope.downloadPushPaymentsAsCsv = function(transactions) {
				    lib.downloadTransactionsAsCsv($scope.processPushNotificationPayments(transactions), $scope.conference);
				}
				$scope.printPaymentsAsCsv = function(transactions) {
				    lib.printTransactionsAsCsv($scope.processPushNotificationPayments(transactions), $scope.conference);
				}

				$scope.openPushNotificationPopup = function() {
				    $scope.checkOne = $scope.checkOne;
				    $scope.checkAll = $scope.checkAll;
				    $scope.checkAllBool = $scope.checkAllPushNotificationPayments;
				    $scope.printTransactionsAsCsv = $scope.printPaymentsAsCsv;
				    $scope.downloadTransactionsAsCsv = $scope.downloadPushPaymentsAsCsv;
				    $scope.conferencePushNotificationPayments = $scope.conferencePushNotificationPayments;
				    ngDialog.open({
              plain: true,
              template: '<push-notifications-receipts-modal></push-notifications-receipts-modal>',
			        className: 'app/stylesheets/_createLabelModal.scss',
				      data      : {
                transactions : $scope.conferencePushNotificationPayments
              }
				    });
				}

				$scope.loadPushNotificationPayments = function() {
				    if ($scope.conferencePushNotificationPayments) {
				        $scope.openPushNotificationPopup();
				    } else {
				        pushNotificationsDataService.getPayments(conferenceId)
				            .then(function(response){
				                $scope.conferencePushNotificationPayments = response.data.custom_push_notification_payments;
				                $scope.openPushNotificationPopup();
				            }).catch(function(error){

				            });
				    }

				}

				$scope.showTargetSelectorNotification = function(){
				    if ($scope.targetSelectors && $scope.targetSelectors.length <= 0) {
				        return true;
				    }
				    var showLevel = $scope.showNotificationLevelOption($scope.targetSelectors[$scope.targetSelectors.length - 1]);
				    var showOption = $scope.showNotificationFieldOption($scope.targetSelectors[$scope.targetSelectors.length - 1]);
				    if (!showLevel && !showOption) {
				        return false;
				    }

				    return true;
				}

				$scope.canAddTargetSelectorNotification = function() {

            if(!$scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType && !$scope.targetSelectors[$scope.targetSelectors.length - 1].custom_push_send_to){
              return false;
            }
            if($scope.targetSelectors && $scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'send_to_everyone'){
              return false;
            }
            var hasRemainingOptions = false;
            var wasAttendeesSet = false;
            var allOptionsSet = 0;
				    _.each($scope.targetSelectors, function(savedSelector) {
              if(['send_to_organizers','send_to_exhibitors','send_to_attendees'].indexOf(savedSelector.sendToType) > -1){
                allOptionsSet++;
              }
              if(savedSelector.sendToType == 'send_to_attendees' && !wasAttendeesSet){
                wasAttendeesSet = true;
              }
            });
            if(!wasAttendeesSet){
  				    var showLevel = $scope.hasRemainingLevel();
  				    var showOption = $scope.hasRemainingOptions();
  				    if (!showLevel && !showOption && allOptionsSet == 2) {
  				        return false;
  				    }
            }
            if(wasAttendeesSet && allOptionsSet == 3){
              return false;
            }


				    if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'registration_level') {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1].selectedLevel) {
				            return true;
				        }
				    }

				    if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'custom_field_value') {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1].selectedOption) {
				            return true;
				        }
				    }

				    if ($scope.customFieldKeys.indexOf($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType) > -1) {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1].selectedOption) {
				            return true;
				        }
				    }

				    if ($scope.customFieldKeys.indexOf($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType) > -1) {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1].selectedOption) {
				            return true;
				        }
				    }
            if($scope.targetSelectors && $scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType != 'send_to_everyone'){
              return true;
            }


            return false;

				}

				$scope.scheduleNotification = function(notificationText, time, receivers) {
				    if (!$scope.sendDateEditable) {
				        toastr.error('You need to set a date for the notification!');
				        return;
				    }

				    var sendDate = new Date($scope.sendDateEditable);
				    var objectToSend = {
				        text: unicodify(hexify(notificationText)),
				        send_at: sendDate,
				        send_at_raw: {
				            "year": sendDate.getFullYear(),
				            "month": sendDate.getMonth() + 1,
				            "day": sendDate.getDate(),
				            "hour": sendDate.getHours(),
				            "minute": sendDate.getMinutes(),
				        },
				        id: $scope.editScheduledNotificationId,
				        custom_push_send_to: [],
				        send_to_all: false
				    };

				    if (!objectToSend.text || objectToSend.length < 5) {
				        toastr.error('The notification message should have at least 5 characters!');
				        return;
				    }

				    if (!objectToSend.send_at_raw) {
				        toastr.error('You need to set a date for the notification!');
				        return;
				    }

				    angular.forEach(receivers, function(receiver) {
                switch (receiver.sendToType) {
                  case 'send_to_exhibitors': objectToSend.send_to_exhibitors = true;break;
                  case 'send_to_attendees': objectToSend.send_to_attendees = true;break;
                  case 'send_to_organizers': objectToSend.send_to_organizers = true;break;
                  case 'send_to_everyone':objectToSend.send_to_everyone = true;break;
                }
				        if (receiver.selectedLevel) {
				            var serverSideTypeObject = {
				                send_type: 'registration_level',
				                send_resource_id: parseInt(receiver.selectedLevel)
				            }
				            objectToSend.custom_push_send_to.push(serverSideTypeObject);
				        } else if (receiver.selectedOption) {
				            var serverSideTypeObject = {
				                send_type: 'custom_field_value',
				                send_resource_id: parseInt(receiver.selectedOption)
				            }
				            objectToSend.custom_push_send_to.push(serverSideTypeObject);
				        }

				    });

				    if ($scope.sendNotificationToAll) {
				        objectToSend.send_to_all = true;
				    }

				    if (objectToSend.custom_push_send_to.length == 0 && !objectToSend.send_to_exhibitors &&
            !objectToSend.send_to_attendees && !objectToSend.send_to_organizers && !objectToSend.send_to_everyone) {
				        toastr.error("You need to select at least one 'Send To' option!");
				        return;
				    }

				    if ($scope.linkOptionActive) {
			        objectToSend.link_type = $scope.model.selectedTarget;
			        objectToSend.link_id = linkId;
				    }

				    validateNotificationContent(objectToSend);


				    if ($scope.moreCreditsAreNecessary) {
				        toastr.error('Please buy more credits to schedule this notification.')
				    } else {
				        pushNotificationsDataService.scheduleNotification(conferenceId, objectToSend).then(function(response) {

				            var notification = response.data;
				            $scope.characterCount = 0;
				            $scope.lessThanTenCharactersLeft = false;
				            $scope.errorOnText = null;
				            notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a');
                    var temporaryDateToBeConvertedToCorrectTimezone = formatRawTimeService.createMomentFromRawTime(notification.send_at_raw);
                    notification.send_at = moment(temporaryDateToBeConvertedToCorrectTimezone).format();
				            if (objectToSend.id) {
				                _.each($scope.scheduledNotifications, function(value, key, obj) {
				                    if (value.id === objectToSend.id) {
				                        $scope.scheduledNotifications[key] = notification;
				                        toastr.success('The scheduled custom push notification was updated!');
				                    }
				                });
				            } else {
				                notification.nr_sent = response.cost;
				                notification.nr_seen = 0;
				                $scope.usedCreditsPercentage = 0;
				                $scope.totalCredits -= $scope.audienceSize;
				                $scope.remainingCredits = $scope.totalCredits;
				                $scope.audienceSize = 0;
				                $scope.scheduledNotifications.push(notification);
				                $scope.scheduledNotifications = _.orderBy($scope.scheduledNotifications, 'send_at', 'desc');
				                if (!notification.was_processed) {
				                    $timeout(function() {
				                      $scope.checkNotificationStatus(notification);
				                    }, 5000);
				                }
				                toastr.success('The custom push notification was scheduled!');
				            }

				            $scope.editScheduledNotificationId = null;
				            $scope.initNotification();

				        }, function(error) {
                  toastr.error("An error has occured while trying to create your notification!");
				        });
				    }

				}

				$scope.deleteNotificationConfirmed = function(notification) {
				    var conference_id = conferenceId;
				    pushNotificationsDataService.removeScheduledNotification(conference_id, notification)
				        .then(function(response){
				            $scope.scheduledNotifications  = _.reject($scope.scheduledNotifications, ['id', notification.id]);
				        }).catch(function(error){

				        });
				}

				$scope.cancelEditNotification = function() {
				    $scope.initNotification();
				}

				$scope.hasRemainingOptions = function() {
				    if (!$scope.targetSelectors || !$scope.customFieldKeys) {
				        return false;
				    }

				    var doNotShow = false;
				    var levelIds = [];
				    _.each($scope.targetSelectors, function(savedSelector) {
				        if ($scope.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
				            if (savedSelector.selectedOption) {
				                levelIds.push(savedSelector.selectedOption);
				            }
				        }
				    });
				    if (levelIds.length == $scope.customFieldSendToOptions.length) {
				        return false;
				    }
				    return true;
				}

				$scope.hasRemainingLevel = function() {
				    if (!$scope.targetSelectors || !$scope.createdRegistrationLevels) {
				        return false;
				    }
				    var doNotShow = false;
				    var levelIds = [];
				    _.each($scope.targetSelectors, function(savedSelector) {
				        if (savedSelector.sendToType == 'registration_level') {
				            if (savedSelector.selectedLevel && savedSelector.selectedLevel) {
				                levelIds.push(savedSelector.selectedLevel);
				            }
				        }

				    });

				    if (levelIds.length == $scope.createdRegistrationLevels.length) {
				        return false;
				    }

				    return true;
				}

				$scope.showAllNotificationOption = function(targetSelector) {
				    if (!$scope.targetSelectors) {
				        return false;
				    }

				    var doNotShow = false;
				    var levelIds = [];

				    _.each($scope.targetSelectors, function(savedSelector) {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector) {
				            if (savedSelector.sendToType == 'registration_level') {
				                if (savedSelector.selectedLevel) {
				                    levelIds.push(savedSelector.selectedLevel);
				                }
				            }
				        }

				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector) {
				            if ($scope.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
				                if (savedSelector.selectedOption) {
				                    levelIds.push(savedSelector.selectedOption);
				                }
				            }
				        }


				    });

				    if (levelIds.length > 0) {
				        return false;
				    }

				    return true;

				}

        $scope.showOtherNotificationOption = function(targetSelector,option){

          var levelIds = [];
          var found = false;

          if (targetSelector.sendToType == option) {
              found = true;

              if (found) {
                  return found;
              }
          }
          
          if (targetSelector == $scope.targetSelectors[$scope.targetSelectors.length - 1] || !found) {
              var doNotShow = false;
              _.each($scope.targetSelectors, function(savedSelector,index) {
                  if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType != savedSelector.sendToType || !found) {

                          if (savedSelector.sendToType && savedSelector.sendToType == option && option != 'send_to_attendees') {
                              doNotShow = true;
                          }
                          if(option == 'send_to_attendees'){
                            if (savedSelector.sendToType && savedSelector.sendToType == option) {
                                doNotShow = true;
                            }
                            if (savedSelector.sendToType == 'registration_level') {
        				                if (savedSelector.selectedLevel) {
        				                    levelIds.push(savedSelector.selectedLevel);
        				                }
        				            }
                            if ($scope.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
        				                if (savedSelector.selectedOption) {
        				                    levelIds.push(savedSelector.selectedOption);
        				                }
        				            }
                          }
                  }
              });
              if (levelIds.length > 0) {
  				        return false;
  				    }

              return !doNotShow;
          } else {
              return true;
          }
        }

				$scope.showNotificationLevelOption = function(targetSelector) {

				    if (!$scope.targetSelectors || !$scope.createdRegistrationLevels ) {
				        return false;
				    }

				    if (targetSelector.sendToType && targetSelector.selectedLevel) {
				        return true;
				    }

				    var doNotShow = false;
				    var levelIds = [];
				    _.each($scope.targetSelectors, function(savedSelector) {
                if(savedSelector.sendToType == 'send_to_attendees'){
                  doNotShow = true;
                }
				        if (($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector || !targetSelector.selectedLevel) && !doNotShow) {
				            if (savedSelector.sendToType == 'registration_level') {
				                if (savedSelector.selectedLevel && savedSelector.selectedLevel) {
				                    levelIds.push(savedSelector.selectedLevel);
				                }
				            }
				        }
				    });

				    if (doNotShow || levelIds.length == $scope.createdRegistrationLevels.length) {
				        return false;
				    }

				    return true;
				}

				$scope.showNotificationLevel = function(targetSelector, level) {
				    var levelIds = [];
				    var found = false;
				    if (targetSelector.sendToType && targetSelector.selectedLevel) {
				        if (level.id == targetSelector.selectedLevel) {
				            found = true;
				        }

				        if (found) {
				            return found;
				        }
				    }

				    if (targetSelector == $scope.targetSelectors[$scope.targetSelectors.length - 1] || !found) {
				        var doNotShow = false;
				        _.each($scope.targetSelectors, function(savedSelector) {
				            if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector || !found) {
				                if (savedSelector.sendToType == 'registration_level') {
				                    if (savedSelector.selectedLevel && savedSelector.selectedLevel == level.id) {
				                        doNotShow = true;
				                    }
				                }
				            }

				        });

				        return !doNotShow;
				    } else {
				        return true;
				    }

				}

				$scope.showNotificationOption = function(targetSelector, option) {

				    if (targetSelector == $scope.targetSelectors[$scope.targetSelectors.length - 1]) {
				        var doNotShow = false;
				        _.each($scope.targetSelectors, function(savedSelector) {
				            if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector) {
				                if (savedSelector.sendToType == 'custom_field_value') {
				                    if (savedSelector.selectedOption && savedSelector.selectedOption == option.id) {
				                        doNotShow = true;
				                    }
				                }
				            }

				        });

				        return !doNotShow;
				    } else {
				        return true;
				    }

				}

				$scope.showNotificationCustomOption = function(targetSelector, option) {

				    var levelIds = [];
				    var found = false;

				    if (targetSelector.sendToType && targetSelector.selectedOption) {
				        if (option.id == targetSelector.selectedOption) {
				            found = true;
				        }

				        if (found) {
				            return found;
				        }
				    }

				    if (targetSelector == $scope.targetSelectors[$scope.targetSelectors.length - 1] || !found) {
				        var doNotShow = false;
				        _.each($scope.targetSelectors, function(savedSelector) {
				            if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector || !found) {
				                if ($scope.customFieldKeys.indexOf(targetSelector.sendToType) > -1) {
				                    if (savedSelector.selectedOption && savedSelector.selectedOption == option.id) {
				                        doNotShow = true;
				                    }
				                }
				            }

				        });

				        return !doNotShow;
				    } else {
				        return true;
				    }

				}

				$scope.showSendToOption = function(targetSelector) {
				    if (!$scope.showNotificationFieldOption(targetSelector) && !$scope.showNotificationLevelOption(targetSelector)) {
				        return false;
				    }

				    return true;
				}

				$scope.showNotificationFieldOption = function(targetSelector) {
				    if (!$scope.targetSelectors || !$scope.customFieldSendToOptions) {
				        return true;
				    }

				    var doNotShow = false;
				    var levelIds = [];
				    _.each($scope.targetSelectors, function(savedSelector) {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector) {
				            if (savedSelector.sendToType == 'custom_field_value') {
				                if (savedSelector.selectedOption) {
				                    levelIds.push(savedSelector.selectedOption);
				                }
				            }
				        }
				    });
				    if (levelIds.length == $scope.customFieldSendToOptions.length) {
				        return false;
				    }

				    return true;
				}

				$scope.showCustomFieldSendToOption = function(targetSelector, custom_field) {
				    var sendToType = 'custom_' + custom_field.id;
				    if (!$scope.targetSelectors
				            || !$scope.customFieldSendToOptions
				            || !$scope.customFieldOptions[sendToType]) {
				        return true;
				    }

				    var levelIds = [];
				    var found = false;
            var doNotShow = false;
				    if (targetSelector.sendToType && targetSelector.selectedOption) {
				        _.each($scope.customFieldOptions[sendToType], function(item) {
				            if (item.id == targetSelector.selectedOption) {
				                found = true;
				            }
				        });

				        if (found) {
				            return found;
				        }
				    }

				    _.each($scope.targetSelectors, function(savedSelector) {
              if(savedSelector.sendToType == 'send_to_attendees'){
                doNotShow = true;
              }
				        if (($scope.targetSelectors[$scope.targetSelectors.length - 1] != savedSelector  || !found) && !doNotShow) {
				            if ($scope.customFieldKeys.indexOf(savedSelector.sendToType) > -1) {
				                if (savedSelector.selectedOption) {
				                    _.each($scope.customFieldOptions[sendToType], function(item) {
				                        if (item.id == savedSelector.selectedOption) {
				                            levelIds.push(savedSelector.selectedOption);
				                        }
				                    });
				                }
				            }
				        }
				    });

				    if (doNotShow || levelIds.length == $scope.customFieldOptions[sendToType].length) {
				        return false;
				    }

				    return true;
				}

				$scope.editScheduledNotification = function(notification) {
				    $scope.notificationTextContent = notification.text;
				    $scope.targetSelectors = [];
				    $scope.editScheduledNotificationId = notification.id;
				    $scope.sendNotificationToAll = false;
				    if (notification.send_to_everyone) {
				        $scope.targetSelectors.push({ sendToType: 'send_to_everyone' });

				    }
            if (notification.send_to_attendees) {
				        $scope.targetSelectors.push({ sendToType: 'send_to_attendees' });

				    }
            if (notification.send_to_exhibitors) {
				        $scope.targetSelectors.push({ sendToType: 'send_to_exhibitors' });

				    }
            if (notification.send_to_organizers) {
				        $scope.targetSelectors.push({ sendToType: 'send_to_organizers' });

				    }
            if(notification.custom_push_send_to.length > 0){
				        _.each(notification.custom_push_send_to, function(send_to_item) {
				            if (send_to_item.send_type == 'registration_level') {
				                $scope.targetSelectors.push(
				                    {
				                        sendToType: 'registration_level',
				                        selectedLevel: send_to_item.send_resource_id
				                    });
				            } else if (send_to_item.send_type == 'custom_field_value') {
				                var customFieldItem = null;
				                _.each($scope.conference.custom_fields, function(custom_field) {
				                    if (custom_field.type != 1) {
				                        _.each(custom_field.options, function(option) {
				                            if (option.id === send_to_item.send_resource_id) {
				                                customFieldItem = custom_field;
				                            }
				                        })
				                    }
				                });
				                $scope.targetSelectors.push(
				                    {
				                        sendToType: 'custom_' + customFieldItem.id,
				                        selectedOption: send_to_item.send_resource_id
				                    });
				            }
				        });
				    }


				    if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'registration_level'
				            ||
				        $scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'custom_field_value'
				            ||
				        $scope.customFieldKeys.indexOf($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType) > -1) {
				        $scope.plusBtnLeft = '436px';
				    } else {
				        $scope.plusBtnLeft = '307px';
				    }

				    $scope.sendDate = new Date(notification.send_at);

				    // $scope.sendDateEditable = moment(new Date(
				    //     notification.send_at_raw.year,
				    //     notification.send_at_raw.month - 1,
				    //     notification.send_at_raw.day,
				    //     notification.send_at_raw.hour,
				    //     notification.send_at_raw.minute
				    // ));

				    $scope.sendDateEditable = formatRawTimeService.createMomentFromRawTime(notification.send_at_raw);

				    if (notification.link_type != 'none') {
				        $scope.linkOptionActive = true;
				        $scope.model.selectedTarget = notification.link_type;
				        $scope.selectedTargetItemId = notification.link_id;
				    }

				    $("#notifications-main-view").animate({
				      scrollTop: 0
				    }, 400);
				}

				$scope.removeScheduledNotification = function(notification) {
				    $scope.notification = notification;
				    $scope.deleteNotificationConfirmed = $scope.deleteNotificationConfirmed;
				    ngDialog.open({template: 'app/views/partials/conference/warning_popup_for_delete_notification.html',
				          className: 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
				          scope: $scope
				    });
				}

				$scope.plusBtnLeft = '307px';
				$scope.selectSendTotType = function(targetSelector) {
				    if (['send_to_attendees','send_to_exhibitors','send_to_organizers','send_to_everyone'].indexOf(targetSelector.sendToType) <= -1) {
				        if ($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'registration_level'
				                ||
				            $scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType == 'custom_field_value'
				                ||
				            $scope.customFieldKeys.indexOf($scope.targetSelectors[$scope.targetSelectors.length - 1].sendToType) > -1) {
				            $scope.plusBtnLeft = '436px';
				        } else {
				            $scope.plusBtnLeft = '307px';
				        }
				    } else {
				        // if ($scope.targetSelectors.length > 1) {
				        //     $scope.targetSelectors.splice(-1,1);
				        // }
				        $scope.plusBtnLeft = '307px';
				        $scope.requestCostAll()
				    }
				}

				$scope.requestCostAll = function(levels) {
				    var objectToSend = {};
            _.each($scope.targetSelectors, function(savedSelector) {
              switch (savedSelector.sendToType) {
                case 'send_to_exhibitors': objectToSend.send_to_exhibitors = true;break;
                case 'send_to_attendees': objectToSend.send_to_attendees = true;break;
                case 'send_to_organizers': objectToSend.send_to_organizers = true;break;
                case 'send_to_everyone':objectToSend.send_to_everyone = true;break;
              }
            });
            var result = $scope.requestCost($scope.targetSelectors);
            objectToSend.custom_push_send_to = result.custom_push_send_to;

				    pushNotificationsDataService.calculateCost(conferenceId,objectToSend).then(function(cost) {
				        $scope.remainingCredits = $scope.totalCredits - cost;
				        $scope.audienceSize = cost;
				        $scope.usedCreditsPercentage = 100 - ($scope.remainingCredits/$scope.totalCredits * 100);

				        if ($scope.remainingCredits < 0) {
				            $scope.moreCreditsAreNecessary = true;
				            $scope.necessaryCreditsToCompleteSchedule = $scope.necessaryCreditsToCompleteSchedule - $scope.remainingCredits;
				            $scope.remainingCredits = 0;
				        } else {
                  $scope.moreCreditsAreNecessary = false;
                }
				    })
				}

				$scope.requestCost = function(levels) {
				    var objectToSend = {custom_push_send_to: []};

				    angular.forEach(levels, function(level) {
				        if (level.selectedLevel) {
				            var serverSideTypeObject = {
				                send_type: 'registration_level',
				                send_resource_id: parseInt(level.selectedLevel)
				            }
				        } else if (level.selectedOption) {
				            var serverSideTypeObject = {
				                send_type: 'custom_attendee_field_option',
				                send_resource_id: parseInt(level.selectedOption)
				            }
				        }
				        if (serverSideTypeObject) {
				            objectToSend.custom_push_send_to.push(serverSideTypeObject);
				        }
				    });
            //
				    // pushNotificationsDataService.calculateCost(conferenceId,objectToSend).then(function(cost) {
				    //     $scope.remainingCredits = $scope.totalCredits - cost;
				    //     $scope.audienceSize = cost;
				    //     $scope.usedCreditsPercentage = 100 - ($scope.remainingCredits/$scope.totalCredits * 100);
            //
				    //     if ($scope.remainingCredits < 0) {
				    //         $scope.moreCreditsAreNecessary = true;
				    //         $scope.necessaryCreditsToCompleteSchedule = $scope.necessaryCreditsToCompleteSchedule - $scope.remainingCredits;
				    //         $scope.remainingCredits = 0;
				    //     } else {
            //       $scope.moreCreditsAreNecessary = false;
            //     }
				    // })
            return objectToSend;
				}

				$scope.checkSelectedTarget = function(id, $event) {
			    linkId = id;
			    $scope.selectedTargetItemId = id;
			    $($event.target).addClass('active');
			    $($event.target).siblings().removeClass('active');
				}

				$scope.buyMoreCredits = function(creditsNumber) {
				    var ceiledCreditsNumber = Math.ceil(creditsNumber/ 1000.0) * 1000;
				    var price = calculatePriceForNotificationsCredits(ceiledCreditsNumber);

				    ngDialog.open({
				        plain: true,
				        template: "<credit-card-modal></credit-card-modal>",
				        className: 'app/stylesheets/_plusAttendee.scss',
				        data: {
				            fromManageNotifications: true,
                    ceiledCreditsNumber: ceiledCreditsNumber,
                    price: price
				        }
				    });
				}

        $scope.checkNotificationStatus = function(notification) {
            pushNotificationsDataService.getScheduledNotification(conferenceId, notification)
                .then(function(response) {
                    notification = response.data;
                    _.each($scope.scheduledNotifications, function(value, key, obj) {
                        if (value.id === notification.id) {
                            notification.nr_sent = notification.cost;
                            var temporaryDateToBeConvertedToCorrectTimezone = formatRawTimeService.createMomentFromRawTime(notification.send_at_raw);
                            notification.send_at = moment(temporaryDateToBeConvertedToCorrectTimezone).format();
                            $scope.scheduledNotifications[key] = notification;
                        }
                    });
                    if (!notification.was_processed) {
                        $timeout(function() {
                          $scope.checkNotificationStatus(notification);
                        }, 5000);
                    }
                });
        }

				function getRegistrationLevels(conferenceId) {
				    pushNotificationsDataService.getRegistrationLevels(conferenceId).then(function(response) {
				        $scope.createdRegistrationLevels = response;
				        $scope.initNotification()
				    })
				};

				function getScheduledNotifications(conferenceId) {
          var conferenceId = conferenceId;
				    pushNotificationsDataService.getScheduledNotifications(conferenceId).then(function(response) {
				        $scope.scheduledNotifications = response.custom_push_notifications;
                angular.forEach($scope.scheduledNotifications, function(notification) {
                  var temporaryDateToBeConvertedToCorrectTimezone = formatRawTimeService.createMomentFromRawTime(notification.send_at_raw);
                  notification.send_at = moment(temporaryDateToBeConvertedToCorrectTimezone).format();
                });

				        $scope.scheduledNotifications = _.orderBy($scope.scheduledNotifications, 'send_at', 'desc');
				        setInterval(function(){
				            $scope.remainingScheduledNotification = [];
				            _.each($scope.scheduledNotifications, function(notification) {
				                if (new Date(notification.send_at) < new Date()) {
				                    pushNotificationsDataService.getScheduledNotification(conferenceId, notification)
				                        .then(function(response) {
				                            notification = response.data;
				                            notification.nr_sent = notification.cost;
				                            notification.nr_seen = 0;
				                            $scope.sentNotifications.push(notification);
				                        }).catch(function(error) {
				                        });

				                } else {
				                    $scope.remainingScheduledNotification.push(notification);
				                }
				            });
				            $scope.scheduledNotifications = $scope.remainingScheduledNotification;
				            $scope.$apply();
				        }, 60000)

				        angular.forEach($scope.scheduledNotifications, function(notification) {
				            if (!notification.was_processed) {
				                $timeout(function() {
				                  $scope.checkNotificationStatus(notification);
				                }, 5000);
				            }
				            notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a');
				        })
				    })
				};

				function getSentNotifications(conferenceId) {
				    pushNotificationsDataService.getSentNotifications(conferenceId).then(function(response) {
				        $scope.sentNotifications = response.custom_push_notifications;
				        angular.forEach($scope.sentNotifications, function(notification) {
				            notification.formattedDate = moment(notification.send_at).format('MMM D, YYYY [at] h:m a')
				        })
				    })
				}

				function validateNotificationContent(notification) {
				    var isText = false;
				    var sendAtDateIsAfterToday = false;
				    var sendToExists = false;

				    var hasLinkTarget = false;


				    if (!notification.text) {
				        isText = false;
				    } else {
				        isText = true;
				    }
				    $scope.errorOnText = !isText;

				    if (!moment(notification.send_at).isAfter(moment())) {
				        sendAtDateIsAfterToday = false;
				    } else {
				        sendAtDateIsAfterToday = true;
				    }

				    $scope.errorOnDate = !sendAtDateIsAfterToday;
				}

				function calculatePriceForNotificationsCredits(numberOfDesiredCredits) {
						var priceForOneBatch = $scope.oneBatchPrice; // batch = 1000 credits
						var numberOfBatches = numberOfDesiredCredits / 1000;

						var discountPercentage = numberOfDesiredCredits >= 5000 ? $scope.discountPercentage : 0;
						var finalPrice = numberOfBatches * priceForOneBatch - (discountPercentage/100 * numberOfBatches * priceForOneBatch);


						return finalPrice;
				}

        function getSessions(conferenceId) {
          toastr.warning("Retrieving sessions for linkage, please wait... ");
          conferenceSessionsDataService.getSessionsList(conferenceId).then(function(response) {
            $scope.sessions = response.sessions;
            toastr.success("All sessions successfully retrieved!");
            sessionsRetrieved = true;
          }, function(error) {
            toastr.error("A problem occured while trying to fetch the sessions and you will not be able to link notifications to them!");
          })
        };

        function getSpeakers(conferenceId) {
          toastr.warning("Retrieving speakers for linkage, please wait... ");
          speakerDataService.getConferenceSpeakersList(conferenceId).then(function(response) {
            toastr.success("All speakers successfully retrieved!")
            $scope.speakers = response.speakers;
            speakersRetrieved = true;
          }, function(error) {
            toastr.error("A problem occured while trying to fetch the speakers and you will not be able to link notifications to them!");
          });
        }

        function getExhibitors(conferenceId) {
          toastr.warning("Retrieving exhibitors for linkage, please wait... ");
          exhibitorsDataService.getPaginatedExhibitors(conferenceId).then(function(response) {
            toastr.success("All exhibitors successfully retrieved!")
            $scope.exhibitors = response.exhibitors;
            exhibitorsRetrieved = true;
          }, function(error) {
            toastr.error("A problem occured while trying to fetch the exhibitors and you will not be able to link notifications to them!");
          });
        }

        function imagify(input) {
				  if (input == null) {
				    return '';
				  }
				  return input.replace(regex, function (match, text) {
				    var className = text.replace(/_/g, '-');
				    var output = ['<i class="emoji-picker emoji-', className, '" alt="', text, '" title=":', text, ':"></i>'];

				    return output.join('') + " ";
				  });
				}

				function getUnicodes() {
				  var swappedHex = {};
				  var unicodes = [];

				  angular.forEach(EmojiHex.emoji, function (value, key) {
				    swappedHex[value] = key;
				    unicodes.push(value);
				  });

				  return unicodes.reverse();
				}

				function getSwappedHex() {
				  var swappedHex = {};

				  angular.forEach(EmojiHex.emoji, function (value, key) {
				    swappedHex[value] = key;
				  });

				  return swappedHex;
				}

				function hexify(text) {
				  if (text == null) {
				    return '';
				  }

				  var emojiRegex = /\:([a-z0-9_+-]+)(?:\[((?:[^\]]|\][^:])*\]?)\])?\:/g;
				  var matches = text.match(emojiRegex);

				  if (matches === null) {
				    return text;
				  }

				  for (var i = 0; i < matches.length; i++) {
				    var emojiString = matches[i];
				    var property = emojiString.replace(/\:/g, '');

				    if (EmojiHex.emoji.hasOwnProperty(property)) {
				      text = text.replace(emojiString, EmojiHex.emoji[property]);
				    }
				  }

				  return text;
				}

				function unicodify(text) {
				  if (text == null) {
				    return '';
				  }

				  var matches = text.match(regexHex);

				  if (matches === null) {
				    return text;
				  }

				  for (var i = 0, len = matches.length; i < len; i++) {
				    var hexString = matches[i];

				    if (hexString.indexOf('-') > -1) {
				      var codePoints = hexString.split('-');
				      var unicode = eval('String.fromCodePoint(0x' + codePoints.join(', 0x') + ')');
				    } else {
				      var codePoint = ['0x', hexString].join('');
				      var unicode = String.fromCodePoint(codePoint);
				    }

				    text = text.replace(hexString, unicode);
				  }

				  return text;
				}

				$rootScope.$on('BOUGHT_CREDITS', function(event, data) {
				    $scope.totalCredits = $scope.totalCredits + data.numberOfCredits;
				    $scope.remainingCredits = $scope.remainingCredits + data.numberOfCredits;

				    $scope.usedCreditsPercentage = 100 - ($scope.remainingCredits/$scope.totalCredits * 100);
				    if ($scope.remainingCredits < 0) {
				        $scope.moreCreditsAreNecessary = true;
				        $scope.necessaryCreditsToCompleteSchedule = $scope.necessaryCreditsToCompleteSchedule - $scope.remainingCredits;
				        $scope.remainingCredits = 0;
				    } else {
				        $scope.moreCreditsAreNecessary = false;
				        $scope.necessaryCreditsToCompleteSchedule = 0;
				    }
				})

        $rootScope.$on('PAYMENT_METHOD_FOR_COMPLETING_NOTIFICATIONS_ACCEPTED', function(event, data) {
          ngDialog.open({
	          plain: true,
	          template: "<purchase-notifications-modal></purchase-notifications-modal>",
	          className: 'app/stylesheets/_plusAttendee.scss',
	          data: {
	              stripeToken			: data.id,
	              lastFourDigits	: data.last4,
	              amountOfCredits : data.ceiledCreditsNumber,
	              finalPrice 			: data.price
	          }
	      	});
        })

			}

    }
})();
