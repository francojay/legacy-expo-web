(function () {
    'use strict';

    var controllerId      = 'setupNotificationsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$stateParams',
      'toastr',
      'ngDialog',
      'pushNotificationsDataService',
      '$state',
      setupNotificationsController
    ]

    app.controller(controllerId, dependenciesArray);

    function setupNotificationsController($scope, $rootScope, $stateParams, toastr, ngDialog, pushNotificationsDataService, $state) {
			if (!$stateParams.obj) {
				$state.go('conferences.dashboard.notifications.overview', {
						conference_id: $stateParams.conference_id
				});
			} else {
				$scope.discountPercentage = $stateParams.obj.discountPercentage;
				$scope.oneBatchPrice = $stateParams.obj.oneBatchPrice;

				$scope.currentNotificationsNumber = 1000;

				$scope.notificationCreditsDropdown = [
						'1000', '2000', '3000', '4000', '5000', '6000', '7000', '8000', '9000', '10000', '15000', '20000',
						'25000', '30000', '40000', '50000', '75000', '100000'
				];
				$scope.finalPrice = 50;

				$scope.calculatePriceForNotifications = function(){
						$scope.finalPrice = calculatePriceForNotificationsCredits($scope.currentNotificationsNumber);
				}

				$scope.selectThisPriceForNotificationDropDown = function(price){
					$scope.currentNotificationsNumber = price;
					$scope.finalPrice = calculatePriceForNotificationsCredits($scope.currentNotificationsNumber);
				};

				$scope.buyNotifications = function(numberOfCredits, finalPrice) {
						ngDialog.open({
								plain 		: true,
								template	: "<credit-card-modal></credit-card-modal>",
								className : 'app/stylesheets/_plusAttendee.scss',
								data 			: {
									fromPurchasePushNotifications: true,
								}
						});
				}

				$rootScope.$on('PAYMENT_METHOD_FOR_PURCHASE_NOTIFICATIONS_ACCEPTED', function(event, data) {
					ngDialog.open({
	          plain: true,
	          template: "<purchase-notifications-modal></purchase-notifications-modal>",
	          className: 'app/stylesheets/_plusAttendee.scss',
	          data: {
	              stripeToken			: data.id,
	              lastFourDigits	: data.last4,
	              amountOfCredits : $scope.currentNotificationsNumber,
	              finalPrice 			: $scope.finalPrice
	          }
	      	});
				})

				function calculatePriceForNotificationsCredits(numberOfDesiredCredits) {
						var priceForOneBatch = $scope.oneBatchPrice; // batch = 1000 credits
						var numberOfBatches = numberOfDesiredCredits / 1000;

						var discountPercentage = numberOfDesiredCredits >= 5000 ? $scope.discountPercentage : 0;
						var finalPrice = numberOfBatches * priceForOneBatch - (discountPercentage/100 * numberOfBatches * priceForOneBatch);


						return finalPrice;
				}
			}

    }
})();
