(function () {
    'use strict'

    var directiveId = 'pushNotificationsReceiptsModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
			'$q',
      'toastr',
			'$timeout',
      '$filter',
			pushNotificationsReceiptsModal
		];

		app.directive(directiveId, dependenciesArray);

    function pushNotificationsReceiptsModal($rootScope, $q, toastr, $timeout, $filter) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/notifications/_components/pushNotificationsReceiptsModal/pushNotificationsReceiptsModalView.html',
            link: link
        }

        function link(scope) {
          scope.transactions              = angular.copy(scope.ngDialogData.transactions);
          scope.checkedTransactions       = [];
          scope.allTransactionsAreChecked = false;

          scope.checkOne = function(transaction) {
            transaction.checked = !transaction.checked;
            if (transaction.checked) {
              scope.checkedTransactions.push(transaction);
            } else {
              angular.forEach(scope.checkedTransactions, function(checkedTransaction, index) {
                if (checkedTransaction.id === transaction.id) {
                  scope.checkedTransactions.splice(index, 1);
                }
              });
            }

            if (scope.checkedTransactions.length == scope.transactions.length) {
              scope.allTransactionsAreChecked = true;
            } else {
              scope.allTransactionsAreChecked = false;
            }
          };

          scope.checkAll = function(state) {
            scope.allTransactionsAreChecked = !state;
            scope.checkedTransactions       = [];
            angular.forEach(scope.transactions, function(transaction, index) {
              transaction.checked = scope.allTransactionsAreChecked;
              if (transaction.checked) {
                scope.checkedTransactions.push(transaction);
              } else {
                scope.checkedTransactions.splice(index, 1);
              }
            })
          };

          scope.downloadTransactionsAsCsv = function(transactions) {
            if (transactions.length > 0) {
              lib.downloadTransactionsAsCsv(processPushNotificationPayments(transactions), $rootScope.conference);
            } else {
              toastr.warning("Please select at least a payment before download!")
            }
          }
          scope.printTransactionsAsCsv = function(transactions) {

            if (transactions.length > 0) {
              lib.printTransactionsAsCsv(processPushNotificationPayments(transactions), $rootScope.conference);
            } else {
              toastr.warning("Please select at least a payment before print!")
            }
          };

          function processPushNotificationPayments(transactions) {
            var payments = [];

            _.each(transactions, function(transaction) {
                payments.push({
                    'checked': transaction.checked,
                    'Date': $filter('date')(transaction.created_at, "MM/dd/yyyy - hh:mm a"),
                    'Order': transaction.nr_credits + " Push Notification Credits",
                    'Last 4': transaction.payment.last4,
                    'Amount': $filter('currency')(transaction.payment.amount / 100, '$')
                });
            });

            return payments;
          }
				}
    }
})();
