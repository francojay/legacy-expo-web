(function() {
    'use strict'
    var serviceId = 'pushNotificationsDataService';

    angular
        .module('app')
        .service(serviceId, ['$q', 'requestFactory', 'Upload', 'API', pushNotificationsDataService]);

    function pushNotificationsDataService($q, requestFactory, Upload, API) {
        var service = {};

        service.getRegistrationLevels = function(conferenceId) {
            var deferred = $q.defer();
            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/registration_levels/';

            requestFactory.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        service.getScheduledNotifications = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/?type=scheduled';

            requestFactory.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.getSentNotifications = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/?type=sent';

            requestFactory.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        service.buyCredits = function(conferenceId, amountOfNotificationCredits, stripeToken) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/payments/';

            amountOfNotificationCredits = parseInt(amountOfNotificationCredits)

            requestFactory.post(url, {
                stripe_token: stripeToken,
                nr_credits: amountOfNotificationCredits
            }, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(response);
            })

            return deferred.promise;
        }

        service.getPayments = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/payments/';

            requestFactory.get(url, {}, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(response);
            })

            return deferred.promise;
        }

        service.checkStatusOfNotificationSetup = function(conferenceId) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/status/'

            requestFactory.get(url, {}, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }

        service.scheduleNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            if (notification.id) {
                var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';
                requestFactory.patch(url, notification, false, true).then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                })
            } else {
                var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/';
                requestFactory.post(url, notification, false, true).then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                })
            }



            return deferred.promise;
        };

        service.removeScheduledNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';

            requestFactory.remove(url, notification, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.getScheduledNotification = function(conferenceId, notification) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/' + notification.id + '/';

            requestFactory.get(url, {}, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        service.calculateCost = function(conferenceId, levels) {
            var deferred = $q.defer();

            var url = 'api/v1.1/conferences/' + conferenceId + '/notifications/cost_calculator';

            requestFactory.post(url, levels, false, true).then(function(response) {
                deferred.resolve(response.data.cost);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        }


        return service;
    }
})();