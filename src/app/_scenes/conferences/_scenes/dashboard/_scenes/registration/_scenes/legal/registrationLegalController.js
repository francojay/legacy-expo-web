(function () {
	'use strict';

	var controllerId = 'registrationLegalController';
	var app 				 = angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$state',
		'ngDialog',
		'toastr',
		'fileService',
		'conferenceRegistrationDataService',
		'registrationLegalDataService',
		'$timeout',
		registrationLegalController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationLegalController($rootScope, $scope, $state, ngDialog, toastr, fileService, conferenceRegistrationDataService, registrationLegalDataService, $timeout) {

		var conference = $rootScope.conference;
		var registrationForm = $rootScope.registrationForm || null;

		 $timeout(function() {
		 	$('.registration-legal').mCustomScrollbar({'theme': 'minimal-dark'});
		 }, 0);

		$scope.isInEdit = false;
		$scope.isLoading = false;
		$scope.registrationFormModel = {
			terms_of_service_file_name : '',
			termsOfServiceText : '',
			refund_policy_file_name : '',
			refundPolicyText : ''
		}

		if(!registrationForm){
			getConferenceRegistrationForm();
		} else {
			$scope.registrationFormModel = $rootScope.registrationForm;
			getLegalTextsHandler(registrationForm);
		}

		$scope.openRefundPolicyRequirements = function(){
			ngDialog.open({
				plain: true,
				template: '<refund-policy-requirements-modal></refund-policy-requirements-modal>',
				className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/legal/_components/refundPolicyRequirementsModal/refundPolicyRequirementsStyles.scss',
			});
		}

		$scope.uploadLegalFiles = function(files, type){
			if (!files[0]) {
					return;
			} else if(files[0].size === 0){
					toastr.error("You can't upload an empty file");
					return;
			}
			if(files[0].type == 'text/plain') {
					fileService
							.getUploadSignature(files[0])
							.then(function(response){
									var file_link = response.data.file_link;
									var file_name = files[0].name;
									var file_type = files[0].type;
									fileService
											.uploadFile(files[0], response)
											.then(function(uploadResponse){
												if(type == 'terms_and_conditions'){
													var data = {
														'terms_of_service' : file_link,
														'terms_of_service_file_name'	:	file_name,
														'terms_of_service_file_type'	:	file_type
													}
												} else if(type == 'refund_policy'){
													var data = {
														'refund_policy' : file_link,
														'refund_policy_file_name'	:	file_name,
														'refund_policy_file_type'	:	file_type
													}
												}
												registrationLegalDataService
															.uploadRegistrationFormLegalAgreements(registrationForm.id, data)
															.then(function(response) {
																	if(type == 'terms_and_conditions'){
																			$scope.registrationFormModel.terms_of_service_file_name = response.terms_of_service_file_name;
																	} else if(type == 'refund_policy'){
																			$scope.registrationFormModel.refund_policy_file_name = response.refund_policy_file_name;
																	}
																	registrationLegalDataService
																			.getFileContent(file_link)
																			.then(function(response){
																					if(type == 'terms_and_conditions') {
																						$scope.registrationFormModel.termsOfServiceText = response;
																					} else if (type == 'refund_policy') {
																						$scope.registrationFormModel.refundPolicyText = response;
																					}
																			}, function(error) {
																			})
															}, function(error) {
															});
											}, function(error) {
											});
							}, function(error) {
								if (error.status == 403) {
										toastr.error('Permission Denied!');
								} else {
										toastr.error('Error!', 'There has been a problem with your request.');
								}
							});
			} else {
					toastr.error("The file must be a .txt file.");
			}
		};

		$scope.finishStep = function() {
			if($scope.isLoading == false){
				$scope.isLoading = true;
				if (!$scope.registrationFormModel.terms_of_service_file_name) {
						toastr.warning("The Terms and Conditions File is required");
						$scope.isLoading = false;
						return
				} if (!$scope.registrationFormModel.refund_policy_file_name) {
						$scope.isLoading = false;
						toastr.warning("The Refund Policy File is required");
						return
				} else {
						$state.go('conferences.dashboard.registration.review');
				}
			}
		}

		function getConferenceRegistrationForm() {
			conferenceRegistrationDataService
				.getRegistrationForm(conference.conference_id)
				.then(function(data) {
					$rootScope.registrationForm = data[0];
					registrationForm = data[0];
					getLegalTextsHandler(registrationForm);
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}

		function getLegalTextsHandler(registrationForm){
			if(registrationForm.terms_of_service){
				$scope.registrationFormModel.terms_of_service_file_name = registrationForm.terms_of_service_file_name;
				getTermsAndConditionsText(registrationForm.terms_of_service);
			}
			if(registrationForm.refund_policy){
				$scope.registrationFormModel.refund_policy_file_name = registrationForm.refund_policy_file_name;
				getRefundPolicyText(registrationForm.refund_policy);
			}
		}

		function getTermsAndConditionsText(link){
			registrationLegalDataService
				.getFileContent(link)
				.then(function(data) {
					$scope.registrationFormModel.termsOfServiceText = data
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}

		function getRefundPolicyText(link){
				registrationLegalDataService
					.getFileContent(link)
					.then(function(data) {
						$scope.registrationFormModel.refundPolicyText = data
					}, function(error) {
						toastr.error('Error!', 'There has been a problem with your request.');
					})
			}


  }
})();
