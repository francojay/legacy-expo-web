(function(){
    'use strict';

    var directiveId = 'deleteRegistrationLevelModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', "toastr", 'registrationLevelsDataService', deleteRegistrationLevelModal]);

    function deleteRegistrationLevelModal($rootScope, toastr, registrationLevelsDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-levels/_components/deleteRegistrationLevelModal/deleteRegistrationLevelModalView.html',
          link: link
      }

      function link(scope){

        var registrationLevel = scope.ngDialogData.level;
        var index = scope.ngDialogData.index;

        scope.deleteRegistrationLevelConfirm = function() {
          registrationLevelsDataService.deleteRegistrationLevel(registrationLevel.id).then(function(data){
            $rootScope.$broadcast('REGISTRATION_LEVEL_DELETED', {index: index});
          }, function(error) {
            if (error.status == 403) {
              toastr.error('Permission Denied!');
            } else {
              toastr.error(error.data.detail);
            }
          });
        }
      }
    }

})();
