(function(){
	'use strict';
  var directiveId 			= 'createFormHeader';
	var app 				 			= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$state',
		'$stateParams',
		createFormHeader
	];

	app.directive(directiveId, dependenciesArray);

	function createFormHeader($rootScope, $state, $stateParams) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_components/createFormHeader/createFormHeaderView.html',
			replace: true,
      link: link
		};

    function link(scope) {

			scope.$state = $state;
			scope.inEdit = false;
			scope.isFormCompleted = false;

			if($rootScope.registrationForm && $rootScope.registrationForm.completed_at != null){
				scope.isFormCompleted = true;
			}

			if(($stateParams.inEdit == true) || (scope.isFormCompleted == true)){
				scope.inEdit = true;
			} else {
				scope.inEdit = false;
			}



    }

	}

}());
