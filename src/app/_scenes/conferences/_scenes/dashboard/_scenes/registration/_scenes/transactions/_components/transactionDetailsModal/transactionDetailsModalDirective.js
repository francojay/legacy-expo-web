(function () {
    'use strict'

    var directiveId = 'transactionDetailsModal';
		var app 				= angular.module('app');

		var dependenciesArray = [
			'$rootScope',
      'toastr',
			'transactionsDataService',
      'attendeesDataService',
      'ngDialog',
      '$timeout',
			transactionDetailsModal
		];

		app.directive(directiveId, dependenciesArray);

    function transactionDetailsModal($rootScope, toastr, transactionsDataService, attendeesDataService, ngDialog, $timeout) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/transactions/_components/transactionDetailsModal/transactionDetailsModalView.html',
            link: link
        }

        function link(scope) {
					var conferenceId         = $rootScope.conference.conference_id;
					var _id 				         = scope.ngDialogData._id;
          var originalBillingEmail = "";

					scope.transaction  = null;
          scope.isEditable   = false;
          scope.listIsOpened = false;

          getTransaction(conferenceId, _id)

          $timeout(function() {
            $('.transaction-details .content').mCustomScrollbar({'theme': 'minimal-dark'});
          }, 0);

          scope.openReceiptUrl = function(receiptUrl) {
            if(receiptUrl) {
              window.open(receiptUrl, "_blank");
  						return false;
            }
					}

          scope.openAttendee = function(_id, active) {
            if(active) {
              ngDialog.open({
                plain     : true,
                template  : '<attendee-details-modal></attendee-details-modal>',
                className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                data      : {
                  "_id" : _id
                }
              });
            }

          };

          scope.openRefundModal = function($event, registrant, transaction) {
            if(scope.transaction.total > 0) {
              ngDialog.open({
                plain     : true,
                template  : '<transaction-refund-modal></transaction-refund-modal>',
                className : 'app/stylesheets/WarningScreenForDeleteAllAttendees.scss',
                data      : {
                  orderId      : transaction.id,
                  lastFour     : transaction.last4,
                  total        : transaction.total
                }
              });
            }
          }

          scope.resendReceipt = function() {
            if(scope.transaction.billing_contact && scope.transaction.receipt_url){
              transactionsDataService.resendReceipt(conferenceId, _id).then(function(){
                toastr.success("The receipt was sent to the user");
              },
              function(error){
                toastr.error(error.data.details);
              });
            }
          }

          scope.toggleActionsList = function(event, state) {
    				event.stopPropagation();
            if(scope.isEditable){
              return
            }
    				scope.listIsOpened = !state;
    			}

    			scope.hideDropdown = function(event) {
    				scope.listIsOpened 					= false;
    			}

          scope.editMode = function(state) {
            if(!state) {
              scope.transaction.billing_contact = originalBillingEmail;
            }
            scope.isEditable = state;
          }

          scope.saveBillingEmail = function () {
            var valid = true;
            if(scope.transaction.billing_contact &&
               scope.transaction.billing_contact != '' &&
               scope.transaction.billing_contact.search(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) > -1){ // email regex
                valid = true;
              } else {
                valid = false;
                toastr.error("Billing contact email is invalid")
              }
            if (valid) {
              var data = {
                'billing_contact': scope.transaction.billing_contact
              };
              transactionsDataService.editTransactionBillingEmail(conferenceId, _id, data).then(function(response) {
                  toastr.success("Billing contact email was updated successfully!")
              }, function(error) {
                toastr.error(error.data.detail);
              });
              scope.isEditable = false;
            }
          }

          function getTransaction(conferenceId, _id) {
            transactionsDataService.getOneTransaction(conferenceId, _id).then(function(response) {
  						scope.transaction                       = JSON.parse(JSON.stringify(response));
              scope.transaction.formattedTransferDate = scope.transaction.transfer_date ? moment(scope.transaction.transfer_date).format('MM/DD/YYYY') : 'Not Applicable';
              scope.transaction.createdAtFormatted    = moment(scope.transaction.created_at).format('MM/DD/YYYY');
              scope.transaction.total = calculateTotalAmount(scope.transaction.amount, scope.transaction.refunds);
              originalBillingEmail = angular.copy(scope.transaction.billing_contact);
  					}, function(error) {
              ngDialog.close();
              toastr.error(error.data.detail);
  					});
          }

          function calculateTotalAmount(amount, refunds) {
            for (var i = 0; i < refunds.length; i++) {
              refunds[i].formattedTransferDate = moment(refunds[i].created_at).format('MM/DD/YYYY');
              amount -= refunds[i].amount;
            }

            return amount;
          }

          $rootScope.$on('GET_DATA_WITH_EXISTING_FILTERS', function() {
            getTransaction(conferenceId, _id);
          });
        }
    }
})();
