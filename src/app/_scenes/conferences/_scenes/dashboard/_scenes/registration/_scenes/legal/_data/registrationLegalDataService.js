(function(){
	'use strict';

    var serviceId = 'registrationLegalDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', '$http', 'requestFactory', registrationLegalDataService
		]);
		function registrationLegalDataService($q, $http, requestFactory){
    		var service = {};

        service.uploadRegistrationFormLegalAgreements = function(registrationFormId, data){
            var deferred = $q.defer();
            requestFactory.post(
              'api/v1.1/registration_form_update/' + registrationFormId + '/',
              data,
              false,
              true
            ).then(function(response){
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.getFileContent = function(fileLink){
            var deferred = $q.defer();
            $http({
              method: "GET",
              url: fileLink,
							transformResponse: [function (data) {
								var newData = {
									text: data
								}
						    return newData;
						  }]
            }).then(function (response) {
              deferred.resolve(response.data.text);
            }, function(error) {
              deferred.reject(error);
            });
            return deferred.promise;
        }


        return service;
    }
})();
