(function(){
	'use strict';

    var serviceId = 'registrationReviewDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', '$http', 'requestFactory', registrationReviewDataService
		]);
		function registrationReviewDataService($q, $http, requestFactory){
    		var service = {};

        service.getCustomFieldsList = function(conferenceId) {
          var deferred = $q.defer();
          requestFactory.get(
            'api/field/list/' + "?conference_id=" + conferenceId,
            {},
            false
          ).then(function(response){
						deferred.resolve(response.data);
					}, function(error){
						deferred.reject(error);
					})
					return deferred.promise;
        }

				service.getFileContent = function(fileLink){
            var deferred = $q.defer();
            $http({
              method: "GET",
              url: fileLink,
							transformResponse: [function (data) {
								var newData = {
									text: data
								}
						    return newData;
						  }]
            }).then(function (response) {
              deferred.resolve(response.data.text);
            }, function(error) {
              deferred.reject(error);
            });
            return deferred.promise;
        }

				service.getAllSessions = function(conferenceId){
					var deferred = $q.defer();
					requestFactory.get(
						'api/session/list' + "?conference_id=" + conferenceId,
						{},
						false,
						true
          ).then(function(response){
          	deferred.resolve(response.data);
          }, function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				service.completeRegistrationForm = function(registrationformId){
						var deferred = $q.defer();
						var data = {};
						requestFactory.post(
							'api/v1.1/registration_form_complete/' + registrationformId + '/',
							data,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						})
						.catch(function(error){
								deferred.reject(error);
						});
						return deferred.promise;
				}

        return service;
    }
})();
