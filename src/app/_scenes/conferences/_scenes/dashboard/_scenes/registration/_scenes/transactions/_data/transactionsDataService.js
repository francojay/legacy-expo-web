(function(){
	'use strict';

    var serviceId = 'transactionsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', transactionsDataService
		]);
		function transactionsDataService($q, requestFactory){
    		var service = {};

        service.getRegistrationTransfers = function(conferenceId, page, query){
          var deferred = $q.defer();
          var payload = {
            page: page
          }
          if(query){
            payload.q = query;
          }
          requestFactory.get(
            'api/v1.1/registration_transfers/' + conferenceId + '/',
            payload,
            false,
            true
          )
          .then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };

				service.downloadRegistrationTransactions = function(conferenceId) {
          var deferred = $q.defer();
          requestFactory.get('api/v1.1/conferences/' + conferenceId + '/registration/transactions/download/')
          .then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };

				service.getOneTransaction = function(conferenceId, transactionId) {
					var deferred = $q.defer();
					var url = "api/v1.1/conferences/" + conferenceId + '/transactions/' + transactionId;

					requestFactory.get(url, {}, false, true).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});

					return deferred.promise;
				}

				service.resendReceipt = function(conferenceId, transactionId) {
					var deferred = $q.defer();
					var url = "api/v1.1/conferences/" + conferenceId + '/transactions/' + transactionId + '/send_receipt/';

					requestFactory.post(url, {}, false, true).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});

					return deferred.promise;
				}

				service.editTransactionBillingEmail = function(conferenceId, transactionId, data) {
					var deferred = $q.defer();
					var url = "api/v1.1/conferences/" + conferenceId + '/transactions/' + transactionId;

					requestFactory.post(url, data, false, true).then(function(response) {
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});

					return deferred.promise;
				}


        return service;
    }
})();
