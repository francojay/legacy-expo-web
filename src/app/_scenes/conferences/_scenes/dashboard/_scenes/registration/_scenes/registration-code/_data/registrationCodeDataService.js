(function(){
	'use strict';

    var serviceId = 'registrationCodeDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', registrationCodeDataService
		]);
		function registrationCodeDataService($q, requestFactory){
    		var service = {};

        service.setTrackingCode = function(registrationForm){
          var deferred = $q.defer();
          var data = {
            'google_tracking_code': (registrationForm.google_tracking_code != null)?registrationForm.google_tracking_code:"",
						'facebook_tracking_code': (registrationForm.facebook_tracking_code != null)?registrationForm.facebook_tracking_code:""
          };
          requestFactory.put(
            'api/v1.1/forms/' + registrationForm.id,
            data,
            false,
            true
          ).then(function(response) {
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        }

        return service;
    }
})();
