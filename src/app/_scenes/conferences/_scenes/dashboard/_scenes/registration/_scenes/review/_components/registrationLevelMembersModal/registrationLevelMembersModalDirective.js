(function(){
    'use strict';

    var directiveId = 'registrationLevelMembersModal';

    angular
        .module('app')
        .directive(directiveId, [registrationLevelMembersModal]);

    function registrationLevelMembersModal() {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/review/_components/registrationLevelMembersModal/registrationLevelMembersModalView.html',
          link: link
      }

      function link(scope){

          scope.members = scope.ngDialogData.members;

      }
    }

})();
