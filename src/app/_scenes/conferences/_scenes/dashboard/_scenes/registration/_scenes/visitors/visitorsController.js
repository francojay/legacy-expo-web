(function () {
	'use strict';

	var controllerId 			= 'visitorsController';
	var app 				 			= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$q',
		'$filter',
		'ngDialog',
		'toastr',
		'dataTableConfigGetter',
		'$state',
		'$location',
		visitorsController
	];

	app.controller(controllerId, dependenciesArray);

	function visitorsController($rootScope, $scope, $q, $filter, ngDialog, toastr,  dataTableConfigGetter, $state, $location) {
		var conference 								= $rootScope.conference;
		var conferenceId 							= conference.conference_id;
		$scope.sharedInfo 						= $location.$$search.share;

		$scope.dataTableConfiguration = null;
		dataTableConfigGetter.getTableConfig(conferenceId, 'visitors').then(function(response) {
			$scope.dataTableConfiguration = response;
		}, function(error) {
			console.log(error);
		})
  }
})();
