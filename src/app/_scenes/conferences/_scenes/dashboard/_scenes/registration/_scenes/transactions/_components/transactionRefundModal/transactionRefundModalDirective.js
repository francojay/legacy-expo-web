(function(){
  'use strict';

  var directiveId = 'transactionRefundModal';
  var app = angular.module('app');

  var dependenciesArray = [
    '$rootScope',
    'attendeesEventsPropagator',
    'toastr',
    'attendeesDataService',
    transactionRefundModal
  ];
  app.directive(directiveId, dependenciesArray);

    function transactionRefundModal($rootScope, attendeesEventsPropagator, toastr, attendeesDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/transactions/_components/transactionRefundModal/transactionRefundModalView.html',
          link: link
      }

      function link(scope) {
        var conferenceId        = $rootScope.conference.conference_id
        var initialRefundAmount = scope.ngDialogData.total;

				scope.orderId			 			= scope.ngDialogData.orderId;
				scope.lastFour		 			= scope.ngDialogData.lastFour;
				scope.refundAmount 			= scope.ngDialogData.total;
        scope.forceMaxRefundAmount = function() {
          var maxAmount = initialRefundAmount;
          if (scope.refundAmount > maxAmount) {
            scope.refundAmount = maxAmount;
          }
        }

        scope.refundPayment = function(id, amount) {
          if(!scope.lockRefundButton){
            if(scope.refundAmount > 0){
                scope.lockRefundButton = true;
                attendeesDataService.refundPayment(conferenceId, scope.orderId, scope.refundAmount).then(function(response) {
                    postRefundProcessing(response, scope.attendee);
                    toastr.success("$" + scope.refundAmount + " have been successfully refunded!");
                    scope.closeThisDialog();
                    scope.lockRefundButton = false;
                  }, function(error) {
                    if(error.data.err_code == 117){
                      toastr.error('For this type of payment the refund process is not available at this moment. Please contact Support.');
                    }
                    scope.lockRefundButton = false;
                    scope.closeThisDialog();
                  });
              } else {
                toastr.error('You can not refund $' + scope.refundAmount + ' !');
                return;
              }

            }
            else {
              return;
            }
        };

        function postRefundProcessing(financial_data, attendee) {
          $rootScope.$broadcast('GET_DATA_WITH_EXISTING_FILTERS', {});
        }
      }
     }
})();
