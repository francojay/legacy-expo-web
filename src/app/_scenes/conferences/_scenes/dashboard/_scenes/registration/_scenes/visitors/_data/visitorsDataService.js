(function(){
	'use strict';

    var serviceId = 'visitorsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', visitorsDataService
		]);
		function visitorsDataService($q, requestFactory){
    		var service = {};

				service.downloadRegistrationVisitors = function(conferenceId) {
          var deferred = $q.defer();
          requestFactory.get('api/v1.1/conferences/' + conferenceId + '/registration/visitors/download/')
          .then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };


        return service;
    }
})();
