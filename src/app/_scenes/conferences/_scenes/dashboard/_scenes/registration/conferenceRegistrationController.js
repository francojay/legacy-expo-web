(function () {
	'use strict';

	var controllerId = 'conferenceRegistrationController';

	angular
		.module('app')
		.controller(controllerId, [
			'$rootScope', '$scope', 'ngDialog', 'conferenceRegistrationDataService', conferenceRegistrationController]);

	function conferenceRegistrationController($rootScope, $scope, ngDialog, conferenceRegistrationDataService) {

		var conference = $rootScope.conference;

		$scope.displayOptionsForGraphic = false;
		$scope.registrationGraphicDays = [
				{'value' : 'Last 7 Days', 	'checked' : true},
				{'value' : 'Current Month', 'checked' : false},
				{'value' : 'Last Month', 		'checked' : false},
				{'value' : 'All Time', 			'checked' : false}
		];
		$scope.registratinStats = {};
		$scope.numberOfRegistrantsPerDate = 0;

		getRegistrationStats();

		$scope.startMerchantAgreement = function() {
			ngDialog.open({
				plain: true,
				template: '<merchant-agreement-modal></merchant-agreement-modal>',
				className: 'app/_scenes/conferences/_components/merchantAgreementModal/merchantAgreementModalStyles.scss',
			});
		}

		$scope.selectOptionForGraphic = function(time)	{
			_.each($scope.registrationGraphicDays, function(option){
					option.checked = false;
			});
			time.checked = true;
			var current_day = new Date();
			var option_time, diff;
			$scope.displayOptionsForGraphic = false;
			if(time.value == 'Last 7 Days'){
					diff = 7;
					$scope.numberOfRegistrantsPerDate = $scope.registrationStats.total_count_last_seven;
			} else if(time.value == 'Current Month'){
					option_time = new Date(current_day.getFullYear(), current_day.getMonth() , 1);
					diff = lib.daysBetween(current_day, option_time);
					$scope.numberOfRegistrantsPerDate = $scope.registrationStats.total_count_current_month;
			} else if(time.value == 'Last Month'){
					if(current_day.getMonth() > 0){
							option_time = new Date(current_day.getFullYear(), current_day.getMonth() - 1, 1);
					} else{
							option_time = new Date(current_day.getFullYear() - 1, 11, 1);
					}
					diff = lib.daysBetween(current_day, option_time);
					$scope.numberOfRegistrantsPerDate = $scope.registrationStats.total_count_last_month;
			} else{
					var date2 = new Date($scope.registrationStats.registration_form_created_at);
					var date1 = new Date();
					var timeDiff = Math.abs(date2.getTime() - date1.getTime());
					diff = Math.ceil(timeDiff / (1000 * 3600 * 24));
					$scope.numberOfRegistrantsPerDate = $scope.registrationStats.total_count;
			}
			setupStats(diff, 'update');
		};

		function getRegistrationStats() {
			conferenceRegistrationDataService
				.getRegistrationStats(conference.conference_id)
				.then(function(response) {
					$scope.registrationStats = response;
					console.log('Attendee registration data should not be sent in registration stats endpoint, because it will return ALL attendees, resulting in a high volume of data. Please fix this as soon as possible');
					setupStats(7);
					$scope.numberOfRegistrantsPerDate = $scope.registrationStats.total_count_last_seven;
				}, function(error) {
					setupStats(7, 'no_form');
				});
		}


		function setupStats(time_option, type) {
				var last_days = time_option;
				var today = new Date();
				today.setHours(0,0,0,0);
				var lastDate = new Date(today.getTime() - last_days * 24 * 60 * 60 * 1000);
				var values = [];
				var offsetInMinutes = lib.timezoneToOffset(conference.time_zone);
				if(type != 'no_form'){
					for (var d = lastDate; d <= today; d.setDate(d.getDate() + 1)) {
						var date = _.cloneDeep(d);
						var countForDay = 0;
						_.each($scope.registrationStats.registration, function(item){
							var item_day_data = item.day.split('-');
							var item_day = new Date(item_day_data[0], item_day_data[1]-1, item_day_data[2]);
							if (d.toDateString() == item_day.toDateString()) {
								countForDay = item.count;
								return;
							}
						});
						values.push({
							x: date,
							y: countForDay
						});
					}
				} else {
					var i;
					for(i = lastDate; i <= today; i.setDate(i.getDate() + 1)){
						var date = _.cloneDeep(i);
						values.push({
							x: date,
							y: 0
						});
					}
				}
				if(type == 'update'){
					lib.updateChart(values);
				} else if(type == 'no_form'){
					lib.drawChart(values, 'no_chart');
				} else{
					lib.drawChart(values);
				}
		}

  }
})();
