(function(){
  'use strict';

  var directiveId = 'refundPolicyRequirementsModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', refundPolicyRequirementsModal]);

    function refundPolicyRequirementsModal($rootScope) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/legal/_components/refundPolicyRequirementsModal/refundPolicyRequirementsView.html',
          link: link
      }

      function link(scope){



      }

    }
})();
