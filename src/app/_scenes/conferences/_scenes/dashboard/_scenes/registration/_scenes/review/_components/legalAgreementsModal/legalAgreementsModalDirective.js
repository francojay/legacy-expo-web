(function(){
    'use strict';

    var directiveId = 'legalAgreementsModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', 'registrationReviewDataService', legalAgreementsModal]);

    function legalAgreementsModal($rootScope, registrationReviewDataService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/review/_components/legalAgreementsModal/legalAgreementsModalView.html',
          link: link
      }

      function link(scope){

          var link = scope.ngDialogData.link;

          scope.legalAgreementText = '';

          getLegalAgreementText();

          function getLegalAgreementText() {
              registrationReviewDataService
                .getFileContent(link)
                .then(function(data) {
                  scope.legalAgreementText = data;
                }, function(error) {
                  toastr.error('There was a problem with your request.');
                })
          }

      }
    }

})();
