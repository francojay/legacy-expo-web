(function(){
  'use strict';

  var directiveId       = 'mapMembersInformationModal';
  var app               = angular.module('app');
  var dependenciesArray = [
    '$rootScope',
    'toastr',
    'registrationLevelOptionsDataService',
    mapMembersInformationModal
  ];

  app.directive(directiveId, dependenciesArray);

  function mapMembersInformationModal($rootScope, toastr, registrationLevelOptionsDataService) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-level-options/_components/mapMembersInformationModal/mapMembersInformationModalView.html',
        link: link
    }

    function link(scope) {
      scope.membersCsvKeys           = scope.ngDialogData.membersCsvKeys;
      scope.memberMappingFields      = scope.ngDialogData.memberMappingFields;
      scope.selectedLevel            = scope.ngDialogData.selectedLevel;
      scope.membersToMap             = scope.ngDialogData.membersToMap
      scope.hasAttendeeId            = false;
      scope.topScroll                = false;
      scope.bottomScroll             = false;
      scope.attendeeFieldMapping     = [];
      scope.attendeeFieldMappingCode = [];
      scope.attendeeFieldMappingObj  = [];
      scope.startedBulkUpload        = false;
      scope.customFields             = '?';

      scope.memberMappingFields      = [
        {
          code      : 'first_name',
          name      : 'First Name',
          mapping   : null,
          isHovered : null,
        },
        {
          code      : 'last_name',
          name      : 'Last Name',
          mapping   : null,
          isHovered : null,
        },
        {
          code      : 'email',
          name      : 'Email',
          mapping   : null,
          isHovered : null,
        }
      ];

      scope.startCallback = function(field, field2, field3, div) {
          field3.mapping = null;
          _.each(scope.customFields, function(customField) {
              if (field3.code == customField.mapping) {
                  customField.mapping = null;
                  scope.$apply();
              }
          });
      }

      scope.stopCallback = function(field) {
          field.target.style.cursor = '-webkit-grab';
      }

      scope.changeDragStyle = function(element){
          // element.target.style.cursor = 'move';
          // element.target.style.display = 'flex';
          // element.target.style.alignItems = 'center';
          // if (element.target.children[1]) {
          //     element.target.children[1].style.display = 'flex';
          //     element.target.children[1].style.alignItems = 'center';
          //     element.target.children[1].style.flexDirection = 'column';
          // }
          // element.target.style.opacity = 0;
      }

      scope.attendeeFieldMappingOver = function(field, field2, field3) {
        scope.topScroll = false;
        scope.bottomScroll = false;
        scope.attendeeFieldMapping[field3.id] = field2.draggable[0].innerText;
        scope.attendeeFieldMappingCode[field2.draggable[0].innerText] = field3.code;
        if (scope.attendeeFieldMappingCode[field2.draggable[0].innerText] == undefined) {
            scope.attendeeFieldMappingCode[field2.draggable[0].innerText] = field3.name;
        }
        scope.attendeeFieldMappingObj[field3.id] = field2;
        field3.mapping = field2.draggable[0].innerText;
        _.each(scope.membersCsvKeys, function(csvField) {
            if (csvField.code == field2.draggable[0].innerText) {
                csvField.mapping = field3.code;
            }
        });
        _.each(scope.attendeesCsvKeys, function(csvField) {
            if (csvField.code == field2.draggable[0].innerText) {
                csvField.mapping = field3.code;
            }
        });
      };

      scope.attendeeFieldMappingHoverIn = function(field, field2, field3) {
          scope.$apply();
      }

      scope.attendeeFieldMappingHoverOut = function(field, field2, field3) {
          scope.$apply();
      }

      scope.undoMapping = function(field) {
        field.mapping = null;
        angular.forEach(scope.membersCsvKeys, function(key) {
          if (key.mapping === field.code) {
            key.mapping = null;
          }
        });
      }

      scope.saveAndUploadAttendeeMembers = function() {
          if (scope.startedBulkUpload || !scope.membersCsvKeys) return;
          var attendeeBulkUploadPayload = [];
          var foundErrorWhileMapping    = false;
          for (var i = 0; i < scope.memberMappingFields.length; i++) {
            if (scope.memberMappingFields[i].mapping === null) {
              toastr.error("Please map something to " + scope.memberMappingFields[i].name + ".");
              foundErrorWhileMapping = true;
              break

            }
          }
          if (!foundErrorWhileMapping) {
            var foundErrorsInMappedValues = false;
            for (var i = 0; i < scope.membersToMap.length - 1; i++) {
              if (!foundErrorsInMappedValues) {
                var member = {};
                for (var j = 0; j < scope.memberMappingFields.length; j++) {
                  if (scope.membersToMap[i][scope.memberMappingFields[j].mapping.trim()]) {
                    member[scope.memberMappingFields[j].code] = scope.membersToMap[i][scope.memberMappingFields[j].mapping.trim()];
                  } else {
                    toastr.error("The member on line " + i + " has incomplete data! Please check its " + scope.memberMappingFields[j].mapping + " value!");
                    foundErrorsInMappedValues = true;
                    break;
                  }
                }
                attendeeBulkUploadPayload.push(member);
              } else {
                break
              }
            };
            if (!foundErrorsInMappedValues) {
              var bad_email = false;

              _.each(attendeeBulkUploadPayload, function(item) {
                if (item.email && !validateEmail(item.email)) {
                    toastr.error(item.email + " is not a valid email address!");
                    bad_email = true;
                    return false;
                }
                _.each(item, function(k, v) {
                    if (k && k.length > 255) {
                        k = k.substring(0,255);
                        item[v] = k;
                    }
                });

                if (item.email != undefined && item.email.trim() == '') {
                    item.email = null;
                }
              });


              if (bad_email) return;

              scope.startedBulkUpload = true;

              registrationLevelOptionsDataService.bulkCreateMembers(attendeeBulkUploadPayload, scope.selectedLevel.id).then(function(response) {
                registrationLevelOptionsDataService.getRegistrationMembers(scope.selectedLevel.id).then(function(response) {
                    $rootScope.$broadcast("UPLOADED_MEMBERS_LIST_SUCCESSFULLY", response);
                    scope.closeThisDialog()
                }, function(error) {

                });
                scope.startedBulkUpload = false;
              }, function(error) {
                  var data = error.data;
                  data = data.replace('<!DOCTYPE html>400<meta charset="utf-8" />{"detail":"','');
                  data = data.replace('"}','');
                  toastr.error(data);
                  scope.startedBulkUpload = false;
              });
            }

          }
      }

      function validateEmail(emailAddress) {
        var regexExpression = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return regexExpression.test(emailAddress);
      }
    }

  }

})();
