(function(){
	'use strict';

    var serviceId = 'registrationLevelOptionsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', registrationLevelOptionsDataService
		]);
		function registrationLevelOptionsDataService($q, requestFactory){
    		var service = {};

        service.createRegistrationAddOnSession = function(registration_level_id, add_on_session_id){
            var deferred = $q.defer();
            var data = {
                'session' : add_on_session_id
            };
            requestFactory.post(
              'api/v1.1/registration_level_add_on_sessions/' + registration_level_id + '/',
              data,
              false,
              true
            ).then(function(response){
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.removeRegistrationAddOnSession = function(add_on_session_id){
            var deferred = $q.defer();
            requestFactory.remove(
              'api/v1.1/registration_level_add_on_session/' + add_on_session_id + '/',
              {},
              false,
              true
            ).then(function(response){
                deferred.resolve(response.data);
            }, function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.updateRegistrationAddOnSessionPricing = function(add_on_session_pricing_id, add_on_session_model){
            var deferred = $q.defer();
            requestFactory.post(
              'api/v1.1/registration_level_add_on_session_pricing/' + add_on_session_pricing_id + '/',
              add_on_session_model,
              false,
              true
            ).then(function(response){
                deferred.resolve(response.data);
            }, function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

        service.createRegistrationAddOnSessionPricing = function(add_on_session_id, add_on_session_model){
            var deferred = $q.defer();
            requestFactory.post(
              'api/v1.1/registration_level_add_on_session_pricings/' + add_on_session_id + '/',
              add_on_session_model,
              false,
              true
            ).then(function(response){
                deferred.resolve(response.data);
            }, function(error){
                deferred.reject(error);
            });
            return deferred.promise;
        }

				service.removeRegistrationAddOnSessionPricing = function(add_on_session_pricing_id){
						var deferred = $q.defer();
						requestFactory.remove(
							'api/v1.1/registration_level_add_on_session_pricing/' + add_on_session_pricing_id + '/',
							{},
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				}

				service.addRegistrationPromoCodes = function(level_id, promo_code){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/registration_level_promo_codes/' + level_id + '/',
							promo_code,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				}

				service.updateRegistrationPromoCode = function(promo_code_id, promo_code){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/registration_level_promo_code/' + promo_code_id + '/',
							promo_code,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				}

				service.removeRegistrationPromoCode = function(promo_code_id){
						var deferred = $q.defer();
						requestFactory.remove(
							'api/v1.1/registration_level_promo_code/' + promo_code_id + '/',
							{},
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error){
								deferred.reject(error);
						});
						return deferred.promise;
				}

				service.bulkCreateMembers = function(payload, registration_level_id){
						var deferred = $q.defer();
						var data = {
								members: payload
						}
						requestFactory.post(
							'api/v1.1/registration_level_members/' + registration_level_id + '/',
							data,
							false
						)
						.then(function(response){
							deferred.resolve(response.data);
						}, function(error){
							deferred.reject(error);
						});
						return deferred.promise;
				};

				service.getRegistrationMembers = function(registration_level_id){
						var deferred = $q.defer();
						requestFactory.get(
							'api/v1.1/registration_level_members/' + registration_level_id + '/',
							{},
							true
						)
						.then(function(response){
							deferred.resolve(response.data);
						}, function(error) {
							deferred.reject(error);
						});
						return deferred.promise;
				};

				service.removeRegistrationMembers = function(registration_level_id, members_ids){
						var deferred = $q.defer();
						members_ids = members_ids.replace('/,/g', '%2C');
						requestFactory.remove(
							'api/v1.1/registration_level_members/' + registration_level_id + '/' + '?member_ids=' + members_ids,
							{},
							false
						)
						.then(function(response){
							deferred.resolve(response.data);
						}, function(error){
							deferred.reject(error);
						});
						return deferred.promise;
				}



        return service;
    }
})();
