(function () {
	'use strict';

	var controllerId = 'registrationFieldsController';
	var app 				 = angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$state',
		'toastr',
		'conferenceRegistrationDataService',
		'registrationFieldsDataService',
		'$timeout',
		'$stateParams',
		'$q',
		registrationFieldsController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationFieldsController($rootScope, $scope, $state, toastr, conferenceRegistrationDataService, registrationFieldsDataService, $timeout, $stateParams, $q) {
		var conference = $rootScope.conference;

		$timeout(function() {
			$('.attendee-fields-entry').mCustomScrollbar({'theme': 'minimal-dark'});
		}, 0);

		$scope.registrationForm = {};
		$scope.standardFields = [
			{label:'First Name', type:"required", name: 'first_name', type_code: 'Required'},
			{label:'Last Name', type:"required", name: 'last_name', type_code: 'Required'},
			{label:'Email', type:"required", name: 'email_address', type_code: 'Required'},
			{label:"Company", type:"optional", name: 'company_name' ,type_code:"Optional"},
			{label:"Job Title", type:"optional", name: 'job_title' ,type_code:"Optional"},
			{label:"Phone", type:"optional", name: 'phone_number' ,type_code:"Optional"},
			{label:"Street", type:"optional", name: 'street_address' ,type_code:"Optional"},
			{label:"City", type:"optional", name: 'city' ,type_code:"Optional"},
			{label:"Zip", type:"optional", name: 'zip_code' ,type_code:"Optional"},
			{label:"State", type:"optional", name: 'state' ,type_code:"Optional"},
			{label:"Country", type:"optional", name: 'country' ,type_code:"Optional"}
		]
		$scope.customFieldName = {};
		$scope.newCustomField = {
			'type'		: 1,
			'options' : []
		};
		$scope.customFields = [];
		$scope.savedCustomFields = [];
		$scope.customFieldSelected = 'Text';
		$scope.customFieldOptions = [];
		$scope.customFieldMoreOptions = false;
		$scope.updateCustomFieldBoolean = false;
		$scope.isProcessing = false;
		$scope.model = {
			newOption : ""
		};

		$scope.newThirdPartyModel = {name:''};
		$scope.addANewThirdParty = false;

		getConferenceRegistrationForm();

		$scope.hideDropdown = function(index){
				if(document.getElementsByClassName('drop-down-type-for-field')[index])
				document.getElementsByClassName('drop-down-type-for-field')[index].style.visibility = 'hidden';
		};

		$scope.dropDownTypeOfField = function(e){
				e.stopPropagation();
				if(e.target.nextElementSibling.style.visibility == 'hidden' || _.isEmpty(e.target.nextElementSibling.style.visibility)){
						e.target.nextElementSibling.style.visibility = "visible";
				} else{
						e.target.nextElementSibling.style.visibility = "hidden";
				}
		};

		$scope.chooseStandardFieldType = function(field, type, e){
				field.type = type;
				if(type == 'required')
						field.type_code = 'Required';
				else if(type == 'optional')
						field.type_code = 'Optional';
				else if(type == 'hidden')
						field.type_code = 'Hidden';
				e.target.parentElement.style.visibility = 'hidden';
		};

		$scope.chooseCustomFieldType = function(field, type, e){
				field.registration_form_type = type;
				if(type == 'required')
						field.registration_form_type = 'Required';
				else if(type == 'optional')
						field.registration_form_type = 'Optional';
				else if(type == 'hidden')
						field.registration_form_type = 'Hidden';
				e.target.parentElement.style.visibility = 'hidden';
		}

		$scope.toggleCustomFieldsMoreOptions = function() {
			$scope.customFieldMoreOptions = !$scope.customFieldMoreOptions;
		}

		$scope.customFieldChose = function(fieldType) {
				if (fieldType == 'Text') {
						$scope.newCustomField.type = 1;
				} else if (fieldType == 'Multiple Choice') {
						$scope.newCustomField.type = 3;
				}
				else if (fieldType == 'Single Choice') {
						$scope.newCustomField.type = 2;
				}
				$scope.customFieldSelected = fieldType;
				$scope.customFieldMoreOptions = false;
		}

		$scope.getEditCustomField = function(customField) {
			$scope.newCustomField = customField;
			$scope.customFieldSelected = $scope.newCustomField.type == 1 ?
						'Text' : ($scope.newCustomField.type == 3 ? 'Multiple Choice' : ($scope.newCustomField.type == 2 ? 'Single Choice' : '')) ;
						$scope.updateCustomFieldBoolean = true;
		};


		$scope.customFieldAddOption = function(model) {
			if (model.newOption == null || model.newOption == '') {
					return
			}
			var fields = $scope.newCustomField.options;
			fields.push({
				value: model.newOption
			});

			fields = Array.from(new Set(fields));

			$scope.customFieldOptions = fields;
			$scope.model.newOption = "";
		}

		$scope.customFieldRemoveOption = function(option) {
				var fields = $scope.newCustomField.options;
				var index = fields.indexOf(option);
				if (index > -1) {
						fields.splice(index, 1);
				}
				$scope.customFieldOptions = fields;
				$scope.newOption = ''; // doesnt work as intended
		}

		$scope.createOrUpdateCustomField = function(customField) {
			//console.log(customField)
			if(validateCustomField(customField)) {
				if($scope.updateCustomFieldBoolean) {
					console.log(customField)
					updateCustomField(customField);
				} else {
					createCustomField(customField);
				}
			}
		}

		$scope.deleteCustomField = function(customField) {
				registrationFieldsDataService
					.deleteCustomField(customField.id)
					.then(function(result) {
							_.pull($scope.savedCustomFields, customField);
							if($scope.newCustomField.hasOwnProperty('id') && customField.id == $scope.newCustomField.id){
									resetCustomFields();
							}
					}, function(error) {
						if (error.status == 403) {
								toastr.error('Permission Denied!');
						} else {
								toastr.error('Error!', 'There has been a problem with your request.');
						}
					});
		};

		$scope.completeFirstStep = function() {
			if($scope.isProcessing == false){
				$scope.isProcessing = true;
				var formattedCustomFields = [];
				angular.forEach($scope.savedCustomFields, function(customField) {
					formattedCustomFields.push({
						'custom_field_id' : customField.id,
						'type'						: customField.registration_form_type ? customField.registration_form_type.toLowerCase() : "optional"
					});
				})
				var formData = {
					'custom_fields'	:	formattedCustomFields,
					'registration_form_id'	:	$scope.registrationForm.id,
					'standard_fields'	:	$scope.standardFields
				}
				registrationFieldsDataService
					.setRegistrationFormFields(formData)
					.then(function(data) {
						if($stateParams.inEdit == true) {
							$state.go('conferences.dashboard.registration.review');
						} else {
							$state.go('conferences.dashboard.registration.levels');
						}
					}, function(error) {
						toastr.error('Error!', 'There has been a problem with your request.');
					})
					.then(function(){
						$scope.isProcessing = false;
					})
			}
		}

		function validateCustomField(customField){
			if (!customField || !customField.name) {
					toastr.error('Field name is required');
					return false;
			} else if(_.isEmpty(customField.name.trim())){
					toastr.error('Field name is required');
					return false;
			} else if(!$scope.updateCustomFieldBoolean){
				 _.each($scope.customFields, function(field){
						if(field.name === customField.name.trim()){
								toastr.error('You already have a field with this name');
								return false;
						}
				});
			}
			return true;
		}

		function updateCustomField(customField) {
			if (((customField.type === 2 || customField.type === 3) && customField.options.length > 1) || customField.type === 1) {
				if ($scope.model.newOption && $scope.model.newOption != '') { // doesnt work
						customField.options.push(
								{
										value: $scope.model.newOption
								}
						);
				}
				customField.conference_id = conference.conference_id;
				registrationFieldsDataService
					.updateCustomField(conference.conference_id, customField)
					.then(function(result) {
						angular.forEach($scope.savedCustomFields, function(savedCustomField, key){
							if(savedCustomField.id == customField.id){
								$scope.savedCustomFields.splice(key, 1, customField);
							}
						})
					}, function(error) {
						if (error.status == 403) {
								toastr.error('Permission Denied!');
						} else {
								lib.processValidationError(error, toastr);
						}
					})
					.then(function(){
						resetCustomFields();
					})
			} else {
				toastr.error("Please insert 2 or more options!");
			}
		}

		function createCustomField(customField) {
			if (((customField.type === 2 || customField.type === 3) && customField.options.length >= 1) || customField.type === 1) {
				if ($scope.model.newOption && $scope.model.newOption != '') { // doesnt work
						customField.options.push(
								{
										value: $scope.model.newOption
								}
						);
				}
				customField.conference_id = conference.conference_id;
				registrationFieldsDataService
					.createCustomField(conference.conference_id, customField)
					.then(function(result) {
						result.registrationFormTypeCode = 'Optional';
						result.registrationFormType			= 'optional'
						$scope.savedCustomFields.push(result);
						toastr.success("Custom field named: " + customField.name + " added successfully!");
					}, function(error) {
						if (error.status == 403) {
								toastr.error('Permission Denied!');
						} else {
								lib.processValidationError(error, toastr);
						}
					})
					.then(function(){
						resetCustomFields();
					})
			} else {
				toastr.error("Please insert 2 or more options!");
			}

		}

		function resetCustomFields() {
				$scope.newCustomField = {
						options: [],
						type: 1
				}
				$scope.customFieldSelected = "Text";
				$scope.updateCustomFieldBoolean = false;
				$scope.model.newOption = "";
		};

		function getConferenceRegistrationForm() {
			var promises = [];
			promises.push(conferenceRegistrationDataService.getRegistrationForm(conference.conference_id));
			promises.push(registrationFieldsDataService.getCustomFieldsList(conference.conference_id));

			$q.all(promises).then(function(response) {
				$scope.registrationForm  = response[0][0];
				$scope.savedCustomFields = response[1].custom_fields;

				angular.forEach($scope.savedCustomFields, function(field) {
					angular.forEach($scope.registrationForm.registrationformcustomfield_set, function(formField) {
						if (field.id === formField.custom_field.id) {
							field.registration_form_type = formField.type.charAt(0).toUpperCase() + formField.type.slice(1);
						}
					})
				});

				angular.forEach($scope.standardFields, function(field) {
					angular.forEach($scope.registrationForm.registrationformstandardfield_set, function(formField) {
						if (field.name === formField.name) {
							field.type = formField.type;
							field.type_code = formField.type.charAt(0).toUpperCase() + formField.type.slice(1);
						}
					})
				})
				getThirdParties($scope.registrationForm.id);
			}, function(error) {
				toastr.error("Error!", "An error has occured while trying to fetch data for form completion.");
			})
		}

		function getThirdParties(formId){
			var promises = [];
			promises.push(registrationFieldsDataService.getThirdParties(conference.conference_id, formId));
			$q.all(promises).then(function(response) {
				$scope.thirdPartiesList  = response[0];

			}, function(error) {
				toastr.error("Error!", "An error has occured while trying to fetch data for form completion.");
			})
		}
		$scope.addNewThirdParty = function(model, addNew){
			var promises = [];
			if(addNew){
				$scope.addANewThirdParty = true;
				$scope.newThirdPartyModel.name = null;
			}
			else{
				$scope.addANewThirdParty = false;
			}
			if(!model.name){
				return false;
			}
			promises.push(registrationFieldsDataService.addNewThirdParty(conference.conference_id, $scope.registrationForm.id, model));
			$q.all(promises).then(function(response) {
				$scope.thirdPartiesList.push(response[0]);
			}, function(error) {
				toastr.error("Error!", "An error has occured while trying to add a new third party.Please check the name.");
			})
		}
		$scope.editThirdParty = function(model, addNew){
			var promises = [];
			promises.push(registrationFieldsDataService.editThirdParty(conference.conference_id, $scope.registrationForm.id, model));
			$q.all(promises).then(function(response) {
				angular.forEach($scope.thirdPartiesList, function(tp,index) {
					if (tp.id === model.id) {
						$scope.thirdPartiesList[index] = response[0] ;
					}
				});
				if(addNew){
					$scope.addANewThirdParty = true;
					$scope.newThirdPartyModel.name = null;
				}
				else{
					$scope.addANewThirdParty = false;
				}
			}, function(error) {
				toastr.error("Error!", "An error has occured while trying to add a new third party.Please check the name.");
			})
		}
		$scope.removeThirdParty = function(id){
			var promises = [];
			$scope.newThirdPartyModel.name = null;
			promises.push(registrationFieldsDataService.removeThirdParty(conference.conference_id, $scope.registrationForm.id, id));
			$q.all(promises).then(function(response) {
				angular.forEach($scope.thirdPartiesList, function(tp,index) {
					if (tp.id === id) {
						$scope.thirdPartiesList.splice(index,1);
					}
				});
			}, function(error) {
				toastr.error("Error!", "An error has occured while trying to remove a third party");
			})
		}
  }
})();
