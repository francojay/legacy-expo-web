(function () {
	'use strict';

	var controllerId = 'registrationReviewController';
	var app 				 = angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$state',
		'$timeout',
		'$window',
		'ngDialog',
		'hashingService',
		'conferenceRegistrationDataService',
		'registrationReviewDataService',
		'$q',
		'toastr',
		registrationReviewController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationReviewController($rootScope, $scope, $state, $timeout, $window, ngDialog, hashingService, conferenceRegistrationDataService, registrationReviewDataService, $q, toastr) {

			var conference = $rootScope.conference;
			var registrationForm = $rootScope.registrationForm || {};

			$scope.standardFields = [
				{label:'First Name', type:"required", name: 'first_name', type_code: 'Required'},
				{label:'Last Name', type:"required", name: 'last_name', type_code: 'Required'},
				{label:'Email', type:"required", name: 'email_address', type_code: 'Required'},
				{label:"Company", type:"optional", name: 'company_name' ,type_code:"Optional"},
				{label:"Job Title", type:"optional", name: 'job_title' ,type_code:"Optional"},
				{label:"Phone", type:"optional", name: 'phone_number' ,type_code:"Optional"},
				{label:"Street", type:"optional", name: 'street_address' ,type_code:"Optional"},
				{label:"City", type:"optional", name: 'city' ,type_code:"Optional"},
				{label:"Zip", type:"optional", name: 'zip_code' ,type_code:"Optional"},
				{label:"State", type:"optional", name: 'state' ,type_code:"Optional"},
				{label:"Country", type:"optional", name: 'country' ,type_code:"Optional"}]
			$scope.savedCustomFields = [];
			$scope.allSessions = [];

			$timeout(function() {
				$('.registration-review').mCustomScrollbar({'theme': 'minimal-dark'});
			}, 0);

			getConferenceRegistrationForm();
			getAllSessions();

			$scope.functionDropSubPanel = function(type, e, index, element){
					lib.functionDropSubPanel(type, e, index, element);
			};

			$scope.openTermsAndContitionsPopup = function() {
					openLegalAgreementModal(registrationForm.terms_of_service);
			}

			$scope.openRefundPolicyPopup = function() {
					openLegalAgreementModal(registrationForm.refund_policy);
			}

			$scope.openViewMembersPopup = function(level) {
				openRegistrationLevelMembersModal(level);
			}

			$scope.canIncludeSessionPerLevel = function(session, level) {
					var found = false;
					if(level){
							_.each(level.registrationlevelsession_set, function(level_session){
									if (session.id == level_session.session.id) {
											found = true;
									}
							});
					} else{
							return !found;
					}
					return !found;
			}

			$scope.editThisRegistrationLevel = function(level) {
				$state.go('conferences.dashboard.registration.levels', {levelId: level.id, inEdit: true});
			}

			$scope.editThisRegistrationLevelOptions = function(level) {
				$state.go('conferences.dashboard.registration.level-options', {levelId: level.id, inEdit: true});
			}

			$scope.previewRegistrationForm = function() {
					var baseLocation = window.location.href.split('/#')[0];
					var formId = hashingService.hashId.encode(registrationForm.id);
					$window.open(baseLocation + '/#!/preview/' + formId + '/step-1');
			}

			$scope.completeRegistrationForm = function() {
					registrationReviewDataService
							.completeRegistrationForm(registrationForm.id)
							.then(function(response){
									$rootScope.registrationForm  = response;
									$state.go('conferences.dashboard.registration.overview');
							}, function(error){
									toastr.error('Error!', 'There has been a problem with your request.');
							});
			}

			function getConferenceRegistrationForm() {
				var promises = [];
				promises.push(conferenceRegistrationDataService.getRegistrationForm(conference.conference_id));
				promises.push(registrationReviewDataService.getCustomFieldsList(conference.conference_id));

				$q.all(promises).then(function(response) {
						$rootScope.registrationForm  = response[0][0];
						$scope.savedCustomFields = response[1].custom_fields;

						angular.forEach($scope.standardFields, function(field) {
							angular.forEach($scope.registrationForm.registrationformstandardfield_set, function(formField) {
								if (field.name === formField.name) {
									field.type = formField.type;
									field.type_code = formField.type.charAt(0).toUpperCase() + formField.type.slice(1);
								}
							})
						});

						angular.forEach($scope.savedCustomFields, function(field) {
							angular.forEach($scope.registrationForm.registrationformcustomfield_set, function(formField) {
								if (field.id === formField.custom_field.id) {
									field.registration_form_type = formField.type.charAt(0).toUpperCase() + formField.type.slice(1);
								}
							})
						})

					}, function(error) {
						toastr.error('Error!', 'There has been a problem with your request.');
					})
			}

			function getAllSessions() {
				registrationReviewDataService
					.getAllSessions(conference.conference_id)
					.then(function(data) {
						$scope.allSessions = data.sessions;
					}, function(error) {
						toastr.error('Error!', 'There has been a problem with your request.');
					})
			}

			function openLegalAgreementModal(link){
				var data = {};
				data.link = link;
				ngDialog.open({
						plain: true,
						template: '<legal-agreements-modal></legal-agreements-modal>',
						className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/review/_components/legalAgreementsModal/legalAgreementsModalStyles.scss',
						data: data
				});
			}

			function openRegistrationLevelMembersModal(level) {
					var data = {};
					data.members = level.registrationlevelmember_set;
					ngDialog.open({
							plain: true,
							template: '<registration-level-members-modal></registration-level-members-modal>',
						 	className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/review/_components/registrationLevelMembersModal/registrationLevelMembersModalStyles.scss',
							data: data
					});
			};


  }
})();
