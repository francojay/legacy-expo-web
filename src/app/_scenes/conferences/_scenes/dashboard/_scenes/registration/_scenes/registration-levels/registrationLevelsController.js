(function () {
	'use strict';

	var controllerId = 'registrationLevelsController';
	var app 				 = angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$state',
		'$timeout',
		'$stateParams',
		'ngDialog',
		'toastr',
		'conferenceRegistrationDataService',
		'registrationLevelsDataService',
		registrationLevelsController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationLevelsController($rootScope, $scope, $state, $timeout, $stateParams, ngDialog, toastr, conferenceRegistrationDataService, registrationLevelsDataService) {

		var conference = $rootScope.conference;
		var registrationForm = {};

		$timeout(function() {
			$('.registration-levels-setup').mCustomScrollbar({'theme': 'minimal-dark'});
		}, 0);

		$scope.isLoading = false;
		$scope.inEdit = false;
		$scope.registrationLevels = [];
		$scope.selectedLevel = {};
		$scope.selectedIndex = null;
		$scope.newLevel = {
			name:''
		}
		$scope.newBenefit = {
			name:''
		};

		$scope.standardPricing = {};
		$scope.sortedDatesForLevelPrices = [];
		$scope.currentDate = new Date();
		$scope.currentDate = $scope.currentDate.getMonth() + 1 + '/' + $scope.currentDate.getDate() + '/' + $scope.currentDate.getFullYear();
		$scope.conference_date_to_moment = moment(conference.date_to);
		$scope.pricing = {};

		$scope.allSessionsRegistration = [];
		$scope.isLoadingSessions = false;

		$scope.selectableRemove = false;
		$scope.selectableAdd = false;
		//SCROLL
		var $levelHolder      = null;
		var $levelSliderRail  = null;
		var levelTabWidth     = 0;
		var levelTabHolderTranslate   = 0;
		$scope.scrollPosition       = 0;


		getConferenceRegistrationForm();

		function getConferenceRegistrationForm() {
			conferenceRegistrationDataService
				.getRegistrationForm(conference.conference_id)
				.then(function(data) {
					$rootScope.registrationForm = data[0];
					registrationForm = data[0];
					populateLevels(registrationForm);
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}

		function populateLevels(registrationForm) {
			$scope.registrationLevels = registrationForm.registrationlevel_set;
			addDatesForMomentPicker($scope.registrationLevels);
			if($scope.registrationLevels.length == 0){
				$scope.addNewLevel();
			}
			if($state.params.levelId != ''){
				angular.forEach($scope.registrationLevels, function(registrationLevel, index) {
					if(registrationLevel.id == $state.params.levelId) {
						$scope.selectLevel(index);
					}
				})
			} else {
				$scope.selectLevel(0);
			}
		}

		function addDatesForMomentPicker(registrationLevels) {
			angular.forEach(registrationLevels, function(registrationLevel){
				angular.forEach(registrationLevel.registrationlevelpricing_set, function(pricing){
					pricing.starts_at_editable = createMomentFromRawTime(pricing.starts_at_raw);
					pricing.ends_at_editable = createMomentFromRawTime(pricing.ends_at_raw);
				})
			})
		}

		function getAllSessions() {
			$scope.isLoadingSessions = true;
			registrationLevelsDataService
				.getAllSessions(conference.conference_id)
				.then(function(data) {
					$scope.allSessionsRegistration = data.sessions;
					angular.forEach($scope.allSessionsRegistration, function(session){
						session.marked = false;
					})
				}, function(error){
					toastr.error('Error!', 'There has been a problem with your request.');
				}).then(function(){
					$scope.isLoadingSessions = false;
				})
		}

		// LEVELS

		$scope.selectLevel = function(index) {
			$scope.selectedLevel = $scope.registrationLevels[index];
			$scope.selectedIndex = index;
			$scope.checkCharacterCounter($scope.selectedLevel,2000);

			if ($scope.selectedLevel && $scope.selectedLevel.registrationlevelpricing_set.length > 1) {
				for (var i = 0; i < $scope.selectedLevel.registrationlevelpricing_set.length; i++) {
					if ($scope.selectedLevel.registrationlevelpricing_set[i].ends_at === null && $scope.selectedLevel.registrationlevelpricing_set[i].starts_at === null) {
						$scope.standardPricing = $scope.selectedLevel.registrationlevelpricing_set[i];
						break
					}
				}
			} else if ($scope.selectedLevel && $scope.selectedLevel.registrationlevelpricing_set.length === 1) {
				$scope.standardPricing = $scope.selectedLevel.registrationlevelpricing_set[0];
			} else {
				$scope.standardPricing = {};
			}
			$scope.newBenefit = {
				name: ''
			}
			$scope.sortedDatesForLevelPrices = [];
			getAllSessions();
		}

		$scope.addNewLevel = function() {
			var data = {
					'registration_form': registrationForm.id,
					'name': ''
			};
			registrationLevelsDataService
				.createRegistrationLevel(data)
				.then(function(data) {
					$scope.registrationLevels.push(data);
					if($scope.registrationLevels.length == 1){
						$scope.selectedLevel = $scope.registrationLevels[0];
					}
				}, function(error){
					if(error.data.detail){
						toastr.error(error.data.detail);
						return ;
					}
					toastr.error('Error!', 'There has been a problem with your request.');
				});
		}

		$scope.editLevel = function(level, index) {
			level.newName = level.name;
			level.inEdit = true;
		}

		$scope.updateLevelName = function(level){
			level.name = level.newName;
			level.inEdit = false;
			$scope.updateLevel(level);
		}

		$scope.updateLevel = function(level) {
			registrationLevelsDataService
				.updateRegistrationLevel(level)
				.then(function(data) {
					$scope.selectedLevel = data;
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}

		$scope.deleteLevel = function(level, index) {
			var data = {};
			data.level = level;
			data.index = index;
			ngDialog.open({
				plain: true,
				template: '<delete-registration-level-modal></delete-registration-level-modal>',
				className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-levels/_components/deleteRegistrationLevelModal/deleteRegistrationLevelStyles.scss',
				data: data
			});
		}

		$rootScope.$on('REGISTRATION_LEVEL_DELETED', function(event, data){
			$scope.registrationLevels.splice(data.index, 1);
			if($scope.registrationLevels.length == 0){
				$scope.addNewLevel();
			} else {
				$scope.selectedLevel = $scope.registrationLevels[0];
				$scope.checkCharacterCounter($scope.selectedLevel,2000);
			}
		})

		// RECOMMENDED LEVEL

		$scope.setRecommendedRegistrationLevel = function(level) {
				var data = {
						"recommended_level": level.id,
						"recommandation_reason": null
				}
				if(registrationForm.recommended_level == level.id){
					data = {
						"recommended_level": null,
						"recommandation_reason": null
					}
				}
				conferenceRegistrationDataService
					.updateRegistrationForm(registrationForm.id, data)
					.then(function(data){
						$rootScope.registrationForm = data;
						registrationForm = data;
					}, function(error){
						toastr.error('Error!', 'There has been a problem with your request.');
					})
		}

		$scope.setRecommendedRegistrationReason = function(reason) {
				var data = {
						"recommandation_reason": reason,
				}
				conferenceRegistrationDataService
					.updateRegistrationForm(registrationForm.id, data)
					.then(function(data){
						$rootScope.registrationForm = data;
						registrationForm = data;
					}, function(error){
						toastr.error('Error!', 'There has been a problem with your request.');
					})
		}

		// BENEFITS

		$scope.addNewBenefitOption = function() {
				$scope.selectedLevel.registrationlevelbenefit_set.push({});
		}

		$scope.enterBenefit = function(benefit) {
				$scope.selectedLevel.registrationlevelbenefit_set.push({});
				$timeout(function() {
						$(".level-benefit-list:last > input").focus();
				}, 100);
		}

		$scope.createNewBenefit = function(benefit) {
			if(benefit.name){
				benefit.loading = true;
				registrationLevelsDataService
					.addBenefit($scope.selectedLevel.id, benefit)
					.then(function(response){
						benefit.loading = false;
						benefit.id = response.id;
						$scope.selectedLevel.registrationlevelbenefit_set.push(benefit);
					}, function(error) {
						benefit.loading = false;
					});
			}
		}

		$scope.createOrUpdateBenefit = function(benefit) {
			if(benefit.name) {
				benefit.loading = true;
				if (benefit.id) {
					registrationLevelsDataService
						.updateBenefit(benefit)
						.then(function(response){
							benefit.loading = false;
							benefit = response;
						}, function(error) {
							benefit.loading = false;
						});
				} else {
					registrationLevelsDataService
						.addBenefit($scope.selectedLevel.id, benefit)
						.then(function(response){
							benefit.loading = false;
							benefit.id = response.id;
						}, function(error) {
							benefit.loading = false;
						});
				}
			}
		}

		$scope.removeBenefit = function(benefit) {
			benefit.loading = true;
			var index = $scope.selectedLevel.registrationlevelbenefit_set.indexOf(benefit);
			if(_.isEmpty(benefit.name)){
				$scope.selectedLevel.registrationlevelbenefit_set.splice(index, 1);
				benefit.loading = false;
				return;
			}
			registrationLevelsDataService
				.removeBenefit(benefit)
				.then(function(response){
					_.remove($scope.selectedLevel.registrationlevelbenefit_set, function(benefit_item){
						return benefit_item.id == benefit.id;
					});
				}, function(error) {
				})
				.then(function(){
					benefit.loading = false;
				});
		}

		// PRICING

		$scope.securePricing = function(pricing) {
				pricing.price = pricing.price.replace(/[^0-9$.,]/g, '');
				var nrDots = pricing.price.split(".").length;
				if (nrDots > 2) {
						var i = pricing.price.lastIndexOf('.');
						if (i != -1) {
								pricing.price = pricing.price.substr(0, i);
						}
				}
		}

		$scope.setStandardPrice = function(pricing, index) {
				$scope.standardPricing = pricing;
				var currentLevel = $scope.registrationLevels[index];
				if (pricing.name && pricing.name != "" && pricing.price) {

						registrationLevelsDataService.addPricing(currentLevel.id, pricing).then(function(response){
									var existing_price = _.find(currentLevel.registrationlevelpricing_set, {id:response.id});
									if (existing_price) {
											existing_price = response;
									} else {
										  if($scope.selectedIndex == index) {
												$scope.selectedLevel.registrationlevelpricing_set.push(response);
												$scope.registrationLevels[$scope.selectedIndex].registrationlevelpricing_set.push(response);
												pricing = response;
												$scope.standardPricing = response;
											}
											else {
												$scope.registrationLevels[index].registrationlevelpricing_set.push(response);
											}

									}
							}, function(error) {
									$scope.standardPricing.price = 0;
									pricing.price = 0;
									lib.processValidationError(error, toastr);
							});
				} else if(pricing.name){
						$scope.standardPricing = pricing;
				}
		};

		$scope.addCustomPrice = function() {
				$scope.selectedLevel.hasCustomPrice = true;
		}

		$scope.updatePricing = function(level_pricing) {
				if (level_pricing.price < 20) {
						level_pricing.price = 20;
				}
				if (level_pricing.starts_at_editable && level_pricing.ends_at_editable) {
						var starts_at = new Date(level_pricing.starts_at_editable);
						var ends_at = new Date(level_pricing.ends_at_editable);
						level_pricing.starts_at_raw = {
								"year": starts_at.getFullYear(),
								"month": starts_at.getMonth() + 1,
								"day": starts_at.getDate()
						}
						level_pricing.ends_at_raw = {
								"year": ends_at.getFullYear(),
								"month": ends_at.getMonth() + 1,
								"day": ends_at.getDate()
						}
						registrationLevelsDataService
								.updatePricing(level_pricing)
								.then(function (res) {
										res.starts_at = moment(res.starts_at);
										res.ends_at = moment(res.ends_at);
										res.starts_at_editable = createMomentFromRawTime(res.starts_at_raw);
										res.ends_at_editable = createMomentFromRawTime(res.ends_at_raw);
										var price_index = $scope.selectedLevel.registrationlevelpricing_set.indexOf(level_pricing);
										$scope.selectedLevel.registrationlevelpricing_set = _.reject($scope.selectedLevel.registrationlevelpricing_set, ['id', res.id]);
										$scope.selectedLevel.registrationlevelpricing_set.splice(price_index, 0, res);
										sortDatesForLevelPrices($scope.selectedLevel.registrationlevelpricing_set);
								}, function(error) {
										level_pricing.price = 0;
										lib.processValidationError(error, toastr);
								});
							} else {
									registrationLevelsDataService
											.updatePricing(level_pricing)
											.then(function(res){
											}, function(error){
													level_pricing.price = 0;
													lib.processValidationError(error, toastr);
													level_pricing.ends_at = null;
											});
							}
		}

		$scope.findIndexOfThisDate = function(selectedDate, date_type, list_of_sorted_dates) {
				if (!selectedDate) {
						return $scope.currentDate;
				}
				var date_index;
				if(date_type === 'start') {
						date_index = _.findIndex(list_of_sorted_dates, function (price_date) {
								if (!price_date) {
										return false;
								}
								return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				} else if(date_type === 'end'){
						date_index = _.findLastIndex(list_of_sorted_dates, function (price_date) {
								if (!price_date) {
										return false;
								}
								return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				}

				if(date_type === 'start' && date_index > 0){
						date_index -= 1;
				} else if(date_type === 'end' && date_index < (list_of_sorted_dates.length - 1)) {
						date_index += 1;
				} else if(date_index === (list_of_sorted_dates.length - 1) || date_index === 0 ) {
						date_index = -1;
				}

				if(date_index != -1){
						var date_to_return = list_of_sorted_dates[date_index];
						if (!date_to_return) {
								return false;
						}
						if(date_type === 'end'){
								return date_to_return.clone().subtract(1, 'days').format('MM/DD/YYYY');
						} else if(date_type === 'start'){
								return date_to_return.clone().add(1, 'days').format('MM/DD/YYYY');
						}
				} else if(date_type === 'end'){
						return $scope.conference_date_to_moment;
				} else if(date_type === 'start'){
						return null;
				}

		};

		$scope.updateChangesOnPricing = function(newValue, oldValue, pricing) {
				$scope.updatePricing(pricing);
		};

		$scope.removePricing = function(pricing_id) {
				registrationLevelsDataService
						.deletePricing(pricing_id)
						.then(function(response){
								$scope.selectedLevel.registrationlevelpricing_set = _.reject($scope.selectedLevel.registrationlevelpricing_set, ['id', pricing_id]);
								sortDatesForLevelPrices($scope.selectedLevel.registrationlevelpricing_set);
								if($scope.selectedLevel.registrationlevelpricing_set.length <= 1){
										$scope.selectedLevel.hasCustomPirce = false;
								}
						}, function(error) {

						});
		}

		$scope.isLastCustomPrice = function(pricing) {
				var last_item;
				_.each($scope.selectedLevel.registrationlevelpricing_set, function(pricing){
						if (pricing.starts_at) {
								last_item = pricing;
						}
				});
				return last_item && pricing.id === last_item.id;
		};

		$scope.setCustomPrice = function(index) {

				if ($scope.pricing && $scope.pricing.name && $scope.pricing.name != "" && $scope.pricing.price && $scope.pricing.starts_at_editable && $scope.pricing.ends_at_editable){
						var starts_at = new Date($scope.pricing.starts_at_editable);
						var ends_at = new Date($scope.pricing.ends_at_editable);
						$scope.pricing.starts_at_raw = {
								"year": starts_at.getFullYear(),
								"month": starts_at.getMonth() + 1,
								"day": starts_at.getDate()
						}

						$scope.pricing.ends_at_raw = {
								"year": ends_at.getFullYear(),
								"month": ends_at.getMonth() + 1,
								"day": ends_at.getDate()
						}
						var selectLevelId = ($scope.selectedIndex == index)?$scope.selectedLevel.id:$scope.registrationLevels[index].id;
						registrationLevelsDataService
								.addPricing(selectLevelId, $scope.pricing)
										.then(function(response){
												$scope.pricing = {};
												var price = response;
												price.starts_at = moment(price.starts_at);
												price.ends_at = moment(price.ends_at);
												price.starts_at_editable = createMomentFromRawTime(price.starts_at_raw);
												price.ends_at_editable = createMomentFromRawTime(price.ends_at_raw);
												if($scope.selectedIndex == index){
													$scope.selectedLevel.registrationlevelpricing_set.push(price);
													sortDatesForLevelPrices($scope.selectedLevel.registrationlevelpricing_set);
													$scope.selectedLevel.hasCustomPrice = false;
												}
												else{
													$scope.registrationLevels[index].registrationlevelpricing_set.push(price);
													sortDatesForLevelPrices($scope.registrationLevels[index].registrationlevelpricing_set);
													$scope.registrationLevels[index].hasCustomPrice = false;
												}

										}, function(error) {
											lib.processValidationError(error, toastr)
										});
				}
		};

		$scope.verifyIfDateIsAvailable = function(date, type, price_model, list_of_prices, picker_type){

				if(type !== 'day'){
						return true;
				}

				var ok = true;
				if (price_model && price_model.hasOwnProperty('starts_at_editable')) {
						price_model.starts_at = price_model.starts_at_editable;
				}
				if (price_model && price_model.hasOwnProperty('ends_at_editable')) {
						price_model.ends_at = price_model.ends_at_editable;
				}

				if(price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) {
						var model_start_index = _.findIndex($scope.sortedDatesForLevelPrices, function(price_date){
								return price_model.starts_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
						var model_end_index = _.findIndex($scope.sortedDatesForLevelPrices, function(price_date){
								return price_model.ends_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				}

				if(picker_type === 'end' && price_model != null && price_model !== undefined && price_model.hasOwnProperty('starts_at')){
						var price_date = _.find($scope.sortedDatesForLevelPrices, function(price_date){
								return price_date.format('MM/DD/YYYY') > price_model.starts_at.format('MM/DD/YYYY');
						});

						if(price_date === undefined){
								ok = true;
						} else if(price_date != undefined && (date.format('MM/DD/YYYY') < price_date.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY'))){
								ok = true;
						} else{
								ok = false;
						}
						return ok;
				}

				if(price_model == null || price_model == undefined || !price_model.hasOwnProperty('starts_at') || !price_model.hasOwnProperty('ends_at')){
						for(var i = 0; i < $scope.sortedDatesForLevelPrices.length; i+=2){
								var j = i + 1;
								if($scope.sortedDatesForLevelPrices[i].format('MM/DD/YYYY') <= date.format('MM/DD/YYYY') &&
										date.format('MM/DD/YYYY') <= $scope.sortedDatesForLevelPrices[j].format('MM/DD/YYYY')){
										ok = false;
										break;
								}
						}
				} else if((price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) &&
						(date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') <= price_model.ends_at.format('MM/DD/YYYY')) ||
						(date.format('MM/DD/YYYY') < price_model.starts_at.format('MM/DD/YYYY') &&
								(model_start_index > 0 && date.format('MM/DD/YYYY') > $scope.sortedDatesForLevelPrices[model_start_index - 1].format('MM/DD/YYYY') ||
								(model_start_index === 0 && date.format('MM/DD/YYYY') >= $scope.currentDate))
						) ||
						(date.format('MM/DD/YYYY') > price_model.ends_at.format('MM/DD/YYYY') &&
								(
										(model_end_index < ($scope.sortedDatesForLevelPrices.length - 1) && date.format('MM/DD/YYYY') < $scope.sortedDatesForLevelPrices[model_end_index + 1].format('MM/DD/YYYY'))
										|| (model_end_index === ($scope.sortedDatesForLevelPrices.length - 1))
								)
						)
				){
						ok = true;
				} else {
						ok = false;
				}

				return ok;
		};

		$scope.removeCustomPrice = function() {
				$scope.selectedLevel.hasCustomPrice = false;
				$scope.pricing = null;
		}

		function createMomentFromRawTime(rawDatetime) {
				if (!rawDatetime || !rawDatetime.year) {
						return null;
				}
				return moment(new Date(
						rawDatetime.year,
						rawDatetime.month - 1,
						rawDatetime.day,
						rawDatetime.hour,
						rawDatetime.minute
				));
		}

		function sortDatesForLevelPrices(list_of_prices) {
				$scope.sortedDatesForLevelPrices = [];
				_.each(list_of_prices, function (price_model) {
						if (price_model.starts_at_editable !== null && price_model.ends_at_editable !== null) {
								$scope.sortedDatesForLevelPrices.push(price_model.starts_at_editable);
								$scope.sortedDatesForLevelPrices.push(price_model.ends_at_editable);
						}
				});
				$scope.sortedDatesForLevelPrices = _.sortBy($scope.sortedDatesForLevelPrices);
		};

		// SESSIONS

		$scope.registrationSessionSelected = function(session, type){
			if(session.marked){
				session.marked = !session.marked;
			} else{
				session.marked = true;
			}
			if(session.marked){
				if(type == 'delete'){
					$scope.selectableRemove = true;
					$scope.selectableAdd = false;
				} else if(type == 'add'){
					$scope.selectableRemove = false;
					$scope.selectableAdd = true;
				}
			}
		};

		$scope.canIncludeSession = function(session) {
			var found = false;
			_.each($scope.selectedLevel.registrationlevelsession_set, function(level_session){
				if (session.id == level_session.session.id) {
					found = true;
				}
			});
			return !found;
		}

		$scope.removeSessionsFromLevel = function() {
			_.each($scope.allSessionsRegistration, function(session){
				if(session.marked && session.marked == true){
					$scope.selectedLevel.registrationlevelsession_set.push({'session': session});
				}
			});
			var session_ids = [];
			_.each($scope.selectedLevel.registrationlevelsession_set, function(session){
					if (!session.registrationleveladdonsessionpricing_set || session.registrationleveladdonsessionpricing_set.length === 0) {
						session.session.addOnSession = {};
						session.session.addOnSession.add_on_price_model = {
								session: session.session.id,
								registration_level: session.registration_level,
								price: null
						};
				}
				if(session.session.marked && session.session.marked == true){
					session_ids.push(session.session.id);
				}
				session.session.marked = false;
			});
			registrationLevelsDataService
				.addSessionToRegistrationLevel($scope.selectedLevel.id, {"sessions": session_ids})
				.then(function(res){
						_.each(session_ids, function(session_id){
								$scope.allSessionsRegistration = _.reject($scope.allSessionsRegistration, function(conference_session){
										return conference_session.id == session_id;
								});
								$rootScope.registrationForm = _.reject($rootScope.registrationForm, function(conference_session){
										return conference_session.id = session_id;
								})
								registrationForm = $rootScope.registrationForm;
						});
				}, function(error){

				});
		};

		$scope.addSessionsToLevel = function() {
				var session_ids = [];
				var session_to_add = [];
				$scope.selectedLevel.registrationlevelsession_set = _.reject($scope.selectedLevel.registrationlevelsession_set, function(session){
						if(session.session.marked && session.session.marked == true){
							session_to_add.push(session.session);
							session_ids.push(session.session.id);
						}
						return session.session.marked == true;
				});
				_.each($scope.selectedLevel.registrationlevelsession_set, function(session){
						session.session.marked = false;
				});
				_.each($scope.allSessionsRegistration, function(session){
						session.marked = false;
				});
				registrationLevelsDataService
					.removeSessionToRegistrationLevel($scope.selectedLevel.id, session_ids)
					.then(function(res){
							_.each(session_to_add, function(session){
									session.marked = false;
									var foundSession = _.find($scope.allSessionsRegistration, function(sessionItem){
											return sessionItem.id === session.id;
									});
									if (!foundSession) {
											$scope.allSessionsRegistration.push(session);
									}
							});
							// $rootScope.registrationForm.registrationlevel_set.push(response.sessions);
							// registrationForm.registrationlevel_set.push(response.sessions);
					}, function(error){
					});
		};

		// FINISH

		$scope.finishStep = function() {
			var ok = true;
			_.each($scope.registrationLevels, function(level, index) {
					if (level.name == '' && ok) {
							toastr.error('Please set names for all the registration levels!');
							ok = false;
							$scope.selectedLevel = level;
					}
					if (level.registrationlevelpricing_set.length <= 0 && ok) {
							if (!(level.id == $scope.selectedLevel.id && $scope.standardPricing &&
											$scope.standardPricing.price >=0 && $scope.standardPricing.name && $scope.standardPricing.name != "")) {
									toastr.error('Please set at least one price for all the registration levels!');
									ok = false;
									$scope.selectedLevel = level;
							}
					}
					_.each(level.registrationlevelpricing_set, function(pricing){
							if (pricing.price == null || pricing.price == undefined) {
									toastr.error('The price field is required for each of the registration prices!');
									$scope.selectedLevel = level;
									ok = false;
							}
					});
			});
			if(ok == true){
				if($stateParams.inEdit == true){
						$state.go('conferences.dashboard.registration.review');
				} else {
						$state.go('conferences.dashboard.registration.level-options');
				}
			}
		}

		$scope.scrollLeft = function() {
			$levelHolder      = angular.element(document.getElementsByClassName('registration-header-levels')[0]);
			$levelSliderRail     = angular.element(document.getElementsByClassName('registration-slider-rail')[0]);
			levelTabWidth           = angular.element(document.getElementsByClassName('registration-header-level')[0]);
			var addLevel    = angular.element(document.getElementsByClassName('add-level')[0]);
			if ($scope.scrollPosition > 0) {
				$scope.scrollPosition--;
				levelTabHolderTranslate = (-1) * levelTabWidth.width() * $scope.scrollPosition;
				$levelHolder.css('transform', 'translateX(' + levelTabHolderTranslate + 'px)');
				addLevel.css('transform', 'translateX(' + levelTabHolderTranslate + 'px)');
			}
		}

		$scope.scrollRight = function() {
			$levelHolder      = angular.element(document.getElementsByClassName('registration-header-levels')[0]);
			$levelSliderRail     = angular.element(document.getElementsByClassName('registration-slider-rail')[0]);
			levelTabWidth           = angular.element(document.getElementsByClassName('registration-header-level')[0]);
			var addLevel    = angular.element(document.getElementsByClassName('add-level')[0]);
			if($scope.scrollPosition < $scope.registrationLevels.length - 3){
				$scope.scrollPosition++;
				levelTabHolderTranslate = (-1) * levelTabWidth.width() * $scope.scrollPosition;
				$levelHolder.css('transform', 'translateX(' + levelTabHolderTranslate + 'px)');
				addLevel.css('transform', 'translateX(' + levelTabHolderTranslate + 'px)');
			}
		}

		$scope.checkCharacterCounter = function(string, maximumLength) {
				$scope.characterCount = _.size(string.description);

				if (_.size(string.description) <= maximumLength) {
						var leftCharacters = maximumLength - _.size(string.description);

						if (leftCharacters < 10) {
								$scope.lessThanTenCharactersLeft = true;
						} else {
								$scope.lessThanTenCharactersLeft = false;
						}
				} else {
						var exceedCharactersCount = maximumLength - _.size(string);
						string.description = string.description.slice(0, maximumLength);

						$scope.characterCount = _.size(string.description);
						$scope.lessThanTenCharactersLeft = true;
				}
		}

  }
})();
