(function () {
	'use strict';

	var controllerId 			= 'registrationCodeController';
	var app 				 			= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$window',
		'API',
		'toastr',
		'hashingService',
		'conferenceRegistrationDataService',
		'registrationCodeDataService',
		'merchantAgreementDataService',
		'ngDialog',
		registrationCodeController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationCodeController($rootScope, $scope, $window, API, toastr, hashingService, conferenceRegistrationDataService, registrationCodeDataService, merchantAgreementDataService, ngDialog) {

		var conference = $rootScope.conference;

		$scope.registrationForm = {};
		$scope.isMerchantAgreementVerified = false;

		getRegistrationForm();

		$scope.updateTrackingCode = function(){
			var registrationForm = $scope.registrationForm;
			registrationCodeDataService.setTrackingCode(registrationForm).then(function(response){
				toastr.success('Registration tracking code successfully updated.');
			}).catch(function(error){
				toastr.error('Error!', 'There has been a problem with your request.');
			})
		}

		$scope.getRegistrationFormHashedId = function(registrationFormId) {
				var hashedFormId = hashingService.hashId.encode(registrationFormId);
				return hashedFormId;
		}

		$scope.getRegistrationJs = function() {
			var hostname = window.location.hostname;
			var origin 	 = window.location.origin;

			if (API.ENV === 'DEV') {
				origin = 'http://expo-registration.s3-website-us-west-2.amazonaws.com'
			} else if (API.ENV === 'PROD') {
				origin = 'https://go.regform.com'
			}

			if (API.ENV === 'DEV') {
				return origin + '/_assets/scripts/dev.js'
			} else {
				return origin + '/_assets/scripts/expo.js'
			}
		}

		$scope.previewRegistrationForm = function(registrationForm) {
			if (registrationForm.id) {
				if (API.ENV === 'DEV') {
					var baseUrl = 'http://expo-registration.s3-website-us-west-2.amazonaws.com'
				} else if (API.ENV === 'PROD') {
					var baseUrl = 'https://go.regform.com'
				}
				var hashedFormId = $scope.getRegistrationFormHashedId(registrationForm.id);
				$window.open(baseUrl + '/#!/registration/' + hashedFormId);
			}	else {
				toastr.warning("Retrieving data for preview, please wait...")
			}

		}

		$scope.copyCodeToClipboard = function() {
				var doc = document
						, text = doc.getElementById('registration_code')
						, range, selection
				;
				if (doc.body.createTextRange) {
						range = document.body.createTextRange();
						range.moveToElementText(text);
						range.select();
				} else if (window.getSelection) {
						selection = window.getSelection();
						range = document.createRange();
						range.selectNodeContents(text);
						selection.removeAllRanges();
						selection.addRange(range);
				}
				document.execCommand('copy');
				if ( document.selection ) {
						document.selection.empty();
				} else if ( window.getSelection ) {
						window.getSelection().removeAllRanges();
				}
				toastr.success("Code copied to clipboard!");
		}

		$scope.openMerchantAgreementForm = function() {
			ngDialog.open({
				plain: true,
				template: "<merchant-agreement-modal></merchant-agreement-modal>",
				className: 'app/stylesheets/_merchantAgreementModal.scss'
			})
		}

		function getRegistrationForm() {
			conferenceRegistrationDataService
				.getRegistrationForm(conference.conference_id)
				.then(function(data) {
					$rootScope.registrationForm = data[0];
					$scope.registrationForm = data[0];
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}
  }
})();
