(function(){
	'use strict';

    var serviceId = 'registrationFieldsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', registrationFieldsDataService
		]);
		function registrationFieldsDataService($q, requestFactory){
    		var service = {};

        service.getCustomFieldsList = function(conferenceId) {
          var deferred = $q.defer();
          requestFactory.get(
            'api/field/list/' + "?conference_id=" + conferenceId,
            {},
            false
          ).then(function(response){
						deferred.resolve(response.data);
					}, function(error){
						deferred.reject(error);
					})
					return deferred.promise;
        }

				service.createCustomField = function(conferenceId, customField){
					var deferred = $q.defer();
					requestFactory.post(
						'api/field/create/',
						customField,
						false
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error){
						deferred.reject(error);
					})
					return deferred.promise;
				}

				service.updateCustomField = function(conferenceId, customField){
					var deferred = $q.defer();
					requestFactory.post(
						'api/field/update/',
						customField,
						false
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error){
						deferred.reject(error);
					})
					return deferred.promise;
				}

				service.deleteCustomField = function(conferenceId, customField){
					var deferred = $q.defer();
					requestFactory.remove(
						'api/field/delete/' + "?id=" + conferenceId,
						{},
						false
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

        service.setRegistrationFormFields = function(data) {
            var deferred = $q.defer();
            requestFactory.post(
              'api/registration/set_fields/',
              data,
              false,
              true
            ).then(function(response){
              deferred.resolve(response.data);
            })
            .catch(function(error){
              deferred.reject(error);
            });
            return deferred.promise;
        }

				service.getThirdParties = function(conferenceId, formId) {
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/conferences/' + conferenceId + '/registration_form/' + formId + '/third_parties/' ,
            {},
            false
          ).then(function(response){
						deferred.resolve(response.data);
					}, function(error){
						deferred.reject(error);
					})
					return deferred.promise;
        }

				service.addNewThirdParty = function(conferenceId, formId, data) {
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/conferences/' + conferenceId + '/registration_form/' + formId + '/third_parties/',
						data,
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
        }

				service.editThirdParty = function(conferenceId, formId, data) {
					var deferred = $q.defer();
					requestFactory.put(
						'api/v1.1/conferences/' + conferenceId + '/registration_form/' + formId + '/third_parties/' + data.id,
						{name:data.name},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
        }

				service.removeThirdParty = function(conferenceId, formId, id) {
					var deferred = $q.defer();
					requestFactory.remove(
						'api/v1.1/conferences/' + conferenceId + '/registration_form/' + formId + '/third_parties/' + id,
						{},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					})
					.catch(function(error){
						deferred.reject(error);
					});
					return deferred.promise;
        }
        return service;
    }
})();
