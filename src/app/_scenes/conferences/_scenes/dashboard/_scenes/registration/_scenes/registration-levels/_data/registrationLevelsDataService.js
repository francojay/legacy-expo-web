(function(){
	'use strict';

    var serviceId = 'registrationLevelsDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', registrationLevelsDataService
		]);
		function registrationLevelsDataService($q, requestFactory){
    		var service = {};

				service.createRegistrationLevel = function(data){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/registration_levels/' + data.registration_form + '/',
							data,
							false,
							true
						)
						.then(function(response) {
							deferred.resolve(response.data);
						}, function(error) {
							deferred.reject(error);
						});
						return deferred.promise;
				}

				service.updateRegistrationLevel = function(level){
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/registration_level/' + level.id + '/',
						level,
						false,
						true
					).then(function(response){
							deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.deleteRegistrationLevel = function(registrationLevelId){
					var deferred = $q.defer();
					requestFactory.remove(
						'api/v1.1/registration_level/' + registrationLevelId + '/',
						{},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.addBenefit = function(levelId, benefit){
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/registration_level_benefits/' + levelId + '/',
						benefit,
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.updateBenefit = function(benefit){
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/registration_level_benefit/' + benefit.id + '/',
						benefit,
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.removeBenefit = function(benefit){
					var deferred = $q.defer();
					requestFactory.remove(
						'api/v1.1/registration_level_benefit/' + benefit.id + '/',
						{},
						false,
						true
					).then(function(response){
						deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}

				service.addPricing = function(levelId, pricing){
					var deferred = $q.defer();
					var url = 'api/v1.1/registration_level_pricings/' + levelId + '/';
					if (pricing.id) {
							url = 'api/v1.1/registration_level_pricing/' + pricing.id + '/';
					}
					requestFactory.post(
						url,
						pricing,
						false,
						true
					).then(function(response){
							deferred.resolve(response.data);
					}, function(error) {
							deferred.reject(error);
					});
					return deferred.promise;
				}

				service.updatePricing = function(pricing){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/registration_level_pricing/' + pricing.id + '/',
							pricing,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				};

				service.deletePricing = function(levelPricingId){
						var deferred = $q.defer();
						requestFactory.remove(
							'api/v1.1/registration_level_pricing/' + levelPricingId + '/',
							{},
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				};

				service.getAllSessions = function(conferenceId){
					var deferred = $q.defer();
					requestFactory.get(
						'api/session/list' + "?conference_id=" + conferenceId,
						{},
						false,
						true
          ).then(function(response){
          	deferred.resolve(response.data);
          }, function(error) {
						deferred.reject(error);
					});
          return deferred.promise;
				};

				service.addSessionToRegistrationLevel = function(levelId, sessions){
					var deferred = $q.defer();
					requestFactory.post(
							'api/v1.1/registration_level_sessions/' + levelId + '/',
							sessions,
							false,
							true
					).then(function(response){
							deferred.resolve(response.data);
					}, function(error) {
							deferred.reject(error);
					});
					return deferred.promise;
				}

				service.removeSessionToRegistrationLevel = function(levelId, sessions){
					var deferred = $q.defer();
					requestFactory.remove(
							'api/v1.1/registration_level_sessions/' + levelId + '/?session_ids=' + sessions.join(),
							{},
							false,
							true
					).then(function(response){
							deferred.resolve(response.data);
					}, function(error){
							deferred.reject(error);
					});
					return deferred.promise;
				}



        return service;
    }
})();
