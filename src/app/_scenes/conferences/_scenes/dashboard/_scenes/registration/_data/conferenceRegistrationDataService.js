(function(){
	'use strict';

    var serviceId = 'conferenceRegistrationDataService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', conferenceRegistrationDataService
		]);
		function conferenceRegistrationDataService($q, requestFactory){
    		var service = {};

				service.getRegistrationForm = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/registration_forms/' + conferenceId + '/',
            {},
            false,
            true
          ).then(function(response) {
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };

				service.createRegistrationForm = function(conferenceId) {
						var deferred = $q.defer();
						var data = {
								conference_id: conferenceId
						};
						requestFactory.post(
							'api/registration/create/',
							data,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						})
						.catch(function(error){
								deferred.reject(error);
						});
						return deferred.promise;
				}

				service.updateRegistrationForm = function(registrationFormId, data){
						var deferred = $q.defer();
						requestFactory.post(
							'api/v1.1/registration_form_update/' + registrationFormId + '/',
							data,
							false,
							true
						).then(function(response){
								deferred.resolve(response.data);
						}, function(error) {
								deferred.reject(error);
						});
						return deferred.promise;
				}

        service.getRegistrationStats = function(conferenceId){
          var deferred = $q.defer();
          requestFactory.get(
            'api/v1.1/registration_form_stats/' + conferenceId + '/',
            {},
            false,
            true
          ).then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };

        service.getRegistrationTransfers = function(conferenceId, page, query){
          var deferred = $q.defer();
          var payload = {
            page: page
          }
          if(query){
            payload.q = query;
          }
          requestFactory.get(
            'api/v1.1/registration_transfers/' + conferenceId + '/',
            payload,
            false,
            true
          )
          .then(function(response){
            deferred.resolve(response.data);
          }, function(error) {
            deferred.reject(error);
          });
          return deferred.promise;
        };

				service.updateRegistrationLevel = function(level){
					var deferred = $q.defer();
					requestFactory.post(
						'api/v1.1/registration_level/' + level.id + '/',
						level,
						false,
						false
					).then(function(response){
							deferred.resolve(response.data);
					}, function(error) {
						deferred.reject(error);
					});
					return deferred.promise;
				}


        return service;
    }
})();
