(function () {
	'use strict';

	var controllerId 			= 'transactionsController';
	var app 				 			= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$q',
		'$filter',
		'ngDialog',
		'toastr',
		'transactionsDataService',
		'dataTableConfigGetter',
		'$state',
		'$location',
		transactionsController
	];

	app.controller(controllerId, dependenciesArray);

	function transactionsController($rootScope, $scope, $q, $filter, ngDialog, toastr, transactionsDataService, dataTableConfigGetter, $state, $location) {
		var conference 								= $rootScope.conference;
		var conferenceId 							= conference.conference_id;
		$scope.sharedInfo 						= $location.$$search.share;

		$scope.dataTableConfiguration = null;
		dataTableConfigGetter.getTableConfig(conferenceId, 'transactions').then(function(response) {
			$scope.dataTableConfiguration = response;
		}, function(error) {
			console.log(error);
		});
  }
})();
