(function () {
	'use strict';

	var controllerId = 'registrationLevelOptionsController';
	var app 				 = angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'$scope',
		'$state',
		'toastr',
		'conferenceRegistrationDataService',
		'registrationLevelOptionsDataService',
		'$timeout',
		'ngDialog',
		'$stateParams',
		registrationLevelOptionsController
	];

	app.controller(controllerId, dependenciesArray);

	function registrationLevelOptionsController($rootScope, $scope, $state, toastr, conferenceRegistrationDataService, registrationLevelOptionsDataService, $timeout, ngDialog, $stateParams) {

		var conference = $rootScope.conference;
		var registrationForm = {};

		$timeout(function() {
			$('.registration-level-options').mCustomScrollbar({'theme': 'minimal-dark'});
		}, 0);

		$scope.inEdit = false;
		$scope.isLoading = false;

		$scope.registrationLevels = [];
		$scope.selectedLevel = {};
		$scope.isLoadingCheck = false;
		$scope.currentDate = new Date();

		$scope.registrationPromoCodeModel = {
			"single_use" : false
		};
		$scope.usd_sign = true;

		$scope.membersCsvFields = {};
		$scope.membersCsvKeys = [];
		$scope.memberFileName = '';
		$scope.memberFieldMapping = {};
		$scope.memberFieldMappingObj = {};
		$scope.memberFieldMappingCode = {};

		$scope.registrant_member_model = [];
		$scope.memebersSelectedBool = false;
		var sessionAddonErrors = [];
		getConferenceRegistrationForm();

		function getConferenceRegistrationForm() {
			conferenceRegistrationDataService
				.getRegistrationForm(conference.conference_id)
				.then(function(data) {
					$rootScope.registrationForm = data[0];
					registrationForm = data[0];
					populateLevels(registrationForm);
				}, function(error) {
					toastr.error('Error!', 'There has been a problem with your request.');
				})
		}

		function populateLevels(registrationForm) {
				$scope.registrationLevels = registrationForm.registrationlevel_set;
				angular.forEach($scope.registrationLevels, function(registrationLevel){
						angular.forEach(registrationLevel.registrationlevelsession_set, function(session){
								if (!session.registrationleveladdonsessionpricing_set || session.registrationleveladdonsessionpricing_set.length == 0) {
										session.session.addOnSession = {};
										session.session.addOnSession.add_on_price_model = {
												session: session.session.id,
												registration_level: session.registration_level,
												price: null
										};
								}
						})
				})
				if($state.params.levelId != ''){
					angular.forEach($scope.registrationLevels, function(registrationLevel, index) {
						if(registrationLevel.id == $state.params.levelId) {
							$scope.selectLevel(index);
						}
					})
				} else {
					$scope.selectLevel(0);
				}
		}

		$scope.selectLevel = function(index) {
			$scope.selectedLevel = $scope.registrationLevels[index];
			_.each($scope.selectedLevel.registrationlevelpromocode_set, function(prom_code){
					prom_code.starts_at = moment(prom_code.starts_at);
					prom_code.starts_at_editable = createMomentFromRawTime(prom_code.starts_at_raw);
					prom_code.ends_at = moment(prom_code.ends_at);
					prom_code.ends_at_editable = createMomentFromRawTime(prom_code.ends_at_raw);
			});
			_.each($scope.selectedLevel.registrationlevelsession_set, function(levelSession){
					var existing_add_on = null;
					_.each($scope.selectedLevel.registrationleveladdonsession_set, function(add_on_session){
							if (add_on_session.session.id === levelSession.session.id) {
									if (add_on_session.deleted_at === null) {
											levelSession.session.add_on_selected = true;
									}
									_.each(add_on_session.registrationleveladdonsessionpricing_set, function(pricing){
											pricing.starts_at = moment(pricing.starts_at);
											pricing.starts_at_editable = createMomentFromRawTime(pricing.starts_at_raw);
											pricing.ends_at = moment(pricing.ends_at);
											pricing.ends_at_editable = createMomentFromRawTime(pricing.ends_at_raw);
									});
									levelSession.session.addOnSession = add_on_session;
									if(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length <= 0){
											$scope.addExtraPricingForAddOnSession(levelSession);
									} else{
											levelSession.session.addOnSession.add_on_price_model = null;
											levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
													sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
									}
							}
					});
			});
		}

		// ADDON SESSIONS

		$scope.toggleAddOnSelection = function(levelSession){
				if($scope.isLoadingCheck == false){
						$scope.isLoadingCheck = true;
				if(levelSession.session.hasOwnProperty('add_on_selected')){
						levelSession.session.add_on_selected = !levelSession.session.add_on_selected;
						delete sessionAddonErrors[levelSession.id];
				} else{
						levelSession.session.add_on_selected = true;
						sessionAddonErrors[levelSession.id] = true;
						if (!levelSession.session.addOnSession.registrationleveladdonsessionpricing_set) {
								levelSession.session.addOnSession.registrationleveladdonsessionpricing_set = [];
						}
				}
				if (!levelSession.registration_level) {
						levelSession.registration_level = $scope.selectLevel;
				}
				if(levelSession.session.add_on_selected){
						registrationLevelOptionsDataService
								.createRegistrationAddOnSession(levelSession.registration_level, levelSession.session.id)
								.then(function(response){
										_.each(response.registrationleveladdonsessionpricing_set, function(pricing){
												pricing.starts_at = moment(pricing.starts_at);
												pricing.ends_at = moment(pricing.ends_at);
										});
										levelSession.session.addOnSession = response;
										if(!levelSession.session.addOnSession.hasOwnProperty('add_on_price_model')) {
												$scope.addExtraPricingForAddOnSession(levelSession);
										} else {
												levelSession.session.addOnSession.add_on_price_model.id = response.id;
										}
										levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
												sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
										$scope.selectedLevel.registrationleveladdonsession_set.push(response);
										$scope.isLoadingCheck = false;
								}, function(error){
										$scope.isLoadingCheck = false;
								});
				} else{
						var add_on_session_id = _.find($scope.selectedLevel.registrationleveladdonsession_set, function(add_on_session){
								if(add_on_session.session.id == levelSession.session.id){
										return add_on_session.id;
								}
						});
						registrationLevelOptionsDataService
								.removeRegistrationAddOnSession(add_on_session_id.id)
								.then(function(response){
										$scope.selectedLevel.registrationleveladdonsession_set = _.reject($scope.selectedLevel.registrationleveladdonsession_set, ['id', add_on_session_id.id]);
										$scope.isLoadingCheck = false;
								}, function(error){
										$scope.isLoadingCheck = false;
								});
				}
			}
		};

		$scope.addExtraPricingForAddOnSession = function(levelSession){
			sessionAddonErrors[levelSession.id] = true;
				levelSession.session.addOnSession.add_on_price_model = {
						session: levelSession.session.id,
						registration_level: levelSession.registration_level,
						price: null
				};
		};

		function sortDatesForAddsOnPrices(list_of_prices){
				var dates_array = [];
				_.each(list_of_prices, function (price_model) {
						if (price_model.starts_at_editable !== null && price_model.ends_at_editable !== null) {
								dates_array.push(price_model.starts_at_editable);
								dates_array.push(price_model.ends_at_editable);
						}
				});
				dates_array = _.sortBy(dates_array);
				return dates_array;
		};

		function createMomentFromRawTime(rawDatetime) {
				if (!rawDatetime || !rawDatetime.year) {
						return null;
				}
				return moment(new Date(
						rawDatetime.year,
						rawDatetime.month - 1,
						rawDatetime.day,
						rawDatetime.hour,
						rawDatetime.minute
				));
		}

		$scope.registrationAddOnLevelHasPricing = function(levelSession) {
				return levelSession && levelSession.session.addOnSession && levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length > 0;
		}

		$scope.securePricing = function(pricing) {
				pricing.price = pricing.price.replace(/[^0-9$.,]/g, '');
				var nrDots = pricing.price.split(".").length;
				if (nrDots > 2) {
						var i = pricing.price.lastIndexOf('.');
						if (i != -1) {
								pricing.price = pricing.price.substr(0, i);
						}
				}
		}

		$scope.setAddOnSessionPrice = function(levelSession, model){
				var add_on_session_id = _.find($scope.selectedLevel.registrationleveladdonsession_set, function(add_on_session){
						if(add_on_session.session.id == levelSession.session.id){
								return add_on_session.id;
						}
				});
				if(model.starts_at_editable && model.ends_at_editable && model.price != undefined && parseInt(model.price) >= 0){
					sessionAddonErrors[levelSession.id] = false;
						if(model.hasOwnProperty('id')){
								if (model.starts_at_editable) {
										var starts_at = new Date(model.starts_at_editable);
										model.starts_at_raw = {
												"year": starts_at.getFullYear(),
												"month": starts_at.getMonth() + 1,
												"day": starts_at.getDate()
										}
								}
								if (model.ends_at_editable) {
										var ends_at = new Date(model.ends_at_editable);
										model.ends_at_raw = {
												"year": ends_at.getFullYear(),
												"month": ends_at.getMonth() + 1,
												"day": ends_at.getDate()
										}
								}
								registrationLevelOptionsDataService
										.updateRegistrationAddOnSessionPricing(model.id, model)
										.then(function(response){
												response.starts_at = moment(response.starts_at);
												response.ends_at = moment(response.ends_at);
												response.starts_at_editable = createMomentFromRawTime(response.starts_at_raw);
												response.ends_at_editable = createMomentFromRawTime(response.ends_at_raw);
												var model_index = levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.indexOf(model);
												levelSession.session.addOnSession.registrationleveladdonsessionpricing_set = _.reject(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set, ['id', model.id]);
												levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.splice(model_index, 0, response);
												levelSession.session.addOnSession.registration_adds_on_session_dates_sorted = sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
										}, function(error){
												model.price = 0;
												toastr.error(error.data.detail);
										});
						} else{
								if (model.starts_at_editable) {
										var starts_at = new Date(model.starts_at_editable);
										model.starts_at_raw = {
												"year": starts_at.getFullYear(),
												"month": starts_at.getMonth() + 1,
												"day": starts_at.getDate()
										}
								}

								if (model.ends_at_editable) {
										var ends_at = new Date(model.ends_at_editable);
										model.ends_at_raw = {
												"year": ends_at.getFullYear(),
												"month": ends_at.getMonth() + 1,
												"day": ends_at.getDate()
										}
								}
								registrationLevelOptionsDataService
										.createRegistrationAddOnSessionPricing(add_on_session_id.id, model)
										.then(function(response){
												response.starts_at = moment(response.starts_at);
												response.ends_at = moment(response.ends_at);
												response.starts_at_editable = createMomentFromRawTime(response.starts_at_raw);
												response.ends_at_editable = createMomentFromRawTime(response.ends_at_raw);
												response.can_be_remove = true;
												levelSession.session.addOnSession.add_on_price_model = null;
												if(!levelSession.session.addOnSession.registrationleveladdonsessionpricing_set){
														levelSession.session.addOnSession.registrationleveladdonsessionpricing_set = [];
												}
												levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.push(response);
												levelSession.session.addOnSession.registration_adds_on_session_dates_sorted = sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);

										}, function(error){
												model.price = 0;
												toastr.error(error.data.detail);
										});
						}
				}
				else{
					sessionAddonErrors[levelSession.id] = true;
				}
		};

		$scope.findIndexOfThisDate = function(selectedDate, date_type, list_of_sorted_dates) {
				if (!selectedDate) {
						return $scope.currentDate;
				}
				var date_index;
				if(date_type === 'start') {
						date_index = _.findIndex(list_of_sorted_dates, function (price_date) {
								if (!price_date) {
										return false;
								}
								return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				} else if(date_type === 'end'){
						date_index = _.findLastIndex(list_of_sorted_dates, function (price_date) {
								if (!price_date) {
										return false;
								}
								return selectedDate.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				}
				if(date_type === 'start' && date_index > 0){
						date_index -= 1;
				} else if(date_type === 'end' && date_index < (list_of_sorted_dates.length - 1)) {
						date_index += 1;
				} else if(date_index === (list_of_sorted_dates.length - 1) || date_index === 0 ) {
						date_index = -1;
				}
				if(date_index != -1){
						var date_to_return = list_of_sorted_dates[date_index];
						if (!date_to_return) {
								return false;
						}
						if(date_type === 'end'){
								return date_to_return.clone().subtract(1, 'days').format('MM/DD/YYYY');
						} else if(date_type === 'start'){
								return date_to_return.clone().add(1, 'days').format('MM/DD/YYYY');
						}
				} else if(date_type === 'end'){
						return $scope.conference_date_to_moment;
				} else if(date_type === 'start'){
						return null;
				}
		};

		$scope.removePricingFromAddOnSession = function(levelSession, model){
			  delete sessionAddonErrors[levelSession.id];
				if(!model.hasOwnProperty('id')){
						levelSession.session.addOnSession.add_on_price_model = null;
						return;
				}
				registrationLevelOptionsDataService
						.removeRegistrationAddOnSessionPricing(model.id)
						.then(function(response){
								levelSession.session.addOnSession.registrationleveladdonsessionpricing_set =
										_.reject(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set, ['id', model.id]);
								if(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set.length <= 0){
										$scope.addExtraPricingForAddOnSession(levelSession);
								} else{
										levelSession.session.addOnSession.registration_adds_on_session_dates_sorted =
												sortDatesForAddsOnPrices(levelSession.session.addOnSession.registrationleveladdonsessionpricing_set);
								}
						}, function(error){
								toastr.error(error.data.detail);
						});
		};

		$scope.verifyIfDateIsAvailableForAddOnSessionPrices = function(date, type, price_model, add_on, picker_type){
				var list_of_prices = add_on.registrationleveladdonsessionpricing_set;
				var sorted_dates_for_add_on = add_on.registration_adds_on_session_dates_sorted;

				if (date > conference.date_to_editable) {
						return false;
				}

				if (price_model && price_model.hasOwnProperty('starts_at_editable')) {
						price_model.starts_at = price_model.starts_at_editable;
				}
				if (price_model && price_model.hasOwnProperty('ends_at_editable')) {
						price_model.ends_at = price_model.ends_at_editable;
				}

				if(type !== 'day' || list_of_prices === undefined || list_of_prices.length === 0){
						return true;
				}

				var ok = true;
				if(price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) {
						var model_start_index = _.findIndex(sorted_dates_for_add_on, function(price_date){
								return price_model.starts_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
						var model_end_index = _.findIndex(sorted_dates_for_add_on, function(price_date){
								return price_model.ends_at.format('MM/DD/YYYY') === price_date.format('MM/DD/YYYY');
						});
				}

				if(picker_type === 'end'){
						var price_date = _.find(sorted_dates_for_add_on, function(price_date){
								if (!price_date || !price_model.starts_at) {
										return null;
								}
								return price_date.format('MM/DD/YYYY') > price_model.starts_at.format('MM/DD/YYYY');
						});
						if(price_date === undefined){
								ok = true;
						} else if(price_date != undefined && (date.format('MM/DD/YYYY') < price_date.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY'))){
								ok = true;
						} else{
								ok = false;
						}
						return ok;
				}

				if(!price_model.hasOwnProperty('starts_at') || !price_model.hasOwnProperty('ends_at')){
						for(var i = 0; i < sorted_dates_for_add_on.length; i+=2){
								var j = i + 1;
								if(sorted_dates_for_add_on[i].format('MM/DD/YYYY') <= date.format('MM/DD/YYYY') &&
										date.format('MM/DD/YYYY') <= sorted_dates_for_add_on[j].format('MM/DD/YYYY')){
										ok = false;
										break;
								}
						}
				} else if((price_model != null && price_model != undefined && price_model.hasOwnProperty('starts_at') && price_model.hasOwnProperty('ends_at')) &&
						(date.format('MM/DD/YYYY') >= price_model.starts_at.format('MM/DD/YYYY') && date.format('MM/DD/YYYY') <= price_model.ends_at.format('MM/DD/YYYY')) ||
						(date.format('MM/DD/YYYY') < price_model.starts_at.format('MM/DD/YYYY') &&
								(model_start_index > 0 && date.format('MM/DD/YYYY') > sorted_dates_for_add_on[model_start_index - 1].format('MM/DD/YYYY') ||
								(model_start_index === 0 && date.format('MM/DD/YYYY') >= $scope.currentDate))
						) ||
						(date.format('MM/DD/YYYY') > price_model.ends_at.format('MM/DD/YYYY') &&
								(
										(model_end_index < (sorted_dates_for_add_on.length - 1) && date.format('MM/DD/YYYY') < sorted_dates_for_add_on[model_end_index + 1].format('MM/DD/YYYY'))
										|| (model_end_index === (sorted_dates_for_add_on.length - 1))
								)
						)
				){
						ok = true;
				} else {
						ok = false;
				}

				return ok;
		};

		// CONTINUING EDUCATION

		$scope.includeContinuingEducationInLevel = function(answer){
			if(answer){
				$scope.selectedLevel.include_continuing_education = true;
			} else{
				$scope.selectedLevel.include_continuing_education = false;
			}
			conferenceRegistrationDataService
				.updateRegistrationLevel($scope.selectedLevel)
				.then(function(response){

				}, function(error){

				});
		};

		// PRE REGISTRATION

		$scope.includePreRegistrationInLevel = function(answer){
			if(answer){
				$scope.selectedLevel.include_pre_registration = true;
			} else{
				$scope.selectedLevel.include_pre_registration = false;
			}
			conferenceRegistrationDataService
				.updateRegistrationLevel($scope.selectedLevel)
				.then(function(response){
				})
				.catch(function(error){

				});
		};

		// PROMO CODES

		$scope.generatePromoCode = function(){
			$scope.registrationPromoCodeModel.code = '';
			var length = 0;
			while(length <= 7){
				var chr_number = Math.floor(Math.random() * (91 - 65) + 65);
				var character = String.fromCharCode(chr_number);
				if( Math.floor(Math.random() * (3 - 1) + 1) % 2 == 0){
					$scope.registrationPromoCodeModel.code += character;
				} else{
					$scope.registrationPromoCodeModel.code += String.fromCharCode(Math.floor(Math.random() * (58 - 48) + 48));
				}
				length++;
			}
		};

		$scope.changeDiscountType = function(){
			if(document.getElementById('promo-code-dicount-type').value == 'USD'){
					$scope.usd_sign = false;
					document.getElementById('promo-code-dicount-type').value = 'PCT';
					document.getElementById('promo-code-dicount-value').innerHTML = '%';
			} else{
					$scope.usd_sign = true;
					document.getElementById('promo-code-dicount-type').value = 'USD';
					document.getElementById('promo-code-dicount-value').innerHTML = '$';
			}
		};

		$scope.promoCodeToggle = function(promo_code) {
				if (promo_code.single_use == undefined) promo_code.single_use = false;
				promo_code.single_use = !promo_code.single_use;
				if (!promo_code.id || promo_code.id == undefined) return;
				registrationLevelOptionsDataService
						.updateRegistrationPromoCode(promo_code.id, promo_code)
						.then(function(response){

						},function(error){

						});
		}

		$scope.createRegistrationPromoCode = function() {
			var ok = _.find($scope.registrationPromoCodeModel, function(promo_code_prop) {
				return promo_code_prop == '';
			});
			if(!$scope.registrationPromoCodeModel.hasOwnProperty('starts_at_editable') ||
					!$scope.registrationPromoCodeModel.hasOwnProperty('ends_at_editable') ||
					!$scope.registrationPromoCodeModel.hasOwnProperty('value')) {
					ok = true;
					toastr.error("Please make sure you've setup name and effective dates the promo code!");
			}
			if (!$scope.registrationPromoCodeModel.value ||
					$scope.registrationPromoCodeModel.value <= 0) {
					toastr.error('Please chose a promo code value higher than 0!');
					ok = true;
					return;
			}
			var discount = document.getElementById('promo-code-dicount-type').value;
			if(discount == "USD") {
				$scope.registrationPromoCodeModel.discount_type = 'usd';
			} else {
				$scope.registrationPromoCodeModel.discount_type = 'pct';
			}
			if (ok == undefined || ok == false) {
				if ($scope.registrationPromoCodeModel.starts_at_editable && $scope.registrationPromoCodeModel.ends_at_editable) {
						var starts_at = new Date($scope.registrationPromoCodeModel.starts_at_editable);
						$scope.registrationPromoCodeModel.starts_at_raw = {
								"year": starts_at.getFullYear(),
								"month": starts_at.getMonth() + 1,
								"day": starts_at.getDate()
						}

						var ends_at = new Date($scope.registrationPromoCodeModel.ends_at_editable);
						$scope.registrationPromoCodeModel.ends_at_raw = {
								"year": ends_at.getFullYear(),
								"month": ends_at.getMonth() + 1,
								"day": ends_at.getDate()
						}
						registrationLevelOptionsDataService
							.addRegistrationPromoCodes($scope.selectedLevel.id, $scope.registrationPromoCodeModel)
							.then(function(response){
								toastr.success("Promo code " + $scope.registrationPromoCodeModel.code + " was successfully created!");
								response.starts_at = moment(response.starts_at);
								response.ends_at = moment(response.ends_at);
								response.starts_at_editable = createMomentFromRawTime(response.starts_at_raw);
								response.ends_at_editable = createMomentFromRawTime(response.ends_at_raw);
								$scope.selectedLevel.registrationlevelpromocode_set.push(response);
								$scope.registrationPromoCodeModel = {
									"single_use" : false
								};
							}, function(error) {
									if(typeof error.data.detail === 'object') {
											var firstField = Object.keys(error.data.detail)[0];
											var firstFieldFirstError = error.data.detail[firstField][0];
											var errorMessage = firstField + ": " + firstFieldFirstError;
											toastr.error(errorMessage);
									} else {
											toastr.error(error.data.detail);
									}
							});
				}
			}
		};

		$scope.updateRegistrationPromoCode = function(promo_code_item){
			var ok = _.find(promo_code_item, function(promo_code_prop){
				return promo_code_prop !== 'single_use' && promo_code_prop === '';
			});
			if (!promo_code_item.value || promo_code_item.value <= 0) {
					toastr.error('Please chose a promo code value higher than 0');
					ok = true;
					return;
			}
			if(ok == undefined){
					if (promo_code_item.starts_at_editable) {
							var starts_at = new Date(promo_code_item.starts_at_editable);
							promo_code_item.starts_at_raw = {
									"year": starts_at.getFullYear(),
									"month": starts_at.getMonth() + 1,
									"day": starts_at.getDate()
							}
					}
					if (promo_code_item.ends_at_editable) {
							var ends_at = new Date(promo_code_item.ends_at_editable);
							promo_code_item.ends_at_raw = {
									"year": ends_at.getFullYear(),
									"month": ends_at.getMonth() + 1,
									"day": ends_at.getDate()
							}
					}
					registrationLevelOptionsDataService
							.updateRegistrationPromoCode(promo_code_item.id, promo_code_item)
							.then(function(response){
									toastr.success("Promo code updated!");
							}, function(error) {
									toastr.error(error.data.detail);
									promo_code_item.value = null;
							});
			}
		}

		$scope.deleteRegistrationPromoCode = function(promo_code_id){
			registrationLevelOptionsDataService
					.removeRegistrationPromoCode(promo_code_id)
					.then(function(response){
						$scope.selectedLevel.registrationlevelpromocode_set = _.reject($scope.selectedLevel.registrationlevelpromocode_set, ['id', promo_code_id]);
					}, function(error){

					});
		}

		// LEVEL GUEST NUMBER

		$scope.changeLevelGuestsNr = function(level) {
			if (typeof level.guest_fee === 'undefined') {
				toastr.error("Guest fee must be higher than 0!");
				return
			}
				conferenceRegistrationDataService
						.updateRegistrationLevel(level)
						.then(function(response) {
							toastr.success("Level options saved successfully!");
						}, function(error) {
							toastr.error(error.data.detail);
						});
		}

		$scope.toggleGuestEmailRequirement = function(state) {
			$scope.selectedLevel.require_guest_email = state;
			$scope.changeLevelGuestsNr($scope.selectedLevel);
		}

		// MEMBERS

		$scope.uploadLevelMembers = function(files){
			var reader = new FileReader();
			$scope.membersCsvKeys = [];
		  reader.readAsText(files[0], 'ISO-8859-1');
			reader.onload = function(e) { // doesnt work
					var data = lib.CSV2JSON(reader.result);
					$scope.membersCsvFields = data;
					var keys = Object.keys(data[0]);
					_.each(keys, function(csvField) {
						 $scope.membersCsvKeys.push({
							'code': csvField
						 });
					});
					$scope.memberFileName = files[0].name;
					_.each($scope.membersCsvKeys, function(csvField) {
							csvField.mapping = null;
					});
					_.each($scope.memberMappingFields, function(field) {
							field.mapping = null;
							field.is_hovered = null;
					});

					openMapMemberInformation();
				  $scope.$apply();
			}
		}

		function openMapMemberInformation() {
				var data = {};
				data.membersCsvKeys = $scope.membersCsvKeys;
				data.memberMappingFields = $scope.memberMappingFields;
				data.selectedLevel = $scope.selectedLevel;
				data.membersToMap = $scope.membersCsvFields;
				ngDialog.open({
						plain: true,
						template: '<map-members-information-modal></map-members-information-modal>',
						className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-level-options/_components/mapMembersInformationModal/mapMembersInformationModalStyles.scss',
						data: data
				});
		}

		$scope.addRegistrationMember = function(){
				var empty = _.find($scope.registrant_member_model[0], _.isEmpty());
				if(empty === undefined){
						registrationLevelOptionsDataService
								.bulkCreateMembers($scope.registrant_member_model, $scope.selectedLevel.id)
								.then(function(response){
										registrationLevelOptionsDataService
												.getRegistrationMembers($scope.selectedLevel.id)
												.then(function(membersList){
														$scope.selectedLevel.registrationlevelmember_set = membersList;
												});
										$scope.registrant_member_model = [{
												'first_name' : '',
												'last_name' : '',
												'email': ''
										}];
								}, function(error){
									lib.processValidationError(error, toastr);
								});
				}
		};

		$scope.removeRegistrationMembers = function(){
				var memebersToRemove = '';
				_.each($scope.selectedLevel.registrationlevelmember_set, function(member){
						if(member.selected && member.selected == true){
								memebersToRemove += member.id + ',';
						}
				});
				memebersToRemove = memebersToRemove.substring(0, (memebersToRemove.length - 1));
				registrationLevelOptionsDataService
						.removeRegistrationMembers($scope.selectedLevel.id, memebersToRemove)
						.then(function(response){
								$scope.selectedLevel.registrationlevelmember_set = _.reject($scope.selectedLevel.registrationlevelmember_set, function(member){
										if(member.selected && member.selected == true){
												return member;
										}
								});
								$scope.memebersSelectedBool = false;
						}, function(error){

						});
		};

		$scope.selectRegistrationMember = function(member){
				if(member){
						if(member.selected){
								member.selected = !member.selected;
						} else{
								member.selected = true;
						}
				}
				var ok = false;
				if($scope.selectedLevel.registrationlevelmember_set){
						_.each($scope.selectedLevel.registrationlevelmember_set, function(member){
								if(member.selected && member.selected == true){
										ok = true;
								}
						});
				} if(ok){
						$scope.memebersSelectedBool = true;
				} else{
						$scope.memebersSelectedBool = false;
				}
		}

		// FINISH

		$scope.nextStep = function() {
			var error = false;
			sessionAddonErrors.forEach(function(level){
				if(level == true){
					toastr.error('Please provide all details if you want to create an add-on for this registration form!');
					error = true;
				}
			});
			if(!error){
				if($stateParams.inEdit == true){
						$state.go('conferences.dashboard.registration.review');
				} else {
						$state.go('conferences.dashboard.registration.legal');
				}
			}
		}

		$rootScope.$on('UPLOADED_MEMBERS_LIST_SUCCESSFULLY', function(event, data) {
			$scope.selectedLevel.registrationlevelmember_set = data;
		})

  }
})();
