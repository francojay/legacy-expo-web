(function(){
    'use strict';

    var controllerId = 'conferenceUsersController';

    angular
        .module('app')
        .controller(controllerId, ['$scope', '$rootScope', 'toastr', 'ngDialog', '$localStorage', 'conferenceUsersService', conferenceUsersController
        ]);

    function conferenceUsersController($scope, $rootScope, toastr, ngDialog, $localStorage, conferenceUsersService){
        $scope.conference             = $rootScope.conference;
        $scope.conferenceUsers        = $rootScope.conference.conference_users;
        $scope.conferencePermissions  = $rootScope.conference.permissions;
        $scope.conference_id          = $rootScope.conference.conference_id;
        $scope.conferenceOwner        = $rootScope.conference.user;
        $scope.searchEmail = false;
        $scope.usersPermissionsPane = false;
        $scope.userOrderAsc = true;
        $scope.checkAllUsers = false;
        $scope.nr_user_slots = localStorage.conference_number_user_slots;
        $scope.user = JSON.parse(localStorage.currentUser);
        $scope.addConferenceUserLock = false;
        $scope.userPermissions = [
            'edit_conference', 'edit_conference_users', 'edit_attendee_fields', 'download_attendees',
            'manage_attendees', 'manage_exhibitors', 'manage_sessions', 'scan_attendees', 'can_refund', 'edit_registration'
        ];
        $scope.invitedUser = {
            conference_id: $scope.conference_id,
            rights: $scope.userPermissions,
            invited_user_email: ''
        };

        $scope.checkedUserList = [];
        $scope.idList= [];

        $scope.validateNewUser = function(newUser) {
          if(newUser.invited_user_email &&
             newUser.invited_user_email != '' &&
             newUser.invited_user_email.search(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/) > -1){ // email regex
              $scope.searchEmail = true;
            } else {
              $scope.searchEmail = false;
            }
        }

        $scope.permissionsForUsers = function() {
            var canEditConferenceUsers = $scope.conferencePermissions.indexOf("edit_conference_users") >= 0;
            var isAdmin = $scope.conferencePermissions.indexOf("admin") >= 0;

            if(_.isEmpty($scope.conferencePermissions)) return false;
            if(canEditConferenceUsers || isAdmin) {
                return true;
            }
            else {
                return false;
            }
        };

        $scope.verifySingleUserChecked = function(){
            var nr = 0;
             _.each($scope.conferenceUsers, function(user) {
                if (user.checked == true) {
                    nr++;
                }
            });
             if(nr == 1){
                return true;
             } else
                return false;
        };

        $rootScope.$on('DELETE_CONFERENCE_USERS', function(){
            deleteUsersConfirmed($scope.idList, $scope.checkedUserList);
        });

        $rootScope.$on('DELETE_ALL_CONFERENCE_USERS', function(){
            deleteAllUsersConfirmed();
        });

        $scope.updateUsersSlotsCount = function(nrUsers) {
            $scope.conference.nr_user_slots += nrUsers;
        }

        $scope.openConferenceUsersPurchaseForm = function(){
            $scope.formLabels = $scope.formLabels;
            $scope.updatedExhibitor = $scope.updatedExhibitor;
            $scope.paymentCodes = $scope.paymentCodes;
            $scope.conference = $scope.conference;
            $scope.user = $scope.user;
            $scope.changeCreditCard = $scope.changeCreditCard;
            $scope.updatePromoCodes = $scope.updatePromoCodes;
            $scope.changeConferencePayment = false;
            $scope.updateUser = $scope.updateUser;
            $scope.updateUsersSlotsCount = $scope.updateUsersSlotsCount;

            $scope.popup = ngDialog.open({
                  plain: true,
                  template: '<user-payment-modal></user-payment-modal>',
                  className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/_components/userPaymentModal/userPaymentModalStyle.scss',
                  controller: "ConferenceExhibitorsController as vm",
                  scope: $scope
            });
        };

        $scope.sortBy = function(value, order){
            value = value + '';
            if(order == true){
                $scope.conferenceUsers =  _.orderBy($scope.conferenceUsers, value , 'asc');
            } else{
                $scope.conferenceUsers =  _.orderBy($scope.conferenceUsers, value , 'desc');
            }
        };

        $scope.deleteSelectedUsers = function() {
            if ($scope.checkAllUsers) {
                var data = {};
                data.numberOfUsers = -1;
                ngDialog.open({
                      plain: true,
                      template: '<delete-conference-users-modal></delete-conference-users-modal>',
                      className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/_components/deleteConferenceUsersModal/deleteConferenceUsersStyle.scss',
                      data: data
                });
            }
            else {
                $scope.checkedUserList = [];
                $scope.idList = [];
                _.each($scope.conferenceUsers, function(user) {
                    if (user.checked == true && user.conference_user_rights.user_id !== $scope.user.id) {
                        $scope.checkedUserList.push(user);
                        $scope.idList.push(user.conference_user_id);
                    }
                });
                if ($scope.idList.length > 0) {
                    // $scope.nrAttendees = idList.length;
                    // $scope.deleteAttendeesConfirmed = $scope.deleteUsersConfirmed;
                    // $scope.checkedAttendeeList = checkedUserList;
                    // $scope.ids = idList;
                    // if (idList.length == 1) $scope.resource = 'user';
                    // else $scope.resource = 'users';
                    // $scope.type = "partial";
                    var data = {};
                    data.numberOfUsers = $scope.idList.length;
                    ngDialog.open({
                          plain: true,
                          template: '<delete-conference-users-modal></delete-conference-users-modal>',
                          className: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/_components/deleteConferenceUsersModal/deleteConferenceUsersStyle.scss',
                          data: data
                    });
                }
            }
        }

        function deleteAllUsersConfirmed() {
          conferenceUsersService.removeAllUsers($scope.conference.conference_id)
            .then(function(response){
                $scope.conferenceUsers = [];
                $scope.users_total_count = 0;
                $scope.userChecked = false;
                $scope.checkAllUsers = false;
            }, function(error) {
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                    toastr.error('Error!', 'There has been a problem with your request.');
                }
            });
        }

        function deleteUsersConfirmed(idList, checkedUserList) {
            conferenceUsersService.removeMultipleUsers($scope.conference.conference_id, idList).then(function(result) {
              // _.each(checkedUserList, function(user) {
              //     var index = $scope.conferenceUsers.indexOf(user);
              //     if (index > -1) {
              //         $scope.conferenceUsers.splice(index, 1);
              //     }
              // });

              var copyOfUsers = angular.copy($scope.conferenceUsers);
              angular.forEach(idList, function(id) {
                angular.forEach(copyOfUsers, function(user, index){
                  if(user.conference_user_id == id) {
                    copyOfUsers.splice(index,1);
                  }
                })
              });
              $scope.userChecked = false;
              $scope.conferenceUsers = copyOfUsers;
            }, function(error) {
              if (error.status == 403) {
                  toastr.error('Permission Denied!');
              } else {
                  toastr.error('Error!', 'There has been a problem with your request.');
              }
            });
        }

        $scope.changePermission = function(option){
            var currentPermissions = $scope.userPermissions;
            var index = currentPermissions.indexOf(option);
            if (index > -1) {
                currentPermissions.splice(index, 1);
                $scope.updateRightsPerUser(currentPermissions);
            }
            else {
                currentPermissions.push(option);
                $scope.updateRightsPerUser(currentPermissions);
            }

            currentPermissions = Array.from(new Set(currentPermissions));
            if($scope.verifySingleUserChecked()){
                _.each($scope.conferenceUsers, function(user) {
                    if (user.checked == true) {
                        currentPermissions = user.rights;
                    }
                });
                $scope.userPermissions = currentPermissions;
            }
            $scope.invitedUser.rights = $scope.userPermissions;
        };

        $scope.testIfUserChecked = function() {
          var checkedUsersList = [];

          _.each($scope.conferenceUsers, function(user) {
              if (user.checked == true) {
                  checkedUsersList.push(user);
              }
          });
          if (checkedUsersList.length > 0) {
              $scope.userChecked = true;
          }
          else {
              $scope.userChecked = false;
          }
          if (checkedUsersList.length === $scope.conferenceUsers.length) {
            $scope.checkAllUsers = true;
          } else {
            $scope.checkAllUsers = false;
          }
        }

        $scope.updateRightsPerUser = function(rights_array){
            var user_id = -1;
            _.each($scope.conferenceUsers, function(user) {
                if (user.checked == true) {
                    user_id = user.conference_user_id;
                }
            });
            if(user_id != -1){
            conferenceUsersService
                .updateUserRights(user_id, rights_array)
                .then(function(response){
                    toastr.success('User permissions updated successfully!');
                })
                .catch(function(error){

                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
            }
        };

        $scope.usersPermissions = function() {
            $scope.usersPermissionsPane = !$scope.usersPermissionsPane;
            if($scope.usersPermissionsPane){
                _.each($scope.conferenceUsers, function(user) {
                    if (user.checked == true) {
                        $scope.userPermissions = user.rights;
                    }
                });
            }
        };

        $scope.checkOne = function(element, name){
            if (element.checked != true) {
                element.checked = true;
            } else {
                element.checked = false;
            }
            if(name === 'user'){
                $scope.userPermissions = element.rights;
                $scope.testIfUserChecked();
            } else if(name === 'attendee') {
                $scope.testIfAttendeeChecked();
            } else if(name === 'exhibitor') {
                $scope.testIfExhibitorChecked();
            } else if(name === 'session_registrants') {
                $scope.testIfSessionRegistrantsChecked();
            } else if(name === 'session_scanned') {
                $scope.testIfSessionScannedChecked();
            }
        };

        $scope.checkAll = function(array) {
          $scope.checkAllUsers = !$scope.checkAllUsers;
          _.each(array, function(element) {
              if(element.conference_user_rights.user_id !== $scope.user.id){
                  element.checked = $scope.checkAllUsers;
              } else if(!$scope.checkAllUsers){
                  element.checked = $scope.checkAllUsers;
              }
          });
          $scope.testIfUserChecked();
        };

        $scope.verifySingleUserChecked = function(){
            var nr = 0;
             _.each($scope.conferenceUsers, function(user) {
                if (user.checked == true) {
                    nr++;
                }
            });
             if(nr == 1){
                return true;
             } else
                return false;
        };

        $scope.verifyCurrentUser = function(){
            var isFound, usersChecked = [], ok = true;
            isFound = _.find($scope.conference.conference_users, function(conference_user){
                return (conference_user.conference_user_rights.user_id === $scope.user.id);
            }) || undefined;
            _.each($scope.conferenceUsers, function(current_user){
                if(current_user.checked){
                    usersChecked.push(current_user);
                }
            });
            _.each(usersChecked, function(userChecked){
                if((isFound !== undefined) && isFound.conference_user_rights.user_id === userChecked.conference_user_rights.user_id){
                    ok = false;
                    return;
                }
            });
           return ok;
        };

        $scope.addUser = function() {
            if ($scope.searchEmail == false) {
              toastr.warning('Invalid user email address!');
              return;
            }
            if ($scope.addConferenceUserLock) return;
            var ok = true;
            var invalidUser = false;
            _.forEach($scope.conferenceUsers, function(userFromTable){
                if($scope.invitedUser.invited_user_email === userFromTable.invited_user_email){
                    ok = false;
                }
            });
            if(($scope.invitedUser.invited_user_email == $scope.user.email) ||
            ($scope.conferenceOwner.email == $scope.invitedUser.invited_user_email)){
                invalidUser = true;
            }
            if(ok && !invalidUser){
                $scope.addConferenceUserLock = true;
                conferenceUsersService.inviteUser($scope.invitedUser, $scope.conference_id)
                    .then(function(data){
                      //reset fields
                      $scope.conferenceUsers.push(data);
                      $scope.invitedUser = {
                          conference_id: $scope.conference_id,
                          rights: [],
                          invited_user_email: ''
                      };
                      $scope.searchEmail = false;
                      $scope.addConferenceUserLock = false;
                      toastr.success('The user has been invited to your event.', 'Invitation Sent!');
                      $scope.invitedUser.invited_user_email = "";
                      $scope.userPermissions = [
                          'edit_conference', 'edit_conference_users', 'edit_attendee_fields', 'download_attendees',
                          'manage_attendees', 'manage_exhibitors', 'manage_sessions', 'scan_attendees', 'can_refund', 'edit_registration'
                      ];
                    })
                    .catch(function(error){
                        $scope.addConferenceUserLock = false;
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else if(error.data.detail) {
                            toastr.error(error.data.detail);
                        }
                    });
            } else if(!ok){
                toastr.warning('This user has been already invited to your event.');
            } else if(invalidUser){
                toastr.warning("You can't perform this action!")
            }
        }

        function getConferenceUsers(){
            conferenceUsersService
            .getAllUsers($scope.conference_id)
            .then(function(response){
                $scope.conferenceUsers = response;
            }, function(error){
                console.log(error);
            });
        }
    }
})();
