(function(){
	'use strict';

    var serviceId = 'conferenceDetailsService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', conferenceDetailsService
		]);
		function conferenceDetailsService($q, requestFactory){
		    		var service = {};

						service.createSupportContact = function(contact){
								var deferred = $q.defer();

								requestFactory.post(
										'api/v1.1/conference_support_contacts/' + contact.conference_id,
										contact,
										false
								)
								.then(function(response){
										deferred.resolve(response.data);
								})
								.catch(function(error){
										deferred.reject(error);
								});

								return deferred.promise;
						}

						service.updateSupportContact = function(contact){
                var deferred = $q.defer();

                requestFactory.post(
                    'api/v1.1/conference_support_contact/' + contact.id + '/',
                    JSON.stringify(contact),
                    false
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

						service.deleteSupportContact = function(contact_id){
                var deferred = $q.defer();

                requestFactory.remove(
                    'api/conference/support_contact_delete' + '?conference_support_contact_id=' + contact_id,
                    {},
                    true
                )
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(error){
                    deferred.reject(error);
                });

                return deferred.promise;
            }

            return service;
        }
})();
