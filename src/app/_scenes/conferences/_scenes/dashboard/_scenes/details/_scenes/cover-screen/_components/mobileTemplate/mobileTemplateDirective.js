(function () {
    'use strict';
    var directiveId = 'mobileTemplate';
    var app         = angular.module('app');
    var dependenciesArray = [
      '$rootScope',
      'SharedProperties',
      mobileTemplateDirective
    ];

    app.directive(directiveId, dependenciesArray);

    function mobileTemplateDirective($rootScope, SharedProperties) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/cover-screen/_components/mobileTemplate/mobileTemplateView.html',
            scope: {
                overlayMode: '@overlayMode',
                displayOverlay: '=displayOverlay',
                textColor: '=textColor',
                backgroundDefaultNumber: '=backgroundDefaultNumber',
                backgroundImages: '=backgroundImages'
            },
            link: link
        };

        function link(scope) {
            scope.currentConference = $rootScope.conference;
            scope.coverScreenOptions = SharedProperties.coverScreenOptions;

            var sliderIndex = parseInt(scope.backgroundDefaultNumber) - 1; // -1 because when translating we start from 0 * width [the first image isn't translated]
            var slidingValue = -375;

            scope.$watch(function(){return SharedProperties.coverScreensImagesOptions}, function (images) { //this is where we update the current values that render the mobile template with the values from the coverScreenController
                scope.backgroundImages = images;
                scope.numberOfImages = scope.backgroundImages.length;
                scope.sliderWidth = scope.backgroundImages.length * 100;
            });

            scope.showPrevButton = true;
            scope.showNextButton = true;

            scope.currentIndex = sliderIndex + 1;

            scope.numberOfImages = scope.backgroundImages.length;
            scope.sliderWidth = scope.backgroundImages.length * 100;

            scope.currentTime = moment().format('HH:mm');
            scope.percentage = 100;

            initializeSlider(sliderIndex);
            initializeConferenceInformation(scope.currentConference);

            scope.nextSlide = function() {
                if (sliderIndex < scope.backgroundImages.length - 1) {
                    sliderIndex++;
                    scope.currentIndex++;
                    scope.showPrevButton = true;
                    if (sliderIndex === scope.backgroundImages.length - 1) {
                        scope.showNextButton = false;
                    }
                } else {
                    scope.showNextButton = false;
                }
                scope.translateValue = sliderIndex * slidingValue;

                SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;
            };

            scope.prevSlide = function() {
                if (sliderIndex > 0) {
                    sliderIndex--;
                    scope.currentIndex--;
                    scope.showNextButton = true;
                    if (sliderIndex === 0) {
                        scope.showPrevButton = false;
                    }
                } else {
                    scope.showPrevButton = false;
                }
                scope.translateValue = sliderIndex * slidingValue;

                SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;
            };

            function initializeSlider(passedIndex) {
                if (passedIndex === 0) {
                    scope.showPrevButton = false;
                } else if (passedIndex === scope.numberOfImages) {
                    scope.showNextButton = false;
                }

                scope.translateValue = sliderIndex * slidingValue;
                SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;

                setInterval(function() {
                    scope.currentTime = moment().format('HH:mm');
                    if (scope.percentage > 0) {
                      scope.percentage--;
                    }
                    scope.$apply();
                }, 20000);
            }

            function initializeConferenceInformation(conference) {
                var momentDateFromWithTz     = moment(conference.date_from).tz(conference.timezone_name);
                var momentDateWithCorrection = momentDateFromWithTz;
                var date_from  = momentDateWithCorrection.format('M/D/Y');
                var momentDateToWithTz         = moment(conference.date_to).tz(conference.timezone_name);
                var momentDateToWithCorrection = momentDateToWithTz;
                var date_to      = momentDateToWithCorrection.format('M/D/Y');
                scope.conferenceDates = date_from + ' - ' +  date_to;
            }

            $rootScope.$on('overlayModeChanged', function(event, data) {
                scope.overlayMode = data.overlayMode;
            });
        }
    }
})();
