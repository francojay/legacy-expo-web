(function(){
    'use strict';

    var directiveId = 'userPaymentModal';

    angular
        .module('app')
        .directive(directiveId, userPaymentModal);

    function userPaymentModal (){

        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/_components/userPaymentModal/userPaymentModalView.html',
            link: link
        };

        function link(scope){

        }

    }
})();
