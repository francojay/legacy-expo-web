(function(){
	'use strict';

    var serviceId = 'travelService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', travelService
		]);
		function travelService($q, requestFactory){
		    var service = {};

        service.createHotel = function(conference_id, travelModel){

            travelModel.country = travelModel.country.trim();

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/hotels/' + conference_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.createCarRental = function(conference_id, travelModel){

            travelModel.country = travelModel.country.trim();

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/car_rentals/' + conference_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.createAirline = function(conference_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/airlines/' + conference_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.createTransportation = function(conference_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/transportations/' + conference_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getHotel = function(travel_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/hotel/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getCarRental = function(travel_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/car_rental/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getAirline = function(travel_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/airline/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getTransportation = function(travel_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/transportation/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getHotels = function(conference_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/hotels/' + conference_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getCarRentals = function(conference_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/car_rentals/' + conference_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getAirlines = function(conference_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/airlines/' + conference_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.getTransportations = function(conference_id){

            var deferred = $q.defer();
            requestFactory.get(
                'api/v1.1/transportations/' + conference_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.updateHotel = function(travel_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/hotel/' + travel_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.updateCarRental = function(travel_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/car_rental/' + travel_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.updateAirline = function(travel_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/airline/' + travel_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.updateTransportation = function(travel_id, travelModel){

            var deferred = $q.defer();
            requestFactory.post(
                'api/v1.1/transportation/' + travel_id + '/',
                travelModel,
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.deleteHotel = function(travel_id){

            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/hotel/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.deleteCarRental = function(travel_id){

            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/car_rental/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.deleteAirline = function(travel_id){

            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/airline/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

        service.deleteTransportation = function(travel_id){

            var deferred = $q.defer();
            requestFactory.remove(
                'api/v1.1/transportation/' + travel_id + '/',
                {},
                false
            )
            .then(function(response){
                 deferred.resolve(response.data);
            })
            .catch(function(error){
                 deferred.reject(error);
            });

            return deferred.promise;
        }

      return service;
    }
})();
