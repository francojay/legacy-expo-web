(function(){
    'use strict';

    var directiveId = 'deleteConferenceUsersModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', deleteConferenceUsersModal]);

    function deleteConferenceUsersModal ($rootScope){

        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/_components/deleteConferenceUsersModal/deleteConferenceUsersView.html',
            link: link
        };

        function link(scope){

          scope.numberOfUsers = scope.ngDialogData.numberOfUsers;

          scope.deleteEvent = function() {
            $rootScope.$emit('DELETE_CONFERENCE_USERS');
          }

          scope.deleteAllEvent = function() {
            $rootScope.$emit('DELETE_ALL_CONFERENCE_USERS');
          }



        }

    }
})();
