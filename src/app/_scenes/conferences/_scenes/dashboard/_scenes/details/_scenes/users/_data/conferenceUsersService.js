(function(){
    'use strict';

    var serviceId = 'conferenceUsersService';

    angular
        .module('app')
        .service(serviceId, [
            '$q', 'requestFactory', conferenceUsersService
        ]);
    function conferenceUsersService($q, requestFactory){
      var service = {};

      service.inviteUser = function(invitedUser, conference_id) {

          var deferred = $q.defer();

          requestFactory.post(
              'api/user/create/',
              invitedUser,
              false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });

          return deferred.promise;
      }

      service.getAllUsers = function(conference_id){
        return requestFactory.get(
            '/api/user/list/' + "?conference_id=" + conference_id, {
            },
            false
        )
        .then(function(response){
            return  response.data;
        })
        .catch(function(error){
            console.log(error);
            return error;
        });
      };

      service.updateUserRights = function(conference_user_id, rights){
        var deferred = $q.defer();
          requestFactory.post(
              '/api/user/rights_update',
              {
                'conference_user_id': conference_user_id,
                'rights': rights
              },
              false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
      };

      service.removeMultipleUsers = function(conference_id, array_users_id){
          var deferred = $q.defer();
          var data = {
              conference_id: conference_id,
              conference_user_ids: array_users_id,
              remove_all:false
          }
          requestFactory.post(
              'api/user/multi_remove/',
              data,
              false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
      };

      service.removeAllUsers = function(conference_id){
          var deferred = $q.defer();
          var data = {
              conference_id: conference_id,
              conference_user_ids: [],
              remove_all:true
          }
          requestFactory.post(
              'api/user/multi_remove/',
              data,
              false
          )
          .then(function(response){
              deferred.resolve(response.data);
          })
          .catch(function(error){
              deferred.reject(error);
          });
          return deferred.promise;
      };

      return service;
    }
})();
