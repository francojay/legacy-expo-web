(function(){
	'use strict';

    var controllerId = 'travelController';
		var app = angular.module('app');

		var dependenciesArray = 	[
															'$rootScope',
															'$scope',
															'$q',
															'SharedProperties',
															'travelService',
															'fileService',
															'toastr',
															'intlTelInputOptions',
															'API',
															'$timeout',
															travelController
														];
			app.controller(controllerId, dependenciesArray);

			function travelController($rootScope, $scope, $q, SharedProperties, travelService, fileService, toastr, intlTelInputOptions, API, $timeout) {
				$scope.conference = $rootScope.conference;
				$scope.conference_id = $rootScope.conference.conference_id;
				$scope.newTravelToLoad = true;

				$scope.united_states = require("unitedStatesList");
				$scope.all_countries = require("countriesList");

        $scope.travelSelected = 'hotel';
        $scope.travel_type_table_order = 'desc';
        $scope.travel_loader = true;
				$scope.travel = {
						'hotel' : [],
						'car_rental': [],
						'airline': [],
						'transportation': []
				};
				SharedProperties.travelItems = $scope.travel;
  			$scope.conferenceDays = [];
				$scope.defaultCountryFlag = 'us';
        angular.forEach(API.COUNTRIES, function(country) {
          if(country.name === $rootScope.conference.country){
            $scope.defaultCountryFlag = country.code.toLowerCase();
          }
        });
				$scope.plus_travel = false;
				$scope.edit_travel_item = false;
        function initiateCustomFieldsData() {

            $scope.initAttendeeFields = presets.initAttendeeFields();
            $scope.removableAttendeeFields = presets.removableAttendeeFields();
            $scope.editAttendeeFields = presets.editAttendeeFields();
            $scope.small_form_for_travel = presets.smallFormForTravel();
            $scope.large_form_for_travel = presets.largeFormForTravel();
            $scope.attendeeFields = [
                {
                    id: -1,
                    code: 'full_name',
                    name: 'Attendee'
                }
            ].concat($scope.removableAttendeeFields);

            $scope.memberMappingFields = presets.memberMappingFields();
            $scope.attendeeMappingFields = presets.attendeeMappingFields();
            $scope.attendeeMappingFields2 = presets.attendeeMappingFields2();
            $scope.attendeeMappingFields = $scope.attendeeMappingFields2.concat($scope.removableAttendeeFields);
            $scope.exhibitorMappingFields = presets.exhibitorMappingFields();
            $scope.tempAttendeeFields = presets.tempAttendeeFields();
            $scope.sessionMappingFields = presets.sessionInputFields();
        }

  			initiateCustomFieldsData();
              $scope.conferenceStreetAddress = '';
              $scope.hasAttendeeId = false;
              $scope.edit_travel_item = false;
              $scope.plus_travel = false;
              $scope.defaultHotels = presets.defaultHotels();
              $scope.defaultCarRentals = presets.defaultCarRentals();
              $scope.defaultAirlines = presets.defaultAirlines();
              $scope.defaultTransportations = presets.defaultTransportations();

              $scope.getDefaultTravelLogo = function(option){
                  var s3_link = 'https://s3.amazonaws.com/expo-static/resources/travel/', index;
                  if($scope.travelSelected == 'hotel'){
                      s3_link = s3_link + 'hotel_logos/';
                      index = $scope.defaultHotels.indexOf(option) + 1;
                      $scope.newTravelType.logo = s3_link + 'logo_hotel' + index + '.png';
                  } else if($scope.travelSelected == 'car_rental'){
                      s3_link = s3_link + 'car_rental_logos/';
                      index = $scope.defaultCarRentals.indexOf(option) + 1;
                      $scope.newTravelType.logo = s3_link + 'logo_car' + index + '.png';
                  } else if($scope.travelSelected == 'airline'){
                      s3_link = s3_link + 'airline_logos/';
                      index = $scope.defaultAirlines.indexOf(option) + 1;
                      $scope.newTravelType.logo = s3_link + 'logo_airline' + index + '.png';
                  } else if($scope.travelSelected == 'transportation'){
                      s3_link = s3_link + 'transportation_logos/';
                      index = $scope.defaultTransportations.indexOf(option) + 1;
                      $scope.newTravelType.logo = s3_link + 'logo_transportation' + index + '.png';
                  }
              };
							$scope.cancelButton = function(){
								$scope.plus_travel = false;
								$scope.edit_travel_item = false;
							}
              $scope.sortTravelType = function(){
                  $scope.travel[$scope.travelSelected] = _.orderBy($scope.travel[$scope.travelSelected], 'name', $scope.travel_type_table_order);
                  if($scope.travel_type_table_order == 'asc'){
                      $scope.travel_type_table_order = 'desc';
                  } else{
                      $scope.travel_type_table_order = 'asc';
                  }
              }

              $scope.addTravelType = function(){
                  $scope.newTravelType = presets.newTravelType();
                  $scope.newTravelType.country = $scope.all_countries[0].name;
                  $scope.newTravelType.state = $scope.united_states[0].abbreviation;
                  $scope.count_country = 0;
                  $scope.count_state = 0;
                  $scope.newTravelType.label = $scope.travelSelected;
                  $scope.plus_travel = true;
                  if($scope.travelSelected == 'hotel'){
                      $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                  } else if($scope.travelSelected == 'car_rental'){
                      $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                  } else if($scope.travelSelected == 'airline'){
                      $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                  } else {
                      $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                  }
              };

              $scope.$watch(
                  function(){
                      return $scope.travelSelected;
                  },
                  function(travel_type){
                      if(travel_type == 'hotel' || travel_type == 'car_rental'){
                          $scope.newTravelType = {
                              'name': '',
                              'city': '',
                              'state': $scope.united_states[0].abbreviation,
                              'street_address': '',
                              'website': '',
                              'special_instructions': '',
                              'logo': '',
                              'image': '',
                              'country': $scope.all_countries[0].name,
                              'group_code': '',
                              'phone_number': '',
                              'price_range_start': '',
                              'price_range_end': '',
                              'zip_code': ''
                          };
                      } else{
                          $scope.newTravelType = {
                              'name': '',
                              'logo': '',
                              'group_code': '',
                              'discount': '',
                              'website': '',
                              'image': ''
                          };
                      }
                      if(travel_type == 'hotel'){
                          $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                      } else if(travel_type == 'car_rental'){
                          $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                      } else if(travel_type == 'airline'){
                          $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                      } else {
                          $scope.newTravelType.image = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                      }
                  }
              );

              $scope.displayTextForUser = function(){
                  if($scope.travelSelected.indexOf('_') < 0){
                      return $scope.travelSelected[0].toUpperCase() + $scope.travelSelected.slice(1, $scope.travelSelected.length);
                  } else{
                      var words = $scope.travelSelected.split('_');
                      return words[0][0].toUpperCase() + words[0].slice(1, words[0].length) + ' ' + words[1][0].toUpperCase() + words[1].slice(1, words[1].length);
                  }
              };



              $scope.editThisTravelItem = function(travel_item){
                  $scope.count_country = -1;
                  _.each($scope.all_countries, function(country){
                      $scope.count_country++;
                      if(_.isString(travel_item.country) && country.name.trim() == travel_item.country.trim()){
                          return false;
                      }
                  });
                  $scope.count_state = 0;
                  if(travel_item.hasOwnProperty('country') && travel_item.country.trim() == 'United States'){
                      _.each($scope.united_states, function(state){
                          if(state.abbreviation != travel_item.state){
                              $scope.count_state++;
                          } else{
                              return false;
                          }
                      });
                  }
                  $scope.newTravelType = _.cloneDeep(travel_item);
                  $scope.plus_travel = true;
                  $scope.edit_travel_item = true;
              };

              $scope.travelCountryUpdate = function(modelItem) {
								var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
		            var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
		            number = number.replace("+"+countryData.dialCode,"");
								if (modelItem.country) {
									angular.forEach(API.COUNTRIES, function(country) {
					          if(country.name === modelItem.country){
					            $rootScope.phoneIntlTelInputCtrl.setCountry(country.code.toLowerCase());
					          }
					        });
			            countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
									if(number){
										$timeout(function() {
											modelItem.phone_number = "+"+countryData.dialCode+number;
										}, 100);
									}

			          }
                  if (modelItem.country.hasOwnProperty('name') ? modelItem.country.name.trim() == 'United States' : modelItem.country.trim() == 'United States') {
                      $scope.count_state = 0;
                      modelItem.state = $scope.united_states[0].abbreviation;
                  } else{
                  }

              }

              $scope.selectTravelTypeToAdd = function(flag_for_another){
                  if($scope.travelSelected == 'hotel'){
                      $scope.addHotel(flag_for_another);
                  } else if($scope.travelSelected == 'car_rental'){
                      $scope.addCarRental(flag_for_another);
                  } else if($scope.travelSelected == 'airline'){
                      $scope.addAirline(flag_for_another);
                  } else if($scope.travelSelected == 'transportation'){
                      $scope.addTransportation(flag_for_another);
                  }
                  return;
              };

							// Upload a travel type logo
              // Get the calculated signature version 4 url from the backend
              // Upload the image to the s3 bucket using that presigned url
              // store the image url in the payload that will be sent to the backend for creating the travel type
              $scope.uploadTravelTypeLogo = function(files) {
                  lib.squareifyImage($q, files)
                      .then(function(response){
                          var imgGile = response.file;

                          fileService.getUploadSignature(imgGile)
                              .then(function(response){
                                  var file_link = response.data.file_link;
                                  fileService.uploadFile(imgGile, response)
                                      .then(function(uploadResponse){
                                          $scope.newTravelType.logo = file_link;
                                      })
                                      .catch(function(error){

                                      });
                              })
                              .catch(function(error){

                                  if (error.status == 403) {
                                      toastr.error('Permission Denied!');
                                  } else {
                                      toastr.error('Error!', 'There has been a problem with your request.');
                                  }
                              });
                      });

              };

              // Upload a travel type image
              // Get the calculated signature version 4 url from the backend
              // Upload the image to the s3 bucket using that presigned url
              // store the image url in the payload that will be sent to the backend for creating the travel type
              $scope.uploadTravelTypeImage = function(files) {
                  fileService.getUploadSignature(files[0])
                      .then(function(response){
                          var file_link = response.data.file_link;
                          fileService.uploadFile(files[0], response)
                              .then(function(uploadResponse){
                                  $scope.newTravelType.image = file_link;
                              })
                              .catch(function(error){

                              });
                      })
                      .catch(function(error){

                          if (error.status == 403) {
                              toastr.error('Permission Denied!');
                          } else {
                              toastr.error('Error!', 'There has been a problem with your request.');
                          }
                      });
              };

              // Add hotel option
              // Validates if the payload contains the required fields
              // TODO better error handling
              $scope.lockTravelButtons = false;
              $scope.addHotel = function(flag_for_another){
                  var ok = true;
                  _.each($scope.newTravelType, function(val, key){
                      if((key == 'price_range_start' || key == 'price_range_end') && val == ''){
                          $scope.newTravelType[key] = 0;
                      } else if((key == 'price_range_start' ||  key == 'price_range_end') && val != ''){
                          $scope.newTravelType[key] = Number($scope.newTravelType[key]);
                      } else if(key == 'image' && val == ''){
                          $scope.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png';
                      }
                  });

                  if(_.isEmpty($scope.newTravelType.name)){
                      toastr.error('The name is a required field!');
                      ok = false;
                      return;
                  }

                  if($scope.newTravelType.phone_number == ""){
                      toastr.error('The phone number is a required field!');
                      ok = false;
                      return;
                  }
									if($scope.newTravelType.phone_number == undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1){
										toastr.error('Please enter a valid phone number !');
										ok = false;
										return;
									}

                  if(_.isEmpty($scope.newTravelType.street_address)){
                      toastr.error('The street address is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.city)){
                      toastr.error('The city is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.zip_code)){
                      toastr.error('The zip code is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.country)){
                      toastr.error('The country is a required field!');
                      ok = false;
                      return;
                  }

                  if($scope.newTravelType.country.trim() != 'United States'){
                      $scope.newTravelType.state = '';
                  } else if($scope.newTravelType.state == '' || $scope.newTravelType.state == '--'){
                      toastr.error('The state is a required field!');
                      ok = false;
                      return;
                  }

                  if(Number($scope.newTravelType.price_range_start) > Number($scope.newTravelType.price_range_end)){
                      toastr.error('"Start Price" must be lower then "End Price"!');
                      ok = false;
                      return;
                  }

									if(Number($scope.newTravelType.price_range_start) < 0 || Number($scope.newTravelType.price_range_end) < 0){
                      toastr.error('Price can\'t be negative!');
                      ok = false;
                      return;
                  }

                  if(ok && $scope.newTravelType.id == undefined && !$scope.lockTravelButtons){
                      $scope.lockTravelButtons = true;
                      travelService
                          .createHotel($scope.conference_id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'city': '',
                                  'state': $scope.united_states[0].abbreviation,
                                  'street_address': '',
                                  'website': '',
                                  'special_instructions': '',
                                  'logo': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png',
                                  'country': $scope.all_countries[0].name,
                                  'group_code': '',
                                  'phone_number': '',
                                  'price_range_start': '',
                                  'price_range_end': '',
                                  'zip_code': ''
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                              $scope.lockTravelButtons = false;
                          })
                          .catch(function(error){
                              $scope.lockTravelButtons = false;
                              lib.processValidationError(error, toastr);
                          });
                  }  else if(ok && $scope.newTravelType.id != undefined && !$scope.lockTravelButtons){
                      travelService
                          .updateHotel($scope.newTravelType.id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'city': '',
                                  'state': $scope.united_states[0].abbreviation,
                                  'street_address': '',
                                  'website': '',
                                  'special_instructions': '',
                                  'logo': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/f49b77899e07f1b4b306bead6a1c8db3.png',
                                  'country': $scope.all_countries[0].name,
                                  'group_code': '',
                                  'phone_number': '',
                                  'price_range_start': '',
                                  'price_range_end': '',
                                  'zip_code': ''
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                          });
                  }
              };

              // Add car rental option
              // Validates if the payload contains the required fields
              // TODO better error handling
              $scope.addCarRental = function(flag_for_another){
                  var ok = true;
                  _.each($scope.newTravelType, function(val, key){
                      if((key == 'price_range_start' ||  key == 'price_range_end') && val == ''){
                          $scope.newTravelType[key] = 0;
                      } else if((key == 'price_range_start' ||  key == 'price_range_end') && val != ''){
                          $scope.newTravelType[key] = Number($scope.newTravelType[key]);
                      } else if(key == 'image' && val == ''){
                          $scope.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png';
                      }
                  });

                  if(_.isEmpty($scope.newTravelType.name)){
                      toastr.error('The name is a required field!');
                      ok = false;
                      return;
                  }

                  if($scope.newTravelType.phone_number == ""){
                      toastr.error('The phone number is a required field!');
                      ok = false;
                      return;
                  }

									if($scope.newTravelType.phone_number == undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1){
										toastr.error('Please enter a valid phone number !');
										ok = false;
										return;
									}

                  if(_.isEmpty($scope.newTravelType.street_address)){
                      toastr.error('The street address is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.city)){
                      toastr.error('The city is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.zip_code)){
                      toastr.error('The zip code is a required field!');
                      ok = false;
                      return;
                  }

                  if(_.isEmpty($scope.newTravelType.country)){
                      toastr.error('The country is a required field!');
                      ok = false;
                      return;
                  }

                  if(Number($scope.newTravelType.price_range_start) > Number($scope.newTravelType.price_range_end)){
                      toastr.error('"Start Price" must be lower then "End Price"!');
                      ok = false;
                  }

									if(Number($scope.newTravelType.price_range_start) < 0 || Number($scope.newTravelType.price_range_end) < 0){
                      toastr.error('Price can\'t be negative!');
                      ok = false;
                      return;
                  }

                  if($scope.newTravelType.country.trim() != 'United States'){
                      $scope.newTravelType.state = '';
                  } else if($scope.newTravelType.state == '' || $scope.newTravelType.state == '--'){
                      toastr.error('The state is a required field!');
                      ok = false;
                  }

                  if(ok && $scope.newTravelType.id == undefined && !$scope.lockTravelButtons){
                      $scope.lockTravelButtons = true;
                      travelService
                          .createCarRental($scope.conference_id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'city': '',
                                  'state': $scope.united_states[0].abbreviation,
                                  'street_address': '',
                                  'website': '',
                                  'special_instructions': '',
                                  'logo': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png',
                                  'country': $scope.all_countries[0].name,
                                  'group_code': '',
                                  'phone_number': '',
                                  'price_range_start': '',
                                  'price_range_end': '',
                                  'zip_code': ''
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                              $scope.lockTravelButtons = false;
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                              $scope.lockTravelButtons = false;
                          });
                  } else if(ok && $scope.newTravelType.id != undefined && !$scope.lockTravelButtons){
                      travelService
                          .updateCarRental($scope.newTravelType.id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'city': '',
                                  'state': $scope.united_states[0].abbreviation,
                                  'street_address': '',
                                  'website': '',
                                  'special_instructions': '',
                                  'logo': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/9d86dc6b04759c0624cc7c33bbbfee9a.png',
                                  'country': $scope.all_countries[0].name,
                                  'group_code': '',
                                  'phone_number': '',
                                  'price_range_start': '',
                                  'price_range_end': '',
                                  'zip_code': ''
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                          });
                  }
              };

              // Add airline option
              // Validates if the payload contains the name (required field)
              // TODO better error validation
              $scope.addAirline = function(flag_for_another){
                  var ok = true;
                  _.each($scope.newTravelType, function(val, key){
                      if(key == 'image' && val == ''){
                          $scope.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png';
                      }
                  });

                  if(_.isEmpty($scope.newTravelType.name)){
                      toastr.error('The name is a required field!');
                      ok = false;
                      return;
                  }
									if(!_.isEmpty($scope.newTravelType.discount)){
										if(!Number($scope.newTravelType.discount)){
	                      toastr.error('Please, complete the discount field with a number!');
	                      ok = false;
	                      return;
	                  }
										else if($scope.newTravelType.discount < 0){
											toastr.error('Please, complete the discount field with a number higher or equal with 0!');
											ok = false;
											return;
										}
									}
                  if(ok && $scope.newTravelType.id == undefined && !$scope.lockTravelButtons){
                      $scope.lockTravelButtons = true;
                      travelService
                          .createAirline($scope.conference_id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'logo': '',
                                  'group_code': '',
                                  'discount': '',
                                  'website': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png'
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                              $scope.lockTravelButtons = false;
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                              $scope.lockTravelButtons = false;
                          });
                  }  else if(ok && $scope.newTravelType.id != undefined && !$scope.lockTravelButtons){
                      travelService
                          .updateAirline($scope.newTravelType.id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'logo': '',
                                  'group_code': '',
                                  'discount': '',
                                  'website': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/821436dda96e726b83d91e203c372d41.png'
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                          });
                  }
              };

              // Add transportation option
              // Validates if the payload contains the name (required field)
              // TODO better error validation
              $scope.addTransportation = function(flag_for_another){
                  var ok = true;
                  _.each($scope.newTravelType, function(val, key){
                      if(key == 'image' && val == ''){
                          $scope.newTravelType[key] = 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png';
                      }
                  });

                  if(_.isEmpty($scope.newTravelType.name)){
                      toastr.error('The name is a required field!');
                      ok = false;
                      return;
                  }
									if(!_.isEmpty($scope.newTravelType.discount)){
										if(!Number($scope.newTravelType.discount)){
	                      toastr.error('Please, complete the discount field with a number!');
	                      ok = false;
	                      return;
	                  }
										else if($scope.newTravelType.discount < 0){
											toastr.error('Please, complete the discount field with a number higher or equal with 0!');
											ok = false;
											return;
										}
									}
                  if(ok && $scope.newTravelType.id == undefined && !$scope.lockTravelButtons){
                      $scope.lockTravelButtons = true;
                      travelService
                          .createTransportation($scope.conference_id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'logo': '',
                                  'group_code': '',
                                  'discount': '',
                                  'website': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png'
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                              $scope.lockTravelButtons = false;
                          })
                          .catch(function(error){
                              $scope.lockTravelButtons = false;
                              lib.processValidationError(error, toastr);
                          });
                  }  else if(ok && $scope.newTravelType.id != undefined && !$scope.lockTravelButtons){
                      travelService
                          .updateTransportation($scope.newTravelType.id, $scope.newTravelType)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.travel[$scope.travelSelected].push(response);
                              if(flag_for_another == true){
                                  $scope.plus_travel = true;
                              } else{
                                  $scope.plus_travel = false;
                              }
                              $scope.newTravelType = {
                                  'name': '',
                                  'logo': '',
                                  'group_code': '',
                                  'discount': '',
                                  'website': '',
                                  'image': 'https://s3.amazonaws.com/expo-images/94504d4aca2ecdfd8bfb58c13bd20f7a.png'
                              };
                              $scope.edit_travel_item = false;
                              $('#travelPage').scrollTop(0);
                          })
                          .catch(function(error){
                              lib.processValidationError(error, toastr);
                          });
                  }
              };

							// Removes a travel & lodging option
              // After the backend response it removes the option from the frontend app view
              // TODO -- test functionality of removing from the frond app view before the backend response for smoother experience
              $scope.deleteTravelItem = function(){
                  if($scope.travelSelected == 'hotel'){
                      travelService
                          .deleteHotel($scope.newTravelType.id)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.newTravelType = {};
                              $scope.plus_travel = false;
                              $scope.edit_travel_item = false;
                          })
                          .catch(function(error){

                          });
                  } else if($scope.travelSelected == 'car_rental'){
                      travelService
                          .deleteCarRental($scope.newTravelType.id)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.newTravelType = {};
                              $scope.plus_travel = false;
                              $scope.edit_travel_item = false;
                          })
                          .catch(function(error){

                          });
                  } else if($scope.travelSelected == 'airline'){
                      travelService
                          .deleteAirline($scope.newTravelType.id)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.newTravelType = {};
                              $scope.plus_travel = false;
                              $scope.edit_travel_item = false;
                          })
                          .catch(function(error){

                          });
                  } else if($scope.travelSelected == 'transportation'){
                      travelService
                          .deleteTransportation($scope.newTravelType.id)
                          .then(function(response){
                              $scope.travel[$scope.travelSelected] = _.reject($scope.travel[$scope.travelSelected], ['id', $scope.newTravelType.id]);
                              $scope.newTravelType = {};
                              $scope.plus_travel = false;
                              $scope.edit_travel_item = false;
                          })
                          .catch(function(error){

                          });
                  }
              };

              // Get travel and lodging data from the backend
              // Stores the data in the travel parameter to be used in the travel and lodging screens
              $scope.requestOnRolling = false;
              $scope.getTravelItems = function(){
                  if($scope.newTravelToLoad == true){

                      SharedProperties.travelItems = {};
                      $scope.requestOnRolling = true;
                      $scope.newTravelToLoad = false;

                      $q.all([
                          travelService
                              .getHotels($scope.conference_id)
                              .then(function(response){
                                  $scope.travel['hotel'] = _.orderBy(response, 'name', 'asc');
                                  SharedProperties.travelItems['hotel'] = $scope.travel['hotel'];
                              })
                              .catch(function(error){
                              }),
                          travelService
                              .getCarRentals($scope.conference_id)
                              .then(function(response){
                                  $scope.travel['car_rental'] = _.orderBy(response, 'name', 'asc');
                                  SharedProperties.travelItems['car_rental'] = $scope.travel['car_rental'];
                              })
                              .catch(function(error){
                              }),
                          travelService
                              .getAirlines($scope.conference_id)
                              .then(function(response){
                                  $scope.travel['airline'] = _.orderBy(response, 'name', 'asc');
                                  SharedProperties.travelItems['airline'] = $scope.travel['airline'];
                              })
                              .catch(function(error){
                              }),
                          travelService
                              .getTransportations($scope.conference_id)
                              .then(function(response){
                                  $scope.travel['transportation'] = _.orderBy(response, 'name', 'asc');
                                  SharedProperties.travelItems['transportation'] = $scope.travel['transportation'];
                              })
                              .catch(function(error){
                              })
                      ])
                      .then(function(){
                          $scope.requestOnRolling = false;
                      });

                  } else{
                      $scope.travel = SharedProperties.travelItems;
                  }
              };

              $scope.$watch(function(){
                  return $scope.newTravelType.name;
              }, function(changed_name){
                  if(changed_name == ''){
                      $scope.newTravelType.logo = '';
                  }
              });
							//this will be used on refactor phase
							function initHotelOrCarRentalFields(type){
								return   [
				          {
				            nameToDisplay : type+" Name",
				            key           : "name",
				            value         : "",
				            defaultValue  : "",
				            mandatory     : true,
				            visible       : true,
				          },
				          {
				            nameToDisplay : type+" Logo",
				            key           : "logo",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				            mandatory      : false
				          },
				          {
				            nameToDisplay : "Group Code",
				            key           : "group_code",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				          },
				          {
										nameToDisplay : "Phone Number",
				            key           : "phone_number",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
										mandatory     : true
				          },
				          {
				            nameToDisplay : "Street Address",
				            key           : "street_address",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				            mandatory     : true
				          },
				          {
				            nameToDisplay : "City",
				            key           : "city",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
										mandatory     : true
				          },
				          {
				            nameToDisplay : "Zip Code",
				            key           : "zip_code",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
										mandatory     : true
				          },
				          {
				            nameToDisplay : "State",
				            key           : "state",
				            value         : "",
				            defaultValue  : "",
				            states        : API.UNITED_STATES,
				            visible       : true,
				          },
				          {
				            nameToDisplay : "Country",
				            key           : "country",
				            value         : "",
				            defaultValue  : "",
				            countries     : API.COUNTRIES,
				            visible       : true,
				          },
				          {
				            nameToDisplay : "Website",
				            key           : "website",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				            mandatory     : false
				          },
									{
				            nameToDisplay : "Price range",
				            key           : "price_range",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				            mandatory     : true
				          },
									{
				            nameToDisplay : "Special Instructions",
				            key           : "special_instructions",
				            value         : "",
				            defaultValue  : "",
				            visible       : true,
				            mandatory     : true
				          }
				        ];
							}

      }

})();
