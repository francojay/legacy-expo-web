(function(){
	'use strict';

    var serviceId = 'socialTagsService';

	angular
		.module('app')
		.service(serviceId, [
            '$q', 'requestFactory', socialTagsService
		]);
		function socialTagsService($q, requestFactory){
		    var service = {};

				service.updateSocialTags = function(conference_id, socialTags){
					var deferred = $q.defer();
					requestFactory.post(
							'api/v1.1/social_tags/' + conference_id + '/',
							socialTags,
							false
					)
					.then(function(response){
							 deferred.resolve(response.data);
					})
					.catch(function(error){
							 deferred.reject(error);
					});

					return deferred.promise;
				}

				service.getSocialTags = function(conference_id){
					var deferred = $q.defer();
					requestFactory.get(
							'api/v1.1/social_tags/' + conference_id + '/',
							{},
							false
					)
					.then(function(response){
							 deferred.resolve(response.data);
					})
					.catch(function(error){
							 deferred.reject(error);
					});

					return deferred.promise;
				}

      return service;
    }
})();
