(function () {

	'use strict';

	var controllerId 			= 'coverScreenController';
	var app 				 			= angular.module("app");
	var dependenciesArray = [
		'$scope',
		'$rootScope',
		'$state',
		'$stateParams',
		'$q',
		'toastr',
		'ngDialog',
		'SharedProperties',
		'conferenceService',
		'fileService',
		coverScreenController
	];

	app.controller(controllerId, dependenciesArray);

	function coverScreenController($scope, $rootScope, $state, $stateParams, $q, toastr, ngDialog, SharedProperties, conferenceService, fileService) {

		$scope.conference = $rootScope.conference;
		$scope.conference_id = $rootScope.conference.conference_id;
		$scope.coverScreen = $rootScope.conference.cover_screen;
		$scope.encodedConferenceId = $stateParams.conference_id;
		$scope.defaultBackgroundImagesFromAppSolution = [
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_0.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_1.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_2.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_3.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_4.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_5.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_6.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_7.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_8.png'
				},
				{
						'background_url': 'assets/images/cover-screen-default/conference_background_9.png'
				}
		];

		SharedProperties.coverScreenOptions = $scope.coverScreen || {};
		SharedProperties.coverScreensImagesOptions = ($scope.coverScreen.background_url == null ?
			$scope.defaultBackgroundImagesFromAppSolution : [{'background_url': $scope.coverScreen.background_url}]);
		SharedProperties.coverScreenOptions.defaultImageIndex = ($scope.coverScreen.default_image_id == null ? 1 : $scope.coverScreen.default_image_id);
		SharedProperties.coverScreenOptions.coverScreenOverlay = $scope.coverScreen.overlay_type == 'dark' ? false : ($scope.coverScreen.overlay_type == 'light');
		SharedProperties.coverScreenOptions.displayOverlay = !($scope.coverScreen.overlay_type == 'off');
		SharedProperties.coverScreenOptions.coverScreenTextColor = $scope.coverScreen.text_color_type == 'dark' ? false : ($scope.coverScreen.text_color_type == 'light');
		SharedProperties.coverScreenOptions.temporaryLogoUrl = $scope.conference.cover_screen_logo ? $scope.conference.cover_screen_logo : ( $scope.conference.logo ? $scope.conference.logo : null);
		$scope.coverScreenOverlay = SharedProperties.coverScreenOptions.coverScreenOverlay;
		$scope.displayOverlay = SharedProperties.coverScreenOptions.displayOverlay;
		$scope.coverScreenTextColor = SharedProperties.coverScreenOptions.coverScreenTextColor;
		$scope.defaultImageIndex = SharedProperties.coverScreenOptions.defaultImageIndex;
		$scope.coverScreensImagesOptions =  SharedProperties.coverScreensImagesOptions;
		$scope.temporaryBackgroundUrlExists = ($scope.coverScreen.background_url != null);
		SharedProperties.coverScreenOptions.removeSlider = false;
		$scope.temporaryLogoUrlExists = SharedProperties.coverScreenOptions.temporaryLogoUrl != $scope.conference.logo;

		$scope.setConferenceCoverScreen = function() {
				$scope.disableSaveCoverScreenButton = true;
				var cover_screen = {};
				var uploadedConferenceLogoFromCoverScreen = '';
				var stringOverlayMode = mapOverlayToString($scope.coverScreenOverlay);
				var textColorMode = mapOverlayToString($scope.coverScreenTextColor);
				var logo = SharedProperties.coverScreenOptions.logo;
				var background = SharedProperties.coverScreenOptions.background;
				var promises = [];

				if (logo) {
						promises.push(uploadCoverScreenResources(logo));
						var logoUploaded = true;
				}
				if (background != undefined) {
						promises.push(uploadCoverScreenResources(background));
						var backgroundUploaded = true;
				}

				$q.all(promises).then(function(response){
						if (response.length == 0) {
								if ($scope.coverScreen.background_url) {
										cover_screen.background_url = $scope.coverScreen.background_url;
										cover_screen.default_image_id = null;
										$rootScope.conference.cover_screen.background_url = $scope.coverScreen.background_url;
								} else {
										cover_screen.background_url = null;
										cover_screen.default_image_id = SharedProperties.coverScreenOptions.defaultImageIndex;
								}
								uploadedConferenceLogoFromCoverScreen = SharedProperties.coverScreenOptions.temporaryLogoUrl;

						}
						else if (response.length == 1){
								if(logoUploaded){
										uploadedConferenceLogoFromCoverScreen = response[0];
								} else if(backgroundUploaded){
										cover_screen.background_url = response[0];
								}
						} else if (response.length == 2){
								uploadedConferenceLogoFromCoverScreen = response[0];
								cover_screen.background_url = response[1];
						}

						cover_screen.overlay_type = !$scope.displayOverlay ? 'off' : stringOverlayMode;
						cover_screen.text_color_type = textColorMode;
						if (!cover_screen.background_url) {
								cover_screen.default_image_id = SharedProperties.coverScreenOptions.defaultImageIndex;
						}

						conferenceService
								.updateConference({
										'cover_screen': cover_screen,
										'cover_screen_logo': uploadedConferenceLogoFromCoverScreen,
										'conference_id': $scope.conference_id
									})
								.then(function(response){
										toastr.success('Your Event Cover Screen was successfully updated!');
										$rootScope.conference.cover_screen_logo = response.cover_screen_logo;
										$rootScope.conference.cover_screen = response.cover_screen;
										$scope.disableSaveCoverScreenButton = false;
								}, function(error){
										toastr.error('There was a problem with your upload!');
										$scope.disableSaveCoverScreenButton = false;
								});

				}, function(error) {
						$scope.disableSaveCoverScreenButton = false;
						toastr.error("Error on uploading images! Please try another to upload other images.")
						console.error(error);
				})
		};

		$scope.uploadLogo = function(files, invalidFiles) {
				if (files.length > 0) {
						SharedProperties.coverScreenOptions.temporaryLogoUrl = files[0].$ngfBlobUrl;
						SharedProperties.coverScreenOptions.logo = files[0];
						$scope.temporaryLogoUrlExists = true;
				} else if (!_.isEmpty(invalidFiles)) {
          if (invalidFiles[0].$error === 'maxSize') {
            toastr.error('Image is too big to upload! Upload an image smaller than 1MB.');
          } else if (invalidFiles[0].$error === 'maxHeight' || invalidFiles[0].$error === 'maxWidth') {
            toastr.error('Try to upload an image with smaller dimensions than 1900 x 430 pixels!', 'Your image is too large!');
          }
        }
		};

		$scope.uploadBackground = function(files, invalidFiles) {
			console.log(files,invalidFiles)
				if (files.length > 0) {
						SharedProperties.coverScreenOptions.temporaryBackgroundUrl = files[0].$ngfBlobUrl;
						SharedProperties.coverScreenOptions.background = files[0];
						SharedProperties.coverScreenOptions.removeSlider = true;

						$scope.defaultImageIndex = 1;
						$scope.temporaryBackgroundUrlExists = true;
				} else if (!_.isEmpty(invalidFiles)) {
          if (invalidFiles[0].$error === 'maxSize') {
            toastr.error('Image is too big to upload! Upload an image smaller than 2MB.');
          } else if (invalidFiles[0].$error === 'maxHeight' || invalidFiles[0].$error === 'maxWidth') {
            toastr.error('Your image is too large!');
          }
        }
		};

		$scope.revertTemporaryLogo = function() {
				delete SharedProperties.coverScreenOptions.temporaryLogoUrl;
				SharedProperties.coverScreenOptions.logo = null;

				SharedProperties.coverScreenOptions.temporaryLogoUrl = $scope.conference.logo;
				$scope.temporaryLogoUrlExists = false;
		};

		$scope.revertTemporaryBackground = function() {
				delete SharedProperties.coverScreenOptions.temporaryBackgroundUrl;
				//delete SharedProperties.coverScreenOptions.background_url;

				SharedProperties.coverScreenOptions.removeSlider = false;
				SharedProperties.coverScreensImagesOptions = $scope.defaultBackgroundImagesFromAppSolution;
				SharedProperties.coverScreenOptions.background = null;
				$scope.coverScreen.background_url = null;
				$scope.temporaryBackgroundUrlExists = false;
		};


		function uploadCoverScreenResources(image) {
				var deferred = $q.defer();

				if (image) {
						fileService.getUploadSignature(image)
								.then(function(response){
										var file_link = response.data.file_link;
										fileService.uploadFile(image, response)
												.then(function(uploadResponse) {
														deferred.resolve(file_link)
												}, function(error) {
														deferred.reject(error);
												})
						}, function(error){
								if (error.status == 403) {
										toastr.error('Permission Denied!');
								} else {
										toastr.error('Error!', 'There has been a problem with your request.');
								}

								deferred.reject(error);
						});
				} else {
						deferred.reject('No image found');
				}

				return deferred.promise;
		}


		function mapOverlayToString(booleanMode) {
				if (booleanMode == true) {
						return 'light';
				} else {
						return 'dark';
				}
		}

		$scope.$watch(function() { // because ng-change isn't fired on md-switch
				//console.log($scope.coverScreenOverlay);
				$rootScope.$broadcast('overlayModeChanged', {overlayMode: $scope.coverScreenOverlay});
				SharedProperties.coverScreenOptions.coverScreenOverlay = $scope.coverScreenOverlay;
		});
		$scope.changeCoverOverlay = function(coverOverlay) {
				$scope.coverScreenOverlay = coverOverlay;
				$rootScope.$broadcast('overlayModeChanged', {overlayMode: coverOverlay});
				SharedProperties.coverScreenOptions.coverScreenOverlay = $scope.coverScreenOverlay;
		};
		$scope.changeDisplayCoverOverlay = function(coverOverlay) {
				$scope.displayOverlay = !$scope.displayOverlay;
				$rootScope.$broadcast('overlayModeChanged', {overlayMode: coverOverlay});
		};
	}
})();
