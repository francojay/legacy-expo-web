(function(){
	'use strict';

    var controllerId = 'conferenceSocialTagsController';

		angular
			.module('app')
			.controller(controllerId, ['$rootScope', 'socialTagsService', 'SharedProperties', 'toastr', conferenceSocialTagsController]);

			function conferenceSocialTagsController($rootScope, socialTagsService, SharedProperties, toastr) {
				var vm = this;
				vm.conference = $rootScope.conference;
				vm.conference_id = vm.conference.conference_id;
				vm.social_tags = { 'social_tag' : ''};

        vm.getConferenceSocialTags = function(){
        		if (SharedProperties.social_tags
                && SharedProperties.social_tags_conference == vm.conference_id) {
                		vm.social_tags['social_tag'] = SharedProperties.social_tags['social_tag'];
            } else {
                socialTagsService
                		.getSocialTags(vm.conference_id)
                    .then(function(response){
                        vm.social_tags['social_tag'] = response.facebook_tags;
                        SharedProperties.social_tags = {};
                        SharedProperties.social_tags['social_tag'] = response.facebook_tags;
                        SharedProperties.social_tags_conference = vm.conference_id;
                    },function(error){
											console.log(error);
										});
            }
        };

				vm.updateConferenceSocialTags = function(){
	          var social_obj = {
	              'facebook_tags' : vm.social_tags['social_tag'],
	              'twitter_tags' : vm.social_tags['social_tag'],
	              'linkedin_tags' : vm.social_tags['social_tag'],
	              'instagram_tags' : vm.social_tags['social_tag'],
	              'social_tags': vm.social_tags['social_tag']
	          };
            socialTagsService
                .updateSocialTags(vm.conference_id, social_obj)
                .then(function(response){
                    vm.social_tags['social_tag'] = response.facebook_tags;
                    SharedProperties.social_tags = vm.social_tags;
										toastr.success("Your social tag has been updated!");
                },function(error){
									toastr.error("Social tag could not be updated!");
								});
        };

			}
})();
