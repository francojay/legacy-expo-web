(function () {
    'use strict';

    var controllerId      = 'conferenceDetailsController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$scope',
      '$rootScope',
      '$state',
      '$q',
      'toastr',
      'API',
      'ngDialog',
      'SharedProperties',
      'conferenceService',
      'fileService',
      '$timeout',
      'urlService',
      'intlTelInputOptions',
      '$http',
      '$filter',
      'formatRawTimeService',
      conferenceDetailsController
    ];

    app.controller(controllerId, dependenciesArray);

    function conferenceDetailsController($scope, $rootScope, $state, $q, toastr, API, ngDialog, SharedProperties, conferenceService, fileService, $timeout, urlService, intlTelInputOptions, $http, $filter, formatRawTimeService) {
      $timeout(function() {
        $('.conference-details-overview').mCustomScrollbar({'theme': 'minimal-dark'});
      });
      var cloneConference = null;

      $scope.conference           = angular.copy($rootScope.conference);
      var momentDateFromWithTz     = moment($scope.conference.date_from).tz($scope.conference.timezone_name);
      var momentDateWithCorrection = momentDateFromWithTz;
      $scope.conference.date_from  = momentDateWithCorrection.format('M/D/Y');
      var momentDateToWithTz         = moment($scope.conference.date_to).tz($scope.conference.timezone_name);
      var momentDateToWithCorrection = momentDateToWithTz;
      $scope.conference.date_to      = momentDateToWithCorrection.format('M/D/Y');

      $scope.defaultTimezones      = $filter('orderBy')(API.TIMEZONES, "value");
      $scope.oldLogo               = angular.copy($scope.conference.logo);
      $scope.conference_id         = $rootScope.conference.conference_id;
      $scope.conferencePermissions = $rootScope.conference.permissions;
      $scope.conferenceOwner       = $rootScope.conference.user;
      $scope.googleMap             = 'https://www.google.com/maps/place/' + $scope.conference.country + ',' + $scope.conference.street + ',' + $scope.conference.city;
      $scope.googleStaticMap       = 'https://maps.googleapis.com/maps/api/staticmap?key=' + API.GOOGLE_API_KEY + '&size=582x256&sensor=false&zoom=15&markers=color%3Ared%7Clabel%3A.%7C' + $scope.conference.country + '%20' + $scope.conference.city + '%20' + $scope.conference.street;
      $scope.editMode              = false;
      $scope.united_states         = API.UNITED_STATES;
      $scope.currentUser           = JSON.parse(localStorage.currentUser);

      $scope.supportContacts       = $rootScope.conference.conference_support_contacts;
      $scope.countriesArray        = API.COUNTRIES;
      $scope.auxialiar_state       = $scope.conference.state;
      var savedFromDate            = null;
      $scope.defaultCountryFlag = 'us';
      if(!$scope.conference.phone_number) {
        angular.forEach(API.COUNTRIES, function(country) {
          if(country.name === $scope.conference.country){
            $scope.defaultCountryFlag = country.code.toLowerCase();
          }
        });
      }

      $scope.hasEditButton = function() {
          if(_.isEmpty($scope.conferencePermissions)) return false;
			    if (($scope.conferencePermissions.indexOf('edit_conference') <= -1) && ($scope.conferencePermissions.indexOf('admin') <= -1)) return false;
          else{
              return true;
          }
			};

      $scope.action = function(option){
          switch(option) {
              case 'details':
                  $scope.editMode = !$scope.editMode;
                  $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                      active: true,
                  });
                  if ($scope.editMode) {

                      $scope.conference.date_from_editable = momentDateWithCorrection;
                      $scope.conference.date_to_editable   = !savedFromDate?formatRawTimeService.createMomentFromRawTime($scope.conference.date_to_raw):savedFromDate;
                      //$scope.conference.date_to_editable = momentDateToWithCorrection;
                      cloneConference = _.clone($scope.conference);
                  }
                  else {
                      if (cloneConference) {
                          $scope.conference = _.cloneDeep(cloneConference);
                          $rootScope.$emit('Conference', $scope.conference);
                      }
                  }
                  break;
              default:

          }
      };

      $scope.sanitizeUrl = function(url) {
        return urlService.sanitizeUrl(url);
      };

      $scope.unSanitizeUrl = function(url) {
        return urlService.unSanitizeUrl(url);
      };

      function createMomentFromRawTime(rawDatetime) {
          if (!rawDatetime || !rawDatetime.year) {
              return null;
          }
          var date = moment(new Date(
              rawDatetime.year,
              rawDatetime.month - 1,
              rawDatetime.day,
              rawDatetime.hour,
              rawDatetime.minute
          )).utcOffset($scope.conference.time_zone);
          return date;
      }

      $scope.cancelEditConference = function() {
          $scope.editMode = false;
          $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
              active: false,
              logo: $scope.oldLogo,
              conferenceName: undefined
          });
          if (cloneConference) {
              $scope.conference = cloneConference;
              $rootScope.$emit('Conference', $scope.conference);
          }
      }

      $scope.changeCountry = function() {
          if ($scope.conference.country != $scope.countriesArray[0]) {
              $scope.conference.state = "";
          } else {
              $scope.conference.state = $scope.auxialiar_state;//$scope.united_states[0].abbreviation;
          }
          updateCountryForPhoneInput();
      }
      function updateCountryForPhoneInput() {
				if ($scope.conference.country !== undefined) {
          var countryCode = 'us';
          angular.forEach(API.COUNTRIES, function(country) {
            if(country.name === $scope.conference.country ){
              countryCode = country.code.toLowerCase();
            }
          });
					var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
					var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
					number = number.replace("+"+countryData.dialCode,"");
					$rootScope.phoneIntlTelInputCtrl.setCountry(countryCode);
					countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
          if(number){
            $timeout(function() {
              $scope.conference.venue_phone_number = "+"+countryData.dialCode+number;
            }, 100);
          }
				}
			}
      $scope.permissionsForManageExhibitors = function(){
          if(_.isEmpty($scope.conferencePermissions)) return false;
          if(($scope.conferencePermissions.indexOf("manage_exhibitors") <= -1) && ($scope.conferencePermissions.indexOf('admin') <= -1)) return false;
          else{
              return true;
          }
      };

      $scope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
          $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                active: false,
                logo: $scope.oldLogo,
                conferenceName: undefined
            });
          $scope.editMode = false;
      })

      // HEADER EVENTS

      $rootScope.$on('UPLOADED_LOGO_FROM_HEADER', function (event, data) {
          $scope.conference.logo = data.logo;
          $scope.conference.conferenceLogoFile = data.logoFile;
      });

      $rootScope.$on('CONFERENCE_TITLE_CHANGED_FROM_HEADER', function (event, data) {
          $scope.conference.name = data.newTitle;
      })

        // CANCEL AND SAVE BUTTONS

      function validateEditedConference(conference){
        if (!conference.name) {
            toastr.warning("The Event Name is a required field");
            return false;
        }
        if (!conference.venue) {
            toastr.warning("The Event Venue field is required");
            return false;
        }
        if (!conference.street) {
            toastr.warning("The Event Street field is required");
            return false;
        }
        if (!conference.city) {
            toastr.warning("The Event City field is required");
            return false;
        }
        if (!conference.state && conference.country == 'United States') {
            toastr.warning("The Event State field is required");
            return false;
        }
        if (!conference.zip_code) {
            toastr.warning("The Event Zip Code field is required");
            return false;
        }
        if (!conference.country) {
            toastr.warning("The Event Country field is required");
            return false;
        }
        if (conference.venue_phone_number === undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1) {
            toastr.warning("Please fill the venue phone number in a correct format");
            return false;
        }
        if (!conference.timezone_name) {
            toastr.error("Please review Event's country, zip code and state fields and correct them.");
            return false;
        }
        return true;
      }

      $scope.updateConference = function() {
          if(validateEditedConference($scope.conference) == false){
            return;
          }
          if ($scope.conferenceLogo || ($scope.conference.logo && (($scope.conference.logo.indexOf('data:image/png;base64') > -1) || ($scope.conference.logo.indexOf('blob:') > -1)))) {
              if (!$scope.conferenceLogo) {
                  $scope.conferenceLogo = $scope.conference.conferenceLogoFile;
              }
              fileService.getUploadSignature($scope.conferenceLogo)
                  .then(function(response){
                      var file_link = response.data.file_link;
                      var promises = [];

                      promises.push(fileService.uploadFile($scope.conferenceLogo, response));
                      var cloneConference = _.clone($scope.conference, true);
                      cloneConference.logo = file_link;
                      promises.push(conferenceService.updateConference(cloneConference));

                      $q.all(promises).then(function(response){
                          toastr.success('The event has been updated successfully.', 'Event Updated!');
                          $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                              active: false,
                              logo: $scope.conference.logo,
                              name: $scope.conference.name
                          });
                          $scope.oldLogo = $scope.conference.logo;
                          $scope.editMode = false;
                      }).catch(function(error){
                        toastr.error('Error!', 'There has been a problem with your request.');
                          $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                              active: false,
                              logo: undefined,
                              name: undefined
                          });
                      });

                  })
                  .catch(function(error){

                      if (error.status == 403) {
                          toastr.error('Permission Denied!');
                      } else {
                          lib.processValidationError(error, toastr);
                      }
                  });
          }
          else {
              var cloneConference = _.cloneDeep($scope.conference, true);

              cloneConference.date_from = new Date(cloneConference.date_from_editable);
              cloneConference.date_to = new Date(cloneConference.date_to_editable);

              if (cloneConference.state.abbreviation) {
                  cloneConference.state = cloneConference.state.abbreviation;
              } else {
                  cloneConference.state = cloneConference.state;
              }

              if (cloneConference.country.name) {
                  cloneConference.country = cloneConference.country.name;
              } else {
                  cloneConference.country = cloneConference.country;
              }
              //if country is not United States we send an empty value
              if (cloneConference.hasOwnProperty("state") && cloneConference.state === ""){
                  cloneConference.state = "";
              }

              cloneConference.date_from_raw.day    = parseInt(cloneConference.date_from_editable.tz(cloneConference.timezone_name).format('D'));
              cloneConference.date_from_raw.month  = parseInt(cloneConference.date_from_editable.tz(cloneConference.timezone_name).format('M'));
              cloneConference.date_from_raw.year   = parseInt(cloneConference.date_from_editable.tz(cloneConference.timezone_name).format('Y'));
              cloneConference.date_from_raw.hour   = 0;
              cloneConference.date_from_raw.minute = 1;

              cloneConference.date_to_raw.day    = parseInt(cloneConference.date_to_editable.tz(cloneConference.timezone_name).format('D'));
              cloneConference.date_to_raw.month  = parseInt(cloneConference.date_to_editable.tz(cloneConference.timezone_name).format('M'));
              cloneConference.date_to_raw.year   = parseInt(cloneConference.date_to_editable.tz(cloneConference.timezone_name).format('Y'));
              cloneConference.date_to_raw.hour   = 23;
              cloneConference.date_to_raw.minute = 59;


              conferenceService.updateConference(cloneConference).then(function(response) {
                toastr.success('The Event has been updated successfully.', 'Event Updated!');
                $scope.editMode              = false;
                $scope.googleMap             = lib.generateGoogleMaps(cloneConference.country, cloneConference.city, cloneConference.street);

                momentDateFromWithTz         = moment(response.date_from).tz($scope.conference.timezone_name);
                momentDateWithCorrection     = momentDateFromWithTz;
                $scope.conference.date_from  = momentDateWithCorrection.format('M/D/Y');

                momentDateToWithTz           = moment(response.date_to).tz($scope.conference.timezone_name);
                momentDateToWithCorrection   = momentDateToWithTz;
                $scope.conference.date_to    = momentDateToWithCorrection.format('M/D/Y');

                var country_name = cloneConference.country;
                if (cloneConference.country.name) {
                    country_name = cloneConference.country.name;
                }
                $scope.conference.time_zone = response.time_zone;
                $scope.googleStaticMap = lib.generateGoogleMapsDetailed(API.GOOGLE_API_KEY, country_name, cloneConference.city, cloneConference.street);
                $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                    active: false,
                    logo: $scope.conference.logo,
                    name: $scope.conference.name
                });
                $rootScope.conference.date_to = response.date_to;
                $rootScope.conference.date_from = response.date_from;
                savedFromDate = formatRawTimeService.createMomentFromRawTime(response.date_to_raw);
              }, function(error) {
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                  toastr.error('Error!', 'There has been a problem with your request.');
                }

                $rootScope.$emit('CONFERENCE_EDIT_MODE_CHANGED', {
                    active: false,
                    logo: undefined,
                    name: undefined
                });
              });
          }
        }

        // MODALS

        $scope.openSupportContactForm = function(currentContact) {
          var data = {};
          if(currentContact){
            data.currentContact = currentContact;
          }
          ngDialog.open({
            plain       : true,
            template    : '<support-contact-modal></support-contact-modal>',
            className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/supportContactModal/supportContactModalStyle.scss',
            data        : data
          })
        }

        $rootScope.$on('SUPPORT_CONTACT_UPDATED', function(event, data) {
          $scope.supportContacts = angular.copy(angular.forEach($scope.supportContacts, function(contact, index) {
            if (contact.id === data.supportContact.id) {
              $scope.supportContacts[index] = data.supportContact;
            }
          }));
        });

        $rootScope.$on('SUPPORT_CONTACT_ADDED', function(event, data) {
          $scope.supportContacts.push(data.supportContact);
        });

        $scope.deleteSupportContact = function(contact_id){
            var data = {
              "contact_id": contact_id,
              "supportContacts": $scope.supportContacts
            }
            ngDialog.open({
              plain       : true,
              template    : '<delete-support-contact-modal></delete-support-contact-modal>',
              className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteSupportContactModal/deleteSupportContactModalStyle.scss',
              data        : data
            });
        }

        $rootScope.$on('SUPPORT_CONTACT_DELETED', function(event, data) {
          $scope.supportContacts = _.reject($scope.supportContacts, ['id', data.supportContactId]);
        });

        // used to enable conference delete modal
        $scope.verifyConferenceOwnerPermissions = function(){
          if ($scope.conference.user.id == $scope.currentUser.id){
            return true;
          } else {
            return false;
          }
        }

        $scope.deleteConference = function() {
            ngDialog.open({
              plain       : true,
              template    : '<delete-conference-modal></delete-conference-modal>',
              className   : 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalStyle.scss'
            });
        }
        $scope.getConferenceTimezone = function() {
            var url = '';
            if($scope.conference.hasOwnProperty('zip_code') && $scope.conference.zip_code != '' && $scope.conference.hasOwnProperty('country') && $scope.conference.country.code != ""){
                if ($scope.conference.country != "United States") {
                    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=country:' +
                        $scope.conference.country + '|postal_code:' + $scope.conference.zip_code +
  											'&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
                } else {
                    if ($scope.conference.hasOwnProperty('state') && $scope.conference.state.abbreviation != "") {
                        url = 'https://maps.googleapis.com/maps/api/geocode/json?components=country:' +
                            $scope.conference.country + '|administrative_area:' +
                            $scope.conference.state +'|postal_code:' +
                            $scope.conference.zip_code + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk';
                    }
                }
            } else {
                $scope.userInvalidTimeZone = false;
                $scope.timeZoneIndex = -1;
                return;
            }

  					$http.get(url).then(function(response) {
                    response = response.data;
                    if(response.status == "OK"){
                        var latitude = response.results[0].geometry.location.lat;
                        var longitude = response.results[0].geometry.location.lng;
                        var timestamp = new Date().getTime().toString();
                        $http({
                            'method': "GET",
                            'url': 'https://maps.googleapis.com/maps/api/timezone/json?location=' + latitude + ',' + longitude + '&timestamp=' +
                            timestamp.substring(0, timestamp.length - 3) + '&key=' + 'AIzaSyCNkzJcTzViDRyBqNbwtunNB5dRDmByZyk'
                        })
                            .then(function(timezone_response){
                                timezone_response = timezone_response.data;
                                if(timezone_response.status == "OK"){
                                    $scope.userInvalidTimeZone = false;
                                    $scope.conference.timezone_name = timezone_response.timeZoneId;
                                } else if(timezone_response.status != "OK"){
                                    $scope.userInvalidTimeZone = true;
  																	toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                                    $scope.timeZoneIndex = -1;
                                }
                            })
                            .catch(function(timezone_error){
                                $scope.userInvalidTimeZone = true;
  															toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                                $scope.timeZoneIndex = -1;
                            });
                    } else if(response.status != "OK"){
                        $scope.userInvalidTimeZone = true;
  											toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                        $scope.timeZoneIndex = -1;
  											$scope.showTimezoneList = true;
                    }
                })
                .catch(function(error) {
                    $scope.userInvalidTimeZone = true;
  									toastr.warning("Please select one from the dropdown or modify the input.", "We couldn't find a timezone for the provided input. ");
                    $scope.timeZoneIndex = -1;
                });
            };


    }
})();
