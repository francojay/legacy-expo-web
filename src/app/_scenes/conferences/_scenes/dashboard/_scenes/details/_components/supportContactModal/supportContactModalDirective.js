(function(){
    'use strict';

    var directiveId = 'supportContactModal';
    var app = angular.module('app');

    var dependenciesArray = [
      '$rootScope',
      '$q',
      "toastr",
      'conferenceDetailsService',
      'fileService',
      'intlTelInputOptions',
      'API',
      supportContactModal
    ];
    app.directive(directiveId, dependenciesArray);

    function supportContactModal ($rootScope, $q, toastr, conferenceDetailsService, fileService, intlTelInputOptions, API) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/supportContactModal/supportContactModalView.html',
          link: link
      }

      function link(scope){
        var requestInProgress = false;
        var conference_id     = $rootScope.conference.conference_id;
        var supportContacts   = $rootScope.conference.conference_support_contacts;

        if(scope.ngDialogData.currentContact){
          scope.newContact = false;
          scope.newSupportContact = angular.copy(scope.ngDialogData.currentContact);
          scope.title = "Edit a Support Contact";
        } else {
          scope.newContact = true;
          scope.newSupportContact = {};
          scope.title = "Add a Support Contact";
        }

        scope.contactLabels = [
            {label:'First Name', code: 'first_name'},
            {label:'Last Name', code: 'last_name'},
            {label:'Company Name', code: 'company_name'},
            {label:'Email Address', code: 'email'},
            {label:'Phone Number', code: 'phone_number'},
            {label:'Area of Support', code: 'area_of_support'}
        ];
        scope.defaultCountryFlag = 'us';
        angular.forEach(API.COUNTRIES, function(country) {
          if(country.name === $rootScope.conference.country){
            scope.defaultCountryFlag = country.code.toLowerCase();
          }
        });
        function validateSupportContactFields(supportContact){
          if(supportContact.first_name == ''){
            toastr.warning('First name required!');
            return false;
          }
          if(supportContact.last_name == ''){
            toastr.warning('Last name required!');
            return false;
          }
          if(supportContact.company_name == ''){
            toastr.warning('Company name required!');
            return false;
          }
          if(supportContact.area_of_support == ''){
            toastr.warning('Area of support required!');
            return false;
          }
          if(supportContact.phone_number === undefined){
            toastr.warning('Please insert a  phone number with a correct format!');
            return false;
          }
          if(supportContact.email){
            if(supportContact.email.search(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) < 0){
              toastr.warning('Invalid email address!');
              return false;
            }
          }
          return true;
        }

        scope.createNewSupportContact = function(newSupportContact, closeThisDialog) {
            newSupportContact.conference_id = conference_id;
            if (!requestInProgress) {
              if(validateSupportContactFields(newSupportContact)) {
                requestInProgress = true;
                conferenceDetailsService.createSupportContact(newSupportContact).then(function(data) {
                  $rootScope.$broadcast('SUPPORT_CONTACT_ADDED', {supportContact: data});
                  if (closeThisDialog == true) {
                      scope.closeThisDialog();
                  } else {
                      scope.eraseInputs();
                  }
                  requestInProgress = false;
                }, function(error) {
                  if (error.status == 403) {
                      toastr.error('Permission Denied!');
                  } else {
                      toastr.error('Error!', 'There has been a problem with your request.');
                  }
                  requestInProgress = false;
                });
              }
            }
        }

        scope.updateSupportContact = function(newSupportContact) {
          if (!requestInProgress) {
            if(validateSupportContactFields(newSupportContact)) {
              requestInProgress = true;
              conferenceDetailsService.updateSupportContact(scope.newSupportContact).then(function(data) {
                $rootScope.$broadcast('SUPPORT_CONTACT_UPDATED', {supportContact: data});
                scope.closeThisDialog();
                requestInProgress = false;
              }, function(error) {
                if (error.status == 403) {
                  toastr.error('Permission Denied!');
                } else {
                  toastr.error('Error!', 'There has been a problem with your request.');
                }
                requestInProgress = false;
              });
            }
          }
        }

        scope.eraseInputs = function() {
           scope.newSupportContact = {};
        };


        scope.canSaveSupportContacts = function(supportContact) {
            if (!supportContact.first_name || !supportContact.last_name || !supportContact.company_name || !supportContact.area_of_support) {
                return false;
            }
            return true;
        }

        scope.uploadSupportPhoto = function(files, support_contact) {
          lib.squareifyImage($q, files)
            .then(function(response){
                var imgGile = response.file;

                fileService.getUploadSignature(imgGile)
                    .then(function(response){
                        var file_link = response.data.file_link;
                        var promises = [];

                        promises.push(fileService.uploadFile(imgGile, response));

                        $q.all(promises).then(function(response){
                            support_contact.photo = file_link;
                        }).catch(function(error){
                            console.log(error);
                        });
                    })
                    .catch(function(error){
                        console.log(error);
                        if (error.status == 403) {
                            toastr.error('Permission Denied!');
                        } else {
                            toastr.error('Error!', 'There has been a problem with your request.');
                        }
                    });
              });
        }

      }





      }

})();
