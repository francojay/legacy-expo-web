(function(){
    'use strict';

    var directiveId = 'deleteSupportContactModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', "toastr", 'conferenceDetailsService', deleteSupportContactModal]);

    function deleteSupportContactModal($rootScope, toastr, conferenceDetailsService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteSupportContactModal/deleteSupportContactModalView.html',
          link: link
      }

      function link(scope){

        var contact_id = scope.ngDialogData.contact_id;
        var supportContacts = scope.ngDialogData.supportContacts;

        scope.deleteSupportContactConfirm = function(){
            conferenceDetailsService
                .deleteSupportContact(contact_id)
                .then(function(data){
                    $rootScope.$broadcast('SUPPORT_CONTACT_DELETED', {supportContactId: contact_id});
                })
                .catch(function(error){
                    if (error.status == 403) {
                        toastr.error('Permission Denied!');
                    } else {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    }
                });
          }
      }
    }

})();
