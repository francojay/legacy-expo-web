(function(){
  'use strict';

  var directiveId = 'deleteConferenceModal';

  angular
    .module('app')
    .directive(directiveId, ['$rootScope', '$state', 'toastr', 'conferenceService', deleteConferenceModal]);

    function deleteConferenceModal($rootScope, $state, toastr, conferenceService) {

      return {
          restrict: 'E',
          replace: true,
          templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_components/deleteConferenceModal/deleteConferenceModalView.html',
          link: link
      }

      function link(scope){

        var conference = $rootScope.conference;
        var conference_id = $rootScope.conference.conference_id;

        scope.confirmDeleteConference = function() {
            conferenceService.deleteConference(conference, conference_id)
                .then(function(){
                    toastr.success('The event has been deleted successfully.', 'Event Deleted!');
                    $state.go('conferences.overview');
                })
                .catch(function(error){
                    toastr.error('Error!', 'There has been a problem with your request.');
                })
        }

      }

    }
})();
