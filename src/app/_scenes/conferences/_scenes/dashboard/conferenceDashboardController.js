(function () {
    'use strict';

    var controllerId = 'conferenceDashboardController';

    angular
        .module('app')
        .controller(controllerId, ['$rootScope', conferenceDashboardController]);

    function conferenceDashboardController($rootScope) {

        var vm = this;
        vm.conference = $rootScope.conference;

        $rootScope.$on('EditConference', function (EditConference, editMode) {
            vm.editMode = editMode;
        });

    }
})();
