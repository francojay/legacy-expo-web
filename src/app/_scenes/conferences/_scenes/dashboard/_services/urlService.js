(function(){
	'use strict';

    var serviceId = 'urlService';

	angular
		.module('app')
		.service(serviceId, [
            urlService
		]);
		function urlService(){
		    var service = {};

            service.unSanitizeUrl = function(url) {
                if (url) { 
                    if (url.substring(0, 7) == "http://") {
                        return url.replace("http://", "");
                    } else if (url.substring(0, 8) == "https://") {
                        return url.replace("https://", "");
                    }
                }

                return url;
            };

            service.sanitizeUrl = function(url) {
                if (url && url.substring(0, 7) != "http://" && url.substring(0, 8) != "https://") {
                    return "http://" + url;
                } else if(url && url.search('https://') == 0){
                    return url.replace('https://', 'http://');
                }

                return url;
            };

            return service;
        }
})();