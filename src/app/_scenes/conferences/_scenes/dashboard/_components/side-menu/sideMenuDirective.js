(function(){
	'use strict'

	var directiveId 		= 'sideMenu';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'$state',
		'ngDialog',
		'localStorageService',
		'conferenceRegistrationDataService',
		sideMenu
	];
	app.directive(directiveId, depdenciesArray);

	function sideMenu ($rootScope, $state, ngDialog, localStorageService, conferenceRegistrationDataService) {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/side-menu/sideMenuView.html',
			link: link
		};

		function link(scope) {
			var conference 							 = $rootScope.conference;
			scope.showBadgeEditorFeature = conference.features.badge;
			scope.showRevenueFeature 	 =  localStorageService.get("is_superuser");
			scope.$state 								 = $state;

			getConferenceRegistrationForm();

			scope.handleRegistrationClick = function() {
				if(conference.has_registration_form == true) {
						$state.go('conferences.dashboard.registration.overview');
				} else {
					ngDialog.open({
							plain: true,
							template: '<start-registration-modal></start-registration-modal>',
							className: 'app/_scenes/conferences/_components/startRegistration/startRegistrationStyles.scss',
					});
				}
			}

			function getConferenceRegistrationForm() {
				conferenceRegistrationDataService
					.getRegistrationForm(conference.conference_id)
					.then(function(data) {
						$rootScope.registrationForm = data[0]
					}, function(error) {
						toastr.error('Error!', 'There has been a problem with your request.');
					})
			}
		}
	}
})();
