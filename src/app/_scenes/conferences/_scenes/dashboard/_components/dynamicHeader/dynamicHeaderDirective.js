(function () {
    'use strict'

    var directiveId = 'dynamicHeader';
    var app =   angular.module('app');
    var dependenciesArray = [
      'SharedProperties',
      '$rootScope',
      'UserService',
      '$q',
      'ngDialog',
      'localStorageService',
      'toastr',
      dynamicHeader
    ];
    app.directive(directiveId, dependenciesArray);

    function dynamicHeader(SharedProperties, $rootScope, UserService, $q, ngDialog, localStorageService, toastr) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/dynamicHeader/dynamicHeaderView.html',
            scope: {
                conferenceDetails: "="
            },
            link: link
        }

        function link(scope) {
            var conference = scope.conferenceDetails;
            var unwatch;

            scope.conferenceName = "Retrieving event name ...";
            scope.conferenceLogo = undefined;
            scope.conferenceIsInEditMode = false;

            getUserProfile().then(function(currentUser) {
                scope.currentUser = currentUser;
                $rootScope.isSuperAdmin = currentUser.is_superuser;

                var eventData = {
      	            'eventCreated': false,
      	            'eventName': "",
      	            'superAdminLink': "",
      	            'eventDate': ""
      	        }
      					window.Intercom('boot', {
      				     app_id: 'ij3yrbte',
      				     email: scope.currentUser.email,
      				     user_hash: scope.currentUser.user_signed,
      				     expo_user_id: scope.currentUser.id,
      				     name: scope.currentUser.first_name + ' ' + scope.currentUser.last_name,
      				     created_at: 1234567890,
      				     custom_launcher_selector: '#my_custom_link',
      				     "eventCreated": eventData.eventCreated
      				  });
            });

            $rootScope.$on('CONFERENCE_EDIT_MODE_CHANGED', function (event, data) {
                scope.conferenceIsInEditMode = data.active;
                if (data.logo !== undefined) {
                    scope.conferenceLogo = data.logo;
                }
                if (data.name) {
                    scope.conferenceName = data.name;
                }
            })

            $rootScope.$on('UPLOADED_LOGO_FROM_COVER_SCREEN',function(event,data) {
                scope.conferenceLogo = data.logo;
            })

            scope.changeConferenceLogo = function (files) {
                lib.squareifyImage($q, files)
                    .then(function (response) {
                        scope.conferenceLogo = response.img;
                        $rootScope.$broadcast('UPLOADED_LOGO_FROM_HEADER', {
                            logo: response.img,
                            logoFile: response.file
                        })
                    });
            }

            scope.notifySpecificsControllerOfTitleChange = function (newTitle) {
                $rootScope.$broadcast('CONFERENCE_TITLE_CHANGED_FROM_HEADER', {
                    newTitle: newTitle
                })
            }

            unwatch = scope.$watch('conferenceDetails', function (newConferenceValue, oldConferenceValue) {
                if (newConferenceValue) {
                    init(newConferenceValue);
                    unwatch();
                }
            });

            scope.openAccountProfile = function() {
              ngDialog.open(
                  {
                      plain: true,
                      template: '<user-info-modal></user-info-modal>',
                      className: 'app/directives/userInfo/userInfo.scss',
                      data: {
                          currentUser: scope.currentUser
                      }
                  });
            };

            function getUserProfile() {
               return UserService.getProfile()
                    .then(function(user) {
                        localStorage.currentUser = JSON.stringify(user);
                        localStorageService.set('currentUser',JSON.stringify(user));
                        return user;
                    })
                    .catch(function(error){
                        console.log(error);
                    });
            }

            function init(data) {
                conference.originalLogo = data.logo;
                scope.conferenceName = data.name;
                scope.conferenceLogo = data.logo;

                UserService.getProfile().then(function (user) {
                    localStorage.currentUser = JSON.stringify(user);
                    localStorageService.set('currentUser',JSON.stringify(user));
                    scope.currentUser = user;
                }, function(error) {
                    toastr.error('There has been a problem with getting the data for your user.')
                })
            }
        }
    }
})();
