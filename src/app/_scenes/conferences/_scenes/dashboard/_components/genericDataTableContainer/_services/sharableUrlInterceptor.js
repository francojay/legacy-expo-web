// this service will have to intercept modifications on some variables and based on those modifications it will generate a new url for the application
(function(){
	'use strict';

  var serviceId = 'sharableURLInterceptor';
	var dependenciesArray = [
		'$rootScope',
		'$state',
		sharableURLInterceptor
	];

	angular.module('app').service(serviceId, dependenciesArray);

	function sharableURLInterceptor($rootScope, $state) {
    var service 				 = {};

		service.sharableLink = "";

		service.sharableDataStructure = {
			dataQuery 			 	 	 			 : "", // XXX:
			dataQueryPlaceholder 			 : "", // XXX:
			selectedDayTypeFilter 		 : "", // XXX:
			customDateFilter 	 				 : { // XXX:
				from: "",
				to  : ""
			},
			filters	 				 	 	 			 : [],
			completeFilters						 : [],
			filtersApplied 						 : false,
			sortType 				 	 	 			 : "",
			sortColumn								 : "",
			currentlyUsedPreviousToken : null, // XXX:
			currentlyUsedNextToken 		 : null, // XXX:
			page 											 : null, // XXX:
			fields 						 		 		 : [], // XXX:
			tableDensity 			 	   		 : "", // XXX:
			showRemovedState 					 : false
		};

		service.sharedUrlExists = false;

		service.usedSharedInfo = {
			genericDataSearchBox 			 : false, // XXX:
			genericDaysSelector 			 : false, // XXX:
			genericFilterListCreator 	 : false, // XXX:
			genericSettingsTool 	 		 : false, // XXX
			listWrapper								 : false
		}

		service.updateVariable = function(variable, value) {
			service.sharableDataStructure[variable] = value;
			service.sharableLink = service._createSharableLink(service.sharableDataStructure, service.sharableLink);
			$rootScope.$broadcast('SHARE_LINK_IS_AVAILABLE');
			// $state.go('.', {
			// 	share : JSON.stringify(service.sharableDataStructure)
			// }, {notify: false});
		}

		service.updateCustomDate = function(fromValue, toValue) {
			if (fromValue) {
				service.sharableDataStructure.customDateFilter.from = fromValue;
			}
			if (toValue) {
				service.sharableDataStructure.customDateFilter.to = toValue;
			}
		}

		service.updateUsedInfo = function(variable, value) {
			service.usedSharedInfo[variable] = true;
		}

		service._createSharableLink = function(dataStructure, currentLink) {
			var link = "";
			link = JSON.stringify(dataStructure);
			return link;
		}

		service.resetSharableLink = function() {
			service.sharableLink = "";
			$rootScope.$broadcast('SHARE_LINK_IS_AVAILABLE');
		}

		service.resetVariables = function() {
			service.sharableDataStructure = {
				dataQuery 			 	 	 			 : "", // XXX:
				dataQueryPlaceholder 			 : "", // XXX:
				selectedDayTypeFilter 		 : "", // XXX:
				customDateFilter 	 				 : { // XXX:
					from: "",
					to  : ""
				},
				filters	 				 	 	 			 : [],
				completeFilters						 : [],
				filtersApplied 						 : false,
				sortType 				 	 	 			 : "",
				sortColumn								 : "",
				currentlyUsedPreviousToken : null, // XXX:
				currentlyUsedNextToken 		 : null, // XXX:
				page 											 : null, // XXX:
				fields 						 		 		 : [], // XXX:
				tableDensity 			 	   		 : "", // XXX:
				showRemovedState 					 : false
			};
		}

		return service;
  }
})();
