(function(){
	'use strict';

  var serviceId = 'dataTableConfigGetter';
	var dependenciesArray = [
		'$q',
		'requestFactory',
		dataTableConfigGetter
	];
	angular.module('app').service(serviceId, dependenciesArray);

	function dataTableConfigGetter($q, requestFactory) {
    var service = {};

		service.getTableConfig = function(conferenceId, table) {
			var deferred = $q.defer();
			if (conferenceId) {
				var url 		 = "api/v1.1/conferences/" + conferenceId + "/" + table + '/meta/';
			} else {
				var url 		 = "api/v1.1/" + table + "/meta/";
			}

			requestFactory.get(url, {}, false, true).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

    return service;
  }
})();
