(function(){
	'use strict'

	var directiveId 		= 'genericSettingsTool';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'genericDataTableDataService',
		'genericDataTableInfoStore',
		'sharableURLInterceptor',
		'$timeout',
		genericSettingsToolDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericSettingsToolDirective($q, $rootScope, genericDataTableDataService, genericDataTableInfoStore, sharableURLInterceptor,$timeout) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericSettingsTool/genericSettingsToolView.html',
			scope : {
				fields 				 	 : "=",
				tableDensities 	 : "=",
				removedItemsMeta : "=",
				shared				 	 : "="
			}
		};
    function link (scope) {
			$timeout(function() {
				$('.generic-settings-tool-wrapper .settings-modal-wrapper .upper-part .generic-fields-wrapper').mCustomScrollbar({
				  axis 								: "y",
				  theme								: "minimal-dark",
					scrollInertia				: 0,
					autoHideScrollbar 	: false,
					alwaysShowScrollbar : true,
					snapAmount 					: 0,
				  advanced					: {
						autoExpandHorizontalScroll : true
					}
				});
			}, 0);
			scope.showDefaultFields 	= true;
			scope.allFieldAreSelected = false;
			scope.changedSettings 		= false;
			scope.showRemovedState 		= false;

			var defaultFields 				= angular.copy(scope.fields);
			var selectedFields				= 0;
			var selectedDefaultFields = 0;
			var defaultSettingsState  = {
				"fields"							  : scope.fields,
				"tableDensitiesDefault" : null,
				"showRemovedState"			: false
			};

			if (scope.shared) {
				sharableURLInterceptor.updateUsedInfo(directiveId, true);

				scope.showRemovedState = scope.shared.showRemovedState;
				genericDataTableInfoStore.setShowRemovedState(scope.showRemovedState);
				$rootScope.$broadcast("SHOW_REMOVED_ITEMS_STATE_CHANGED", {
					state: scope.showRemovedState
				});
			}

			if (scope.shared && scope.shared.fields && scope.shared.fields.length > 0) {
				for (var i = 0; i < scope.fields.length; i++) {
					for (var j = 0; j < scope.shared.fields.length; j++) {
						if (scope.fields[i].name === scope.shared.fields[j].name) {
							scope.fields[i].selected = scope.shared.fields[j].selected;
							if (scope.shared.fields[j].selected) {
								selectedFields++;
							}

						}
					}
				}
				$rootScope.$broadcast(genericDataTableDataService.events.TABLE_HEADER_CONFIGURATION_UPDATED, scope.fields);
			} else {
				for (var i = 0; i < scope.fields.length; i++) {
					if (scope.fields[i].selected) {
						selectedFields++;
					}
				}
			}

			scope.settingsModalOpened = false;
			scope.tableDensitiesDefault = [
				{
					value 	 : 50,
					class 	 : "fa fa-bars",
					selected : false,
				},
				{
					value    : 75,
					class 	 : "fa fa-align-justify",
					selected : false
				},
				{
					value 	 : 100,
					class 	 : "fa fa-barcode inverted",
					selected : false
				}
			];

			angular.merge(scope.tableDensitiesDefault, scope.tableDensities.options);

			for (var i = 0; i < scope.tableDensitiesDefault.length; i++) {
				if (scope.shared && scope.shared.tableDensity) {
					if (scope.tableDensitiesDefault[i].name === scope.shared.tableDensity.name) {
						scope.tableDensitiesDefault[i].selected = true;
						genericDataTableInfoStore.updateTableDensity(scope.tableDensitiesDefault[i]);
					}
				} else {
					if(scope.tableDensitiesDefault[i].is_default) {
						scope.tableDensitiesDefault[i].selected = true;
					}
				}
			}
			for( var i = 0; i < defaultFields.length; i++ ) {
				if(defaultFields[i].selected) {
					selectedDefaultFields++;
				}
			}

			scope.allFieldAreSelected = (selectedFields == defaultFields.length) ? true : false;
			defaultSettingsState.tableDensitiesDefault = scope.tableDensitiesDefault;

			scope.viewOptions = [
				{
					value 	 : 'table',
					class 	 : "fa fa-list-ul",
					selected : true
				},
				{
					value 	 : 'chart',
					class 	 : "fa fa-bar-chart",
					selected : false
				}
			]

			scope.openSettingsModal = function(event, state) {
				if(!state && !scope.changedSettings) {
					copyCurrentSettingsState();
				}
				else if(state) {
					scope.changedSettings = false;
					scope.tableDensitiesDefault = defaultSettingsState.tableDensitiesDefault;
					scope.fields 								= defaultSettingsState.fields;
					selectedFields							= defaultSettingsState.selectedFields;
					scope.showDefaultFields 		= (selectedFields == 0) ? false : true;
					scope.allFieldAreSelected 	= (selectedFields == defaultFields.length) ? true : false;
					scope.showRemovedState			= defaultSettingsState.showRemovedState;
				}

				scope.settingsModalOpened = !state;
			}

			scope.toggleFieldSelectState = function(event, field) {
				event.stopPropagation();
				field.selected = !field.selected;
				if(field.selected) {
					selectedFields ++;
				} else {
					selectedFields --;
				}

				scope.changedSettings = true;

				scope.showDefaultFields 	= (selectedFields == 0) ? false : true;
				scope.allFieldAreSelected = (selectedFields == defaultFields.length) ? true : false;
				genericDataTableInfoStore.setActiveFields(scope.fields);
			};

			scope.selectTableDensity = function(event, density) {
				event.stopPropagation();

				for (var i = 0; i < scope.tableDensitiesDefault.length; i++) {
					scope.tableDensitiesDefault[i].selected = false;
				}
				density.selected = true;

				scope.changedSettings = true;
				genericDataTableDataService.sharedPreviousToken = null;
				genericDataTableDataService.sharedNextToken		 	= null;
			};

			scope.toggleAllFieldsSelectionState = function($event, fields, state) {
				$event.stopPropagation();
				$event.preventDefault();

				for (var i = 0; i < fields.length; i++) {
					fields[i].selected = state;
				}
				if(state) {
					selectedFields = fields.length;
				} else {
					selectedFields = 0;
				}

				scope.changedSettings = true;
				scope.showDefaultFields = (selectedFields == 0) ? false : true;
				scope.allFieldAreSelected = (selectedFields == defaultFields.length) ? true : false;
				genericDataTableInfoStore.setActiveFields(scope.fields);

			}

			scope.hideDropdown = function() {
				scope.settingsModalOpened = false;
				scope.changedSettings = false;
				scope.tableDensitiesDefault = defaultSettingsState.tableDensitiesDefault;
				scope.fields 								= defaultSettingsState.fields;
				selectedFields							= defaultSettingsState.selectedFields;
				scope.showDefaultFields 	  = (selectedFields == 0) ? false : true;
				scope.allFieldAreSelected   = (selectedFields == defaultFields.length) ? true : false;
				scope.showRemovedState			= defaultSettingsState.showRemovedState;
			}

			scope.showDefaultFieldsState = function ($event) {
				$event.stopPropagation();
				$event.preventDefault();

				scope.fields = angular.copy(defaultFields);
				scope.showDefaultFields = true;
				selectedFields = selectedDefaultFields;
				scope.allFieldAreSelected = (selectedFields == defaultFields.length) ? true : false;
				scope.changedSettings = true;
				genericDataTableInfoStore.setActiveFields(scope.fields);
			}

			scope.applySettings = function ($event) {
				$event.stopPropagation();
				$event.preventDefault();
				var density = null;
				for (var i = 0; i < scope.tableDensitiesDefault.length; i++) {
					if(scope.tableDensitiesDefault[i].selected == true){
						density = scope.tableDensitiesDefault[i];
					}
				}

				if(genericDataTableInfoStore.tableDensity.page_size != density.page_size ) {
					if (defaultSettingsState.showRemovedState == scope.showRemovedState)	{
							genericDataTableInfoStore.updateTableDensity(density);
					} else {
						genericDataTableInfoStore.tableDensity = density;
					}

				}

				$rootScope.$broadcast(genericDataTableDataService.events.TABLE_HEADER_CONFIGURATION_UPDATED, scope.fields);
				if (defaultSettingsState.showRemovedState != scope.showRemovedState)	{
					genericDataTableInfoStore.setShowRemovedState(scope.showRemovedState);
				}
				copyCurrentSettingsState();
				$rootScope.$broadcast("SHOW_REMOVED_ITEMS_STATE_CHANGED", {
					state: scope.showRemovedState
				});

				scope.changedSettings 		= false;
				scope.settingsModalOpened = false;
			}

			scope.toggleRemovedItemsFlag = function($event, state) {
				$event.stopPropagation();
				$event.preventDefault();

				scope.showRemovedState = !state;
			}

			function copyCurrentSettingsState() {
					defaultSettingsState.fields = angular.copy(scope.fields);
					defaultSettingsState.tableDensitiesDefault = angular.copy(scope.tableDensitiesDefault);
					defaultSettingsState.selectedFields = angular.copy(selectedFields);
					defaultSettingsState.showRemovedState = angular.copy(scope.showRemovedState);
			}

    }
  }
})();
