(function(){
	'use strict'

	var directiveId 		= 'longRunningProcessModal';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		longRunningProcessModalDirective
	];

	app.directive(directiveId, depdenciesArray);

  function longRunningProcessModalDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericActionsSelector/_components/longRunningProcessModal/longRunningProcessModalView.html',
		};
    function link (scope) {
			scope.processData = scope.ngDialogData._data;

			scope.downloadFile = function(process) {
				console.log(process);

				var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document


        anchor.attr({
            href: process.result_url,
            target: '_blank',
        })[0].click();

        anchor.remove();

				genericDataTableDataService.archiveProcess(process.id).then(function(response) {
					console.log(response);
					scope.closeThisDialog();
				}, function(error) {
					console.log(error);
				});
			}

			scope.redirectToAvery = function(process) {
				debugger
				var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document


        anchor.attr({
            href: process.result_url,
            target: '_blank',
        })[0].click();

        anchor.remove();

				genericDataTableDataService.archiveProcess(process.id).then(function(response) {
					console.log(response);
					scope.closeThisDialog();
				}, function(error) {
					console.log(error);
				});
			}

			scope.fetchProcesses = function() {
				$rootScope.$broadcast("FETCH_PROCESSES", {});
			}
    }
  }
})();
