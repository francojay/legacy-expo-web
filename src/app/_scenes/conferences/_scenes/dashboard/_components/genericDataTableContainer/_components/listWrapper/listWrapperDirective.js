(function(){
	'use strict'

	var directiveId 		= 'listWrapper';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'genericDataTableDataService',
		'$timeout',
		'$filter',
		'ngDialog',
		'genericDataTableInfoStore',
		'sharableURLInterceptor',
		'$injector',
		'$state',
		'conferenceService',
		listWrapperDirective
	];

	app.directive(directiveId, depdenciesArray);

  function listWrapperDirective($q, $rootScope, genericDataTableDataService, $timeout, $filter, ngDialog, genericDataTableInfoStore, sharableURLInterceptor, $injector, $state, conferenceService) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/listWrapper/listWrapperView.html',
			scope : {
				listConfig					: "=",
				defaultSort 				: "=",
				share			  				: "=",
				itemClickActionType : "="
			}
		};
    function link (scope) {
			var currentConference  = $rootScope.conference;
			var allRowsAreSelected = false;

			if (scope.share && scope.share.sortColumn && scope.share.sortType) {
				var selectedColumnToSort 		= scope.share.sortColumn;
				var selectedDirectionToSort = scope.share.sortType;
				genericDataTableInfoStore.updateSort(selectedColumnToSort, selectedDirectionToSort);
			} else {
				var selectedColumnToSort 		= scope.defaultSort.field || "";
				var selectedDirectionToSort = scope.defaultSort.order || "";
			}

			sharableURLInterceptor.updateUsedInfo(directiveId, true);

			scope.columns 				 = [];
			scope.listItems 			 = null;
			scope.selectedRowsIds  = [];
			scope.showRemovedState = false;

			scope.openFile = function(fileLink) {
				if(fileLink != null) {
					window.open(fileLink, "_blank");
				}
			}

			scope.handleItemClick = function(_id) {
				if (scope.itemClickActionType === 'redirect') {
					var eventId = conferenceService.hash_conference_id.encode(_id);
					$state.go('conferences.dashboard.details.overview', {
						"conference_id": eventId
					});
				} else if (scope.itemClickActionType === 'modal') {
					var templateModal = "<" + genericDataTableDataService.endpointType + "-details-modal></" + genericDataTableDataService.endpointType + "-details-modal>";
					if ($injector.has(genericDataTableDataService.endpointType+'DetailsModalDirective')) {
						var data = {
							"_id" : _id
						};
						if (scope.showRemovedState) {
							data.removedItem = true ;
						}
						ngDialog.open({
							plain: true,
							template: templateModal,
							className: '',
							data: data
						});
		      }
				}
			}

			scope.selectAllRows = function($event, state) {
				if ($event) {
					$event.stopPropagation();
					$event.preventDefault();
				}

				scope.selectedRowsIds 	= [];
				allRowsAreSelected		  = state || !allRowsAreSelected;
				var columnWithSelectors = scope.columns[0];
				for (var i = 0; i < columnWithSelectors.valueObjects.length; i++) {
					columnWithSelectors.valueObjects[i].selectedRow = allRowsAreSelected;
					scope.selectRow($event, columnWithSelectors.valueObjects[i], true, allRowsAreSelected);
				}
			}

			scope.selectRow = function($event, valueObject, shouldOverride, selectedState) {
				if ($event) {
					$event.stopPropagation();
					$event.preventDefault();
				}

				if (shouldOverride) {
					valueObject.selectedRow = selectedState;
				} else {
					valueObject.selectedRow = !valueObject.selectedRow;
				}
				//remove current clicked element if already exists
				if (!valueObject.selectedRow) {
					for (var i = 0; i < scope.selectedRowsIds.length; i++) {
						if (valueObject._id === scope.selectedRowsIds[i]) {
							scope.selectedRowsIds.splice(i, 1);
						}
					}
				}

				//put back the id only if selected
				if (valueObject.selectedRow) {
					scope.selectedRowsIds.push(valueObject._id);
				} else {
					allRowsAreSelected = false;
				}

				if (scope.columns[0].length === scope.selectedRowsIds.length) {
					allRowsAreSelected = true;
				}

				for (var i = 0; i < scope.columns.length; i++) {
					var currentColumn = scope.columns[i];
					for (var j = 0; j < currentColumn.valueObjects.length; j++) {
						var currentValueObject = currentColumn.valueObjects[j];
						if (currentValueObject._id === valueObject._id) {
							currentValueObject.selected = valueObject.selectedRow;
						}
					}
				}
			}

			scope.sortBy = function($event, column) {
				if(!column.canSortBy) {
					return;
				}
				genericDataTableDataService.sharedNextToken 		= null;
				genericDataTableDataService.sharedPreviousToken = null;
				column.sortBy 		= true;
				if (column.sortType === "") {
					column.sortType = "desc";
				} else if (column.sortType === 'desc') {
					column.sortType = "asc";
				} else if (column.sortType === 'asc') {
					column.sortType = 'desc';
				}

				for (var i = 1; i < scope.columns.length; i++) {
					if (scope.columns[i].name !== column.name) {
						scope.columns[i].sortBy 	= false;
						scope.columns[i].sortType = "";
					}
				}

				selectedColumnToSort 		= column.name;
				selectedDirectionToSort = column.sortType;
				scope.listItems = [];
				genericDataTableInfoStore.updateSort(column.name, column.sortType);
			}

			function createColumnsWithData(columnsConfig, data, fullReconfiguration) {
				data = data || [];
				if (fullReconfiguration) {
					var columns = [];
					columns.push({
						label  			 : "",
						name 	 			 : "selector",
						format 			 : "",
						baseType 		 : "selector",
						fieldType 	 : "selector",
						valueObjects : populateSelectorColumn(data)
					});
				} else {
					var columns 			 = [];
					if(scope.columns) {
						var selectorColumn = JSON.parse(JSON.stringify(scope.columns[0]));
						columns.push(selectorColumn);
					}
				}

				for (var i = 0; i < columnsConfig.length; i++) {
					if (columnsConfig[i].selected) {
						var column = {
							label 			 : columnsConfig[i].label,
							name  			 : columnsConfig[i].name,
							format 			 : columnsConfig[i].format,
							baseType 		 : columnsConfig[i].base_type,
							fieldType		 : columnsConfig[i].field_type,
							canSortBy		 : columnsConfig[i].can_sort_by,
							sortBy			 : false,
							sortType		 : "",
							valueObjects : []
						};

						if (column.name === selectedColumnToSort) {
							column.sortBy 	= true;
							column.sortType = selectedDirectionToSort
						}


						for (var j = 0; j < data.length; j++) {
							var listItem = data[j];
							var _id 		 = listItem.id;
							var value 	 = listItem[column.name];

							if (column.baseType === 'datetime') {
									var valueObject = {
										"_id"			 : _id,
										"selected" : false
									};
									if (currentConference) {
										valueObject["value"] = value ? moment(value).tz(currentConference.timezone_name).format(column.format) : "Not Applicable";
									} else {
										valueObject["value"] = value ? moment(value).format(column.format) : "Not Applicable";
									}
							} else if (column.baseType === 'enum') {
								var valueObject = {
									"value" 	 : value ? value.charAt(0).toUpperCase() + value.slice(1) : "",
									"_id"			 : _id,
									"selected" : false
								}
							} else if (column.fieldType === 'currency') {
								var valueObject = {
									"value"		 : value,
									"_id"			 : _id,
									"selected" : false
								}
							}
							else {
								var valueObject = {
									"value" 	 : value,
									"_id"			 : _id,
									"selected" : false
								}
							}

							if (columns[0]) {
								valueObject.selected = columns[0].valueObjects[j].selectedRow;
							}
							column.valueObjects.push(valueObject);
						}
						columns.push(column);
					}
				};
				return columns;
			}

			function populateSelectorColumn(data) {
				var values = [];

				for (var i = 0; i < data.length; i++) {
					values.push({
						"_id"					: data[i].id,
						"selectedRow" : false
					});
				}

				return values;
			}

			function updateAppearanceOfTable() {
				var $columns					 = $('.list-column');
				var width	  					 = 0;
				var actualWidthOfTable = $('.generic-list-wrapper').width();
				var elmnt = document.querySelector(".columns-header");
				var offsetWidthOfTable = elmnt ? elmnt.offsetWidth : 0;

				actualWidthOfTable = actualWidthOfTable < offsetWidthOfTable ? offsetWidthOfTable - 200 : actualWidthOfTable;
				for (var i = 0; i < $columns.length; i++) {

					$columns[i].style.width = Math.ceil($columns[i].getBoundingClientRect().width) <= 265 ? Math.ceil($columns[i].getBoundingClientRect().width) + 'px': '265px';

					if (i === $columns.length - 1 && width < actualWidthOfTable && actualWidthOfTable - width > $columns[i].getBoundingClientRect().width) {

						$columns[i].style.width = actualWidthOfTable -  width + 'px';

					}

					width += $columns[i].getBoundingClientRect().width
				}

				scope.width = width;

			}

			function updateColumnsConfiguration(currentColumns, listConfiguration) {
				for (var i = 0; i < currentColumns.length; i++) {
					var currentColumn = currentColumns[i];
					for (var j = 0; j < listConfiguration.length; j++) {
						var currentField = listConfiguration[j];
						if ((currentColumn.name === currentField.name) && !currentField.selected) {
							currentColumns.splice(i, 1);
							break;
						}
					}
				}
				return currentColumns;
			}

			scope.$watch('selectedRowsIds', function(newValue, oldValue) {
				genericDataTableInfoStore.updateSelectedItems(scope.selectedRowsIds);

				if (scope.listItems && (newValue.length === scope.listItems.length) && newValue.length > 0) {
					genericDataTableInfoStore.togglePageSelectionState(allRowsAreSelected);
					$rootScope.$broadcast('ALL_ROWS_IN_LIST_ARE_SELECTED', {
						numberOfItemsSelected : newValue.length
					});
				} else {
					$rootScope.$broadcast('NOT_ALL_ROWS_ARE_SELECTED', {
						numberOfItemsSelected : newValue.length
					});
					genericDataTableInfoStore.togglePageSelectionState(allRowsAreSelected);
				}
			}, true);

			$rootScope.$on(genericDataTableDataService.events.LIST_ITEMS_UPDATED, function(event, data) {
				scope.listItems 			= angular.copy(data.items);
				scope.columns 				= createColumnsWithData(scope.listConfig, scope.listItems, true);
				scope.selectedRowsIds = [];
				allRowsAreSelected 		= false;

				$timeout(function() {
					updateAppearanceOfTable();
					document.getElementById('box').addEventListener('scroll',scrollFunction);
				});
			});

			$rootScope.$on(genericDataTableDataService.events.TABLE_HEADER_CONFIGURATION_UPDATED, function(event, data) {
				scope.listConfig = data;
				scope.columns 	 = createColumnsWithData(scope.listConfig, scope.listItems, false);

				$timeout(function() {
					updateAppearanceOfTable();
					scrollFunction();
				});
			});

			$rootScope.$on("DESELECT_ENTIRE_LIST", function(event, data) {
				scope.selectAllRows(null,data.state);
			});

			$rootScope.$on("STARTED_GETTING_NEW_ITEMS", function(event, data) {
				scope.listItems = null;
				scope.columns 	= null;
			})

			$rootScope.$on("SCROLLING_HEADER", function() {
				$timeout(function() {
					scrollFunction();
				});
			});

			scope.resetFilters = function() {
				$rootScope.$broadcast('RESET_SEARCH_QUERY');
				$rootScope.$broadcast('RESET_FILTERS');
				$rootScope.$broadcast('RESET_DAYS_SELECTOR');
				$rootScope.$broadcast('RESET_DATA');
			}

			scope.openQR = function (value) {
				var data = {};
				var templateModal = "<" + genericDataTableDataService.endpointType + "-qr-code-modal></" + genericDataTableDataService.endpointType + "-qr-code-modal>";
				if ($injector.has(genericDataTableDataService.endpointType+'QrCodeModalDirective')) {
					if(genericDataTableDataService.endpointType == 'attendee'){
	          data.qrCode = unescape(encodeURIComponent(value));
					}
					ngDialog.open({
						plain: true,
						template: templateModal,
						className: '',
						data: data
					});
	      }
    	}

			$rootScope.$on('SHOW_REMOVED_ITEMS_STATE_CHANGED', function($event, data) {
				scope.showRemovedState = data.state;
			});

			function scrollFunction() {
				var scrollTop 	 = document.getElementById('box').scrollTop;
				var rect         = document.getElementById('box').getBoundingClientRect();
				var headerHeight = rect.y + "px";
				var $columns		 = $('.list-column');
				var elmnt        = document.querySelector(".columns-header");
				if (elmnt) {
					elmnt.style      = "";


        if (scrollTop == 0)
        {
					for (var i = 0; i < $columns.length; i++) {
						$($columns[i]).find('.list-column-name')[0].style = "";
						if($($columns[i]).find('.list-column-value')[0]) {
							$($columns[i]).find('.list-column-value')[0].style = "";
						}
					}
					elmnt.style = "";
        } else {
					elmnt.style = "position: fixed;z-index:2;";
					elmnt.style.top = headerHeight;
					var width = 0;
					for (var i = 0; i < $columns.length; i++) {
						$($columns[i]).find('.list-column-name')[0].style       = "position: fixed;z-index:4;top:"+headerHeight+";";
						$($columns[i]).find('.list-column-name')[0].style.width = $columns[i].style.width;
						$($columns[i]).find('.list-column-value')[0].style      = "margin-top:60px;";
						width  += $columns[i].getBoundingClientRect().width;
					}
					elmnt.style.width = width + "px" ;
				}

				var ScrollLeft = document.getElementById('box').scrollLeft;
		    if (ScrollLeft > 0 && elmnt.style.position == "fixed")
		    {
					var width = 0;
					for (var i = 0; i < $columns.length; i++) {
						$($columns[i]).find('.list-column-name')[0].style.left  = "calc(" + rect.x + "px - " + ScrollLeft + "px + " + width+"px)";
						$($columns[i]).find('.list-column-name')[0].style.width = $columns[i].style.width;
						width  += $columns[i].getBoundingClientRect().width;
					}

		        elmnt.style.left = "calc(" + rect.x + "px - " + ScrollLeft + "px + " + width+"px)" ;
		    }
		    //elmnt.style.left = "-" + ScrollLeft + "px";
			}
		}
    }
  }
})();
