(function(){
	'use strict'

	var directiveId 		= 'copyLinkModal';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		copyLinkModalDirective
	];

	app.directive(directiveId, depdenciesArray);

  function copyLinkModalDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericShare/_components/copyLinkModal/copyLinkModalView.html',
		};
    function link (scope) {
			scope.response = window.location.href.split("?")[0] + "?share="+scope.ngDialogData._data;

			scope.copyShareLink = function() {
					recCopy();
      }

			// recursively using setTimeout to wait for response
			var recCopy = function (){
			    if(scope.response){
			        copy(scope.response);
			        return;
			    }
			    else {
			        setTimeout(recCopy,2000); // for e.g. 2ms
			    }
			}

			function copy(value) {
			    var tempInput = document.getElementById("share_link");
			    tempInput.select();
			    try {
	          var successful = document.execCommand('copy',false,null);
	          var msg = successful ? 'successful' : 'unsuccessful';
	          scope.message = 'Shareable link was copied';
	        } catch (err) {
	            scope.message = 'Oops, unable to copy to clipboard';
	        }
			}
    }
  }
})();
