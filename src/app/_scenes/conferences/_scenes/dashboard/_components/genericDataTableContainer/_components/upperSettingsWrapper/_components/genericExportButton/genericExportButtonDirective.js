(function(){
	'use strict'

	var directiveId 		= 'genericExportButton';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'genericDataTableInfoStore',
		genericExportButtonDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericExportButtonDirective(genericDataTableInfoStore) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericExportButton/genericExportButtonView.html',
		};
    function link (scope) {
			var allDataIsSelected = false;
			scope.export = function() {
				if (allDataIsSelected) {
					exportAllData();
				} else {
					exportSelectedData();
				}
			}
    }
  }
})();
