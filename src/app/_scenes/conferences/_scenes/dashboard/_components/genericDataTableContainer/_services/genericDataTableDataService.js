(function(){
	'use strict';

  var serviceId = 'genericDataTableDataService';
	var dependenciesArray = [
		'$q',
		'$rootScope',
		'requestFactory',
		'$stateParams',
		'sharableURLInterceptor',
		genericDataTableDataService
	];
	angular.module('app').service(serviceId, dependenciesArray);

	function genericDataTableDataService($q, $rootScope, requestFactory, $stateParams, sharableURLInterceptor) {
    var service 												= {};

		service.firstLoad 									= true;
		service.events											= {
			LIST_ITEMS_UPDATED								 	 : "LIST_ITEMS_UPDATED",
			TABLE_HEADER_CONFIGURATION_UPDATED 	 : "TABLE_HEADER_CONFIGURATION_UPDATED",
			PAGINATION_INTERACTOR_CONFIG_UPDATED : "PAGINATION_INTERACTOR_CONFIG_UPDATED"
		};

		service.endpointType 				= "";
		service.nextToken 					= null;
		service.previousToken 			= null;
		service.currentPage 				= 1;
		service.totalNumberOfItems	= 1;
		service.tableDensity        = null;
		service.sortOrder 					= "";
		service.filtered            = "";
		service.dateFilter          = "";

		if ($rootScope.conference) {
			service.currentConferenceId = $rootScope.conference.conference_id;
		} else {
			service.currentConferenceId = null;
		}

		service.sharedNextToken 		= null;
		service.sharedPreviousToken = null;

		if (service.sharedParams && service.sharedParams.currentlyUsedNextToken) {
			service.sharedNextToken = service.sharedParams.currentlyUsedNextToken;
			sharableURLInterceptor.updateVariable('currentlyUsedNextToken', service.sharedNextToken);
			sharableURLInterceptor.updateVariable('currentlyUsedPreviousToken', null);
		}

		if (service.sharedParams && service.sharedParams.currentlyUsedPreviousToken) {
			service.sharedPreviousToken = service.sharedParams.currentlyUsedPreviousToken;
			sharableURLInterceptor.updateVariable('currentlyUsedNextToken', null);
			sharableURLInterceptor.updateVariable('currentlyUsedPreviousToken', service.sharedPreviousToken);
		}

		if (service.sharedParams && service.sharedParams.page) {
			service.currentPage 	= service.sharedParams.page;
			sharableURLInterceptor.updateVariable('page', service.currentPage);
		}

		service.setEndpointType = function(type) {
			if ($rootScope.conference) {
				service.currentConferenceId = $rootScope.conference.conference_id;
			} else {
				service.currentConferenceId = null;
			}

			service.endpointType = type;
		};

		service.getData = function (showRemovedItems, searchBoxQuery, dateFilter, filter, tableDensity, sortOrder, next, previous) {
			$rootScope.$broadcast('STARTED_GETTING_NEW_ITEMS', {});
			var deferred 				 = $q.defer();
			if (next) {
				sharableURLInterceptor.updateVariable('currentlyUsedNextToken', service.nextToken);
				sharableURLInterceptor.updateVariable('currentlyUsedPreviousToken', null);
				service.sharedNextToken = null;
			} else if (previous) {
				service.sharedPreviousToken = null;
				sharableURLInterceptor.updateVariable('currentlyUsedNextToken', null);
				sharableURLInterceptor.updateVariable('currentlyUsedPreviousToken', service.previousToken);
			}
			if (service.currentConferenceId) {
				var url 		 			 	 = "api/v1.1/conferences/" + service.currentConferenceId + "/" + service.endpointType + "s/";
			} else {
				var url 						 = "api/v1.1/" + service.endpointType + "/";
			}
			var showRemovedItemsUrl = "";
			var searchBoxUrl  	 		= "";
			var dateFilterUrl 	 		= "";
			var filterUrl 			 		= "";
			var tableDensityUrl  		= "";
			var sortOrderUrl  	 		= "";

			var nextTokenUrl 		 		= "";
			var previousTokenUrl 		= "";

			if (showRemovedItems) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage = 1;
				}
				showRemovedItemsUrl = "?show_removed=true";
			}

			if (searchBoxQuery) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage = 1;
				}
				if (!showRemovedItemsUrl) {
					searchBoxUrl 	= "?search_query=" + searchBoxQuery;
				} else {
					searchBoxUrl 	= "&search_query=" + searchBoxQuery;
				}
			}

			if (dateFilter) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage = service.dateFilter != dateFilter ? 1 : service.currentPage;
				}
				service.dateFilter = dateFilter;

				if (typeof dateFilter === 'string') {
					if (!showRemovedItemsUrl && !searchBoxQuery) {
						dateFilterUrl = "?date_filter=" + dateFilter;
					} else {
						dateFilterUrl = "&date_filter=" + dateFilter;
					}
				} else if (typeof dateFilter === 'object') {
					if (!showRemovedItemsUrl && !searchBoxQuery) {
						dateFilterUrl += "?starts_at=" + dateFilter.from + "&ends_at=" + dateFilter.to;
					} else {
						dateFilterUrl += "&starts_at=" + dateFilter.from + "&ends_at=" + dateFilter.to;
					}
				}
			}

			if (filter) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage = service.filtered != filter ? 1 : service.currentPage;
				}
				service.filtered = filter;

				if (!showRemovedItemsUrl && !dateFilter && !searchBoxQuery) {
					filterUrl = "?filter=" + encodeURI(JSON.stringify(filter));
				} else {
					filterUrl = "&filter=" + encodeURI(JSON.stringify(filter));
				}
			}

			if (tableDensity) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage  = service.tableDensity != tableDensity ? 1 : service.currentPage;
				}
				service.tableDensity = tableDensity;

				if (!showRemovedItemsUrl && !dateFilter && !searchBoxQuery && !filter) {
					tableDensityUrl = "?table_density=" + tableDensity.name;
				} else {
					tableDensityUrl = "&table_density=" + tableDensity.name;
				}
			}

			if (sortOrder) {
				if (!service.sharedNextToken && !service.sharedPreviousToken) {
					service.currentPage = service.sortOrder != sortOrder ? 1 : service.currentPage;
				}
				service.sortOrder = sortOrder;

				if (!showRemovedItemsUrl && !dateFilter && !searchBoxQuery && !filter && !tableDensity) {
					sortOrderUrl = "?sort_by=" + sortOrder;
				} else {
					sortOrderUrl = "&sort_by=" + sortOrder;
				}
			}

			if (next) {
				if (!showRemovedItemsUrl && !searchBoxQuery && !dateFilter && !filter && !tableDensity) {
					nextTokenUrl 	= "?next=" + service.nextToken;
				} else {
					nextTokenUrl 	= "&next=" + service.nextToken;
				}
			} else if (previous) {
				if (!showRemovedItemsUrl && !searchBoxQuery && !dateFilter && !filter && !tableDensity) {
					previousTokenUrl = "?previous=" + service.previousToken;
				} else {
					previousTokenUrl = "&previous=" + service.previousToken;
				}
			}

			if (!next && !previous && service.sharedNextToken) {
				if (!showRemovedItemsUrl && !searchBoxQuery && !dateFilter && !filter && !tableDensity) {
					nextTokenUrl 	= "?next=" + service.sharedNextToken;
				} else {
					nextTokenUrl 	= "&next=" + service.sharedNextToken;
				}
			} else if (!previous && !next && service.sharedPreviousToken) {
				if (!showRemovedItemsUrl && !searchBoxQuery && !dateFilter && !filter && !tableDensity) {
					previousTokenUrl = "?previous=" + service.sharedPreviousToken;
				} else {
					previousTokenUrl = "&previous=" + service.sharedPreviousToken;
				}
			}

			url = url + showRemovedItemsUrl + searchBoxUrl + dateFilterUrl + filterUrl + tableDensityUrl +  sortOrderUrl + nextTokenUrl + previousTokenUrl;

			if (service.sharedParams) {
				var allComponentsStatus = sharableURLInterceptor.usedSharedInfo;
				var allComponentsAreDone = true;
				angular.forEach(allComponentsStatus, function(value, key) {
					allComponentsAreDone = (allComponentsAreDone && value);
				})
			}

			if (!service.sharedParams || (service.sharedParams && allComponentsAreDone)) {
				requestFactory.get(url, {}, true, false).then(function(response) {
					service.nextToken		 			 = response.data.next;
					service.previousToken			 = response.data.previous;
					service.totalNumberOfPages = Math.ceil(response.data.total_nr / service.tableDensity.page_size);
					service.totalNumberOfItems = response.data.total_nr;
					if (next) {
						service.currentPage++;
						sharableURLInterceptor.updateVariable('page', service.currentPage);
					} else if (previous && service.currentPage > 1) {
						service.currentPage--;
						sharableURLInterceptor.updateVariable('page', service.currentPage);
					}


					$rootScope.$broadcast(service.events.LIST_ITEMS_UPDATED, response.data);
					$rootScope.$broadcast(service.events.PAGINATION_INTERACTOR_CONFIG_UPDATED, {});

					service.firstLoad = false;

					deferred.resolve(response.data);
				}, function(error) {
					deferred.reject(error);
				});
			}

			return deferred.promise;
		};

		service.getSuggestions = function(query) {
			var deferred = $q.defer();
			if (service.currentConferenceId) {
				var url 		 = "api/v1.1/conferences/" + service.currentConferenceId + "/" + service.endpointType + "s/suggestions/?search_query=" + query;
 			} else {
				var url 		 = "api/v1.1/" + service.endpointType + "/suggestions/?search_query=" + query;
			}

			requestFactory.get(url, {}, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.getLongRunningProcesses = function() {
			var deferred = $q.defer();
			if  (service.currentConferenceId) {
				var url			 = "api/v1.1/conferences/" + service.currentConferenceId + "/" + service.endpointType + "s/processes";
			} else {
				var url 		 = "api/v1.1/" + service.endpointType + "/processes";
			}

			requestFactory.get(url, {}, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.archiveProcess = function(processId) {
			var deferred = $q.defer();
			if (service.currentConferenceId) {
				var url			 = "api/v1.1/conferences/" + service.currentConferenceId + "/" + service.endpointType + "s/processes/" + processId;
			} else {
				var url 		 = "api/v1.1/" + service.endpointType + "/processes/" + processId;
			}

			requestFactory.remove(url, {}, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;

		}

		service.setDefaultTableDensity = function(density) {
			service.tableDensity = density;
		}

		service.setSharedParams = function(sharedParams) {
			service.sharedParams = sharedParams;
		}

		service.purgePaginationInfo = function() {
			var deferred = $q.defer();
			service.nextToken 				 = null;
			service.previousToken 		 = null;
			service.currentPage 			 = 1;
			service.totalNumberOfItems = 1;
			deferred.resolve();
			return deferred.promise;
		}

    return service;
  };
})();
