(function(){
	'use strict'

	var directiveId 		= 'genericPageInteractor';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'genericDataTableDataService',
		'genericDataTableInfoStore',
		genericPageInteractorDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericPageInteractorDirective($q, $rootScope, genericDataTableDataService, genericDataTableInfoStore) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericPageInteractor/genericPageInteractorView.html',
		};
    function link (scope) {
			initVars();


			scope.next = function() {
				if(scope.nextTokenExists) {
					genericDataTableInfoStore.requireNextPage().then(function(response) {
					}, function(error) {
					});
				}
			}

			scope.previous = function() {
				if(scope.previousTokenExists) {
					genericDataTableInfoStore.requirePreviousPage().then(function(response) {
					}, function(error) {
					});
				}
			}

			$rootScope.$on(genericDataTableDataService.events.PAGINATION_INTERACTOR_CONFIG_UPDATED, function(event, data) {
				initVars();
			})

			function initVars() {
				scope.nextTokenExists 		 = genericDataTableDataService.nextToken ? true : false;
				scope.previousTokenExists  = genericDataTableDataService.previousToken ? true : false;

				scope.currentPage 				 = genericDataTableDataService.currentPage;
				scope.totalPages 					 = genericDataTableDataService.totalNumberOfPages;
				scope.numberOfItemsPerPage = genericDataTableDataService.tableDensity.page_size;
				scope.totalNumberOfItems 	 = genericDataTableDataService.totalNumberOfItems;

				scope.startIndex = (scope.currentPage - 1) * scope.numberOfItemsPerPage + 1;
				scope.endIndex	 = scope.currentPage * scope.numberOfItemsPerPage;

				if (scope.endIndex > scope.totalNumberOfItems) {
					scope.endIndex = scope.totalNumberOfItems;
				}

				if (scope.startIndex > scope.totalNumberOfItems) {
					scope.startIndex = scope.totalNumberOfItems;
				}
			}
    }
  }
})();
