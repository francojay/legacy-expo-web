(function(){
	'use strict'

	var directiveId 		= 'genericFilterListCreator';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'filtersConvertorService',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'sharableURLInterceptor',
		'$timeout',
		genericFilterListCreatorDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericFilterListCreatorDirective($q, $rootScope, filtersConvertorService, genericDataTableInfoStore, genericDataTableDataService, sharableURLInterceptor, $timeout) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericFilterListCreator/genericFilterListCreatorView.html',
			scope : {
				fields : "=",
				shared : "="
			}
		};
    function link (scope) {
			var currentConference = $rootScope.conference;

			scope.allFilterableFields			 = defineFilterableFields(scope.fields);
			scope.originalFilterableFields = defineFilterableFields(scope.fields);
			scope.alreadyUsedFields 			 = [];
			scope.filters 								 = [];
			scope.filterApplied 					 = false;
			scope.filterCreatorOpened			 = false;
			scope.showOverlay							 = false;

			var filtersForApi 						 = null;
			var lastValueOfPickerState 		 = false;
			var defaultFiltersState        = {
				filters : scope.filters
			};
			var hasBeenChanged						 = false;

			$rootScope.filterListMomentPicker  = [];

			initWatchers();

			if (scope.shared) {
				sharableURLInterceptor.updateUsedInfo(directiveId, true);
			}

			if (scope.shared && scope.shared.completeFilters) {
				scope.filters					  = angular.copy(scope.shared.completeFilters);
				for (var i = 0; i < scope.allFilterableFields.length; i++) {
					for (var j= 0; j< scope.filters.length; j++) {
						if (scope.allFilterableFields[i].name === scope.filters[j].database_name) {
							var usedFilter = scope.allFilterableFields.splice(i, 1)[0];
							scope.alreadyUsedFields.push(usedFilter);
						}
					}
				}

				for (var i = 0; i < scope.filters.length; i++) {
					if (scope.filters[i].base_type === 'datetime') {
						scope.filters[i].value = moment(scope.filters[i].value);
					}
				}

				filtersForApi = filtersConvertorService.convert(scope.filters);

				scope.filtersApplied = scope.shared.filtersApplied;

				if (scope.filtersApplied) {
					scope.filterCreatorOpened = !scope.filterCreatorOpened;
					scope.filterApplied = true;
					genericDataTableInfoStore.updateFilter(filtersForApi);
					sharableURLInterceptor.updateVariable('filtersApplied', scope.filterApplied);
				}
				sharableURLInterceptor.updateVariable('completeFilters', scope.filters);
			}

			copyCurrentFiltersState()

			scope.addEmptyFilter = function(event) {
				event.stopPropagation();
				scope.filters.push(generateDefaultFilter(scope.allFilterableFields[0]));
				var usedFilter = scope.allFilterableFields.splice(0, 1)[0];
				scope.alreadyUsedFields.push(usedFilter);
				hasBeenChanged = true;
			}

			scope.clearAllFilters = function() {
				scope.filters 						= [];
				scope.allFilterableFields = JSON.parse(JSON.stringify(scope.originalFilterableFields));
				scope.filterApplied 			= false;
				scope.filterCreatorOpened = false ;

				copyCurrentFiltersState();
				sharableURLInterceptor.updateVariable('filtersApplied', scope.filterApplied);
				genericDataTableInfoStore.updateFilter("").then(function(response) {
				}, function(error) {
				})
			}

			scope.toggleFilterSelector = function(event, filter, state) {
				event.stopPropagation();
				if(scope.allFilterableFields.length > 0) {
					filter.filterSelectorIsOpened = !state;
					scope.showOverlay  = !scope.showOverlay;
				}
			}

			scope.overwriteFilter = function(filter, field, $event) {
				$event.stopPropagation();
				$event.preventDefault();

				for (var i = 0; i < scope.alreadyUsedFields.length; i++) {
					if (scope.alreadyUsedFields[i].name === filter.database_name) {
						var previousUsedFilter = scope.alreadyUsedFields.splice(i, 1)[0];
						scope.allFilterableFields.push(previousUsedFilter);
					}
				}

				filter.table 			 	 					= field.label;
				filter.type 				 					= field.type;
				filter.base_type 							= field.base_type;
				filter.field_type 						= field.field_type;
				filter.database_name 					= field.name;
				filter.operands			 					= generateOperandsForFilterType(field.base_type);
				filter.selectedOperand				= generateOperandsForFilterType(field.base_type)[0];
				filter.value 									= generateDefaultValueForFilterType(field.base_type);
				filter.filterSelectorIsOpened = false;
				filter.operandsListOpened 		= false;

				if (field.field_type === 'enum') {
					filter.options = [];
					for (var i = 0; i < field.options.length; i++) {
						var option = {
							name: field.options[i],
							checked : false
						}
						filter.options.push(option);
					}
				}

				for (var i = 0; i < scope.allFilterableFields.length; i++) {
					if (scope.allFilterableFields[i].name === field.name) {
						var usedFilter = scope.allFilterableFields.splice(i, 1)[0];
						scope.alreadyUsedFields.push(usedFilter);
					}
				}
				scope.showOverlay  = !scope.showOverlay;
			}

			scope.selectOption = function(filter, option, $event) {
				$event.stopPropagation();
				$event.preventDefault();
				option.checked = !option.checked;
				if(option.checked){
					filter.value.push(option.name);
				} else {
					var index = filter.value.indexOf(option.name);
					if (index !== -1) filter.value.splice(index, 1);
				}

			}

			scope.openOperandsList = function(event, filter) {
				event.stopPropagation();
				filter.operandsListOpened = !filter.operandsListOpened;
				scope.showOverlay  = !scope.showOverlay;

			};

			scope.selectOperand = function(filter, operand, $event) {
				$event.stopPropagation();
				$event.preventDefault();

				filter.selectedOperand 		= operand;
				scope.showOverlay  = !scope.showOverlay;
				filter.operandsListOpened = !filter.operandsListOpened;

			}

			scope.openFilterCreator = function(event,state) {
				if(!state && !hasBeenChanged) {
					copyCurrentFiltersState();
				}
				else if(state) {
					hasBeenChanged = false;
					scope.filters  = defaultFiltersState.filters ;
					scope.allFilterableFields = JSON.parse(JSON.stringify(scope.originalFilterableFields));
					scope.alreadyUsedFields   = [];
					for(var i = 0; i < scope.allFilterableFields.length; i++ ){
						for(var j = 0; j < scope.filters.length; j++ ){
							if (scope.allFilterableFields[i].label === scope.filters[j].table) {
								var usedFilter = scope.allFilterableFields.splice(i, 1)[0];
								scope.alreadyUsedFields.push(usedFilter);
							}
						}
					}

				}
				scope.showOverlay  = false;
				scope.filterCreatorOpened = !state;

			}

			scope.applyFilters = function() {
				genericDataTableDataService.sharedNextToken 	 	= null;
				genericDataTableDataService.sharedPreviousToken = null;
				scope.filterCreatorOpened = !scope.filterCreatorOpened;
				scope.filterApplied = true;
				genericDataTableInfoStore.updateFilter(filtersForApi);
				sharableURLInterceptor.updateVariable('filtersApplied', scope.filterApplied);
				copyCurrentFiltersState();
			}

			scope.hideDropdown = function() {
				if(scope.filterCreatorOpened) {
					if ($rootScope.filterListMomentPicker.length == 0 && (lastValueOfPickerState.length == 0 || !lastValueOfPickerState)) {
						scope.filterCreatorOpened = false;
						scope.filters 						= defaultFiltersState.filters;
						scope.allFilterableFields = JSON.parse(JSON.stringify(scope.originalFilterableFields));
						scope.alreadyUsedFields   = [];
						for(var i = 0; i < scope.allFilterableFields.length; i++ ){
							for(var j = 0; j < scope.filters.length; j++ ){
								if (scope.allFilterableFields[i].label === scope.filters[j].table) {
									var usedFilter = scope.allFilterableFields.splice(i, 1)[0];
									scope.alreadyUsedFields.push(usedFilter);
								}
							}
						}
						hasBeenChanged						= false;
					}
					else if ($rootScope.filterListMomentPicker.length > 0 && lastValueOfPickerState.length > 0) {
						var areAllClosed = true;
						for (var i = 0; i < $rootScope.filterListMomentPicker.length; i++) {
								if($rootScope.filterListMomentPicker[i] == true) {
									areAllClosed = false;
								}
						}
						if(areAllClosed) {
							$rootScope.filterListMomentPicker = [];
						}
					}
					lastValueOfPickerState = $rootScope.filterListMomentPicker;
				}
			}

			scope.stopEvent = function($event) {
				$event.stopPropagation();
			}

			function generateDefaultFilter(field) {
				var filter = {
					table 	 				 			 : field.label,
					field_type 		 				 : field.field_type,
					base_type							 : field.base_type,
					database_name 				 : field.name,
					operands 			 				 : generateOperandsForFilterType(field.base_type),
					selectedOperand 			 : generateOperandsForFilterType(field.base_type)[0],
					value 								 : generateDefaultValueForFilterType(field.base_type),
					filterSelectorIsOpened : false,
					operandsListOpened		 : false
				};

				if (field.field_type === 'enum') {
					filter.options = [];
					for (var i = 0; i < field.options.length; i++) {
						var option = {
							name		: field.options[i],
							checked : false
						}
						filter.options.push(option);
					}
				}

				return filter;
			}

			function defineFilterableFields(fields) {
				var filterable = [];

				for (var i = 0; i < fields.length; i++) {
					if (fields[i].can_filter_by) {
						filterable.push(fields[i]);
					}
				};

				return filterable;
			}

			function generateOperandsForFilterType(fieldType) {
				var operandsForEnum 	= [];

				var operandsForString = [
					{
						label : "EQUALS",
						value : "eq"
					},
					{
						label : "CONTAINS",
						value : "ct"
					},
					{
						label : "STARTS WITH",
						value : "sw"
					},
					{
						label : "ENDS WITH",
						value : "ew"
					}
				];

				var operandsForNumbers = [
					{
						label : "EQUALS",
						value : "eq"
					},
					{
						label : "GREATER THAN",
						value : "gt"
					},
					{
						label : "GREATER OR EQUAL",
						value : "gte"
					},
					{
						label : "LESS THAN",
						value : "lt"
					},
					{
						label : "LESS OR EQUAL",
						value : "lte"
					}
				]

				var operandsForDates = [
					{
						label : "GREATER THAN",
						value : "gt"
					},
					{
						label : "GREATER OR EQUAL",
						value : "gte"
					},
					{
						label : "LESS THAN",
						value : "lt"
					},
					{
						label : "LESS OR EQUAL",
						value : "lte"
					},
					{
						label : "EQUALS",
						value : "eq"
					}
				];

				if (fieldType === 'string') {
					return operandsForString;
				} else if (fieldType === 'datetime') {
					return operandsForDates;
				} else if (fieldType === 'float' || fieldType === 'integer') {
					return operandsForNumbers;
				} else if (fieldType === 'enum') {
					return operandsForEnum;
				}
			}

			function generateDefaultValueForFilterType(fieldType) {
				var value = null;
				if (fieldType === 'string') {
					value = "";
				} else if (fieldType === 'float' || fieldType === 'int') {
					value = 0;
				} else if (fieldType === 'datetime') {
					if (currentConference) {
						value = moment().utc().tz(currentConference.timezone_name);
					} else {
						value = moment().utc()
					}
				} else if (fieldType === 'enum') {
					value = [];
				}
				return value;
 			}

			function initWatchers() {
				scope.$watch('filters', function(newValue, oldValue) {
					if (newValue !== oldValue && newValue !== undefined) {
						sharableURLInterceptor.updateVariable('completeFilters', newValue);
						filtersForApi = filtersConvertorService.convert(newValue);
						sharableURLInterceptor.updateVariable('filters', filtersForApi);
					}
				}, true);


				scope.$watch('showOverlay', function(newValue, oldValue) {
					$timeout(function() {
						var modal = angular.element(document.querySelectorAll('.filter-creator-modal'));
						var overlay = angular.element(document.querySelectorAll('.filter-overlay'));
						if(scope.showOverlay) {
							overlay[0].style.height = modal[0].scrollHeight + 'px';
						}
						else{
							var overlay = angular.element(document.querySelectorAll('.nodisplay'));
							if(overlay.length > 0) {
								overlay[0].style.height = '100%';
							}

						}
          },200);

				}, true);

				scope.$watch('filterCreatorOpened', function(newValue, oldValue) {
					$rootScope.$broadcast("TOGGLE_CUSTOM_SCROLLBAR", {
						"filterCreatorIsOpened" : newValue
					});
				});

				$rootScope.$on('RESET_FILTERS', function(event) {
					scope.filters = [];
					scope.allFilterableFields = JSON.parse(JSON.stringify(scope.originalFilterableFields));
					scope.filterApplied = false;
					sharableURLInterceptor.updateVariable('filtersApplied', scope.filterApplied);
					copyCurrentFiltersState();
					hasBeenChanged = false;
				});
			}

			function copyCurrentFiltersState() {
					defaultFiltersState.filters = angular.copy(scope.filters);
			}
    }
  }
})();
