(function(){
	'use strict';

  var serviceId = 'filtersConvertorService';
	var dependenciesArray = [
		'$q',
		'requestFactory',
		'genericDataTableDataService',
		filtersConvertorService
	];
	angular.module('app').service(serviceId, filtersConvertorService);

	function filtersConvertorService($q, requestFactory, genericDataTableDataService) {
    var service = {};

		service.convert = function(filters) {
			var filterObject = {};


			/**
			filter is
			"database_field_name" : {
			  op: "operation_name",
				v: "value",
				v1: "first_value", //if there exists between(bt) operand
				v2: "last_value" //if there exists between(bt) operand
			}
			**/

			for (var i = 0; i < filters.length; i++) {
				filterObject[filters[i].database_name] = {
					op: service._computeOperand(filters[i].base_type, filters[i].selectedOperand),
					v: service._computeValue(filters[i].base_type, filters[i].value)
				}
			};
			return filterObject;
		}

		service._computeValue = function(filterType, value) {
			if (filterType === 'enum') {
				return value;
			} else if (filterType === 'datetime') {
				return value.utc().format();
			} else if (filterType === 'float' || filterType === 'integer') {
				return value || 0;
			} else if (filterType === 'string') {
				return value;
			}
		}

		service._computeOperand = function(filterType, operand) {
			if (filterType === 'enum') {
				return "in"
			} else {
				return operand.value
			}
		}

    return {
			convert: service.convert
		};
  }
})();
