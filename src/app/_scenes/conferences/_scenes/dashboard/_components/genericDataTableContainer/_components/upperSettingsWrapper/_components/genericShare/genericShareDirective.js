(function(){
	'use strict'

	var directiveId 		= 'genericShare';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'shareURLService',
		'sharableURLInterceptor',
		'ngDialog',
		genericShareDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericShareDirective($q, $rootScope, shareURLService, sharableURLInterceptor, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericShare/genericShareView.html',
		};
    function link (scope) {
			scope.shareLinkIsAvailable = false;
			scope.shareModalOpened     = false;
			scope.linkWasCopied				 = false;

      scope.getShareLink = function() {
				if(scope.shareLinkIsAvailable && !scope.shareModalOpened) {
	        shareURLService.getShareLink().then(function(response){
					  scope.response = window.location.href.split("?")[0] + "?share="+response.data.code;
						scope.shareModalOpened = true;
	        },
					 function(error) {
							console.log(error);
					});
				}
				else{
					scope.shareModalOpened = false;
				}
      }

			$rootScope.$on('SHARE_LINK_IS_AVAILABLE',function(){
				scope.shareLinkIsAvailable = sharableURLInterceptor.sharableLink ? true: false;
			});

			scope.hideDropdown = function() {
				scope.shareModalOpened = false;
				scope.linkWasCopied    = false;
			}

			scope.copyShareLink = function() {
				var tempInput = document.getElementById("share_link");
				tempInput.select();
				try {
					var successful = document.execCommand('copy',false,null);
					scope.linkWasCopied = successful ? true : false;
				} catch (err) {
						console.log('Oops, unable to copy to clipboard');
				}
      }

			scope.closeMessage = function() {
				scope.linkWasCopied = false;
			}

    }
  }
})();
