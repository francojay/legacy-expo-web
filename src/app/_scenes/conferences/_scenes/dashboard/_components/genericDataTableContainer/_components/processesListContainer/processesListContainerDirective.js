(function(){
	'use strict'

	var directiveId 		= 'processesListContainer';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'$timeout',
		processesListContainerDirective
	];

	app.directive(directiveId, depdenciesArray);

  function processesListContainerDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, $timeout) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/processesListContainer/processesListContainerView.html',
		};
    function link (scope) {
			scope.processes = [];
			getProcesses();

			setInterval(function() {
				getProcesses()
			}, 30000);


			scope.userActionResult = function(process) {
				var anchor = angular.element('<a/>');
        anchor.css({display: 'none'}); // Make sure it's not visible
        angular.element(document.body).append(anchor); // Attach to document


        anchor.attr({
            href: process.result_url,
            target: '_blank',
        })[0].click();

        anchor.remove();

				genericDataTableDataService.archiveProcess(process.id).then(function(response) {
					for (var i = 0; i < scope.processes.length; i++) {
						if (scope.processes[i].id === process.id) {
							scope.processes[i].status = 'archived';
							$timeout(function() {
								scope.processes.splice(i, 1);
							}, 200);
							break
						}
					}
				}, function(error) {
					console.log(error);
				});
			}

			scope.archiveProcess = function(process) {
				genericDataTableDataService.archiveProcess(process.id).then(function(response) {
					for (var i = 0; i < scope.processes.length; i++) {
						if (scope.processes[i].id === process.id) {
							scope.processes[i].status = 'archived';
							$timeout(function() {
								scope.processes.splice(i, 1);
								$rootScope.$broadcast("SCROLLING_HEADER");
							}, 200);
							break
						}
					}
				}, function(error) {
					console.log(error);
				});
			}

			function getProcesses() {
				genericDataTableDataService.getLongRunningProcesses().then(function(response) {
					scope.processes = angular.copy(response.data.items);
				}, function(error) {
					console.log(error);
				});
			}

			$rootScope.$on("NEW_PROCESS_HAS_BEEN_CREATED", function(event, data) {
				scope.processes.unshift(data);
			});
    }
  }
})();
