(function(){
	'use strict';

  var serviceId = 'shareURLService';
	var dependenciesArray = [
		'$q',
		'requestFactory',
		'sharableURLInterceptor',
    'genericDataTableDataService',
		shareURLService
	];
	angular.module('app').service(serviceId, dependenciesArray);

	function shareURLService($q, requestFactory, sharableURLInterceptor, genericDataTableDataService) {
    var service = {};

    service.getShareLink = function() {
			var deferred = $q.defer();
			if (genericDataTableDataService.currentConferenceId) {
				var url 	 = "api/v1.1/conferences/" + genericDataTableDataService.currentConferenceId + "/" + genericDataTableDataService.endpointType + "s/share";
			} else {
				var url 	 = "api/v1.1/" + genericDataTableDataService.endpointType + "/share"
			}
			var data = {
				share_link_data : sharableURLInterceptor.sharableLink
			}

			requestFactory.post(url, data).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.getShareParametersFromLink = function(shareCode) {
			var deferred = $q.defer();

			if (genericDataTableDataService.currentConferenceId) {
				var url 		 = "api/v1.1/conferences/" + genericDataTableDataService.currentConferenceId + "/" + genericDataTableDataService.endpointType + "s/share/?code=" + shareCode;
			} else {
				var url 	 	 = "api/v1.1/" + genericDataTableDataService.endpointType + "/share?code=" + shareCode;
			}

			requestFactory.get(url, {}, false, false).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

    return service;
  }
})();
