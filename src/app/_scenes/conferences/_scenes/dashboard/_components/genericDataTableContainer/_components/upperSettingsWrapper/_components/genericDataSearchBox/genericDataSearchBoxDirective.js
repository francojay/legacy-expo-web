(function(){
	'use strict'

	var directiveId 		= 'genericDataSearchBox';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'sharableURLInterceptor',
		'genericDataTableDataService',
		genericDataSearchBoxDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericDataSearchBoxDirective($rootScope, genericDataTableInfoStore, sharableURLInterceptor, genericDataTableDataService) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericDataSearchBox/genericDataSearchBoxView.html',
			scope: {
				configuration : "=",
				shared 			  : "="
			}
		};
    function link (scope) {
			scope.model = {
				query : ""
			};

			scope.suggestions 						= [];
			scope.hasDoneSearch 					= false;
			scope.defaultQueryPlaceholder = scope.configuration.placeholder;

			if (scope.shared) {
				initVars();
				if (scope.shared.dataQueryPlaceholder) {
					scope.hasDoneSearch = true;
				}
				sharableURLInterceptor.updateUsedInfo(directiveId, true);
			}

			scope.handleChange = function(query) {
				scope.suggestions 	 = [];
				var customSuggestion = {
					label 								 : "<b>" + query + "</b>",
					suggested_search_query : encodeURI(query)
				};

				genericDataTableInfoStore.updateSuggestionQuery(query).then(function(response) {
					scope.suggestions.push(customSuggestion);
					scope.suggestions = scope.suggestions.concat(response.data.suggestion_list);
				}, function(error) {
				});
			};

			scope.overwriteSearchQuery = function(suggestion) {
				scope.configuration.placeholder = suggestion.label.replace(/<(?:.|\n)*?>/gm, '');
				scope.model.query 							= "";
				scope.hasDoneSearch 						= true;
				genericDataTableDataService.sharedPreviousToken = null;
				genericDataTableDataService.sharedNextToken			= null;
				genericDataTableInfoStore.updateDataQuery(suggestion.suggested_search_query, scope.configuration.placeholder).then(function(response) {
					scope.suggestions = [];
				}, function(error) {
				});
			}

			scope.overWriteSearchOnEnter = function($event) {
				if ($event.which === 13) {
					if (scope.suggestions[0]) {
						scope.overwriteSearchQuery(scope.suggestions[0]);
					}
				}
			}

			scope.resetSearchQuery = function() {
				scope.model.query 							= "";
				scope.configuration.placeholder = scope.defaultQueryPlaceholder;
				scope.suggestions 	 = [];
				if(scope.hasDoneSearch) {
					scope.hasDoneSearch = false;
					genericDataTableInfoStore.updateDataQuery(scope.model.query, "").then(function(response) {
					}, function(error) {
					});
				}
			}

			function initVars() {
				scope.configuration.placeholder = decodeURIComponent(scope.shared.dataQueryPlaceholder || scope.configuration.placeholder);
				genericDataTableInfoStore.updateDataQuery(scope.shared.dataQuery, scope.configuration.placeholder).then(function(response) {
					scope.suggestions = [];
				}, function(error) {
				});
			}

			$rootScope.$on('RESET_SEARCH_QUERY', function(event) {
				scope.model.query 							= "";
				scope.configuration.placeholder = scope.defaultQueryPlaceholder;
				scope.suggestions 	 = [];
				scope.hasDoneSearch = false;
				genericDataTableInfoStore.dataQuery = scope.model.query;
				genericDataTableInfoStore.search_query = scope.model.query;
			});
    }
  }
})();
