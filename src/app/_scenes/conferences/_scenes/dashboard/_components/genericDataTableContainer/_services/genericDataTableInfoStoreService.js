(function(){
	'use strict';

  var serviceId = 'genericDataTableInfoStore';
	var dependenciesArray = [
		'sharableURLInterceptor',
		'genericDataTableDataService',
		genericDataTableInfoStore
	];

	angular.module('app').service(serviceId, dependenciesArray);

	function genericDataTableInfoStore(sharableURLInterceptor, genericDataTableDataService) {
    var service = {};

		service.selectedColumns 					 = [];
		service.tableDensity						 	 = null;
		service.currentPage 					 		 = null;
		service.paginationToken 					 = null;
		service.suggestionQuery 					 = null;
		service.suggestionQueryPlaceholder = null;
		service.showRemovedState					 = null;
		service.filters 							 		 = "";
		service.selectedDayTypeFilter 		 = "";
		service.dataQuery 								 = "";
		service.customPeriod 							 = {
			from : null,
			to 	 : null
		}
		service.selectedItems 			 			 = [];
		service.sortOrder									 = "";
		service.activeAveryLabel 					 = null;
		service.activeAveryBadge 					 = null;

		// these are only for backend use so that we don't modify the entire frontend
		service.ids 									= [];
		service.fields								= [];
		service.date_filter 					= "";
		service.sort_by								= "";
		service.search_query 					= "";
		// these are only for backend use so that we don't modify the entire frontend

		service.nextPageWasRequested 		 = false;
		service.previousPageWasRequested = false;

		service.HAS_SEARCHBOX 							= null;
		service.HAS_DATE_SELECTOR 					= null;
		service.HAS_FILTERING 							= null;
		service.HAS_CUSTOMIZABLE_COLUMNS 		= null;
		service.HAS_ACTIONS									= null;

		service.HAS_ONLY_ONE_ACTION 				= null;
		service.HAS_NO_ACTIONS 							= null;
		service.HAS_MULTIPLE_ACTIONS 				= null;

		service.HAS_EXPORT_FUNCTIONALITY 		= null;

		service.ENTIRE_PAGE_IS_SELECTED			= false;
		service.ENTIRE_LIST_IS_SELECTED			= false;

		service.getDataWithExistingFilters		 = function() {
			return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
		};

		service.updateSuggestionQuery					 = function(suggestionQuery) {
			service.suggestionQuery = suggestionQuery;
			return genericDataTableDataService.getSuggestions(service.suggestionQuery);
		};

		service.updateDataQuery								 = function(query, placeholder) {
			service.dataQuery		 				 = query;
			service.search_query 				 = query;
			service.dataQueryPlaceholder = placeholder;
			sharableURLInterceptor.updateVariable('dataQuery', query);
			sharableURLInterceptor.updateVariable('dataQueryPlaceholder', placeholder);

			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			}
		};

		service.updateDateFilter							 = function(filter) {
			service.selectedDayTypeFilter = filter.name;
			service.date_filter 					= filter.name;

			service.customPeriod.from 		= null;
			service.customPeriod.to 			= null;

			sharableURLInterceptor.updateVariable('selectedDayTypeFilter', filter.name);
			return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
		};

		service.updateCustomDateFilter				 = function(date, from, to) {
			if (from) {
				service.customPeriod.from = date.hours(0).minutes(0).seconds(0).format();
				sharableURLInterceptor.updateCustomDate(date, null);
			} else if (to) {
				service.customPeriod.to 	= date.hours(23).minutes(59).seconds(59).format();
				sharableURLInterceptor.updateCustomDate(null, date);
			}

			if (service.customPeriod.from && service.customPeriod.to) {
				service.selectedDayTypeFilter = null;
				service.date_filter						= null;
				sharableURLInterceptor.updateVariable('selectedDayTypeFilter', "");
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, false);
			}
		}

		service.updateFilter									 = function(filters) {
			service.filters = filters;
			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			}
		};

		service.updateSort										 = function(columnName, sortType) {
			service.sortOrder = "";
			if (sortType === 'asc') {
				service.sortOrder += columnName;
			} else if (sortType === 'desc') {
				service.sortOrder += ("-" + columnName);
			}
			sharableURLInterceptor.updateVariable('sortType', sortType);
			sharableURLInterceptor.updateVariable('sortColumn', columnName);

			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			}
		};

		service.requireNextPage								 = function() {
			service.nextPageWasRequested 		 = true;
			service.previousPageWasRequested = false;
			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, true, false);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, true, false);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, true, false);
			}
		};

		service.requirePreviousPage						 = function() {
			service.nextPageWasRequested 		 = false;
			service.previousPageWasRequested = true;
			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, true);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, true);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, true);
			}
		};

		service.updateTableDensity						 = function(tableDensity) {
			service.tableDensity = tableDensity;
			sharableURLInterceptor.updateVariable('tableDensity', tableDensity);
			if (service.selectedDayTypeFilter) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else if (service.customPeriod.from && service.customPeriod.to) {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.customPeriod, service.filters, service.tableDensity, service.sortOrder, false, false);
			} else {
				return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
			}
		};

		service.updateSelectedItems						 = function(array) {
			service.selectedItems = array;
			service.ids 					= array;
		};

		service.togglePageSelectionState			 = function(state) {
			service.ENTIRE_PAGE_IS_SELECTED = state;
		};

		service.toggleEntireListSelectionState = function(state) {
			service.ENTIRE_LIST_IS_SELECTED = state;
			if (state === true) {
				service.ids = "all";
			} else {
				service.ids = service.selectedItems;
			}
		};

		service.setActiveFields								 = function(fields) {
			var selectedFields = [];
			for (var i = 0; i < fields.length; i++) {
				if (fields[i].selected) {
					selectedFields.push(fields[i].name);
				}
			};
			sharableURLInterceptor.updateVariable('fields', fields);
			service.fields = JSON.parse(JSON.stringify(selectedFields));
		}

		service.resetServiceFilters        		 = function() {
			service.selectedColumns 					 = [];
			service.tableDensity						 	 = null;
			service.currentPage 					 		 = null;
			service.paginationToken 					 = null;
			service.suggestionQuery 					 = null;
			service.suggestionQueryPlaceholder = null;
			service.filters 							 		 = "";
			service.selectedDayTypeFilter 		 = "";
			service.dataQuery 								 = "";
			service.customPeriod 							 = {
				from : null,
				to 	 : null
			}
			service.selectedItems 			 			 = [];
			service.sortOrder									 = "";

			// these are only for backend use so that we don't modify the entire frontend
			service.ids 									= [];
			service.fields								= [];
			service.date_filter 					= "";
			service.sort_by								= "";
			service.search_query 					= "";
			// these are only for backend use so that we don't modify the entire frontend


			service.nextPageWasRequested 		 = false;
			service.previousPageWasRequested = false;

			service.HAS_SEARCHBOX 							= null;
			service.HAS_DATE_SELECTOR 					= null;
			service.HAS_FILTERING 							= null;
			service.HAS_CUSTOMIZABLE_COLUMNS 		= null;
			service.HAS_ACTIONS									= null;

			service.HAS_ONLY_ONE_ACTION 				= null;
			service.HAS_NO_ACTIONS 							= null;
			service.HAS_MULTIPLE_ACTIONS 				= null;

			service.HAS_EXPORT_FUNCTIONALITY 		= null;

			service.ENTIRE_PAGE_IS_SELECTED			= false;
			service.ENTIRE_LIST_IS_SELECTED			= false;
		}

		service.setActiveAveryLabel						 = function(label) {
			service.activeAveryLabel = label;
		}

		service.setActiveAveryBadge						 = function(badge) {
			service.activeAveryBadge = badge;
		}

		service.setShowRemovedState 					 = function(state) {
			if (service.showRemovedState !== state) {
				service.showRemovedState = state;
				sharableURLInterceptor.updateVariable('showRemovedState', state);
				genericDataTableDataService.purgePaginationInfo().then(function(result) {
					service.nextPageWasRequested 		  = false;
					service.previousPageWasRequested = false;
					return genericDataTableDataService.getData(service.showRemovedState, service.dataQuery, service.selectedDayTypeFilter, service.filters, service.tableDensity, service.sortOrder, false, false);
				});
			}
		};

		service.resetVariables 								 = function() {
			service.selectedColumns 					 = [];
			service.tableDensity						 	 = null;
			service.currentPage 					 		 = null;
			service.paginationToken 					 = null;
			service.suggestionQuery 					 = null;
			service.suggestionQueryPlaceholder = null;
			service.showRemovedState					 = null;
			service.filters 							 		 = "";
			service.selectedDayTypeFilter 		 = "";
			service.dataQuery 								 = "";
			service.customPeriod 							 = {
				from : null,
				to 	 : null
			}
			service.selectedItems 			 			 = [];
			service.sortOrder									 = "";
			service.activeAveryLabel 					 = null;
			service.activeAveryBadge 					 = null;

			// these are only for backend use so that we don't modify the entire frontend
			service.ids 									= [];
			service.fields								= [];
			service.date_filter 					= "";
			service.sort_by								= "";
			service.search_query 					= "";
			// these are only for backend use so that we don't modify the entire frontend

			service.nextPageWasRequested 		 = false;
			service.previousPageWasRequested = false;
		}

    return service;
  }
})();
