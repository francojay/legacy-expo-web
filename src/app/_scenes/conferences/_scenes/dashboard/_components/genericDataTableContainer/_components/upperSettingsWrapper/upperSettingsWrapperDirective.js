(function(){
	'use strict'

	var directiveId 		= 'upperSettingsWrapper';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'$timeout',
		upperSettingsWrapperDirective
	];

	app.directive(directiveId, depdenciesArray);

  function upperSettingsWrapperDirective($q, $rootScope, $timeout) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/upperSettingsWrapperView.html',
			scope : {
				upperConfig : "=",
				share 			: "="
			}
		};
    function link (scope) {
			$timeout(function() {
				$(".upper-settings-wrapper").mCustomScrollbar({
				  axis 								: "x",
				  theme								: "minimal-dark",
					scrollInertia				: 0,
					autoHideScrollbar 	: false,
					alwaysShowScrollbar : true,
					snapAmount 					: 0,
				  advanced					: {
						autoExpandHorizontalScroll : true
					}
				});
			}, 0);

			$rootScope.$on("TOGGLE_CUSTOM_SCROLLBAR", function(event, data) {
				if (data.filterCreatorIsOpened) {
					$(".upper-settings-wrapper").mCustomScrollbar("disable");
				} else {
					$(".upper-settings-wrapper").mCustomScrollbar("update");
				}
			})
    }
  }
})();
