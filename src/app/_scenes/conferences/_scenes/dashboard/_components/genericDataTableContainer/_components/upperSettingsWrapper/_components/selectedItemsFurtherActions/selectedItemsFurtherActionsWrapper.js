(function() {
	'use strict'

	var directiveId 		= 'selectedItemsFurtherActions';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableDataService',
		'genericDataTableInfoStore',
		selectedItemsFurtherActionsDirective
	];

	app.directive(directiveId, depdenciesArray);

  function selectedItemsFurtherActionsDirective($rootScope, genericDataTableDataService, genericDataTableInfoStore) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/selectedItemsFurtherActions/selectedItemsFurtherActionsView.html',
			scope : {}
		};

		function link (scope) {
			scope.promptMessage 			= false;
			scope.totalNumberOfItems  = 0;
			scope.allListIsSelected 	= false;

			scope.toggleSelectionForList = function(state) {
				scope.allListIsSelected = state;
				genericDataTableInfoStore.toggleEntireListSelectionState(scope.allListIsSelected);
				$rootScope.$broadcast("DESELECT_ENTIRE_LIST",{state:scope.allListIsSelected});
			}

			$rootScope.$on("ALL_ROWS_IN_LIST_ARE_SELECTED", function($event, data) {
				scope.numberOfItemsSelected = data.numberOfItemsSelected;
				scope.promptMessage 				= true;

				if (scope.numberOfItemsSelected === scope.totalNumberOfItems) {
					scope.allListIsSelected = true;
					genericDataTableInfoStore.toggleEntireListSelectionState(scope.allListIsSelected);
				}
			});

			$rootScope.$on('NOT_ALL_ROWS_ARE_SELECTED', function($event, data) {
				if(data.numberOfItemsSelected == 0) {
						scope.promptMessage 				= false;
				}
				else {
					scope.promptMessage 				= true;
					scope.numberOfItemsSelected = data.numberOfItemsSelected;
				}

				scope.allListIsSelected 		= false;
				genericDataTableInfoStore.toggleEntireListSelectionState(scope.allListIsSelected);
			});

			$rootScope.$on(genericDataTableDataService.events.LIST_ITEMS_UPDATED, function($event, data) {
				scope.totalNumberOfItems = data.total_nr;
			});
    }
  }
})();
