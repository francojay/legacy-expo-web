(function() {
	'use strict'

	var directiveId 		= 'genericActionsSelector';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'actionCallerService',
		'$q',
		'ngDialog',
		genericActionsSelectorDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericActionsSelectorDirective($rootScope, genericDataTableInfoStore, actionCallerService, $q, ngDialog) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericActionsSelector/genericActionsSelectorView.html',
			scope: {
				actions: "="
			}
		};
    function link (scope) {
			var conferenceId 							 = $rootScope.conference ? $rootScope.conference.conference_id : null;
			scope.listIsOpened 						 = false;
			scope.numberOfActionsAvailable = 0;
			scope.showRemovedState				 = false;

			for(var i = 0; i < scope.actions.length; i++) {
				if(scope.actions[i].minimum_items == 0) {
					scope.numberOfActionsAvailable++;
					scope.actions[i].isAvailable = true;
				}
				else {
					scope.actions[i].isAvailable = false;
				}
			}

			scope.callAction 						= function(action) {
				console.log(action);
				if (action.isAvailable) {
					var actionIsAsync = action.async;
					var promises = [];
				}
				if (!action.modal_name) {
					actionCallerService.callAction(conferenceId, action).then(function(response) {
						var data 	 = response.data;
						var status = response.status;

						if (data.result_type === 'download_file') { //export was called
							$rootScope.$broadcast("NEW_PROCESS_HAS_BEEN_CREATED", data);
							$rootScope.$broadcast("SCROLLING_HEADER");
						}
					}, function(error) {

					})
				} else if (action.modal_name) {
					ngDialog.open({
						plain			: true,
						template	: action.modal_name,
						className : '',
						data			: {
							"_data": action
						}
					});
				}
			};

			scope.toggleActionsList = function($event, state) {
				$event.stopPropagation();
				scope.listIsOpened = !state;
			}

			scope.hideDropdown = function($event) {
				scope.listIsOpened 					= false;
			}

			$rootScope.$on("ALL_ROWS_IN_LIST_ARE_SELECTED", function($event, data) {
				scope.numberOfItemsSelected 	 = genericDataTableInfoStore.selectedItems.length;
				scope.numberOfActionsAvailable = 0;
				for(var i = 0; i < scope.actions.length; i++) {
					var action = scope.actions[i];
					if(scope.numberOfItemsSelected >= action.minimum_items && (scope.numberOfItemsSelected <= action.maximum_items || !action.maximum_items)) {
						action.isAvailable = true;
						scope.numberOfActionsAvailable++;
					} else {
						action.isAvailable = false;
					}
				}
				console.log(scope.actions)
			});

			$rootScope.$on('NOT_ALL_ROWS_ARE_SELECTED', function($event, data) {
				scope.numberOfActionsAvailable = 0;
				scope.numberOfItemsSelected 	 = genericDataTableInfoStore.selectedItems.length;
				for (var i = 0; i < scope.actions.length; i++) {
					var action = scope.actions[i];
					if (scope.numberOfItemsSelected >= action.minimum_items && (scope.numberOfItemsSelected <= action.maximum_items || !action.maximum_items)) {
						action.isAvailable = true;
						scope.numberOfActionsAvailable++;
					} else {
						action.isAvailable = false;
					}
				}
			});

			$rootScope.$on('SHOW_REMOVED_ITEMS_STATE_CHANGED', function($event, data) {
				scope.showRemovedState = data.state;
			});

			function downloadAsCSV(data, filename) {
				if(!filename) {
					var date = new Date();

	        var actualDate = '(' + date.getDate() + "-" + date.getMonth() + '-' + date.getFullYear() + ')';
	        filename = $rootScope.conference.name + actualDate + ".csv";
				}

        var blob = new Blob( [data], {type:'application/csv'} ) ;
				saveAs(blob, filename);

			}
    }
  }
})();
