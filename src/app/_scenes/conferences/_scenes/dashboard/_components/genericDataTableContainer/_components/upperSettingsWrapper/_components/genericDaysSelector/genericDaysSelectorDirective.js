(function(){
	'use strict'

	var directiveId 		= 'genericDaysSelector';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$rootScope',
		'genericDataTableInfoStore',
		'genericDataTableDataService',
		'sharableURLInterceptor',
		genericDaysSelectorDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericDaysSelectorDirective($rootScope, genericDataTableInfoStore, genericDataTableDataService, sharableURLInterceptor) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/_components/upperSettingsWrapper/_components/genericDaysSelector/genericDaysSelectorView.html',
			scope: {
				configuration : "=",
				shared 			  : "="
			}
		};
    function link (scope) {
			var currentConference = $rootScope.conference;

			$rootScope.fromSelectorMomentPicker = false;
			$rootScope.toSelectorMomentPicker 	= false;

			var lastValueOfFromPickerState 	= false;
			var lastValueOfToPickerState 		= false;

			scope.periodOptions 				= scope.configuration.options;
			scope.daySelectorWasClicked = false;

			if (currentConference) {
				scope.customPeriod = {
					from : moment().utc().tz(currentConference ? currentConference.timezone_name : ""),
					to   : moment().utc().tz(currentConference ? currentConference.timezone_name : "")
				};
			} else {
				scope.customPeriod = {
					from : moment().utc(),
					to 	 : moment().utc()
				}
			}

			if (scope.shared) {
				sharableURLInterceptor.updateUsedInfo(directiveId, true);
			}
			if (scope.shared && scope.shared.selectedDayTypeFilter) {
				for (var i = 0; i < scope.periodOptions.length; i++) {
					if (scope.periodOptions[i].name === scope.shared.selectedDayTypeFilter) {
						scope.currentPeriodSelected = scope.periodOptions[i];
						genericDataTableInfoStore.updateDateFilter(scope.currentPeriodSelected);
					}
				}
				scope.daySelectorWasClicked = true;
			} else if (scope.shared && scope.shared.customDateFilter && scope.shared.customDateFilter.from && scope.shared.customDateFilter.to) {
				sharableURLInterceptor.updateUsedInfo(directiveId, true);
				scope.customPeriod = {
					from : moment(scope.shared.customDateFilter.from).utc(),
					to   : moment(scope.shared.customDateFilter.to).utc()
				};
				genericDataTableInfoStore.updateCustomDateFilter(scope.customPeriod.from, true, false);
				genericDataTableInfoStore.updateCustomDateFilter(scope.customPeriod.to, false, true);
				scope.customPeriodSelected = true;
				scope.currentPeriodSelected = {
					"label" : "Custom",
					"value" : null
				};
				scope.daySelectorWasClicked = true;
			} else {
				scope.currentPeriodSelected = scope.periodOptions[0];
			}
			scope.filtersPanelOpen 			= false;

			scope.changePeriod = function($event, period) {
				genericDataTableDataService.sharedNextToken 		= null;
				genericDataTableDataService.sharedPreviousToken = null;
				scope.currentPeriodSelected = period;
				scope.customPeriodSelected 	= false;
				genericDataTableInfoStore.updateDateFilter(period);
				if(period.name !== scope.periodOptions[0].name) {
					scope.daySelectorWasClicked = true;
				} else{
					scope.daySelectorWasClicked = false;
				}


			}

			scope.openFiltersPanel = function($event, filtersPanelOpen) {
				return !filtersPanelOpen;
			}

			scope.preventClose = function($event) {
				$event.stopPropagation();
			}

			scope.updateFromDate = function(date) {
				genericDataTableInfoStore.updateCustomDateFilter(date, true, false);
			}

			scope.updateToDate = function(date) {
				genericDataTableInfoStore.updateCustomDateFilter(date, false, true);
				scope.customPeriodSelected 	= true;
				scope.currentPeriodSelected = null;
				scope.currentPeriodSelected = {
					"label" : "Custom",
					"value" : null
				};
				genericDataTableDataService.sharedNextToken 		= null;
				genericDataTableDataService.sharedPreviousToken = null;
				scope.filtersPanelOpen 													= false;
			};

			scope.updateCustomDate = function($event) {
				$event.stopPropagation();
				scope.updateFromDate(scope.customPeriod.from);
				scope.updateToDate(scope.customPeriod.to);
				scope.daySelectorWasClicked = true;
			};

			scope.hideDropdown = function() {
				if ((!$rootScope.fromSelectorMomentPicker && !lastValueOfFromPickerState) && (!$rootScope.toSelectorMomentPicker && !lastValueOfToPickerState)) {
					scope.filtersPanelOpen = false;
				}
				lastValueOfFromPickerState = $rootScope.fromSelectorMomentPicker;
				lastValueOfToPickerState	 = $rootScope.toSelectorMomentPicker;
			}

			$rootScope.$on('RESET_DAYS_SELECTOR', function(event) {
				genericDataTableDataService.sharedNextToken 		= null;
				genericDataTableDataService.sharedPreviousToken = null;
				scope.currentPeriodSelected = scope.periodOptions[0];
				scope.customPeriodSelected 	= false;
				scope.daySelectorWasClicked = false;
				sharableURLInterceptor.updateVariable('selectedDayTypeFilter', scope.periodOptions[0].name);
			});
    }
  }
})();
