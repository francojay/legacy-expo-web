(function(){
	'use strict'

	var directiveId 		= 'genericDataTable';
	var app 						= angular.module("app");
	var depdenciesArray = [
		'$q',
		'$rootScope',
		'genericDataTableDataService',
		'genericDataTableInfoStore',
		'sharableURLInterceptor',
		'shareURLService',
		'$state',
		genericDataTableDirective
	];

	app.directive(directiveId, depdenciesArray);

  function genericDataTableDirective($q, $rootScope, genericDataTableDataService, genericDataTableInfoStore, sharableURLInterceptor, shareURLService, $state) {
    return {
			restrict: 'E',
			link: link,
			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_components/genericDataTableContainer/genericDataTableView.html',
			scope: {
				configuration : "=",
				type		  		: "=",
				shared 				: "="
			},
		};

    function link (scope) {
			genericDataTableDataService.setEndpointType(scope.type);
			sharableURLInterceptor.resetVariables();
			genericDataTableInfoStore.resetVariables();
			scope.sharedInfoHasLoaded = false;
			if (scope.shared) {
					shareURLService.getShareParametersFromLink(scope.shared).then(function(response){
						scope.sharedInfo = JSON.parse(response.data.share_link_data);
						genericDataTableDataService.setSharedParams(scope.sharedInfo);
						//genericDataTableInfoStore.updateTableDensity(scope.sharedInfo.tableDensity);
						genericDataTableDataService.setDefaultTableDensity(scope.sharedInfo.tableDensity);
						scope.sharedInfoHasLoaded = true;
						sharableURLInterceptor.sharedUrlExists = true;
					});
			}
			else{
				scope.sharedInfoHasLoaded = true;
			}

			var tableDefaultDensity = null;
			for( var i = 0; i < scope.configuration.table_density.options.length; i++ ) {
				if(scope.configuration.table_density.options[i].is_default) {
					tableDefaultDensity = scope.configuration.table_density.options[i];
				}
			}
			if (!scope.shared) {
				genericDataTableInfoStore.resetServiceFilters();
				genericDataTableDataService.setDefaultTableDensity(tableDefaultDensity);
				genericDataTableInfoStore.updateTableDensity(tableDefaultDensity);
			}

			$rootScope.$on('GET_DATA_WITH_EXISTING_FILTERS', function() {
				genericDataTableInfoStore.getDataWithExistingFilters()
			});

			$rootScope.$on("RESET_DATA", function() {
				sharableURLInterceptor.resetSharableLink();
				genericDataTableInfoStore.resetServiceFilters();
				genericDataTableDataService.setDefaultTableDensity(tableDefaultDensity);
				genericDataTableInfoStore.updateTableDensity(tableDefaultDensity);
			});

    }
  }
})();
