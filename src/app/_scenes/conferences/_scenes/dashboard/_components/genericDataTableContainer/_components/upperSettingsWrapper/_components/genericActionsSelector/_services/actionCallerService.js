
(function(){
	'use strict';

  var serviceId = 'actionCallerService';
	var dependenciesArray = [
		'$q',
		'requestFactory',
		'genericDataTableInfoStore',
		'toastr',
		actionCallerService
	];
	angular.module('app').service(serviceId, dependenciesArray);

	function actionCallerService($q, requestFactory, genericDataTableInfoStore, toastr) {
    var service = {};

		service.callAction = function(conferenceId, action) {
			var deferred = $q.defer();

			//these are params for API
			var queryParams  = {
				date_filter  : genericDataTableInfoStore.date_filter,
				search_query : genericDataTableInfoStore.search_query,
				ids 				 : genericDataTableInfoStore.ids,
				filter 		 	 : genericDataTableInfoStore.filters,
				fields 			 : genericDataTableInfoStore.fields,
				sort_by			 : genericDataTableInfoStore.sortOrder
			}

			if (action.name === 'select_avery_label') {
				queryParams.avery_template = genericDataTableInfoStore.activeAveryLabel.id;
			}

			if (action.name === 'select_avery_badge') {
				queryParams.avery_template = genericDataTableInfoStore.activeAveryBadge.id;
			}
			if (conferenceId) {
				var url 		 = "api/v1.1/conferences/" + conferenceId + action.endpoint;
			} else {
				var url 		 = "api/v1.1/" + action.endpoint;
			}

			var queryString = "";
			var paramsIndex = 0;
			for (var param in queryParams) {
				var hasValue = false;
				if (typeof queryParams[param] !== 'undefined' && typeof	queryParams[param] === 'string') {
					hasValue = queryParams[param].length > 0 ? true : false;
				} else if (typeof queryParams[param] !== 'undefined' && typeof	queryParams[param] === 'object') {
					if (Array.isArray(queryParams[param]) && queryParams[param].length > 0) {
						hasValue = true;
					} else if (Object.keys(queryParams[param]).length > 0) {
						hasValue = true;
					}
				}
				if (hasValue) {
					if (queryString.length > 0) {
						if (typeof queryParams[param] === 'object') {
							queryString += ("&" + param + "=" + encodeURI(JSON.stringify(queryParams[param])));
						} else {
							queryString += ("&" + param + "=" + queryParams[param]);
						}
					} else {
						if (typeof queryParams[param] === 'object') {
							queryString += (param + "=" + encodeURI(JSON.stringify(queryParams[param])));
						} else {
							queryString += (param + "=" + queryParams[param]);
						}
					}
				}
			}

			if (queryString.length > 0) {
				url += ("?" + queryString);
			}

			if (genericDataTableInfoStore.showRemovedState) {
				url += "&show_removed=true";
			}

			if (action.method === 'GET') {
				requestFactory.get(url, {}, true, false).then(function(response) {
					deferred.resolve(response);
				}, function(error) {
					toastr.error(error.data.detail);
					deferred.reject(error);
				});

			}
			return deferred.promise;
		}

    return service;
  }
})();
