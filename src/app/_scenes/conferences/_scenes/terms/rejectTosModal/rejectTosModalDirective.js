(function() {
  'use strict';
  var directiveId = 'rejectTosModal';
  var app = angular.module('app');

  var dependenciesArray = [
    'ngDialog',
    'localStorageService',
    '$state',
    rejectTosModal
  ];

  app.directive(directiveId, dependenciesArray);

  function rejectTosModal(ngDialog, localStorageService, $state ) {
    return {
      restrict: 'E',
      templateUrl: 'app/_scenes/conferences/_scenes/terms/rejectTosModal/rejectTosModalView.html',
      link: link,
      replace: true
    };

    function link(scope) {
      var currentDialogId = scope.currentDialogId;
      scope.logOut = function() {
        if (localStorageService.clearAll()) {
					ngDialog.closeAll();
					window.Intercom("shutdown");
					$state.transitionTo('account.login');
				}
      };
      scope.continue = function() {
        ngDialog.close(currentDialogId);

      }

    }
  };
}());
