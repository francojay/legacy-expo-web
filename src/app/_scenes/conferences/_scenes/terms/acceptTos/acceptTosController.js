(function() {

  'use strict';

  var controllerId = 'acceptTosController';
  var app = angular.module('app');

  var dependenciesArray = [
    '$scope',
    'ngDialog',
    'legalTermsService',
    '$q',
    '$timeout',
    'API',
    '$location',
    'localStorageService',
    acceptTosController
  ];

  app.controller(controllerId, dependenciesArray);

  function acceptTosController($scope, ngDialog, legalTermsService, $q, $timeout, API, $location, localStorageService) {
      $scope.environment = API.ENV;

      $scope.tosTabOpened = true;
      $scope.agreeChecked = false;
      $timeout(function() {
        $('.tos-modal .legal-text').mCustomScrollbar({
          'theme': 'minimal-dark'
        });
      }, 0);
      var promises = [];

      promises.push(legalTermsService.getLegalTerms());
      $q.all(promises).then(function(response) {
        $scope.terms_of_service = response[0].terms_of_service.terms_text.split(/\n/g);
        $scope.privacy_policy = response[0].privacy_policy.terms_text.split(/\n/g);
      }).catch(function(error) {

      });

      $scope.switchTabs = function(tab) {
        if (tab === 'tos') {
          $scope.tosTabOpened = true;
        } else if (tab === 'pp') {
          $scope.tosTabOpened = false;
        }
        $timeout(function() {
          $('.tos-modal .legal-text').mCustomScrollbar({
            'theme': 'minimal-dark'
          });
        }, 0);
      };

      $scope.agreeTos = function() {
        $scope.agreeChecked = !$scope.agreeChecked ? true : false;
      }

      $scope.acceptTos = function(){
        if($scope.agreeChecked){
          legalTermsService.acceptLegalTerms().then(function(response){
            if(response.status == 200){
              var currentUser = JSON.parse(localStorageService.get('currentUser'));
              currentUser.legal.terms_of_service.require_acceptance = false;
              currentUser.legal.privacy_policy.require_acceptance = false;
              localStorage.currentUser = JSON.stringify(currentUser);
              localStorageService.set('currentUser',currentUser);
    					$scope.currentUser = currentUser;
              $location.path("/events");
            }
          });
        }
      }

      $scope.rejectTos = function(){
        ngDialog.open(
          {
            plain: true,
            template: '<reject-tos-modal></reject-tos-modal>',
            closeByEscape:false,
            closeByDocument:false
          }
        );
      }
  };
}());
