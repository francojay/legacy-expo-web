(function() {
    'use strict'
    var serviceId = 'supportService'
    var app = angular.module('app');
    var dependenciesArray = [
        '$q',
        'API',
        'requestFactory',
        'Upload',
        supportService
    ];

    app.service(serviceId, dependenciesArray);

    function supportService($q, API, requestFactory, Upload) {
        var service = {};

        service.getUsers = function(query, page) {
            var deferred = $q.defer();

            if (query) {
                var url = API.SUPERADMIN_USERS_GET + "?q=" + query + "&page=" + page;
            } else {
                var url = API.SUPERADMIN_USERS_GET + "?page=" + page;
            }

            requestFactory.get(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data.user_profiles);
                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        service.searchUsers = function(string) {
            var deferred = $q.defer();

            requestFactory.get(API.SUPERADMIN_USERS_GET + "?q=" + string, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response.data.user_profiles);
                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        service.suspendUser = function(userId) {
            var deferred = $q.defer();
            var url = API.SUPERADMIN_USER_SUSPEND + userId;

            requestFactory.post(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        service.unsuspendUser = function(userId) {
            var deferred = $q.defer();
            var url = API.SUPERADMIN_USER_UNSUSPEND + userId;

            requestFactory.post(url, {}, false, true)
                .then(function(response) {
                    deferred.resolve(response);
                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        service.changePassword = function(userId, updatedPassword) {
            var deferred = $q.defer();
            var url = API.SUPERADMIN_USER_CHANGE_PASS + userId;

            requestFactory.post(url, {
                password: updatedPassword
            }, false, true).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        return service;
    }
})();