//supportController.js
(function () {
    'use strict';
    var controllerId = 'supportController';
		var app 				 = angular.module("app");
		var dependenciesArray = [
			'$rootScope',
			'$scope',
			'SharedProperties',
			'API',
			'supportService',
			supportController
		]
    app.controller(controllerId, dependenciesArray);
    function supportController($rootScope, $scope, SharedProperties, API, supportService) {
      var vm = this;
      $scope.users = [];

      var currentPage = 1;
      $scope.searchQuery = '';

      getUsers($scope.searchQuery, currentPage);

      function getUsers(query, currentPage) {
          supportService.getUsers(query, currentPage).then(function (users) {
              if (query !== '' && currentPage === 1) {
                  $scope.users = [];
              }
              $scope.users.push.apply($scope.users, users);
          }, function (error) {
              console.log(error);
          });
      }

      $scope.searchUsers = function(query) {
          $scope.users = [];
          currentPage = 1;
          getUsers(query, currentPage);
      }

      $('.support-table-items-container').on('scroll', function () {
          if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
              currentPage++;

              getUsers($scope.searchQuery, currentPage);
          }
      })
    }

}());
