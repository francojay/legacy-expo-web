(function () {
    'use strict';
    var directiveId = 'suspendUserModal';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', 'SharedProperties', 'ngDialog', suspendUserModalDirective]);
    function suspendUserModalDirective($rootScope, SharedProperties, ngDialog) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/_scenes/conferences/_scenes/support/_components/suspendUserModal/suspendUserModalView.html',
            link: link
        };

        function link(scope) {
            scope.user = scope.ngDialogData.user;
            scope.userName = scope.user.first_name + ' ' + scope.user.last_name;
            scope.action = scope.ngDialogData.action;

            var eventToSendToParentDirective = '';

            scope.sendSuspendConfirmation = function(user, action) {
                if (action === 'suspend') {
                    eventToSendToParentDirective = 'CONFIRM_SUSPEND_USER';
                } else {
                    eventToSendToParentDirective = 'CONFIRM_UNSUSPEND_USER';
                }

                $rootScope.$broadcast(eventToSendToParentDirective, {user: user});
                ngDialog.closeAll();
            }
        }
    }
})();
