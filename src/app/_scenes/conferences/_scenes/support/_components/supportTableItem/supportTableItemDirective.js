(function () {
	'use strict';
	var directiveId = 'supportTableItemUser';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'SharedProperties', '$location', '$state', 'ngDialog', 'supportService', '$timeout', supportTableItemUserDirective]);

	function supportTableItemUserDirective($rootScope, SharedProperties, $location, $state, ngDialog, supportService, $timeout) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_scenes/support/_components/supportTableItem/supportTableItemView.html',
			replace: true,
			link: link,
			scope: {
				user: '=user'
			}
		}

		function link(scope) {
			scope.openPasswordPopup = function () {
				ngDialog.open({
					plain: true,
					template: '<change-password-modal></change-password-modal>',
					data: {
						userName: scope.user.first_name + ' ' + scope.user.last_name,
						userInitials: scope.user.first_name[0] + scope.user.last_name[0],
						profilePictureUrl: scope.user.userprofile.profile_url,
						userId: scope.user.id,
					},
					className: 'app/stylesheets/_plusAttendee.scss',
				});
			}

			scope.openSuspendUserConfirmationModal = function (user, action) {
				ngDialog.open({
					plain: true,
					template: '<suspend-user-modal></suspend-user-modal>',
					data: {
						user: user,
						action: action
					},
					className: 'app/stylesheets/_plusAttendee.scss',
				});
			}

			function unsuspendUser(user) {
				supportService.unsuspendUser(user.id).then(function (response) {
					user.is_active = true;
				}, function (error) {
					console.error(error);
				})
			}

			function suspendUser(user) {
				supportService.suspendUser(user.id).then(function (response) {
					user.is_active = false;
				}, function (error) {
					console.error(error);
				})
			}

			$rootScope.$on('CONFIRM_SUSPEND_USER', function (event, data) {
				suspendUser(data.user);
			})

			$rootScope.$on('CONFIRM_UNSUSPEND_USER', function (event, data) {
				unsuspendUser(data.user);
			})
		}
	};

}());
