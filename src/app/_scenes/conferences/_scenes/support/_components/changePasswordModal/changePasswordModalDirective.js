(function(){
	'use strict';

    var directiveId = 'changePasswordModal';

	angular
		.module('app')
		.directive(directiveId, ['SharedProperties', '$location', '$state', 'ngDialog', 'supportService', 'toastr', changePasswordModalDirective]);

	function changePasswordModalDirective(SharedProperties, $location, $state, ngDialog, supportService, toastr) {
		return {
			restrict: 'E',
			templateUrl: 'app/_scenes/conferences/_scenes/support/_components/changePasswordModal/changePasswordModalView.html',
			replace: true,
			link: link
		}

        function link(scope) {
			scope.userName = scope.ngDialogData.userName;
			scope.userInitials = scope.ngDialogData.userInitials;
			scope.profilePictureUrl = scope.ngDialogData.profilePictureUrl;
			scope.userId = scope.ngDialogData.userId;

			scope.password = '';
			scope.confirmedPassword = '';

			scope.successUpdate = false;

			scope.submitNewPassword = function (password, confirmedPassword) {
				if (password === confirmedPassword) {
					supportService.changePassword(scope.userId, password).then(function(response) {
						scope.successUpdate = true;
						scope.password = password;
					}, function(error) {
						console.log(error);
					})
				} else {
					toastr.error('Please match the passwords');
				}
			}

			scope.closeModal = function() {
				ngDialog.closeAll();
			}
        }
	};

}());
