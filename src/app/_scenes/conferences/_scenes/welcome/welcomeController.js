(function () {
    'use strict';

    var controllerId			= 'welcomeController';
		var app 				 			= angular.module("app");
		var dependenciesArray = [
			'$rootScope',
			'$scope',
			'toastr',
			'ConferenceExhibitorsService',
			'conferenceService',
			'SharedProperties',
			'$q',
			'$state',
			'$timeout',
      'localStorageService',
      '$location',
      'API',
			welcomeController
		];

    app.controller(controllerId, dependenciesArray);

		function welcomeController($rootScope, $scope, toastr, ConferenceExhibitorsService, conferenceService, SharedProperties, $q, $state, $timeout, localStorageService, $location, API) {
      $scope.environment = API.ENV;
			$scope.$on('$viewContentLoaded', function() {
        if (!localStorageService.get('access_token')) {
          $location.path("/login");
          return
        }

        $timeout(function() {
					$scope.appReady = true;
				}, 300);

			});

			var sharedData	 	= SharedProperties.sharedObject;
			var stores 				= {
				"google" : "https://play.google.com/store/apps/details?id=io.expopass.expo",
				"apple"  : "https://itunes.apple.com/us/app/expo-pass/id921625648?mt=8"
			};

			$scope.model 			= {
				"code": ""
			};
			$scope.codeError 	= false;

			$scope.joinConferenceButtonHandler = function(code) {
				var codeIsValid	 = validateCode(code);
				$scope.codeError = !codeIsValid;

				if (codeIsValid) {
					joinConferenceWithCode(code);
				} else {
					toastr.error("Please insert a code that has 6 characters!");
					return;
				}
			}

			$scope.openStore = function(type) {
				var win = window.open(stores[type], '_blank');
				win.focus();
			}

      $scope.logOut = function() {
        if (localStorageService.clearAll()) {
					window.Intercom("shutdown");
					$state.go('account.login');
				}
      }

			function validateCode(code) {
				return code.length === 6 ? true : false;
			}

			function joinConferenceWithCode(code) {
				ConferenceExhibitorsService.getConferenceByCode(code).then(function (response) {
					sharedData.current_exhibitor_conference_id = response.conference.id;
					var joinedConferenceId = conferenceService.hash_conference_id.encode(response.conference.id);
					loadExhibitorCompanies(response.conference.id).then(function(response) {
						if (response.length === 0) {
							$state.go('conferences.join.create', {conference_id: joinedConferenceId})
						} else {
							$state.go('conferences.join.select', {conference_id: joinedConferenceId, exhibitorsList: response});
						}
					});
				}, function(error) {
          $scope.codeError = true;
          if (error.status === 404) {
            toastr.warning("Your 6-digit code was not recognized.");
          } else {
            toastr.warning("An error has occured. Please try again later or contact the event owner for further details.")
          }
				});
			}

			function loadExhibitorCompanies(conferenceId) {
				var deferred = $q.defer();
				ConferenceExhibitorsService.listExhibitorCompanies(conferenceId).then(function(response) {
					deferred.resolve(response);
				},function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			}
    }

}());
