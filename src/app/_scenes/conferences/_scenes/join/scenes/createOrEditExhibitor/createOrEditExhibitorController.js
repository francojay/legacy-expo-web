(function () {
    'use strict';

    var controllerId = 'createOrEditExhibitor';
    var app          = angular.module("app");

    var dependenciesArray = [
      '$rootScope',
      '$scope',
      '$state',
      '$stateParams',
      'ConferenceExhibitorsService',
      'SharedProperties',
      'ConferenceUsersService',
      'conferenceService',
      'ConferenceSessionsService',
      'API',
      'toastr',
      '$q',
      'fileService',
      '$timeout',
      'intlTelInputOptions',
      createOrEditExhibitor
    ];

    app.controller(controllerId, dependenciesArray);

    function createOrEditExhibitor ($rootScope, $scope, $state, $stateParams, ConferenceExhibitorsService, SharedProperties, ConferenceUsersService, conferenceService, ConferenceSessionsService, API, toastr, $q, fileService, $timeout, intlTelInputOptions) {

        var vm                   = this;
        var sharedData           = SharedProperties.sharedObject;

        var encodedConference_id = $stateParams.conference_id;
        var conference_id        = conferenceService.hash_conference_id.decode(encodedConference_id)[0];
        var logoLink             = "";
        vm.exhibitorDetails      = $stateParams.exhibitorDetails;

        if (vm.exhibitorDetails) {
          vm.logo                  = vm.exhibitorDetails.logo;
        } else {
          vm.logo                  = "";
        }
        vm.united_states         = API.UNITED_STATES;
        vm.all_countries         = API.COUNTRIES;

        vm.mainExhibitorProfileFields = [
            {
                label: 'Company Name',
                code: 'name',
                required: true
            },
            {
                label: 'Email Address',
                code: 'admin_email',
                required: true
            },
            {
                label: 'Phone Number',
                code: 'phone_number',
                required: false,
                value : ""
            },
            {
                label: 'Street Address',
                code: 'street_address',
                required: false
            },
            {
                label: 'City',
                code: 'city',
                required: false
            },
            {
                label: 'Country',
                code: 'country',
                placeholder: 'Choose a country',
                required: false
            },
            {
                label: 'State',
                code: 'state',
                placeholder: '--',
                required: true
            },
            {
                label: 'Zip Code',
                code: 'zip_code',
                required: false
            },
            {
                label: 'Website',
                code: 'website',
                required: false
            },
            {
                label: 'Promo Code',
                code: 'promo_code',
                required: false
            },
            {
                label: 'Description',
                code: 'description',
                required: false
            },
            {
                label: 'Company Logo',
                code: 'logo',
                required: false
            }
        ];
        vm.defaultCountryFlag = $stateParams.conferenceCountry;

        $timeout(function() {
          $('.join-conference.company-profile .add-form-wrapper').mCustomScrollbar({'theme': 'minimal-dark'});
        }, 0);

        vm.saveButtonHandler = function (exhibitor) {
            if ($stateParams.exhibitorDetails) {
                editExhibitor(exhibitor);
            } else {
                createExhibitor(exhibitor);
            }
        }

        vm.uploadCompanyLogo = function (files) {
          if(files.length > 0) {
            fileService.getUploadSignature(files[0])
                .then(function (response) {
                    fileService.uploadFile(files[0], response)
                        .then(function (uploadResponse) {
                          logoLink = response.data.file_link;
                          vm.logo = logoLink;
                        }, function(error) {
                          toastr.error("An error has occured while trying to upload the company logo!")
                        });
                }, function(error) {
                  if (error.status == 403) {
                      toastr.error('Permission Denied!');
                  } else {
                      toastr.error('Error!', 'There has been a problem with your request.');
                  }
                });
          }

        };

        vm.goToOverview = function($event) {
          $event.stopPropagation();
          $event.preventDefault();

          $state.go('conferences.overview')
        }
        vm.updateCountryForPhoneInput = function(field) {
  				if (field !== undefined) {
            angular.forEach(API.COUNTRIES, function(country) {
              if(country.name === field){
                var countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
                var number = $rootScope.phoneIntlTelInputCtrl.getNumber(intlTelInputUtils.numberFormat.NATIONAL);
                number = number.replace("+"+countryData.dialCode,"");
                $rootScope.phoneIntlTelInputCtrl.setCountry(country.code.toLowerCase());
                countryData = $rootScope.phoneIntlTelInputCtrl.getSelectedCountryData();
                if(number){
                  $timeout(function() {
                    scope.mainExhibitorProfileFields[2].value = "+"+countryData.dialCode+number;
                  }, 100);
                }
              }
            });

  				}
  			}
        function createExhibitor(exhibitor) {
            var exhibitorDetailsAreCorrect = validateForm(exhibitor);
            if (exhibitorDetailsAreCorrect) {
                ConferenceExhibitorsService
                    .createExhibitor(conference_id, exhibitor, logoLink)
                    .then(function (response) {
                        toastr.success('You succesfully joined the conference!');
                        $state.transitionTo('conferences.overview');
                    }, function (error) {
                        toastr.error(error.data.detail)
                    })
            }

        }

        function editExhibitor(exhibitor) {
            var exhibitorDetailsAreCorrect = validateForm(exhibitor);
            if (exhibitorDetailsAreCorrect) {
                exhibitor.company_name = exhibitor.name;
                ConferenceExhibitorsService
                    .updateExhibitorCompanyProfile(exhibitor, logoLink)
                    .then(function (response) {
                        toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                        $state.transitionTo('conferences.overview');
                    })
                    .catch(function (error) {
                      if(error.data.detail) {
                        toastr.error('Error!', error.data.detail);
                        return ;
                      }
                      toastr.error('Error!', 'There has been a problem with your request.');
                    });
            }

        }

        function validateForm(exhibitor) {
            if (exhibitor) {
                var emailIsValid = validateEmail(exhibitor.admin_email);
                var nameIsValid = validateExhibitorName(exhibitor.name);
                var stateAndCountryAreValid = validateCountryAndState(exhibitor.country, exhibitor.state);
                var phoneNumberIsValid = true;

                if (!emailIsValid) {
                    vm.adminInvalid = true;
                    popToastr('error', 'Please insert a valid email!');
                } else {
                    vm.adminInvalid = false;
                }

                if (!nameIsValid) {
                    vm.nameInvalid = true;
                    popToastr('error', 'Name must be at least 3 characters long!');
                } else {
                    vm.nameInvalid = false;
                }

                if (!stateAndCountryAreValid) {
                    popToastr('error', 'Please select a State!');
                }
                if(vm.mainExhibitorProfileFields[2].value === undefined || $rootScope.phoneIntlTelInputCtrl.getNumberType() == -1){
                  phoneNumberIsValid = false;
                  popToastr('error', 'Please fill the phone number field with a correct phone number!');
                }
                else if(!_.isEmpty(vm.mainExhibitorProfileFields[2].value)){
                  phoneNumberIsValid = true;
                  exhibitor.phone_number = vm.mainExhibitorProfileFields[2].value;
                }

            } else {
                popToastr('error', 'Required exhibitor details are not completed');
                vm.adminInvalid = true;
                vm.nameInvalid = true;
            }

            return emailIsValid && nameIsValid && stateAndCountryAreValid && phoneNumberIsValid;
        }

        function validateEmail(email) {
            var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

            return emailRegex.test(email);
        }

        function validateExhibitorName(name) {
            if (name) {
                return name.length >= 1;
            } else return false;
        }

        function validateCountryAndState(country, state) {
            if(country === 'United States')  {
                if (state && state !== '--') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }

        function popToastr(type, message) {
            if (type === 'warning') {
                toastr.warn(message);
            } else if (type === 'error') {
                toastr.error(message);
            }
        }

    }
})();
