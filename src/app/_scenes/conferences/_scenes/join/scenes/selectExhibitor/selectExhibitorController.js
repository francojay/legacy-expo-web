(function () {
    'use strict';

    var controllerId      = 'selectExhibitorController';
    var app               = angular.module('app');
    var dependenciesArray = [
      '$rootScope',
      '$scope',
      '$state',
      '$stateParams',
      'ConferenceExhibitorsService',
      'SharedProperties',
      'ConferenceUsersService',
      'conferenceService',
      'ConferenceSessionsService',
      'toastr',
      '$q',
      'joinConferenceDataService',
      selectExhibitorController
    ];

    app.controller(controllerId, dependenciesArray);

    function selectExhibitorController($rootScope, $scope, $state, $stateParams, ConferenceExhibitorsService, SharedProperties, ConferenceUsersService, conferenceService, ConferenceSessionsService, toastr, $q, joinConferenceDataService) {
        $scope.conferenceId = $stateParams.conference_id;
        var sharedData = SharedProperties.sharedObject;

        var encodedConference_id = $stateParams.conference_id;
        var conference_id = conferenceService.hash_conference_id.decode(encodedConference_id)[0];

        init();

        function init() {
            if ($stateParams.exhibitorsList.length === 0) {
                initializeExhibitorsList(conference_id).then(function(response) {
                    $scope.exhibitorsList = response;
                }, function(error) {
                    console.error(error);
                });
            } else {
                $scope.exhibitorsList = $stateParams.exhibitorsList;
            }
        }

        $scope.checkExhibitorCompany = function (exhibitorCompany) {
            _.each($scope.exhibitorsList, function (company) {
                if (company.id != exhibitorCompany.id) {
                    company.checked = false;
                }
            });

            exhibitorCompany.checked = !exhibitorCompany.checked;

            if (exhibitorCompany.id === $scope.checkedExhibitorCompanyId) {
                $scope.checkedExhibitorCompanyId = undefined;
            } else {
                $scope.checkedExhibitorCompanyId = exhibitorCompany.id;
            }
        }

        $scope.joinAsCompany = function (companyId) {
            if (companyId) {
                joinConferenceDataService.joinAsCompany(companyId)
                    .then(function (response) {
                        var hashedExhbitorId = conferenceService.hash_conference_id.encode(response.data.id);
                        $state.go('conferences.join.edit', {conference_id: $stateParams.conference_id, exhibitor_id: hashedExhbitorId, exhibitorDetails: response.data});
                    }, function(error) {
                      toastr.error(error.data.detail || "Permission Denied! Please contact the company administrator for access!");
                    });
            } else {
                toastr.error("Please select a company first!");
            }
        }

        function initializeExhibitorsList(conferenceId) {
            var deferred = $q.defer();
            ConferenceExhibitorsService.listExhibitorCompanies(conferenceId)
                .then(function (response) {
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
})();
