(function(){
	'use strict';

  var serviceId = 'joinConferenceDataService';
	var app 			= angular.module('app');
  var dependenciesArray = [
    '$q',
    'API',
    'requestFactory',
    joinConferenceDataService
  ];

	app.service(serviceId, dependenciesArray);

	function joinConferenceDataService($q, API, requestFactory) {
    var service = {};

    service.joinAsCompany = function(exhibitorId){

        var deferred = $q.defer();
  			var url 		 = API.JOIN_AS_COMPANY + exhibitorId + '/';

  			requestFactory.get(url, {}, false).then(function(response){
  				deferred.resolve(response);
  			}, function(error) {
  				deferred.reject(error);
  			});

  			return deferred.promise;
    };

    return service;
  }
})();
