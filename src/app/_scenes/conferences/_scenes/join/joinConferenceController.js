(function () {
    'use strict';

    var controllerId = 'joinConferenceController';
    var app          = angular.module('app');
    var dependenciesArray = [
      '$rootScope',
      '$scope',
      'API',
      '$state',
      '$stateParams',
      'ngDialog',
      '$location',
      'ConferenceExhibitorsService',
      'SharedProperties',
      'ConferenceUsersService',
      'conferenceService',
      'ConferenceSessionsService',
      'fileService',
      '$q',
      'toastr',
      joinConferenceController
    ];

    app.controller(controllerId, dependenciesArray);

    function joinConferenceController(
        $rootScope, $scope, API, $state, $stateParams, ngDialog, $location, ConferenceExhibitorsService, SharedProperties, ConferenceUsersService,
        conferenceService, ConferenceSessionsService,
        fileService, $q, toastr) {

        var sharedData = SharedProperties.sharedObject;
        var conferencesList = $rootScope.conferencesList;
        $scope.united_states = API.UNITED_STATES;
        $scope.all_countries = API.COUNTRIES;
        $scope.conferencePassKey = [];

        $scope.createExhibitor = function () {
            if (_.isObject($scope.previousExhibitorData.country)) {
                var country = $scope.previousExhibitorData.country.name;
                if (country == 'United States') {
                    var state = $scope.previousExhibitorData.state.abbreviation;
                    $scope.previousExhibitorData.state = state;
                }
                $scope.previousExhibitorData.country = country;
            }
            $scope.previousExhibitorData = _.pickBy($scope.previousExhibitorData, function (infok, infov) {
                if ($scope.previousExhibitorData[infov] != null && $scope.previousExhibitorData[infov] != '') {
                    return $scope.previousExhibitorData[infov];
                }
            });
            $scope.previousExhibitorData.id = sharedData.current_exhibitor_id;
            $scope.previousExhibitorData.name = $scope.previousExhibitorData.company_name;
            //console.log($scope.previousExhibitorData);

            if ($scope.companySelected) {
                $scope.previousExhibitorData.id = $scope.companySelected.id;
                ConferenceExhibitorsService
                    .updateExhibitorCompanyProfile($scope.previousExhibitorData)
                    .then(function (response) {
                        toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                        $location.path('/exhibitor/' + ConferenceSessionsService.hash_session_id.encode(sharedData.current_exhibitor_conference_id));
                    })
                    .catch(function (error) {
                        toastr.error('Error!', 'There has been a problem with your request.');
                    });
            } else {
                ConferenceExhibitorsService
                    .createExhibitor(sharedData.current_exhibitor_conference_id, $scope.previousExhibitorData)
                    .then(function (response) {
                        $location.path('/exhibitor/' + ConferenceSessionsService.hash_session_id.encode(sharedData.current_exhibitor_conference_id));
                        toastr.success('You succesfully join the ' + response.conference_info.name + ' event!');
                    })
                    .catch(function (error) {
                        // toastr.error('You enter a invalid Pass Key!');
                    });
            }
        }

        $scope.joinConference = function () {
            var stringPassKey = ''

            var passKeyExists = verifyIfPassKeyExists($scope.conferencePassKey)

            if(passKeyExists) {
                stringPassKey = convertPassKeyToString($scope.conferencePassKey);
                var isJoin = false;
                ConferenceExhibitorsService
                    .getConferenceByCode(stringPassKey)
                    .then(function (response) {
                        sharedData.current_exhibitor_conference_id = response.conference.id;
                        var joinedConferenceId = conferenceService.hash_conference_id.encode(response.conference.id);
                        var conferenceDefaultCountry = 'us';
                        angular.forEach(API.COUNTRIES, function(country) {
                          if(country.name === response.conference.country){
                            conferenceDefaultCountry = country.code.toLowerCase();
                          }
                        });

                        _.each(conferencesList,function(item){
                            if(item.id == joinedConferenceId && item.role=='exhibitor' && isJoin == false){
                              isJoin = true;
                            }
                        });
                        if(!isJoin){
                          loadExhibitorCompanies(response.conference.id).then(function(response) {
                              if (response.length === 0) {
                                  $state.go('conferences.join.create', {conference_id: joinedConferenceId, conferenceCountry: conferenceDefaultCountry})
                              } else {
                                  $state.go('conferences.join.select', {conference_id: joinedConferenceId, exhibitorsList: response, conferenceCountry: conferenceDefaultCountry});
                              }
                          });
                        }
                        else{
                          toastr.error("You already joined at this event. This event is listed on your dashboard. ")
                        }

                    })
                    .catch(function (error) {
                      if(error.data.details) {
                        toastr.error(error.data.details);
                        return ;
                      }
                        toastr.error("An error has occured. Please try again.")
                    });

            } else {
                toastr.error("Please insert the code!")
            }
        }

        $scope.joinConferenceEnter = function (e) {
            if (e.keyCode == 13) {
                $scope.joinConference();
            }
        };

        function loadExhibitorCompanies(conferenceId) {
            var deferred = $q.defer();
            ConferenceExhibitorsService.listExhibitorCompanies(conferenceId)
                .then(function (response) {
                    deferred.resolve(response);
                }).catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function verifyIfPassKeyExists(passKey) {
            if(passKey.length === 6) {
                return true;
            } else {
                return false;
            }
        }

        function convertPassKeyToString(passKey) {
            var code = '';

            _.each(passKey, function (digit) {
                code = code + digit;
            });

            return code;
        }

        $scope.focus = function (id) {
            if (id != 0 && document.getElementById('passkey' + (id - 1)).value !== '') {
                document.getElementById('passkey' + id).focus();
            }
        };

        $scope.uploadCompanyLogo = function (files) {
            lib.squareifyImage($q, files)
                .then(function (response) {
                    var imgGile = response.file;

                    fileService.getUploadSignature(imgGile)
                        .then(function (response) {
                            var file_link = response.data.file_link;
                            fileService.uploadFile(imgGile, response)
                                .then(function (uploadResponse) {
                                    $scope.previousExhibitorData.logo = file_link;
                                })
                                .catch(function (error) {
                                });
                        })
                        .catch(function (error) {
                            if (error.status == 403) {
                                toastr.error('Permission Denied!');
                            } else {
                                toastr.error('Error!', 'There has been a problem with your request.');
                            }
                        });
                });
        };

        $scope.updateExhibitorProfile = function () {
            if (_.isObject($scope.previousExhibitorData.country)) {
                var country = $scope.previousExhibitorData.country.name;
                if (country == 'United States') {
                    var state = $scope.previousExhibitorData.state.abbreviation;
                    $scope.previousExhibitorData.state = state;
                }
                $scope.previousExhibitorData.country = country;
            }
            $scope.previousExhibitorData = _.pickBy($scope.previousExhibitorData, function (infok, infov) {
                if ($scope.previousExhibitorData[infov] != null && $scope.previousExhibitorData[infov] != '') {
                    return $scope.previousExhibitorData[infov];
                }
            });
            $scope.previousExhibitorData.id = sharedData.current_exhibitor_id;
            ConferenceExhibitorsService
                .updateExhibitorCompanyProfile($scope.previousExhibitorData)
                .then(function (response) {
                    toastr.success("Update Succeeded", "Your Company Profile has been succesfully updated!");
                    $location.path('exhibitor/' + $scope.conference_id);
                })
                .catch(function (error) {
                    toastr.error('Error!', 'There has been a problem with your request.');
                });
        };
    }
})();
