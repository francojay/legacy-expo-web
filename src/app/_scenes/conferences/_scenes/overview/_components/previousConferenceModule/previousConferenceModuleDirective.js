(function(){
	'use strict'

    var directiveId = 'previousConferenceModule';

		angular
			.module('app')
			.directive(directiveId, previousConferenceModule);
			function previousConferenceModule (){
				return {
					restrict: 'A',
					replace: true,
					templateUrl: 'app/_scenes/conferences/_scenes/overview/_components/previousConferenceModule/previousConferenceModuleView.html'
                }
		}
})();
