(function(){
	'use strict'

	var directiveId = 'upcomingConferenceModule';
		angular
			.module('app')
			.directive(directiveId, upcomingConferenceModule);
			function upcomingConferenceModule (){
				return {
					restrict: 'E',
					replace: true,
					scope: {
						upcomingConferenceData: '=conferenceData',
						formatRole: '=formatRole'
					},
					templateUrl: 'app/_scenes/conferences/_scenes/overview/_components/upcomingConferenceModule/upcomingConferenceModuleView.html'
				}
		}
})();
