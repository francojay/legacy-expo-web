(function(){
	'use strict'

  var controllerId = 'conferencesOverviewController';
	var app					 = angular.module("app");

	var dependenciesArray = [
		'$scope',
		'$http',
		'conferenceService',
		'SharedProperties',
		'localStorageService',
		'$timeout',
		'$rootScope',
		'$state',
		'$location',
		'dataTableConfigGetter',
		conferencesOverviewController
	];

	app.controller(controllerId, dependenciesArray);
	function conferencesOverviewController($scope, $http, conferenceService, SharedProperties, localStorageService, $timeout, $rootScope, $state, $location, dataTableConfigGetter) {
		var vm = this;
		var sharedData 								= SharedProperties.sharedObject;
		var conferencesAllPage 				= 1;

		sharedData.displayConference  = true;
		sharedData.currentPageTitle   = 'dashboard';

		$rootScope.conferencesList 	  = [];
		$rootScope.isSuperAdmin 		  = localStorageService.get('is_superuser');

		$scope.upcomingConferences 	  = [];
		$scope.previousConferences 	  = [];
		$scope.orderDesc				 	 	  = true;
		$scope.sharedInfo 					  = $location.$$search.share;
		$scope.dataTableConfiguration = null;

		if ($rootScope.isSuperAdmin) {
			dataTableConfigGetter.getTableConfig(null, 'super_admin/conferences').then(function(response) {
				$scope.dataTableConfiguration = response;
				$rootScope.conference 				= null;
			}, function(error) {
				console.log(error);
			})
		}
		init();
		$scope.formatRole = function(role, user_role, upcoming){
			if(upcoming){
				if(role == 'admin' && user_role == 'admin'){
					return 'Event Owner';
				}
				else if(user_role == 'conference_user'){
					return 'Event User';
				}
				else if(role == 'exhibitor'){
					return 'Exhibitor';
				}
				else if(role == "both"){
					return 'Exhibitor & Event Owner';
				}

			} else{
				if(role == 'admin' && user_role == 'admin'){
					return 'Event Owner';
				}
				else if(user_role == 'conference_user'){
					return 'Event User';
				}
				else if(role == 'exhibitor'){
					return 'Exhibitor';
				}
				else if(role == "both"){
					return 'Exhibitor & Event Owner';
				}
			}
		};

    $scope.sortByDate = function() {
    	if($scope.orderDesc){
    		$scope.previousConferences = _.orderBy($scope.previousConferences, ['date_from','date_to'], ['asc', 'asc']);
				if ($rootScope.isSuperAdmin) {
					$scope.conferences = _.orderBy($scope.conferences, ['date_from','date_to'], ['asc', 'asc']);
				}
    		$scope.orderDesc = false;
    	} else {
    		$scope.previousConferences = _.orderBy($scope.previousConferences, ['date_from','date_to'], ['desc', 'desc']);
				if ($rootScope.isSuperAdmin) {
					$scope.conferences = _.orderBy($scope.conferences, ['date_from','date_to'], ['desc', 'desc']);
				}
    		$scope.orderDesc = true;
    	}
    };

		function init() {
			conferenceService.getAllConferences().then(function(conferences) {
				if (conferences.conferences.length === 0) {
					$state.go('conferences.welcome');
				}
				if ($rootScope.isSuperAdmin) {
					$scope.conferences = _.orderBy(conferences.conferences, 'date_from', 'desc');
					_.each($scope.conferences, function(conference) {
						if(!conference.time_zone){
							conference.time_zone = '-0500'; // New york time
						} else if((conference.time_zone[0] != '-') && (conference.time_zone[0] != '+')){
							conference.time_zone = '+' + conference.time_zone;
						}
						conference.conference_id = conferenceService.hash_conference_id.encode(conference.conference_id);
						var momentDateFromWithTz     = moment(conference.date_from).tz(conference.timezone_name);
			      var momentDateWithCorrection = momentDateFromWithTz;
			      conference.date_from  = momentDateWithCorrection.format('M/D/Y');
			      var momentDateToWithTz         = moment(conference.date_to).tz(conference.timezone_name);
			      var momentDateToWithCorrection = momentDateToWithTz;
			      conference.date_to      = momentDateToWithCorrection.format('M/D/Y');
					});
					$scope.conferencesTotalCount = conferences.total_count;
				} else {
					SharedProperties.conference = null;
					_.each(conferences.conferences, function(conference) {
						var now = new Date();
						if(!conference.time_zone){
							conference.time_zone = '-0500'; // New york time
						} else if((conference.time_zone[0] != '-') && (conference.time_zone[0] != '+')){
							conference.time_zone = '+' + conference.time_zone;
						}
						conference.conference_id = conferenceService.hash_conference_id.encode(conference.conference_id);
						var conference_date = new Date(conference.date_to);
						conference_date.setDate(conference_date.getDate()+1);
						var momentDateFromWithTz     = moment(conference.date_from).tz(conference.timezone_name);
			      var momentDateWithCorrection = momentDateFromWithTz;
			      conference.date_from  = momentDateWithCorrection.format('M/D/Y');
			      var momentDateToWithTz         = moment(conference.date_to).tz(conference.timezone_name);
			      var momentDateToWithCorrection = momentDateToWithTz;
			      conference.date_to      = momentDateToWithCorrection.format('M/D/Y');
						$rootScope.conferencesList.push({id:conference.conference_id,role:conference.conference_role});
						if (now <= conference_date) {
							$scope.upcomingConferences.push(conference);
						} else {
							$scope.previousConferences.push(conference);
						}
					});
					$scope.previousConferences = _.orderBy($scope.previousConferences, 'date_from', 'desc');
					render();
				}
			});
		};

		function render() {
			if($scope.upcomingConferences.length === 0){
				$scope.tabFilled = false;
				$scope.tabEmpty = true;
			} else{
				$scope.tabFilled = true;
				$scope.tabEmpty = false;
			}

			if($scope.previousConferences.length === 0){
                    $scope.tabFilled2 = false;
                    $scope.tabEmpty2 = true;
                } else{
                    $scope.tabFilled2 = true;
                    $scope.tabEmpty2 = false;
                }
		};

		setTimeout(function () {
			$('.grid-table-super-admin > table > tbody').on('scroll', function () {
				if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
					conferencesAllPage++;
					conferenceService.getAllConferences(conferencesAllPage, SharedProperties.conferenceSearchQuery).then(function (conferences) {
							angular.forEach(conferences.conferences, function(conference) {
								conference.conference_id = conferenceService.hash_conference_id.encode(conference.conference_id);
								if(conference.timezone_name != null){
									var momentDateFromWithTz     = moment(conference.date_from).tz(conference.timezone_name);
						      var momentDateWithCorrection = momentDateFromWithTz;
						      conference.date_from  = momentDateWithCorrection.format('M/D/Y');
						      var momentDateToWithTz         = moment(conference.date_to).tz(conference.timezone_name);
						      var momentDateToWithCorrection = momentDateToWithTz;
						      conference.date_to      = momentDateToWithCorrection.format('M/D/Y');
								}
							})
							$scope.conferences.push.apply($scope.conferences, conferences.conferences);
							$scope.conferences = _.uniqBy($scope.conferences, 'conference_id');
						});
				}
			})
		}, 1000);

		$scope.$watch(function(){
            return $rootScope.conferenceSearchQuery;
            }, function(image) {
            	conferencesAllPage = 0;
              conferenceService.getAllConferences(conferencesAllPage, $rootScope.conferenceSearchQuery).then(function (conferences) {
						angular.forEach(conferences.conferences, function(conference) {
							conference.conference_id = conferenceService.hash_conference_id.encode(conference.conference_id);
							if(conference.timezone_name != null){
								var momentDateFromWithTz     = moment(conference.date_from).tz(conference.timezone_name);
					      var momentDateWithCorrection = momentDateFromWithTz;
					      conference.date_from  = momentDateWithCorrection.format('M/D/Y');
					      var momentDateToWithTz         = moment(conference.date_to).tz(conference.timezone_name);
					      var momentDateToWithCorrection = momentDateToWithTz;
					      conference.date_to      = momentDateToWithCorrection.format('M/D/Y');
							}
						})
						$scope.conferences = conferences.conferences;
					});
        });
	}
})();
