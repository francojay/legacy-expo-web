(function() {
    'use strict';

    var countries = require("countriesList");
    var unitedStates = require("unitedStatesList");
    var timezones = require("timezonesList");

    var detectEnvironment = require("environmentDetector");
    var urlConfigurations = require("urlConfigurations");

    angular.module('app', [])
        .constant('API', {
            AUTH_URL_DEV: urlConfigurations.getAuthUrl(),
            BASE_URL: urlConfigurations.getBaseUrl(),
            REGISTER_URL_DEV: urlConfigurations.getRegisterUrl(),
            ENV: detectEnvironment(),
            CLIENT_ID_DEV: '3XTNVEmw9cLkhUfLSb5Ar3dCd5DgPansVxm9EDqh',
            FORGOT_PASSWORD_URL: 'api/account/forgot_password/',
            VALIDATE_ACCOUNT_URL: 'api/account/validate/',
            RESET_ACCOUNT_PASSWORD_URL: 'api/account/reset_password',
            GET_DEFAULT_COVER_SCREENS: 'api/v1.1/default_cover_screens',
            //super-admin
            EXHIBITOR_USERS_CR: '/api/v1.1/exhibitor_users/',
            EXHIBITOR_USERS_UD: '/api/v1.1/exhibitor_user/',
            EXHIBITOR_QUALIFIERS_CR: '/api/v1.1/exhibitor_questions/',
            ///
            UPDATE_REGISTERED_ATTENDEE: 'api/v1.1/update_registration/',
            CONTINUE_REGISTRATION: 'api/v1.1/continue_registration/',
            // new endpoints
            CONFERENCE_CRUD: '/api/v1.1/conference/',
            CONFERENCE_SUPPORT_CONTACTS_CR: '/api/v1.1/conference_support_contacts/',
            CONFERENCE_SUPPORT_CONTACTS_UD: '/api/v1.1/conference_support_contact/',
            CONFERENCE_USERS_CR: '/api/v1.1/conference_users/',
            CONFERENCE_USERS_UD: '/api/v1.1/conference_user/',
            GET_UPCOMING_CONFERENCES_URL: 'api/conference/upcoming/',
            CREATE_CONFERENCE_URL: 'api/conference/create/',
            CREATE_CONFERENCE_V11_URL: 'api/v1.1/conferences/',
            GET_CONFERENCE_TZ_V11_URL: 'api/v1.1/conference_tz/',
            DELETE_CONFERENCE_URL: 'api/conference/delete/',
            UPDATE_CONFERENCE_URL: 'api/conference/update/',
            UPDATE_CONFERENCE_V11_URL: 'api/v1.1/conference/',
            GET_ALL_CONFERENCES_URL: 'api/conference/all/',
            GET_CONFERENCE_SUPPORT_CONTACTS_URL: 'api/conference/support_contact_list',
            CREATE_CONFERENCE_SUPPORT_CONTACTS_URL: 'api/conference/support_contact_create',
            DELETE_CONFERENCE_SUPPORT_CONTACT_URL: 'api/conference/support_contact_delete',
            UPDATE_CONFERENCE_SUPPORT_CONTACT_URL: 'api/conference/support_contact_update',
            UPDATE_CONFERENCE_SUPPORT_CONTACT_V1_1_URL: 'api/v1.1/conference_support_contact/',
            CREATE_CONFERENCE_SUPPORT_CONTACT_V1_1_URL: 'api/v1.1/conference_support_contacts/',
            GET_ACCOUNT_PROFILE: 'api/account/profile/',
            INVITE_USER: 'api/invitation/create/',
            CREATE_CONFERENCE_USER_URL: 'api/user/create/',
            REMOVE_CONFERENCE_USER_URL: 'api/user/remove',
            UPDATE_USER_RIGHTS: '/api/user/rights_update',
            CREATE_CONFERENCE_SESSION_URL: 'api/session/create',
            UPDATE_CONFERENCE_SESSION_URL: 'api/session/update',
            REMOVE_CONFERENCE_SESSION_URL: 'api/session/remove',
            GET_ALL_CONFERENCE_SESSIONS_URL: 'api/session/list',
            GET_BATCH_CONFERENCE_SESSIONS_URL: 'api/v1.1/batch_sessions_for_web/',
            GET_SESSIONS_URL: 'api/session/getf',
            GET_ALL_CONFERENCE_USERS_URL: '/api/user/list/',
            GET_ATTENDEE_BY_ID: '/api/v1.1/attendee/',
            GET_ALL_CONFERENCES_ATTENDEES_URL: '/api/attendee/list/',
            GET_ALL_CONFERENCES_EXHIBITORS_URL: '/api/exhibitor/list/',
            CREATE_CONFERENCES_EXHIBITORS_URL: '/api/exhibitor/create/',
            EXHIBITOR_FILES_CRUD: 'api/v1.1/exhibitor_files/',
            EXHIBITOR_FILE_CRUD: 'api/v1.1/exhibitor_file/',
            EXHIBITOR_CONTACTS_CURD: 'api/v1.1/exhibitor_support_contacts/',
            EXHIBITOR_CONTACT_CURD: 'api/v1.1/exhibitor_support_contact/',
            CONFERENCE_EXHIBITOR_CONTACTS_CRUD: 'api/v1.1/conference_exhibitor_support_contacts/',
            CONFERENCE_EXHIBITOR_CONTACT_CRUD: 'api/v1.1/conference_exhibitor_support_contact/',
            CONFERENCE_EXHIBITOR_PAYMENTS: 'api/v1.1/conference_exhibitor_payments/',
            UPDATE_CONFERENCE_EXHIBITOR: 'api/v1.1/exhibitor_company/',
            ATTENDEE_PAYMENTS: 'api/v1.1/attendee_payments/',
            ATTENDEE_REFUND_PAYMENT: 'api/v1.1/refund_attendee_payment/',
            MERCHANT_AGREEMENT_CRUD: 'api/v1.1/merchant_agreement/',
            GET_ALL_CUSTOM_FIELDS_URL: 'api/field/list/',
            CREATE_CUSTOM_FIELDS_URL: 'api/field/create/',
            DELETE_CUSTOM_FIELDS_URL: 'api/field/delete/',
            UPDATE_CUSTOM_FIELDS_URL: 'api/field/update/',
            UPDATE_CONFERENCE_ATTENDEE_URL: 'api/attendee/update/',
            REMOVE_CONFERENCE_ATTENDEE_URL: 'api/attendee/multi_remove',
            CREATE_CONFERENCE_ATTENDEE_URL: 'api/attendee/create/',
            UPLOAD_SIGNATURE_URL: 'api/file/sign/',
            UPLOAD_URL: 'https://s3.amazonaws.com/expo-images/',
            UPDATE_USER_PROFILE: 'api/account/update/',
            UPDATE_USER_PROFILE_V1_1: 'api/v1.1/account/',
            SESSION_ADD_FILE: 'api/session/add_file/',
            SESSION_REMOVE_FILE: 'api/session/delete_file/',
            SESSION_ATTENDEE_LIST_URL: 'api/session/attendees_list',
            SESSION_ADD_ATTENDEE_URL: 'api/session/attendees_add',
            SESSION_SCAN_ATTENDEE_URL: 'api/session/attendees_scan',
            SESSION_REMOVE_ATTENDEE_URL: 'api/session/attendees_remove',
            SESSION_REMOVE_ATTENDEE_SCANNED_URL: 'api/session/scanned_attendees_remove',
            SESSION_UPDATE_ATTENDEE_URL: 'api/session/attendee_update',
            SESSION_CUSTOM_FIELDS_CRUD: 'api/v1.1/custom_session_fields/',
            SESSION_CUSTOM_FIELD_CRUD: 'api/v1.1/custom_session_field/',
            USER_REGISTER_PAYMENT: 'api/account/add_stripe/',
            CREATE_PROMO_CODES: 'api/exhibitor/promo_codes_create/',
            CREATE_PROMO_CODES_UPDATED: 'api/exhibitor/promo_codes_create_updated/',
            GET_PROMO_CODES: 'api/exhibitor/promo_codes_get/',
            ADD_EXHIBITOR_MATERIAL: 'api/exhibitor/material_add/',
            LIST_EXHIBITOR_MATERIALS: 'api/exhibitor/material_list/',
            REMOVE_EXHIBITOR_MATERIALS: 'api/exhibitor/material_remove/',
            REVOKE_EXHIBITOR: 'api/exhibitor/revoke/',
            RESEND_VERIFICATION_CODE: 'api/account/resend_verification_code/',
            CONFERENCE_SPEAKERS_LIST: 'api/v1.1/conference_speakers/',
            CONFERENCE_SPEAKER_CURD: 'api/v1.1/speaker/',
            CONFERENCE_USER_JOIN: 'api/user/join/',
            DOWNLOAD_ATTENDEES: 'api/v1.1/download_attendees/',
            CONFERENCE_ADD_TEMPLATE: 'api/conference/template_add/',
            CONFERENCE_REMOVE_MULTIPLE_ATTENDEES: 'api/attendee/multi_remove/',
            CONFERENCE_REMOVE_MULTIPLE_USERS: 'api/user/multi_remove/',
            CONFERENCE_LIST_TEMPLATE: 'api/conference/template_list/',
            CONFERENCE_REMOVE_TEMPLATE: 'api/conference/template_remove/',
            CONFERENCE_LIST_TRANSACTIONS: 'api/conference/transactions_list/',
            CONFERENCE_BULK_CREATE_ATTENDEE: 'api/attendee/bulk_create/',
            CONFERENCE_BULK_CREATE_ATTENDEE_IN_SESSION: 'api/v1.1/session_upload_attendees/',
            CONFERENCE_BULK_CREATE_SESSION: 'api/v1.1/sessions_bulk/',
            CONFERENCE_CONFERENCE_HOUR_SET: 'api/conference/hours_set/',
            CONFERENCE_CONTINUING_EDUCATIONS_CRUD: 'api/v1.1/continuing_educations/',
            CONFERENCE_CONTINUING_EDUCATION_CRUD: 'api/v1.1/continuing_education/',
            CONFERENCE_SOCIAL_TAGS_CRUD: 'api/v1.1/social_tags/',
            LIST_ATTENDEE_SESSIONS: 'api/session/sessions_list/',
            LIST_SCANNED_ATTENDEES: 'api/session/scanned_attendees_list/',
            LIST_SCANNED_SESSIONS: 'api/session/scanned_list/',
            GET_REGISTRATION_FORM: 'api/registration/get_form/',
            CREATE_REGISTRATION_FORM_LEVEL: 'api/v1.1/registration_levels/',
            REGISTRATION_LEVEL: 'api/v1.1/registration_level/',
            SESSIONS_REGISTRATION_LEVEL: 'api/v1.1/registration_level_sessions/',
            CREATE_REGISTRATION_FORM_BENEFITS: 'api/v1.1/registration_level_benefits/',
            START_REGISTRATION_FORM: 'api/registration/create/',
            SET_REGISTRATION_FIELDS: 'api/registration/set_fields/',
            BUY_CONFERENCE_USERS: 'api/user/users_buy/',
            REGISTRATION_OPEN: 'api/v1.1/registration_form_open/',
            REGISTRATION_FIRST_CALL: 'api/v1.1/registration_form_open_first_page/',
            REGISTRATION_SECOND_CALL: 'api/v1.1/registration_form_open_after_first_page/',
            EXHIBITOR_LIST_BULK_UPLOAD: 'api/v1.1/exhibitor_companies/',
            GET_CONFERENCE_BY_CODE: 'api/v1.1/conference_get_by_code/',
            JOIN_AS_COMPANY: 'api/v1.1/company/',
            EXHIBITOR_JOIN_CONFERENCE: 'api/exhibitor/join/',
            GET_EXHIBITOR_COMPLETE: 'api/exhibitor/complete_data/',
            CREATE_EXHIBITOR_USER: 'api/exhibitor/user_add/',
            DELETE_EXHIBITOR_USER: 'api/v1.1/exhibitor_users/',
            UPDATE_EXHIBITOR_USER: 'api/exhibitor/user_update/',
            UPDATE_EXHIBITOR_COMPANY_INFORMATION: 'api/exhibitor/update',
            UPDATE_EXHIBITOR_COMPANY_INFORMATION_V11: 'api/v1.1/exhibitor/',
            ASSIGN_PROMO_CODE: 'api/exhibitor/promo_codes_assign_updated/',
            EXHIBITOR_PAY_NEW: 'api/v1.1/exhibitor_payments/',
            REGISTRATION_FORMS: 'api/v1.1/registration_forms/',
            BENEFITS_CRUD: 'api/v1.1/registration_level_benefits/',
            BENEFIT_CRUD: 'api/v1.1/registration_level_benefit/',
            PRICINGS_CRUD: 'api/v1.1/registration_level_pricings/',
            PRICING_CRUD: 'api/v1.1/registration_level_pricing/',
            ADD_ON_SESSIONS_CRUD: 'api/v1.1/registration_level_add_on_sessions/',
            ADD_ON_SESSION_CRUD: 'api/v1.1/registration_level_add_on_session/',
            ADD_ON_SESSION_PRICINGS_CRUD: 'api/v1.1/registration_level_add_on_session_pricings/',
            ADD_ON_SESSION_PRICING_CRUD: 'api/v1.1/registration_level_add_on_session_pricing/',
            REGISTRATION_LEVEL_PROMO_CODES_CRUD: 'api/v1.1/registration_level_promo_codes/',
            REGISTRATION_LEVEL_PROMO_CODE_CRUD: 'api/v1.1/registration_level_promo_code/',
            REGISTRATION_LEVEL_MEMBER_TEST: 'api/v1.1/registration_level_members_test/',
            SESSION_FEEDBACK_LIST: 'api/v1.1/attendee_feedbacks/',
            SIGN_MQTT: 'api/v1.1/get_signed_url/',
            COGNITO_GET: 'api/v1.1/get_cognito_identity/',
            REGISTRATION_LEVEL_MEMBERS_CRUD: 'api/v1.1/registration_level_members/',
            REGISTRATION_FORM_UPDATE: 'api/v1.1/registration_form_update/',
            REGISTRATION_FORM_COMPLETE: 'api/v1.1/registration_form_complete/',
            REGISTRATION_FORM_DOCUMENTS_CRUD: 'api/v1.1/registration_form_documents/',
            REGISTRATION_FORM_DOCUMENT_CRUD: 'api/v1.1/registration_form_document/',
            CONFERENCE_HOURS_LIST: 'api/v1.1/conference_hours/',
            REGISTRATION_FORM_STATS: 'api/v1.1/registration_form_stats/',
            REGISTRATION_TRANSFERS: 'api/v1.1/registration_transfers/',
            REGISTRATION_FORM_CALL: 'api/v1.1/register_from_iframe/',
            EXHIBITOR_BOOTH: 'api/v1.1/exhibitor_actions/',
            VALIDATE_PROMO_CODE: 'api/v1.1/validate_promo_code/',
            EXHIBITOR_WEB_LIST: 'api/v1.1/exhibitor_web_list/',
            PREVIOUS_EXHIBITOR: 'api/v1.1/exhibitor_previous/',
            EXHIBITOR_REVOKE_ACCESS: 'api/v1.1/exhibitors_revoke/',
            EXHIBITOR_CRUD: 'api/v1.1/exhibitors/',
            HOTELS_CRUD: 'api/v1.1/hotels/',
            HOTEL_CRUD: 'api/v1.1/hotel/',
            CAR_RENTALS_CRUD: 'api/v1.1/car_rentals/',
            CAR_RENTAL_CRUD: 'api/v1.1/car_rental/',
            AIRLINES_CRUD: 'api/v1.1/airlines/',
            AIRLINE_CRUD: 'api/v1.1/airline/',
            TRANSPORTATIONS_CRUD: 'api/v1.1/transportations/',
            TRANSPORTATION_CRUD: 'api/v1.1/transportation/',
            GET_ALL_CONFERNCE_BADGES: 'api/v1.1/conferences/',
            //superadmin
            SUPERADMIN_USERS_GET: 'api/v1.1/super_admin/users/',
            SUPERADMIN_USER_SUSPEND: 'api/v1.1/super_admin/suspend_user/',
            SUPERADMIN_USER_UNSUSPEND: 'api/v1.1/super_admin/unsuspend_user/',
            SUPERADMIN_USER_CHANGE_PASS: 'api/v1.1/super_admin/update_user_password/',
            SUPER_ADMIN_REVENUE_GET: '/api/v1.1/super_admin/conference/',
            SESSION_UPDATE_COMPLETE: 'api/v1.1/session_complete_update/',
            SESSION_CREATE_COMPLETE: 'api/v1.1/session_complete/',
            STRIPE_KEY: urlConfigurations.getStripePublishableKey(),
            UNITED_STATES: unitedStates,
            COUNTRIES: countries,
            TIMEZONES: timezones,
            GOOGLE_API_KEY: 'AIzaSyA0dA47GflyWNZ4nbB91WDhFyygDLYo9qU'
        })
        .constant('AUTH_URL_DEV', urlConfigurations.getAuthUrl())
        .constant('CLIENT_ID_DEV', '3XTNVEmw9cLkhUfLSb5Ar3dCd5DgPansVxm9EDqh')
        .constant('BASE_URL', urlConfigurations.getBaseUrl())
        .constant('GET_UPCOMING_CONFERENCES_URL', 'api/conference/upcoming/')
        .constant('GET_ALL_CONFERENCES_URL', 'api/conference/upcoming/')
        .constant('CREATE_CONFERENCE_URL', 'api/conference/create/')
        .constant('UPDATE_CONFERENCE_URL', 'api/conference/update/')
        .constant('GET_CONFERENCE_BY_ID', 'api/conference/get/')
        .constant('REGISTER_URL_DEV', urlConfigurations.getRegisterUrl())
        .constant('EVENTS', {
            DELETE_CONFERENCE_CONFIRMED: 'DELETE_CONFERENCE_CONFIRMED',
            ADD_CONFERENCE_SUPPORT_CONTACT: 'ADD_CONFERENCE_SUPPORT_CONTACT',
            UPDATE_CONFERENCE_SUPPORT_CONTACT: 'UPDATE_CONFERENCE_SUPPORT_CONTACT',
            DELETE_CONFERENCE_SUPPORT_CONTACT: 'DELETE_CONFERENCE_SUPPORT_CONTACT',
            DELETE_CONFERENCE_USERS: 'DELETE_CONFERENCE_USERS',
            DELETE_ALL_CONFERENCE_USERS: 'DELETE_ALL_CONFERENCE_USERS'
        })
        .filter('searchInName', function() {
            return function(attendees, chunck) {
                var attendeesSearched = [],
                    i,
                    chunckMatch = new RegExp(chunck, 'i');

                for (i = 0; i < attendees.length; i++) {
                    var piceOfName = attendees[i].full_name;

                    if (chunckMatch.test(piceOfName.substr(0, chunck.length))) {
                        attendeesSearched.push(attendees[i]);
                    }
                }
                return attendeesSearched;
            };
        })
        .filter('SearchWord', function() {
            return function(registrantProfile, searchedWord) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(searchedWord, 'i');
                for (i = 0; i < registrantProfile.length; i++) {
                    var companyInfo = registrantProfile[i].company,
                        lastName = registrantProfile[i].full_name.split(" ")[1],
                        email = registrantProfile[i].email;

                    if (searchedWordMatch.test(companyInfo.substr(0, searchedWord.length)) ||
                        searchedWordMatch.test(lastName.substr(0, searchedWord.length)) ||
                        searchedWordMatch.test(email.substr(0, searchedWord.length))) {

                        info.push(registrantProfile[i]);
                    }
                }
                return info;
            };
        })
        .filter('SearchAttendee', function() {
            return function(registrantProfile, attendee_info) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(attendee_info, 'i');
                for (i = 0; i < registrantProfile.length; i++) {
                    var companyInfo = registrantProfile[i].company_name,
                        lastName = registrantProfile[i].last_name,
                        email = registrantProfile[i].email_address;
                    if (companyInfo === null) {
                        companyInfo = '';
                    }
                    if (lastName == null) {
                        lastName = '';
                    }
                    if (email === null) {
                        email = '';
                    }
                    if (searchedWordMatch.test(companyInfo.substr(0, attendee_info.length)) ||
                        searchedWordMatch.test(lastName.substr(0, attendee_info.length)) ||
                        searchedWordMatch.test(email.substr(0, attendee_info.length))) {

                        info.push(registrantProfile[i]);
                    }
                }
                return info;
            };
        })
        .filter('SearchAttendeeByFirstName', function() {
            return function(attendeeProfile, attendee_firstName) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(attendee_firstName, 'i');

                for (i = 0; i < attendeeProfile.length; i++) {
                    var firstName = attendeeProfile[i].first_name;
                    if (firstName === null) {
                        firstName = '';
                    }
                    if (searchedWordMatch.test(firstName.substr(0, attendee_firstName.length))) {
                        info.push(attendeeProfile[i]);
                    }
                }
                return info;
            };
        })
        .filter('SearchSessionRegistrant', function() {
            return function(registrantProfile, registrant_info) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(registrant_info, 'i');
                for (i = 0; i < registrantProfile.length; i++) {
                    var companyInfo = registrantProfile[i].attendee.company_name,
                        lastName = registrantProfile[i].attendee.last_name,
                        email = registrantProfile[i].attendee.email_address;

                    if (email === null) {
                        email = '';
                    }
                    if (lastName === null) {
                        lastName = '';
                    }
                    if (companyInfo === null) {
                        companyInfo = '';
                    }
                    if (searchedWordMatch.test(companyInfo.substr(0, registrant_info.length)) ||
                        searchedWordMatch.test(lastName.substr(0, registrant_info.length)) ||
                        searchedWordMatch.test(email.substr(0, registrant_info.length))) {

                        info.push(registrantProfile[i]);
                    }
                }
                return info;
            };
        })
        .filter('SearchExhibitor', function() {
            return function(registrantProfile, exhibitor_info) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(exhibitor_info, 'i');

                for (i = 0; i < registrantProfile.length; i++) {
                    var companyInfo = registrantProfile[i].company_name;
                    var email = registrantProfile[i].admin_email;

                    if (registrantProfile[i].admin !== null) {
                        var lastName = registrantProfile[i].admin.last_name;
                    } else {
                        lastName = '';
                    }

                    if (companyInfo === null) {
                        companyInfo = '';
                    }
                    if (email === null) {
                        email = '';
                    }
                    if (searchedWordMatch.test(companyInfo.substr(0, exhibitor_info.length)) ||
                        searchedWordMatch.test(lastName.substr(0, exhibitor_info.length)) ||
                        searchedWordMatch.test(email.substr(0, exhibitor_info.length))) {

                        info.push(registrantProfile[i]);
                    }
                }
                return info;
            }
        })
        .filter('formatLabel', function() {
            return function(name) {
                var string = '';
                if (name.search('_') > -1) {
                    var words = name.split('_');
                    _.each(words, function(word) {
                        word = word.replace(/^.*?([a-z])/, word[0].toUpperCase());
                        string = string.concat(word, ' ');
                    });
                    name = string;
                    string = '';
                } else {
                    name = name.replace(/^.*?([a-z])/, name[0].toUpperCase());
                }
                return name;
            }
        })
        .filter('moment', function() {
            return function(dateString, format, timezone_data) {
                var offsetInMinutes = lib.timezoneToOffset(timezone_data);
                var dateStringObj = _.clone(new Date(dateString));
                dateStringObj.setTime(dateStringObj.getTime() + (dateStringObj.getTimezoneOffset() + offsetInMinutes) * 60 * 1000);
                return moment(dateStringObj).format(format);
            };
        })
        .filter('orderObjectBy', function() {
            return function(items, field, key, reverse) {
                var filtered = [];
                items.sort(function(a, b) {
                    return (a[field][key] > b[field][key] ? 1 : -1);
                });
                if (reverse) items.reverse();
                return items;
            }
        })
        .filter('SearchSupportContacts', function() {
            return function(exhibitorSupportContacts, support_contact_info) {
                var info = [],
                    i,
                    searchedWordMatch = new RegExp(support_contact_info, 'i');
                for (i = 0; i < exhibitorSupportContacts.length; i++) {
                    var lastName = exhibitorSupportContacts[i].last_name,
                        firstName = exhibitorSupportContacts[i].first_name,
                        email = exhibitorSupportContacts[i].email;
                    if (lastName == null) {
                        lastName = '';
                    }
                    if (email === null) {
                        email = '';
                    }
                    if (searchedWordMatch.test(firstName.substr(0, support_contact_info.length)) ||
                        searchedWordMatch.test(lastName.substr(0, support_contact_info.length)) ||
                        searchedWordMatch.test(email.substr(0, support_contact_info.length))) {

                        info.push(exhibitorSupportContacts[i]);
                    }
                }
                return info;
            };
        });
})();