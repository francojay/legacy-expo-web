(function(){
	'use strict';
	angular
		.module('app')
		.directive('useravatar', useravatarDirective);

	function useravatarDirective(){
		return {
			restrict: 'E',
			templateUrl: 'app/directives/useravatar/useravatar.html',
			controller: ['$scope', 'SharedProperties', 'localStorageService', '$location', 'ngDialog', 'FileService',
			useravatarDirectiveController],
			scope: {
                image: "=",
                firstname: "=",
                size: "@",
                fontsize: "@",
                lastname: "="
            },
			replace: true
		}
	};

	function useravatarDirectiveController($scope, SharedProperties, localStorageService, $location, ngDialog,
	FileService){
		var sharedData = SharedProperties.sharedObject;
		var current_page = sharedData.currentPageTitle;
		var vm = this;

		if (!$scope.size) {
		    $scope.size = "35px"
		}

		if (!$scope.fontsize) {
            $scope.fontsize = "14px"
        }

		if ($scope.firstname && $scope.lastname) {
		}

		$scope.$watch('firstname', function(newFirstName, oldFirstName) {
			if (newFirstName) {
				$scope.initials = $scope.firstname[0].toUpperCase() + $scope.lastname[0].toUpperCase();
			}
		})

		$scope.$watch('lastname', function(newLastName, oldLastName) {
			if (newLastName) {
				$scope.initials = $scope.firstname[0].toUpperCase() + newLastName[0].toUpperCase();
			}
		})

	}
}());
