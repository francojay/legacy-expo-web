(function(){
	'use strict'
		angular
			.module('app')
			.directive('confTab1', confTabDirective);
			function confTabDirective (){
				return{
					restrict: 'E',
					replace: true,
					templateUrl: 'app/directives/confTab/confTab.html'
				}
			};
})();