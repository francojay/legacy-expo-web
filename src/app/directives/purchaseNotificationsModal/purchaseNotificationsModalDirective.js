(function(){
	'use strict';

    var directiveId = 'purchaseNotificationsModal';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'SharedProperties', '$location', '$state', '$stateParams', 'ngDialog', 'manageNotificationsService', 'toastr', 'ConferenceService', purchaseNotificationsModalDirective]);

	function purchaseNotificationsModalDirective($rootScope, SharedProperties, $location, $state, $stateParams, ngDialog, manageNotificationsService, toastr, ConferenceService) {
		return {
			restrict: 'E',
			templateUrl: 'app/directives/purchaseNotificationsModal/purchaseNotificationsModalView.html',
			replace: true,
			link: link
		}

        function link(scope) {
        	var conferenceId = ConferenceService.hash_conference_id.decode($stateParams.conference_id)[0];
			scope.amountOfCredits = scope.ngDialogData.amountOfCredits;
			scope.finalPrice = scope.ngDialogData.finalPrice;
			scope.lastFourDigits = scope.ngDialogData.lastFourDigits;
			scope.nrPacks = scope.amountOfCredits / 1000;


			var stripeToken = scope.ngDialogData.stripeToken;

			scope.calculateFinalPrice = function(nrPacks) {
				if (nrPacks <= 0) {
					scope.nrPacks = 1;
					nrPacks = 1;
				}
				scope.amountOfCredits = nrPacks * 1000;
				var priceForOneBatch = 50; // batch = 1000 credits
                var numberOfBatches = scope.amountOfCredits / 1000;

                var discountPercentage = scope.amountOfCredits >= 5000 ? 10 : 0;
                var finalPrice = numberOfBatches * priceForOneBatch - (discountPercentage/100 * numberOfBatches * priceForOneBatch);


                scope.finalPrice = finalPrice;
			}

			scope.calculateFinalPrice(scope.nrPacks);

			scope.purchaseNotifications = function(amountOfCredits) {
				manageNotificationsService.buyCredits(conferenceId, scope.amountOfCredits, stripeToken).then(function(response) {

					ngDialog.closeAll();

					$rootScope.$broadcast('BOUGHT_CREDITS', {
						numberOfCredits: response.data.nr_credits
					});

					$state.go('conferences.dashboard.notifications.manage', {
						conference_id: $stateParams.conference_id
					})
				})
			}
        }
	};

}());
