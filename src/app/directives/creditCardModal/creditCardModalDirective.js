// (function(){
// 	'use strict';
//
//     var directiveId = 'creditCardModal';
// 		var app 				= angular.module('app');
// 		var dependenciesArray = [
// 			'SharedProperties',
// 			'$location',
// 			'$state',
// 			'$stateParams',
// 			'ngDialog',
// 			'supportService',
// 			'toastr',
// 			creditCardModalDirective
// 		];
//
// 		app.directive(directiveId, dependenciesArray);
//
// 	function creditCardModalDirective(SharedProperties, $location, $state, $stateParams, ngDialog, supportService, toastr) {
// 		return {
// 			restrict: 'E',
// 			templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/payments/_components/creditCardModal/creditCardModalView.html',
// 			replace: true,
// 			link: link
// 		}
//
//         function link(scope) {
//
//             var amountOfCredits = scope.ngDialogData.amountOfCredits;
//             var finalPrice = scope.ngDialogData.finalPrice;
//
//             scope.closeDialog = function() {
//                 ngDialog.closeAll();
//             };
//
// 						scope.confirmCreditCard = function() {
//                 if (verifyEmptyCardInputs()) {
//                     var form = angular.element('#payment-form');
//                     Stripe.card.createToken(form, responseHandler);
//                 } else {
//
//                     var holderName = document.querySelectorAll("[data-stripe='name']")[0];
//                     if(_.isEmpty(holderName.value)){
//                         toastr.error('Please input the credit card holder\'s name.');
//                     }
//
//                     var zipCode = document.querySelectorAll("[data-stripe='address_zip']")[0];
//                     if(_.isEmpty(zipCode.value)){
//                         toastr.error('Please input the credit card\'s zip code.');
//                     }
//
//                     var cvv = document.querySelectorAll("[data-stripe='cvc']")[0];
//                     if(_.isEmpty(cvv.value)){
//                         toastr.error('Please input the security code.');
//                     }
//
//                     var expiration = document.querySelectorAll("[data-stripe='exp']")[0];
//                     if(_.isEmpty(expiration.value)){
//                         toastr.error('Please input the credit card\'s expiration date.');
//                     }
//
//                     var number = document.querySelectorAll("[data-stripe='number']")[0];
//                     if(_.isEmpty(number.value)){
//                         toastr.error('Please input the credit card number.');
//                     }
//
//                 }
//
// 			};
//
// 			scope.checkTypeOfCard = function(){
// 	            var number = document.getElementById('CCN').value;
//                 number = number.toString();
//                 if(_.isEmpty(number) === false){
//                     if((number.indexOf(4) === 0) && (number.length === 13 || number.length === 16)){
//                         scope.CCNType = 'visa';
//                     } else if((number.indexOf(5) === 0) && (number.charAt(1) >= 1 || number.charAt(1) <= 5) && (number.length === 16)){
//                         scope.CCNType = 'master';
//                     } else if((number.length === 16) && ((_.inRange(number.substring(0, 8), 60110000,  60119999)) ||
//                         (_.inRange(number.substring(0, 8), 65000000,  65999999)) ||
//                         (_.inRange(number.substring(0, 8), 62212600,  62292599)) ) ){
//                         scope.CCNType = 'discover';
//                     } else if((number.indexOf(3) === 0) && ((number.indexOf(4) === 1 || number.indexOf(7) === 1)) && (number.length === 15)){
//                         scope.CCNType = 'american_exp'
//                     } else if(((number.substring(0,4) == 3088) || (number.substring(0,4) == 3096) || (number.substring(0,4) == 3112) ||
//                         (number.substring(0,4) == 3158) || (number.substring(0,4) == 3337) || (_.inRange(number.substring(0,8),35280000, 35899999))) &&
//                         (number.length === 16)){
//                         scope.CCNType = 'jcb';
//                     } else if((number.indexOf(3) === 0) && (number.indexOf(0) === 1 || number.indexOf(6) === 1 || number.indexOf(8) === 1) && (number.length === 14)){
//                         scope.CCNType = 'diners';
//                     } else {
//                         scope.CCNType = '';
//                     }
//                 } else{
//                     scope.CCNType = '';
//                 }
//         	}
//
//             function verifyEmptyCardInputs(){
//                 var ok = true;
//                 var fields_counter = 0;
//                 _.each(document.getElementsByClassName('required-payment-field'), function(required_field){
//                     if(!_.isEmpty(required_field.value)){
//                         fields_counter++;
//                     }
//                 });
//                 if(fields_counter < 5){
//                     ok = false;
//                 }
//                 return ok;
//             }
//
//             function responseHandler(status, response) {
//                 if (response.error) {
//                     toastr.error(response.error.message);
//                 } else {
//                     var stripeToken = response.id;
//                     var lastFourDigits = response.card.last4;
//
//                     ngDialog.open({
//                         plain: true,
//                         template: "<purchase-notifications-modal></purchase-notifications-modal>",
//                         className: 'app/stylesheets/_plusAttendee.scss',
//                         data: {
//                             stripeToken: stripeToken,
//                             lastFourDigits: lastFourDigits,
//                             amountOfCredits: amountOfCredits,
//                             finalPrice: finalPrice
//                         }
//                     });
//                 }
//             }
//         }
// 	};
//
// }());
