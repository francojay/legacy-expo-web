// (function () {
//     'use strict';
//     var directiveId = 'mobileTemplate';

//     angular
//         .module('app')
//         .directive(directiveId, ['$rootScope', 'SharedProperties', mobileTemplateDirective]);
//     function mobileTemplateDirective($rootScope, SharedProperties) {
//         return {
//             restrict: 'E',
//             replace: true,
//             templateUrl: 'app/directives/mobileTemplate/mobileTemplateView.html',
//             scope: {
//                 overlayMode: '@overlayMode',
//                 displayOverlay: '=displayOverlay',
//                 textColor: '=textColor',
//                 backgroundDefaultNumber: '=backgroundDefaultNumber',
//                 backgroundImages: '=backgroundImages'
//             },
//             link: link
//         };

//         function link(scope) {
//             scope.currentConference = SharedProperties.conference;
//             scope.coverScreenOptions = SharedProperties.coverScreenOptions;

//             var sliderIndex = parseInt(scope.backgroundDefaultNumber) - 1; // -1 because when translating we start from 0 * width [the first image isn't translated]
//             var slidingValue = -375;

//             scope.$watch(function(){return SharedProperties.coverScreensImagesOptions}, function (images) {
//                 scope.backgroundImages = images;
//                 scope.numberOfImages = scope.backgroundImages.length;
//                 scope.sliderWidth = scope.backgroundImages.length * 100;
//             });

//             scope.showPrevButton = true;
//             scope.showNextButton = true;

//             scope.currentIndex = sliderIndex + 1;

//             scope.numberOfImages = scope.backgroundImages.length;
//             scope.sliderWidth = scope.backgroundImages.length * 100;
            
//             scope.currentTime = moment().format('HH:mm');
//             scope.percentage = 100;

//             initializeSlider(sliderIndex);
//             initializeConferenceInformation(scope.currentConference);

//             scope.nextSlide = function() {
//                 if (sliderIndex < scope.backgroundImages.length - 1) {
//                     sliderIndex++;
//                     scope.currentIndex++;
//                     scope.showPrevButton = true;
//                     if (sliderIndex === scope.backgroundImages.length - 1) {
//                         scope.showNextButton = false;
//                     }
//                 } else {
//                     scope.showNextButton = false;
//                 }
//                 scope.translateValue = sliderIndex * slidingValue;

//                 SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;               
//             };

//             scope.prevSlide = function() {
//                 if (sliderIndex > 0) {
//                     sliderIndex--;
//                     scope.currentIndex--;
//                     scope.showNextButton = true;
//                     if (sliderIndex === 0) {
//                         scope.showPrevButton = false;
//                     }
//                 } else {
//                     scope.showPrevButton = false;
//                 }
//                 scope.translateValue = sliderIndex * slidingValue;

//                 SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;
//             };

//             function initializeSlider(passedIndex) {
//                 if (passedIndex === 0) {
//                     scope.showPrevButton = false;
//                 } else if (passedIndex === scope.numberOfImages) {
//                     scope.showNextButton = false;
//                 }

//                 scope.translateValue = sliderIndex * slidingValue;
//                 SharedProperties.coverScreenOptions.defaultImageIndex = scope.currentIndex;
                
//                 setInterval(function() {
//                     scope.currentTime = moment().format('HH:mm');
//                     scope.percentage--;
//                     scope.$apply();
//                 }, 20000);          
//             }

//             function initializeConferenceInformation(conference) {
//                 scope.conferenceDates = moment(scope.currentConference.date_from).format('M/D/YYYY') + ' - ' +  moment(scope.currentConference.date_to).format('M/D/YYYY');
//             }

//             $rootScope.$on('overlayModeChanged', function(event, data) {
//                 scope.overlayMode = data.overlayMode;
//             })
//         }
//     }
// })();