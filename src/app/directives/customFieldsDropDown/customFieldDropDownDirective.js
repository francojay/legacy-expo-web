(function() {
    'use strict';

    var directiveId = 'customFieldDropDown';
    angular
        .module('app')
        .directive(directiveId, customFieldDropDownDirective);

    function customFieldDropDownDirective(){
        return {
            restrict: 'E',
            templateUrl: 'app/directives/customFieldsDropDown/customFieldDropDownView.html',
            scope: {
                'customField': "="
            },
            replace: true,
            link: link
        };

        function link(scope){
            scope.isSingleSelect = scope.customField.custom_field.type === 2;
            scope.showCustomFieldDropDown = false;
            scope.customFieldValue = "";
            scope.customFieldValueList = [];

            scope.$on('TOGGLE_THIS_DROP_DOWN', function(TOGGLE_THIS_DROP_DOWN){
                scope.showCustomFieldDropDown = !scope.showCustomFieldDropDown;
            });

            scope.chooseOption = function(option){
                if(option.hasOwnProperty('checked')){
                   option.checked = !option.checked;
                } else{
                    option.checked = true;
                }
                
                if(scope.isSingleSelect){
                    _.each(scope.customField.custom_field.customattendeefieldoption_set, function(custom_field_option){
                        if(option.id !== custom_field_option.id) {
                            custom_field_option.checked = false;
                        }
                    })
                }

                var customFieldValueList = [];

                _.each(scope.customField.custom_field.customattendeefieldoption_set, function(custom_field_option){
                    if(custom_field_option.checked) {
                        customFieldValueList.push(custom_field_option.value);
                    }
                });

                var customFieldValueTxt = customFieldValueList.join(); 

                scope.customField.selectedValues = customFieldValueList;
                scope.customField.customFieldValueTxt = customFieldValueTxt;

                scope.$emit('SELECTED_OPTION_FOR_THIS_CUSTOM_FIELD', scope.customField);
            };

        }
    }
})();