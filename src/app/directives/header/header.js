(function(){
	'use strict';
	angular
		.module('app')
		.directive('header', headerDirective);

	function headerDirective(){
		return {
			restrict: 'E',
			templateUrl: 'app/directives/header/header.html',
            scope: {
                'conferenceDetails' : "=conferenceDetails",
                'edit' : "=edit"
            },
			controller: ['$scope', '$rootScope', 'SharedProperties', 'localStorageService', '$location', 'ngDialog', 'FileService', 'UserService', 'ConferenceService',
            'toastr', '$state', 'merchantService', 'API', '$q', headerDirectiveController],
			replace: true
		}
	};

	function headerDirectiveController($scope, $rootScope, SharedProperties, localStorageService, $location, ngDialog, FileService, UserService, ConferenceService,
        toastr, $state, merchantService, API, $q){
		var sharedData = SharedProperties.sharedObject;
		var current_page = sharedData.currentPageTitle;

		var vm = this;

        $scope.user = SharedProperties.user;
        vm.originalConference = null;
        vm.status = 'not_started';

        var eventData = {
            'eventCreated': false,
            'eventName': "",
            'superAdminLink': "",
            'eventDate': ""
        }

        if ($scope.user.has_created_event) {
            eventData['eventCreated'] = true;
        }

        window.Intercom('boot', {
           app_id: 'ij3yrbte',
           email: $scope.user.email,
           user_hash: $scope.user.user_signed,
           expo_user_id: $scope.user.id,
           name: $scope.user.first_name + ' ' + $scope.user.last_name,
           created_at: 1234567890,
           custom_launcher_selector: '#my_custom_link',
           "eventCreated": eventData.eventCreated
        });

        $scope.coverScreenOptions = SharedProperties.coverScreenOptions;


        vm.getMerchantAgreementStatus = function(popup_scope) {
            ConferenceService
                .getMerchantAgreementResponse()
                .then(function(response){
                    if(response.hasOwnProperty('status')){
                        popup_scope.status = response.status;
                    }

                    if(response.hasOwnProperty('verification')){
                        popup_scope.verification = response.verification;
                    }
                })
                .catch(function(error){
                    console.log(error);
                });
        };

        $scope.searchConferences = function(conferenceSearchQuery) {
            if (conferenceSearchQuery.length > 2) {
                SharedProperties.conferenceSearchQuery = conferenceSearchQuery;
            } else if (SharedProperties.conferenceSearchQuery.length > 0 && conferenceSearchQuery == "") {
                SharedProperties.conferenceSearchQuery = "";
            }
        }

        vm.openMerchantAgreementForm = function(){
            ngDialog.close();
            setTimeout(function(){
                merchantService.startMerchantAgreement($scope)
            }
            , 1000);
        };

        $scope.changeConferenceLogo = function(files, conferenceDetails) {
            if (!vm.originalConference) {
                vm.originalConference = _.cloneDeep(conferenceDetails);
            }

            conferenceDetails.logo = files[0].$ngfBlobUrl;
            conferenceDetails.conferenceLogoFile = files[0];
        }

		vm.edit_profile_labels = [
			{label:"First Name", code:'first_name', type: "text", valid: "true"},
            {label:"Last Name", code:'last_name', type: "text", valid: "true" },
            {label:"Job Title", code:'job_title', type: "text"},
            {label:"Email Address", code:'email', type: "email"},
            {label:"Company Name", code:'company_name', type: "text"},
            {label:"Phone Number", code:'phone_number', type: "text"},
            {label:"Street Address", code:'street_address', type: "text"},
            {label:"City", code:'city', type: "text"},
            {label:"State", code:'state', placeH: "--", type: "text"},
            {label:"Zip Code", code:'zip_code'},
            {label:"Country", code:'country', placeH: "Choose a Country", type: "text"}
		];
		vm.main_profile_labels = _.takeRight(vm.edit_profile_labels, 7);
		vm.profileTab = true;
		vm.editProfileInfo = false;

        $scope.is_superuser = localStorageService.get('is_superuser');

		vm.uploadPhotoCropPopup = function(files) {
		    var file = files[0];
		    $scope.file = file;
		    vm.dialog = ngDialog.open({template: 'app/views/partials/conference/crop_image_popup.html',
               className: 'app/directives/userInfo/userInfo.scss',
               scope: $scope
            });
		}

		vm.uploadYourPhoto = function(files) {
            lib.squareifyImage($q, files)
                .then(function(response){
                    var imgGile = response.file;

                    FileService.getUploadSignature(imgGile)
                        .then(function(response){
                            var file_link = response.data.file_link;
                            FileService.uploadFile(imgGile, response)
                                .then(function(uploadResponse){
                                    $scope.userProfile.profile_url = file_link;
                                })
                                .catch(function(error){
                                    console.log(error)
                                });
                        })
                        .catch(function(error){
                            console.log(error);
                        });
                });
		}

		vm.LogOut = function(){

			if(localStorageService.clearAll()){
                window.Intercom("shutdown");
                $location.path('/login');
            }
		}

		$scope.openAccountProfile = function(){
            var popup_scope = $scope;
            vm.getMerchantAgreementStatus(popup_scope);
			$scope.userProfile = _.cloneDeep($scope.user);
			$scope.profileTab = vm.profileTab;
			$scope.editProfile = vm.editProfileInfo;
			$scope.details = vm.main_profile_labels;
			$scope.editDetails = vm.edit_profile_labels;
			$scope.uploadYourPhoto = vm.uploadYourPhoto;
			$scope.uploadPhotoCropPopup = vm.uploadPhotoCropPopup;
            $scope.openMerchantAgreementForm = vm.openMerchantAgreementForm;
			$scope.LogOut = vm.LogOut;
            $scope.all_countries = API.COUNTRIES;
            $scope.united_states = API.UNITED_STATES;
			vm.dialog = ngDialog.open({template: 'app/directives/userInfo/userInfo.html',
               className: 'app/directives/userInfo/userInfo.scss',
               scope: $scope
            });
		};

        // console.log($location.path());
		$scope.$watch(function(){
            return $state.current.url;
        }, function(value){
            // console.log(value);
            if ((value == 'conferences') || (value == 'dashboard')) {
                $scope.join_btn_style = sharedData.headerButtons['dashboard'].joinBtn;
                $scope.right_btn_style = sharedData.headerButtons['dashboard'].rightBtn;
                $scope.display_conference = false;
                $scope.display_join = true;
                $scope.display_create = true;
            } else if (value == 'conference/create') {
                $scope.join_btn_style = sharedData.headerButtons['create_conf'].joinBtn;
                $scope.right_btn_style = sharedData.headerButtons['create_conf'].rightBtn;
                $scope.display_conference = true;
                $scope.display_join = true;
                $scope.display_create = false;
            } else if (value == 'join' || value == 'exhibitor/form') {
                $scope.left_btn_style = sharedData.headerButtons['join_conf'].leftBtn;
                $scope.right_btn_style = sharedData.headerButtons['join_conf'].rightBtn;
                $scope.display_conference = true;
                $scope.display_join = false;
                $scope.display_create = true;
            } else {
                $scope.right_btn_style = sharedData.headerButtons['manage_conf'].rightBtn;
                $scope.display_conference = true;
                $scope.display_join = false;
                $scope.display_create = false;
            }
        });

        vm.validateNewPassword = function(){
            var numberExp = /[0-9]/g;
            if(($scope.userProfile.password.length < 8 || $scope.userProfile.password.length > 16) || (!numberExp.test($scope.userProfile.password))){
                return false;
            } else {
                return true;
            }
        };

        $scope.saveUserData = function() {
            $scope.userProfile = _.omit($scope.userProfile, 'website');
            var ok = true;

			if(!validateEditedUserData()){
				return;
			}

            if(_.isEmpty($scope.userProfile.password)){
                $scope.userProfile = _.omit($scope.userProfile, 'password');
            } else if(!vm.validateNewPassword()){
                ok = false;
            }
            if(ok){
                $scope.user.profile_url = $scope.userProfile.profile_url;
                UserService
                    .updateProfile($scope.userProfile)
                    .then(function(response){
                        SharedProperties.user = response;
                        $scope.user = _.cloneDeep($scope.userProfile);
                        vm.dialog.close();
                        toastr.success("Your account has been updated!");
                    })
                    .catch(function(error){
                        lib.processValidationError(error, toastr);
                    })
            } else{
                toastr.error('The password must contain at least 1 digit', 'Must be longer than 8 characters and shorter than 16 characters');
            }
        }

    	$scope.$watch(function(){
            return SharedProperties.user.profile_url;
            }, function(image){
                if(image != $scope.user.profile_url){
                    $scope.user.profile_url = image;
                }
        });

        $scope.$watch(function(){
            return $scope.edit;
        	}, function(editMode){
        });

		function validateEditedUserData(){
			if(!$scope.userProfile.first_name || $scope.userProfile.first_name.length <= 0){
				toastr.error('The first name is a required field!');
				return false;
			} 
            if (!$scope.userProfile.last_name || $scope.userProfile.last_name.length <= 0) {
                toastr.error('The last name is a required field');
                return false;
            }

            else{
                var ok = true;
                _.forOwn($scope.userProfile, function(value, key) { 
                    if (!value || String(value).trim() == '') {
                        $scope.userProfile[key] = null;
                    }

                    if (value && value.length >= 255) {
                        ok = false;
                        toastr.error(lib.titleCaseForKey(key) + ': ensure this field is no longer that 255 characters!')
                    }
                });

				return ok;
			}
		}
	}
}());
