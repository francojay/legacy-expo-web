(function(){
	'use strict';
	angular
		.module('app')
		.directive('footer', footerDirective);

	function footerDirective(){
		return {
			restrict: 'E',
			templateUrl: 'app/directives/footer/footer.html',
			replace: true,
			controller: ['$scope', 'SharedProperties', '$location', '$state', footerDirectiveController]
		}
	};

	function footerDirectiveController($scope, SharedProperties, $location, $state){
		var current_page = SharedProperties.sharedObject.currentPageTitle;

		$scope.$watch(function(){
            return $state.current.url;
        }, function(value){
            if ((value === 'conferences') || (value === 'dashboard') || (value === 'join')) {
                $scope.display = true;
            } else {
                $scope.display = false;
            }
        });
	}
}());
