(function() {
    'use strict';

    var directiveId = 'hppPageModal';
    var app = angular.module('app');

    var dependenciesArray = [
        '$rootScope',
        '$window',
        '$timeout',
        '$document',
        '$sce',
        hppPageModal
    ];

    app.directive(directiveId, dependenciesArray);

    function hppPageModal($rootScope, $window, $timeout, $document, $sce) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/directives/hppPageModal/hppPageModalView.html',
            link: link
        };

        function link(scope) {
            scope.hpp = angular.copy(scope.ngDialogData);
            $timeout(function() {
                $('.custom-modal-wrapper.add-exhibitor .bottom-part').mCustomScrollbar({ 'theme': 'minimal-dark' });
            }, 0);

            // scope.closeModal = function() {
            //     scope.closeThisDialog();
            // };
            ////REMOVE THIS WHEN TESTING LIVE
            //scope.hpp.consumer_url = "https://login.expopass.io/HPP_interpreted.html";
            scope.consumerUrl = $sce.trustAsResourceUrl(scope.hpp.consumer_url);

        }
    }

})();