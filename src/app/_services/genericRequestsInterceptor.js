'use strict'
var factoryId 					 = "invalidTokenInterceptor";
var module 							 = angular.module('app');

var requestInProgress 	 = false

module
	.factory(factoryId, ['$injector', invalidTokenInterceptor])
	.config(function($httpProvider) {
		$httpProvider.interceptors.push("invalidTokenInterceptor");
	});

function invalidTokenInterceptor($injector) {
  return {
    request: function(config) {
      return config;
    },

    requestError: function(config) {
      return config;
    },

    response: function(res) {
      return res;
    },

    responseError: function(error) {
			console.log(error)
			if (error.status === 401) {
				var $state 			 				= $injector.get('$state');
				var $http 			 				= $injector.get('$http');
				var loginService			  = $injector.get("LoginService");
				var localStorageService = $injector.get("localStorageService");
				var toastr 							= $injector.get("toastr");
				if (error.config.url.indexOf('/token/') > -1) {
					if ($state.current.name !== "account.login") {
						toastr.error("Your token has expired, please login again.");
						localStorageService.set('access_token', null);
						localStorageService.set('refresh_token', null);
						localStorageService.set('expires_in', null);
						$state.go("account.login", {}, {reload: true})
					}  else {
						if ($state.current.name === "account.login") {
							throw error
						}
					}
				} else {
					console.log('nu a incercat sa faca token refresh')
					if (!requestInProgress) {
						console.log('requestul de refresh token se face')
						requestInProgress = true;
						if (localStorage.currentUser) {
							loginService.refresh(JSON.parse(localStorage.currentUser)).then(function(response) {
								requestInProgress = false;
								localStorageService.set('access_token', response.access_token);
								localStorageService.set('refresh_token', response.refresh_token);
								localStorageService.set('expires_in', response.expires_in);

								$state.go($state.current, {}, {reload: true}); //second parameter is for $stateParams
								return response;
							}, function(error) {
								$state.go('account.login');
							});
						} else {
							$state.go('account.login', {errorMessage: "Your credentials were invalidated, please log in again."})
						}

					}
				}
			}

			throw error
    }
  }
}
