(function(){
	'use strict';

	var factoryId = 'requestFactory';

	angular
		.module('app')
		.factory(factoryId, [
			'$http', 'LoginService', 'BASE_URL', '$q', '$location',
			requestFactory
		]);

	function requestFactory($http, LoginService, BASE_URL, $q, $location){

		function joinEndpoints(parts){
			var separator='/';
			var replace = new RegExp(separator+'{1,}', 'g');
			return parts.join(separator).replace(replace, separator);
		}

		function addTrailingSlash(uri) {
			if (uri.indexOf('?') != -1) {
				return uri.replace('//','/');
			} else {
				var lastChar = uri.substr(-1);
				if (lastChar != '/') {
				   uri = uri + '/';
				}
				return uri.replace('//','/');
			}
		}

		function urlEncode(data){
			var kvp = [];
			angular.forEach(data, function(value, key){
				kvp.push([key, encodeURIComponent(value)]);
        	});
        	var encodedData = kvp.map(function(pair) {
          		return pair.join('=');
        	}).join('&');

        	return encodedData;
		}

		return {
			get: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/'); //routes fucked up needed this something bullshit, also affects all api environments that don't start with https://a
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode)	{

							return $http.get(uri, {
							    ignoreLoadingBar: noLoader,
								headers: {
									'Authorization': 'Bearer ' + LoginService.getAccessToken(),
									'Content-Type': 'application/x-www-form-urlencoded',
									'Accept': '*/*'
								}
							}).then(function(response) {
								return response;
							}).catch(function(error) {
								throw error;
							})
				} else {
					return $http.get(uri, {
					    ignoreLoadingBar: noLoader,
						params: data,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					})
					.then(function(response) {
				    return response;
					})
					.catch(function(error) {
            throw error;
          });

				}
			},

			post: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.post(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
					return $http.post(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					});
				}
			},

			put: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.put(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
					return $http.put(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					});
				}
			},

			patch: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
            	var uri = BASE_URL + endpoint;
            	uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');
            	if(encode){

					return $http.patch(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
					return $http.patch(uri, data, {
						ignoreLoadingBar: noLoader,
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Accept': '*/*'
						}
					});
				}
			},

			remove: function(endpoint, data, url_encoded, noLoader){
				noLoader = noLoader || false;
				var encode = url_encoded ? true : false;
				if(encode){
					var data = urlEncode(data);
				}
				var uri = BASE_URL + endpoint;
				uri = addTrailingSlash(uri);
            	uri = uri.replace('//', '/');
            	uri = uri.replace('https:/a', 'https://a');

				if(encode){
					return $http.delete(uri, {
						headers: {
							'Authorization': 'Bearer ' + LoginService.getAccessToken(),
							'Content-Type': 'application/x-www-form-urlencoded',
							'Accept': '*/*'
						}
					});

				} else {
				    if (data && !_.isEmpty(data)) {
				        return $http.delete(uri, {
                            data: data,
														ignoreLoadingBar: noLoader,
                            headers: {
                                'Authorization': 'Bearer ' + LoginService.getAccessToken(),
                                'Accept': '*/*'
                            }
                        });
				    } else {
				        return $http.delete(uri, {
														ignoreLoadingBar: noLoader,
                            headers: {
                                'Authorization': 'Bearer ' + LoginService.getAccessToken(),
                                'Accept': '*/*'
                            }
                        });
				    }

				}
			}

		}
	}

}());
