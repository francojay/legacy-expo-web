(function(){
	'use strict'

  var serviceId = 'fileService';

	angular
		.module('app')
		.service(serviceId, [
			'$q', 'Upload', 'requestFactory', fileService
		]);

		function fileService($q, Upload, requestFactory){
				var service = {};

				service.getUploadSignature = function(file, is_avery) {
          var deferred = $q.defer();
          var file_type = file.type;
          if (is_avery) {
            file_type = 'custom/avery';
          }
          requestFactory.post(
	          'api/file/sign/',
	          {
	              file_type: file_type,
	              is_public: true
	          },
	          false
          ).then(function(response) {
                deferred.resolve(response);
              }, function(error) {
								deferred.reject(error);
							})
            
          return deferred.promise;
       }

			 service.uploadFile = function(file, response) {
         var deferred = $q.defer();
         if (!file.type || file.type == "") {
           var data1 = {
              key: response.data.filename,
              AWSAccessKeyId: response.data.aws_key,
              acl: 'public-read',
              policy: response.data.policy,
              signature: response.data.signature,
              file: file
           }
         } else {
           var data1 = {
              key: response.data.filename,
              AWSAccessKeyId: response.data.aws_key,
              acl: 'public-read',
              policy: response.data.policy,
              signature: response.data.signature,
              "Content-Type": file.type != '' ? file.type : file.type,
              file: file
           }
         }
				 if(!response.data.presigned_url){
            Upload.upload({
               url: API.UPLOAD_URL,
               method: 'POST',
               data: data1
             })
             .then(function(response){
                deferred.resolve(response);
             })
             .catch(function(error){
                deferred.reject(error);
             })
 					} else {
							Upload.http({
							 url: response.data.presigned_url,
							 method: 'PUT',
							 data: file,
							 headers:{
								 'x-amz-acl': 'public-read',
								 'Content-Type': file.type
							 }
						 }).then(function(response){
							deferred.resolve(response);
						 }, function(error) {
						 	deferred.reject(error);
					 });
					 }
        return deferred.promise;
       }


		return service;
	}
})();
