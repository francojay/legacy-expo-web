(function() {
    'use strict';

    var serviceId = 'uploadToS3';

    angular
        .module('app')
        .service(serviceId, [
            '$q', 'fileService', 'toastr', uploadToS3
        ]);
    function uploadToS3($q, fileService, toastr) {
        var service = {};

        service.uploadImage = function(files) {
            fileService
            .getUploadSignature(files[0])
            .then(function(response){
                var file_link = response.data.file_link;
                var promises = [];
                promises.push(fileService.uploadFile(files[0], response));

                $q.all(promises).then(function(){
                    return file_link;
                }).catch(function(error){
                    toastr.error('Error!', 'There has been a problem with your request.');
                    return undefined;
                });
            })
            .catch(function(error){
                if (error.status == 403) {
                    toastr.error('Permission Denied!');
                } else {
                    toastr.error('Error!', 'There has been a problem with your request.');
                }
            });
        };

        return service;
    }
})();
