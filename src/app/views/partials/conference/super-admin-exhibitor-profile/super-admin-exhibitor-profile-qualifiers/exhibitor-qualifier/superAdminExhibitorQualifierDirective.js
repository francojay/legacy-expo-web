(function() {
    'use strict';

    var directiveId = 'superAdminExhibitorQualifier';

    angular
        .module('app')
        .directive(directiveId, superAdminExhibitorQualifier);
    function superAdminExhibitorQualifier() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/views/partials/conference/super-admin-exhibitor-profile/super-admin-exhibitor-profile-qualifiers/exhibitor-qualifier/superAdminExhibitorQualifierView.html',
            scope: {
                exhibitorQualifierData: '=',
                exhibitorQualifierIndex: '='
            },
            link: link
        };

        function link(scope){

            scope.exhibitorQualifiersOptionType = [
                {'name' : 'Answer type: Freeform text', 'id' : 1},
                {'name' : 'Answer type: Single-select', 'id': 2},
                {'name' : 'Answer type: Multi-select', 'id' : 3}
            ];

            scope.removeQualifier = function(){
                scope.$emit("DELETE_EXHIBITOR_QUALIFIER", scope.exhibitorQualifierIndex);
            };

            scope.setQualifierType = function(qualifierData){
                if(qualifierData.type != 1){
                    qualifierData.exhibitorquestionoption_set = [{'value': '', 'order': 1}];
                } else{
                    qualifierData.exhibitorquestionoption_set = [];
                }
            };

            scope.removeQualifierOption = function(index){
                if(scope.exhibitorQualifierData.exhibitorquestionoption_set.length > 1) {
                    scope.exhibitorQualifierData.exhibitorquestionoption_set.splice(index, 1);
                }
                else{
                    scope.exhibitorQualifierData.exhibitorquestionoption_set[index].value = '';
                }
            };

            scope.addQualifierOption = function(qualifierData){
                qualifierData.exhibitorquestionoption_set.push({'value': '', 'order': 1});
            };
        }
    }
}());
