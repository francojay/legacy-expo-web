(function(){
    'use strict';

    var directiveId = 'superAdminExhibitorProfileQualifiers';

    angular
        .module('app')
        .directive(directiveId, ['toastr', 'ConferenceExhibitorsService', 'SharedProperties', superAdminExhibitorProfileQualifiers]);
    function superAdminExhibitorProfileQualifiers (toastr, ConferenceExhibitorsService, SharedProperties){
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'app/views/partials/conference/super-admin-exhibitor-profile/super-admin-exhibitor-profile-qualifiers/superAdminExhibitorProfileQualifiersView.html',
            scope: {
                exhibitorData : '='
            },
            link: link
        };

        function link(scope){
            scope.exhibitorQualifiers = {};

            scope.$on('DELETE_EXHIBITOR_QUALIFIER', function(DELETE_EXHIBITOR_QUALIFIER, index){
                scope.clonedExhibitorQualifiers.exhibitor_questions.splice(index, 1);
            });

            scope.saveAllModificationsOnExhibitorQualifiers = function(){
                var ok = true;
                _.each(scope.clonedExhibitorQualifiers.exhibitor_questions, function(qualifierModel){
                    if(_.isEmpty(qualifierModel.text)){
                        toastr.error('Qualifier text field can not be empty!');
                        ok = false;
                        return false;
                    }
                });
                if(ok) {
                    ConferenceExhibitorsService
                    .modifyExhibitorQualifiers(scope.exhibitorData.id, scope.clonedExhibitorQualifiers)
                    .then(function (response) {
                        _.each(response, function(question){
                            question.exhibitorquestionoption_set = _.filter(question.exhibitorquestionoption_set, ['deleted_at', null]);
                        });
                        scope.exhibitorQualifiers.exhibitor_questions = _.filter(response, ['deleted_at', null]);
                        SharedProperties.currentExhibitorProfileQualifiers = scope.exhibitorQualifiers.exhibitor_questions;
                        scope.clonedExhibitorQualifiers = _.cloneDeep(scope.exhibitorQualifiers);
                    }, function (error) {
                        console.log(error);
                    });
                }
            };

            scope.addExhibitorQualifier = function(){
                scope.clonedExhibitorQualifiers.exhibitor_questions.push({
                    'deleted_at': null,
                    'order': 1,
                    'type': 1,
                    'exhibitorquestionoption_set': [],
                    'exhibitor': scope.exhibitorData.id
                });
            };

            (function(){
                if (!SharedProperties.hasOwnProperty('currentExhibitorProfileQualifiers') ||
                    SharedProperties.currentExhibitorProfileQualifiers.length <= 0 ||
                    SharedProperties.currentExhibitorProfileQualifiers[0].exhibitor != scope.exhibitorData.id) {
                    ConferenceExhibitorsService
                        .getExhibitorQualifiers(scope.exhibitorData.id)
                        .then(function(response){
                            _.each(response, function(question){
                                question.exhibitorquestionoption_set = _.filter(question.exhibitorquestionoption_set, ['deleted_at', null]);
                            });
                            scope.exhibitorQualifiers.exhibitor_questions = _.filter(response, ['deleted_at', null]);
                            SharedProperties.currentExhibitorProfileQualifiers = scope.exhibitorQualifiers.exhibitor_questions;
                            scope.clonedExhibitorQualifiers = _.cloneDeep(scope.exhibitorQualifiers);
                        }, function(error){
                            console.log(error);
                        });
                } else if(SharedProperties.currentExhibitorProfileQualifiers.length >= 0){
                    _.each(SharedProperties.currentExhibitorProfileQualifiers, function(question){
                        question.exhibitorquestionoption_set = _.filter(question.exhibitorquestionoption_set, ['deleted_at', null]);
                    });
                    scope.exhibitorQualifiers.exhibitor_questions =  _.filter(SharedProperties.currentExhibitorProfileQualifiers, ['deleted_at', null]);
                    scope.clonedExhibitorQualifiers = _.cloneDeep(scope.exhibitorQualifiers);
                }
            })();
        }
    }
})();