(function(){
    'use strict';

    var directiveId = 'superAdminExhibitorProfileUsers';

    angular
        .module('app')
        .directive(directiveId, ['toastr', 'ConferenceExhibitorsService', 'SharedProperties', superAdminExhibitorProfileUsers]);
    function superAdminExhibitorProfileUsers (toastr, ConferenceExhibitorsService, SharedProperties){
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'app/views/partials/conference/super-admin-exhibitor-profile/super-admin-exhibitor-profile-users/superAdminExhibitorProfileUsersView.html',
            scope: {
                exhibitorData : '='
            },
            link: link
        };

        function link(scope){

            scope.$on('EXHIBITOR_USER_DELETED', function(EXHIBITOR_USER_DELETED, exhibitor_user_deleted_id){
                scope.exhibitorUsers = _.reject(scope.exhibitorUsers, ['id', exhibitor_user_deleted_id]);
            });

            scope.is_active = false;
            scope.newExhibitorUserPermissions = [
                ['Edit Company Profile', 'edit_company_profile'],
                ['Scan Leads', 'scan_leads'],
                ['Manage Users', 'manage_users'],
                ['Download Leads', 'download_leads']
            ];
            scope.exhibitorUserModel = {
                'edit_company_profile': true,
                'scan_leads': true,
                'manage_users': true,
                'download_leads': true
            };

            scope.changeNewExhibitorUserPermission = function(permission){
                scope.exhibitorUserModel[permission] = !scope.exhibitorUserModel[permission];
            };

            scope.addNewExhibitorUser = function(exhibitorUserModel){
                ConferenceExhibitorsService
                .createExhibitorUsers(scope.exhibitorData.id, exhibitorUserModel)
                .then(function(response){
                    scope.exhibitorUsers.push(response);
                    exhibitorUserModel.invited_user_email = '';
                }, function(error){
                    toastr.error(error.data.detail);
                });
            };

            (function(){
                if (!SharedProperties.hasOwnProperty('currentExhibitorProfileUsers') ||
                    _.isEmpty(SharedProperties.currentExhibitorProfileUsers) ||
                    SharedProperties.currentExhibitorProfileUsers[0].exhibitor != scope.exhibitorData.id) {

                    ConferenceExhibitorsService
                        .getExhibitorUsers(scope.exhibitorData.id)
                        .then(function (response) {
                            if (response) {
                                scope.exhibitorUsers = response;
                                SharedProperties.currentExhibitorProfileUsers = response;
                            }
                        }, function (error) {
                            console.log(error);
                        });
                } else{
                    scope.exhibitorUsers = SharedProperties.currentExhibitorProfileUsers;
                }
            })();

        }
    }
})();