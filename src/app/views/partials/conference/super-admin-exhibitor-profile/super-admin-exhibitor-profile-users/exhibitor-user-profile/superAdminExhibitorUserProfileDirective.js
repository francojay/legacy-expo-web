(function(){
    'use strict';

    var directiveId = 'superAdminExhibitorUserProfile';

    angular
        .module('app')
        .directive(directiveId, ['$rootScope', 'ConferenceExhibitorsService', superAdminExhibitorUserProfile]);
    function superAdminExhibitorUserProfile ($rootScope, ConferenceExhibitorsService){
        return{
            restrict: 'E',
            replace: true,
            templateUrl: 'app/views/partials/conference/super-admin-exhibitor-profile/super-admin-exhibitor-profile-users/exhibitor-user-profile/superAdminExhibitorUserProfileView.html',
            scope: {
                exhibitorUserData : '=',
                exhibitorUserIndex: '='
            },
            link: link
        };

        function link(scope){

            scope.$on('PERMISSIONS_EDIT_ACTIVE', function(Permissions_Edit_Active, index){
                scope.is_active = scope.exhibitorUserIndex == index && !scope.is_active;
            });

            scope.userPermissions = [
                ['Edit Company Profile', 'edit_company_profile'],
                ['Scan Leads', 'scan_leads'],
                ['Manage Users', 'manage_users'],
                ['Download Leads', 'download_leads']
            ];

            scope.editExhibitorUserPermissions = function(){
                $rootScope.$broadcast("PERMISSIONS_EDIT_ACTIVE", scope.exhibitorUserIndex);
            };

            scope.changeExhibitorUserPermission = function(permission){
                scope.exhibitorUserData[permission] = !scope.exhibitorUserData[permission];
                ConferenceExhibitorsService
                    .updateExhibitorUsers(scope.exhibitorUserData)
                    .then(function(response){
                    }, function(error){
                        console.log(error);
                    });
            };

            scope.deleteExhibitorUser = function(exhibitorUser){
                ConferenceExhibitorsService
                .deleteExhibitorUsers(exhibitorUser.id)
                .then(function(response){
                    scope.$emit("EXHIBITOR_USER_DELETED", exhibitorUser.id);
                }, function(error){
                    console.log(error);
                });
            };
        }
    }
})();