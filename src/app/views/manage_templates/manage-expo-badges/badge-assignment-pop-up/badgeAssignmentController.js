(function(){
  'use strict';
  var module = angular.module('app');
  var controllerId = 'badgeAssignmentController';

  module.controller(controllerId, ['$rootScope', '$scope', '$state', '$timeout', 'toastr', 'ngDialog', 'expoBadgesService', badgeAssignmentController]);

  function badgeAssignmentController($rootScope, $scope, $state, $timeout, toastr, ngDialog, expoBadgesService){

    var conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];
    $scope.badge = $scope.ngDialogData.badgeToAssign;
    $scope.defaultBadge = $scope.ngDialogData.alreadySettedUpBadgeAssignmentRules.default || false;
    $scope.badgeAssignmentRules = [];
    $scope.alreadySettedUpBadgeAssignmentRules = $scope.ngDialogData.alreadySettedUpBadgeAssignmentRules;
    $scope.currentAttendeeSessionsRules = [];
    $scope.numberOfAttendeesAssigned = $scope.alreadySettedUpBadgeAssignmentRules.attendee_nr || 0;
    $scope.currentAttendeeGuestsRules = [

        {
         'optionName': 'Yes',
         'optionValue': true,
         'checked': false
        },

        {
         'optionName': 'No',
         'optionValue': false,
         'checked': false
        }
     ];
    $scope.registrationLevels = [];
    $scope.attendeeCustomFields = [];

    $scope.selectedRules = [];

    mapRegistrationLevels();
    mapAttendeeCustomFields();

    setRegistrationLevels();
    setAttendeeCustomFields();
    setAddOnSessions();
    setGuestAssignmentRule();

    initRules();

    $scope.toggleDefaultBadge = function(){
        $scope.defaultBadge = !$scope.defaultBadge;
    };

    $scope.toggleCheckForThisLevel = function(level) {
      level.checked = !level.checked;

      //reset rules for add on session
      $scope.badgeAssignmentRules = _.reject($scope.badgeAssignmentRules, function(savedRule){
        return savedRule.categoryName === 'Add-on Sessions' && !_.isEmpty(savedRule.optionChecked);
      });

      //in case the badge has no more rules after reset
      initRules();

      _.each(level.levelAddOnSessions, function(addOnSession) {
        if(level.checked) {
          $scope.currentAttendeeSessionsRules.push(addOnSession);
          $scope.currentAttendeeSessionsRules = _.uniqBy($scope.currentAttendeeSessionsRules, 'optionValue');
        } else {
          $scope.currentAttendeeSessionsRules = _.reject($scope.currentAttendeeSessionsRules, ['optionValue', addOnSession.optionValue]);
        }
      });

      // calculate on the fly the number of attendees assigned
      calculateNumberOfAttendeesAssigned();
    };

    $scope.toggleCategorySelection = function(ruleIndex) {
      $scope.categorySelected = $scope.categorySelected !== ruleIndex ? ruleIndex : -1;
      $scope.categoryOptionSelected = -1;
    };

    $scope.toggleCategoryOptionSelection = function(ruleIndex) {
      $scope.categoryOptionSelected = $scope.categoryOptionSelected !== ruleIndex ? ruleIndex : -1;
      $scope.categorySelected = -1;
    };

    $scope.selectThisCategoryOption = function($event, optionIndex, rule) {
      //exclude already selected options
      var pos = rule.categoryOptions.map(function(e) { return e.checked; }).indexOf(true);//rule.categoryOptions.findIndex(item => item.checked === true);
      if(pos >= 0){
        pos = rule.categoryOptions[pos].optionValue;
        $scope.selectedRules.splice($scope.selectedRules.indexOf(pos),1);
      }

      $scope.selectedRules.push(rule.categoryOptions[optionIndex].optionValue);

      $event.stopPropagation();
      $scope.categoryOptionSelected = -1;

      //reset all options that were checked for this rule
      _.each(rule.categoryOptions, function(option){
        option.checked = false;
      });

      rule.categoryOptions[optionIndex].checked = true;
      rule.optionChecked = rule.categoryOptions[optionIndex].optionName;

      // calculate on the fly the number of attendees assigned
      calculateNumberOfAttendeesAssigned();
    }

    $scope.selectThisCategory = function($event, categoryName, categoryOptions, rule) {
      $event.stopPropagation();
      $scope.categorySelected = -1;

      //reset options that were checked in preview
      _.each(rule.categoryOptions, function(option) {
        option.checked = false;
      });

      rule.categoryName = categoryName;
      rule.categoryOptions = _.cloneDeep(categoryOptions);
      rule.optionChecked = '';
    };

    $scope.addNewBadgeAssignmentRule = function() {
      //close opened menus
      $scope.categoryOptionSelected = -1;
      $scope.categorySelected = -1;

      $scope.currentBadgeAssignmentRuleModel = {
          categoryName: 'Add-on Sessions',
          categoryOptions: _.cloneDeep($scope.currentAttendeeSessionsRules),
          optionChecked: ''
      };

      $scope.badgeAssignmentRules.push($scope.currentBadgeAssignmentRuleModel);
    };

    $scope.removeBadgeAssignmentRule = function($event,index) {
      $event.stopPropagation();

      //close opened menus
      $scope.categoryOptionSelected = -1;
      $scope.categorySelected = -1;
      //exclude already selected options
      if($scope.badgeAssignmentRules[index].optionChecked){
        var pos = $scope.badgeAssignmentRules[index].categoryOptions.map(function(e) { return e.optionName; }).indexOf($scope.badgeAssignmentRules[index].optionChecked);
        pos = $scope.badgeAssignmentRules[index].categoryOptions[pos].optionValue;
        $scope.selectedRules.splice($scope.selectedRules.indexOf(pos),1);
      }
      $scope.badgeAssignmentRules.splice(index, 1);

      // calculate on the fly the number of attendees assigned
      calculateNumberOfAttendeesAssigned();
    };

    $scope.saveBadgeAssignment = function() {
      var objectToSend = prepareDataOfAssigmentRules();
      var requestType = $scope.alreadySettedUpBadgeAssignmentRules.hasOwnProperty('rule_created');

      if(!requestType) {
        expoBadgesService.createBadgeAssignment(conferenceId, $scope.badge.id, {'rules': objectToSend, 'default': $scope.defaultBadge})
        .then(
            function(response) {
              $scope.numberOfAttendeesAssigned = response.data.assignment_count;
              toastr.success('Badge Assignment Rules are Successfully Saved');
          }, function(error) {
              toastr.error('Faild to save Badge Assignment Rules');
          }
        );
      } else {
        expoBadgesService.updateBadgeAssignment(conferenceId, $scope.badge.id, {'rules': objectToSend, 'default': $scope.defaultBadge})
        .then(
            function(response) {
              $scope.numberOfAttendeesAssigned = response.data.assignment_count;
              toastr.success('Badge Assignment Rules are Successfully Updated');
          }, function(error) {
              toastr.error('Faild to update Badge Assignment Rules');
          }
        );
      }
    };

    function prepareDataOfAssigmentRules() {
      var objectToSend = {
        "registrationLevelIds": [],
        "customAttendeeFieldOptionIds": [],
        "sessionIds": [],
      };

      getCheckedLevels(objectToSend);
      getCheckedAttendeeCustomFields(objectToSend);
      getCheckedAddOnSessions(objectToSend);
      getCheckedGuestRule(objectToSend);

      return objectToSend;
    };

    function calculateNumberOfAttendeesAssigned() {
      var objectToSend = prepareDataOfAssigmentRules();

      expoBadgesService.calculateNumberOfAttendeesAssigned(conferenceId, $scope.badge.id, {'rules': objectToSend, 'default': $scope.defaultBadge})
      .then(
          function(response) {
            $scope.numberOfAttendeesAssigned = response.data.assignment_count;
        }, function(error) {
        }
      );
    };

    function initRules() {
      if($scope.badgeAssignmentRules.length == 0) {
          $scope.currentBadgeAssignmentRuleModel = {
              categoryName: 'Add-on Sessions',
              categoryOptions: _.cloneDeep($scope.currentAttendeeSessionsRules),
              optionChecked: ''
          };

          $scope.badgeAssignmentRules = [$scope.currentBadgeAssignmentRuleModel];
      }
    };

    function mapRegistrationLevels() {
      _.each($scope.ngDialogData.registrationLevels, function(level) {
          var objectToPush = {'levelName': level.name, 'levelId': level.id, 'checked': false};
          objectToPush.levelAddOnSessions = [];

          _.each(level.registrationleveladdonsession_set, function(addOnSession) {
            objectToPush.levelAddOnSessions.push({'optionName': addOnSession.session.title, 'optionValue': addOnSession.session.id, 'checked': false});
          });

          $scope.registrationLevels.push(objectToPush);
      });
    };

    function mapAttendeeCustomFields() {
      _.each($scope.ngDialogData.attendeeCustomFields, function(customField) {

        if(customField.type != 1) {
            var objectToPush = {};
            objectToPush.customFieldName = customField.name;
            objectToPush.customFieldOptions = [];

            _.each(customField.options, function(option) {
              objectToPush.customFieldOptions.push({'optionName': option.value, 'optionValue': option.id, 'checked': false});
            });
            $scope.attendeeCustomFields.push(objectToPush);
        }

      });
    };

    function getCheckedLevels(objectToSend) {
      _.each($scope.registrationLevels, function(level){
          if(level.checked) {
              objectToSend.registrationLevelIds.push(level.levelId);
          }
      });
    };

    function getCheckedAttendeeCustomFields(objectToSend) {
      _.each($scope.badgeAssignmentRules, function(savedRule) {
          if(savedRule.categoryName !== 'Add-on Sessions' && savedRule.categoryName !== 'Guest') {
            _.each(savedRule.categoryOptions, function(option) {
              if(option.checked) {
                  objectToSend.customAttendeeFieldOptionIds.push(option.optionValue);
              }
            });
          }
      });
    };

    function getCheckedAddOnSessions(objectToSend) {
      _.each($scope.badgeAssignmentRules, function(savedRule) {
          if(savedRule.categoryName === 'Add-on Sessions') {
            _.each(savedRule.categoryOptions, function(option) {
              if(option.checked) {
                  objectToSend.sessionIds.push(option.optionValue);
                  objectToSend.sessionIds = _.uniq(objectToSend.sessionIds);
              }
            });
          }
      });
    };

    function getCheckedGuestRule(objectToSend) {
      _.each($scope.badgeAssignmentRules, function(savedRule) {
          if(savedRule.categoryName === 'Guest' && !_.isEmpty(savedRule.optionChecked)) {
            _.each(savedRule.categoryOptions, function(option) {

              if(option.optionName === 'Yes' && option.checked) {
                objectToSend.guest = true;
              } else if(option.optionName === 'No' && option.checked) {
                objectToSend.guest = false;
              }

            });
          }
      });
    }

    function setRegistrationLevels() {
      _.each($scope.registrationLevels, function(level){
          var checkedLevel = _.find($scope.alreadySettedUpBadgeAssignmentRules.registrationLevelIds, function(levelId) {
            return level.levelId === levelId;
          });

          if(checkedLevel !== undefined) {
            level.checked = true;

            _.each(level.levelAddOnSessions, function(addOnSession) {
                $scope.currentAttendeeSessionsRules.push(addOnSession);
                $scope.currentAttendeeSessionsRules = _.uniqBy($scope.currentAttendeeSessionsRules, 'optionValue');
            });
          }
      });
    };

    function setAttendeeCustomFields() {
      _.each($scope.attendeeCustomFields, function(customField){
        _.each(customField.customFieldOptions, function(customFieldOption){
          var checkedOption = _.find($scope.alreadySettedUpBadgeAssignmentRules.customAttendeeFieldOptionIds, function(customFieldOptionId) {
            return customFieldOption.optionValue === customFieldOptionId;
          });

          if(checkedOption !== undefined) {

            var objectToPush = {};
            objectToPush.categoryName = customField.customFieldName;
            objectToPush.categoryOptions = _.cloneDeep(customField.customFieldOptions);
            _.each(objectToPush.categoryOptions, function(option){
              if(option.optionValue == checkedOption) {
                  option.checked = true;
              }
            });

            objectToPush.optionChecked = customFieldOption.optionName;

            $scope.badgeAssignmentRules.push(objectToPush);

          }
        });
      });
    };

    function setAddOnSessions() {
      _.each($scope.currentAttendeeSessionsRules, function(addOnSession) {
        var checkedAddOnSession = _.find($scope.alreadySettedUpBadgeAssignmentRules.sessionIds, function(sessionId) {
          return addOnSession.optionValue === sessionId;
        });

        if(checkedAddOnSession !== undefined) {

          var objectToPush = {};
          objectToPush.categoryName = 'Add-on Sessions';
          objectToPush.categoryOptions = _.cloneDeep($scope.currentAttendeeSessionsRules);
          _.each(objectToPush.categoryOptions, function(option){
            if(option.optionValue == checkedAddOnSession) {
              option.checked = true;
            }
          });

          objectToPush.optionChecked = addOnSession.optionName;

          $scope.badgeAssignmentRules.push(objectToPush);
        }
      });
    };

    function setGuestAssignmentRule() {
      var hasGuestRule = $scope.alreadySettedUpBadgeAssignmentRules.hasOwnProperty('guest');

      if (hasGuestRule && $scope.alreadySettedUpBadgeAssignmentRules.guest === true) {

        var objectToPush = {};
        objectToPush.categoryName = 'Guest';
        $scope.currentAttendeeGuestsRules[0].checked = true;
        objectToPush.categoryOptions = $scope.currentAttendeeGuestsRules;
        objectToPush.optionChecked = 'Yes';

        $scope.badgeAssignmentRules.push(objectToPush);

      } else if (hasGuestRule && $scope.alreadySettedUpBadgeAssignmentRules.guest === false) {

        var objectToPush = {};
        objectToPush.categoryName = 'Guest';
        $scope.currentAttendeeGuestsRules[1].checked = true;
        objectToPush.categoryOptions = $scope.currentAttendeeGuestsRules;
        objectToPush.optionChecked = 'No';

        $scope.badgeAssignmentRules.push(objectToPush);
      }
    };

  };

})();
