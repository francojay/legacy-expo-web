(function() {
    'use strict';

    var controllerId = 'badgesRentPrinterController';
    angular
        .module('app')
        .controller(controllerId, ['$scope', '$state', 'toastr', 'ngDialog', 'SharedProperties',
            badgesRentPrinterController
        ]);

    function badgesRentPrinterController($scope, $state, toastr, ngDialog, SharedProperties) {
      $scope.currentStep = 1;
      $scope.rentalAgreement = false;

      $scope.openRentForm = function(){
        ngDialog.open({
            template: 'app/views/manage_templates/badge-rent-printer/badge-rent-printer-form/badgeRentPrinterFormView.html',
            className: 'src/app/views/manage_templates/badge-rent-printer/badge-rent-printer-form/badgeRentPrinterFormStyle.scss',
            controller: 'badgesRentPrinterController'
        });
      };

      $scope.nextPage = function(flow){
        if($scope.currentStep < 4 && flow == 'forward'){
          if($scope.currentStep == 1 && $scope.rentalAgreement === true) {
            $scope.currentStep++;
          } else if($scope.currentStep != 1) {
            $scope.currentStep++;
          } else{
            toastr.warning('You have to agree with our terms and conditions!');
            return;
          }

        } else if($scope.currentStep > 1 && flow == 'backward'){
          $scope.currentStep--;
        }
      };

      $scope.toggleRentalAgreement = function(){
        $scope.rentalAgreement = !$scope.rentalAgreement;
      }

      $scope.checkTypeOfCard = function(){
          var number = document.getElementById('rent_printer_payment_CCN').value;
          number = number.toString();
          if(_.isEmpty(number) === false){
              if((number.indexOf(4) === 0) && (number.length === 13 || number.length === 16)){
                  $scope.CCNType = 'visa';
              } else if((number.indexOf(5) === 0) && (number.charAt(1) >= 1 || number.charAt(1) <= 5) && (number.length === 16)){
                  $scope.CCNType = 'master';
              } else if((number.length === 16) && ((_.inRange(number.substring(0, 8), 60110000,  60119999)) ||
                  (_.inRange(number.substring(0, 8), 65000000,  65999999)) ||
                  (_.inRange(number.substring(0, 8), 62212600,  62292599)) ) ){
                  $scope.CCNType = 'discover';
              } else if((number.indexOf(3) === 0) && ((number.indexOf(4) === 1 || number.indexOf(7) === 1)) && (number.length === 15)){
                  $scope.CCNType = 'american_exp'
              } else if(((number.substring(0,4) === 3088) || (number.substring(0,4) === 3096) || (number.substring(0,4) === 3112) ||
                  (number.substring(0,4) === 3158) || (number.substring(0,4) === 3337) || (_.inRange(number.substring(0,8),35280000, 35899999))) &&
                  (number.length === 16)){
                  $scope.CCNType = 'jcb';
              } else if((number.indexOf(3) === 0) && (number.indexOf(0) === 1 || number.indexOf(6) === 1 || number.indexOf(8) === 1) && (number.length === 14)){
                  $scope.CCNType = 'diners';
              } else {
                  $scope.CCNType = '';
              }
          } else{
              $scope.CCNType = '';
          }
      };

      $scope.shippingFormFields = [
        {
            field_name: 'Ship to:',
            field_code: 'ship_to',
            field_width: '100%'
        },
        {
            field_name: 'Address',
            field_code: 'adress_1',
            field_width: '100%'
        },
        {
            field_name: 'Address 2',
            field_code: 'address_2',
            field_width: '100%'
        },
        {
            field_name: 'Address 3',
            field_code: 'address_3',
            field_width: '100%'
        },
        {
            field_name: 'City',
            field_code: 'city',
            field_width: '100%'
        },
        {
            field_name: 'State',
            field_code: 'state',
            field_width: '21%'
        },
        {
            field_name: 'Zip Code',
            field_code: 'zip_code',
            field_width: '48%'
        },
        {
            field_name: 'Phone Number',
            field_code: 'phone_number',
            field_width: '48%'
        },
        {
            field_name: 'Mobile Number',
            field_code: 'mobile_number',
            field_width: '48%'
        },
        {
            field_name: 'Email',
            field_code: 'email_address',
            field_width: '100%'
        }
      ];

    }
})();
