(function() {
    'use strict';

    var serviceId = 'constructBaseUrlService';
    var module = angular.module('app');

    module.service(serviceId, constructBaseUrlService);

    function constructBaseUrlService() {
        var service = {};

        service._getEnvironment = function() {
            var location = window.location.href.split('#')[0];
            if (['http://localhost:3000/', 'http://expo-frontend.s3-website-us-west-2.amazonaws.com/',
                    'http://expo-frontend-prod.s3-website-us-west-2.amazonaws.com/',
                    'http://expo-registration.s3-website-us-west-2.amazonaws.com/', 'http://expo-editor.s3-website-us-west-2.amazonaws.com/'
                ].indexOf(location) > -1) {
                return 'DEV';
            } else {
                return 'PROD';
            }
        };

        service.getBaseUrl = function() {
            var env = service._getEnvironment();
            if (env == 'DEV') return 'https://api-dev.expopass.com';
            else if (env == 'PROD') return 'https://api.expopass.com';
            else if (env == 'SANDBOX') return 'https://fppfvciyi1.execute-api.us-west-2.amazonaws.com/sandbox/api/register/';
        };

        var exposedObject = {
            getBaseUrl: service.getBaseUrl
        }

        return exposedObject;
    }
})();
