(function(){
	'use strict';

    var directiveId = 'printerRentalStatus';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', printerRentalStatusDirective]);

	function printerRentalStatusDirective($rootScope) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/manage_templates/_directives/rentalStatusForPrinter/rentalStatusForPrinterView.html',
			replace: true,
			link: link,
			scope: {
				data: "=data"
			}
		};

    function link(scope){
      scope.order_placed_date =  new Date();
    }

  }
})();
