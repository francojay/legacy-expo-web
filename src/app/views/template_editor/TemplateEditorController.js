(function () {
    'use strict'
    var controllerId = 'templateEditorController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$state', '$timeout', 'templateEventsDispatcher', '$stateParams', templateEditorController]);
    function templateEditorController($rootScope, $scope, $state, $timeout, templateEventsDispatcher, $stateParams) {
      $rootScope.componentsList = {
          "shell": [],
          "imprint": []
      };
      $rootScope.showBadge = true;
      $scope.conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];

      var leftKeyHeld = false;
      var rightKeyHeld = false;
      var upKeyHeld = false;
      var downKeyHeld = false;

      var availableEvents = templateEventsDispatcher.availableEvents;
      var badgeId = $state.params.badge_id;

      $timeout(function() {
        $scope.badgeEditorStep = $state.current.name;
      }, 0);

      $scope.$watch(function() {
        return $state.current.name;
      }, function() {
        $scope.badgeEditorStep = $state.current.name;
      });

      $rootScope.$on(availableEvents.GO_FULL_SCREEN, function(event, data) {
        var element = document.body;

        var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

        if (requestMethod) { // Native full screen.
           requestMethod.call(element);
        } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
           var wscript = new ActiveXObject("WScript.Shell");
           if (wscript !== null) {
               wscript.SendKeys("{F11}");
           }
        }
      });

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options) {
        if(!_.isEmpty(toParams.badge_id)) {
          badgeId = toParams.badge_id
        }
        if (fromState.name === 'template_editor.customize') {
          interact('.drag-drop').unset();
        }
      });

      $scope.hideAllOpenMenus = function($event) {
        if (event.which === 1) {
          if ($state.current.name === 'template_editor.customize') {
            templateEventsDispatcher.dispatchEvent(availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS);
          }
        }
    	}

      $scope.backToPreviousStep = function() {
        var encodedConferenceId = new Hashids('Expo', 7).encode($scope.conferenceId)

        if ($scope.badgeEditorStep === 'template_editor.customize') {
          templateEventsDispatcher.dispatchEvent(availableEvents.SHOW_BADGE_FRONT);

          $scope.badgeEditorStep = 'template_editor.define_badge';
          templateEventsDispatcher.dispatchEvent(availableEvents.SAVE_TEMPLATE, {toState: $scope.badgeEditorStep});

          $state.go($scope.badgeEditorStep, {'conference_id': encodedConferenceId, 'badge_id': badgeId});

        } else if ($scope.badgeEditorStep === 'template_editor.review') {
          templateEventsDispatcher.dispatchEvent(availableEvents.SHOW_BADGE_FRONT);
          $scope.badgeEditorStep = 'template_editor.customize';
          $state.go($scope.badgeEditorStep, {'conference_id': encodedConferenceId, 'badge_id': badgeId});
        }
      }

      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (fromState.name === 'template_editor.customize' && $rootScope.autosaveTimerId) {
          clearInterval($rootScope.autosaveTimerId);
          $rootScope.autosaveTimerId = null;
        }
      });

      // move component by 1px from keyboard (left, right, up, down arrows)
      $(window).on('keydown', function(e) {
        var keyCode = e.which;

        if(keyCode == 37 ) {
          if (!leftKeyHeld) {
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_LEFT, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
          }

          var timer = setTimeout(function() {
            leftKeyHeld = true;
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_LEFT, {saveEvent: false, componentId: $rootScope.currentActiveComponentIdentifier});
            window.onkeyup = function() {leftKeyHeld = false;};
          }, 200);

          window.onkeyup = function(e) {
            var keyCode = e.which;
            if (keyCode == 37) {
              clearTimeout(timer);
              templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_LEFT, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
              leftKeyHeld = false;
            }
          }
        } else if (keyCode == 38) {
          if (!upKeyHeld) {
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_UP, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
          }

          var timer = setTimeout(function() {
            upKeyHeld = true;
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_UP, {saveEvent: false, componentId: $rootScope.currentActiveComponentIdentifier});
            window.onkeyup = function() {upKeyHeld = false;};
          }, 200);

          window.onkeyup = function(e) {
            var keyCode = e.which;
            if (keyCode == 38) {
              clearTimeout(timer);
              templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_UP, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
              upKeyHeld = false;
            }
          }
        } else if(keyCode == 39) {
          if (!rightKeyHeld) {
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_RIGHT, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
          }

          var timer = setTimeout(function() {
            rightKeyHeld = true;
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_RIGHT, {saveEvent: false, componentId: $rootScope.currentActiveComponentIdentifier});
            window.onkeyup = function() {rightKeyHeld = false;};
          }, 200);

          window.onkeyup = function(e) {
            var keyCode = e.which;
            if (keyCode == 39) {
              clearTimeout(timer);
              templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_RIGHT, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
              rightKeyHeld = false;
            }
          }
        } else if(keyCode == 40) {
          if (!downKeyHeld) {
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_DOWN, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
          }

          var timer = setTimeout(function() {
            downKeyHeld = true;
            templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_DOWN, {saveEvent: false, componentId: $rootScope.currentActiveComponentIdentifier});
            window.onkeyup = function() {downKeyHeld = false;};
          }, 200);

          window.onkeyup = function(e) {
            var keyCode = e.which;
            if (keyCode == 40) {
              clearTimeout(timer);
              templateEventsDispatcher.dispatchEvent(availableEvents.MOVE_DOWN, {saveEvent: true, componentId: $rootScope.currentActiveComponentIdentifier});
              downKeyHeld = false;
            }
          }
        }

      });
    }
})();
