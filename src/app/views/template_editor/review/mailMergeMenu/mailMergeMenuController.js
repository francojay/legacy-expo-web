(function () {
    'use strict'
    var controllerId = 'mailMergeMenuController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$state', '$timeout', 'templateEventsDispatcher', 'templateEditorApiService', mailMergeMenuController]);
    function mailMergeMenuController($rootScope, $scope, $state, $timeout, templateEventsDispatcher, templateEditorApiService) {
      var conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];

      $scope.mailMergeData = '';
      $scope.mailMergeListOfAttendees = [];
      $scope.toggleAttendeesList = false;
      $scope.next_page = 1;

      $timeout(function() {
        $timeout(function() {
          $scope.showComponentsMenu = true;
        }, 200);
      }, 0);

      $scope.setAttendeeDataForBadge = function(mailMergeAttendee) {
        angular.forEach($rootScope.componentsList.imprint, function(component) {
          $timeout(function() {
            if (component.props.hasOwnProperty('mailMergeField')) {
              if (typeof component.props.mailMergeField === 'number') {
                var mergeFieldList = [];
                mergeFieldList.push(component.props.mailMergeField);
              } else {
                var mergeFieldList = component.props.mailMergeField.split(',');
              }
              var innerText = mergeFieldList
              .reduce(function(string, mergeField) {
                if (typeof mergeField === 'number') {
                  if (mailMergeAttendee.custom_fields[mergeField]) {
                    return (typeof mailMergeAttendee.custom_fields[mergeField].value == "string")?mailMergeAttendee.custom_fields[mergeField].value:mailMergeAttendee.custom_fields[mergeField].value.toString();
                  } else {
                    return string;
                  }
                } else {
                  if (mailMergeAttendee[mergeField]) {
                    return string + mailMergeAttendee[mergeField] + " ";
                  } else {
                    return string;
                  }
                }
              }, "")
              .trim();

              $('#template-safe-zone').find('div[data-id=' + component.props.id + ']').children('#text-' + component.props.id).fadeOut(200, function() {
                  $(this).html(innerText).fadeIn(200);
              });
            }
          }, 0);
        });
      }

      function scrollEvent(event) {
        var mailMergeList = event.target;
          if (mailMergeList.scrollTop + mailMergeList.offsetHeight > (mailMergeList.scrollHeight)) {
            $scope.getModelDataForBadgeReview(true);
          }
      }

      $scope.getModelDataForBadgeReview = function(loadMore) {
        if ($scope.mailMergeData != '') {

          templateEditorApiService.getModelDataForBadge(conferenceId, $scope.mailMergeData, $scope.next_page)
          .then(function(res) {
            $scope.loadingMoreMailMergeData = false;

            if (loadMore) {
              _.each(res.data.attendees, function(attendee){
                $scope.mailMergeListOfAttendees.push(attendee);
              });
            } else {
              $scope.mailMergeListOfAttendees = res.data.attendees;
            }

            $scope.total_attendees = res.data.total_count;

            if ($scope.total_attendees > $scope.mailMergeListOfAttendees.length) {
              var mailMergeList = document.getElementsByClassName('mail-merge-list-of-attendees')[0];

              mailMergeList.addEventListener('scroll', scrollEvent);

              $scope.loadingMoreMailMergeData = true;
              $scope.next_page++;

            } else {
              var mailMergeList = document.getElementsByClassName('mail-merge-list-of-attendees')[0];
              mailMergeList.removeEventListener('scroll', scrollEvent);
            }
            $scope.toggleAttendeesList = true;
          }, function(err) {
            $scope.loadingMoreMailMergeData = false;
            $scope.mailMergeListOfAttendees = [];
            $scope.toggleAttendeesList = false;
          });
        } else {
          $scope.next_page = 1;
          $scope.mailMergeListOfAttendees = [];
          $scope.toggleAttendeesList = false;
        }
      }
    }
})();
