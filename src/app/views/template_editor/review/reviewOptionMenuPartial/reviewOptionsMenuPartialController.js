(function() {
    'use strict'
    var controllerId = 'reviewOptionsMenuPartialController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$timeout', 'templateEventsDispatcher', '$http', '$sce', 'templateEditorApiService', '$state', '$stateParams', 'ConferenceService', 'toastr', reviewOptionsMenuPartialController]);

    function reviewOptionsMenuPartialController($rootScope, $scope, $timeout, templateEventsDispatcher, $http, $sce, templateEditorApiService, $state, $stateParams, ConferenceService, toastr) {
      var conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];
      var badgeId      = $stateParams.badge_id;

      $scope.showBadgeSidesSwitch  = $rootScope.badgeNumberSides && $rootScope.badgeNumberSides === 2 ? true : false;

      $scope.TEST_PRINT = templateEventsDispatcher.availableEvents.TEST_PRINT;

      $scope.saveBadgeRenderedTemplates = function() {
        $('.badge-print-hole').remove();
        var badgeHtmlRenderedTemplates = {
          front       : '',
          shell       : '',
          imprint     : '',
          back        : ''
        };

        var fontsArray      = getAllFonts();
        var head            = createHeadElementWithFontsLinks(fontsArray);

        var badgeHtml       = document.createElement('html');
        var badgeBody       = document.createElement('body');

        var badges          = document.getElementsByClassName('badge-container');
        resetMailMergeFieldsInitialText(badges[0]);
        var frontBadge      = badges[0];
        var backBadge       = badges[1];
        var frontSafeZone   = document.getElementById("template-safe-zone");

        frontBadge.style.transform = 'scale(1)';
        if (backBadge) {
          backBadge.style.transform  = 'scale(1)';
        }

        var $head           = $(head);
        var $badgeHtml      = $(badgeHtml);
        var $badgeBody      = $(badgeBody);

        var $backBadgeHead  = angular.copy($(head));
        var $backBadgeHtml  = angular.copy($(badgeHtml));
        var $backBadgeBody  = angular.copy($(badgeBody));

        var $frontBadge     = angular.copy($(frontBadge));
        var $backBadge      = angular.copy($(backBadge));
        var $frontSafeZone  = angular.copy($(frontSafeZone));

        $badgeBody.css('width',  '100%');
        $badgeBody.css('height', '100%');
        $badgeBody.css('margin', '0px');
        $badgeBody.css('padding', '0px');

        $badgeHtml.css('margin', '0px');
        $badgeHtml.css('padding', '0px');

        $badgeHtml.css('width',  badges[0].style.width);
        $badgeHtml.css('height', badges[0].style.height);

        $backBadgeHtml.css('width',  badges[0].style.width);
        $backBadgeHtml.css('height', badges[0].style.height);

        $badgeBody.append($frontBadge);
        $badgeHtml.append($head);
        $badgeHtml.append($badgeBody);

        $backBadgeBody.append($backBadge);
        $backBadgeHtml.append($backBadgeHead);
        $backBadgeHtml.append($backBadgeBody);
        $badgeHtml.children('body').children('.badge-container').children('#template-safe-zone').css("border", "none");

        angular.forEach($badgeHtml.children('body').children('.badge-container').children('#template-safe-zone').children(), function(element) {
          element.attributes.style.value = element.attributes.style.value.replace('transform', '-webkit-transform');
          angular.element(element).children('.ui-draggable').remove();
          angular.element(element).children('.anchor-points-wrapper').remove()
        });

        var $mobileTemplateHtml = angular.copy($badgeHtml);

        angular.forEach($mobileTemplateHtml.children('body').children('.badge-container').children('#template-safe-zone').children(), function(element) {
          if (element.attributes.class.value.indexOf('imprint') >= 0) {
            if (element.attributes.class.value.indexOf('text-component') >= 0) {
              var mergeFieldsOnElement = [];
              var regexInnerText       = "";
              if (element.children[0].dataset.mergeFields) {
                mergeFieldsOnElement = element.children[0].dataset.mergeFields.split(',');
              }
              if (mergeFieldsOnElement.length > 0) {
                angular.forEach(mergeFieldsOnElement, function(mergeField) {
                  regexInnerText = regexInnerText + "__" + mergeField + "__" + " ";
                });
                regexInnerText = regexInnerText.trim();
                element.children[0].innerText = regexInnerText;
              }
            } else if (element.attributes.class.value.indexOf('code-component') >= 0) {
              element.children[0].src = "__QR_CODE__";
            }
          }
        });

        badgeHtmlRenderedTemplates.templateFrontKey = $.trim($mobileTemplateHtml[0].outerHTML); // shell + imprint pentru mobile

        badgeHtmlRenderedTemplates.front   = $.trim($badgeHtml[0].outerHTML); //shell + imprint

        var $frontHtmlCopy                 = angular.copy($badgeHtml);
        $frontHtmlCopy.children('body').children('.badge-container').children('#template-safe-zone').children('.imprint').remove();

        badgeHtmlRenderedTemplates.shell   = $.trim($frontHtmlCopy[0].outerHTML); // doar shell

        $badgeHtml.children('body').children('.badge-container').children('#template-safe-zone').children('.shell').remove();

        $mobileTemplateHtml.children('body').children('.badge-container').children('#template-safe-zone').children('.shell').remove();
        var $imprintHtmlCopy = angular.copy($mobileTemplateHtml);

        $imprintHtmlCopy[0].style['backgroundColor']                         = "white";
        $imprintHtmlCopy[0].children[1].style['backgroundColor']             = "white";
        $imprintHtmlCopy[0].children[1].children[0].style['backgroundColor'] = "white";

        badgeHtmlRenderedTemplates.imprint = $.trim($imprintHtmlCopy[0].outerHTML); // doar imprint
        badgeHtmlRenderedTemplates.back    = $.trim($backBadgeHtml[0].outerHTML); // doar spate

        templateEditorApiService.saveTemplates(conferenceId, badgeId, badgeHtmlRenderedTemplates).then(function(response) {
          $scope.goToManageTemplates();
          toastr.success("Template reviewed successfully!", "Redirecting to templates manager... ")
        }, function(error) {
          toastr.error("An error has occured!", "Please try again.");
        });
      }

      $scope.goToManageTemplates = function() {
        var encodedConferenceId = new Hashids("Expo", 7).encode(conferenceId);
        $state.go('conferences.dashboard.attendees.expo_badges', {'conference_id': encodedConferenceId});
      }

      $timeout(function() {
          $timeout(function() {
              $scope.showComponentsMenu = true;
          }, 200);

          if (!$rootScope.badgeName) {
            var encodedConferenceId = new Hashids("Expo", 7).encode(conferenceId);
            $state.go('template_editor.customize', {'conference_id': encodedConferenceId, 'badge_id': badgeId});
          }

      }, 0);

      function getAllFonts() {
        var arr             = [];
        var textComponents  = $('.text-component');
        angular.forEach(textComponents, function(component) {
          var fontFamily = $($(component).children()[1]).css('fontFamily');
          arr.push(fontFamily);
        });

        return _.uniq(arr);
      }

      function createHeadElementWithFontsLinks(arr) {
        var linksArray  = [];
        var head        = document.createElement('head');

        angular.forEach(arr, function(arr) {
          var link      = document.createElement('link');
          var protocol  = window.location.protocol;
          link.rel      = 'stylesheet';
          link.type     = 'text/css';
          link.href     = protocol + '//fonts.googleapis.com/css?family='+arr;
          link.media    = 'all';

          link.href.replace(/['"]+/g, '');
          head.appendChild(link);
        });

        return head;
      }

      function resetMailMergeFieldsInitialText(template) {
        var componentsWithMergeFields = $rootScope.componentsList.imprint;
        componentsWithMergeFields.filter(function(component) {
          return component.props.mailMergeField && !_.isEmpty(component.props.mailMergeField)
        });
        _.each(componentsWithMergeFields, function(component) {
          $(template)
          .children('#template-safe-zone').find('div[data-id=' + component.props.id + ']')
          .children('#text-' + component.props.id)
          .html(component.props.innerText);
        });
      }
    }
})();
