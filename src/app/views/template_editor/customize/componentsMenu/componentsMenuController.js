(function () {
    'use strict'
    var controllerId = 'componentsMenuController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', '$state', 'templateEventsDispatcher', '$timeout', componentsMenuController]);
    function componentsMenuController($rootScope, $scope, $state, templateEventsDispatcher, $timeout) {
    	var availableEvents = templateEventsDispatcher.availableEvents;
    	$timeout(function() {
            $timeout(function() {
                $scope.showComponentsMenu = true;
            }, 200)
        }, 0)

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options) {
          $scope.showComponentsMenu = true;
      })

    }
})();