(function(){
	'use strict';

    var directiveId = 'shapePropertiesContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', shapePropertiesContextualMenuDirective]);

	function shapePropertiesContextualMenuDirective($rootScope, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/contextualMenus/shapePropertiesContextualMenu/shapePropertiesContextualMenuView.html',
			replace: true,
			link: link,
			scope: {

			}
		}

        function link(scope) {
        	var availableEvents = templateEventsDispatcher.availableEvents;
        	scope.currentProperties = {
        		opacity 	: 100,
        		strokeWidth : 0,
        		strokeColor : '#FFFFFF',
        		fillColor 	: 'black',
            width       : 45,
            height      : 45,
            xAxis       : 0,
            yAxis       : 0
        	}

					scope.notifyBadgeToAddShape = function(type) {
						if ($rootScope.badgeRotationDegree === 0) {
							var eventToSend = availableEvents.ADD_SHAPE_COMPONENT;
						} else if ($rootScope.badgeRotationDegree === 180) {
							var eventToSend = availableEvents.ADD_BACK_SHAPE_COMPONENT;
						}

						var shapeObject = {
							shapeType 	: type,
							opacity 		: scope.currentProperties.opacity,
							strokeWidth : scope.currentProperties.strokeWidth,
							strokeColor : scope.currentProperties.strokeColor,
							fillColor 	: scope.currentProperties.fillColor,
		          width       : scope.currentProperties.width,
		          height      : scope.currentProperties.height,
		          xAxis       : scope.currentProperties.xAxis,
		          yAxis       : scope.currentProperties.yAxis
						}

						templateEventsDispatcher.dispatchEvent(eventToSend, shapeObject);
					};

          scope.strokeWidthChanged = function() {
          	if (scope.currentShapeComponentId && scope.currentShapeComponentId === $rootScope.currentActiveComponentIdentifier ) {
          		var objectToSend = {
          			strokeWidth: scope.currentProperties.strokeWidth,
          			componentId: scope.currentShapeComponentId
          		};
          		templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_SHAPE_PROPS__STROKE_WIDTH, objectToSend);
          	}
          };

          scope.opacityChanged = function() {
          	if (scope.currentShapeComponentId && scope.currentShapeComponentId === $rootScope.currentActiveComponentIdentifier ) {
          		var objectToSend = {
          			opacity    	: scope.currentProperties.opacity,
          			componentId : scope.currentShapeComponentId
          		};
          		templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_SHAPE_PROPS__OPACITY, objectToSend);
          	}
          };

          scope.strokeColorChanged = function() {
          	if (scope.currentShapeComponentId && scope.currentShapeComponentId === $rootScope.currentActiveComponentIdentifier ) {
          		var objectToSend = {
          			strokeColor : scope.currentProperties.strokeColor,
          			componentId : scope.currentShapeComponentId
          		};

          		templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_SHAPE_PROPS__STROKE_COLOR, objectToSend);
          	}
          };

          scope.fillColorChanged = function() {
          	if (scope.currentShapeComponentId && scope.currentShapeComponentId === $rootScope.currentActiveComponentIdentifier ) {
          		var objectToSend = {
          			fillColor  	: scope.currentProperties.fillColor,
          			componentId : scope.currentShapeComponentId
          		};

          		templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_SHAPE_PROPS__FILL, objectToSend);
          	}
          };

					$rootScope.$on(availableEvents.UPDATED_SHAPE_PROPS, function(event, data) {
						scope.currentShapeComponentId 			= data.id;
						scope.currentProperties.opacity 		= data.opacity;
						scope.currentProperties.strokeWidth 	= data.strokeWidth;
						scope.currentProperties.strokeColor 	= data.strokeColor;
						scope.currentProperties.fillColor 		= data.fillColor;
		      });
        }
	};

}());
