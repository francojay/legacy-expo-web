(function(){
	'use strict';

	var directiveId = 'textPropertiesContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', '$q', 'templateEventsDispatcher', 'templateEditingService', 'badgeHistoryService', textPropertiesContextualMenuDirective]);

	function textPropertiesContextualMenuDirective($rootScope, $q, templateEventsDispatcher, templateEditingService, badgeHistoryService) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/contextualMenus/textPropertiesContextualMenu/textPropertiesContextualMenuView.html',
			replace: true,
			link: link,
			scope: {
			}
		}

		function link(scope) {
			scope.customFields = processCustomFields($rootScope.conference.custom_fields);
			scope.isBold = false;
			scope.isItalic = false;
			scope.isUnderlined = false;

			var promise = getFonts();
			promise.then(function(response) {
				scope.currentProperties = {
				  "fontSize": 16,
				  "fontFamily": scope.fonts[0].family, //first fontFamily
				  "fontStyle": 'inherit',
				  "textDecoration": 'none',
				  "textAlign": 'left',
				  "opacity": 100,
				  "fontWeight": 400,
				  "color": "#000",
					"mailMergeField": "",
					"xAxis": 0,
					"yAxis": 0,
					"width": "",
					"height": ""
			  };
			}, function(error) {

			})

		  scope.colorPickerOptions = {
			  alphaChannel: true,
			  hsl: true,
				default: "#000"
		  }

			var availableEvents = templateEventsDispatcher.availableEvents;
			scope.attendeeFields              = [
				{
					nameToDisplay : "First Name",
					key           : "first_name"
				},
				{
					nameToDisplay : "Last Name",
					key           : "last_name"
				},
				{
					nameToDisplay : "Company Name",
					key           : "company_name"
				},
				{
					nameToDisplay : "Email Address",
					key           : "email_address"
				},
				{
					nameToDisplay : "Job Title",
					key           : "job_title"
				},
				{
					nameToDisplay : "Phone Number",
					key           : "phone_number"
				},
				{
					nameToDisplay : "Street Address",
					key           : "street_address"
				},
				{
					nameToDisplay : "City",
					key           : "city"
				},
				{
					nameToDisplay : "Zip Code",
					key           : "zip_code"
				},
				{
					nameToDisplay : "State",
					key           : "state"
				},
				{
					nameToDisplay : "Registration Level",
					key           : "attendee_registration_level_name"
				},
				{
					nameToDisplay : "Attendee ID",
					key           : "attendee_id"
				}
			];
			scope.notifyBadgeToAddTextComponent = function($event) {
				var eventToSend = $rootScope.badgeRotationDegree === 0 ? availableEvents.ADD_TEXT_COMPONENT : availableEvents.ADD_BACK_TEXT_COMPONENT;

				$event.preventDefault();
				$event.stopPropagation();

				var head = document.getElementsByTagName('head')[0];
				var link = document.createElement('link');

				link.rel = 'stylesheet';
				link.type = 'text/css';
				link.href = 'https://fonts.googleapis.com/css?family='+scope.currentProperties.fontFamily;
				link.media = 'all';
				head.appendChild(link);
				scope.currentProperties.xAxis  = 0;
				scope.currentProperties.yAxis  = 0;
				scope.currentProperties.width  = "",
				scope.currentProperties.height = "";

				templateEventsDispatcher.dispatchEvent(eventToSend, scope.currentProperties);

			}

			scope.changeFontFamily = function(font) {
				if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
					var head = document.getElementsByTagName('head')[0];
					var link = document.createElement('link');

					link.rel = 'stylesheet';
					link.type = 'text/css';
					link.href = 'https://fonts.googleapis.com/css?family='+scope.currentProperties.fontFamily;
					link.media = 'all';
					head.appendChild(link);

					var objectToSend = {
						componentId: scope.currentProperties.id,
						fontFamily: scope.currentProperties.fontFamily
					}
					templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_FAMILY, objectToSend);
				}
			};

			scope.changeFontStyle = function (isItalic) {
				if (!isItalic) {
					scope.currentProperties.fontStyle = 'italic';
				} else {
					scope.currentProperties.fontStyle = 'inherit';
				}

				scope.isItalic = !isItalic;
				if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
					var objectToSend = {
						componentId: scope.currentProperties.id,
						fontStyle: scope.currentProperties.fontStyle
					};
					templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_STYLE, objectToSend);
				}
			};

			scope.changeTextDecoration = function (isUnderlined) {
				if (!isUnderlined) {
					scope.currentProperties.textDecoration = 'underline';
				} else {
					scope.currentProperties.textDecoration = 'none';
				}

				scope.isUnderlined = !isUnderlined;
				if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
					var objectToSend = {
						componentId: scope.currentProperties.id,
						textDecoration: scope.currentProperties.textDecoration
					};
					templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__TEXT_DECORATION, objectToSend);
				}
			};

			scope.changeTextAlign = function (alignType) {
				if (scope.currentProperties.textAlign === alignType) {
					scope.currentProperties.textAlign = 'left';
				} else {
					scope.currentProperties.textAlign = alignType;
				}
				if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
					var objectToSend = {
						componentId: scope.currentProperties.id,
						textAlign: scope.currentProperties.textAlign
					};
					templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__TEXT_ALIGN, objectToSend);

				}
			};

		  scope.changeFontSize = function(fontSize) {
			  if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier && scope.currentProperties.fontSize) {
				  var objectToSend = {
					  componentId: scope.currentProperties.id,
					  fontSize: scope.currentProperties.fontSize
				  };
				  templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_SIZE, objectToSend);

			  }
		  };

		  scope.changeOpacity = function(opacity, oldOpacity) {
			  if (scope.currentProperties.id && scope.currentProperties.opacity && opacity !== parseInt(oldOpacity) && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
				  var objectToSend = {
					  componentId: scope.currentProperties.id,
					  opacity: scope.currentProperties.opacity / 10
				  };
				  templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__OPACITY, objectToSend);
			  }
		  };

		  scope.changeFontWeight = function(isBold) {
			  if (!isBold) {
				  scope.currentProperties.fontWeight = 700;
			  } else {
				  scope.currentProperties.fontWeight = 400;
			  }

			  scope.isBold = !isBold;
			  if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
				  var objectToSend = {
					  componentId: scope.currentProperties.id,
					  fontWeight: scope.currentProperties.fontWeight
				  };
				  templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_WEIGHT, objectToSend);

			  }
		  };

		  scope.openColorPicker = function($event) {
		  };

		  scope.changeFontColor = function() {
			  if (scope.currentProperties.id && scope.currentProperties.id === $rootScope.currentActiveComponentIdentifier ) {
				  var objectToSend = {
					  componentId: scope.currentProperties.id,
					  color: scope.currentProperties.color
				  };
				  templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__COLOR, objectToSend);

			  }
		  }

		  $rootScope.$on(availableEvents.UPDATED_TEXT_PROPS, function(event, data) {
			  scope.currentProperties = data;
			  scope.isBold            = false;
			  scope.isItalic          = false;
			  scope.isUnderlined      = false;

			  if (scope.currentProperties.fontStyle === 'italic') {
				  scope.isItalic = true;
			  }

			  if (scope.currentProperties.textDecoration === 'underline') {
				  scope.isUnderlined = true;
			  }

			  if (scope.currentProperties.fontWeight == 700) {
				  scope.isBold = true;
			  }
		  });

		  $rootScope.$on(availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, function() {
				if($rootScope.currentActiveComponentIdentifier === null) {
					scope.currentProperties = {
					  "fontSize"			: 16,
					  "fontFamily"		: scope.fonts[0].family, //first fontFamily
						"color"					: "#000",
					  "fontStyle"			: 'inherit',
					  "textDecoration": 'none',
					  "textAlign"			: 'left',
					  "opacity"				: 100,
					  "fontWeight"		: 400
				  };

				  scope.isBold            = false;
				  scope.isItalic          = false;
				  scope.isUnderlined      = false;
				}
		  });

			function processCustomFields(fields) {
				var arr = [];
				angular.forEach(fields, function(field) {
					var obj = {};
					obj.dataDragValue = field.id;
					obj.shownValue 		= field.name;
					arr.push(obj);
				});

				return arr;
			}

		  function getFonts() {
				var deferred = $q.defer();

			  templateEditingService.getGoogleFontsList().then(function(response) {
				  scope.fonts = response.data.items;
					return response;
			  }, function(error) {
					return error;
			  })

				return deferred.promise;

		  };
		 }
	};

}());
