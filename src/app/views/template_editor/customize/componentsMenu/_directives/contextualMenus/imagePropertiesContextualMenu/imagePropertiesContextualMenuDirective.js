(function(){
	'use strict';

  var directiveId = 'imagePropertiesContextualMenu';
	var app 				= angular.module("app");
	var dependenciesArray = [
		'$rootScope',
		'templateEventsDispatcher',
		'toastr',
		imagePropertiesContextualMenuDirective
	];

	app.directive(directiveId, dependenciesArray);

	function imagePropertiesContextualMenuDirective($rootScope, templateEventsDispatcher, toastr) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/contextualMenus/imagePropertiesContextualMenu/imagePropertiesContextualMenuView.html',
			replace: true,
			link: link,
			scope: {

			}
		}

    function link(scope) {
    	var availableEvents = templateEventsDispatcher.availableEvents;

    	scope.isConstrained = false;
    	scope.transparencyLevel = 100;
    	scope.currentClickedImageId = undefined;

    	scope.uploadFiles = function(files, invalidFiles) {
    		if (files) {
					if ($rootScope.badgeRotationDegree === 0) {
						var eventToSend = availableEvents.ADD_IMAGE_COMPONENT;
					} else if ($rootScope.badgeRotationDegree === 180) {
						var eventToSend = availableEvents.ADD_BACK_IMAGE_COMPONENT;
					}

					var imageObject = {
						file            : files,
      			link            : files.$ngfBlobUrl,
            height          : files.$ngfHeight,
            width           : files.$ngfWidth,
      			opacity         : scope.transparencyLevel,
      			isConstrained   : scope.isConstrained,
      			isBackground    : scope.isBackground
      		};
    			templateEventsDispatcher.dispatchEvent(eventToSend, imageObject)
    		} else if (invalidFiles) {
					var errorMessage = "The image you tried to upload exceeds ";

					if (invalidFiles[0].$errorMessages.maxHeight) {
						errorMessage += "the maximum height of " + invalidFiles[0].$errorParam + " pixels!"
					}
					if (invalidFiles[0].$errorMessages.maxSize) {
						errorMessage += "the maximum size of " + invalidFiles[0].$errorParam + " megabyte!"
					}

					toastr.error(errorMessage);
				}
    	}

    	scope.toggleConstrain = function(state) {
    		scope.isConstrained = !state;

    		if (scope.currentClickedImageId && scope.currentClickedImageId === $rootScope.currentActiveComponentIdentifier ) {
    			var objectToSend = {
    				componentId     : scope.currentClickedImageId,
    				isConstrained   : scope.isConstrained
    			};

    			templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_IMAGE_PROPS__CONSTRAINING, objectToSend);
    		}
    	}

    	scope.notifyComponentToUpdateTransparencyLevel = function() {
    		if (scope.currentClickedImageId && scope.currentClickedImageId === $rootScope.currentActiveComponentIdentifier ) {
    			var objectToSend = {
    				componentId     : scope.currentClickedImageId,
    				opacity         : scope.transparencyLevel
    			};
	    		templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_IMAGE_PROPS__OPACITY, objectToSend);
    		}
    	}

    	$rootScope.$on(availableEvents.UPDATED_IMAGE_PROPS, function(event, data) {
    		scope.isConstrained           = data.isConstrained;
    		scope.transparencyLevel       = data.opacity;
    		scope.currentClickedImageId   = data.id;
    	});


    }
	};

}());
