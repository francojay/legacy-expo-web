(function(){
	'use strict';

    var directiveId = 'codePropertiesContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'toastr', 'templateEventsDispatcher', codePropertiesContextualMenuDirective]);

	function codePropertiesContextualMenuDirective($rootScope, toastr, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/contextualMenus/codePropertiesContextualMenu/codePropertiesContextualMenuView.html',
			replace: true,
			link: link,
			scope: {

			}
		}

    function link(scope) {
			scope.currentProperties = {
				width: 90,
				height: 90,
				opacity: 100
			}
			var availableEvents = templateEventsDispatcher.availableEvents;

			scope.notifyBadgeToAddCode = function() {
				var numberOFQRCodes = $rootScope.componentsList.imprint.filter(function (component){
					return component.type === 'CODE'
				}).length;

				if (numberOFQRCodes === 0) {
					templateEventsDispatcher.dispatchEvent(availableEvents.ADD_CODE_COMPONENT, scope.currentProperties);
				} else {
					toastr.warning("You can have only one QR Code Component per Badge");
				}

			}
    }
	};

}());
