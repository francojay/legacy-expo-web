(function(){
	'use strict';

    var directiveId = 'componentsMenuButton';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', '$timeout', 'templateEventsDispatcher', componentsMenuButtonDirective]);

	function componentsMenuButtonDirective($rootScope, $timeout, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/componentsMenuButton/componentsMenuButtonView.html',
			replace: true,
			link: link,
			scope: {
				icon: "@icon",
				name: "@name",
				contextualMenuTitle: "@contextualMenuTitle",
				typeOfContextualMenu: "@typeOfContextualMenu",
				props: "@props"
			}
		}

    function link(scope) {
			scope.contextualMenuIsActive = false;
			$rootScope.currentActiveComponentMenu = {}

			scope.toggleContextualMenu = function($event, state, typeOfTab) {
				$event.stopPropagation();
				$event.preventDefault();
				$rootScope.currentActiveComponentMenu = {type: typeOfTab};
				templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, typeOfTab);
				scope.contextualMenuIsActive = !state;
			}

			$rootScope.$on(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, function(event, typeOfTab) {
				$rootScope.currentActiveComponentMenu = {type: typeOfTab};
				scope.contextualMenuIsActive = false;
			});

			$rootScope.$on(templateEventsDispatcher.availableEvents.OPEN_TEXT_MENU_FROM_BADGE_COMPONENT, function(event, data) {
				if (scope.typeOfContextualMenu === 'text') {
						if($rootScope.currentActiveComponentMenu && $rootScope.currentActiveComponentMenu.type != 'Text'){

							templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, 'Text');

							$timeout(function(){
								scope.contextualMenuIsActive = true;
								templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_TEXT_PROPS, data);
							}, 200);

						} else{
							scope.contextualMenuIsActive = true;
							templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_TEXT_PROPS, data);
						}
				}
			});

			$rootScope.$on(templateEventsDispatcher.availableEvents.OPEN_IMAGE_MENU_FROM_BADGE_COMPONENT, function(event, data) {
				if (scope.typeOfContextualMenu === 'image') {
					if($rootScope.currentActiveComponentMenu && $rootScope.currentActiveComponentMenu.type != 'Image'){

						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, 'Image');

						$timeout(function(){
							scope.contextualMenuIsActive = true;
							templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_IMAGE_PROPS, data);
						}, 200);

					} else{
						scope.contextualMenuIsActive = true;
						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_IMAGE_PROPS, data);
					}
				}
			});

			$rootScope.$on(templateEventsDispatcher.availableEvents.OPEN_SHAPE_MENU_FROM_BADGE_COMPONENT, function(event, data) {
				if (scope.typeOfContextualMenu === 'shape') {
					if($rootScope.currentActiveComponentMenu && $rootScope.currentActiveComponentMenu.type != 'Shape'){

						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, 'Shape');

						$timeout(function(){
							scope.contextualMenuIsActive = true;
							templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_SHAPE_PROPS, data);
						}, 200);

					} else{
						scope.contextualMenuIsActive = true;
						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.UPDATED_SHAPE_PROPS, data);
					}
				}
			});

			$rootScope.$on(templateEventsDispatcher.availableEvents.OPEN_CODE_MENU_FROM_BADGE_COMPONENT, function(event, data) {
				if (scope.typeOfContextualMenu === 'code') {
					if($rootScope.currentActiveComponentMenu && $rootScope.currentActiveComponentMenu.type != 'Code'){
						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS, 'Code');
						scope.contextualMenuIsActive = true;
					}
				}
			})
    }
	};

}());
