(function(){
	'use strict';

    var directiveId = 'componentsMenuButtonContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', componentsMenuButtonContextualMenuDirective]);

	function componentsMenuButtonContextualMenuDirective($rootScope, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/componentsMenu/_directives/componentsMenuButtonContextualMenu/componentsMenuButtonContextualMenuView.html',
			replace: true,
			link: link,
			scope: {
				title: "@title",
				typeOfContextualMenu: "=typeOfContextualMenu"
			}
		}

        function link(scope) {
					$('.components-menu-button-wrapper .components-menu-button-contextual-menu').mCustomScrollbar({'theme': 'minimal-dark'});
					scope.$watch('$viewContentLoaded', function() {
						interact('.drag-drop').draggable({
							'manualStart' : true,
							'onmove' : function (event) {
								var target = event.target;
								var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
								var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;
								target.style.webkitTransform = target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
								target.setAttribute('data-x', x);
								target.setAttribute('data-y', y);
							}
						}).on('move', function (event) {
								var interaction = event.interaction;
								if (interaction.pointerIsDown && !interaction.interacting() && event.currentTarget.classList.contains('drag-element-source') && event.currentTarget.getAttribute('clonable') != 'false') {
									var original = event.currentTarget;
									var clone = event.currentTarget.cloneNode(true);
									clone.setAttribute('clonable','false');
									original.appendChild(clone);
									clone.className = clone.className.replace(/\bdrag-element-source\b/,'');
									interaction.start({ name: 'drag' }, event.interactable, clone);
							}
						});
					});
					scope.preventClick = function($event) {
        		$event.preventDefault();
        		$event.stopPropagation();
        	}
        }
	};
}());
