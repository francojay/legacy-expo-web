(function () {
    'use strict'
    var controllerId = 'customizeOptionsMenuPartialController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', 'templateEventsDispatcher', 'toastr', '$state', '$stateParams', '$timeout', 'ConferenceService', 'templateEditorApiService', '$compile', customizeOptionsMenuPartialController]);
    function customizeOptionsMenuPartialController($rootScope, $scope, templateEventsDispatcher, toastr, $state, $stateParams, $timeout, ConferenceService, templateEditorApiService, $compile) {
      getBadgeData($stateParams.conference_id, $stateParams.badge_id);
      getComponentsIfNecessary($state.current.name, $stateParams);

    	var availableEvents = templateEventsDispatcher.availableEvents;
      var conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];
      var badgeId = $state.params.badge_id;
    	$scope.undoEvent             = availableEvents.UNDO_ACTION;
      $scope.redoEvent             = availableEvents.REDO_ACTION;
      $scope.showBadgeSidesSwitch  = $rootScope.badgeNumberSides && $rootScope.badgeNumberSides === 2 ? true : false;

      $rootScope.autosaveTimerId = 0;

      var savedComponentsOnServer  = {
        frontComponents: {
          shell: [],
          imprint: []
        },
        backComponents: {
          back: []
        }
      };


      $scope.alignContextualMenuData = [
          {
              name: "Left",
              event: availableEvents.ALIGN_TO_LEFT,
              icon: "fa-align-left"
          },
          {
              name: "Right",
              event: availableEvents.ALIGN_TO_RIGHT,
              icon: "fa-align-right"
          },
          {
              name: "Center",
              event: availableEvents.ALIGN_TO_CENTER,
              icon: "fa-align-center"
          },
      ];

      $scope.arrangeContextualMenuData = [
          {
              name: "Send Backward",
              event: availableEvents.SEND_BACKWARD,
              icon: "icon-send-backward"
          },
          {
              name: "Send Forward",
              event: availableEvents.SEND_FORWARD,
              icon: "icon-send-forward"
          },
          {
              name: "Send to Back",
              event: availableEvents.SEND_TO_BACK,
              icon: "icon-send-to-back"
          },
          {
              name: "Send to Front",
              event: availableEvents.SEND_TO_FRONT,
              icon: "icon-send-to-front"
          },
      ];

      $scope.optionsContextualMenuData = [
          {
              name: "Rotate 90°",
              event: availableEvents.ROTATE_90_DEGREES,
              icon: "rotate90(option-icon).png"
          },
          {
              name: "Snap to Edges",
              event: availableEvents.SNAP_TO_EDGES,
              icon: "snap to edges(option-icon).png"
          },
          {
              name: "Full Screen",
              event: availableEvents.GO_FULL_SCREEN,
              icon: "fullscreen(option-icon).png"
          },
      ];

      $timeout(function() {
          $scope.showCustomizeMenu = true;
          $scope.$apply();
      }, 0)

      $scope.saveTemplateEventListener = $rootScope.$on(availableEvents.SAVE_TEMPLATE, function(event, data) {
        $scope.saveComponentsAndNextStep(data.toState);
        $scope.saveTemplateEventListener(); //destroy listener
      });

      $scope.saveComponentsAndNextStep = function(toState) {
        var objectToSend = {
          frontComponents: {
            shell: [],
            imprint: []
          },
          backComponents: {
            back: []
          }
        };

        objectToSend.frontComponents.shell   = removeActiveComponentFlag($rootScope.componentsList.shell);
        objectToSend.frontComponents.imprint = removeActiveComponentFlag($rootScope.componentsList.imprint);
        objectToSend.backComponents.back     = removeActiveComponentFlag($rootScope.backSideComponents);

        templateEditorApiService.saveComponents(conferenceId, badgeId, objectToSend).then(function(response) {

          toastr.success("Template saved successfully");
          savedComponentsOnServer = JSON.parse(angular.toJson(objectToSend));
          templateEventsDispatcher.dispatchEvent(availableEvents.SHOW_BADGE_FRONT);

          clearInterval($rootScope.autosaveTimerId);
          $rootScope.autosaveTimerId = null;

          var objectToSendForBadgeUpdate = {
            'id'                 : badgeId,
            'width'              : $rootScope.badgeWidth,
            'height'             : $rootScope.badgeHeight,
            'sides'              : $rootScope.badgeNumberSides,
            'title'              : $rootScope.badgeName,
            'background'         : $rootScope.badgeBackgroundColor,
            'backSideBackground' : $rootScope.badgeBackSideBackgroundColor
          }

          if (toState !== 'template_editor.define_badge') {
            templateEditorApiService.updateBadge(conferenceId, objectToSendForBadgeUpdate).then(function(response) {
              var encodedConferenceId = new Hashids('Expo', 7).encode(conferenceId);
              $state.go('template_editor.review', {'conference_id': encodedConferenceId, 'badge_id': badgeId});
            }, function(error) {

            });
          }

        }, function(error) {
          toastr.error("Autosaving failed!")
        })
      }

      $scope.goToManageTemplates = function() {
        $state.go('conferences.dashboard.attendees.expo_badges', {'conference_id': $stateParams.conference_id});
      }

      function startAutosaveTimer() {
        var autosaveTimerId = setInterval(function() {
          var objectToSend = {
            frontComponents: {
              shell: [],
              imprint: []
            },
            backComponents: {
              back: []
            }
          };

          objectToSend.frontComponents.shell   = removeActiveComponentFlag($rootScope.componentsList.shell);
          objectToSend.frontComponents.imprint = removeActiveComponentFlag($rootScope.componentsList.imprint);
          objectToSend.backComponents.back     = removeActiveComponentFlag($rootScope.backSideComponents);

          var componentsHaveNotBeenModifiedSinceLastSave = _.isEqual(savedComponentsOnServer, objectToSend);

          if (!componentsHaveNotBeenModifiedSinceLastSave) {
            templateEditorApiService.saveComponents(conferenceId, badgeId, objectToSend).then(function(response) {
              toastr.success("Progress saved successfully!");
              savedComponentsOnServer = JSON.parse(angular.toJson(objectToSend));
            }, function(error) {
              toastr.error("Autosaving failed!")
            })
          }
        }, 20000);

        return autosaveTimerId;
      }

      function removeActiveComponentFlag(list) {
        angular.forEach(list, function(listItem) {
          listItem.props.componentSelected = false;
          listItem.visibleContextualMenu   = false;
          delete listItem['$$hashKey'];
        });

        return list;
      }

      function getBadgeData(conferenceId, badgeId) {
        conferenceId = new Hashids('Expo', 7).decode(conferenceId)[0];

        templateEditorApiService.getBadgeData(conferenceId, badgeId).then(function(response) {
          $rootScope.badgeWidth           = response.metadata.width;
          $rootScope.badgeHeight          = response.metadata.height;
          $rootScope.badgeBackgroundColor = response.metadata.background;
          $rootScope.badgeNumberSides     = response.metadata.sides;
          $rootScope.badgeName            = response.metadata.title;
          $rootScope.badgeBackSideBackgroundColor = response.metadata.backSideBackground || '#ffffff';

          $scope.showBadgeSidesSwitch  = $rootScope.badgeNumberSides === 2 ? true : false;
        })
      }

       function deleteAllComponents(componentsList) {
         if (componentsList && componentsList.length > 0) {
           var eventsArray = ['DELETE_TEXT_COMPONENT', 'DELETE_SHAPE_COMPONENT', 'DELETE_IMAGE_COMPONENT', 'DELETE_CODE_COMPONENT'];

           angular.forEach(componentsList, function(component) {
             _.find(eventsArray, function(eventText) {
               if (eventText.indexOf(component.type) > 0) {
                 templateEventsDispatcher.dispatchEvent(eventText, {componentId: component.id});
               }
             })
           });
         }
       }

      function getComponentsIfNecessary(currentState, stateParams) {
        var badgeId = stateParams.badge_id;
        var conferenceId = new Hashids('Expo', 7).decode(stateParams.conference_id)[0];

        deleteAllComponents($rootScope.componentsList.shell);
        deleteAllComponents($rootScope.componentsList.imprint);
        deleteAllComponents($rootScope.backSideComponents);

        templateEditorApiService.getBadgeComponents(conferenceId, badgeId).then(function(response) {
          $rootScope.autosaveTimerId = startAutosaveTimer();
          $rootScope.backSideComponents = angular.copy(response.backComponents.back) || [];
          if (!('shell' in response.frontComponents) && !('imprint' in response.frontComponents)) {
            $rootScope.componentsList = {
              shell: [],
              imprint: []
            }
          } else {
            $rootScope.componentsList = angular.copy(response.frontComponents);
          }

          angular.forEach($rootScope.componentsList.shell, function(component) {
            if (component) {
              var badgeInnerContainer = $('#template-safe-zone');

              var newScope = $scope.$new(true);
              newScope.props = component.props;
              if (component.type === "TEXT") {
                  loadDefaultFontFamily(component.props.id, component.props.fontFamily);
                var element = $compile('<text-component data-props="props"></text-component>')(newScope)
              } else if (component.type === "IMAGE") {
                newScope.props.file = null;
                var element = $compile('<image-component data-props="props"></image-component>')(newScope);
              } else if (component.type === "SHAPE") {
                var element = $compile('<shape-component data-props="props"></shape-component>')(newScope);
              }

              badgeInnerContainer.append(element);
            }
          });
          angular.forEach($rootScope.componentsList.imprint, function(component) {
            if (component) {
              var badgeInnerContainer = $('#template-safe-zone');

              var newScope = $scope.$new(true);
              newScope.props = component.props;
              if (component.type === "TEXT") {
                  loadDefaultFontFamily(component.props.id, component.props.fontFamily);
                var element = $compile('<text-component data-props="props"></text-component>')(newScope)
              } else if (component.type === "IMAGE") {
                newScope.props.file = null;
                var element = $compile('<image-component data-props="props"></image-component>')(newScope);
              } else if (component.type === "SHAPE") {
                var element = $compile('<shape-component data-props="props"></shape-component>')(newScope);
              } else if (component.type === "CODE") {
                var element = $compile('<code-component data-props="props"></code-component>')(newScope);
              }
              badgeInnerContainer.append(element);
            }
          });
          }, function(error) {
            toastr.error("An error has occured!", "Some data failed to fetch, please refresh!");
          });
        }

        function loadDefaultFontFamily(componentId, font) {
            var head = document.getElementsByTagName('head')[0];
            var link = document.createElement('link');

            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = 'https://fonts.googleapis.com/css?family='+ font;
            link.media = 'all';
            head.appendChild(link);

            var objectToSend = {
              componentId: componentId,
              fontFamily: font
            }
            templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_FAMILY, objectToSend);
        };
      }
})();
