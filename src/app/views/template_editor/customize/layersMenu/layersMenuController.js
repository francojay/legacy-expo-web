(function () {
    'use strict'
    var controllerId = 'layersMenuController';

    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', 'templateEventsDispatcher', 'badgeHistoryService', '$timeout', 'SharedProperties', layersMenuController]);

    function layersMenuController($rootScope, $scope, templateEventsDispatcher, badgeHistoryService, $timeout, SharedProperties) {
      $scope.shellTabActive                       = true;
      $scope.imprintTabActive                     = false;
      $scope.hideImprintMenu                      = false;
      $rootScope.currentActiveComponentIdentifier = null;

      var availableEvents = templateEventsDispatcher.availableEvents;

      $scope.changeTab = function(typeOfTab) {
        $scope.shellTabActive = false;
        $scope.imprintTabActive = false
        if (typeOfTab === 'shell') {
        	$scope.shellTabActive = true;
          templateEventsDispatcher.dispatchEvent(availableEvents.CURRENT_FOLDER_TO_ADD_CHANGED, {typeOfTab: 'shell'});
        } else if (typeOfTab === 'imprint') {
        	$scope.imprintTabActive = true;
        	templateEventsDispatcher.dispatchEvent(availableEvents.CURRENT_FOLDER_TO_ADD_CHANGED, {typeOfTab: 'imprint'});
        }
      };

      $scope.openContextualMenu = function($event, component, list) {
        $event.stopPropagation();
        $event.preventDefault();

        _.each(list, function(listItem) {
          if (listItem.id != component.id) {
            listItem.visibleContextualMenu = false;
          }
        })

        component.visibleContextualMenu = !component.visibleContextualMenu;
      }

      $scope.moveLayerToADifferentFolder = function($event, component, destination) {
        var oldLists = {};
        var newLists = {};

        oldLists.shell   = angular.copy($rootScope.componentsList.shell);
        oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

        if (destination === 'imprint') {
          angular.forEach($rootScope.componentsList.shell, function(item, index) {
              if (item.id === component.id) {
                var copy = angular.copy($rootScope.componentsList.shell[index]);
                copy.partOf = 'imprint';
                copy.props.partOf = 'imprint';
                copy.visibleContextualMenu = false;
                $rootScope.componentsList.shell.splice(index, 1);
                $rootScope.componentsList.imprint.push(copy);
                templateEventsDispatcher.dispatchEvent(availableEvents.CHANGE_FOLDER_FOR_COMPONENT, copy);
              }
          });
        } else if (destination === 'shell') {
          angular.forEach($rootScope.componentsList.imprint, function(item, index) {
              if (item.id === component.id) {
                var copy = angular.copy($rootScope.componentsList.imprint[index]);
                copy.partOf = 'shell';
                copy.props.partOf = 'shell';
                copy.visibleContextualMenu = false;
                $rootScope.componentsList.imprint.splice(index, 1);
                $rootScope.componentsList.shell.push(copy);
                templateEventsDispatcher.dispatchEvent(availableEvents.CHANGE_FOLDER_FOR_COMPONENT, copy);
              }
          });
        }
        templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_OPEN_FOLDER, {folder: destination});

        newLists.shell   = angular.copy($rootScope.componentsList.shell);
        newLists.imprint = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
      }

      $scope.deleteLayer = function($event, component, from) {
        if (from === 'shell') {
          angular.forEach($rootScope.componentsList.shell, function(item, index) {
              if (item.id === component.id) {
                $rootScope.componentsList.shell.splice(index, 1);
              }
          });

          angular.forEach($rootScope.backSideComponents, function(item, index) {
              if (item.id === component.id) {
                $rootScope.backSideComponents.splice(index, 1);
              }
          });

        } else if (from === 'imprint') {
          angular.forEach($rootScope.componentsList.imprint, function(item, index) {
              if (item.id === component.id) {
                $rootScope.componentsList.imprint.splice(index, 1);
              }
          });
        }

        if(component.type === 'TEXT'){
          templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_TEXT_COMPONENT, {
            componentId: component.id
          });
        } else if(component.type === 'SHAPE'){
          templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_SHAPE_COMPONENT, {
            componentId: component.id
          });
        } else if(component.type === 'IMAGE'){
          templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_IMAGE_COMPONENT, {
            componentId: component.id
          });
        } else if(component.type === 'CODE'){
          templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_CODE_COMPONENT, {
            componentId: component.id
          });
        }

        $rootScope.currentActiveComponentIdentifier = null;
        var shellList   = angular.copy($rootScope.componentsList.shell);
        var imprintList = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(shellList, imprintList);
      }

      $scope.makeActiveLayer = function($event, component) {
        var activeLayerComponentId = null;

        angular.forEach($rootScope.componentsList.imprint, function(badgeComponent) {
          if (badgeComponent.id !== component.id) {
            badgeComponent.active = false;
          } else {
            badgeComponent.active = !badgeComponent.active;
          }
        });

        angular.forEach($rootScope.componentsList.shell, function(badgeComponent) {
          if (badgeComponent.id !== component.id) {
            badgeComponent.active = false;
          } else if (badgeComponent.id === component.id) {
            badgeComponent.active = !badgeComponent.active;
          }
        });

        angular.forEach($rootScope.backSideComponents, function(badgeComponent) {
          if (badgeComponent.id !== component.id) {
            badgeComponent.active = false;
          } else if (badgeComponent.id === component.id) {
            badgeComponent.active = !badgeComponent.active;
          }
        });

        var activeComponentInImprint            = _.find($rootScope.componentsList.imprint, {'active': true});
        var activeComponentInShell              = _.find($rootScope.componentsList.shell, {'active': true});
        var activeComponentInBackSideComponents = _.find($rootScope.backSideComponents, {'active': true});

        if (activeComponentInImprint) {
          activeLayerComponentId = activeComponentInImprint.id;
        } else if (activeComponentInShell) {
          activeLayerComponentId = activeComponentInShell.id;
        } else if(activeComponentInBackSideComponents) {
            activeLayerComponentId = activeComponentInBackSideComponents.id;
        }

        if (component.status) {
          $rootScope.currentActiveComponentIdentifier = component.id;
        } else {
          $rootScope.currentActiveComponentIdentifier = activeLayerComponentId; // null if no component selected
        }

        templateEventsDispatcher.dispatchEvent(availableEvents.MAKE_COMPONENT_ACTIVE, activeLayerComponentId);
      }

      $scope.$on('dragToReorder.reordered', function ($event, reordered) {
        var shellList = angular.copy($rootScope.componentsList.shell);
        var imprintList = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(shellList, imprintList);
        angular.forEach($rootScope.componentsList.shell, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });

        angular.forEach($rootScope.backSideComponents, function(component, index) {
          templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
              componentId: component.id,
              zIndex: $rootScope.backSideComponents.length - index
          });
        });

      });

      $rootScope.$on(availableEvents.MAKE_LAYER_COMPONENT_ACTIVE, function(event, component){
          var atLeastOneComponentActive = null;

          angular.forEach($rootScope.componentsList.shell, function(badgeComponent) {
            if (badgeComponent.id === component.data.id) {
              badgeComponent.active = component.status;
              if(badgeComponent.active){
                atLeastOneComponentActive = badgeComponent.id;
                $scope.changeTab(component.data.partOf);
              }
            } else if(badgeComponent.active){
              atLeastOneComponentActive = badgeComponent.id;
            }
          });

          angular.forEach($rootScope.componentsList.imprint, function(badgeComponent) {
            if (badgeComponent.id === component.data.id) {
              badgeComponent.active = component.status;
              if(badgeComponent.active){
                atLeastOneComponentActive = badgeComponent.id;
                $scope.changeTab(component.data.partOf);
              }
            } else if(badgeComponent.active){
              atLeastOneComponentActive = badgeComponent.id;
            }
          });

          angular.forEach($rootScope.backSideComponents, function(badgeComponent) {
            if (badgeComponent.id === component.data.id) {
              badgeComponent.active = component.status;
              if(badgeComponent.active){
                atLeastOneComponentActive = badgeComponent.id;
              }
            } else if(badgeComponent.active){
              atLeastOneComponentActive = badgeComponent.id;
            }
          });

          if(component.status){
            $rootScope.currentActiveComponentIdentifier = component.data.id;
          } else{
            $rootScope.currentActiveComponentIdentifier = atLeastOneComponentActive;
          }

      });

      $rootScope.$on(availableEvents.UPDATE_LAYERS_MENU_ORDER, function(event, data) {
        if ($rootScope.badgeRotationDegree === 0) {
          updateLayersMenuOrderFront(data)
        } else if ($rootScope.badgeRotationDegree === 180) {
          updateLayersMenuOrderBack(data)
        }
      });

      $rootScope.$on(availableEvents.UPDATE_OPEN_FOLDER, function(event, data) {
        $scope.changeTab(data.folder);
      });

      $rootScope.$on(availableEvents.SEND_BACKWARD, function(event, data) {
        if ($rootScope.badgeRotationDegree === 0) {
          sendBackwardInBadgeFront(data)
        } else if ($rootScope.badgeRotationDegree === 180) {
          sendBackwardInBadgeBack(data)
        }
      });

      $rootScope.$on(availableEvents.SEND_FORWARD, function(event, data) {
        if ($rootScope.badgeRotationDegree === 0) {
          sendForwardInBadgeFront(data)
        } else if ($rootScope.badgeRotationDegree === 180) {
          sendForwardInBadgeBack(data)
        }
      })

      $rootScope.$on(availableEvents.SEND_TO_FRONT, function(event, data) {
        if ($rootScope.badgeRotationDegree === 0) {
          sendToFrontInBadgeFront(data)
        } else if ($rootScope.badgeRotationDegree === 180) {
          sendToFrontInBadgeBack(data)
        }
      });

      $rootScope.$on(availableEvents.SEND_TO_BACK, function(event, data) {
        if ($rootScope.badgeRotationDegree === 0) {
          sendToBackInBadgeFront(data)
        } else if ($rootScope.badgeRotationDegree === 180) {
          sendToBackInBadgeBack(data)
        }
      });

      $rootScope.$on(availableEvents.SHOW_BADGE_BACK, function(event, data){
        $scope.imprintTabActive = false;
        $scope.hideImprintMenu  = true;
        $scope.shellTabActive   = true;
      });

      $rootScope.$on(availableEvents.SHOW_BADGE_FRONT, function(event, data){
        $scope.hideImprintMenu = false;
      });

      function updateLayersMenuOrderFront(data) {
        $rootScope.componentsList.shell = data.shell;
        $rootScope.componentsList.imprint = data.imprint;
        angular.forEach($rootScope.componentsList.shell, function(shellItem, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: shellItem.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(imprintItem, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: imprintItem.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });
      };

      function updateLayersMenuOrderFront(data) {
        $rootScope.backSideComponents = data;

        angular.forEach($rootScope.backSideComponents, function(backItem, index) {
          templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
              componentId: backItem.id,
              zIndex: $rootScope.backSideComponents.length - index
          });
        });
      };

      function sendBackwardInBadgeFront(data) {
        var oldLists = {};
        var newLists = {};

        oldLists.shell   = angular.copy($rootScope.componentsList.shell);
        oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

        var iterating = true;

        angular.forEach($rootScope.componentsList.shell, function(shellItem, index) {
          if (shellItem.id === $rootScope.currentActiveComponentIdentifier && iterating) {
            if ($rootScope.componentsList.shell.length === 2) {
              var itemToBeMovedDown = $rootScope.componentsList.shell.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.componentsList.shell.splice(0, 1)[0];

              $rootScope.componentsList.shell.push(itemToBeMovedUp);
              $rootScope.componentsList.shell.push(itemToBeMovedDown);
            } else if ($rootScope.componentsList.shell.length > 2) {
              var itemToBeMovedDown = $rootScope.componentsList.shell.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.componentsList.shell.splice(index, 1)[0];

              $rootScope.componentsList.shell.splice(index, 0, itemToBeMovedUp);
              $rootScope.componentsList.shell.splice(index + 1, 0, itemToBeMovedDown);
              iterating = false;
            }
          }
        });

        var iterating = true;

        angular.forEach($rootScope.componentsList.imprint, function(imprintItem, index) {
          if (imprintItem.id === $rootScope.currentActiveComponentIdentifier && iterating) {
            if ($rootScope.componentsList.shell.length === 2) {
              var itemToBeMovedDown = $rootScope.componentsList.imprint.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.componentsList.imprint.splice(0, 1)[0];

              $rootScope.componentsList.im.push(itemToBeMovedUp);
              $rootScope.componentsList.im.push(itemToBeMovedDown);
            } else if ($rootScope.componentsList.im.length > 2) {
              var itemToBeMovedDown = $rootScope.componentsList.im.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.componentsList.im.splice(index, 1)[0];

              $rootScope.componentsList.im.splice(index, 0, itemToBeMovedUp);
              $rootScope.componentsList.im.splice(index + 1, 0, itemToBeMovedDown);
              iterating = false;
            }
          }
        });

        newLists.shell   = angular.copy($rootScope.componentsList.shell);
        newLists.imprint = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.componentsList.shell, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });
      };

      function sendBackwardInBadgeBack(data) {
        var oldLists = {};
        var newLists = {};

        oldLists     = angular.copy($rootScope.backSideComponents);

        var iterating = true;

        angular.forEach($rootScope.backSideComponents, function(backItem, index) {
          if (backItem.id === $rootScope.currentActiveComponentIdentifier && iterating) {
            if ($rootScope.backSideComponents.length === 2) {
              var itemToBeMovedDown = $rootScope.backSideComponents.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.backSideComponents.splice(0, 1)[0];

              $rootScope.backSideComponents.push(itemToBeMovedUp);
              $rootScope.backSideComponents.push(itemToBeMovedDown);
            } else if ($rootScope.backSideComponents.length > 2) {
              var itemToBeMovedDown = $rootScope.backSideComponents.splice(index, 1)[0];
              var itemToBeMovedUp   = $rootScope.backSideComponents.splice(index, 1)[0];

              $rootScope.backSideComponents.splice(index, 0, itemToBeMovedUp);
              $rootScope.backSideComponents.splice(index + 1, 0, itemToBeMovedDown);
              iterating = false;
            }
          }
        });

        newLists = angular.copy($rootScope.backSideComponents);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.backSideComponents, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.backSideComponents.length - index
            });
        });
      };

      function sendForwardInBadgeFront(data) {
        var oldLists = {};
        var newLists = {};

        oldLists.shell   = angular.copy($rootScope.componentsList.shell);
        oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

        angular.forEach($rootScope.componentsList.shell, function(shellItem, index) {
          if (shellItem.id === $rootScope.currentActiveComponentIdentifier) {
              var itemToBeMovedUp   = $rootScope.componentsList.shell.splice(index, 1)[0];
              var itemToBeMovedDown = $rootScope.componentsList.shell.splice(index - 1, 1)[0];
              $rootScope.componentsList.shell.splice(index - 1, 0, itemToBeMovedUp);
              $rootScope.componentsList.shell.splice(index, 0, itemToBeMovedDown);
          }
        });

        angular.forEach($rootScope.componentsList.imprint, function(imprintItem, index) {
          if (imprintItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp   = $rootScope.componentsList.imprint.splice(index, 1)[0];
            var itemToBeMovedDown = $rootScope.componentsList.imprint.splice(index - 1, 1)[0];

            $rootScope.componentsList.imprint.splice(index - 1, 0, itemToBeMovedUp);
            $rootScope.componentsList.imprint.splice(index, 0, itemToBeMovedDown);
          }
        });

        newLists.shell   = angular.copy($rootScope.componentsList.shell);
        newLists.imprint = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.componentsList.shell, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });
      };

      function sendForwardInBadgeBack(data) {
        var oldLists = {};
        var newLists = {};

        oldLists   = angular.copy($rootScope.backSideComponents);

        angular.forEach($rootScope.backSideComponents, function(backItem, index) {
          if (backItem.id === $rootScope.currentActiveComponentIdentifier) {
              var itemToBeMovedUp   = $rootScope.backSideComponents.splice(index, 1)[0];
              var itemToBeMovedDown = $rootScope.backSideComponents.splice(index - 1, 1)[0];
              $rootScope.backSideComponents.splice(index - 1, 0, itemToBeMovedUp);
              $rootScope.backSideComponents.splice(index, 0, itemToBeMovedDown);
          }
        });

        newLists   = angular.copy($rootScope.backSideComponents);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.backSideComponents, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.backSideComponents.length - index
            });
        });
      };

      function sendToFrontInBadgeFront() {
        var oldLists = {};
        var newLists = {};

        oldLists.shell   = angular.copy($rootScope.componentsList.shell);
        oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

        angular.forEach($rootScope.componentsList.shell, function(shellItem, index) {
          if (shellItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.componentsList.shell.splice(index, 1)[0];
            $rootScope.componentsList.shell.unshift(itemToBeMovedUp);
          }
        });

        angular.forEach($rootScope.componentsList.imprint, function(imprintItem, index) {
          if (imprintItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.componentsList.imprint.splice(index, 1)[0];
            $rootScope.componentsList.imprint.unshift(itemToBeMovedUp);
          }
        });

        newLists.shell   = angular.copy($rootScope.componentsList.shell);
        newLists.imprint = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.componentsList.shell, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });
      };

      function sendToFrontInBadgeBack() {
        var oldLists = {};
        var newLists = {};

        oldLists   = angular.copy($rootScope.backSideComponents);

        angular.forEach($rootScope.backSideComponents, function(backItem, index) {
          if (backItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.backSideComponents.splice(index, 1)[0];
            $rootScope.backSideComponents.unshift(itemToBeMovedUp);
          }
        });

        newLists   = angular.copy($rootScope.backSideComponents);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.backSideComponents, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.backSideComponents.length - index
            });
        });
      };

      function sendToBackInBadgeFront() {
        var oldLists = {};
        var newLists = {};

        oldLists.shell   = angular.copy($rootScope.componentsList.shell);
        oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

        angular.forEach($rootScope.componentsList.shell, function(shellItem, index) {
          if (shellItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.componentsList.shell.splice(index, 1)[0];
            $rootScope.componentsList.shell.push(itemToBeMovedUp);
          }
        });

        angular.forEach($rootScope.componentsList.imprint, function(imprintItem, index) {
          if (imprintItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.componentsList.imprint.splice(index, 1)[0];
            $rootScope.componentsList.imprint.push(itemToBeMovedUp);
          }
        });

        newLists.shell   = angular.copy($rootScope.componentsList.shell);
        newLists.imprint = angular.copy($rootScope.componentsList.imprint);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.componentsList.shell, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.shell.length - index
            });
        });

        angular.forEach($rootScope.componentsList.imprint, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.componentsList.imprint.length - index
            });
        });
      };

      function sendToBackInBadgeBack() {
        var oldLists = {};
        var newLists = {};

        oldLists   = angular.copy($rootScope.backSideComponents);

        angular.forEach($rootScope.backSideComponents, function(backItem, index) {
          if (backItem.id === $rootScope.currentActiveComponentIdentifier) {
            var itemToBeMovedUp = $rootScope.backSideComponents.splice(index, 1)[0];
            $rootScope.backSideComponents.push(itemToBeMovedUp);
          }
        });

        newLists   = angular.copy($rootScope.backSideComponents);
        badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);

        angular.forEach($rootScope.backSideComponents, function(component, index) {
            templateEventsDispatcher.dispatchEvent(availableEvents.INDEX_HAS_CHANGED, {
                componentId: component.id,
                zIndex: $rootScope.backSideComponents.length - index
            });
        });
      };

    }
})();
