(function(){
	'use strict';

    var directiveId = 'backgroundActionBarButton';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', backgroundActionBarButtonDirective]);

	function backgroundActionBarButtonDirective($rootScope, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/_directives/backgroundActionBarButton/backgroundActionBarButtonView.html',
			replace: true,
			link: link,
			scope: {
				icon: "@icon",
				name: "@name",
				event: "@event"
			}
		}

      function link(scope) {
      	scope.showContextualMenu 				= false;
				scope.badgeBackgroundColorRGB 	= {
					red		: 255,
					green	: 255,
					blue	: 255
				};

				scope.eventHandler = function ($event, contextualMenuState) {
					$event.stopPropagation();
					$event.preventDefault();
					if ($event.target.classList.contains('action-bar-button-with-menu', 'background')) {
						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS);
						scope.showContextualMenu = !contextualMenuState;
					}
				};

				scope.badgeBackgroundChanged = function(value) {
					var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(value);
			    scope.badgeBackgroundColorRGB = {
			        red 	: parseInt(result[1], 16),
			        green : parseInt(result[2], 16),
			        blue 	: parseInt(result[3], 16)
			    };
				};

				scope.updateHexColorFromRGB = function(value) {
					if ($rootScope.badgeRotationDegree === 0) {
						$rootScope.badgeBackgroundColor = "#" + ((1 << 24) + (value.red << 16) + (value.green << 8) + value.blue).toString(16).slice(1);
					} else if ($rootScope.badgeRotationDegree === 180){
						$rootScope.badgeBackSideBackgroundColor = "#" + ((1 << 24) + (value.red << 16) + (value.green << 8) + value.blue).toString(16).slice(1);
					}
				}

				$rootScope.$on(templateEventsDispatcher.availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS, function(event,data) {
      		scope.showContextualMenu = false;
      	});
	      }


	};

}());
