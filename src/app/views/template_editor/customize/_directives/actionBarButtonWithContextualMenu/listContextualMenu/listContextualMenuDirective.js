(function(){
	'use strict';

    var directiveId = 'listContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', listContextualMenuDirective]);

	function listContextualMenuDirective($rootScope, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/_directives/actionBarButtonWithContextualMenu/listContextualMenu/listContextualMenuView.html',
			replace: true,
			link: link,
			scope: {
				menuData: "@menuData"
			}
		}

        function link(scope) {
        	scope.menu = JSON.parse(scope.menuData)

					scope.$watch(function(){
						return $rootScope.currentActiveComponentIdentifier;
					}, function(currentComponent){
						scope.activeComponent = currentComponent != null ? true : false;
					})

        	scope.dispatchEvent = function($event, eventName, activeComponentId) {

	        		$event.stopPropagation();
	        		$event.preventDefault();

							var objectToSend = {
								componentId: $rootScope.currentActiveComponentIdentifier
							};

	        		templateEventsDispatcher.dispatchEvent(eventName, objectToSend);
        	}

        }
	};

}());
