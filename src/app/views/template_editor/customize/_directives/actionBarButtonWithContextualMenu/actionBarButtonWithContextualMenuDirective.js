(function(){
	'use strict';

    var directiveId = 'actionBarButtonWithContextualMenu';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', actionBarButtonWithContextualMenuDirectiveDirective]);

	function actionBarButtonWithContextualMenuDirectiveDirective($rootScope, templateEventsDispatcher) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/_directives/actionBarButtonWithContextualMenu/actionBarButtonWithContextualMenuView.html',
			replace: true,
			link: link,
			scope: {
				icon: "@icon",
				name: "@name",
				contextualMenuData: "@contextualMenuData"
			}
		}

        function link(scope) {

					scope.$watch(function(){
						return $rootScope.currentActiveComponentIdentifier;
					}, function(currentActiveComponentIdentifier){
						scope.buttonNotAllowed = currentActiveComponentIdentifier == null ? true : false;
						if(scope.name == 'Options') {
							scope.buttonNotAllowed = false;
						}
					});

        	scope.eventHandler = function($event, contextualMenuState) {
        		$event.stopPropagation();
        		$event.preventDefault();

        		templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS);

        		scope.showContextualMenu = !contextualMenuState;
        	};

        	$rootScope.$on(templateEventsDispatcher.availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS, function(event,data) {
        		scope.showContextualMenu = false;
        	});
        }
	};

}());
