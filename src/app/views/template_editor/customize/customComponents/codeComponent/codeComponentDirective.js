(function(){
	'use strict';

    var directiveId = 'codeComponent';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'toastr', 'templateEventsDispatcher', 'badgeHistoryService', '$timeout', 'base64', codeComponentDirective]);

	function codeComponentDirective($rootScope, toastr, templateEventsDispatcher, badgeHistoryService, $timeout, base64) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/customComponents/codeComponent/codeComponentView.html',
			replace: true,
			link: link,
			scope: {
				props: "="
			}
		}

        function link(scope, element) {
					var availableEvents = templateEventsDispatcher.availableEvents;

					scope.openContextualMenu = function($event) {
						scope.componentSelected = true;

						templateEventsDispatcher.dispatchEvent(availableEvents.DESELECT_CURRENT_ACTIVE_COMPONENT, {componentId: scope.props.id});

						templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.OPEN_CODE_MENU_FROM_BADGE_COMPONENT, scope.props);

						templateEventsDispatcher.dispatchEvent(availableEvents.MAKE_LAYER_COMPONENT_ACTIVE, {data: scope.props, status: scope.componentSelected});

							if ($event && !$($event.target).parent('div.draggable').hasClass('active')) {
									$($event.target).parent('div.draggable').addClass('active');
							}
					};

					scope.makeComponentInactive = function(props) {
						if(scope.componentSelected){
							scope.componentSelected = false;
						}
						var propsToSend 	= JSON.parse(JSON.stringify(props));
						propsToSend.UID 	= scope.$id;
						templateEventsDispatcher.dispatchEvent(availableEvents.MAKE_LAYER_COMPONENT_ACTIVE, {data: propsToSend, status: scope.componentSelected});
					};

					$rootScope.$on(availableEvents.ROTATE_90_DEGREES, function(event, data){
						if (data.componentId === scope.props.id && !data.fromHistory) {
								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "CODE";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ROTATE_90_DEGREES;
								action.propsOldValue        = {
										rotateAngle: scope.props.rotateAngle,
								};

								if(scope.props.rotateAngle == undefined){
									scope.props.rotateAngle = 1.5707963268; // 90deg
								} else {
									scope.props.rotateAngle += 1.5707963268; // 90deg
								}


								action.props                = {
										rotateAngle: scope.props.rotateAngle
								};

								badgeHistoryService.rememberAction(action);

						} else if (data.componentId === scope.props.id && data.fromHistory) {
							scope.props.rotateAngle = data.rotateAngle;
						}
					});

					$rootScope.$on(availableEvents.SNAP_TO_EDGES, function(event, data){
						// toastr.warning("QR Code Component can't be that larger!")
					});

					$rootScope.$on(availableEvents.DRAG_END, function(event, data) {
							if (data.componentId === scope.props.id && !data.fromHistory) {
									var action = badgeHistoryService.createEmptyActionObject();
									action.type             = "EDIT";
									action.componentType    = "CODE";
									action.componentId      = scope.props.id;
									action.triggeredEvent   = availableEvents.DRAG_END;
									action.propsOldValue    = {
											xAxis: scope.props.xAxis,
											yAxis: scope.props.yAxis
									};
									action.props            = {
											xAxis: parseFloat(data.target.getAttribute('data-x')),
											yAxis: parseFloat(data.target.getAttribute('data-y'))
									};

									badgeHistoryService.rememberAction(action);

									scope.props.xAxis           = parseFloat(data.target.getAttribute('data-x'));
									scope.props.yAxis           = parseFloat(data.target.getAttribute('data-y'));

									scope.$apply(function() {
											$timeout(function() {
													scope.componentSelected = true;
											}, 100)
									});
							} else if (data.componentId === scope.props.id && data.fromHistory) {
									scope.props.xAxis = data.xAxis;
									scope.props.yAxis = data.yAxis;
							}
					});

					$rootScope.$on(availableEvents.RESIZE_END, function(event, data) {
							if (data.componentId === scope.props.id && !data.fromHistory) {
									var action = badgeHistoryService.createEmptyActionObject();

									action.type                 = "EDIT";
									action.componentType        = "CODE";
									action.componentId          = scope.props.id;
									action.triggeredEvent       = availableEvents.RESIZE_END;
									action.propsOldValue        = {
											xAxis: scope.props.xAxis,
											yAxis: scope.props.yAxis,
											width: scope.props.width,
											height: scope.props.height
									};

									action.props                = {
											xAxis: parseInt(data.target.getAttribute('data-x')),
											yAxis: parseInt(data.target.getAttribute('data-y')),
											width: parseInt(data.target.style.width),
											height: parseInt(data.target.style.height)
									};

									badgeHistoryService.rememberAction(action);

									scope.props.width           = parseInt(data.target.style.width);
									scope.props.height          = parseInt(data.target.style.height);
									scope.props.xAxis           = parseFloat(data.target.getAttribute('data-x'));
									scope.props.yAxis           = parseFloat(data.target.getAttribute('data-y'));

									scope.$apply(function() {
											$timeout(function() {
													scope.componentSelected = true;
											}, 100)
									});
							} else if (data.componentId === scope.props.id && data.fromHistory) {
									scope.props.width           = data.width;
									scope.props.height          = data.height;
									scope.props.xAxis           = data.xAxis;
									scope.props.yAxis           = data.yAxis;
							}
					});

					$rootScope.$on(availableEvents.INDEX_HAS_CHANGED, function(event, data) {
							if (data.componentId === scope.props.id) {
									scope.props.zIndex = data.zIndex;
							}
					});

					$rootScope.$on(availableEvents.ALIGN_TO_LEFT, function(event, data) {
						if(data.componentId === scope.props.id && !data.fromHistory ){

								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "CODE";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ALIGN_TO_LEFT;
								action.propsOldValue        = {
										xAxis: scope.props.xAxis
								};

								scope.props.xAxis = 0;

								action.props                = {
										xAxis: (scope.props.xAxis)
								};

								badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
					});

					$rootScope.$on(availableEvents.ALIGN_TO_CENTER, function(event, data) {
						if(data.componentId === scope.props.id && !data.fromHistory ){

								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "CODE";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ALIGN_TO_CENTER;
								action.propsOldValue        = {
										xAxis: scope.props.xAxis
								};

								var offSetOfSafeZone = document.getElementById('template-safe-zone').offsetWidth;
								var offSetOfComponent = element.context.offsetWidth;

								scope.props.xAxis = (offSetOfSafeZone / 2) - (offSetOfComponent / 2) - 2; // -2 for borders

								action.props                = {
										xAxis: scope.props.xAxis
								};

								badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
					});

					$rootScope.$on(availableEvents.ALIGN_TO_RIGHT, function(event, data) {
						if(data.componentId === scope.props.id && !data.fromHistory ){

								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "CODE";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ALIGN_TO_RIGHT;
								action.propsOldValue        = {
										xAxis: scope.props.xAxis
								};

								var offSetOfSafeZone = document.getElementById('template-safe-zone').offsetWidth;
								var offSetOfComponent = element.context.offsetWidth;

								scope.props.xAxis = offSetOfSafeZone - offSetOfComponent;

								action.props                = {
										xAxis: scope.props.xAxis
								};

								badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
					});

					$rootScope.$on(availableEvents.ADD_CODE_COMPONENT, function(event, data) {
							if (data.fromHistory && data.type === "UNDO") {
									scope.$destroy();
									$('#template-safe-zone').find('div[data-id=' + data.componentId + ']').remove();

									angular.forEach($rootScope.componentsList.imprint, function(item, index) {
											if (item.id === data.componentId) {
													$rootScope.componentsList.imprint.splice(index, 1);
											}
									});
							};
					});

					$rootScope.$on(availableEvents.DELETE_CODE_COMPONENT, function(event, data) {
						if (data.componentId === scope.props.id) {
							scope.$destroy();
							var selector = 'div[data-id=' + scope.props.id + ']';
							$(selector).remove();
						}
					});

					$rootScope.$on(availableEvents.MAKE_COMPONENT_ACTIVE, function(event, data) {
						if (data === scope.props.id) {
							scope.componentSelected = !scope.componentSelected;
							var propsToSend 	= scope.props;
							propsToSend.UID 	= scope.$id;
							templateEventsDispatcher.dispatchEvent(availableEvents.OPEN_CODE_MENU_FROM_BADGE_COMPONENT, propsToSend);
						} else {
							scope.componentSelected = false;
						}
					});

					$rootScope.$on(availableEvents.MOVE_UP, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "CODE";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_UP;
							action.propsOldValue        = {
									yAxis: scope.props.yAxis,
							};

							if(scope.props.yAxis == undefined){
								scope.props.yAxis = 0;
							}

							scope.props.yAxis -= 1;

							action.props                = {
									yAxis: scope.props.yAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {

							scope.props.yAxis = data.yAxis;

						}
					});

					$rootScope.$on(availableEvents.MOVE_DOWN, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "CODE";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_DOWN;
							action.propsOldValue        = {
									yAxis: scope.props.yAxis,
							};

							if(scope.props.yAxis == undefined){
								scope.props.yAxis = 0;
							}

							scope.props.yAxis += 1;

							action.props                = {
									yAxis: scope.props.yAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {

							scope.props.yAxis = data.yAxis;

						}
					});

					$rootScope.$on(availableEvents.MOVE_RIGHT, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "CODE";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_RIGHT;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis,
							};

							if(scope.props.xAxis == undefined){
								scope.props.xAxis 				= 0;
							}

							scope.props.xAxis 					+= 1;

							action.props                = {
									xAxis: scope.props.xAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {
							scope.props.xAxis = data.xAxis;
						}
					});

					$rootScope.$on(availableEvents.MOVE_LEFT, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "CODE";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_LEFT;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis,
							};

							if(scope.props.xAxis == undefined){
								scope.props.xAxis = 0;
							}

							scope.props.xAxis -= 1;

							action.props                = {
									xAxis: scope.props.xAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {
							scope.props.xAxis = data.xAxis;
						}
					});


					$timeout(function() {
							var params = {
								angle: scope.props.rotateAngle,
								start: function(event, ui) {
								},
								rotate: function(event, ui) {
								},
								stop: function(event, ui) {
										var action              = badgeHistoryService.createEmptyActionObject();
										action.type             = "EDIT";
										action.componentType    = "CODE";
										action.componentId      = scope.props.id;
										action.triggeredEvent   = availableEvents.UPDATE_ROTATION_ANGLE;
										action.props            = {
												rotateAngle: ui.angle.current
										};
										action.propsOldValue    = {
												rotateAngle: scope.props.rotateAngle
										};

										scope.props.rotateAngle = ui.angle.current;
										$($(event.target).children('.anchor-points-wrapper')[0]).children().removeClass('active');
										scope.componentSelected = true;
										$(".draggable.code-component[data-id=" + scope.props.id +  "] .rotation-point").removeClass('active');

										badgeHistoryService.rememberAction(action);
								}
							};

							var selector = ".draggable.code-component[data-id=" + scope.props.id + "]";

							$(selector).rotatable(params);

							$(".draggable.code-component[data-id=" + scope.props.id + "] .rotation-point").bind("mousedown", function(e) {
									e.preventDefault();
									e.stopPropagation();
									$(e.target).addClass('active');
									$(selector).rotatable("instance").startRotate(e);
							});



							if (scope.props.componentSelected === true) {
								scope.openContextualMenu();
							}
					}, 0);
				}
	};

}());
