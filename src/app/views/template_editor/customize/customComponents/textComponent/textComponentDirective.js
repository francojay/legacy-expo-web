
(function(){
	'use strict';

    var directiveId = 'textComponent';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', 'templateEventsDispatcher', '$window', 'badgeHistoryService', '$timeout', textComponentDirective]);

	function textComponentDirective($rootScope, templateEventsDispatcher, $window, badgeHistoryService, $timeout) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/customize/customComponents/textComponent/textComponentView.html',
			replace: true,
			link: link,
			scope: {
				props   : "=",
			}
		}

        function link(scope, element) {
        	var availableEvents = templateEventsDispatcher.availableEvents;
					scope.componentSelected = scope.props.componentSelected || false;
					$timeout(function() {
						var selector = ".draggable.text-component[data-id=" + scope.props.id + "]";
						if (!scope.props.width) {
							scope.props.width = $(selector)[0].clientWidth
						};
					}, 0)
					var mailmergePossibleValues = {
						"first_name"			: "First Name",
						"last_name" 			: "Last Name",
						"company_name" 		: "Company Name",
						"email_address" 	: "Email Address",
						"phone_number" 		: "Phone Number",
						"street_address" 	: "Street Address",
						"city" 						: "City",
						"state"						: "State"
					};

					scope.openContextualMenu = function(props, $event) {
            scope.componentSelected = true;

        		var propsToSend 	= JSON.parse(JSON.stringify(props));
        		propsToSend.UID 	= scope.$id;

						if ($rootScope.badgeRotationDegree === 0) {
							templateEventsDispatcher.dispatchEvent(availableEvents.DESELECT_CURRENT_ACTIVE_COMPONENT, {componentId: scope.props.id});
						} else if ($rootScope.badgeRotationDegree === 180) {
							templateEventsDispatcher.dispatchEvent(availableEvents.DESELECT_CURRENT_ACTIVE_BACK_COMPONENT, {componentId: scope.props.id});
						}

        		templateEventsDispatcher.dispatchEvent(availableEvents.OPEN_TEXT_MENU_FROM_BADGE_COMPONENT, propsToSend);

						templateEventsDispatcher.dispatchEvent(availableEvents.MAKE_LAYER_COMPONENT_ACTIVE, {data: propsToSend, status: scope.componentSelected});
      		};

					scope.makeComponentInactive = function(props){
						scope.componentSelected = false;
						scope.props.componentSelected = false;

						var propsToSend 	= JSON.parse(JSON.stringify(props));
        		propsToSend.UID 	= scope.$id;

						templateEventsDispatcher.dispatchEvent(availableEvents.MAKE_LAYER_COMPONENT_ACTIVE, {data: propsToSend, status: scope.componentSelected});
					};

					scope.makeComponentMovable = function() {
						scope.inMoveMode = true;
					}

					scope.makeComponentUnmovable = function() {
						scope.inMoveMode = false;
					}

					$rootScope.$on(availableEvents.CHANGE_FOLDER_FOR_COMPONENT, function(event, data){
							if(scope.props.id === data.props.id){
								scope.props.partOf = data.props.partOf;
							}
					});

          $timeout(function() {
              var params = {
								angle: scope.props.rotateAngle,
                start: function(event, ui) {
                },
                rotate: function(event, ui) {
                },
                stop: function(event, ui) {
                  var action              = badgeHistoryService.createEmptyActionObject();
                  action.type             = "EDIT";
                  action.componentType    = "TEXT";
                  action.componentId      = scope.props.id;
                  action.triggeredEvent   = availableEvents.UPDATE_ROTATION_ANGLE;
                  action.props            = {
                      rotateAngle: ui.angle.current
                  };
                  action.propsOldValue    = {
                      rotateAngle: scope.props.rotateAngle
                  };

                  scope.props.rotateAngle = ui.angle.current;
                  $($(event.target).children('.anchor-points-wrapper')[0]).children().removeClass('active');
                  badgeHistoryService.rememberAction(action);

									$(".draggable.text-component[data-id=" + scope.props.id +  "] .rotation-point").removeClass('active');
                }
              };

              var selector = ".draggable.text-component[data-id=" + scope.props.id + "]";
              $(selector).rotatable(params);

              $(".draggable.text-component[data-id=" + scope.props.id +  "] .rotation-point").bind("mousedown", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(e.target).addClass('active');
                $(selector).rotatable("instance").startRotate(e);
              });
							initDroppableCapabilities();
							if (scope.props.componentSelected === true) {
								scope.openContextualMenu(scope.props);
							}

          }, 0);

					$rootScope.$on(availableEvents.ROTATE_90_DEGREES, function(event, data){
						if (data.componentId === scope.props.id && !data.fromHistory) {
								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "TEXT";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ROTATE_90_DEGREES;
								action.propsOldValue        = {
										rotateAngle: scope.props.rotateAngle,
								};

								if(scope.props.rotateAngle == undefined){
									scope.props.rotateAngle = 1.5707963268; // 90deg
								} else {
									scope.props.rotateAngle += 1.5707963268; // 90deg
								}


								action.props                = {
										rotateAngle: scope.props.rotateAngle
								};

								badgeHistoryService.rememberAction(action);

						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.rotateAngle = data.rotateAngle;
						}
					});

					$rootScope.$on(availableEvents.SNAP_TO_EDGES, function(event, data){
						if (data.componentId === scope.props.id && !data.fromHistory) {
								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "TEXT";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.SNAP_TO_EDGES;
								action.propsOldValue        = {
										width: scope.props.width,
										xAxis: scope.props.xAxis
								};

								var offSetOfSafeZone 	= document.getElementById('template-safe-zone').offsetWidth;
								scope.props.width 		= parseInt(offSetOfSafeZone - 4);

								scope.props.xAxis 		= 0;


								action.props                = {
										width: scope.props.width,
										xAxis: scope.props.xAxis
								};

								badgeHistoryService.rememberAction(action);

						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.width           = data.width;
								scope.props.xAxis						= data.xAxis;
						}
					});

        	$rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__FONT_STYLE, function(event, data) {
            if(!data.fromHistory) {
                var action = badgeHistoryService.createEmptyActionObject();
                action.type = 'EDIT';
                action.componentType = 'TEXT';
                action.componentId = scope.props.id;
                action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__FONT_STYLE;
                action.props = {
                   fontStyle: data.fontStyle
                };
                action.propsOldValue = {
                    fontStyle: scope.props.fontStyle
                }
                action.affectedElements = scope.lastInsertedSpans;
                badgeHistoryService.rememberAction(action);
            } else {
                scope.lastInsertedSpans = data.affectedElements;
            }

            if (data.componentId === scope.props.id) {
    					scope.props.fontStyle = data.fontStyle;
    				}
        	});

        	$rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__TEXT_DECORATION, function(event, data) {
                if(!data.fromHistory) {
                    var action = badgeHistoryService.createEmptyActionObject();
                    action.type = 'EDIT';
                    action.componentType = 'TEXT';
                    action.componentId = scope.props.id;
                    action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__TEXT_DECORATION;
                    action.props = {
                       textDecoration: data.textDecoration
                    };
                    action.propsOldValue = {
                        textDecoration: scope.props.textDecoration
                    }
                    action.affectedElements = scope.lastInsertedSpans;
                    badgeHistoryService.rememberAction(action);
                } else {
                    scope.lastInsertedSpans = data.affectedElements;

                }
        		if (data.componentId === scope.props.id) {
        			scope.props.textDecoration = data.textDecoration;
        		} else if (data.componentId === scope.props.id && scope.lastInsertedSpans.length > 0) {
                    angular.forEach(scope.lastInsertedSpans, function(span) {
                        span.style.textDecoration = data.textDecoration
                    })

                }
        	});

        	$rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__TEXT_ALIGN, function(event, data) {
                if(!data.fromHistory) {
                    var action = badgeHistoryService.createEmptyActionObject();
                    action.type = 'EDIT';
                    action.componentType = 'TEXT';
                    action.componentId = scope.props.id;
                    action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__TEXT_ALIGN;
                    action.props = {
                       textAlign: data.textAlign
                    };
                    action.propsOldValue = {
                        textAlign: scope.props.textAlign
                    }
                    action.affectedElements = scope.lastInsertedSpans;
                    badgeHistoryService.rememberAction(action);
                } else {
                    scope.lastInsertedSpans = data.affectedElements;
                }
        		if (data.componentId === scope.props.id && !scope.lastInsertedSpan) {
        			scope.props.textAlign = data.textAlign;
        		}
        	});

        	$rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__OPACITY, function(event, data) {
                if(!data.fromHistory) {
                    var action = badgeHistoryService.createEmptyActionObject();
                    action.type = 'EDIT';
                    action.componentType = 'TEXT';
                    action.componentId = scope.props.id;
                    action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__OPACITY;
                    action.props = {
                       opacity: data.opacity
                    };
                    action.propsOldValue = {
                        opacity: scope.props.opacity
                    }
                    action.affectedElements = scope.lastInsertedSpans;
                    badgeHistoryService.rememberAction(action);
                } else {
                    scope.lastInsertedSpans = data.affectedElements;
                }

        		if (data.componentId === scope.props.id) {
        			scope.props.opacity = data.opacity;
        		} else if (data.componentId === scope.props.id && scope.lastInsertedSpans) {
                    angular.forEach(scope.lastInsertedSpans, function(span) {
                        span.style.opacity = data.opacity;
                    })
                }
        	});

        	$rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__FONT_SIZE, function(event, data) {
              if(!data.fromHistory) {
                  var action = badgeHistoryService.createEmptyActionObject();
                  action.type = 'EDIT';
                  action.componentType = 'TEXT';
                  action.componentId = scope.props.id;
                  action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__FONT_SIZE;
                  action.props = {
                     fontSize: data.fontSize
                  };
                  action.propsOldValue = {
                      fontSize: scope.props.fontSize
                  }
                  action.affectedElements = scope.lastInsertedSpans;
                  badgeHistoryService.rememberAction(action);
              } else {
                  scope.lastInsertedSpans = data.affectedElements;
              }

	      		if (data.componentId === scope.props.id) {
	      			scope.props.fontSize = data.fontSize;
	      		} else if (data.componentId === scope.props.id && scope.lastInsertedSpans) {
              angular.forEach(scope.lastInsertedSpans, function(span) {
                  span.style.fontSize = data.fontSize + 'px';
              });
            }
	      	});

          $rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__FONT_FAMILY, function(event, data) {
              if(!data.fromHistory) {
                  var action = badgeHistoryService.createEmptyActionObject();
                  action.type = 'EDIT';
                  action.componentType = 'TEXT';
                  action.componentId = scope.props.id;
                  action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__FONT_FAMILY;
                  action.props = {
                     fontFamily: data.fontFamily
                  };
                  action.propsOldValue = {
                      fontFamily: scope.props.fontFamily
                  };
                  action.affectedElements = scope.lastInsertedSpans;
                  badgeHistoryService.rememberAction(action);
              } else {
                  scope.lastInsertedSpans = data.affectedElements;
              }

              if (data.componentId === scope.props.id) {
                  scope.props.fontFamily = data.fontFamily;
              }
          });

          $rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__FONT_WEIGHT, function(event, data) {
              if(!data.fromHistory) {
                  var action = badgeHistoryService.createEmptyActionObject();
                  action.type = 'EDIT';
                  action.componentType = 'TEXT';
                  action.componentId = scope.props.id;
                  action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__FONT_WEIGHT;
                  action.props = {
                     fontWeight: data.fontWeight
                  };
                  action.propsOldValue = {
                      fontWeight: scope.props.fontWeight
                  }
                  badgeHistoryService.rememberAction(action);
              }

              if (data.componentId === scope.props.id) {
                  scope.props.fontWeight = data.fontWeight;
              }
          });

          $rootScope.$on(availableEvents.UPDATE_TEXT_PROPS__COLOR, function(event, data) {
						if(!data.fromHistory) {
								var action = badgeHistoryService.createEmptyActionObject();
								action.type = 'EDIT';
								action.componentType = 'TEXT';
								action.componentId = scope.props.id;
								action.triggeredEvent = availableEvents.UPDATE_TEXT_PROPS__COLOR;
								action.props = {
									 color: data.color
								};
								action.propsOldValue = {
										color: scope.props.color
								}
								badgeHistoryService.rememberAction(action);
						}

            if (data.componentId === scope.props.id) {
                scope.props.color = data.color;
            }
          });

        $rootScope.$on(availableEvents.DRAG_END, function(event, data) {
            if (data.componentId === scope.props.id && !data.fromHistory) {
                var action = badgeHistoryService.createEmptyActionObject();
                action.type             = "EDIT";
                action.componentType    = "TEXT";
                action.componentId      = scope.props.id;
                action.triggeredEvent   = availableEvents.DRAG_END;
                action.propsOldValue    = {
                    xAxis: scope.props.xAxis,
                    yAxis: scope.props.yAxis
                };
                action.props            = {
                    xAxis: parseFloat(data.target.getAttribute('data-x')),
                    yAxis: parseFloat(data.target.getAttribute('data-y'))
                };

                badgeHistoryService.rememberAction(action);

                scope.props.xAxis           = parseFloat(data.target.getAttribute('data-x'));
                scope.props.yAxis           = parseFloat(data.target.getAttribute('data-y'));

                scope.$apply(function() {
                    $timeout(function() {
                        scope.componentSelected = true;
                    }, 100)
                });
            } else if (data.componentId === scope.props.id && data.fromHistory) {
                scope.props.xAxis       = data.xAxis;
                scope.props.yAxis       = data.yAxis;
            }
        });

          $rootScope.$on(availableEvents.RESIZE_END, function(event, data) {
              if (data.componentId === scope.props.id && !data.fromHistory) {
                  var action = badgeHistoryService.createEmptyActionObject();

                  action.type                 = "EDIT";
                  action.componentType        = "TEXT";
                  action.componentId          = scope.props.id;
                  action.triggeredEvent       = availableEvents.RESIZE_END;
                  action.propsOldValue        = {
                      xAxis: scope.props.xAxis,
                      yAxis: scope.props.yAxis,
                      width: scope.props.width,
                      height: scope.props.height
                  };

                  action.props                = {
                      xAxis: parseInt(data.target.getAttribute('data-x')),
                      yAxis: parseInt(data.target.getAttribute('data-y')),
                      width: parseInt(data.target.style.width),
                      height: parseInt(data.target.style.height)
                  };

                  badgeHistoryService.rememberAction(action);

                  scope.props.width           = parseInt(data.target.style.width);
                  scope.props.height          = parseInt(data.target.style.height);
                  scope.props.xAxis           = parseFloat(data.target.getAttribute('data-x'));
                  scope.props.yAxis           = parseFloat(data.target.getAttribute('data-y'));

                  scope.$apply(function() {
                      $timeout(function() {
                          scope.componentSelected = true;
                      }, 100)
                  });
              } else if (data.componentId === scope.props.id && data.fromHistory) {
                  scope.props.xAxis           = data.xAxis;
                  scope.props.yAxis           = data.yAxis;
                  scope.props.width           = data.width;
                  scope.props.height          = data.height;
              }
          });

          $rootScope.$on(availableEvents.INDEX_HAS_CHANGED, function(event, data) {
              if (data.componentId === scope.props.id) {
                  scope.props.zIndex = data.zIndex
              }
          });

          $rootScope.$on(availableEvents.ALIGN_TO_LEFT, function(event, data) {
						if (data.componentId === scope.props.id && (data === undefined || !data.fromHistory) ) {
                var action = badgeHistoryService.createEmptyActionObject();

                action.type                 = "EDIT";
                action.componentType        = "TEXT";
                action.componentId          = scope.props.id;
                action.triggeredEvent       = availableEvents.ALIGN_TO_LEFT;
                action.propsOldValue        = {
                    xAxis: scope.props.xAxis
                };

								scope.props.xAxis 					= 0;

                action.props                = {
                    xAxis: (scope.props.xAxis)
                };

                badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
          });

          $rootScope.$on(availableEvents.ALIGN_TO_CENTER, function(event, data) {
						if (data.componentId === scope.props.id && (data === undefined || !data.fromHistory) ) {

							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.ALIGN_TO_CENTER;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis
							};

							var offSetOfSafeZone = document.getElementById('template-safe-zone').offsetWidth;
							var offSetOfComponent = element.context.offsetWidth;

							scope.props.xAxis = (offSetOfSafeZone / 2) - (offSetOfComponent / 2) - 2; // -2 for borders

							action.props                = {
									xAxis: scope.props.xAxis
							};

							badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
          });

          $rootScope.$on(availableEvents.ALIGN_TO_RIGHT, function(event, data) {
						if(data.componentId === scope.props.id && !data.fromHistory) {

							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.ALIGN_TO_RIGHT;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis
							};

							var offSetOfSafeZone = document.getElementById('template-safe-zone').offsetWidth;
							var offSetOfComponent = element.context.offsetWidth;

							scope.props.xAxis = offSetOfSafeZone - offSetOfComponent;

							action.props                = {
									xAxis: scope.props.xAxis
							};

							badgeHistoryService.rememberAction(action);
						} else if (data.componentId === scope.props.id && data.fromHistory) {
								scope.props.xAxis = data.xAxis;
						}
          });

          $rootScope.$on(availableEvents.ADD_TEXT_COMPONENT, function(event, data) {
              if (data.fromHistory && data.type === "UNDO") {
                  scope.$destroy();
                  $('#template-safe-zone').find('div[data-id=' + data.componentId + ']').remove();

                  angular.forEach($rootScope.componentsList.shell, function(item, index) {
                      if (item.id === data.componentId) {
                          $rootScope.componentsList.shell.splice(index, 1);
                      }
                  });
                  angular.forEach($rootScope.componentsList.imprint, function(item, index) {
                      if (item.id === data.componentId) {
                          $rootScope.componentsList.imprint.splice(index, 1);
                      }
                  });
              }
          });

					$rootScope.$on(availableEvents.ADD_BACK_TEXT_COMPONENT, function(event, data) {
            if (data.fromHistory && data.type === "UNDO") {
                scope.$destroy();
                $('#back-template-safe-zone').find('div[data-id=' + data.componentId + ']').remove();

                angular.forEach($rootScope.backSideComponents, function(item, index) {
                    if (item.id === data.componentId) {
                      $rootScope.backSideComponents.splice(index, 1);
                    }
                });
            }
          });

					$rootScope.$on(availableEvents.ADD_MAIL_MERGE_FIELD, function(event, data) {
						if (data.componentId === scope.props.id) {
							var selector = '#text-' + scope.props.id;
							if (data.mergeFields) {
								document.getElementById(selector).setAttribute('data', "merge-fields: " + data.mergeFields);
								var mailMergeFieldsArray = data.mergeFields.split(',');
								var innerText = "";

								angular.forEach(mailMergeFieldsArray, function(value) {
									innerText = innerText + " " + mailmergePossibleValues[value];
								});

								$(selector).text(innerText);
							} else {
								$(selector).data('merge-fields', "");
								$(selector).text("Drag and Drop a Mail Merge Field");
							}
						}
					});

					$rootScope.$on(availableEvents.DELETE_TEXT_COMPONENT, function(event, data) {
						if (data.componentId === scope.props.id) {
							scope.$destroy();
							var selector = '#text-' + scope.props.id;
							$(selector).parent('div').remove();
						}
					});

					$rootScope.$on(availableEvents.MAKE_COMPONENT_ACTIVE, function(event, data) {
						if (data === scope.props.id) {
							scope.componentSelected = !scope.componentSelected;
							var propsToSend 	= scope.props;
	        		propsToSend.UID 	= scope.$id;

	        		templateEventsDispatcher.dispatchEvent(availableEvents.OPEN_TEXT_MENU_FROM_BADGE_COMPONENT, propsToSend);
						} else {
							scope.componentSelected = false;
						}
					});

					$rootScope.$on(availableEvents.MOVE_UP, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory && scope.inMoveMode) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_UP;
							action.propsOldValue        = {
									yAxis: scope.props.yAxis,
							};

							if(scope.props.yAxis == undefined){
								scope.props.yAxis = 0;
							}

							scope.props.yAxis -= 1;

							action.props                = {
									yAxis: scope.props.yAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {

							scope.props.yAxis = data.yAxis;

						}
					});

					$rootScope.$on(availableEvents.MOVE_DOWN, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory && scope.inMoveMode) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_DOWN;
							action.propsOldValue        = {
									yAxis: scope.props.yAxis,
							};

							if(scope.props.yAxis == undefined){
								scope.props.yAxis = 0;
							}

							scope.props.yAxis += 1;

							action.props                = {
									yAxis: scope.props.yAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {

							scope.props.yAxis = data.yAxis;

						}
					});

					$rootScope.$on(availableEvents.MOVE_RIGHT, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory && scope.inMoveMode) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_RIGHT;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis,
							};

							if(scope.props.xAxis == undefined){
								scope.props.xAxis 				= 0;
							}

							scope.props.xAxis 					+= 1;

							action.props                = {
									xAxis: scope.props.xAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {
							scope.props.xAxis = data.xAxis;
						}
					});

					$rootScope.$on(availableEvents.MOVE_LEFT, function(event, data){
						if(data.componentId == scope.props.id && !data.fromHistory && scope.inMoveMode) {
							var action = badgeHistoryService.createEmptyActionObject();

							action.type                 = "EDIT";
							action.componentType        = "TEXT";
							action.componentId          = scope.props.id;
							action.triggeredEvent       = availableEvents.MOVE_LEFT;
							action.propsOldValue        = {
									xAxis: scope.props.xAxis,
							};

							if(scope.props.xAxis == undefined){
								scope.props.xAxis = 0;
							}

							scope.props.xAxis -= 1;

							action.props                = {
									xAxis: scope.props.xAxis
							};

							if (data.saveEvent) {
								badgeHistoryService.rememberAction(action);
							}

							scope.$apply();

						} else if (data.componentId === scope.props.id && data.fromHistory) {
							scope.props.xAxis = data.xAxis;
						}
					});

					function initDroppableCapabilities() {
						var selector = '#text-' + scope.props.id;
						interact(selector).dropzone({
						  accept: '.drag-drop',
						  overlap: 0.2,
						  ondropactivate: function (event) {
						    event.target.classList.add('drop-active');
						  },
						  ondragenter: function (event) {
						    var draggableElement 	= event.relatedTarget;
								var dropzoneElement 	= event.target;
						    $(dropzoneElement).parent('.text-component').addClass('drop-target');
						  },
						  ondragleave: function (event) {
								$(event.target).parent('.text-component').removeClass('drop-target');
						  },
						  ondrop: function (event) {
								var mergeFieldContent 				= $(event.relatedTarget).html();
								var mailMergeDragData 				= $(event.relatedTarget).data('drag-value');
								var currentTextFieldMergeData = event.target.getAttribute('data-merge-fields');
								if (!currentTextFieldMergeData) {
									var updatedTextFieldMergeData = mailMergeDragData;
								} else {
									var updatedTextFieldMergeData = currentTextFieldMergeData + "," + mailMergeDragData;
								}

								scope.props.mailMergeField = updatedTextFieldMergeData;

								var mailMergeFieldsArray = updatedTextFieldMergeData.split(',');

								var innerText = "";

								angular.forEach(mailMergeFieldsArray, function(value) {
									innerText = innerText + " " + mailmergePossibleValues[value];
								});

								event.target.innerText = innerText;
								scope.props.innerText = innerText;
								scope.props.partOf = 'imprint';

								$(event.relatedTarget).remove();

								if ($rootScope.currentFolder === 'shell') {
									angular.forEach($rootScope.componentsList.shell, function(item, index) {
                      if (item.id === scope.props.id) {
												var copy = angular.copy($rootScope.componentsList.shell[index]);
												copy.props.partOf = "imprint";
												copy.props.innerText = innerText;

												$rootScope.componentsList.shell.splice(index, 1);
												$rootScope.componentsList.imprint.push(copy);

												templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_OPEN_FOLDER, {folder: 'imprint'});
                      }
                  });
								};

								var action = badgeHistoryService.createEmptyActionObject();

								action.type                 = "EDIT";
								action.componentType        = "TEXT";
								action.componentId          = scope.props.id;
								action.triggeredEvent       = availableEvents.ADD_MAIL_MERGE_FIELD;
								action.propsOldValue        = {
									mergeFields	: currentTextFieldMergeData
								};

								action.props                = {
									mergeFields : updatedTextFieldMergeData
								};

								badgeHistoryService.rememberAction(action);
						  },
						  ondropdeactivate: function (event) {
								if(!event.dragEvent.dropzone){
									$(event.relatedTarget).remove();
								}
						    event.target.classList.remove('drop-active');
						    event.target.classList.remove('drop-target');
								$(event.target).parent('.text-component').removeClass('drop-target');
						  }
						});
					}
      }
	};

}());
