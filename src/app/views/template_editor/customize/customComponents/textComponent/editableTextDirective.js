
(function(){
	'use strict';

    var directiveId = 'editableText';

	angular
		.module('app')
		.directive(directiveId, editableTextDirective);

	function editableTextDirective() {
		return {
			restrict: 'A',
			replace: true,
      require: 'ngModel',
			link: link
		}

        function link(scope, element, attrs, ngModel) {
          function read() {
            ngModel.$setViewValue(element.html());
          }

          ngModel.$render = function() {
            element.html(ngModel.$viewValue || "");
          };

          element.bind("blur keyup change", function() {
            scope.$apply(read);
          });
        }

	};

}());
