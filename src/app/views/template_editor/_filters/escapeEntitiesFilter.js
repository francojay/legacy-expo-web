(function(){
  'use strict';

  var module = angular.module('app');

  module.filter('escape_caracters', function(){
    return function(text){
      text = _.unescape(text);
      return text;
    }
})
})();
