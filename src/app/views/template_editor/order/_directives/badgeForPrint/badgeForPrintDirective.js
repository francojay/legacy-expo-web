(function(){
  'use strict'
  var directiveId = 'badgeForPrint'

  angular
    .module('app')
		.directive(directiveId, badgeForPrintDirective);

	function badgeForPrintDirective() {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/order/_directives/badgeForPrint/badgeForPrintView.html',
			replace: true
		}
  }
})();
