(function () {
    'use strict'
    var controllerId = 'orderDetailsController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', orderDetailsController]);
    function orderDetailsController($rootScope, $scope) {
      $rootScope.showBadge = false;
      $scope.badgeOrderFormFields = [
        {
            code: 'first_name',
            label: 'First Name'
        },
        {
            code: 'last_name',
            label: 'Last Name'
        },
        {
            code: 'company_name',
            label: 'Company Name'
        },
        {
            code: 'country',
            label: 'Country'
        },
        {
            code: 'street_address',
            label: 'Address'
        },
        {
            code: 'city',
            label: 'City'
        },
        {
            code: 'state',
            label: 'State'
        },
        {
            code: 'zip_code',
            label: 'Zip Code'
        },
        {
            code: 'phone_number',
            label: 'Phone Number'
        },
        {
            code: 'email_address',
            label: 'Email Address'
        }
      ];
    }
})();
