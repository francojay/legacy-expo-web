(function () {
    var controllerId = 'badgeBackEditorController';
    var module = angular.module('app');

    module.controller(controllerId, ['$scope', '$rootScope', 'templateEventsDispatcher', '$state', '$stateParams', '$compile', 'badgeHistoryService', badgeBackEditorController]);

    function badgeBackEditorController($scope, $rootScope, templateEventsDispatcher, $state, $stateParams, $compile, badgeHistoryService) {
      initInteract();

      $rootScope.backSideComponents   = [];
      var availableEvents = templateEventsDispatcher.availableEvents;

      $rootScope.$on(availableEvents.ADD_BACK_TEXT_COMPONENT, function(event, data) {
        if (data.type === "UNDO") {

        } else {
          var data = {
              "fontSize"          : data.fontSize,
              "fontStyle"         : data.fontStyle,
              "fontFamily"        : data.fontFamily,
              "textAlign"         : data.textAlign,
              "textDecoration"    : data.textDecoration,
              "opacity"           : data.opacity,
              "fontWeight"        : data.fontWeight,
              "color"             : data.color,
              "height"            : "auto",
              "innerText"         : _.unescape("Drag & Drop Mail Merge Field"),
              "id"                : calculateNewComponentId($rootScope.backSideComponents),
              "componentSelected" : true
          };

          var componentSpecs = {
              type  : 'TEXT',
              props : data,
              id    : data.id
          };

          $rootScope.backSideComponents.unshift(componentSpecs);


          var action = {
            type            : "ADD",
            componentType   : "TEXT",
            componentId     : componentSpecs.id,
            triggeredEvent  : availableEvents.ADD_BACK_TEXT_COMPONENT,
            props           : componentSpecs.props,
            propsOldValue   : componentSpecs.props
          };

          badgeHistoryService.rememberActionForShellBackSide(action);
        }
      });

      $rootScope.$on(availableEvents.DESELECT_CURRENT_ACTIVE_BACK_COMPONENT, function(event, data){
        if ($rootScope.currentActiveComponentIdentifier && $rootScope.currentActiveComponentIdentifier !== data.componentId) {
          var currentActiveElement =  $("#back-template-safe-zone").find("[data-id=" + $rootScope.currentActiveComponentIdentifier + "]");
          var currentActiveScope   = angular.element(currentActiveElement).scope();
          currentActiveScope.$$childHead.makeComponentInactive(currentActiveScope.$$childHead.props);
        }
      });

      $rootScope.$on(availableEvents.ADD_BACK_IMAGE_COMPONENT, function(event, data) {
        if (data.type !== "UNDO") {
          data.id = calculateNewComponentId($rootScope.backSideComponents);
          data.componentSelected = true;

          var componentSpecs = {
            type  : "IMAGE",
            props : data,
            id    : data.id
          };

          $rootScope.backSideComponents.unshift(componentSpecs);

          var action = {
            type            : "ADD",
            componentType   : "IMAGE",
            componentId     : componentSpecs.props.id,
            triggeredEvent  : availableEvents.ADD_BACK_IMAGE_COMPONENT,
            props           : componentSpecs.props,
            propsOldValue   : componentSpecs.props
          };

          badgeHistoryService.rememberActionForShellBackSide(action);
        }
      });

      $rootScope.$on(availableEvents.ADD_BACK_SHAPE_COMPONENT, function(event, data) {
        if (data.type !== "UNDO") {
          data.id = calculateNewComponentId($rootScope.backSideComponents);
          data.componentSelected = true;

          var componentSpecs = {
            type  : "SHAPE",
            props : data,
            id    : data.id
          };

          $rootScope.backSideComponents.unshift(componentSpecs);

          var action = {
            type            : "ADD",
            componentType   : "SHAPE",
            componentId     : componentSpecs.props.id,
            triggeredEvent  : availableEvents.ADD_BACK_SHAPE_COMPONENT,
            props           : componentSpecs.props,
            propsOldValue   : componentSpecs.props
          };

          badgeHistoryService.rememberActionForShellBackSide(action);
        }
      });

      function initInteract() {
          var badge = document.querySelector('.badge-back-container');

          interact('div.draggable', {
              context: badge
          })
          .draggable({
              intertia: true,
              onmove: dragMoveListener,
          })
          .resizable({
              edges: { left: true, right: true, bottom: true, top: true },
              invert: 'none',
              margin: 5,
          })
          .actionChecker(function (pointer, event, action, interactable) {
              interactable.options.resize.preserveAspectRatio = event.shiftKey;
              return action;
          })
          .on('resizemove', function(event) {
              var target          = event.target;
              var x               = (parseFloat(target.getAttribute('data-x')) || 0);
              var y               = (parseFloat(target.getAttribute('data-y')) || 0);

              var numberOfSelectedEdges = 0;
              var selectedEdge = '';
              _.each(event.edges, function(edge, key){
                if(edge){
                  selectedEdge = key;
                  numberOfSelectedEdges++;
                }
              });

              // - 32 for the text component ( padding + border )
              if(numberOfSelectedEdges == 2) {
                target.style.width  = (event.rect.width / $rootScope.badgeZoom) - 32 + 'px';
                target.style.height = (event.rect.height / $rootScope.badgeZoom) - 32 + 'px';
              } else if(selectedEdge == 'top' || selectedEdge == 'bottom') {
                target.style.height = (event.rect.height / $rootScope.badgeZoom) - 32 + 'px';
              } else if(selectedEdge == 'left' || selectedEdge == 'right') {
                target.style.width  = (event.rect.width / $rootScope.badgeZoom) - 32 + 'px';
              } else {
                event.interactable.resizable({
                  preserveAspectRatio: event.shiftKey
                });

                target.style.width  = (event.rect.width / $rootScope.badgeZoom) - 32 + 'px';
                target.style.height = (event.rect.height / $rootScope.badgeZoom) - 32 + 'px';
              }

              x                   += (event.deltaRect.left / $rootScope.badgeZoom);
              y                   += (event.deltaRect.top / $rootScope.badgeZoom);

              target.style.top    = y + 'px';
              target.style.left   = x + 'px';

              target.setAttribute('data-x', x);
              target.setAttribute('data-y', y);
          })
          .on('dragend', function(event) {
              var objectToSend = {
                  componentId : parseInt(event.target.getAttribute('data-id')),
                  target      : event.target
              }
              templateEventsDispatcher.dispatchEvent(availableEvents.DRAG_END, objectToSend);
          })
          .on('resizeend', function(event) {
              var objectToSend = {
                  componentId : parseInt(event.target.getAttribute('data-id')),
                  target      : event.target
              }
              templateEventsDispatcher.dispatchEvent(availableEvents.RESIZE_END, objectToSend);
          })
          .preventDefault('never')
      };

      function dragMoveListener(event) {
        var target = event.target,
                 x = (parseFloat(target.getAttribute('data-x')) || 0) + (event.dx / $rootScope.badgeZoom),
                 y = (parseFloat(target.getAttribute('data-y')) || 0) + (event.dy / $rootScope.badgeZoom);
                id = parseInt(target.getAttribute('data-id'));
        target.setAttribute('data-x', x);
        target.setAttribute('data-y', y);

        target.style.top  = y + 'px';
        target.style.left = x + 'px';

        var objectToSend = {
            x: x,
            y: y,
            componentId: id
        };
      }

      function calculateNewComponentId(componentsList) {
        var maximumId = 0;
        angular.forEach(componentsList, function(item) {
          if (item.id > maximumId) {
            maximumId = item.id - 100;
          }
        });

        return maximumId + 101;
      }
    }
})();
