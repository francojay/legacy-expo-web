(function () {
    var controllerId = 'badgeEditorController';
    var module = angular.module('app');

    module.controller(controllerId, ['$scope', '$rootScope', 'templateEventsDispatcher', '$state', '$stateParams', '$compile', 'badgeHistoryService', 'ConferenceService', 'templateEditorApiService', badgeEditorController]);

    function badgeEditorController($scope, $rootScope, templateEventsDispatcher, $state, $stateParams, $compile, badgeHistoryService, ConferenceService, templateEditorApiService) {
        initInteract();
        initDroppableCapabilities();
        var availableEvents                     = templateEventsDispatcher.availableEvents;
        var divPosition                         = {};

        $scope.currentState                     = $state.current.name;
        $rootScope.badgeWidth                   = 4;
        $rootScope.badgeHeight                  = 3;
        $rootScope.badgeNumberSides             = $rootScope.badgeNumberSides || 1;
        $rootScope.badgeName                    = '';
        $rootScope.currentFolder                = 'shell';
        $rootScope.badgeRotationDegree          = 0;
        $rootScope.badgeZoom                    = 1;
        $rootScope.badgeBackgroundColor         = '#ffffff';
        $rootScope.badgeBackSideBackgroundColor = '#ffffff';

        $rootScope.$on(availableEvents.UPDATE_BADGE_SIZES, function(event, data) {
        	$rootScope.badgeWidth  = data.width;
        	$rootScope.badgeHeight = data.height;
        });

        $rootScope.$on(availableEvents.ZOOM_IN, function(event, zoomValue) {
            $rootScope.badgeZoom = zoomValue / 100;
        })

        $rootScope.$on(availableEvents.ZOOM_OUT, function(event, zoomValue) {
            $rootScope.badgeZoom = zoomValue / 100;
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
            if (toState.name === 'template_editor.customize') {
                $scope.currentState = toState.name;
            }
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
            if (toState.name === 'template_editor.define' || toState.name === 'template_editor.define_badge') {
                $scope.currentState = toState.name;
            }
        });

        $rootScope.$on(availableEvents.DESELECT_CURRENT_ACTIVE_COMPONENT, function(event, data){
          if ($rootScope.currentActiveComponentIdentifier && $rootScope.currentActiveComponentIdentifier !== data.componentId) {
            var currentActiveElement =  $("#template-safe-zone").find("[data-id=" + $rootScope.currentActiveComponentIdentifier + "]");
            var currentActiveScope   = angular.element(currentActiveElement).scope();
            currentActiveScope.$$childHead.makeComponentInactive(currentActiveScope.$$childHead.props);
          }
        });

        $scope.$on(availableEvents.ADD_TEXT_COMPONENT, function(event, data) {
            if (!data.fromHistory || (data.fromHistory && data.type === 'REDO')) {
                var oldLists = {};
                var newLists = {};

                oldLists.shell   = angular.copy($rootScope.componentsList.shell);
                oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

                var badgeInnerContainer = $('#template-safe-zone');
                var data = {
                    "fontSize"          : data.fontSize,
                    "fontStyle"         : data.fontStyle,
                    "fontFamily"        : data.fontFamily,
                    "textAlign"         : data.textAlign,
                    "textDecoration"    : data.textDecoration,
                    "opacity"           : data.opacity,
                    "fontWeight"        : data.fontWeight,
                    "color"             : data.color,
                    "id"                : calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint),
                    "height"            : "auto",
                    "zIndex"            : calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint),
                    "partOf"            : $rootScope.currentFolder,
                    "type"              : data.type,
                    "innerText"         : _.unescape("Click to Edit Copy"),
                    "xAxis"             : data.xAxis || 0,
                    "yAxis"             : data.yAxis || 0,
                    "componentSelected" : true
                };

                var componentSpecs = {
                    type  : 'TEXT',
                    props : data,
                    id    : data.id
                };

                if ($rootScope.currentFolder === 'shell') {
                    $rootScope.componentsList.shell.unshift(componentSpecs);
                } else {
                    $rootScope.componentsList.imprint.unshift(componentSpecs);
                }

                var newScope = $scope.$new(true);
                newScope.props = data;
                var element = $compile('<text-component data-props="props"></text-component>')(newScope);
                badgeInnerContainer.append(element);

                var action              = badgeHistoryService.createEmptyActionObject();

                action.type             = "ADD";
                action.componentType    = "TEXT";
                action.triggeredEvent   = "ADD_TEXT_COMPONENT";
                action.componentId      = componentSpecs.id;
                action.propsOldValue    = data;
                action.props            = data;
                action.props.opacity    = data.opacity;

                if (data.type !== "REDO") {
                  badgeHistoryService.rememberAction(action);

                  newLists.shell   = angular.copy($rootScope.componentsList.shell);
                  newLists.imprint = angular.copy($rootScope.componentsList.imprint);
                  badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
                }
            };

        });

        $scope.$on(availableEvents.ADD_IMAGE_COMPONENT, function(event,data) {
            if (!data.fromHistory || (data.fromHistory && data.type === 'REDO')) {
              var oldLists = {};
              var newLists = {};

              oldLists.shell   = angular.copy($rootScope.componentsList.shell);
              oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

              data.id                 = calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint);
              data.zIndex             = calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint);
              data.partOf             = $rootScope.currentFolder;
              data.componentSelected  = true;

              var componentSpecs = {
                  type    : 'IMAGE',
                  props   : data,
                  id      : data.id
              };

              if ($rootScope.currentFolder === 'shell') {
                  $rootScope.componentsList.shell.unshift(componentSpecs);
              } else {
                  $rootScope.componentsList.imprint.unshift(componentSpecs);
              }

              var badgeInnerContainer = $('#template-safe-zone');
              var newScope            = $scope.$new(true);
              newScope.props          = data;

              var element             = $compile('<image-component data-props="props"></image-component>')(newScope)

              badgeInnerContainer.append(element);

              var action              = badgeHistoryService.createEmptyActionObject();
              action.type             = "ADD";
              action.componentType    = "IMAGE";
              action.componentId      = componentSpecs.id;
              action.props            = data;
              action.propsOldValue    = data;
              action.triggeredEvent   = "ADD_IMAGE_COMPONENT";

              if (data.type !== "REDO") {
                badgeHistoryService.rememberAction(action);

                newLists.shell   = angular.copy($rootScope.componentsList.shell);
                newLists.imprint = angular.copy($rootScope.componentsList.imprint);
                badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
              }
            }
        });

        $scope.$on(availableEvents.ADD_SHAPE_COMPONENT, function(event, data) {
            if (!data.fromHistory || (data.fromHistory && data.type === 'REDO')) {
              var oldLists = {};
              var newLists = {};

              oldLists.shell   = angular.copy($rootScope.componentsList.shell);
              oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

              data.id                 = calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint)
              data.zIndex             = data.id;
              data.partOf             = $rootScope.currentFolder;
              data.componentSelected  = true;

              var componentSpecs = {
                  type  : 'SHAPE',
                  props : data,
                  id    : data.id
              };

              if ($rootScope.currentFolder === 'shell') {
                  $rootScope.componentsList.shell.unshift(componentSpecs);
              } else {
                  $rootScope.componentsList.imprint.unshift(componentSpecs);
              }

              var badgeInnerContainer = $('#template-safe-zone');
              var newScope            = $scope.$new(true);
              newScope.props = data;

              var element = $compile('<shape-component data-props="props"></shape-component>')(newScope);
              badgeInnerContainer.append(element);

              var action = badgeHistoryService.createEmptyActionObject();

              action.type = "ADD";
              action.componentType = "SHAPE";
              action.componentId = componentSpecs.id;
              action.props = data;
              action.propsOldValue = data;
              action.triggeredEvent = "ADD_SHAPE_COMPONENT";

              if (data.type !== "REDO") {
                badgeHistoryService.rememberAction(action);
                newLists.shell   = angular.copy($rootScope.componentsList.shell);
                newLists.imprint = angular.copy($rootScope.componentsList.imprint);
                badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
              }
            }

        });

        $scope.$on(availableEvents.ADD_CODE_COMPONENT, function(event, data) {
            if (!data.fromHistory || (data.fromHistory && data.type === 'REDO')) {
              var oldLists = {};
              var newLists = {};

              oldLists.shell   = angular.copy($rootScope.componentsList.shell);
              oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

              $rootScope.currentFolder = 'imprint';

              data.id                 = calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint);
              data.zIndex             = data.id;
              data.partOf             = $rootScope.currentFolder;
              data.componentSelected  = true;

              var componentSpecs = {
                  type  : 'CODE',
                  props : data,
                  id    : data.id
              };


                $rootScope.componentsList.imprint.unshift(componentSpecs);
                templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_OPEN_FOLDER, {folder: 'imprint'});

              var badgeInnerContainer = $('#template-safe-zone');

              var newScope            = $scope.$new(true);
              data.id                 = componentSpecs.id;
              data.zIndex             = data.id;

              newScope.props = data;

              var element = $compile('<code-component data-props="props"></code-component>')(newScope);
              badgeInnerContainer.append(element);

              var action = badgeHistoryService.createEmptyActionObject();

              action.type = "ADD";
              action.componentType = "CODE";
              action.componentId = componentSpecs.id;
              action.props = data;
              action.propsOldValue = data;
              action.triggeredEvent = "ADD_CODE_COMPONENT";
              if (data.type !== "REDO") {
                badgeHistoryService.rememberAction(action);
                newLists.shell   = angular.copy($rootScope.componentsList.shell);
                newLists.imprint = angular.copy($rootScope.componentsList.imprint);
                badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
              }
            }

        });

        $rootScope.$on(availableEvents.CURRENT_FOLDER_TO_ADD_CHANGED, function(event, data) {
            $rootScope.currentFolder = data.typeOfTab;
        });

        $rootScope.$on(availableEvents.SHOW_BADGE_FRONT, function(event, data){
          $rootScope.badgeRotationDegree = 0;
        });

        $rootScope.$on(availableEvents.SHOW_BADGE_BACK, function(event, data){
          $rootScope.badgeRotationDegree = 180;
        });

        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
           toggleBadgeComponentsVisibility(to, from);
           $rootScope.previousState = from;
        });

        function toggleBadgeComponentsVisibility(toState, fromState) {
          var badgeContainer = document.getElementById('template-safe-zone')

          if (toState.name === 'template_editor.define_badge' && fromState && fromState.name === 'template_editor.customize') {
            _.each(badgeContainer.children, function(component){
                component.style.visibility = 'hidden';
            });

          } else if (toState.name === 'template_editor.customize' && fromState && fromState.name === 'template_editor.define_badge') {
            _.each(badgeContainer.children, function(component){
                component.style.visibility = 'visible';
            });

          } else if (toState.name === 'template_editor.review' && fromState && fromState.name === 'template_editor.customize') {
            _.each(badgeContainer.children, function(component){
                component.style.pointerEvents = 'none';
            });
          }  else if (toState.name === 'template_editor.customize' && fromState && fromState.name === 'template_editor.review') {
            _.each(badgeContainer.children, function(component){
                component.style.pointerEvents = 'unset';
            });
          }
        }

        function initInteract() {
            var badge = document.querySelector('.badge-container');

            interact('div.draggable', {
                context: badge
            })
            .resizable({
                edges: { left: true, right: true, bottom: true, top: true },
                invert: 'none',
                margin: 20,
            })
            .draggable({
                intertia: true,
                onmove: dragMoveListener,
            })
            .actionChecker(function (pointer, event, action, interactable) {
                interactable.options.resize.preserveAspectRatio = event.shiftKey;
                return action;
            })
            .on('resizemove', function(event) {
                var target          = event.target;

                var x               = (parseFloat(target.getAttribute('data-x')) || 0);
                var y               = (parseFloat(target.getAttribute('data-y')) || 0);

                var numberOfSelectedEdges = 0;
                var selectedEdge = '';
                _.each(event.edges, function(edge, key){
                  if(edge){
                    selectedEdge = key;
                    numberOfSelectedEdges++;
                  }
                });

                // - 32 for the text component ( padding + border )
                if(numberOfSelectedEdges == 2) {
                  if (target.classList.contains('code-component')) {
                    target.style.width  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                    target.style.height = (event.rect.width / $rootScope.badgeZoom) + 'px';
                  } else {
                    target.style.width  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                    target.style.height = (event.rect.height / $rootScope.badgeZoom) + 'px';
                  }
                } else if((selectedEdge == 'top' || selectedEdge == 'bottom')) {
                  if (target.classList.contains('code-component')) {
                    target.style.height = (event.rect.height / $rootScope.badgeZoom) + 'px';
                    target.style.width = (event.rect.height / $rootScope.badgeZoom) + 'px';
                  }
                  target.style.height = (event.rect.height / $rootScope.badgeZoom) + 'px';
                } else if((selectedEdge == 'left' || selectedEdge == 'right')) {
                  if (target.classList.contains('code-component')) {
                    target.style.width  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                    target.style.height  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                  } else {
                    target.style.width  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                  }
                } else {
                  target.style.width  = (event.rect.width / $rootScope.badgeZoom) + 'px';
                  target.style.height = (event.rect.height / $rootScope.badgeZoom) + 'px';

                  event.interactable.resizable({
                    preserveAspectRatio: event.shiftKey
                  });

                }
                x                   += (event.deltaRect.left / $rootScope.badgeZoom);
                y                   += (event.deltaRect.top / $rootScope.badgeZoom);

                target.style.top    = y + 'px';
                target.style.left   = x + 'px';
                target.setAttribute('data-x', x);
                target.setAttribute('data-y', y);
            })
            .on('dragend', function(event) {
                var objectToSend = {
                    componentId : parseInt(event.target.getAttribute('data-id')),
                    target      : event.target
                }
                templateEventsDispatcher.dispatchEvent(availableEvents.DRAG_END, objectToSend);
            })
            .on('resizeend', function(event) {
                var objectToSend = {
                    componentId : parseInt(event.target.getAttribute('data-id')),
                    target      : event.target
                }
                templateEventsDispatcher.dispatchEvent(availableEvents.RESIZE_END, objectToSend);
            })
            .preventDefault('never')
        };

        function dragMoveListener(event) {
            var target = event.target,d
                     x = (parseFloat(target.getAttribute('data-x')) || 0) + (event.dx / $rootScope.badgeZoom),
                     y = (parseFloat(target.getAttribute('data-y')) || 0) + (event.dy / $rootScope.badgeZoom),
                    id = parseInt(target.getAttribute('data-id'));


            target.setAttribute('data-x', x);
            target.setAttribute('data-y', y);

            target.style.top  = y + 'px';
            target.style.left = x + 'px';
        }

        function initDroppableCapabilities() {
          interact('#template-safe-zone').dropzone({
            accept: '.category-button',
            overlap: 0.5,
            ondropactivate: function (event) {
              event.target.classList.add('drop-active');
              $('#template-safe-zone').mouseup(function(e) {
                divPosition.x = e.offsetX;
                divPosition.y = e.offsetY;
              })
            },
            ondragenter: function (event) {
              var divPosition = {};

                $('#template-safe-zone').mouseup(function(e) {
                  divPosition.x = e.offsetX;
                  divPosition.y = e.offsetY;
                });

              var draggableElement 	= event.relatedTarget;
              var dropzoneElement 	= event.target;
              $(dropzoneElement).parent('.text-component').addClass('drop-target');
            },
            ondragleave: function (event) {
              $(event.target).parent('.text-component').removeClass('drop-target');
            },
            ondrop: function (event) {
              var oldLists = {};
              var newLists = {};

              oldLists.shell   = angular.copy($rootScope.componentsList.shell);
              oldLists.imprint = angular.copy($rootScope.componentsList.imprint);

              var mergeFieldContent   = $(event.relatedTarget).html();
              var mailMergeDragData   = $(event.relatedTarget).data('drag-value');
              var isCustomField       = $(event.relatedTarget).data('is-custom-field=');
              var badgeInnerContainer = $('#template-safe-zone');
              $(event.relatedTarget).remove();


              var badgeInnerContainer = $('#template-safe-zone');
              var data = {
                  "fontSize"            : 16,
                  "fontStyle"           : 'inherit',
                  "fontFamily"          : "ABeeZee",
                  "textAlign"           : 'left',
                  "textDecoration"      : 'none',
                  "opacity"             : 1,
                  "fontWeight"          : '400',
                  "color"               : 'black',
                  "id"                  : calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint),
                  "height"              : 16,
                  "zIndex"              : calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint),
                  "innerText"           : mergeFieldContent,
                  "mailMergeField"      : mailMergeDragData,
                  "isCustomField"       : isCustomField,
                  "partOf"              : 'imprint',
                  "yAxis"               : divPosition.y,
                  "xAxis"               : divPosition.x,
                  "componentSelected"   : true
              };

              loadDefaultFontFamily(data.id, data.fontFamily);

              var componentSpecs = {
                  type: 'TEXT',
                  props: data,
                  id: calculateNewComponentId($rootScope.componentsList.shell, $rootScope.componentsList.imprint)
              };

              $rootScope.componentsList.imprint.unshift(componentSpecs);

              var newScope = $scope.$new(true);
              newScope.props = data;

              var element = $compile('<text-component data-props="props"></text-component>')(newScope)

              badgeInnerContainer.append(element);
              templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_OPEN_FOLDER, {folder: 'imprint'});

              var action              = badgeHistoryService.createEmptyActionObject();

              action.type             = "ADD";
              action.componentType    = "TEXT";
              action.triggeredEvent   = "ADD_TEXT_COMPONENT";
              action.componentId      = componentSpecs.id;
              action.propsOldValue    = data;
              action.props            = data;
              action.props.opacity    = data.opacity * 100;
              if (data.type !== "REDO") {
                badgeHistoryService.rememberAction(action);
              }
              newLists.shell   = angular.copy($rootScope.componentsList.shell);
              newLists.imprint = angular.copy($rootScope.componentsList.imprint);
              badgeHistoryService.rememberActionForLayersMenu(oldLists, newLists);
            },
            ondropdeactivate: function (event) {
              event.target.classList.remove('drop-active');
              event.target.classList.remove('drop-target');
              $(event.target).parent('.text-component').removeClass('drop-target');
            }
          });

          function loadDefaultFontFamily(componentId, font) {
    					var head = document.getElementsByTagName('head')[0];
    					var link = document.createElement('link');

    					link.rel = 'stylesheet';
    					link.type = 'text/css';
    					link.href = 'https://fonts.googleapis.com/css?family='+ font;
    					link.media = 'all';
    					head.appendChild(link);

    					var objectToSend = {
    						componentId: componentId,
    						fontFamily: font
    					}
    					templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_TEXT_PROPS__FONT_FAMILY, objectToSend);
    			};
        }

        function calculateNewComponentId(shell, imprint) {
          var maximumId = 0;
          angular.forEach(shell, function(shellItem) {
            if (shellItem.id > maximumId) {
              maximumId = shellItem.id;
            }
          });

          angular.forEach(imprint, function(imprintItem) {
            if (imprintItem.id > maximumId) {
              maximumId = imprintItem.id;
            }
          });

          return maximumId + 1;
        }

        document.getElementsByClassName('badge-container')[0].addEventListener('keydown', function (event) {
          if (event.keyCode == 8) {
            var currentId = $rootScope.currentActiveComponentIdentifier;

            var currentActiveElement      =  $("#template-safe-zone").find("[data-id=" + $rootScope.currentActiveComponentIdentifier + "]");
            var currentActiveScope        = angular.element(currentActiveElement).scope();
            var propsOfCurrentActiveScope = currentActiveScope.$$childHead;
            if ('inMoveMode' in propsOfCurrentActiveScope) {
              if (propsOfCurrentActiveScope['inMoveMode']) {
                angular.forEach($rootScope.componentsList.shell, function(component, index) {
                  if (component.id === currentId) {
                    templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_TEXT_COMPONENT, {componentId: component.id});
                    $rootScope.componentsList.shell.splice(index, 1);
                  }
                });
                angular.forEach($rootScope.componentsList.imprint, function(component, index) {
                  if (component.id === currentId) {
                    templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_TEXT_COMPONENT, {componentId: component.id});
                    $rootScope.componentsList.imprint.splice(index, 1);
                  }
                });
              } else {
                angular.forEach($rootScope.componentsList.imprint, function(component, index) {
                  if (component.id === currentId) {
                    templateEventsDispatcher.dispatchEvent(availableEvents.DELETE_TEXT_COMPONENT, {componentId: component.id});
                    $rootScope.componentsList.imprint.splice(index, 1);
                  }
                });
              }
            } else {
              angular.forEach($rootScope.componentsList.shell, function(component, index) {
                if (component.id === currentId) {
                  var eventToSend = "DELETE_" + component.type + "_COMPONENT";
                  templateEventsDispatcher.dispatchEvent(eventToSend, {componentId: component.id});
                  $rootScope.componentsList.shell.splice(index, 1);
                }
              });

              angular.forEach($rootScope.componentsList.imprint, function(component, index) {
                if (component.id === currentId) {
                  var eventToSend = "DELETE_" + component.type + "_COMPONENT";
                  templateEventsDispatcher.dispatchEvent(eventToSend, {componentId: component.id});
                  $rootScope.componentsList.imprint.splice(index, 1);
                }
              })
            }

          }
      });
    };
})();
