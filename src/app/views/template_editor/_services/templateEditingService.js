(function(){
	'use strict'
  var serviceId = 'templateEditingService';

  var module = angular.module('app');

  module.service(serviceId, ['$rootScope', '$q', '$http', 'localStorageService', 'constructBaseUrlService', templateEditingService]);

	function templateEditingService($rootScope, $q, $http, localStorageService, constructBaseUrlService) {
		var service = {};

    service.getGoogleFontsList = function() {
      var deferred = $q.defer();
      var url = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyCzL20bQ8GEE9j9ixK54IF55Y02RIqj7js';

      $http({
        method: 'GET',
        url: url
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

      return deferred.promise;
    };

		return service;
	}
})();
