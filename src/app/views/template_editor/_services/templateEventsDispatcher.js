(function() {
    'use strict'
    var serviceId = 'templateEventsDispatcher';

    var module = angular.module('app');

    module.service(serviceId, ['$rootScope', templateEventsDispatcher]);

    function templateEventsDispatcher($rootScope) {
        var service = {};

        service.availableEvents = {
            //misc events
            HIDE_OPEN_CONTEXTUAL_MENUS                    : 'HIDE_OPEN_CONTEXTUAL_MENUS',
            COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS : 'COMPONENTS_BUTTONS_HIDE_OPEN_CONTEXTUAL_MENUS',
            DRAG_END                                      : 'DRAG_END',
            RESIZE_END                                    : 'RESIZE_END',
            UPDATE_POSITION                               : 'UPDATE_POSITION',
            CURRENT_FOLDER_TO_ADD_CHANGED                 : 'CURRENT_FOLDER_TO_ADD_CHANGED',
            INDEX_HAS_CHANGED                             : 'INDEX_HAS_CHANGED',
            UPDATE_ROTATION_ANGLE                         : 'UPDATE_ROTATION_ANGLE',
            UPDATE_LAYERS_MENU_ORDER                      : 'UPDATE_LAYERS_MENU_ORDER',
            UPDATE_OPEN_FOLDER                            : 'UPDATE_OPEN_FOLDER',
            MOVE_COMPONENT_TO_SHELL                       : 'MOVE_COMPONENT_TO_SHELL',
            MOVE_COMPONENT_TO_IMPRINT                     : 'MOVE_COMPONENT_TO_IMPRINT',
            MAKE_LAYER_COMPONENT_ACTIVE                   : 'MAKE_LAYER_COMPONENT_ACTIVE',
            MAKE_COMPONENT_ACTIVE                         : 'MAKE_COMPONENT_ACTIVE',
            DESELECT_CURRENT_ACTIVE_COMPONENT             : 'DESELECT_CURRENT_ACTIVE_COMPONENT',
            DESELECT_CURRENT_ACTIVE_BACK_COMPONENT        : 'DESELECT_CURRENT_ACTIVE_BACK_COMPONENT',
            SAVE_TEMPLATE                                 : 'SAVE_TEMPLATE',
            //define events
            UPDATE_BADGE_SIZES                            : 'UPDATE_BADGE_SIZES',
            UPDATE_TYPE_OF_PRINT                          : 'UPDATE_TYPE_OF_PRINT',
            SHOW_BADGE_FRONT                              : 'SHOW_BADGE_FRONT',
            SHOW_BADGE_BACK                               : 'SHOW_BADGE_BACK',
            // undo,redo events
            UNDO_ACTION                                   : 'UNDO_ACTION',
            REDO_ACTION                                   : 'REDO_ACTION',
            // move component with arrows
            MOVE_LEFT                                     : 'MOVE_LEFT',
            MOVE_RIGHT                                    : 'MOVE_RIGHT',
            MOVE_UP                                       : 'MOVE_UP',
            MOVE_DOWN                                     : 'MOVE_DOWN',
            // align events
            ALIGN_TO_LEFT                                 : 'ALIGN_TO_LEFT',
            ALIGN_TO_RIGHT                                : 'ALIGN_TO_RIGHT',
            ALIGN_TO_CENTER                               : 'ALIGN_TO_CENTER',
            // arrange events
            SEND_BACKWARD                                 : 'SEND_BACKWARD',
            SEND_FORWARD                                  : 'SEND_FORWARD',
            SEND_TO_BACK                                  : 'SEND_TO_BACK',
            SEND_TO_FRONT                                 : 'SEND_TO_FRONT',
            // zoom events
            ZOOM_IN                                       : 'ZOOM_IN',
            ZOOM_OUT                                      : 'ZOOM_OUT',
            // options events
            ROTATE_90_DEGREES                             : 'ROTATE_90_DEGREES',
            SHOW_TEXT_BOX_OUTLINES                        : 'SHOW_TEXT_BOX_OUTLINES',
            SNAP_TO_EDGES                                 : 'SNAP_TO_EDGES',
            GO_FULL_SCREEN                                : 'GO_FULL_SCREEN',
            //customize events

            //add components events
            ADD_TEXT_COMPONENT                            : 'ADD_TEXT_COMPONENT',
            ADD_IMAGE_COMPONENT                           : 'ADD_IMAGE_COMPONENT',
            ADD_SHAPE_COMPONENT                           : 'ADD_SHAPE_COMPONENT',
            ADD_CODE_COMPONENT                           : 'ADD_CODE_COMPONENT',

            ADD_BACK_TEXT_COMPONENT                       : 'ADD_BACK_TEXT_COMPONENT',
            ADD_BACK_IMAGE_COMPONENT                      : 'ADD_BACK_IMAGE_COMPONENT',
            ADD_BACK_SHAPE_COMPONENT                      : 'ADD_BACK_SHAPE_COMPONENT',
            //delete components events
            DELETE_TEXT_COMPONENT                         : 'DELETE_TEXT_COMPONENT',
            DELETE_SHAPE_COMPONENT                        : 'DELETE_SHAPE_COMPONENT',
            DELETE_IMAGE_COMPONENT                        : 'DELETE_IMAGE_COMPONENT',
            DELETE_CODE_COMPONENT                         : 'DELETE_CODE_COMPONENT',


            //open contextual menus from components events
            OPEN_TEXT_MENU_FROM_BADGE_COMPONENT           : 'OPEN_TEXT_MENU_FROM_BADGE_COMPONENT',
            UPDATED_TEXT_PROPS                            : 'UPDATED_TEXT_PROPS',
            CHANGE_FOLDER_FOR_COMPONENT                   : 'CHANGE_FOLDER_FOR_COMPONENT',
            OPEN_IMAGE_MENU_FROM_BADGE_COMPONENT          : 'OPEN_IMAGE_MENU_FROM_BADGE_COMPONENT',
            UPDATED_IMAGE_PROPS                           : 'UPDATED_IMAGE_PROPS',
            OPEN_SHAPE_MENU_FROM_BADGE_COMPONENT          : 'OPEN_SHAPE_MENU_FROM_BADGE_COMPONENT',
            UPDATED_SHAPE_PROPS                           : 'UPDATED_SHAPE_PROPS',
            OPEN_CODE_MENU_FROM_BADGE_COMPONENT           : 'OPEN_CODE_MENU_FROM_BADGE_COMPONENT',


            //update existing text component
            UPDATE_TEXT_PROPS__FONT_STYLE                 : 'UPDATE_TEXT_PROPS__FONT_STYLE',
            UPDATE_TEXT_PROPS__TEXT_DECORATION            : 'UPDATE_TEXT_PROPS__TEXT_DECORATION',
            UPDATE_TEXT_PROPS__TEXT_ALIGN                 : 'UPDATE_TEXT_PROPS__TEXT_ALIGN',
            UPDATE_TEXT_PROPS__OPACITY                    : 'UPDATE_TEXT_PROPS__OPACITY',
            UPDATE_TEXT_PROPS__FONT_SIZE                  : 'UPDATE_TEXT_PROPS__FONT_SIZE',
            UPDATE_TEXT_PROPS__FONT_FAMILY                : 'UPDATE_TEXT_PROPS__FONT_FAMILY',
            UPDATE_TEXT_PROPS__FONT_WEIGHT                : 'UPDATE_TEXT_PROPS__FONT_WEIGHT',
            UPDATE_TEXT_PROPS__COLOR                      : 'UPDATE_TEXT_PROPS__COLOR',
            ADD_MAIL_MERGE_FIELD                          : 'ADD_MAIL_MERGE_FIELD',
            //update existing image component
            UPDATE_IMAGE_PROPS__CONSTRAINING              : 'UPDATE_IMAGE_PROPS_CONSTRAINING',
            UPDATE_IMAGE_PROPS__OPACITY                   : 'UPDATE_IMAGE_PROPS_OPACITY',

            //update existing shape component
            UPDATE_SHAPE_PROPS__STROKE_WIDTH              : 'UPDATE_SHAPE_PROPS__STROKE_WIDTH',
            UPDATE_SHAPE_PROPS__OPACITY                   : 'UPDATE_SHAPE_PROPS__OPACITY',
            UPDATE_SHAPE_PROPS__STROKE_COLOR              : 'UPDATE_SHAPE_PROPS__STROKE_COLOR',
            UPDATE_SHAPE_PROPS__FILL                      : 'UPDATE_SHAPE_PROPS__FILL',

            TEST_PRINT                                    : 'TEST_PRINT',

        };

        service.dispatchEvent = function(typeOfEvent, data) {
            var dispatchedEventIsRegistered = false;

            if (typeOfEvent) {
                dispatchedEventIsRegistered = service._searchEventInGivenSet(service.availableEvents, typeOfEvent);

                if (dispatchedEventIsRegistered) {
                    if (!$rootScope.$$listeners[typeOfEvent]) {
                        console.warn('THE EVENT: [', typeOfEvent, '] HAS NO LISTENERS! PLEASE IMPLEMENT THE LISTENER WHERE IT IS NEEDED! EVENT HAS NOT BEEN DISPATCHED!');
                    } else {
                        $rootScope.$broadcast(typeOfEvent, data);
                    }
                } else {
                    alert('EVENT: [' + typeOfEvent + '] HAS NOT BEEN DEFINED! USE EVENTS ONLY FROM THE ONES DECLARED');
                }
            } else {
                alert('SEND EVENT NAME!');
            }
        };

        service._searchEventInGivenSet = function(set, givenEvent) { // functions starting with "_" are internal functions left only for the internal purpose of the service and should not be exposed;
            var foundEventInSet = false;
            angular.forEach(set, function(event) {
                if (event === givenEvent) {
                    foundEventInSet = true;
                }
            });

            return foundEventInSet;
        }

        var exposedService = {
            availableEvents: service.availableEvents,
            dispatchEvent: service.dispatchEvent,
        }

        return exposedService;
    }
})();
