(function(){
	'use strict'
  var serviceId = 'templateEditorApiService';

  var module = angular.module('app');

  module.service(serviceId, ['$rootScope', '$q', '$http', 'localStorageService', 'Request2', 'constructBaseUrlService', templateEditorApiService]);

	function templateEditorApiService($rootScope, $q, $http, localStorageService, Request2, constructBaseUrlService) {
		var service = {};

		service.createBadge = function(conferenceId, badge) {
			var deferred = $q.defer();

			var url = 'api/v1.1/conferences/' + conferenceId + '/badges/';

			Request2.post(url, badge, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.updateBadge = function(conferenceId, badge) {
			var deferred = $q.defer();

			var url = '/api/v1.1/conferences/' + conferenceId + '/badges/' + badge.id;

			$http({
        method: 'PUT',
        url: constructBaseUrlService.getBaseUrl() + url,
				data: badge,
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('access_token'),
          'Content-Type': 'application/json',
          'Accept': '*/*'
        }
      }).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

			return deferred.promise;
		};

		service.saveComponents = function(conferenceId, badgeId, componentsObject) {
			var deferred = $q.defer();

			var url = 'api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/components/';

			Request2.post(url, componentsObject, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.saveTemplates = function(conferenceId, badgeId, templates) {
			var deferred = $q.defer();

			var url = 'api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId + '/rendered/';

			Request2.post(url, templates, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.getBadgeData = function(conferenceId, badgeId) {
			var deferred = $q.defer();

			var url = 'api/v1.1/conferences/' + conferenceId + '/badges/' + badgeId;

			Request2.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		};

		service.getBadgeComponents = function(conferenceId, badgeId) {
			var deferred = $q.defer();

			var url = "api/v1.1/conferences/" + conferenceId + '/badges/' + badgeId + '/components/';

			Request2.get(url, {}, false).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.getModelDataForBadge = function(conferenceId, queryString, next_page) {
			var deferred = $q.defer();

			$http({
				method: 'GET',
				url: constructBaseUrlService.getBaseUrl() + '/api/attendee/list/?conference_id=' + conferenceId + '&q=' + queryString + '&page=' + next_page,
				headers: {
					'Authorization': 'Bearer ' + localStorageService.get('access_token'),
					'Content-Type': 'application/json',
					'Accept': '*/*'
				}
			}).then(function successCallback(response) {
        deferred.resolve(response);
      }, function errorCallback(error) {
        deferred.reject(error)
      });

			return deferred.promise;
		}

		return service;
	}
})();
