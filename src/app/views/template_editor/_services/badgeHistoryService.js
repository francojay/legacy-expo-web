(function(){
	'use strict'
  var serviceId = 'badgeHistoryService';

  var module = angular.module('app');

  module.service(serviceId, ['$rootScope', '$q', '$http', 'templateEventsDispatcher', badgeHistoryService]);

	function badgeHistoryService($rootScope, $q, $http, templateEventsDispatcher) {
		var service 														= {};

    service.currentActionsRenderedOnScreen 	= [];
		service.currentActionsRenderedOnBackSide = [];
    service.undoedActions 									= [];
		service.layersStateHistory 							= [];
		service.poppedLayersHistory 						= [];
		service.undoedActionsForShellBackSide 	= [];

    service.createEmptyActionObject 				= function() {
      var obj = {
        type: '', // 'ADD', 'EDIT', 'DELETE'
        componentType: '', // 'TEXT', 'IMAGE', 'SHAPE', 'CODE', 'BADGE'
        componentId: '',
        triggeredEvent: '',
        props: {},
        propsOldValue: {}
      };

      return obj;
    };

    service.rememberActionForLayersMenu 		= function(oldLists, newLists) {
      var action = {
        type: "EDIT",
        componentType: "LAYERS_MENU",
        triggeredEvent: "UPDATE_LAYERS_MENU_ORDER",
        oldLists: oldLists,
        newLists: newLists
      };
			service.currentActionsRenderedOnScreen.push(action);
    }

    service.rememberAction 									= function(action) {
			if ($rootScope.badgeRotationDegree === 0) {
				service.currentActionsRenderedOnScreen.push(action);
			} else if ($rootScope.badgeRotationDegree === 180) {
				service.rememberActionForShellBackSide(action);
			}
    };

		service.rememberActionForShellBackSide 	= function(action) {
			service.currentActionsRenderedOnBackSide.push(action);
		}

		service.undoActionForShellBackSide			= function() {
			if (service.currentActionsRenderedOnBackSide.length > 0) {
				var undoedAction = service.currentActionsRenderedOnBackSide.pop();
				undoedAction.type = "UNDO";
				service.undoedActionsForShellBackSide.push(undoedAction);
				service._sendEventToUpdateComponent(undoedAction);
			}
		}

		service.redoActionForShellBackSide			= function() {
			
		}

    service.redoAction 											= function() {
			if ($rootScope.badgeRotationDegree === 180) {
				service.redoActionForShellBackSide()
			} else {
				var action = null;
				if (service.undoedActions.length > 0) {
					action = service.undoedActions.pop();
					action.type = "REDO";
					service.currentActionsRenderedOnScreen.push(action);
					if (action.triggeredEvent.indexOf("ADD_") > -1) {
						service._sendEventToUpdateComponent(action);
						service.redoAction();
					} else if (action.componentType === "LAYERS_MENU") {
						var objectToSend = action.newLists;
						templateEventsDispatcher.dispatchEvent(action.triggeredEvent, objectToSend);
					} else {
						service._sendEventToUpdateComponent(action);
					}
				}
				return action;
			}
    }

    service.undoAction 											= function() {
			var action = null;
			if ($rootScope.badgeRotationDegree === 180) {
				service.undoActionForShellBackSide();
			} else {
				if (service.currentActionsRenderedOnScreen.length > 0) {
					action = service.currentActionsRenderedOnScreen.pop();

					action.type = "UNDO";
		      service.undoedActions.push(action);
		      if (action.componentType === 'LAYERS_MENU') {
						var objectToSend = action.oldLists;
						templateEventsDispatcher.dispatchEvent(action.triggeredEvent, objectToSend);
						//am modificat ceva in layers dar e posibil sa se fi modificat in acelasi timp cu adaugarea unei noi componente
						var actionBeforeLayersMenuAction = service.currentActionsRenderedOnScreen[service.currentActionsRenderedOnScreen.length - 1];
						if (actionBeforeLayersMenuAction.type === "ADD") {
							var action = service.currentActionsRenderedOnScreen.pop();
							action.type = "UNDO";
							service.undoedActions.push(action);
							service._sendEventToUpdateComponent(action);
						}
		      } else {
		        service._sendEventToUpdateComponent(action);
		      }
					return action;
				}
			}
    };

    service._sendEventToUpdateComponent 		= function(action) {
      var objectToSend = {};

      if (action.type === "UNDO") {
        objectToSend            = action.propsOldValue;
      } else if (action.type === "REDO") {
        objectToSend            = action.props;
      }
      objectToSend.componentId  = action.componentId;
      objectToSend.type         = action.type;
      objectToSend.fromHistory  = true;

      if (action.affectedElements) {
        objectToSend.affectedElements = action.affectedElements;
      }


      templateEventsDispatcher.dispatchEvent(action.triggeredEvent, objectToSend);
    };

    var exposedService = {
      createEmptyActionObject					: service.createEmptyActionObject,
      rememberAction									: service.rememberAction,
      undoAction											: service.undoAction,
      redoAction											: service.redoAction,
      rememberActionForLayersMenu			: service.rememberActionForLayersMenu,
			rememberActionForShellBackSide 	: service.rememberActionForShellBackSide
    }

		return exposedService;
	}
})();
