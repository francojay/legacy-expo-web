(function(){
	'use strict';

    var directiveId = 'zoomButton';

	angular
		.module('app')
		.directive(directiveId, ['templateEventsDispatcher', '$rootScope', zoomButtonDirective]);

	function zoomButtonDirective(templateEventsDispatcher, $rootScope) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/_directives/zoomButton/zoomButtonView.html',
			replace: true,
			link: link
		}

    function link(scope) {
			scope.currentZoomValue = ($rootScope.badgeZoom * 100) || 100;

			scope.zoomIn = function(currentZoomValue) {
				if (currentZoomValue < 200) {
					currentZoomValue = currentZoomValue + 50;
				}

				templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.ZOOM_IN, currentZoomValue);
				scope.currentZoomValue = currentZoomValue;
			};

			scope.zoomOut = function(currentZoomValue) {
				if (currentZoomValue > 50) {
					currentZoomValue = currentZoomValue - 50;
				}

				templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.ZOOM_OUT, currentZoomValue);
				scope.currentZoomValue = currentZoomValue;
			}

    }
	};

}());
