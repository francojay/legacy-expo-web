(function(){
	'use strict';

    var directiveId = 'actionBarSwitch';

	angular
		.module('app')
		.directive(directiveId, ['templateEventsDispatcher', 'badgeHistoryService', actionBarSwitchDirective]);

	function actionBarSwitchDirective(templateEventsDispatcher, badgeHistoryService) {
    return {
      restrict: 'E',
			templateUrl: 'app/views/template_editor/_directives/actionBarSwitch/actionBarSwitchView.html',
			replace: true,
			link: link
    }

    function link(scope){
			var availableEvents = templateEventsDispatcher.availableEvents;
			scope.currentBadgeSide = 'front';

			scope.switchSidesOfBadge = function(){
				if(scope.currentBadgeSide == undefined || scope.currentBadgeSide == 'front'){
					scope.currentBadgeSide = 'back';
					templateEventsDispatcher.dispatchEvent(availableEvents.SHOW_BADGE_BACK);
				} else{
					scope.currentBadgeSide = 'front';
					templateEventsDispatcher.dispatchEvent(availableEvents.SHOW_BADGE_FRONT);
				}
			};

    };
  };
})();
