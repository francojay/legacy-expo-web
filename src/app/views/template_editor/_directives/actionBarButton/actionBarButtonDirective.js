(function(){
	'use strict';

    var directiveId = 'actionBarButton';

	angular
		.module('app')
		.directive(directiveId, ['$rootScope', '$timeout', 'templateEventsDispatcher', 'badgeHistoryService', actionBarButtonDirective]);

	function actionBarButtonDirective($rootScope, $timeout, templateEventsDispatcher, badgeHistoryService) {
		return {
			restrict: 'E',
			templateUrl: 'app/views/template_editor/_directives/actionBarButton/actionBarButtonView.html',
			replace: true,
			link: link,
			scope: {
				icon: "@icon",
				name: "@name",
				event: "@event"
			}
		}

        function link(scope) {
					if (scope.name === 'Undo') {
						init();
					}

					function init() {
						scope.canUndo = false;
						var functionsArray = [];

						angular.forEach(['ADD_TEXT_COMPONENT', 'ADD_BACK_TEXT_COMPONENT', 'ADD_IMAGE_COMPONENT','ADD_BACK_IMAGE_COMPONENT' ,'ADD_SHAPE_COMPONENT', 'ADD_BACK_SHAPE_COMPONENT', 'ADD_MAIL_MERGE_FIELD', 'ADD_CODE_COMPONENT'], function(event_name){
							 functionsArray[event_name] = $rootScope.$on(event_name, function(event){
								 	scope.canUndo = true;
									functionsArray[event_name]();
							 });
					 	});
					}


					scope.dispatchEvent = function($event) {
						$event.stopPropagation();
						$event.preventDefault();

						if (scope.event === 'UNDO_ACTION') {
							if(!badgeHistoryService.undoAction()){
								scope.canUndo = false;
							} else{
								scope.canUndo = true;
							}
						} else if (scope.event === "REDO_ACTION") {
							if(!badgeHistoryService.redoAction()){
								scope.canRedo = false;
							} else{
								scope.canRedo = true;
							}
						} else {
							if (scope.event) {
								templateEventsDispatcher.dispatchEvent(scope.event);
							}
							templateEventsDispatcher.dispatchEvent(templateEventsDispatcher.availableEvents.HIDE_OPEN_CONTEXTUAL_MENUS);
						}
					};
        }
	};

}());
