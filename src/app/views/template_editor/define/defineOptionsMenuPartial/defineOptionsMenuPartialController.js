(function () {
    'use strict'
    var controllerId = 'defineOptionsMenuPartialController';
    angular
        .module('app')
        .controller(controllerId, ['$rootScope', '$scope', 'templateEventsDispatcher', 'toastr', '$state', '$stateParams', '$timeout', 'templateEditorApiService', 'ConferenceService', defineOptionsMenuPartialController]);
    function defineOptionsMenuPartialController($rootScope, $scope, templateEventsDispatcher, toastr, $state, $stateParams, $timeout, templateEditorApiService, ConferenceService) {
    	var vm = this;
      var conferenceId = new Hashids('Expo', 7).decode($state.params.conference_id)[0];
    	var availableEvents = templateEventsDispatcher.availableEvents;
      $scope.showDefineMenu = true;

      $timeout(function() {
          if ($state.current.name.indexOf('define_badge') > 0) {
            //init with current values from the rootScope
            if ($rootScope.previousState && $rootScope.previousState.name === 'template_editor.customize') {
              $scope.printIndex = _.findIndex($scope.typesOfPrint,['sides', $rootScope.badgeNumberSides])
              $scope.sizesIndex = _.findIndex($scope.sizesOfPrint, {'width': $rootScope.badgeWidth, 'height': $rootScope.badgeHeight})
            } else {
              var badgeId = $state.params.badge_id;
              getBadgeData(badgeId);

            }

          } else {
            $scope.sizesIndex = 1;
        		$scope.printIndex = 0;
          }
      }, 0);

    	$scope.sizesOfPrint = [
    		{
    			width  : 4,
    			height : 6,
    			unit   : 'inch',
    		},
        {
          width  : 4,
          height : 3,
          unit   : 'inch'
        }
    	];

    	$scope.typesOfPrint = [
    		{
    			sides: 1,
    			name: '1-sided'
    		},
    		{
    			sides: 2,
    			name: '2-sided'
    		}
    	];

  		$scope.nextSize = function($event) {
        $event.stopPropagation();

        if ($scope.sizesOfPrint.length - $scope.sizesIndex > 1) {
  				$scope.sizesIndex++;
  			} else {
          $scope.sizesIndex = 0;
        }
        templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_BADGE_SIZES, $scope.sizesOfPrint[$scope.sizesIndex]);
  		};

  		$scope.previousSize = function($event) {
        $event.stopPropagation();

  			if ($scope.sizesIndex >= 1) {
				  $scope.sizesIndex--;
  			} else {
          $scope.sizesIndex = $scope.sizesOfPrint.length - 1;
        }
        templateEventsDispatcher.dispatchEvent(availableEvents.UPDATE_BADGE_SIZES, $scope.sizesOfPrint[$scope.sizesIndex]);
  		};

  		$scope.nextPrintType = function() {
  			if ($scope.typesOfPrint.length - $scope.printIndex > 1) {
  				$scope.printIndex++;
  			} else {
          $scope.printIndex = 0;
        }
  		};

  		$scope.previousPrintType = function() {
  			if ($scope.printIndex >= 1) {
  				$scope.printIndex--;
  			} else {
          $scope.printIndex = $scope.typesOfPrint.length - 1;
        }
  		};

      $scope.createBadge = function(dimensions, sides, title) {
        $rootScope.badgeNumberSides = $scope.typesOfPrint[$scope.printIndex].sides;
        if (title.length === 0) {
          toastr.error('Title of the badge is mandatory!');

        } else if ($state.params.badge_id) {
          var objectToSend = {
            id                 : $state.params.badge_id,
            width              : dimensions.width,
            height             : dimensions.height,
            sides              : sides.sides,
            title              : title,
            background         : $rootScope.badgeBackgroundColor || '#ffffff',
            backSideBackground : $rootScope.badgeBackSideBackgroundColor || '#ffffff'
          }

          updateBadge(objectToSend);

        } else {
          var objectToSend = {
            width              : dimensions.width,
            height             : dimensions.height,
            sides              : sides.sides,
            title              : title,
            background         : '#ffffff',
            backSideBackground : '#ffffff'
          }
          templateEditorApiService.createBadge(conferenceId, objectToSend).then(function(response) {
            $scope.showDefineMenu = false;
            var encodedConferenceId = new Hashids("Expo", 7).encode(conferenceId);
            $state.go('template_editor.customize', {'conference_id': encodedConferenceId , 'badge_id': response.id});
          }, function(error) {
            if(error.data){
              toastr.error(error.data.detail)
            }
          });
        }
      };

      $scope.goToManageTemplates = function() {
        var encodedConferenceId = new Hashids("Expo", 7).encode(conferenceId);
        $state.go('conferences.dashboard.attendees.expo_badges', {'conference_id': encodedConferenceId});
      }

      function initBadgeDefineSettings(badgeProps) {
        var props = badgeProps;
        $scope.printIndex = _.findIndex($scope.typesOfPrint,['sides', props.sides])

        $scope.sizesIndex = _.findIndex($scope.sizesOfPrint, {'width': props.width, 'height': props.height})

        $rootScope.badgeWidth = props.width
        $rootScope.badgeHeight = props.height;
        $rootScope.badgeNumberSides = props.sides;
        $rootScope.badgeName = props.title;
        $rootScope.badgeBackgroundColor = props.background || '#ffffff';
        $rootScope.badgeBackSideBackgroundColor = props.backSideBackground || '#ffffff';

      }

      function getBadgeData(badgeId) {
        templateEditorApiService.getBadgeData(conferenceId, badgeId)
        .then(function(response) {
          initBadgeDefineSettings(response.metadata);
        }, function(error) {

        })
      }

      function updateBadge(objectToSend) {
        templateEditorApiService.updateBadge(conferenceId, objectToSend).then(function(response) {
          $scope.showDefineMenu = false;
          var encodedConferenceId = new Hashids("Expo", 7).encode(conferenceId);
          var updatedProps = response.data.metadata;

          $rootScope.badgeWidth = updatedProps.width
          $rootScope.badgeHeight = updatedProps.height;
          $rootScope.badgeNumberSides = updatedProps.sides;
          $rootScope.badgeName = updatedProps.title;
          $rootScope.badgeBackgroundColor = updatedProps.background || '#ffffff';
          $rootScope.badgeBackSideBackgroundColor = updatedProps.backSideBackground || '#ffffff';

          $state.go('template_editor.customize', {'conference_id': encodedConferenceId , 'badge_id': objectToSend.id});
        }, function(error) {
          if(error.data){
            toastr.error(error.data.detail)
          }
        });
      }

    }
})();
