'use strict';

var app = angular.module('expo', ['ngAnimate', 'ngCookies', 'ngSanitize', 'toastr', 'ngStorage', 'ui.router', 'ngMaterial', 'ui.grid', 'nvd3', 'ngDialog',
    'app', 'LocalStorageModule', 'puElasticInput', 'ja.qr', 'ngFileUpload', 'ngImgCrop', 'ngDragDrop', 'as.sortable', 'ngTouch',
    'angular-loading-bar', 'angular-click-outside', 'vkEmojiPicker', 'moment-picker', 'mdColorPicker', 'mb-dragToReorder', 'betsol.intlTelInput'
]);

app.config(['$stateProvider', '$urlRouterProvider', '$mdThemingProvider', '$mdIconProvider', 'localStorageServiceProvider', '$locationProvider',
    'cfpLoadingBarProvider', 'ngDialogProvider', '$anchorScrollProvider', 'momentPickerProvider', 'intlTelInputOptions',
    function($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdIconProvider, localStorageServiceProvider, $locationProvider, cfpLoadingBarProvider,
        ngDialogProvider, $anchorScrollProvider, momentPickerProvider, intlTelInputOptions) {
          angular.extend(intlTelInputOptions, {
            nationalMode: true,
            utilsScript: 'assets/scripts/betsol/intlTelInput/libphonenumber/utils.js',
            defaultCountry: 'auto',
            autoFormat: true,
            autoPlaceholder: true,
            allowDropdown:false
          });

        $stateProvider
            .state('account', {
              abstract: true,
              url: '',
              views: {
                "app": {
                  templateUrl: 'app/_scenes/account/accountView.html',
                  controller: 'accountController',
                }
              },
              params : {
                errorMessage : null
              }
            })
            .state('account.login', {
              url: '/login?&redirect_to',
              views: {
                "account-feature": {
                  templateUrl: 'app/_scenes/account/login/loginView.html',
                  controller: 'loginController'
                }
              }
            })
            .state('account.create', {
                url: '/signup',
                views: {
                    "account-feature": {
                        templateUrl: 'app/_scenes/account/create/createUserView.html',
                        controller: 'createUserController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('account.forgot', {
                url: '/forgot',
                views: {
                    "account-feature": {
                        templateUrl: 'app/_scenes/account/forgot/forgotPasswordView.html',
                        controller: 'forgotPasswordController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('account.reset', {
                url: '/reset',
                views: {
                    "account-feature": {
                        templateUrl: 'app/_scenes/account/reset/resetPasswordView.html',
                        controller: 'resetPasswordController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('account.confirm', {
              url: '/confirm?&redirect_to',
                views: {
                    "account-feature": {
                        templateUrl: 'app/_scenes/account/confirm/confirmAccountView.html',
                        controller: 'confirmAccountController',
                        controllerAs: 'vm'
                    }

                }
            })
            .state('account.zapier', {
              url: '/zapier-grant-access/?client_id&state&redirect_uri&response_type&redirect_to',
              views: {
                "account-feature": {
                  templateUrl: 'app/_scenes/account/zapier/zapierAccessView.html',
                  controller: 'zapierAccessController',
                }
              }
            })
            .state('conferences.terms', {
                url: '/terms-of-service',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/terms/acceptTos/acceptTosView.html',
                        controller: 'acceptTosController',

                    }

                }
            })
            .state('conferences', {
                url: '',
                views: {
                    "app": {
                        templateUrl: 'app/_scenes/conferences/conferencesView.html'
                    }
                }
            })
            .state('conferences.welcome', {
                url: '/welcome',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/welcome/welcomeView.html',
                        controller : 'welcomeController'
                    }
                }
            })
            .state('conferences.join', {
                url: '/events/join',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/join/joinConferenceView.html',
                        controller: 'joinConferenceController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('conferences.join.select', {
                url: '/:conference_id/exhibitors/select',
                views: {
                    "conferences-features@conferences": { //overriding content of view
                        templateUrl: 'app/_scenes/conferences/_scenes/join/scenes/selectExhibitor/selectExhibitorView.html',
                        controller: 'selectExhibitorController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    exhibitorsList: [],
                    conferenceCountry: 'us'
                }
            })
            .state('conferences.join.edit', {
                url: '/:conference_id/exhibitors/:exhibitor_id/edit',
                views: {
                    "conferences-features@conferences": {
                        templateUrl: 'app/_scenes/conferences/_scenes/join/scenes/createOrEditExhibitor/createOrEditExhibitorView.html',
                        controller: 'createOrEditExhibitor',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    exhibitorDetails: {}
                }
            })
            .state('conferences.join.create', {
                url: '/:conference_id/exhibitors/create',
                views: {
                    "conferences-features@conferences": {
                        templateUrl: 'app/_scenes/conferences/_scenes/join/scenes/createOrEditExhibitor/createOrEditExhibitorView.html',
                        controller: 'createOrEditExhibitor',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    conferenceCountry: 'us'
                }
            })
            .state('conferences.overview', {
                url: '/events',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/overview/conferencesOverviewView.html',
                        controller: 'conferencesOverviewController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('conferences.admin', {
                url: '/admin_events',
                views: {
                    "conferences-features": {
                        templateUrl  : 'app/_scenes/conferences/_scenes/superadmin_overview/superadminConferencesOverviewView.html',
                        controller   : 'superadminConferencesOverviewController',
                    }
                }
            })
            .state('conferences.create', {
                url: '/create',
                views: {
                    'conferences-features': {
                        templateUrl: 'app/_scenes/conferences/_scenes/create/createConferenceView.html',
                        controller: 'createConferenceController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('conferences.dashboard', {
                url: '/events/:conference_id',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/dashboard/conferenceDashboardView.html',
                        controller: 'conferenceDashboardController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                  conference: ['$rootScope', '$stateParams', 'conferenceService', 'toastr', '$timeout', 'userPermissionsService', '$state', function($rootScope, $stateParams, conferenceService, toastr, $timeout, userPermissionsService, $state) {
                        var encodedConference_id = $stateParams.conference_id;
                        var conference_id = conferenceService.hash_conference_id.decode(encodedConference_id)[0];
                        //root variable used for phone number input
                        $rootScope.phoneIntlTelInputCtrl;
                        if ($rootScope.conference && $rootScope.conference.conference_id == conference_id) {
                            return $rootScope.conference;
                        } else {
                            return conferenceService
                                .getConferenceById(encodedConference_id)
                                .then(function(data) {
                                    $rootScope.conference = data;
                                    $rootScope.userPermissions = {
                                      admin                  : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "admin"),
                                      editConference         : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_conference"),
                                      editConferenceUsers    : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_conference_users"),
                                      editAttendeesFields    : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_attendee_fields"),
                                      downloadAttendeesAsCsv : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "download_attendees"),
                                      manageAttendees        : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_attendees"),
                                      manageExhibitors       : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_exhibitors"),
                                      manageSessions         : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_sessions"),
                                      scanAttendees          : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "scan_attendees"),
                                      canRefund              : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "can_refund"),
                                      editRegistration       : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_registration")
                                    };

                                    if ($rootScope.userPermissions.admin) {
                                      angular.forEach($rootScope.userPermissions, function(value, key) {
                                        $rootScope.userPermissions[key] = true;
                                      });
                                    }

                                }, function(error) {
                                  console.log(error);
                                    $timeout(function() {
                                        $state.go('account.login', {
                                          errorMessage: error.data.detail
                                        });
                                    }, 300);
                                });
                        }
                    }]
                }
            })
            // CONFERENCE DETAILS
            .state('conferences.dashboard.details', {
                url: '/details',
                abstract: true,
                views: {
                    "conference-details": {
                        template: "<ui-view/>"
                    }
                }
            })
            .state('conferences.dashboard.details.overview', {
                url: '',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/conferenceDetailsView.html',
                controller: 'conferenceDetailsController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.details.social_tags', {
                url: '/social-tags',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/social-tags/conferenceSocialTagsView.html',
                controller: 'conferenceSocialTagsController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.details.cover_screen', {
                url: '/cover',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/cover-screen/coverScreenView.html',
                controller: 'coverScreenController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.details.users', {
                url: '/users',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/users/conferenceUsersView.html',
                controller: 'conferenceUsersController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.details.travel', {
                url: '/travel',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/details/_scenes/travel/travelView.html',
                controller: 'travelController',
                controllerAs: 'vm'
            })
            // CONFERENCE ATTENDEES
            .state('conferences.dashboard.attendees', {
                url: '/attendees?share=',
                abstract: true,
                views: {
                    "conference-details": {
                        template: '<ui-view/>',
                    }
                }
            })
            .state('conferences.dashboard.attendees.overview', {
                url: '',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/attendeesView.html',
                controller: 'attendeesController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.attendees.legacy', {
                url: '/attendees-list-legacy',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/legacy/attendeesLegacyView.html',
                controller: 'attendeesLegacyController',
                controllerAs: 'vm'
            })
            // SESSIONS
            .state('conferences.dashboard.sessions', {
                url: '/sessions',
                abstract: true,
                views: {
                    "conference-details": {
                        template: "<ui-view/>"
                    }
                }
            })
            .state('conferences.dashboard.sessions.overview', {
                url: '',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/conferenceSessionsView.html',
                controller: 'conferenceSessionsController',
            })
            .state('conferences.dashboard.sessions.details', {
                url: '/:session_id/details',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/details/sessionDetailsView.html',
                controller: 'sessionDetailsController',
            })
            .state('conferences.dashboard.sessions.edit', {
                url: '/:session_id/edit',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/create/createSessionView.html',
                controller: 'createSessionController',
            })
            .state('conferences.dashboard.sessions.attendees', {
                url: '/:session_id/attendees/:attendees_type', // attendees_type : 'registered' || 'scanned'
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/attendees/sessionAttendeesView.html',
                controller: 'sessionAttendeesController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.sessions.upload-attendees', {
                url: '/:session_id/upload-attendees',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/upload-attendees/uploadSessionAttendeesView.html',
                controller: 'uploadSessionAttendeesController'
            })
            .state('conferences.dashboard.sessions.create', {
                url: '/create',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/create/createSessionView.html',
                controller: 'createSessionController',
                params: {
                    speakersList: null
                }
            })
            .state('conferences.dashboard.sessions.upload_sessions', {
                url: '/upload',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/upload-sessions/uploadSessionsView.html',
                controller: 'uploadSessionsController',
            })
            .state('conferences.dashboard.sessions.continuing_education', {
                url: '/education',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/continuing_education/continuingEducationView.html',
                controller: 'continuingEducationController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.sessions.speakers', {
                url: '/speakers',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/speakers/speakersView.html',
                controller: 'speakersController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.sessions.custom-session-fields', {
                url: '/fields',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/sessions/_scenes/custom_session_fields/customSessionFieldsView.html',
                controller: 'customSessionFieldsController'
            })
            // REGISTRATION
            .state('conferences.dashboard.registration', {
                url: '/registration',
                abstract: true,
                views: {
                    "conference-details": {
                        template: '<ui-view/>',
                    }
                },
                params: {
                    share: ""
                }
            })
            .state('conferences.dashboard.registration.overview', {
                url: '',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/conferenceRegistrationView.html',
                controller: 'conferenceRegistrationController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.registration.create-form', {
                url: '/create-form',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/create-form/createFormView.html',
                controller: 'createFormController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.registration.fields', {
                url: '/fields',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-fields/registrationFieldsView.html',
                controller: 'registrationFieldsController',
                controllerAs: 'vm',
                params: {
                    inEdit: false
                }
            })
            .state('conferences.dashboard.registration.levels', {
                url: '/levels',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-levels/registrationLevelsView.html',
                controller: 'registrationLevelsController',
                controllerAs: 'vm',
                params: {
                    levelId: '',
                    inEdit: false
                }
            })
            .state('conferences.dashboard.registration.level-options', {
                url: '/level-options',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-level-options/registrationLevelOptionsView.html',
                controller: 'registrationLevelOptionsController',
                controllerAs: 'vm',
                params: {
                    levelId: '',
                    inEdit: false
                }
            })
            .state('conferences.dashboard.registration.legal', {
                url: '/legal',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/legal/registrationLegalView.html',
                controller: 'registrationLegalController',
                controllerAs: 'vm',
                params: {
                    inEdit: false
                }
            })
            .state('conferences.dashboard.registration.review', {
                url: '/review',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/review/registrationReviewView.html',
                controller: 'registrationReviewController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.registration.registration-code', {
                url: '/code',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/registration-code/registrationCodeView.html',
                controller: 'registrationCodeController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.registration.transactions', {
                url: '/transactions',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/transactions/transactionsView.html',
                controller: 'transactionsController',
            })
            // .state('conferences.dashboard.registration.visitors', {
            //     url: '/visitors',
            //     templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/registration/_scenes/visitors/visitorsView.html',
            //     controller: 'visitorsController',
            // })
            .state('preview', {
                url: '/preview/:conference_id',
                views: {
                    "app": {
                        templateUrl: 'app/views/preview.html',
                        controller: 'RegistrationFormController',
                    }
                }
            })
            .state('ordercomplete', {
                url: '/order-complete/:conference_id/:redir/:order_code',
                views: {
                    "app": {
                        templateUrl: 'app/views/registration_iframe_steps/order_complete.html',
                        controller: 'RegistrationFromOrderCompleteController',
                    }
                }
            })
            .state('preview.step1', {
                url: '/step-1',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_1.html',
                        controller: 'RegistrationFromStep1Controller'
                    }
                },
                params: {
                    previous: null,
                    edit: null,
                    previousStep: null
                }
            })
            .state('preview.step2', {
                url: '/step-2',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_2.html',
                        controller: 'RegistrationFormStep2Controller'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.step3', {
                url: '/step-3',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_3.html',
                        controller: 'RegistrationFormStep3Controller'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null,
                    individualEdit: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.step_guests', {
                url: '/step-guests',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_guests.html',
                        controller: 'RegistrationFormGuestsController'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null,
                    individualEdit: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.step4', {
                url: '/step-4',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_4.html',
                        controller: 'RegistrationFormStep4Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.step5', {
                url: '/review',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_5.html',
                        controller: 'RegistrationFormStep5Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.intermediary_step', {
                url: '/step-submitted',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/intermediary_step.html',
                        controller: 'RegistrationFormIntermediaryStepController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.pre_register_step1', {
                url: '/pre-register-step-1',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/pre_register_step_1.html',
                        controller: 'RegistrationFormPreRegisterStep1Controller'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.pre_register_step2', {
                url: '/pre-register-step-2',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/pre_register_step_2.html',
                        controller: 'RegistrationFormPreRegisterStep2Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.registration_last_step', {
                url: '/registration-complete',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/registration_complete.html',
                        controller: 'RegistrationFormCompleteController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('preview.register_another', {
                url: '/register-another',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/register_another.html',
                        controller: 'RegistrationFormRegisterAnotherController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration', {
                url: '/registration/:conference_id',
                views: {
                    "app": {
                        templateUrl: 'app/views/registration.html',
                        controller: 'RegistrationFormController'
                    }
                }
            })
            .state('registration.step1', {
                url: '/step-1',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_1.html',
                        controller: 'RegistrationFromStep1Controller'
                    }
                },
                params: {
                    previous: null,
                    edit: null,
                    previousStep: null
                }
            })
            .state('registration.step2', {
                url: '/step-2',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_2.html',
                        controller: 'RegistrationFormStep2Controller'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.step3', {
                url: '/step-3',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_3.html',
                        controller: 'RegistrationFormStep3Controller'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null,
                    individualEdit: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.step_guests', {
                url: '/step-guests',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_guests.html',
                        controller: 'RegistrationFormGuestsController'
                    }
                },
                params: {
                    stepNumber: null,
                    edit: null,
                    previousStep: null,
                    individualEdit: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.step4', {
                url: '/step-4',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_4.html',
                        controller: 'RegistrationFormStep4Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.step5', {
                url: '/review',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/step_5.html',
                        controller: 'RegistrationFormStep5Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.intermediary_step', {
                url: '/step-submitted',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/intermediary_step.html',
                        controller: 'RegistrationFormIntermediaryStepController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.pre_register_step1', {
                url: '/pre-register-step-1',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/pre_register_step_1.html',
                        controller: 'RegistrationFormPreRegisterStep1Controller'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.pre_register_step2', {
                url: '/pre-register-step-2',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/pre_register_step_2.html',
                        controller: 'RegistrationFormPreRegisterStep2Controller'
                    }
                },
                params: {
                    stepNumber: null
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.registration_last_step', {
                url: '/registration-complete',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/registration_complete.html',
                        controller: 'RegistrationFormCompleteController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('registration.register_another', {
                url: '/register-another',
                views: {
                    "form-container": {
                        templateUrl: 'app/views/registration_iframe_steps/register_another.html',
                        controller: 'RegistrationFormRegisterAnotherController'
                    }
                },
                resolve: {
                    redirectToFirstStep: ['SharedProperties', function(SharedProperties) {
                        return !(!SharedProperties.hasOwnProperty('registeredAttendee') || _.isEmpty(SharedProperties.registeredAttendee));
                    }]
                }
            })
            .state('template_editor', {
                url: '/events/:conference_id/badges', //this will have conference id before
                views: {
                    "app": {
                        templateUrl: 'app/views/template_editor/templateEditorView.html',
                        controller: 'templateEditorController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                  conference: ['$rootScope', '$stateParams', 'conferenceService', 'toastr', '$timeout', 'userPermissionsService', function($rootScope, $stateParams, conferenceService, toastr, $timeout, userPermissionsService) {
                        var encodedConference_id = $stateParams.conference_id;
                        var conference_id = conferenceService.hash_conference_id.decode(encodedConference_id)[0];

                        if ($rootScope.conference && $rootScope.conference.conference_id == conference_id) {
                            return $rootScope.conference;
                        } else {
                            return conferenceService
                                .getConferenceById(encodedConference_id)
                                .then(function(data) {
                                    $rootScope.conference = data;
                                    $rootScope.userPermissions = {
                                      admin: userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "admin"),
                                      editConference : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_conference"),
                                      editConferenceUsers : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_conference_users"),
                                      editAttendeesFields : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_attendee_fields"),
                                      downloadAttendeesAsCsv : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "download_attendees"),
                                      manageAttendees : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_attendees"),
                                      manageExhibitors : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_exhibitors"),
                                      manageSessions : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "manage_sessions"),
                                      scanAttendees : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "scan_attendees"),
                                      canRefund : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "can_refund"),
                                      editRegistration : userPermissionsService.findIfPermissionExists($rootScope.conference.permissions, "edit_registration")
                                    };

                                }, function(error) {
                                    tostr.error("An error has occured while trying to fetch the conference details. Redirecting to overview...");
                                    $timeout(function() {
                                        $state.go('conferences.overview');
                                    }, 300);
                                });
                        }
                    }]
                },
                abstract: true
            })
            .state('template_editor.define', {
                url: '/define',
                views: {
                    "options-menu": {
                        templateUrl: 'app/views/template_editor/define/defineOptionsMenuPartial/defineOptionsMenuPartialView.html',
                        controller: 'defineOptionsMenuPartialController'
                    }
                }
            })
            .state('template_editor.define_badge', {
                url: '/:badge_id/define',
                views: {
                    "options-menu": {
                        templateUrl: 'app/views/template_editor/define/defineOptionsMenuPartial/defineOptionsMenuPartialView.html',
                        controller: 'defineOptionsMenuPartialController'
                    }
                }
            })
            .state('template_editor.customize', {
                url: '/:badge_id/customize',
                views: {
                    "options-menu": {
                        templateUrl: 'app/views/template_editor/customize/customizeOptionsMenuPartial/customizeOptionsMenuPartialView.html',
                        controller: 'customizeOptionsMenuPartialController'
                    },
                    "components-menu": {
                        templateUrl: 'app/views/template_editor/customize/componentsMenu/componentsMenuView.html',
                        controller: 'componentsMenuController'
                    },
                    "layers-menu": {
                        templateUrl: "app/views/template_editor/customize/layersMenu/layersMenuView.html",
                        controller: "layersMenuController"
                    }
                }
            })
            .state('template_editor.review', {
                url: '/:badge_id/review',
                views: {
                    "options-menu": {
                        templateUrl: 'app/views/template_editor/review/reviewOptionMenuPartial/reviewOptionsMenuPartialView.html',
                        controller: 'reviewOptionsMenuPartialController'
                    },
                    "components-menu": {
                        templateUrl: 'app/views/template_editor/review/mailMergeMenu/mailMergeMenuView.html',
                        controller: 'mailMergeMenuController'
                    }
                }
            })
            .state('template_editor.order', {
                url: "/order",
                abstract: true
            })
            .state('template_editor.order.details', {
                url: "/details",
                views: {
                    "options-menu@template_editor": {
                        templateUrl: 'app/views/template_editor/order/details/orderDetailsMenuPartial/orderDetailsMenuPartialView.html',
                        controller: 'orderDetailsMenuPartialController'
                    },
                    "order-container-view@template_editor": {
                        templateUrl: 'app/views/template_editor/order/details/orderDetailsView.html',
                        controller: "orderDetailsController"
                    }
                }
            })
            .state('conferences.dashboard.attendees.badge_print_settings', {
                url: '/templates/settings',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/badge_print_settings/badgePrintSettingsView.html',
                controller: 'badgePrintSettingsController',
            })
            .state('conferences.dashboard.attendees.expo_badges', {
                url: '/templates/expo',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/expo_badges/expoBadgesView.html',
                controller: 'expoBadgesController',
            })
            .state('conferences.dashboard.exhibitors', {
                url: '/exhibitors',
                abstract: true,
                views: {
                    "conference-details": {
                        template: '<ui-view/>',
                    }
                }
            })
            .state('conferences.dashboard.exhibitors.overview', {
                url: '',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/exhibitorsView.html',
                controller: 'exhibitorsController'
            })
            .state('conferences.dashboard.exhibitors.contacts', {
                url: '/contacts',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/support_contacts/supportContactsView.html',
                controller: 'supportContactsController'
            })
            .state('conferences.dashboard.exhibitors.materials', {
                url: '/materials',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/upload_materials/uploadMaterialsView.html',
                controller: 'uploadMaterialsController'
            })
            .state('conferences.dashboard.exhibitors.upload', {
                url: '/upload',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/upload_exhibitors_list/uploadExhibitorsView.html',
                controller: 'uploadExhibitorsController'
            })
            .state('conferences.dashboard.exhibitors.payments', {
                url: '/payments',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/payments/paymentsView.html',
                controller: 'exhibitorPaymentsController'
            })
            .state('conferences.dashboard.exhibitors.exhibiting_details', {
                url: '/exhibiting',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/exhibitors/_scenes/exhibiting_details/exhibitingDetailsView.html',
                controller: 'exhibitingDetailsController',
                controllerAs: 'vm'
            })
            .state('conferences.dashboard.attendees.upload', {
                url: '/upload',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/upload/uploadAttendeesView.html',
                controller: 'uploadAttendeesController',
            })
            .state('conferences.dashboard.attendees.custom_fields', {
                url: '/fields',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/custom_attendee_fields/customAttendeeFieldsView.html',
                controller: 'customAttendeeFieldsController',
            })
            .state('conferences.dashboard.attendees.templates', {
                url: '/templates/avery',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/avery_templates/averyTemplatesView.html',
                controller: 'averyTemplatesController',
            })
            .state('conferences.dashboard.attendees.materials', {
                url: '/materials',
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/attendees/_scenes/materials/attendeeMaterialsView.html',
                controller: 'attendeeMaterialsController',
            })
            .state('conferences.dashboard.notifications', {
                url: '/notifications',
                abstract: true,
                views: {
                    "conference-details": {
                        template: '<ui-view/>',
                    }
                }
            })
            .state('conferences.dashboard.notifications.overview', {
                url: "",
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/notifications/pushNotificationsView.html',
                controller: 'pushNotificationsController',
            })
            .state('conferences.dashboard.notifications.setup', {
                url: "/setup",
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/notifications/_scenes/setup_notifications/setupNotificationsView.html',
                controller: 'setupNotificationsController',
                params: {
                    obj: null
                }
            })
            .state('conferences.dashboard.notifications.manage', {
                url: "/manage",
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/notifications/_scenes/manage_notifications/manageNotificationsView.html',
                controller: 'manageNotificationsController',
                params: {
                    obj: null
                }
            })
            .state('conferences.dashboard.revenue', {
                url: '/revenue',
                abstract: true,
                views: {
                    "conference-details": {
                        template: '<ui-view/>',
                    }
                }
            })
            .state('conferences.dashboard.revenue.overview', {
                url: "",
                templateUrl: 'app/_scenes/conferences/_scenes/dashboard/_scenes/revenue/superAdminConferenceRevenueView.html',
                controller: 'superAdminConferenceRevenue',
            })
            .state('conferences.exhibitor_toolkit', {
                url: '/events/:conference_id/exhibitors/toolkit',
                abstract: true,
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/exhibitorDashboardView.html',
                        controller: 'exhibitorDashboardController',
                    }
                },
                resolve: {
                    conference: ['$rootScope', '$stateParams', 'exhibitorDashboardDataService', function($rootScope, $stateParams, exhibitorDashboardDataService) {
                        var conferenceId = exhibitorDashboardDataService.hashConferenceId.decode($stateParams.conference_id)[0];
                        return exhibitorDashboardDataService
                            .getConferenceAsExhibitor(conferenceId)
                            .then(function(data) {
                                $rootScope.exhibitorConference = data;
                            }, function(error) {
                                console.log(error);
                            });
                    }]
                }
            })
            .state('conferences.exhibitor_toolkit.details', {
                url: '/details',
                views: {
                    "conference-details": {
                        templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/details/exhibitorConferenceDetailsView.html',
                        controller: 'exhibitorConferenceDetailsController',
                    }
                },
            })
            .state('conferences.exhibitor_toolkit.users', {
                url: '/users',
                views: {
                    "conference-details": {
                        templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/users/exhibitorConferenceUsersView.html',
                        controller: 'exhibitorConferenceUsersController',
                    }
                }
            })
            .state('conferences.exhibitor_toolkit.company_profile', {
                url: '/profile',
                views: {
                    "conference-details": {
                        templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/company/exhibitorConferenceCompanyView.html',
                        controller: 'exhibitorConferenceCompanyController',
                    }
                }
            })
            .state('conferences.exhibitor_toolkit.lead_retrieval', {
                url: '/leads',
                views: {
                    "conference-details": {
                        templateUrl: 'app/_scenes/conferences/_scenes/exhibitor_dashboard/_scenes/lead_retrieval/exhibitorConferenceLeadRetrievalView.html',
                        controller: 'exhibitorConferenceLeadRetrievalController',
                    }
                }
            })
            .state('conferences.support', {
                url: '/support',
                views: {
                    "conferences-features": {
                        templateUrl: 'app/_scenes/conferences/_scenes/support/supportView.html',
                        controller: 'supportController'
                    }
                }
      })
      .state('connect', {
        url : "/connect?scope&code",
        views : {
          "app" : {
            templateUrl : 'app/_scenes/stripe-connect/stripeConnectView.html',
            controller  : 'stripeConnectController'
          }
        }
            })

        $locationProvider.hashPrefix("!");
        localStorageServiceProvider.setPrefix('expoPass');

        $urlRouterProvider.otherwise('/events');
        $urlRouterProvider.when('', '/#!/login');
        $urlRouterProvider.when("", '/#!/login');
        $urlRouterProvider.when('/events/:conference_id', '/events/:conference_id/details');
        $urlRouterProvider.when('/conferences', '/events');

        ngDialogProvider.setDefaults({
            closeByNavigation: true,
            closeByEscape: true
        });

        momentPickerProvider.options({
            autoclose: true,
            local: "en",
            hoursFormat: 'h a',
        });


        cfpLoadingBarProvider.includeSpinner = false;
    }
])


app.run(function($rootScope, $state, $location, localStorageService, $timeout, $http, UserService, $q, ngDialog) {
  var openDialogs = 0;

  $rootScope.$on('ngDialog.opened', function(e, $dialog) {
    openDialogs++;
    $rootScope.pageBlurred = true;
  });

  $rootScope.$on('ngDialog.closed', function(e, $dialog) {
    openDialogs--;
    if (openDialogs === 0) {
      $rootScope.pageBlurred = false;
      $rootScope.$apply();
    }
  });
 //this will be enabled when the GDPR will be released
  // $rootScope.$on('$stateChangeSuccess',  function (event, toState, toParams, fromState, fromParams, options) {
  //
  //   if (localStorageService.get('access_token') ) {
  //     if(localStorageService.get('currentUser')){
  //       var currentUser = localStorageService.get('currentUser');
  //       currentUser = (typeof currentUser == 'string')? JSON.parse(currentUser):currentUser;
  //         if((currentUser.legal.terms_of_service.require_acceptance || currentUser.legal.privacy_policy.require_acceptance) && !currentUser.is_superuser){
  //           $state.go("conferences.terms");
  //         }
  //         else{
  //           $state.go(toState, toParams);
  //         }
  //       }
  //     }
  //     else{
  //       $state.go(toState, toParams);
  //     }
  // });

  if ($location.$$url === "") {
    if (localStorageService.get('access_token')) {
      $location.path("/events");
    } else {
      $location.path("/login");
    }
  }
})
