require.register("environmentDetector", function(exports, require, module) {
    var detectEnvironment = function() {
        var env              = 'PROD';
        var location         = window.location.hostname + "/";
        var developmentHosts = [
            "localhost/",
            'expo-frontend.s3-website-us-west-2.amazonaws.com/',
            'expo-frontend-prod.s3-website-us-west-2.amazonaws.com/',
            'expo-dev.s3-website-us-west-2.amazonaws.com/',
            'expo-registration.s3-website-us-west-2.amazonaws.com/',
            'expo-editor.s3-website-us-west-2.amazonaws.com/',
            'expo-feature.s3-website-us-west-2.amazonaws.com/'
        ];

        for (var i = 0; i < developmentHosts.length; i++) {
          if (location.indexOf(developmentHosts[i]) > -1) {
            env = 'DEV';
          }
        };
        return env;
    };

    module.exports = detectEnvironment;
});
