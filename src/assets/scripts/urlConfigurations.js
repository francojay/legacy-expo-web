require.register("urlConfigurations", function(exports, require, module) {
    var detectEnvironment = require("environmentDetector");
    var currentDetectedEnvironment = detectEnvironment();

    var configurator = {
        "getAuthUrl": function() {
            if (currentDetectedEnvironment == 'DEV') return 'https://api-dev.expopass.com/o/token/';
            else if (currentDetectedEnvironment == 'PROD') return 'https://api.expopass.com/o/token/';
            else if (currentDetectedEnvironment == 'SANDBOX') return 'https://fppfvciyi1.execute-api.us-west-2.amazonaws.com/sandbox/o/token/';
        },
        "getBaseUrl": function() {
            if (currentDetectedEnvironment == 'DEV') return 'https://api-dev.expopass.com/';
            else if (currentDetectedEnvironment == 'PROD') return 'https://api.expopass.com/';
            else if (currentDetectedEnvironment == 'SANDBOX') return 'https://fppfvciyi1.execute-api.us-west-2.amazonaws.com/sandbox/';
        },
        "getRegisterUrl": function() {
            if (currentDetectedEnvironment == 'DEV') return 'https://api-dev.expopass.com/api/register/';
            else if (currentDetectedEnvironment == 'PROD') return 'https://api.expopass.com/api/register/';
            else if (currentDetectedEnvironment == 'SANDBOX') return 'https://fppfvciyi1.execute-api.us-west-2.amazonaws.com/sandbox/api/register/';
        },
        "getStripePublishableKey": function() {
            if (currentDetectedEnvironment == 'DEV') return 'pk_test_jhZTPtSmu2SfMqTZ9EnGZvOo';
            else if (currentDetectedEnvironment == 'PROD') return 'pk_live_rpe80tCl1hjfTFORfbORdenw';
            else if (currentDetectedEnvironment == 'SANDBOX') return 'pk_test_jhZTPtSmu2SfMqTZ9EnGZvOo';
        }
    };

    module.exports = configurator;
});