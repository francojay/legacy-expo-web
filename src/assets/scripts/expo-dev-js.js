var showForm = function(div, key, width){
    var urlParams = new URLSearchParams(window.location.search);
    if (urlParams.get('ticket')) {
        var ticket = urlParams.get('ticket').trim();
    }

    var location = window.location.href;

    if (ticket) {
        document.getElementById(div).innerHTML=
        "<iframe src='http://expo-registration.s3-website-us-west-2.amazonaws.com/#!/registration/" +
            key + "/step-1?ticket=" + ticket + "&callback=" + location + "' style='overflow: visible; ' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' onload=\"autoResize('formFrame', )\" id='formFrame' allowfullscreen></iframe>";
        } else {
            document.getElementById(div).innerHTML=
        "<iframe src='http://expo-registration.s3-website-us-west-2.amazonaws.com/#!/registration/" +
            key + "/step-1?callback=" + location + "' style='overflow: visible; ' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' onload=\"autoResize('formFrame', )\" id='formFrame' allowfullscreen></iframe>";
        }

    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var eventer = window[eventMethod];
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
    eventer(messageEvent,function(e) {

    var newHeight = e.data;

    document.getElementById(div).setAttribute("style","height:" + newHeight + 'px; width:' + width + 'px');
    document.getElementById('formFrame').setAttribute("style","height:" +  '300px; width:100%');

    },false);
}
