function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[#?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function autoResize(frameId) {
  setSize(frameId)

  // var images = document.getElementById(frameId).contentWindow.document.querySelectorAll('img');
  // Array.prototype.forEach.call(images, function(element, index) {
  //  element.addEventListener('load', function(e) {
  //    setSize(frameId);
  //  }, false);
  // });
}

function setSize(frameId) {
  // document.getElementById(frameId).contentWindow.document.body.style.margin = 0;
  // document.getElementById(frameId).contentWindow.document.body.style.padding = 0;
  // var newheight = document.getElementById(frameId).contentWindow.document.body.scrollHeight;
  // var newwidth = document.getElementById(frameId).contentWindow.document.body.scrollWidth;
  // document.getElementById(frameId).style.height = (newheight) + "px";
  // document.getElementById(frameId).style.width = (newwidth) + "px";
  postMessage(document.getElementById(frameId).style.height,'*');
}

var showForm = function(div, key, width) {
  window.scrollPositions   = [];
  window.hostPageHasLoaded = false;
  window.formHasLoaded     = false;
  window.scrollTo(0, 1);
  window.addEventListener('scroll', initScroll);

  loadJQuery(div, key, width);

  var myInterval = setInterval(function() {
    if (window.hostPageHasLoaded && window.formHasLoaded) {
      var parentWithOffset  = document.getElementById('IframeDivId').offsetParent;
      var $parentWithOffset = jQuery(parentWithOffset);

      if ($parentWithOffset[0].nodeName === "BODY") {
        var topOffset = document.getElementById('IframeDivId').offsetTop
      }  else {
        var topOffset         = $parentWithOffset.offset().top;
      }

      window.scrollPositions.push(topOffset);
      window.removeEventListener('scroll', initScroll);
      clearInterval(myInterval);
    } else {
      console.log('ceva nu e loaded');
    }
  }, 300);

}

var loadJQuery = function(div, key, width) {
  if (typeof jQuery == 'undefined') {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
    script.type = 'text/javascript';
    script.onload = function() {
      jQuery(window).load(function() {
        window.hostPageHasLoaded = true;
        initForm(div, key, width);
      });
    }
    document.getElementsByTagName("head")[0].appendChild(script);
  } else {
    window.hostPageHasLoaded = true;
    initForm(div, key, width);
  }

}

var initForm = function(div, key, width) {
  var ticket = getParameterByName('ticket', location);

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function()
  {n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  if (ticket) {
    var originOfJs = document.getElementById('expo_js').src.split('/assets')[0];
      document.getElementById(div).innerHTML=
      "<iframe src=" + originOfJs + "/#!/registration/" +
key + "/step-1?ticket=" + ticket + "&callback=" + location + "' style='overflow: visible; ' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' id='expoform' allowfullscreen></iframe>";
      } else {
        var originOfJs = document.getElementById('expo_js').src.split('/assets')[0];
        var iframeHtmlText = "<iframe src='" + "http://expo-registration.s3-website-us-west-2.amazonaws.com/#!/registration/" + key + "/info'" + " style='width: 100%;height:100%;' onload=\"autoResize('formFrame')\" id='formFrame' allowfullscreen frameborder='0'></iframe>"
        document.getElementById(div).innerHTML= iframeHtmlText;

      }

  var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
  eventer(messageEvent,function(e) {
      if (isNaN(e.data)) {
          if (e.data['task'] == 'scroll_top') {
            var parentWithOffset  = document.getElementById('IframeDivId').offsetParent;
            var $parentWithOffset = jQuery(parentWithOffset);

            if ($parentWithOffset[0].nodeName === "BODY") {
              var topOffset = document.getElementById('IframeDivId').offsetTop
            }  else {
              var topOffset = $parentWithOffset.offset().top;
            }

            window.scrollPositions.push(topOffset);

            var max = window.scrollPositions.reduce(function(a, b) {
              return Math.ceil(Math.max(a, b));
            });
            window.scrollTo(0, max);
          } else {
            if (e.data['task'] == 'form_loaded') {
              window.formHasLoaded = true;
            } else {
              var message = e.data;
              try {
                  var obj = JSON.parse(message);
                  if (obj.func == 'analytics' && obj.tracking_code) {
                      ga('create', obj.tracking_code, 'auto');
                      ga('send', {
                        hitType: 'event',
                        eventCategory: 'ExpoRegistration',
                        eventAction: 'Purchase',
                        eventLabel: obj.code,
                        eventValue: obj.price
                      });
                  }
                  if(obj.func == 'facebook' && obj.facebook_tracking_code){
                    fbq('init', obj.facebook_tracking_code);
                    fbq('track', obj.page,obj.event_object);
                  }
              }
              catch(err) {
                  console.log(e);
              }
            }
          }
      } else {
          var newHeight = e.data;
          document.getElementById(div).setAttribute("style","height:" + newHeight + 'px; width:' + width + 'px');
          //document.getElementById('expoform').setAttribute("style","height:" + newHeight + 'px; width:100%');
      }
  },false);
}

function initScroll() {
  if (window.scrollPositions.length > 0) {
    var output = [];
    for (var i=0; i < window.scrollPositions.length; i++) {
        if (!output[output.length-1] || output[output.length-1].value != window.scrollPositions[i])
            output.push({value: window.scrollPositions[i], times: 1})
        else
            output[output.length-1].times++;
    }
    var max = window.scrollPositions.reduce(function(a, b) {
      return Math.ceil(Math.max(a, b));
    });

    window.scrollTo(0, max);
  }
}
